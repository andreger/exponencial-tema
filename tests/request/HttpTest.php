<?php

namespace Exponencial\Core\tests\Exponencial\Core\tests\http;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use PHPUnit\Framework\TestCase;

class HttpTest extends TestCase
{
    private const SITE_URL = "http://exponencial";

    protected function setUp()
    {
        $this->markTestSkipped("Https são lentos. Ativar ao subir o código.");
    }

    /**
     * @group ignore
     */
    public function testPaginas(): void
    {
        $client = new Client();

        $paginas = [
            "",
            "/cursos-por-concurso",
            "/questoes/main/resolver_questoes"
        ];

        foreach ($paginas as $pagina) {
            $url = self::SITE_URL . $pagina;
            $response = null;
            $code = null;

            try {
                $response = $client->request("GET", $url);
                $code = $response->getStatusCode();
            } catch (ClientException $e) {
                $code = 400;
            } catch (ServerException $e) {
                $code = 500;
            }

            $this->assertEquals(200, $code, "A url $url retornou um erro da família $code");
        }
    }
}
