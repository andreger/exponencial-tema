var gulp = require('gulp'),
    clean = require('gulp-clean'),
    cssmin = require('gulp-cssmin'),
    htmlmin = require('gulp-htmlmin'),
    imagemin = require('gulp-imagemin'),
    rev = require('gulp-rev'),
    uglify = require('gulp-uglify-es').default,
    usemin = require('gulp-usemin');

gulp.task('clean', function() {
    return gulp.src('dist', {allowEmpty: true})
        .pipe(clean());
});

gulp.task('copy', function() {
    return gulp.src('src/**/*',  {allowEmpty: true, dot: true, buffer:false})
        .pipe(gulp.dest('dist'));
});

gulp.task('build-img', function() {
    return gulp.src('dist/core/resources/img/**/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/core/resources/img'));
});

gulp.task('usemin', function() {
    return gulp.src('dist/**/*.html.twig')
        .pipe(usemin({
            'path': 'dist',
            'assetsDir': 'dist',
            'outputRelativePath': '/',
            'js' : [function() { return uglify() }, function() { return rev() }],
            'css' : [function() { return cssmin() }, function() { return rev() }],
            'html': [ function() { return htmlmin({ collapseWhitespace: true }) }]
        }))
        .pipe(gulp.dest('dist'))
});

gulp.task('default', gulp.series('clean', 'copy', 'usemin'));

//gulp.watch('src/**/*.*').on('change', gulp.series('default'));