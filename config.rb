require 'compass/import-once/activate'
# Require any additional compass plugins here.

# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "src/core/resources/css"
sass_dir = "src/core/resources/sass"
images_dir = "src/core/resources/img"
javascripts_dir = "src/core/resources/js"

# You can select your preferred output style here (can be overridden via the command line):
# output_style = :expanded or :nested or :compact or :compressed

# To enable relative paths to assets via compass helper functions. Uncomment:
# relative_assets = true

# To disable debugging comments that display the original location of your selectors. Uncomment:
line_comments = false


# If you prefer the indented syntax, you might want to regenerate this
# project again passing --syntax sass, or you can uncomment this:
# preferred_syntax = :sass
# and then run:
# sass-convert -R --from scss --to sass sass scss && rm -rf sass && mv scss sass

# Customização para definição de path de imagens a partir do ambiente

#file = File.open("../.env")

#case file.read
#when "prd"
#  http_generated_images_path = "https://static0.exponencialconcursos.com.br/core/img"
#when "tst"
#  http_generated_images_path = "http://homol-static.exponencialconcursos.com.br/core/img"
#when "sbx"
#  http_generated_images_path = "http://sandbox-static.exponencialconcursos.com.br/core/img"
#else
#  http_generated_images_path = "http://exponencial/wp-content/themes/academy/core/resources/img"
#end

http_generated_images_path = "/wp-content/themes/academy/core/resources/img"
