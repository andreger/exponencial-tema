<?php
/*
Template Name: Template-Depoimentos
*/
?>
<?php get_header(); ?>
     <section id="">
      <div class="section_head">
        <div class="row">
          <div class="section_head_img">
          	<img src="<?php echo get_template_directory_uri(); ?>/src/images/artigos.png" alt="" class="alignnone">
          </div>
          <div class="header_text">Deplomentas</div>
        </div>
      </div>
    </section>
    
    <div class="section_content secbg_2">
    <div class="row">
    <?php
        // The Query
        query_posts('post_type=testimonial');
        
        // The Loop
        while ( have_posts() ) : the_post();
        ?>
		
    
    	
        
        
		<div class="sixcol fleft h_wid_full m_bt">
            	<div class=" elevencol fleft inner_area">
                    <div class="fivecol fleft wid_full">
                        <div class="person_img_area">
                             <?php
if( has_post_thumbnail() ) { ?>
<!--<a href=<?php the_permalink(); ?>"><img src="<?php the_post_thumbnail(); ?>" width="50x" height="50px" title="<?php the_title(); ?>" /></a>-->
<?php the_post_thumbnail();?>

<?php } else { ?>
<img src="<?php echo get_template_directory_uri(); ?>/src/images/default_avatar.jpg" />
<?php } ?>
                        </div>
                    </div>
                    <div class="sevencol fright wid_full">
                        <div class="sl_p_nome">
                           <?php the_title();?>
                        </div>
                        <div class="sl_p_dis">
                            <?php the_excerpt();?>
                        </div>
                        <div class="sl_p_text">
                    
                             <?php the_date('d-m-Y'); ?>
                        </div>
      
                        <div class="sl_bt_area">
                            <a href="<?php the_permalink();?>">
                                <img src="<?php echo get_template_directory_uri(); ?>/src/images/ler_depoimento_completo_exponencial.png" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
			

        <?php
        endwhile;
        
        // Reset Query
        wp_reset_query();
        ?>
			</div>
</div>
<?php get_footer(); ?>