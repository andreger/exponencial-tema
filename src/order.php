<?php
// Template Name:order

redirecionar_se_nao_estiver_logado();

get_header ();

KLoader::model('PedidoModel');
KLoader::helper('UiMensagemHelper');
KLoader::helper('PedidoHelper');

$user_ID = get_current_user_id ();

$is_migrar_agora = isset($_GET['mig'])?$_GET['mig']:0;

$is_migrar_pedidos = PedidoHelper::migrar_pedidos($user_ID, $is_migrar_agora);
?>
<div class="container">
<div class="row pt-1 pt-md-5 pb-5"  id="minha-conta">
	<div class="col-12">
		<?php if($_POST['Cancelar']){
			$num_pedido = $_POST['PedidoId'];
			PedidoModel::alterar_status($num_pedido, 'wc-cancelled'); 
			echo UiMensagemHelper::get_mensagem_sucesso_html('<div class="text-center">Produto cancelado com sucesso.</div>');
		} ?> 
	</div>
	
	<!--Barra superior -->
	<?php KLoader::view("minha_conta/micon/micon_barra_superior") ?>

	<?php if($is_migrar_pedidos && !$is_migrar_agora): ?>
		<!-- Carregando -->
		<?php KLoader::view("minha_conta/micon/micon_carregando") ?>
	<?php else: ?>
		<!-- Abas -->
		<?php KLoader::view("minha_conta/micon/micon_abas", ['user_id' => $user_ID]) ?>
	<?php endif; ?>

</div>
</div>
<div class="sectionlargegap"></div>
<?php get_footer(); ?>