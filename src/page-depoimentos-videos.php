<?php
get_header();

KLoader::model('DepoimentoModel');
KLoader::helper("UrlHelper");

$url_base = UrlHelper::get_entrevistas_videos_url();

// Montagem da paginação	
$pg = isset($_GET['pg']) ? $_GET['pg'] : "1";
$total = DepoimentoModel::contar_depoimentos([DEPOIMENTO_DEPOIMENTO_VIDEO]);
$pattern = $url_base."?pg=(:num)";
$limit = LIMITE_DEPOIMENTOS;
$paginator = new Paginator($total, $limit, $pg, $pattern);
$offset = $pg ? ($pg - 1) * $limit : 0;

$depoimentos = DepoimentoModel::listar_depoimentos([DEPOIMENTO_DEPOIMENTO_VIDEO], $offset, $limit);

$data['depoimentos'] = $depoimentos;
$data['paginacao'] = $paginator;
$data['titulo'] = "Depoimentos";
$data['apresentacao'] = get_post_field('post_content');
$data['voltar'] = TRUE;

KLoader::view('depoimentos/listagem_em_video', $data);

get_footer();

