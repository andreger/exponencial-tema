<?php 
get_header(); 
the_post();
?>
<div class="post-content">
	<?php the_content(); ?>
</div>
<?php get_footer(); ?>