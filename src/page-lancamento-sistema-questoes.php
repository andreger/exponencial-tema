<html>
<head>
	<link rel="shortcut icon" href="/questoes/favicon.ico" />
	<link href=" <?php bloginfo('template_url')?>/style.css" rel="stylesheet">
	<link href=" <?php bloginfo('template_url')?>/custom.css" rel="stylesheet">
    <link href=" <?php bloginfo('template_url')?>/mobile.css" rel="stylesheet">
    
    <link href=" <?php bloginfo('template_url')?>/lancamento.css" rel="stylesheet">
    <script src="/wp-content/themes/academy/js/jquery-1.9.1.min.js"></script>
    
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<meta name="viewport" content="user-scalable=yes, initial-scale=1">
    
</head>


<body>
	<div class="lancamento-sq-capa">
		<div><?php echo get_tema_image_tag("logo-colorido.png") ?></div>
		<div class="lancamento-texto-div">
			<div style="font-weight: bold; font-size: 40px">Vem aí</div>
			<div class="lancamento-sq-texto-medio">Uma plataforma</div>
			<div class="lancamento-sq-texto-medio">para aprimorar</div>
			<div class="lancamento-sq-texto-medio">seu estudo</div>
			<div class="lancamento-texto-grande">EXPONENCIALMENTE</div>
		</div>
		<div class="lancamento-sq-desconto"><span style="color:#ffda25;font-size:40px; font-weight:bold; vertical-align:middle;">60%</span> de desconto para os 200 primeiros inscritos!</div>
	</div>
	
	<div style="margin-top: 20px"  class="lancamento-sq-tabela">
		<div class="col-3 column lancamento-sq-tabela-1">
			<div>
				<div class="col-4 column " style="text-align: right; margin: 20px 12px;">
					<?php echo get_tema_image_tag("sq-icone1.png") ?>
				</div>
				<div class="col-7 column" style="line-height: 16px; margin: 20px 0;">
					Mais de 350 mil<br>questões online dos mais<br>diversos concursos.
				</div>
			</div>
			<div style="clear: both"></div>
			
			<div>
				<div class="col-4 column" style="text-align: right; margin: 20px 12px;">
					<?php echo get_tema_image_tag("sq-icone2.png") ?>
				</div>
				<div class="col-7 column" style="line-height: 16px; margin: 20px 0;">
					Simulados online focados,<br>revisados e comentados<br>por professores.
				</div>
			</div>
			<div style="clear: both"></div>
			
			<div>
				<div class="col-4 column" style="text-align: right; margin: 20px 12px;">
					<?php echo get_tema_image_tag("sq-icone3.png") ?>
				</div>
				<div class="col-7 column" style="line-height: 16px; margin: 20px 0;">
					Ranking de simulados, com<br>análise de desempenho do<br>aluno, por matéria e assunto.
				</div>
			</div>
			
		</div>
		
		<div class="col-4 column full-width">
			<div style="color: #0674A5; font-size: 24px; font-weight: bold; text-transform: uppercase;">Quero me inscrever agora!</div>
			<form method="post" action="https://ymlp.com/subscribe.php?id=ghhmwjbgmgb" id="form-lancamento">
				<table border="0" align="center" cellspacing="0" cellpadding="0">
					<tr><td valign="top"><input type="text" name="YMP0" size="20" placeholder="E-mail" /></td></tr>
					<tr><td valign="top"><input type="text" name="YMP1" size="20" placeholder="Nome" /></td></tr>
					<tr><td valign="top"><select name="YMP2"><option value="Centro-Oeste">Centro-Oeste</option><option value="Nordeste">Nordeste</option><option value="Norte">Norte</option><option value="Sudeste">Sudeste</option><option value="Sul">Sul</option></select></td></tr>
					<tr><td colspan="2">
						<a href="#" id="btn-lancamento"><?php echo get_tema_image_tag("botao-inscreva-sq.png") ?></a>
						<input style="display:none" type="submit" value="Submit" />&nbsp;
					</td></tr>
				</table>
			</form>
			<div><i>Fique tranquilo, também odiamos spam =)</i></div>
		</div>
		
		<div class="col-4 column full-width last mobile-hide">
			<div class="lancamento-sq-tablet">
				<?php echo get_tema_image_tag("tablet.png") ?>
			</div>
		</div>
	
	</div>
	<div style="clear: both"></div>
	
	<div class="lancamento-sq-rodape">
		<div class="col-8 column" style="margin-top: 15px; color: #fff">
			Esta é apenas a pré-inscrição para receber ANTECIPADAMENTE a oportunidade de ganhar 60% de desconto no sistema de questão. Disponível para os 200 primeiros inscritos.<br>
			Caso desista de participar, não tem problema! Não iremos lhe cobrar nada por inscrever-se agora :D
		</div>
		<div class="col-3 column last mobile-hide">
			<?php echo get_tema_image_tag("logo-negativo.png") ?>
		</div>
		<div style="clear: both"></div>
	</div>
	<div class="lancamento-sq-footer">
		2016. Exponencial Concursos. Todos os direitos reservados.
	</div>
	
	<script>
	$(function() {
		$('#btn-lancamento').click(function(e) {
			e.preventDefault();

			$('#form-lancamento').submit();
		});
	});
	</script>
	
</body>
</html>
