<?php
include_once ($_SERVER ['DOCUMENT_ROOT'] . '/wp-config.php');
get_header ();
$topic_id = $_GET ['id'];
$action_url = "/wp-content/themes/academy/bbpress/helper/forum_helper.php?act=editar_topic&id=" . $topic_id;
?>
<div id="bbpress-forums" class="container pt-1">

<?php bbp_topic_tag_list( $topic_id ); ?>

	<?php bbp_single_topic_description( array( 'topic_id' => $topic_id ) ); ?>


<div id="new-topic-<?php echo $topic_id; ?>" class="bbp-topic-form">

		<form id="new-post" name="new-post" method="post"
			action="<?php echo $action_url; ?>">



			<div class="text-editando b_main_heading"><h1>
<?php printf( 'Editando &ldquo;%s&rdquo;', bbp_get_topic_title($topic_id) ); ?></h1>
</div>

			<fieldset class="bbp-form">
				<div class="bbp-template-notice">
					<p>A sua conta tem a capacidade de publicar conteúdo HTML sem
						restrições.</p>
				</div>
				<div>
					<p>
						<label for="bbp_topic_title"><?php printf( 'Título do Tópico (Comprimento máximo %d):', bbp_get_title_max_length() ); ?></label><br />
						<input class="form-control" type="text" id="bbp_topic_title"
							value="<?php echo get_post_field( 'post_title', $topic_id ); ?>"
							tabindex="<?php bbp_tab_index(); ?>" size="40"
							name="bbp_topic_title"
							maxlength="<?php bbp_title_max_length(); ?>" />
					</p>

					<div class="bbp-the-content-wrapper">
						<div id="wp-bbp_topic_content-wrap"
							class="wp-core-ui wp-editor-wrap tmce-active">
							<textarea class="form-control bbp-the-content wp-editor-area" rows="12"
								tabindex="102" cols="40" name="bbp_topic_content"
								id="bbp_topic_content"><?php echo get_post_field( 'post_content', $topic_id );?></textarea>
						</div>
					</div>
				</div>

				<div class="bbp-submit-wrapper">
					<button type="submit" tabindex="110" id="bbp_topic_submit"
						name="bbp_topic_submit" class="btn u-btn-blue">Enviar</button>
				</div>
			</fieldset>
		</form>
	</div>
</div>
<?php get_footer();?>