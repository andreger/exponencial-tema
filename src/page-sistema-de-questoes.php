<?php get_header(); 
$secao_barra_clara_user = 'secao-barra-clara';
    if($usuario_id = get_current_user_id()) {
      if(exibe_assinatura_bar($usuario_id)) {
        $secao_barra_clara_user = 'secao-barra-clara-ass-bar';
      }
   }
?>
<div class="pt-3 pt-md-5 sistema-de-questoes">
	<div class="d-block d-md-none bg-blue p-3">
			<div class="row ml-0 ml-lg-4 text-center">
			<div class="col-12 col-md-2 mb-1">
				<img src="<?php echo get_tema_image_url('sq-mini.png')?>">
			</div>
				<div class="col-12">
				<a class="t-d-none font-weight-bold text-white text-uppercase" href="#como-utilizar">Como utilizar</a></div>			
				<div class="col-12">
				<a class="t-d-none font-weight-bold text-white text-uppercase" href="#evolucao">Acompanhe sua evolução</a></div>
				<div class="col-12">
				<a class="t-d-none font-weight-bold text-white text-uppercase" class="botao-verde" href="#assinatura">Assinatura</a></div>
				<div class="col-12">
				<a class="t-d-none font-weight-bold text-white text-uppercase" href="#depoimentos">Depoimentos</a></div>
			</div>
		</div>
	<div class="text-center text-md-left pt-4 mt-0 mt-md-4 mb-4 text-blue container">
		<h1>Sistema de Questões</h1>
	</div>
	<div class="row-fluid">
		<div class="s-b-c d-none d-md-block border-top border-white bg-blue p-3 <?= $secao_barra_clara_user ?> col-12">
			<div class="container row ml-auto mr-auto">
			<div class="col-12 p-0 text-uppercase">
				<img class="ml-2 mr-2" src="<?php echo get_tema_image_url('sq-mini.png')?>">
				<a class="font-weight-bold text-white ml-2 ml-lg-3 mr-3" href="#como-utilizar">Como utilizar</a>	
				
				<a class="font-weight-bold text-white ml-2 ml-lg-3 mr-3" href="#evolucao">Acompanhe sua evolução</a>				
				<a class="font-weight-bold text-white ml-2 ml-lg-3 mr-3" class="botao-verde" href="#assinatura">Assinatura</a>				
				<a class="font-weight-bold text-white ml-2 ml-lg-3 mr-3" href="#depoimentos">Depoimentos</a>			
			</div>	
			</div>		
	</div>
	
	
		<div class="secao-barra-escura"><?php putRevSlider('capa-sq') ?></div>
		<!-- <img src="<?php //echo get_tema_image_url('sq-capa.jpg')?>"> -->
	
	<a class="anchor" id="como-utilizar"></a>
	<div class="col-12 text-center font-pop mb-3 mt-4">
    Como Utilizar
	</div>	


	
<div class="row ml-auto mr-auto p-5 font-10">			
	<div class="col-12 col-md-3 text-center">
		<img src="<?php echo get_tema_image_url('sq-login.png')?>">
			<p class="mt-0 mb-4 mb-md-0 mt-md-3">Faça login e acesse MINHA CONTA.
	</div>			
	<div class="col-12 col-md-3 text-center">
		<img src="<?php echo get_tema_image_url('sq-resolva.png')?>">
			 <p class="mt-0 mb-4 mb-md-0 mt-md-3">Clique em 'Sistema de Questões'. Resolva as questões e veja os comentários dos professores.
	</div>			
	<div class="col-12 col-md-3 text-center">
		<img src="<?php echo get_tema_image_url('sq-como-evolucao.png')?>">
			<p class="mt-0 mb-4 mb-md-0 mt-md-4 pt-1">Identifique seus pontos fracos e acompanhe sua evolução.
	</div>			
	<div class="col-12 col-md-3 text-center">
		<img src="<?php echo get_tema_image_url('sq-rapido.png')?>">
			<p class="mt-0 mb-4 mb-md-0 mt-md-3">Caminho certo e rápido para sua aprovação.
	</div>			
</div>
	
	<!-- <div class="row text-align-center" >
		<img src="<?php echo get_tema_image_url('.png')?>">
	</div> -->
<a class="anchor" id="evolucao"></a>
<div class="container-fluid bg-gray pt-4 pb-5">
	<div class="col-12 text-center font-pop mb-3">
    Acompanhe sua evolução
	</div>	
<div class="container">	
<div class="row ml-auto mr-auto">
	<div class="col-12 col-md-6 col-lg-3 mb-3 mb-lg-0 text-center font-weight-bold text-uppercase">
		<img src="<?php echo get_tema_image_url('g1.png')?>">
		<p class="mt-3">Desempenho por disciplina</p>
	</div>
	<div class="col-12 col-md-6 col-lg-3 mb-3 mb-lg-0 text-center font-weight-bold text-uppercase">
		<img src="<?php echo get_tema_image_url('g2.png')?>">
			<p class="mt-3">Desempenho por concurso</p>
	</div>	
	<div class="col-12 col-md-6 col-lg-3 mb-3 mb-sm-0 text-center font-weight-bold text-uppercase">
		<img src="<?php echo get_tema_image_url('g3.png')?>">
			<p class="mt-3">Aproveitamento Global</p>
	</div>			
	<div class="col-12 col-md-6 col-lg-3 text-center font-weight-bold text-uppercase">
		<img src="<?php echo get_tema_image_url('g4.png')?>">
			<p class="mt-3">Resultado</p>
	</div>	
</div>
</div>
</div>
	<a class="anchor" id="assinatura"></a>
	<div class="col-12 text-center font-pop mb-3 mt-4">
    Vantagem de ser assinante
	</div>	

	
	
	<div class="col-12 mb-3">
		<?php include get_pagina_url('vantagens_sq.php'); ?>
	</div>
	
	<a class="anchor" id="plano"></a>
	<div class="container mb-4">		
		<div>
			<?php 
				$mensal = get_produto_assinatura_mensal();
				$trimestral = get_produto_assinatura_trimestral();
				$semestral = get_produto_assinatura_semestral();
				$anual = get_produto_assinatura_anual();
			?>

		<div class="row p-4 ml-auto mr-auto">
			<div class="text-center col-12 col-md-3 ">
				<div>
					<img class="img-fluid" src="<?php echo get_tema_image_url('sq-mensal.png')?>">
					<div class="sq-preco"><span class="tachado">R$ <?= moeda($mensal->regular_price, false)?></span><span> por </span> <small>R$</small> <?= moeda($mensal->price, false)?></div>
				</div>
				<div class="mt-4 mt-md-custom-1 mt-lg-5 mt-xl-4 mb-2 mb-md-custom-1 mb-lg-4 font-md-9 font-10">Acesso a todos os<br>comentários.</div>
				<div class="mb-3 mb-md-0">
					<a href="<?= get_produto_compra_url($mensal, '/sistema-de-questoes') ?>">
						<img class="img-fluid" src="<?php echo get_tema_image_url('sq-assinar-plano-1.png')?>">
					</a>
				</div>
			</div>
		
			<div class="text-center col-12 col-md-3">
				<div>
					<img class="img-fluid" src="<?php echo get_tema_image_url('sq-trimestral.png')?>">
					<div class="sq-preco"><span class="tachado">R$ <?= moeda($trimestral->regular_price, false)?></span><span> por </span> <small>R$</small> <?= moeda($trimestral->price, false)?></div>
				</div>
				<div class="mt-4 mt-md-custom-2 mt-lg-5 mt-xl-4 mb-2 mb-md-4 font-md-9 font-10"><?= get_percentual_desconto_str($trimestral) ?><br>Acesso a todos os comentários.</div>
				<div class="mb-3 mb-md-0">
					<a href="<?= get_produto_compra_url($trimestral, '/sistema-de-questoes') ?>">
						<img class="img-fluid" src="<?php echo get_tema_image_url('sq-assinar-plano-2.png')?>">
					</a>
				</div>
			</div>
			
			<div class="text-center col-12 col-md-3">
				<div>
					<img class="img-fluid" src="<?php echo get_tema_image_url('sq-semestral.png')?>">
					<div class="sq-preco"><span class="tachado">R$ <?= moeda($semestral->regular_price, false)?></span><span> por </span> <small>R$</small> <?= moeda($semestral->price, false)?></div>
				</div>
				<div class="mt-3 mb-1 mb-lg-2 font-md-9 font-10"><?= get_percentual_desconto_str($semestral) ?><br>Acesso a todos os comentários.<br>Descontos exclusivos em simulados.</div>
				<div class="mb-3 mb-md-0">
					<a href="<?= get_produto_compra_url($semestral, '/sistema-de-questoes') ?>">
						<img class="img-fluid" src="<?php echo get_tema_image_url('sq-assinar-plano-3.png')?>">
					</a>
				</div>
			</div>
			
			<div class="text-center col-12 col-md-3">
				<div>
					<img class="img-fluid" src="<?php echo get_tema_image_url('sq-anual.png')?>">
					<div class="sq-preco"><span class="tachado">R$ <?= moeda($anual->regular_price, false)?></span><span> por </span> <small>R$</small> <?= moeda($anual->price, false)?></div>
				</div>
				<div class="mt-3 mb-1 mb-lg-2 font-md-9 font-10"><?= get_percentual_desconto_str($anual) ?><br>Acesso a todos os comentários.<br>Descontos exclusivos em simulados.</div>
				<div class="mb-3 mb-md-0">
					<a href="<?= get_produto_compra_url($anual, '/sistema-de-questoes') ?>">
						<img class="img-fluid" src="<?php echo get_tema_image_url('sq-assinar-plano-4.png')?>">
					</a>
				</div>
			</div>
			</div>
		</div>
	</div>

<a class="anchor" id="depoimentos"></a>
<div class="container-fluid bg-gray pt-4 pb-5 pl-0 pr-0">
	<div class="col-12 text-center font-pop mb-3">
    Depoimentos
	</div>	
	<div class=""><?php putRevSlider('depoimentos-sqs') ?></div>
</div>	
</div>
</div>
<?php get_footer() ?>