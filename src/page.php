<?php
get_header();

if (get_post_status() != 'publish') {
    redirecionar_erro_404();
}
else {
    the_post();
    the_content();
    get_footer();
}