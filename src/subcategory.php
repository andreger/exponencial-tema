<?php
// Template Name:Courses gallery subcategory
/*get_header();

KLoader::model("ProdutoModel");
KLoader::helper("ProdutoHelper");
KLoader::helper("UrlHelper");

global $wpdb;

$orderby=$_GET['o'];

$cat_name = get_query_var('cat_name');
$slug = get_query_var('nome');
$con_name = get_query_var('con_name');

$posts = null;

if(isset($cat_name) && $cat_name) {
	global $materia;
	$title = $materia->name;
	
	$url_base = UrlHelper::get_cursos_por_materia_especifica_url($cat_name);
	
	$filtro = isset($_GET['curso']) ? $_GET['curso'] : "";
	$filtro_param = "";
	if($filtro){
		$filtro_param = "curso=".$filtro."&";
	}

	// Montagem da paginação	
	$pg = isset($_GET['pg']) ? $_GET['pg'] : "1";
	$total = ProdutoModel::contar_por_materia($cat_name, $filtro);
	$pattern = $url_base."?{$filtro_param}pg=(:num)";
	$limit = LIMITE_POR_MATERIA;
	$paginator = new Paginator($total, $limit, $pg, $pattern);
	$offset = $pg ? ($pg - 1) * $limit : 0;

	$cursos = ProdutoModel::listar_por_materia($cat_name, $filtro, $offset, $limit);


	// $cursos = get_cursos_por_categoria($cat_name);
	include 'subcategory-materia.php';
}
elseif(isset($slug) && $slug) {
	global $professor;

	if($professor->col_oculto){
		echo "<script>window.location.href='/cursos-por-concurso'</script>";
	}else{

		$order_param = "";
		if($orderby) {
			
			$order_param = "o=".$orderby."&";

			switch ($orderby) {
				case 'menor-preco': {
					$order_by = 'pro_preco ASC';
					break;
				}
				case 'maior-preco': {
					$order_by = 'pro_preco DESC';
					break;
				}
				case 'mais-novos' : {
					$order_by = 'pro_publicado_em DESC';
					break;
				}
				case 'alfabetica' : {
					$order_by = 'post_title ASC';
					break;
				}
			}
		}
		else {
			$order_by = 'post_title ASC';
		}

		$url_base = UrlHelper::get_cursos_por_professor_especifico_url($slug);
		
		$filtro = isset($_GET['curso']) ? $_GET['curso'] : "";
		$filtro_param = "";
		if($filtro){
			$filtro_param = "curso=".$filtro."&";
		}

		// Montagem da paginação	
		$pg = isset($_GET['pg']) ? $_GET['pg'] : "1";
		$total = ProdutoModel::contar_por_professor($professor->user_id, $filtro);
		$pattern = $url_base."?{$order_param}{$filtro_param}pg=(:num)";
		$limit = LIMITE_POR_PROFESSOR;
		$paginator = new Paginator($total, $limit, $pg, $pattern);
		$offset = $pg ? ($pg - 1) * $limit : 0;


		$cursos = ProdutoModel::listar_por_professor($professor->user_id, $filtro, $order_by, $offset, $limit);
		
		// $cursos = get_cursos_por_autor($professor->user_id, -1, $order_by, $order, $meta_key);
		include 'subcategory-autor.php';
	}

}elseif(isset($con_name) && $con_name) {

	global $concurso;
	$title = $concurso->post_title;
	
	$url_base = UrlHelper::get_cursos_por_concurso_especifico_paginado_url($con_name);
	
	$filtro = isset($_GET['curso']) ? $_GET['curso'] : "";
	$filtro_param = "";
	if($filtro){
		$filtro_param = "curso=".$filtro."&";
	}

	// Montagem da paginação	
	$pg = isset($_GET['pg']) ? $_GET['pg'] : "1";
	$total = ProdutoModel::contar_por_concurso($con_name, null, null, null, null, null, null, $filtro);
	$pattern = $url_base."?{$filtro_param}pg=(:num)";
	$limit = LIMITE_POR_CONCURSO;
	$paginator = new Paginator($total, $limit, $pg, $pattern);
	$offset = $pg ? ($pg - 1) * $limit : 0;

	$cursos = ProdutoModel::listar_por_concurso($con_name, null, null, null, null, null, null, $filtro, $offset, $limit);

	include 'subcategory-concurso.php';

}

else {
	echo "<script>window.location.href='/cursos-por-concurso'</script>";
}*/
?>
<?php get_footer();?>