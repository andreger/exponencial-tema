<?php
date_default_timezone_set('America/Sao_Paulo');

tem_acesso(array(
	ADMINISTRADOR, COORDENADOR
), ACESSO_NEGADO);

ini_set('memory_limit', '-1');
set_time_limit(600);

function get_table($lines, $excel = false) {
	$table_head =
		"<table border='0' cellspacing='0' cellpadding='0' class='table table-bordered'>
			<tr class='font-10 bg-blue text-white text-center'>";

	if(isset($_POST['export-excel'])) {
		$table_head .= "<th>Área</th>";
	}

	$table_head .=
				"<th>Concurso</th>
				<th>Qtde Alunos</th>
				<th>Total Vendido</th>
				<th>Total Descontos</th>
				<th>Total Reembolsado</th>
				<th class='relatorio-destaque'>Total Pago</th>
				<th>% do Total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
				<th>Total PagSeguro</th>
				<th>Total Imposto</th>
				<th>Total Afiliados</th>
				<th>Total Líquido</th>
				<th>Total Líquido do Professor</th>
				<th>Valor Líquido pós Professor</th>
			</tr>";

	$soma_alunos = 0;
	$soma_valor_de_venda = 0;
	$soma_desconto = 0;
	$soma_valor_pago = 0;
	$soma_valor_imposto = 0;
	$soma_pag_seguro = 0;
	$soma_folha_dirigida = 0;
	$soma_liquido_venda = 0;
	$soma_lucro_professor = 0;
	$soma_liquido_pos_professor = 0;

	$total_pago = get_total_pago($lines);

	$i = 1;

	$table_body = '';

	/**
	 *@todo verificar escopo de variáveis aninhadas e com nomes semelhantes
	 */
	foreach ($lines as $key => $line) {
// 		$valor_pago = $line['total'] - $line['discount'] - $line['reembolso'];
		$valor_pago = $line['valor_pago'];
		$valor_imposto = $line['valor_imposto'];
		$pagseguro = $line['pagseguro'];
		$folha_dirigida = $line['folha_dirigida'];
		$liquido = $line['liquido'];
		$lucro_professor = $line['lucro_professor'];
		$liquido_pos_professor = $line['liquido_pos_professor'];
		$percentual = $valor_pago > 0 ? $valor_pago / $total_pago * 100 : 0;


		if($excel) {
			$link = $key;
		} else {
			$link = "<a href='#detalhe' onclick='event.preventDefault();toggle($i)'>$key</a>";
		}

		$table_body .= "<tr class='text-right'>";

		if(isset($_POST['export-excel'])) { // Área
			$c_cursos_ids = array();
			foreach ($line['cursos'] as $item) {
				array_push($c_cursos_ids, $item['curso_id']);
			}

			$c_areas = get_area($c_cursos_ids);

			$table_body .= "<td>{$c_areas}</td>";
		}

		$desconto = $line['discount'] != 0 ? format_moeda($line['discount']) : '';
		$reembolso = $line['reembolso'] != 0 ? format_moeda($line['reembolso']) : '';

		$table_body .=
				"<td>" . $link . "</td>
				<td>" . $line['qtde_alunos'] . "</td>
				<td>" . format_moeda($line['total']) . "</td>
				<td>" . $desconto . "</td>
				<td>" .$reembolso . "</td>
				<td class='relatorio-destaque'>" . format_moeda($valor_pago) . "</td>
				<td>" . porcentagem($percentual) . "</td>
				<td>" . format_moeda($pagseguro) . "</td>
				<td>" . format_moeda($valor_imposto) . "</td>
				<td>" . format_moeda($folha_dirigida) . "</td>
				<td>" . format_moeda($liquido) . "</td>
				<td>" . format_moeda($lucro_professor) . "</td>
				<td>" . format_moeda($liquido_pos_professor) . "</td>
			</tr>";

		$soma_valor_de_venda += $line['total'];
		$soma_desconto += $line['discount'];
		$soma_reembolso += $line['reembolso'];
		$soma_valor_pago += $valor_pago;
		$soma_valor_imposto += $valor_imposto;
		$soma_pag_seguro += $pagseguro;
		$soma_folha_dirigida += $folha_dirigida;
		$soma_liquido_venda += $liquido;
		$soma_lucro_professor += $lucro_professor;
		$soma_liquido_pos_professor += $liquido_pos_professor;

		foreach ($line['cursos'] as $nome => $curso) {
// 			$valor_pago = $curso['total'] - $curso['discount'] - $curso['reembolso'];
			$valor_pago = $curso['valor_pago'];
			$valor_imposto = $curso['valor_imposto'];
			$pagseguro = $curso['pagseguro'];
			$folha_dirigida = $curso['folha_dirigida'];
			$liquido = $curso['liquido'];
			$curso_lucro_professor = $curso['lucro_professor'];
			$curso_liquido_pos_professor = $curso['liquido_pos_professor'];

			$table_body .= "<tr class='tr-$i' style='display:none'>";

			if(isset($_POST['export-excel'])) {
				$table_body .= "<td>" . get_area($curso['curso_id']) . "</td>";
			}

			$curso_desconto = $curso['discount'] != 0 ? format_moeda($curso['discount']) : '';
			$curso_reembolso = $curso['reembolso'] != 0 ? format_moeda($curso['reembolso']) : '';

			$table_body .=
				"<td><span style='margin-left: 30px'>" . $nome . "</span></td>
				<td>" . $curso['qtde_alunos'] . "</td>
				<td>" . format_moeda($curso['total']) . "</td>
				<td>" . $curso_desconto . "</td>
				<td>" .$curso_reembolso . "</td>
				<td>" . format_moeda($valor_pago) . "</td>
				<td></td>
				<td>" . format_moeda($pagseguro) . "</td>
				<td>" . format_moeda($valor_imposto) . "</td>
				<td>" . format_moeda($folha_dirigida) . "</td>
				<td>" . format_moeda($liquido) . "</td>
				<td>" . format_moeda($curso_lucro_professor) . "</td>
				<td>" . format_moeda($curso_liquido_pos_professor) . "</td>
			</tr>";

			$soma_alunos += $curso['qtde_alunos'];
		}

		$i++;
	}

	// Adiciona linha somatório total
	$table_sumary = "<tr class='bg-secondary text-white font-11'>";

	if(isset($_POST['export-excel'])) { // Área
		$table_sumary .= "<th></th>";
	}

	$table_sumary .=
			"<th></th>
			<th>" . $soma_alunos . "</th>
			<th>" . format_moeda($soma_valor_de_venda) . "</th>
			<th>" . format_moeda($soma_desconto) . "</th>
			<th>" . format_moeda($soma_reembolso) . "</th>
			<th class='relatorio-destaque'>" . format_moeda($soma_valor_pago) . "</th>
			<th></th>
			<th>" . format_moeda($soma_pag_seguro) . "</th>
			<th>" . format_moeda($soma_valor_imposto) . "</th>
			<th>" . format_moeda($soma_folha_dirigida) . "</th>
			<th>" . format_moeda($soma_liquido_venda) . "</th>
			<th>" . format_moeda($soma_lucro_professor) . "</th>
			<th>" . format_moeda($soma_liquido_pos_professor) . "</th>
		</tr>";

	$table =  $table_head . $table_sumary . $table_body . "</table>";

	return $table;
}

$msg = 'Exibindo resultados entre ' . date('d/m/Y', strtotime(date('Y-m-1'))) . ' e ' . date('d/m/Y');

$end_date = date('d/m/Y');
$start_date = date('1/m/Y');

if(isset($_POST['submit'])) {

	if(!empty($_POST['start_date'])) {

		if(has_error_dates( $_POST['start_date'],  $_POST['end_date'])) {
			$msg = null;
		} else {
			$start_date = $_POST['start_date'];

			if(!empty($_POST['end_date'])) {
				$end_date = $_POST['end_date'];
				$msg = 'Exibindo resultados entre ' . $_POST['start_date'] . ' e ' . $_POST['end_date'];
			} else {
				$msg = 'Exibindo resultados entre ' . $_POST['start_date'] . ' e ' . date('d/m/Y');
			}

		}
	}
}

$data_i = date('d/m/Y', strtotime(str_replace('/','-',$start_date)));
$data_f = date('d/m/Y', strtotime(str_replace('/','-',$end_date)));
$ordem = isset($_POST['ordem']) ? $_POST['ordem'] : 3;

$lines = listar_vendas_agrupadas_por_concurso($start_date, $end_date, $ordem);

if(isset($_POST['export-excel'])) {
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="stats.xls"');

	$table_result = get_table($lines, true);
	$table_result = str_replace("<table", "<table style='border-collapse:collapse; border: 1px solid black;'", $table_result);
	$table_result = str_replace("<tr", "<tr style='border: 1px solid black;'", $table_result);
	$table_result = str_replace("<td", "<td style='border: 1px solid black;'", $table_result);

	$list = get_html_translation_table(HTML_ENTITIES);
	unset($list['"']);
	unset($list['<']);
	unset($list['>']);
	unset($list['&']);

	$search = array_keys($list);
	$values = array_values($list);
	$search = array_map('utf8_encode', $search);

	$out =  str_replace($search, $values, $table_result);

	echo "<html xmlns:x='urn:schemas-microsoft-com:office:excel'>
		<head>
			<meta http-equiv='Content-Type' content='text/html;charset=UTF-8' />
		    <!--[if gte mso 9]>
		    <xml>
		        <x:ExcelWorkbook>
		            <x:ExcelWorksheets>
		                <x:ExcelWorksheet>
		                    <x:Name>Sheet 1</x:Name>
		                    <x:WorksheetOptions>
		                        <x:Print>
		                            <x:ValidPrinterInfo/>
		                        </x:Print>
		                    </x:WorksheetOptions>
		                </x:ExcelWorksheet>
		            </x:ExcelWorksheets>
		        </x:ExcelWorkbook>
		    </xml>
		    <![endif]-->
		</head>

		<body>
		   " . $out . "
		</body></html>";

	exit;
} else {
	get_header();
}
?>
<div class="container-fluid">
<div class="pt-3 mb-5 col-12 text-center text-md-left text-blue">
		<h1>Relatório de vendas por concurso</h1>
	</div>
</div>
<div class="container pt-1 pt-md-2">
<form id="relatorio_form" action="/relatorio-de-vendas-por-concurso" method="post">
	<div class="row">
		<div class="col-1">Período:</div>
		<div class="col">
			<input type="text" class="form-control date-picker" id="start_date" name="start_date" value="<?= isset($_POST['start_date']) ? $_POST['start_date'] : '' ?>" />
		</div>
		<div class="text-center">até</div>
		<div class="col">
			<input type="text" class="form-control date-picker" id="end_date" name="end_date" value="<?= isset($_POST['end_date']) ? $_POST['end_date'] : '' ?>" />
		</div>
		<div class="col">
			<?= get_relatorio_ordenacao_combo($ordem, false) ?>
		</div>
		<div class="col">
			<input class="btn u-btn-blue" id="submit_form" type="submit" value="Filtrar Consulta" name="submit"/>
		</div>
	</div>
</form>

<div class="row mt-4">
	<span style="margin: 0 10px"><a id="mes_anterior" href="#mes">Mês Anterior</a></span> |
	<span style="margin: 0 10px"><a id="mes_passado" href="#mes">Mês Passado</a></span> |
	<span style="margin: 0 10px"><a id="mes_atual" href="#mes">Mês Atual</a></span>
</div>

<div class="row mt-3">
	<form action="/relatorio-de-vendas-por-concurso" method="post">
		<input class="btn u-btn-blue" type="hidden" name="export-excel" value="1">

		<?php if(isset($_POST['start_date'])) : ?>
		<input class="btn u-btn-blue" type="hidden" name="start_date" value="<?php echo $_POST['start_date'] ?> ">
		<?php endif; ?>

		<?php if(isset($_POST['end_date'])) : ?>
		<input class="btn u-btn-blue" type="hidden" name="end_date" value="<?php echo $_POST['end_date'] ?> ">
		<?php endif; ?>

		<input class="btn u-btn-blue" type="submit" value="Exportar para Excel" name="submit" />
	</form>
</div>

<div style="display:none">
	<form action="/relatorio-de-vendas" method="post" target="_blank">
		<input type="hidden" id="professor_id" name="professor_id" value="">
		<input type="hidden" id="start_date" name="start_date" value="<?= $data_i ?>">
		<input type="hidden" id="end_date" name="end_date" value="<?= $data_f ?>">
		<input type="submit" value="submit" name="submit" id="submit-detalhe" />
	</form>
</div>

<div class="row mt-3" >
<?php echo is_null($msg) ? "Período inválido" : $msg; ?>
</div>

<div class="row">
	<div class="mt-3 ml-auto mr-auto">
		<?php if(!is_null($msg)) echo get_table($lines, false) ?>
	</div>
</div>
</div><!-- #content -->
<div class="sectionlargegap"></div>
</div>
<?= get_date_picker(".date-picker") ?>
<script>
function mes_atual() {
	jQuery('#start_date').css('color','#fff');
	jQuery('#end_date').css('color','#fff');
	jQuery('#start_date').val('<?php echo date("d/m/Y", strtotime("first day of this month")) ?>');
	jQuery('#end_date').val('<?php echo date("d/m/Y", strtotime("last day of this month")) ?>');
	jQuery('#submit_form').click();
}

function mes_passado() {
	jQuery('#start_date').css('color','#fff');
	jQuery('#end_date').css('color','#fff');
	jQuery('#start_date').val('<?php echo date("01/m/Y", strtotime("first day of previous month")) ?>');
	jQuery('#end_date').val('<?php echo date("t/m/Y", strtotime("last day of previous month")) ?>');
	jQuery('#submit_form').click();
}

function mes_anterior() {
	jQuery('#start_date').css('color','#fff');
	jQuery('#end_date').css('color','#fff');
	jQuery('#start_date').val('<?php echo date("01/m/Y", strtotime("-2 months")) ?>');
	jQuery('#end_date').val('<?php echo date("t/m/Y", strtotime("-2 months")) ?>');
	jQuery('#submit_form').click();
}

function detalhar_professor($professor_id) {
	jQuery("#professor_id").val($professor_id);
	jQuery("#submit-detalhe").click();

}

function toggle($i) {
	jQuery(".tr-"+$i).toggle();
}

jQuery().ready(function() {

	jQuery('.datepicker').datepicker({
		dateFormat: "dd/mm/yy",
		language: "pt-BR"
	});

	jQuery("#mes_anterior").click(function() {
		mes_anterior();
	});

	jQuery("#mes_passado").click(function() {
		mes_passado();
	});

	jQuery("#mes_atual").click(function() {
		mes_atual();
	});

});
</script>
<?php get_footer(); ?>
