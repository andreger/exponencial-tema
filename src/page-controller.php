<?php
$qv_controller = stripslashes(get_query_var('controller'));
$qv_controller_params = get_query_var('controller_params');

$params = explode("/", $qv_controller_params);

$nomeController = ucfirst($qv_controller);
$controller = $qv_controller;

$method = array_shift($params);

$c = new $controller();

call_user_func_array([$c, $method], $params);