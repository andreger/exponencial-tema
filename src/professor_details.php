<?php 
// Template Name: Details of professors

/**
 * Página individual de cada professor
 * 
 * http://www.exponencialconcursos.com.br/professor/SLUG_DO_USUARIO
 * 
 * @todo Remover o template do WP
 */

get_header();

// carrega js
//carregar_owl_carousel();
carregar_remodal();

KLoader::model("BlogModel");
KLoader::model("ProdutoModel");

global $professor;

$questoes_comentadas = get_num_questoes_comentadas($professor->user_id);
$questoes_comentadas_str = $questoes_comentadas == 1 ? "1 Questão online comentada" : number_format($questoes_comentadas, 0, ',', '.') . " Questões online comentadas";

$produtos = ProdutoModel::listar_por_professor($professor->user_id, null, 'pro_publicado_em DESC', 0, PROFESSOR_MAX_CURSOS);
$artigos = BlogModel::listar_por_professor($professor->user_id, [TIPO_ARTIGO], 'post_date DESC', PROFESSOR_MAX_ARTIGOS);
$total_produtos = ProdutoModel::contar_por_professor($professor->user_id);
$total_artigos = BlogModel::contar_por_professor($professor->user_id, [TIPO_ARTIGO]);
if(!$professor || $professor->col_oculto == 1){
    header('Location: /professores');
    exit;
} 
?>


<div class="container pt-1 pt-md-5">
    <div class="text-center row d-block">
        <h1><?= $professor->display_name ?></h1>       
        <div class="font-sm-12 font-16 font-calibri font-weight-bold"><?= $professor->col_cargo ?></div>
    </div>

    <div class="row mt-3">
        <div class="col-12 col-lg-3 box-amarelo-light rounded-left">
            <div class="pt-3 box-amarelo-img"> 
            <?php if($professor->col_foto_url_200 !== get_site_url().'/wp-content/themes/academy/images/avatar.png')  : ?>              
                <img src="<?= $professor->col_foto_url_365 ?>">
            <?php endif; ?>
            </div>
        </div>        
         <div class="col-12 col-lg-6 box-amarelo-light rounded-right pt-3 pl-3 pl-lg-0">
            <p class="font-10 text-justify"><?= $professor->col_descricao ?></p>             
            <p class="box-img-social"><?= social_box($professor) ?></p>
        </div>
        <div class="col-12 col-lg-3">
            <div class="mt-3 mt-lg-0 box-azul font-weight-bold font-12 text-center"> 
            <img width="30" src="/wp-content/themes/academy/images/ico-meus-cursos.png">
            <a href="#meus-cursos">Meus Cursos</a>                 
            </div> 
            <div class="box-amarelo font-weight-bold mt-3 font-12 text-center"> 
                <img width="30" src="/wp-content/themes/academy/images/ico-meus-artigos.png">            
                <a href="#meus-artigos">Meus Artigos</a>
            </div> 
            <div class="box-verde font-weight-bold mt-3 font-12 text-center">  
                <div><?= get_tema_image_tag('ico-meus-comentarios.png'); ?></div>
                <p><?= $questoes_comentadas_str ?></p>
            </div>
        </div>
    </div>
<a class="anchor" id="meus-cursos"></a>
<div class="row">
    <div class="text-center text-lg-left barra-azul-v2 d-block mt-5 font-weight-bold font-12">
        <img width="30" src="/wp-content/themes/academy/images/ico-meus-cursos.png">
        <span>Meus Cursos</span>
    </div>
</div>
<div class="row ml-1 mr-1 ml-sm-0 mr-sm-0">    
   <?php if($produtos) : ?>
        <div id="owl-carousel-cursos" class="mt-3 owl-carousel owl-theme">
        <?php foreach ($produtos as $produto) : ?>
            <div>
                <?= curso_box($produto) ?>
            </div>
        <?php endforeach ?>
        
        <?php if($total_produtos > PROFESSOR_MAX_CURSOS) : ?>
        	<div>
       			<div class="border rounded p-2 box-cursos-professor text-center todos-produtos">
       				<a href="<?= UrlHelper::get_cursos_por_professor_especifico_url($professor->col_slug) ?>">Ver Todos</a>
       			</div>
        	</div>        
        <?php endif ?>
        </div>
    <?php else : ?>  
    	<div class="mt-3 col-12 font-12 font-weight-bold">
            O professor ainda não possui nenhum curso.
        </div> 
    <?php endif ?>
</div>
<a class="anchor" id="meus-artigos"></a>
<div class="row">
    <div class="text-center text-lg-left barra-amarela-v2 font-weight-bold font-12 mt-4">
        <img width="30" src="/wp-content/themes/academy/images/ico-meus-cursos.png">
        <span>Meus Artigos</span>
    </div>
</div>
<div class="row ml-1 mr-1 ml-sm-0 mr-sm-0">  
        <?php if($artigos) : ?>
    
            <div id="owl-carousel-artigos" class="mt-5 owl-carousel owl-theme">
            <?php foreach ($artigos as $artigo) : ?>
                <div>
                    <?= midia_box($artigo) ?>
                </div>
            <?php endforeach ?>
            
            <?php if($total_artigos > PROFESSOR_MAX_ARTIGOS) : ?>
            	<div>
           			<div class="border rounded p-2 box-artigos text-center todos-artigos">
           				<a href="<?= UrlHelper::get_artigos_por_professor_especifico_url($professor->col_slug) ?>">Ver Todos</a>
           			</div>
            	</div>        
            <?php endif ?>
        	</div>
        <?php else : ?>
        	<div class="mt-3 col-12 font-12 font-weight-bold">
                O professor ainda não publicou nenhum artigo.
            </div>  
        <?php endif ?>
</div></br>
<div class="remodal" data-remodal-id="modal">
    <button data-remodal-action="close" class="remodal-close"></button>    
    <h5>Enviar mensagem ao professor</h5>
    <?php if(is_usuario_logado()) : ?>
        <div class="contato form_outter">
            <div class="mensagem-panel" style="display:none">
                <span id="mensagem-span"></span>
                <button data-remodal-action="cancel" class="btn btn-danger">Fechar</button>
            </div>
            <form class="contato-professor-form" method="POST">
                <div style="text-align: left">
                    <input class="form-control" type="hidden" name="professor_id" value="<?= $professor->user_id ?>" />
                    <input class="form-control" type="text" id="assunto" name="assunto" placeholder="Assunto"/>
                    <textarea class="mt-2 mb-3 form-control" id="mensagem" name="mensagem" placeholder="Digite a mensagem..."></textarea>
                </div>
                <?= carregar_recaptcha() ?>
                <br>
                <div>
                    <button data-remodal-action="cancel" class="btn btn-danger">Cancelar</button>
                    <button data-remodal-action="" id="enviar" class="btn u-btn-primary">Enviar</button>
                    <span id="carregando" style="display:none"> <?= loading_img() ?> Enviando...</span>
                </div>
            </form>
        </div>
    
    <?php else : ?>

        <p>É necessário estar logado para enviar uma mensagem ao professor.</p>
        <p><a href="<?= login_url(get_autor_url($professor->user_id) . "#modal") ?>">Clique aqui</a> para realizar o login.</p>
        <br>
        <button data-remodal-action="cancel" class="btn btn-danger">Fechar</button>
    
    <?php endif ?>

</div>
</div>

<script src="/wp-content/themes/academy/js/jquery.validate.min.js"></script>
<script>
jQuery(document).ready(function(){
    jQuery("#owl-carousel-cursos").owlCarousel({
        margin: 20,
        nav: true,
        navText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
        loop: false,
        dots: false,
        responsive: {
            0:{
                items:1
            },
            640:{
                items:3
            },
            1024: {
                items:4
            }
        }
    });

    jQuery("#owl-carousel-artigos").owlCarousel({
        margin: 20,
        nav: true,
        navText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
        loop: false,
        dots: false,
        responsive: {
            0:{
                items:1
            },
            640:{
                items:2
            },
            1024: {
                items:3
            }
        }
    });

    jQuery('#enviar').click(function (e) {
        e.preventDefault();

        if(jQuery(".contato-professor-form").valid()) {
            jQuery('#carregando').show();
            jQuery.post('/wp-content/themes/academy/ajax/enviar_email_professor.php', jQuery('.contato-professor-form').serialize(), function(data) {
                jQuery('#mensagem-span').html(data);
                jQuery('.contato-professor-form').hide();
                jQuery('.mensagem-panel').show();
            });
        }
    });

    jQuery(".contato-professor-form").validate({
        rules: {
            assunto: {
                required: true,
            },
            mensagem: {
                required: true,
            }
        },
        messages: {
            assunto: {
                required: "Assunto é obrigatório",
            },
            mensagem: {
                required: "Mensagem é obrigatória",
            }
        },
        errorPlacement: function(error, element) {
            error.insertAfter(element);
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

    jQuery("#professor-social-email-link").click(function() {
        jQuery('#assunto').val("");
        jQuery("#mensagem").val("");
        jQuery('#carregando').hide();
        jQuery(".mensagem-panel").hide();
        jQuery(".contato-professor-form").show();
    });
});
</script>

<?php get_footer(); ?>