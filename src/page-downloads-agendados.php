<?php 
get_header();

KLoader::model("DownloadModel");
KLoader::helper("DownloadHelper");

delete_option(DownloadHelper::get_notificacao_id(get_current_user_id()));

$downloads = DownloadModel::listar(get_current_user_id());
?>

<div class="container-fluid pt-1 pt-md-5">
	
	<div class="mb-5 col-12 text-center">
		<h1>Downloads Agendados</h1>
	</div>
    
    <div class="container">	
    	<div class="text-center mb-5">
    		<strong>Os downloads agendados ficam armazenados no sistema por 24 horas.</strong>
    	</div>
		<?php if($downloads) : ?>
			<table border="0" cellspacing="0" cellpadding="0" class="table mt-3 mb-5">
				<tr class="text-blue font-14">
					<th style="border: 0">Data</th>
					<th style="border: 0">Item</th>
					<th style="border: 0">Status</th>
					<th style="border: 0"></th>
				</tr>
				
				<?php foreach ($downloads as $item) : ?>
				<tr>
					<td class="border-box-white bg-box-azul-light d-none d-lg-table-cell font-14" style="vertical-align: middle"><?= converter_para_ddmmyyyy_HHiiss($item->dow_data) ?></td>
					<td class="border-box-white bg-box-azul-light d-none d-lg-table-cell font-14" style="vertical-align: middle">
						<?= DownloadHelper::get_relatorio_nome($item->dow_id)?>
					</td>
					<td class="text-center box-w-2 border-box-white bg-box-azul-light font-14 <?= $item->dow_status == DOWNLOAD_STATUS_CONCLUIDO ? "text-success" :  ""; ?>" style="vertical-align: middle">
						<?= DownloadHelper::get_status_nome($item->dow_status)?>
					</td>
					<td class="d-none d-lg-block border-box-white" style="padding: 4px 0.75rem">
						<?php if($item->dow_status == DOWNLOAD_STATUS_CONCLUIDO) : ?>
							<a class="btn u-btn-darkblue font-12 font-arial font-weight-bold" href="<?= $item->dow_arquivo ?>"><i class="fa fa-download"></i> Download</a>
						<?php endif; ?>
					</td>
				</tr>
				<?php endforeach; ?>
			</table>
		<?php else : ?>
			<div class="text-center mb-5">Não há downloads agendados para você.</div>
		<?php endif; ?>
	</div>
</div>

<div class="sectionlargegap"></div>

<?php get_footer() ?>