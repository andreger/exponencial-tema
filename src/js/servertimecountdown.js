(function() {
	'use strict'
	
	$.fn.servertimecountdown = function(args) {

		$.get("/wp-content/themes/academy/ajax/servertimecountdown.php", {date: args.date}, function(data) {
			var data = parseInt(data, 10);
			setInterval(function() {
				var d = Math.floor(data / 86400),
					h = Math.floor((data % 86400 ) / 3600),
					m = Math.floor(((data % 86400 ) % 3600 ) / 60),
					s = (( data % 86400 ) % 3600 ) % 60,
					dt = d == 1 ? "dia" : "dias",
					ht = h == 1 ? "hora" : "horas",
					mt = m == 1 ? "minuto" : "minutos",
					st = s == 1 ? "segundo" : "segundos";
				
				data--;

				if (d < 0) {
					d = "00";
				}
				else if(d < 10) {
					d = "0" + d;
				}

				if (h < 0) {
					h = "00";
				}
				else if (h < 10) {
					h = "0" + h;
				}

				if (m < 0) {
					m = "00";
				}
				else if (m < 10) {
					m = "0" + m;
				}

				if (s < 0) {
					s = "00";
				}
				else if(s < 10) {
					s = "0" + s;
				}

				$("#countdown .days").html(d);
				$("#countdown .days_text").html(dt);
				$("#countdown .hours").html(h);
				$("#countdown .hours_text").html(ht);
				$("#countdown .minutes").html(m);
				$("#countdown .minutes_text").html(mt);
				$("#countdown .seconds").html(s);
				$("#countdown .seconds_text").html(st);
				
			}, 1000);
		});
	}
})();