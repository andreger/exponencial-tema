jQuery().ready(function() {
	jQuery("#search").change( function () {	
		var filter = jQuery(this).val(); // get the value of the input, which we filter on
			console.log(filter);
		if (filter) {
			jQuery(".lista-professores").find(".pr_1text:not(:Contains(" + filter + "))").closest(".hide-prof").hide();
			jQuery(".lista-professores").find(".pr_1text:Contains(" + filter + ")").closest(".hide-prof").show();
		} else {
			jQuery(".lista-professores").find("div").show();
		}
	}).keyup( function () {
		jQuery(this).change();
	}).keydown (function(e) {
		if(e.keyCode == 13) {
			return false;
		}
	});

	// Case-Insensitive Contains
	jQuery.expr[':'].Contains = function(a,i,m){
	    return (a.textContent || a.innerText || "").toUpperCase().indexOf(m[3].toUpperCase())>=0;
	};
});