jQuery().ready(function() {
	jQuery("#search").change( function () {
		var filter = jQuery(this).val(); // get the value of the input, which we filter on

		if (filter) {
			jQuery(".list").find(".nome_prof:not(:Contains(" + filter + "))").closest(".mt-4").hide();
			jQuery(".list").find(".nome_prof:Contains(" + filter + ")").closest(".mt-4").show();
		} else {
			jQuery(".list").find("div").show();
		}
	}).keyup( function () {
		// fire the above change event after every letter
		jQuery(this).change();
	}).keydown (function(e) {
		if(e.keyCode == 13) {
			return false;
		}
	});
	
	jQuery("#search-cursos").change( function () {
		var filter = jQuery(this).val(); // get the value of the input, which we filter on
		
		if (filter) {
			jQuery(".col-hide").find(".concurso-curso-tit:not(:Contains(" + filter + "))").closest(".col-hide").hide();
			jQuery(".col-hide").find(".concurso-curso-tit:Contains(" + filter + ")").closest(".col-hide").show();
		console.log(filter)
		} else {
			jQuery(".col-hide").closest(".col-hide").show();
		}
	}).keyup( function () {
		jQuery(this).change();
	}).keydown (function(e) {
		if(e.keyCode == 13) {
			return false;
		}
	});

	// Case-Insensitive Contains
	jQuery.expr[':'].Contains = function(a,i,m){
	    return (a.textContent || a.innerText || "").toUpperCase().indexOf(m[3].toUpperCase())>=0;
	};
});