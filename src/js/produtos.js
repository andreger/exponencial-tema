var Premium = function () {
	
	var tr_num = 0;
	
	function init_tipo_listeners() {
		jQuery(document).on('change', '.premium-tipo', function() {
			var tipo = jQuery(this).val(),
				operador = jQuery(this).closest("tr").find(".premium-operador", 0),
				valores_produtos = jQuery(this).closest("tr").find(".premium-valores-produtos", 0),
				valores_categorias = jQuery(this).closest("tr").find(".premium-valores-categorias", 0);
				
			operador.hide();
			valores_produtos.hide();
			valores_categorias.hide();
			
			if(tipo == 1) {
				valores_categorias.find('select', 0).val(null).trigger('change');
				valores_produtos.find('select', 0).val(null).trigger('change');
			}
			
			if(tipo == 2) {
				operador.show();
				valores_produtos.show();
				valores_categorias.find('select', 0).val(null).trigger('change');
			}
			
			if(tipo == 3) {
				operador.show();
				valores_categorias.show();
				valores_produtos.find('select', 0).val(null).trigger('change');
			}
		});
	}
	
	function init_valores_listeners() {
		jQuery(".premium-valores-produtos select").select2();
		jQuery(".premium-valores-produtos select").select2();
	}
	
	function init_and_listeners() {
		jQuery(document).on('click', '.premium-and', function() {
			clonar_tr(jQuery(this).closest("tr"));
		});	
	}
	
	function init_delete_listeners() {
		jQuery(document).on('click', '.premium-delete', function() {
			
			if(jQuery('.premium-delete').length > 1) {
				var atual_tr = jQuery(this).closest("tr"), 
					grupo_num = atual_tr.find(".premium-grupo", 0).val();
				
				atual_tr.remove();
				
				if(jQuery(".premium-grupo[value=" + grupo_num + "]").length == 0) {
					jQuery(".tr-or-" + grupo_num).remove();
				}
			}
		});
	}
	
	function init_or_listeners() {
		jQuery(document).on('click', '.premium-or', function() {
			var ultimo_tr = jQuery('.premium-tipo:last').closest('tr'),
				grupo_num = parseInt(ultimo_tr.find('.premium-grupo', 0).val(), 10) + 1,
				tr_or = "<tr class='tr-or-"+ grupo_num +"'><td colspan='4'><strong>Ou</strong></td></tr>";
			
			clonar_tr(ultimo_tr);
			
			var novo_tr = jQuery('.premium-tipo:last').closest('tr');
					
			novo_tr.find('.premium-grupo', 0).val(grupo_num);
			novo_tr.before(tr_or);
		});	
	}
	
	function init_names() {
		jQuery(".tr-and").each(function () {
			set_names(jQuery(this));
		});
	}
	
	function init_premium_checkbox_listeners() {
		jQuery("#produto_premium").click(function () {
			exibir_esconder_configuracoes();	
		});
	}
	
	function init_capa_listeners() {
		jQuery("#produto_incluir_capa").click(function () {		
			atualizar_campos_capa();
		});
	}
	
	function init_import_listeners() {
		jQuery("#importar-cronograma-btn").click(function (e) {	
			e.preventDefault(); 
			
			if(jQuery("#save-post").is(":visible")) {
				jQuery("#save-post").click();
			}
			else {
				jQuery("#publish").click();
			}
		});
		
		jQuery("#importar-cronograma-cancel-btn").click(function (e) {
			e.preventDefault();
			jQuery("#importar-cronograma-div").hide();
		});
		
		jQuery("#importar-cronograma-open-btn").click(function (e) {
			e.preventDefault();
			jQuery("#importar-cronograma-div").toggle();
		});
	}
	
	function atualizar_campos_capa() {
		if(jQuery("#produto_incluir_capa").is(":checked")) {
			jQuery(".produto_capa_texto_concurso_field").show();
			jQuery(".produto_capa_texto_disciplina_field").show();
		}
		else {
			jQuery(".produto_capa_texto_concurso_field").hide();
			jQuery(".produto_capa_texto_disciplina_field").hide();
			jQuery("#produto_capa_texto_concurso").val("");
			jQuery("#produto_capa_texto_disciplina").val("");
		}
	}
	
	
	function exibir_esconder_configuracoes() {
		if(jQuery("#produto_premium").is(':checked')) {
			jQuery("#configuracoes-premium").show();
		}
		else {
			jQuery("#configuracoes-premium").hide();
		}
	}
	
	function clonar_tr(tr_atual) {
		tr_novo = tr_atual.clone();
	
		tr_atual.after(tr_novo);
		
		tr_novo.find(".premium-valores-produtos select", 0).removeClass('select2-hidden-accessible enhanced');
		tr_novo.find(".premium-valores-produtos span").remove();
		jQuery(".premium-valores-produtos select", 0).select2();
		
		tr_novo.find(".premium-valores-categorias select", 0).removeClass('select2-hidden-accessible enhanced');
		tr_novo.find(".premium-valores-categorias span").remove();
		jQuery(".premium-valores-categorias select", 0).select2();
		
		set_names(tr_novo);
		
		tr_novo.find('.premium-valores-produtos select', 0).val(null).trigger('change');
		tr_novo.find('.premium-valores-categorias select', 0).val(null).trigger('change');
		tr_novo.find(".premium-tipo", 0).change();
	}
	
	function set_names(elem) {
		elem.find(".premium-grupo", 0).prop("name", "produto_premium_regras[" + tr_num + "][grupo]");
		elem.find(".premium-tipo", 0).prop("name", "produto_premium_regras[" + tr_num + "][tipo]");
		elem.find(".premium-operador", 0).prop("name", "produto_premium_regras[" + tr_num + "][operador]");
		elem.find(".premium-valores-produtos select", 0).prop("name", "produto_premium_regras[" + tr_num + "][valores_produtos][]");
		elem.find(".premium-valores-categorias select", 0).prop("name", "produto_premium_regras[" + tr_num + "][valores_categorias][]");
		
		tr_num++;
	}
	
	return {
		
		init: function () {
			init_tipo_listeners();
			init_and_listeners();
			init_delete_listeners();
			init_or_listeners();
			init_names();
			init_premium_checkbox_listeners();
			init_capa_listeners();
			init_import_listeners();
			
			exibir_esconder_configuracoes();
			atualizar_campos_capa();
			jQuery(".premium-tipo").change();
		}
	
	};
}();

function preencher_hidden()
{
	var a = [];
	var v = [];
	var c = [];
	var csq = [];
	var m = [];
	var r = [];

	jQuery('.adm-cronograma-aula').each(function (i, aula) {
		var b = [];
		var v2 = []
		var c2 = [];
		var csq2 = [];
		var m2 = [];
		var r2 = [];

		jQuery(aula).find('input[name="_wc_file_urls_expo[]"]').each(function (j, arquivo) {
			b.push(arquivo.value);	
		});
		
		a.push(b);

		jQuery(aula).find('input[name="vimeo_urls[]"]').each(function (j, arquivo) {
			v2.push(arquivo.value);	
		});

		v.push(v2);

		jQuery(aula).find('input[name="caderno_ids[]"]').each(function (j, arquivo) {
			c2.push(arquivo.value);	
		});

		c.push(c2);

		jQuery(aula).find('input[name="caderno_sq_urls[]"]').each(function (j, arquivo) {
			csq2.push(arquivo.value);
		});

		csq.push(csq2);

		jQuery(aula).find('input[name="mapa_urls[]"]').each(function (j, arquivo) {
			m2.push(arquivo.value);	
		});

		m.push(m2);

		jQuery(aula).find('input[name="resumo_urls[]"]').each(function (j, arquivo) {
			r2.push(arquivo.value);	
		});

		r.push(r2);
	});

	jQuery('#aulas_arquivo').val(JSON.stringify(a));
	jQuery('#aulas_vimeo').val(JSON.stringify(v));
	jQuery('#aulas_caderno').val(JSON.stringify(c));
	jQuery('#aulas_caderno_sq').val(JSON.stringify(csq));
	jQuery('#aulas_mapa').val(JSON.stringify(m));
	jQuery('#aulas_resumo').val(JSON.stringify(r));
}

function listar_cadernos_professores(elem) 
{
	jQuery('#caderno-modal-content').html("<div>Carregando...</div>");

	var d = new Date();
	var n = d.getTime();

	if(elem !== undefined) {
		jQuery(elem).attr('id', n);
		jQuery('#caderno-modal-content').attr('data-elemento', n);
	}

	jQuery.get('/wp-content/themes/academy/ajax/caderno_listar_professores.php', function(data) {
		jQuery('#caderno-modal-content').html(data);
		jQuery('#TB_ajaxContent').css('width', 'auto');
	});
}

function listar_cadernos_professor(id, event, pagina) 
{
	if(event){
		event.preventDefault();
	}
	
	jQuery('#caderno-modal-content').html("<div>Carregando...</div>");

	var queryStr = '';
	if(pagina){
		queryStr = '&pagina='+pagina;
	}

	jQuery.get('/wp-content/themes/academy/ajax/caderno_listar_cadernos_professor.php?id=' + id + queryStr, function(data) {
		jQuery('#caderno-modal-content').html(data);
		jQuery('#TB_ajaxContent').css('width', 'auto');
	});
}

function listar_canais_vimeo(elem) 
{
	listar_canais_vimeo(elem, null);
}
function listar_canais_vimeo(elem, pagina) 
{
	jQuery('#vimeo-modal-content').html("<div>Carregando...</div>");

	var d = new Date();
	var n = d.getTime();

	if(elem !== undefined) {
		jQuery(elem).attr('id', n);
		jQuery('#vimeo-modal-content').attr('data-elemento', n);
	}

	var queryStr = '';
	if(pagina){
		queryStr = '?pagina='+pagina;
	}

	jQuery.get('/wp-content/themes/academy/ajax/vimeo_listar_canais.php'+queryStr, function(data) {
		// console.log(data);

		jQuery('#vimeo-modal-content').html(data);
	});
}

function listar_videos_vimeo(id, event, pagina) 
{
	if(event){
		event.preventDefault();
	}
	
	jQuery('#vimeo-modal-content').html("<div>Carregando...</div>");

	var queryStr = '';
	if(pagina){
		queryStr = '&pagina='+pagina;
	}

	jQuery.get('/wp-content/themes/academy/ajax/vimeo_listar_videos.php?id=' + id + queryStr, function(data) {
		// console.log(data);

		jQuery('#vimeo-modal-content').html(data);
	});
}

function buscar_videos_vimeo(q, event) 
{
	event.preventDefault();

	jQuery('#vimeo-modal-content').html("<div>Carregando...</div>");

	jQuery.get('/wp-content/themes/academy/ajax/vimeo_buscar_videos.php?q=' + q, function(data) {
		console.log(data);

		jQuery('#vimeo-modal-content').html(data);
	});
}

function adicionar_caderno(cad_id, nome, event){

	event.preventDefault();
	
	jQuery('#TB_closeWindowButton').click();

	var id = '#' + jQuery('#caderno-modal-content').attr('data-elemento');
	jQuery(id).closest('tr').find('.caderno_id input').val(cad_id);
	jQuery(id).closest('tr').find('.caderno_nome input').val(nome);

	preencher_hidden();
}

function adicionar_vimeo(link, event) 
{
	event.preventDefault();

	jQuery('#TB_closeWindowButton').click();

	var id = '#' + jQuery('#vimeo-modal-content').attr('data-elemento');
	jQuery(id).closest('tr').find('.file_url input').val(link);

	preencher_hidden();
}

function preencher_campos_default()
{
	if(jQuery('#sample-permalink').length == false) {
		jQuery('#_virtual').prop('checked', true);
		jQuery('#_downloadable').prop('checked', true);
		jQuery('#fb_pxl_checkbox').prop('checked', true);
		jQuery('#_sold_individually').prop('checked', true);
	}
}

function controlar_dias_liberacao_divs() 
{
	var dias_liberacao_divs = jQuery(".aulas_liberacao").closest(".form-field");
	
	if (jQuery("#liberar_gradualmente").prop("checked")) {
		dias_liberacao_divs.show();
	}
	else {
		dias_liberacao_divs.hide();
	}
}

function erro_validar_dias_liberacao()
{
	var tem_erro = false;
	jQuery('.aulas_liberacao').each(function () {
		var valor = jQuery(this).val();
		
		console.log(valor);
		
		if(valor != "" &&  Number.isInteger(parseInt(valor, 10)) == false) {
			tem_erro = true;
		}
	});
	
	if(tem_erro) {
		return true;
	}
	return false;
}

function validar_form(){

	var status = jQuery("#post_status").val();
	var acao = jQuery('#publish').prop("name");
	
	if(acao == "publish" || (acao == "save" && status == "publish")) {
		
		var numAutores = 0;
		var somaPercentuais = 0;
		var erroPercentualVazio = false;
		
		jQuery(".autor-div").each(function () {
			var cbbAutor = jQuery(this).find(".campo-autor").first();
			var cbbPercentual = jQuery(this).find(".campo-percentual").first();
			
			if (cbbAutor.val() != -1) {
				
				numAutores++;
				
				if(cbbPercentual.val() == "") {
					erroPercentualVazio = true;
				}
				
				var percentual = cbbPercentual.val().replace(",", ".");
				
				somaPercentuais += parseFloat(percentual);
			}
			
		});
		
		// validação não é necessária para pacotes e para produtos premiums	
		if(jQuery("#product-type").val() != "bundle" && jQuery("#produto_premium").prop("checked") == false) {

			if(erroPercentualVazio == true) {
				alert("ERRO: É necessário definir o percentual de vendas para todos os autores.");
				return false;
			}
			else if(numAutores == 0) {
				alert("ERRO: É necessário selecionar, pelo menos, 1 autor.");
				return false;
			}
			else if(somaPercentuais.toFixed(2) != 100.00) {
				alert("ERRO: A soma dos percentuais de vendas dos autores precisa ser igual a 100.\n\nO valor atual é: " 
						+ somaPercentuais.toFixed(2).replace(".", ","));
				return false;
			}
			
		}
		
	}
	
	if(erro_validar_dias_liberacao()) {
		alert("ERRO: Os campos 'Dias para Liberação Após Compra' devem conter apenas números inteiros.");
		return false;
	}
	
	preencher_hidden();

	return true;
}

jQuery(function() {
	preencher_hidden();
	preencher_campos_default();
	
	jQuery(document).on('click', '.caderno-voltar', function(e) {
		e.preventDefault();
		listar_cadernos_professores();
	});

	jQuery(document).on('click', '.vimeo-voltar', function(e) {
		e.preventDefault();
		listar_canais_vimeo();
	});

	jQuery.datepicker.setDefaults({
		dateFormat: 'dd/mm/yy',
	    dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
	    dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
	    dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
	    monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
	    monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
	    nextText: 'Próximo',
	    prevText: 'Anterior'
	});
	
	jQuery('.aulas_arquivo').prop('readOnly', true);
	jQuery('.campo_data').datepicker();
	
	jQuery(document).on('click', '.adicionar-aula', function(event) {
		event.preventDefault();
		
		var novo_id = "dp_id_" + jQuery('.adm-cronograma-aula').length;
		var appendDiv = jQuery('.adm-cronograma-aula:last').clone(); 
		appendDiv.find('input').val('');
//		appendDiv.insertAfter('.adm-cronograma-aula:last'); 
		appendDiv.insertAfter( jQuery(event.target).closest(".adm-cronograma-aula")/*.find(0)*/ ); 
		
		jQuery(appendDiv).find('.campo_data', 0)
			.removeClass('hasDatepicker')
			.removeData('datepicker')
			.unbind()
			.prop("id", novo_id)
			.datepicker();
	});

	jQuery('#publish').click(function() {
		return validar_form();
	});

	jQuery('#post').submit(function() {
		return validar_form();
	});

	jQuery('#save-post').click(function() {
		preencher_hidden();
	});

	jQuery('.nome_arquivo').change(function () {
		preencher_hidden();
	});

	jQuery(document).on("click", ".remover-aula", function () {
		if(confirm('Deseja remover a aula?')) {
			jQuery(this).parent().parent().parent().remove(); 
	
		}
		return false;		
	});
	
	jQuery("#liberar_gradualmente").click(function () {
		controlar_dias_liberacao_divs();
	});

	jQuery(document).on("change", ".caderno_nome_text", function(event) {
		var url_caderno = jQuery(event.target).val();
		var aux = url_caderno.split("/");
		aux = aux[ aux.length - 1 ].split("?");
		var id_caderno = '';
		if( Number.isInteger( parseInt( aux[ 0 ] ) ) ){
			id_caderno = aux[ 0 ];
		}
		
		jQuery(event.target).closest('tr').find('.caderno_id input').val(id_caderno);
		preencher_hidden();

	});

	jQuery(document).on("click", ".collapse-sessao.in", function(event) {
		var el = event.target;
		if(jQuery(el).is(".expand")){
			el = jQuery(event.target).parent();
		}
		jQuery(el).parent().find(".collapse-sessao-item", 0).hide();
		if(jQuery(el).parent().find(".collapse-sessao > .expand:first", 0).text().length > 1){
			jQuery(el).parent().find(".collapse-sessao > .expand:first", 0).text("+ Exibir arquivos");
		}else{
			jQuery(el).parent().find(".collapse-sessao > .expand:first", 0).text("+");
		}
		jQuery(el).removeClass("in");
		jQuery(el).addClass("out");

	});

	jQuery(document).on("click", ".collapse-sessao.out", function(event) {
		var el = event.target;
		if(jQuery(el).is(".expand")){
			el = jQuery(event.target).parent();
		}
		jQuery(el).parent().find(".collapse-sessao-item", 0).show();
		if(jQuery(el).parent().find(".collapse-sessao > .expand:first", 0).text().length > 1){
			jQuery(el).parent().find(".collapse-sessao > .expand:first", 0).text("- Recolher arquivos");
		}else{
			jQuery(el).parent().find(".collapse-sessao > .expand:first", 0).text("-");
		}
		jQuery(el).removeClass("out");
		jQuery(el).addClass("in");

	});

	jQuery(document).on("click", ".collapse-todos.recolher", function(event){
		jQuery(event.target).parent().find(".collapse-sessao-item").each(function(){
			jQuery(this).hide();
		});
		jQuery(event.target).parent().find(".collapse-sessao > .expand").each(function(){
			if(jQuery(this).text().length > 1){
				jQuery(this).text("+ Exibir arquivos");
			}else{
				jQuery(this).text("+");
			}
		});
		jQuery(event.target).parent().find(".collapse-sessao").each(function(){
			jQuery(this).removeClass("in");
			jQuery(this).addClass("out");
		});
	});
	
	jQuery(document).on("click", ".collapse-todos.exibir", function(event){
		jQuery(event.target).parent().find(".collapse-sessao-item").each(function(){
			jQuery(this).show();
		});
		jQuery(event.target).parent().find(".collapse-sessao > .expand").each(function(){
			if(jQuery(this).text().length > 1){
				jQuery(this).text("- Recolher arquivos");
			}else{
				jQuery(this).text("-");
			}
		});
		jQuery(event.target).parent().find(".collapse-sessao").each(function(){
			jQuery(this).removeClass("out");
			jQuery(this).addClass("in");
		});
	});

	jQuery("#adm-cronograma-aula-sort").sortable({
		items: '.adm-cronograma-aula'
	});

	controlar_dias_liberacao_divs();
	
	Premium.init();
});