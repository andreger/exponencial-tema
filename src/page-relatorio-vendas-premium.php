<?php
date_default_timezone_set('America/Sao_Paulo');

tem_acesso(array(
    ADMINISTRADOR
), ACESSO_NEGADO);

ini_set('memory_limit', '-1');
set_time_limit(600);

session_start();

KLoader::model("PremiumModel");
KLoader::model("ProfessorModel");
KLoader::helper("RelatorioVendasPremiumHelper");

if(!$_GET['inicio']) {
    $_GET['inicio'] = date("01/m/Y");
}

if(!$_GET['fim']) {
    $_GET['fim'] = date("t/m/Y");
}

// Monta dados para chamadas de modelo
$filtros = [
    "inicio" => converter_para_yyyymmdd($_GET['inicio']) . " 00:00:00",
    'fim' => converter_para_yyyymmdd($_GET['fim']) .  " 23:59:59",
];

$professores_combo = ProfessorModel::listar_por_tipo([COLABORADOR_PROFESSOR, COLABORADOR_PROFESSOR_CONSULTOR]);

$pedidos = PremiumModel::listar_pedidos_com_produto_premium($filtros);
$professores_pedido = [];
$total = [];
$professores = [];

foreach ($pedidos as $pedido) {
    $premium_info =  PremiumModel::get_premium_info($pedido->post_id, date("Ym", strtotime($pedido->ven_data)));

    $total[$pedido->post_id] = PremiumModel::get_valor_total_produtos($pedido->post_id, date("Ym", strtotime($pedido->ven_data)));

    $professores_pedido = PremiumModel::listar_premium_info_items_professores($premium_info->pri_id);


    foreach($professores_pedido as $professor_pedido) {
        $percentual = number_format($professor_pedido->pri_item_preco / $total[$pedido->post_id], 5) / 2;
        $item_valor = number_format($percentual * $pedido->ven_valor_venda, 2);
        
//         $info = [
//             'pedido_id' => $pedido->ven_order_id,
//             'pedido_data' => $pedido->ven_data,
//             'pedido_valor' => $pedido->ven_valor_venda,
//             'item_id' => $professor_pedido->pro_item_id,
//             'item_nome' => $professor_pedido->post_title,
//             'item_percentual' => $percentual,
//             'item_valor' => $item_valor,
//         ];
        
        $professores[$professor_pedido->display_name]['id'] = $professor_pedido->user_id;
//         $professores[$professor_pedido->display_name]['info'][] = $info;
        $professores[$professor_pedido->display_name]['total'] += $item_valor;
        
        if($professor_pedido->user_id == 99437) {
            log_debug("INFO", "Professor {$professor_pedido->display_name} {$professor_pedido->user_id} : {$item_valor}, curso: {$professor_pedido->pro_item_id}, pedido: {$pedido->ven_order_id}");
        }
    }
}


ksort($professores);

if(isset($_POST['exportar-xls'])) {
//     header('Content-Type: application/vnd.ms-excel');
//     header('Content-Disposition: attachment;filename="stats.xls"');
    
    echo RelatorioVendasPremiumHelper::exportar_xls($professores);
    exit;
} else {
    get_header();
}

?>


<div class="container-fluid pt-1 pt-md-4">
	<div class="mb-5 col-12 text-center text-md-left text-blue">
		<h1>Relatório de Vendas de Produtos Premium</h1>
	</div>
</div>
<div class="container">
	<form id="relatorio_form" action="/relatorio-vendas-premium" method="GET">
    	<div class="row">
    		<div class="col-4">
    			<strong>Data Início:</strong>
    			<input type="text" class="form-control date-picker" id="start_date" name="inicio" value="<?= $_GET['inicio'] ?>" />
    		</div>

    		<div class="col-4">
    			<strong>Data Fim:</strong>
    			<input type="text" class="form-control date-picker" id="end_date" name="fim" value="<?= $_GET['fim'] ?>" />
    		</div>	
    			
    		
    		<div class="col-12 mt-2">
    			<input class="btn u-btn-blue" id="submit_form" type="submit" value="Filtrar Consulta" name="filtrar"/> 
        		<input class="btn u-btn-blue" type="submit" value="Exportar para Excel" id="exportar-xls" name="exportar-xls" />
    		</div>
    	</div>
    </form>
    
	<div class="mt-4 mb-4">
	<?php if($professores) :?>	
		<div style="margin: 0 15px">
			<?= RelatorioVendasPremiumHelper::get_table($professores); ?>		
		</div>
	<?php endif ?>
	</div>
</div>


<?= get_date_picker(".date-picker") ?>

<?php get_footer(); ?>  
	