<?php
/**
 * Página de detalhamento do coaching
 * 
 * https://www.exponencialconcursos.com.br/detalhamento-do-coaching
 */

get_header();

// carrega owl carousel
//carregar_owl_carousel();


?>
	
	<div class="row mt-5">
		<div id="owl-demo" class="owl-carousel owl-theme" style="margin:20px 0">
			<?php foreach (get_consultores_array(FALSE) as $consultor) : ?>						
			<div class='item'>
				<div class='' style="text-align:center">
				    <div><a href='<?php echo get_autor_url($consultor['id']) ?>'><?= get_avatar($consultor['id'], 212); ?></a></div>
				    
					<div class="texto-azul detalhamento-coaching-titulo"><a href='<?php echo get_autor_url($consultor['id']) ?>'><?php echo str_replace(" ", "<br>", $consultor['nome_completo']) ?></a></div>
				</div>
			</div>				
			<?php endforeach; ?>
		</div>
	</div>
	

<?php get_footer() ?>