<?php
include_once ($_SERVER ['DOCUMENT_ROOT'] . '/wp-config.php');
get_header ();
$reply_id = $_GET ['id'];
$action_url = "/wp-content/themes/academy/bbpress/helper/forum_helper.php?act=editar_reply&id=" . $reply_id;
?>
<div class="main-content">
	<div id="bbpress-forums">

		<div id="new-reply-<?php bbp_reply_topic_id($reply_id); ?>"
			class="bbp-reply-form">

			<form id="new-post" name="new-post" method="post"
				action="<?php echo $action_url; ?>">
				<fieldset class="bbp-form">
					<legend><?php printf( 'Resposta para: %s', bbp_get_reply_topic_title($reply_id) ); ?></legend>
					<div class="bbp-template-notice">
						<p>A sua conta tem a capacidade de publicar conteúdo HTML sem
							restrições.</p>
					</div>

					<div>
						<div class="bbp-the-content-wrapper">
							<div id="wp-bbp_reply_content-wrap"
								class="wp-core-ui wp-editor-wrap tmce-active">

								<textarea class="bbp-the-content wp-editor-area" rows="12"
									tabindex="101" cols="40" name="bbp_reply_content"
									id="bbp_reply_content"><?php echo get_post_field( 'post_content', $reply_id );?></textarea>
							</div>
						</div>

					</div>
					<div class="bbp-submit-wrapper">

						<button type="submit" tabindex="106" id="bbp_reply_submit"
							name="bbp_reply_submit" class="btn u-btn-blue">Enviar</button>

					</div>
				</fieldset>

			</form>
		</div>
	</div>
</div>
<?php get_footer(); ?>