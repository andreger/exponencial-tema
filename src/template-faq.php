<?php
/*
Template Name: FAQ Page
*/

get_header(); ?>

<div class="main-content">
  <div class="sectionlargegap"></div>
  <div class="section_content">
    <div class="row padding">
      <h1>Perguntas Frequentes</h1>
      <div class="toggles-wrap accordion">

<?php echo do_shortcode('[widgets_on_pages id="FAQ"]'); ?>
         
      </div>
    </div>
  </div>
  <div class="sectionlargegap"></div>
</div>
<?php get_footer(); ?>
