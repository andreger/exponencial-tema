<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $product, $woocommerce_loop;

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) )
	$woocommerce_loop['loop'] = 0;

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) )
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 4 );

// Ensure visibility
// if ( ! $product || ! $product->is_visible() )
// 	return;
	
// if(is_produto_expirado($product->id))
// 	return;

if(!is_produto_publicado($product)){
	return;
}

// if(is_produto_assinatura($product->id))
// 	return;

// Increase loop count
// $woocommerce_loop['loop']++;

// Extra post classes
$classes = array();
if ( 0 == ( $woocommerce_loop['loop'] ) % $woocommerce_loop['columns'] || 1 == $woocommerce_loop['columns'] ) {
	$classes[] = 'first';
}
if ( 0 == $woocommerce_loop['loop'] % $woocommerce_loop['columns'] ) {
	// $classes[] = 'last';
}

if ( ! function_exists('contador_i')) {
	function contador_i() {
		static $i = 0;
	 	return ++$i;
	}
}

$i = contador_i();

// if($i > CURSOS_ONLINE_POR_PAGINA) {
// 	return;
// }
?>


<div class="cursos-online col-12">	
<div class="p-0 col-12 mb-4 border rounded">		
	<div class="c-o-h-p p-3">
		<div class="text-dark font-weight-bold font-15 pesquisa-titulo">
		<?php the_title();?>
		</div>
		<div class="mt-3 font-10 pesquisa-autores">
			<span class="font-weight-bold">Professores: </span>
			<?= get_autores_str($post); ?>
		</div>
	</div>	
	<div class="line-height-1-3 pt-2 pb-2 c-o-h-b ml-auto mr-auto row bg-gray">
		<div class="col-6 mt-auto mb-auto text-blue">
		<span class="font-price-del font-weight-bold font-15">
			<?php echo $product->get_price_html(); ?>
		</span>
		<?php if($product->get_price() > 0) : ?>
		<div class="pesquisa-parcelado d-block d-md-inline">
			<small> ou </small>
			12x
			<?php echo moeda($product->get_price() / 12) ?>
		
			<span class="pesquisa-parcelado d-block d-md-inline">sem juros</span>	
		</div>	
		<?php endif ?>
		</div>
		<div class="mt-auto mb-auto col-6 p-0 pr-2 text-right">
			<a class="mt-1 btn-cursos btn u-btn-blue" href="<?php echo get_permalink(); ?>">Saiba Mais</a>
			<?php if($product->get_price() > 0) : ?>
				<a class="mt-1 btn-cursos btn u-btn-primary" href="<?= adicionar_produto_ao_carrinho_url($product->id) ?>">Comprar</a>
			<?php else : ?>
				<a class="mt-1 btn-cursos btn btn-danger" href="<?= adicionar_produto_ao_carrinho_url($product->id) ?>">Grátis</a>
			<?php endif ?>
		</div>
	</div>
</div>
</div>		
	




<?php if($i == CURSOS_ONLINE_POR_PAGINA) : ?>
	<?php $qs_orderby = isset($_GET['orderby']) ? "&orderby={$_GET['orderby']}" : ""; ?>
	<?php $proxima_pagina = $wp_query->query['paged'] ? $wp_query->query['paged'] + 1 : 2; ?>

	<div class="col-12 text-center" id="ver-mais"> 
		<span> 
			<a class="ver-mais-simulados" href="#" onclick="mais(event, this)">CARREGAR MAIS</a>
		</span>

		<script>
		function mais(e, el) {
			e.preventDefault();
			jQuery(el).html('Carregando...');

			var url = "/wp-content/themes/academy/ajax/cursos_online.php?p=<?= $proxima_pagina . $qs_orderby ?>";

			jQuery.get(url, function(data) {

				//jQuery(el).closest('div').remove();
				jQuery(el).html('CARREGAR MAIS');
				jQuery('.cursos-online').append(data);
				jQuery(".cursos-online li:lt(<?= $proxima_pagina * CURSOS_ONLINE_POR_PAGINA / 2 ?>)").show();
			});
		}
		
		<?php if(!$wp_query->query['paged']) : ?>
		jQuery(".cursos-online li:lt(<?= CURSOS_ONLINE_POR_PAGINA / 2 ?>)").show();
		<?php endif ?>
		</script>
	</div>
<?php endif ?>

<script>
jQuery(function() {                       
  jQuery("#pesquisa-grid-2").click(function() {  
    jQuery(".cursos-online").addClass("col-4").removeClass("col-12");
    jQuery(".c-o-h-p").addClass("cursos-online-h-top");
    jQuery(".c-o-h-b").addClass("cursos-online-h-body")
    .removeClass("pt-2").removeClass("pb-2");
    jQuery(".pesquisa-parcelado").removeClass("d-md-inline");
  });
  jQuery("#pesquisa-linha-2").click(function() {  
    jQuery(".cursos-online").addClass("col-12").removeClass("col-4"); 
     jQuery(".c-o-h-p").removeClass("cursos-online-h-top");
    jQuery(".c-o-h-b").removeClass("cursos-online-h-body")
    .addClass("pt-2").addClass("pb-2");
    jQuery(".pesquisa-parcelado").addClass("d-md-inline");
  });
});

  
</script>
