<?php 
	$disponivel_class = $aula['disponivel'] ? "curso-aula-disponivel" : "curso-aula-indisponivel";

	if($aula['demo']) {
		$icone_sufixo = 'demo';
	}
	elseif($aula['is_disponivel']) {
		$icone_sufixo = 'disponivel';	
	}
	else {
		$icone_sufixo = 'indisponivel';
	}

	if($is_conta_usuario)
	{
		$atualizacao = tem_atualizacao_aula($produto_id, $aula['nome'], get_current_user_id());
	}

?>

<div class="<?= $aula['demo'] ? 'box-aula-demo' : ''?> box-aula row">
	<div class="font-15 font-weight-bold text-blue text-center mt-0 col-12 col-lg-3 text-center text-lg-left">
	<?php if($aula['demo']) : ?>
		<div class="text-center">
			<span class="lbl-aula-demo">
				<i class="fa fa-star"></i> <?= $chamada_instrucao ? "Como Acessar" : "Aula Demo" ?>
			</span>
		</div>
	<?php else : ?>
		<div class="text-center col-12"><?= $aula['nome']?></div>
	<?php endif ?>
		<div class="text-dark font-11 text-center mt-1 col-12"><?= $aula['disponivel'] ?></div>
	</div>

	<div class="mt-2 mt-lg-0 col-12 col-lg-6 font-10 curso-aula-descricao text-center text-lg-left">
		<?= $aula['descricao'] ?>
	</div>
	
	<div class="ml-2 ml-lg-0 mt-3 mt-lg-0 col-12 col-lg-3 text-center text-lg-left curso-aula-icones">
		<?php 

			$fundir_arquivos_pdf = get_post_meta($produto_id, PRODUTO_FUNDIR_ARQUIVOS, true);

		    // conta o número de pds na aula
		    $num_pdfs = contar_arquivos_aula_por_extensao($produto_id, $index, 'pdf');
		    
		    // trata caso do merge de pdfs
		    if($num_pdfs > 1 && $fundir_arquivos_pdf == YES) {
		        $num_pdfs = 1;
		    }
		    
		    // conta o número de vídeos na aula
			$num_videos = contar_arquivos_aula_por_extensao($produto_id, $index, get_video_extensoes()) +
				contar_videos_aula_vimeo($produto_id, $index);
				
		    // conta o número de cadernos na aula
			$num_cadernos = contar_cadernos_aula($produto_id, $index);

			//conta o número de mp3
			$num_audios = contar_arquivos_aula_por_extensao($produto_id, $index, 'mp3');
			$num_pdfs += $num_audios;

			// conta o número de resumos na aula
			$num_resumos = contar_resumos_aula($produto_id, $index);
			
			// trata caso do merge de pdfs
		    if($num_resumos > 1 && $fundir_arquivos_pdf == YES) {
		        $num_resumos = 1;
		    }

			// conta o número de mapas na aula
			$num_mapas = contar_mapas_aula($produto_id, $index);

			// trata caso do merge de pdfs
		    if($num_mapas > 1 && $fundir_arquivos_pdf == YES) {
		        $num_mapas = 1;
		    }
		?>


		<?php if($atualizacao) : ?>
			<div class="mb-3"><span class="nova-versao"><?= get_tema_image_tag("aula-alerta.png") ?> Nova versão</span></div>
		<?php endif ?>

		<?php if($num_pdfs) : ?>
			<?php if($aula['is_disponivel']) : ?>
			<a class="t-d-none cronograma-botao" href="#" data-t="<?= TIPO_AULA_PDF ?>" data-i="<?= $index ?>" data-p="<?= $produto_id ?>" data-d="<?= $aula['demo'] ?>">
				<?= get_tema_image_tag("icone-pdf-{$icone_sufixo}.png") ?>
				<span class="numero-arquivos"><?= $num_pdfs ?></span>
			</a>
			<?php else : ?>
				<?= get_tema_image_tag("icone-pdf-{$icone_sufixo}.png") ?>
				<span class="numero-arquivos"><?= $num_pdfs ?></span>
			<?php endif ?>
		<?php endif ?>

		<?php if($num_videos) : ?>
			<?php if($aula['is_disponivel']) : ?>
			<a class="t-d-none cronograma-botao" href="#" data-t="<?= TIPO_AULA_VIDEO ?>" data-i="<?= $index ?>" data-p="<?= $produto_id ?>" data-d="<?= $aula['demo'] ?>">
				<?= get_tema_image_tag("icone-video-{$icone_sufixo}.png") ?>
				<span class="numero-arquivos"><?= $num_videos ?></span>
			</a>
			<?php else : ?>
				<?= get_tema_image_tag("icone-video-{$icone_sufixo}.png") ?>
				<span class="numero-arquivos"><?= $num_videos ?></span>
			<?php endif ?>
		<?php endif ?>

		<?php if($num_cadernos) : ?>
			<?php if($aula['is_disponivel']) : ?>
			<a class="t-d-none cronograma-botao" href="#" data-t="<?= TIPO_AULA_CADERNO ?>" data-i="<?= $index ?>" data-p="<?= $produto_id ?>" data-d="<?= $aula['demo'] ?>">
				<?= get_tema_image_tag("icone-caderno-{$icone_sufixo}.png", "52px", "52px") ?>
				<span class="numero-arquivos"><?= $num_cadernos ?></span>
			</a>
			<?php else : ?>
				<?= get_tema_image_tag("icone-caderno-{$icone_sufixo}.png", "52px", "52px") ?>
				<span class="numero-arquivos"><?= $num_cadernos ?></span>
			<?php endif ?>
		<?php endif ?>

		<?php if($num_resumos) : ?>
			<?php if($aula['is_disponivel']) : ?>
			<a class="t-d-none cronograma-botao" href="#" data-t="<?= TIPO_AULA_RESUMO ?>" data-i="<?= $index ?>" data-p="<?= $produto_id ?>" data-d="<?= $aula['demo'] ?>">
				<?= get_tema_image_tag("icone-resumo-{$icone_sufixo}.png") ?>
				<span class="numero-arquivos"><?= $num_resumos ?></span>
			</a>
			<?php else : ?>
				<?= get_tema_image_tag("icone-resumo-{$icone_sufixo}.png") ?>
				<span class="numero-arquivos"><?= $num_resumos ?></span>
			<?php endif ?>
		<?php endif ?>

		<?php if($num_mapas) : ?>
			<?php if($aula['is_disponivel']) : ?>
			<a class="t-d-none cronograma-botao" href="#" data-t="<?= TIPO_AULA_MAPA ?>" data-i="<?= $index ?>" data-p="<?= $produto_id ?>" data-d="<?= $aula['demo'] ?>">
				<?= get_tema_image_tag("icone-mapa-{$icone_sufixo}.png") ?>
				<span class="numero-arquivos"><?= $num_mapas ?></span>
			</a>
			<?php else : ?>
				<?= get_tema_image_tag("icone-mapa-{$icone_sufixo}.png") ?>
				<span class="numero-arquivos"><?= $num_mapas ?></span>
			<?php endif ?>
		<?php endif ?>

	</div>


</div>
<div class="mb-3 cronograma-detalhe cronograma-detalhe-<?= $produto_id ?>-<?= $index ?>" id="cronograma-detalhe-<?= $produto_id ?>-<?= $index ?>-<?= TIPO_AULA_PDF ?>" style="display: none"></div>
<div class="mb-3 cronograma-detalhe cronograma-detalhe-<?= $produto_id ?>-<?= $index ?>" id="cronograma-detalhe-<?= $produto_id ?>-<?= $index ?>-<?= TIPO_AULA_VIDEO ?>" style="display: none"></div>
<div class="mb-3 cronograma-detalhe cronograma-detalhe-<?= $produto_id ?>-<?= $index ?>" id="cronograma-detalhe-<?= $produto_id ?>-<?= $index ?>-<?= TIPO_AULA_CADERNO ?>" style="display: none"></div>
<div class="mb-3 cronograma-detalhe cronograma-detalhe-<?= $produto_id ?>-<?= $index ?>" id="cronograma-detalhe-<?= $produto_id ?>-<?= $index ?>-<?= TIPO_AULA_RESUMO ?>" style="display: none"></div>
<div class="mb-3 cronograma-detalhe cronograma-detalhe-<?= $produto_id ?>-<?= $index ?>" id="cronograma-detalhe-<?= $produto_id ?>-<?= $index ?>-<?= TIPO_AULA_MAPA ?>" style="display: none"></div>