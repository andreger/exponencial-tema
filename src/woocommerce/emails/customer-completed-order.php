<?php
/**
 * Customer completed order email
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>

<html>
	<head>
		<meta http-equiv='Content-Type' content='text/html;UTF-8' />
	</head>
	<body style='margin: 0px; background-color: #F4F3F4; font-family: Helvetica, Arial, sans-serif; font-size:12px;' text='#444444' bgcolor='#F4F3F4' link='#21759B' alink='#21759B' vlink='#21759B' marginheight='0' topmargin='0' marginwidth='0' leftmargin='0'>
		<table cellspacing="0" style="width: 100%; border: 1px solid #eee;" border="1" bordercolor="#eee">
			<tr>
				<td align="center">
				
					<table cellspacing="0" cellpadding="6" style="width: 600px">
						<tr>
							<td align="center">
					
								<div style='border-bottom: 2px solid #ccc'>
									<a href='https://www.exponencialconcursos.com.br'>
										<img alt='Exponencial Concursos' src='https://www.exponencialconcursos.com.br/wp-content/themes/academy/images/logo2018.png' border='0' style='width: 400px'/>
									</a>
								</div>
								
								<div style="margin-top: 20px; margin-bottom: 40px; font-size: 32px; font-weight: bold; color: #79C258;">O seu pedido est&aacute; conclu&iacute;do. Muito obrigado por sua compra</div>
								
								<div style="margin-bottom: 20px;">Ol&aacute;, o seu pedido foi conclu&iacute;do. Para fazer o download dos arquivos, acesse nosso site em <a href='https://www.exponencialconcursos.com.br'>www.exponencialconcursos.com.br</a>, fa&ccedil;a o login e clique no menu Área do aluno' que aparece no canto superior direito de nosso site. Em seguida, fa&ccedil;a o download dos seus arquivos!</div>

								<div style="margin-bottom: 20px;">
									<a href='https://www.exponencialconcursos.com.br/minha-conta'>
										<img alt='Exponencial Concursos' src='https://www.exponencialconcursos.com.br/wp-content/themes/academy/images/botao-email-acesse.jpg' border='0' style='width: 400px'/>
									</a>
								</div>

								<div>Os detalhes do pedido est&atilde;o exibidos abaixo para sua refer&ecirc;ncia:</div>
								
								<?php do_action( 'woocommerce_email_before_order_table', $order, $sent_to_admin, $plain_text ); ?>
								
								<h2 style="font-size: 30px">N&uacute;mero do Pedido: #<?php echo $order->get_order_number(); ?></h2>
							</td>
						</tr>
					</table>
					
					<table cellspacing="0" cellpadding="6" style="width: 600px; border: 1px solid #eee;" border="1" bordercolor="#eee">
						<thead>
							<tr style="background-color: #047AAC; color: #fff">
								<th scope="col" style="text-align:left; border: 1px solid #eee;"><?php _e( 'Product', 'woocommerce' ); ?></th>
								<th scope="col" style="text-align:left; border: 1px solid #eee;"><?php _e( 'Quantity', 'woocommerce' ); ?></th>
								<th scope="col" style="text-align:left; border: 1px solid #eee;">Pre&ccedil;</th>
							</tr>
						</thead>
						<tbody>
							<?php echo $order->email_order_items_table( false, false, true ); // primeiro parametro especifica se entra link de downloads ?> 
						</tbody>
						<tfoot>
							<?php
								if ( $totals = $order->get_order_item_totals() ) {
									$i = 0;
									foreach ( $totals as $total ) {
										$i++;
										?><tr>
											<th scope="row" colspan="2" style="<?= ($i != 1) ? "color: #79C258;" : "" ?> text-align:left; border: 1px solid #eee; <?php if ( $i == 1 ) echo 'border-top-width: 4px;'; ?>"><?php echo $total['label']; ?></th>
											<td style="<?= ($i != 1) ? "background-color: #79C258; color: #fff;" : "" ?> text-align:left; border: 1px solid #eee; <?php if ( $i == 1 ) echo 'border-top-width: 4px;'; ?>"><?php echo $total['value']; ?></td>
										</tr><?php
									}
								}
							?>
						</tfoot>
					</table>
					
					<table width='600' border='0' cellspacing='0' cellpadding='0' style='border: 0'>
						<tbody>
							<tr>
								<td style='padding: 15px;'><center>
									<table width='600' cellspacing='0' cellpadding='0' align='center' bgcolor='#ffffff' style="border: 0">
										<tbody>
											<tr>
												<?php
													$cupons_bonus = array();
													foreach ($order->get_items() as $item) {
														$item_id = $item['product_id'];

														$post = get_post($item_id);	
														
														$cupons = get_cupom_bonus_by_item_slug($post->post_name, ITEM_BONUS_PRODUTO);

														if(!$cupons) {
															log_wp('debug','Email: Nao encontramos cupom com slug: ' . $post->post_name);
															log_wp('debug','Email: Listando categorias do post: ' . $post->ID);
															$categorias = listar_categorias($post->ID);
															
															log_wp('debug','Email: Achamos '. count($categorias) . ' categorias do post: ' . $post->ID);
															foreach ($categorias as $categoria) {

																log_wp('debug','Email: verificando categorias com slug: ' . $categoria->slug);
																if($itens = get_cupom_bonus_by_item_slug($categoria->slug, ITEM_BONUS_CATEGORIA)) {

																	foreach ($itens as $item) {
																		$item->nome_item =  $categoria->name;

																		array_push($cupons, $item);
																		log_wp('debug','Email: Achou cupom de categoria' . $categoria->slug);
																	}
																}
															}
														}
														else {
															foreach ($cupons as &$cupom) {
																$cupom->nome_item = $post->post_title;
															}
														}
														
														if($cupons) {
															foreach ($cupons as $cupom) {
																log_wp('debug', 'Email: Adicionando cupom nome:  '. $cupom->post_name. ' descricao: ' . $cupom->post_excerpt . ' item: ' . $cupom->nome_item);
																
																array_push($cupons_bonus, array(
																	'nome' => $cupom->post_name,
																	'descricao' => $cupom->post_excerpt,
																	'item' => $cupom->nome_item
																));
															}
														}
													}
												
													$relacionados = get_cursos_relacionados_pedido($order);
												?>
												
												<?php log_wp('debug', 'Numero de cupons: '. count($cupons_bonus)); ?>
												<?php if(count($cupons_bonus) > 0) : ?>
												<td align='left' valign='top' style="border: 0; width: 48%;">
													<div style="margin-bottom: 20px">
														<img alt='Exponencial Concursos' src='https://www.exponencialconcursos.com.br/wp-content/themes/academy/images/email-descontos.jpg' border='0' />
													</div>
													
													<?php foreach ($cupons_bonus as $cupom_desconto) : ?>
													<div style="margin: 0 0 20px 5px">
														<div style="color: #047AAC; font-size: 16px; font-weight: bold;"><?= htmlentities($cupom_desconto['descricao']) ?></div>

														<?php  
														/**
														 * Remoção da segunda linha e aumento de fonte da terceira linha
														 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/1307 
														 */
														?>
														<!--<div><?= htmlentities($cupom_desconto['item']) ?></div>-->
														<div style="color: #79C258; font-size: 12px; font-weight: bold;">use o cupom: <span style="font-size: 14px"><?= htmlentities($cupom_desconto['nome']) ?></span></div>	
													</div>
													<?php endforeach;?>
													
												</td>
												<?php endif ?>
												
												<?php if(count($relacionados) > 0) : ?>
												
												<td align='left' valign='top' style="border: 0; width: 48%;">
													<div style="text-align: center; margin-bottom: 20px">
														<img alt='Exponencial Concursos' src='https://www.exponencialconcursos.com.br/wp-content/themes/academy/images/email-recomendacoes.jpg' border='0' />
													</div>
			
													<?php foreach ($relacionados as $relacionado) : ?>		
													
													<?php $preco = is_pacote($relacionado) ? $relacionado->max_price : $relacionado->get_price(); ?>
																					
													<div style="text-align: center; margin-bottom: 20px">
														<div style="color: #047AAC; font-size: 14px; font-weight: bold;"><a style="color: #047AAC; font-size: 14px; font-weight: bold; text-decoration: none" href="<?= get_permalink($relacionado->post->ID) . get_utm('Pedido', 'relacionamento', 'upsell') ?>"><?= htmlentities($relacionado->post->post_title) ?></a></div>
														<div>por 10 x <?= moeda($preco / 10); ?></div>											
													</div>
													<?php endforeach; ?>
												</td>
												<?php endif; ?>
											</tr>
										</tbody>
									</table>
								</center></td>
							</tr>
						</tbody>
					</table>

					<table cellspacing="0" cellpadding="6" style="width: 600px">
						<tr>
							<td align="center" valign="middle" style="width: 200px">
								<a href='https://www.exponencialconcursos.com.br'>
										<img alt='Exponencial Concursos' src='http://www.exponencialconcursos.com.br/wp-content/themes/academy/images/logo2018.png' border='0' style='width: 200px'/>
								</a>
							</td>
								
							<td align="center" valign="middle" style="width: 250px; color: #666; font-size: 16px; margin-bottom: 20px">
								Acompanhe-nos nas redes sociais
							</td>

							<td align="center" valign="middle">
								<div>
									<a href="http://www.facebook.com/exponencialconcursos" class="descontos-promocoes">
										<img src="https://www.exponencialconcursos.com.br/wp-content/themes/academy/images/icone-facebook-colorido.png" width="20" height="20" alt="" class="">
									</a>
									<a href="http://www.instagram.com/exponencial_concursos" class="descontos-promocoes">
											<img src="https://www.exponencialconcursos.com.br/wp-content/themes/academy/images/icone-instagram-colorido.png" width="20" height="20" alt="" class="">							
									</a>
									<a href="http://www.youtube.com/channel/UCr9rg5WOPmXvZgOfBl-HEuw/videos" class="descontos-promocoes">
											<img src="https://www.exponencialconcursos.com.br/wp-content/themes/academy/images/icone-youtube-colorido.png" width="20" height="20" alt="" class="">							
									</a>
								</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>		
