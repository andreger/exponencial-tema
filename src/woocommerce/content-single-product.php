<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * Override this template by copying it to yourtheme/woocommerce/content-single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if (! defined ( 'ABSPATH' ))
	exit (); // Exit if accessed directly

global $product;

KLoader::model("ProdutoModel");
KLoader::helper("ProdutoHelper");

// se o produto está expirado, redireciona
if(is_produto_expirado($product->id)){
	wp_redirect(get_site_url() . '/cursos-por-concurso/?expirado=1', 301);
	exit;
}

// se o produto não está publicado, redireciona
if($product->get_status() != STATUS_POST_PUBLICADO) {
    wp_redirect(get_site_url() . '/cursos-por-concurso/?expirado=1', 301);
    exit;
}

// envia para página do carrinho, se parâmetro estiver presente
if (isset ( $_GET ['add-to-cart'] )) {
	header ( 'Location: /carrinho' );
}

$produto = ProdutoModel::get_by_id($product->id);
$relacionados = ProdutoModel::listar_relacionados($product->id);
$preco = is_pacote($product) ? $product->get_bundle_price() : $product->get_price();

$arquivos_demo = get_arquivos_aula_demonstrativa($product);
$chamada_instrucao = get_post_meta($product->id, 'chamada', true) == 1 ? true : false;
$mostrar_garantia = get_post_meta($product->id, 'mostrar_garantia', true) !== '' ? get_post_meta($product->id, 'mostrar_garantia', true) : 'yes';
?>

<?php KLoader::view("produto/botoes_flutuantes", ["product" => $product]) ?>
<?php KLoader::view("produto/informacoes/informacoes", ["product" => $product, 
    "produto" => $produto,
    "arquivos_demo" => $arquivos_demo,
    "chamada_instrucao" => $chamada_instrucao,
    "preco" => $preco,
    "mostrar_garantia" => $mostrar_garantia,
    "ocultar_parcelamento" => $ocultar_parcelamento])
?>

<?php KLoader::view("produto/cronograma/cronograma", ["product" => $product]) ?>

<?php KLoader::view("produto/rodape/rodape", [
    "arquivos_demo" => $arquivos_demo,
    "chamada_instrucao" => $chamada_instrucao,
    "product" => $product,
    "relacionados" => $relacionados
]) ?>

<?php KLoader::view("produto/tooltipster_templates") ?>

<?= vimeo_player_assets() ?>

<script>
	jQuery(function () {
		jQuery(document).on('click', '.pacote-produto-titulo', function () {
			jQuery(this).parent().find('.pacote-produto-conteudo').toggle();
		});

		jQuery(document).on('click', '.cronograma-botao', function (e) {
			e.preventDefault();

			var i = jQuery(this).data("i");
			var p = jQuery(this).data("p");
			var t = jQuery(this).data("t");
			var d = jQuery(this).data("d");

			if(jQuery("#cronograma-detalhe-"+p+"-"+i+"-"+t).is(':visible')) {
				jQuery(".cronograma-detalhe-"+p+"-"+i).hide();
			}
			else {
				jQuery(".cronograma-detalhe-"+p+"-"+i).hide();
				jQuery("#cronograma-detalhe-"+p+"-"+i+"-"+t).html("<div class='cronograma-detalhe-interno'>Carregando...</div>");
				jQuery("#cronograma-detalhe-"+p+"-"+i+"-"+t).show();

				jQuery.post("<?= TEMA_AJAX_PATH ?>listar_arquivos_aula.php", {'i': i, 'p': p, 't': t, 'd': d}, function(data) {
					jQuery("#cronograma-detalhe-"+p+"-"+i+"-"+t).html(data);
				});
			}
		});

		jQuery(document).on('click', '#ir-aula-demo', function (event) {
			<?php if(is_pacote($product)) : ?>
				jQuery(document).find('.pacote-produto-conteudo:first').show();
			<?php endif; ?>

			jQuery('html, body').animate({
				scrollTop: jQuery(document).find( '.box-aula-demo:first' ).offset().top - 230
			}, 500);
		});

	});
</script>