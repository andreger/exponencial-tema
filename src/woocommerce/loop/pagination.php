<?php

return;

if (!defined('ABSPATH')) {
	exit;
}

global $wp_query;
if ($wp_query->max_num_pages<=1) {
	return;
}
?>
<nav class="pagination bbp-pagination-links cursos-online-pagination" style="margin-top: 0 !important;">
	<?php
		echo paginate_links(apply_filters('woocommerce_pagination_args', array(
			'base' 			=> str_replace(999999999, '%#%', get_pagenum_link(999999999)),
			'format' 		=> '',
			'current' 		=> max(1, get_query_var('paged')),
			'total' 		=> $wp_query->max_num_pages,
			'prev_text' 	=> '« Anterior',
			'next_text' 	=> 'Próximo »',
			'type'			=> 'plain',
			'end_size'		=> 3,
			'mid_size'		=> 3
		)));
	?>
</nav>