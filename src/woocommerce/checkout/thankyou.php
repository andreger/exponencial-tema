<?php
/**
 * Thankyou page
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $woocommerce;

if ( $order ) : ?>
	
	<div class="pedido-concluido">
	
	<?php $fb_value = number_format($order->get_total(), 2) ?>
	
	<?= google_pixel($fb_value, $order->get_id()) ?>
	
	<?php if ( in_array( $order->status, array( 'failed' ) ) ) : ?>

		<p><?php _e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction.', 'woocommerce' ); ?></p>

		<p><?php
			if ( is_user_logged_in() )
				_e( 'Please attempt your purchase again or go to your account page.', 'woocommerce' );
			else
				_e( 'Please attempt your purchase again.', 'woocommerce' );
		?></p>

		<p>
			<a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay"><?php _e( 'Pay', 'woocommerce' ) ?></a>
			<?php if ( is_user_logged_in() ) : ?>
			<a href="<?php echo esc_url( get_permalink( wc_get_page_id( 'myaccount' ) ) ); ?>" class="button pay"><?php _e( 'My Account', 'woocommerce' ); ?></a>
			<?php endif; ?>
		</p>

	<?php else : ?>
		<div class="container pt-2 pt-md-5 pb-5">
			
			
			<div class="col-12 text-center mt-4 mt-md-0">
				<div class="col-md-11 col-lg-8 text-center p-2 bg-blue ml-auto mr-auto border-top border-primary">
				<h1 class="font-agradecimento text-white">Obrigado por sua compra. Conte conosco na sua trajetória rumo à aprovação!</h1>
			</div>
				<div>
					<a href="/minha-conta"><img src="<?php echo get_tema_image_url('material-exponencial.png') ?>"></a></div>
			</div>
			<!-- <div class="threecol fleft column">
				<div class="acessar-conta">
					<a href="/minha-conta"><img src="<?php echo get_tema_image_url('acessar-conta.png') ?>"></a>
					<a href="/minha-conta"><img src="<?php echo get_tema_image_url('material.png') ?>"></a>
				</div>
			</div> -->
		
		
		<div class="row mt-4">	
			<div class="col-md-1 col-lg-2"></div>	
			<div class="col-md-11 col-lg-10">
				<div class="order_details font-10">
					<div class="order">
						<?php _e( 'Pedido:', 'woocommerce' ); ?>
						<strong><?php echo $order->get_order_number(); ?></strong>
					</div>
					<div class="date">
						<?php _e( 'Date:', 'woocommerce' ); ?>
						<strong><?php echo date_i18n( 'j \d\e F \d\e Y', strtotime( $order->order_date ) ); ?></strong>
					</div>
					<div class="total">
						<?php _e( 'Total:', 'woocommerce' ); ?>
						<strong><?php echo $order->get_formatted_order_total(); ?></strong>
					</div>
					<?php if ( $order->payment_method_title ) : ?>
					<div class="method">
						<?php _e( 'Método de Pagamento:', 'woocommerce' ); ?>
						<strong><?php echo $order->payment_method_title; ?></strong>
					</div>
					<?php endif; ?>
				</div>
			</div>						
		</div>
		

	<?php endif; ?>
	
		<div class="row mt-3">
			<div class="col-md-1 col-lg-2"></div>
			<div class="col-md-10 col-lg-8">				
				<?php do_action( 'woocommerce_thankyou_' . $order->payment_method, $order->id ); ?>
				<?php do_action( 'woocommerce_thankyou', $order->id ); ?>
			</div>			
		</div>		
	</div>
<?php else : ?>

	<p><?php _e( 'Thank you. Your order has been received.', 'woocommerce' ); ?></p>

<?php endif; ?>
</div>
<?php 
	//B2700 removido 21/10/2019
	/*KLoader::view("plugins/google_merchant/google_merchant_opt_in", 
		[
			'order_id' => $order->get_id(), 
			'user_email' => $order->get_billing_email(),
			'user_country' => $order->get_billing_country(),
			'order_date' => $order->get_date_created()->date('Y-m-d')
		]
	);*/ 
?>