<?php
/**
 * Checkout billing information form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.1.2
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if(!is_user_logged_in()) {
	header('Location: /cadastro-login/?ref=/checkout');
	exit();
}
else {
	
	KLoader::helper("CarrinhoHelper");
	$tem_assinatura_recorrente = CarrinhoHelper::tem_produto_assinatura_recorrente_no_carrinho();
	$perfil_basico = ( (WC()->cart->subtotal == 0) && !$tem_assinatura_recorrente ) ? true : false;
	if(!dados_usuario_completos(get_current_user_id(), $perfil_basico)) {
		header('Location: ' . '/questoes/perfil/editar?from=checkout&perfil_basico=' . (int)$perfil_basico);
		exit;
	}
}
?>

<div class="woocommerce-billing-fields col-12">
	<?php if ( WC()->cart->ship_to_billing_address_only() && WC()->cart->needs_shipping() ) : ?>

		<h3><?php _e( 'Billing &amp; Shipping', 'woocommerce' ); ?></h3>

	<?php else : ?>

		<h3>Detalhes da Nota Fiscal</h3>

	<?php endif; ?>

	<?php do_action( 'woocommerce_before_checkout_billing_form', $checkout ); ?>

	<?php 
		/**
		 * Adicionados CEP e Cidade padrão. Esses dados não são obrigatórios para compras gratuitas, mas o Woocommerce obriga o preenchimento desses campos
		 * 
		 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2037
		 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/1421
		 */
		
		$checkout->checkout_fields['billing']['billing_last_name']['placeholder'] = 'Sobrenome';
		$checkout->checkout_fields['billing']['billing_postcode']['placeholder'] = 'CEP';
//		$checkout->checkout_fields['billing']['billing_company']['placeholder'] = 'Empresa';

		$aluno = get_usuario_array();
		$cidade_row = get_cidade_por_codigo_ibge($aluno['cidade']);

		$u_map = array(
			'billing_country' => '',
			'billing_first_name' => $aluno['nome'],
			'billing_last_name' => $aluno['sobrenome'],
			'billing_address_1'=> $aluno['endereco'] ?: "Rua A",
			'billing_address_2' => "-",
			'billing_city' =>  $cidade_row['cid_nome'] ?: "Cidade",
			'billing_state' => $aluno['uf'] ?: "RJ",
			'billing_postcode' => $aluno['cep'] ?: "10000000",
			'billing_email' => $aluno['email'] ?: "",
			'billing_phone' => $aluno['telefone'] ?: "(11) 92345-6789",
			'billing_cpf' => $aluno['cpf'] ?: "60737983507", // cpf válido gerado por código
			'billing_neighborhood' => $aluno['bairro'] ?: "Bairro",
			'billing_number' => "-"
		);		
	?>

	<?php foreach ( $checkout->checkout_fields['billing'] as $key => $field ) : ?>

	<?php
		// Campos fixos são necessários para corrigir bug onde dados inválidos antigos são enviados para o pagseguro impedindo a conclusão da compra
		$campos_fixos = ['billing_first_name', 'billing_last_name', 'billing_phone', 'billing_number', 'billing_address_1','billing_address_2', 'billing_postcode', 'billing_neighborhood', 'billing_cpf', 'billing_state', 'billing_city', 'billing_email'];
		
		if(in_array($key, $campos_fixos)) {
			woocommerce_form_field( $key, $field, $u_map[$key]);
		}
		else {
			woocommerce_form_field( $key, $field, strlen($checkout->get_value( $key )) == 0 ? $u_map[$key] : $checkout->get_value( $key ));
		}
	?>

	<?php endforeach; ?>

	<?php do_action('woocommerce_after_checkout_billing_form', $checkout ); ?>

	<?php if ( ! is_user_logged_in() && $checkout->enable_signup ) : ?>

		<?php if ( $checkout->enable_guest_checkout ) : ?>

			<p class="form-row form-row-wide create-account">
				<input class="input-checkbox" id="createaccount" <?php checked( ( true === $checkout->get_value( 'createaccount' ) || ( true === apply_filters( 'woocommerce_create_account_default_checked', false ) ) ), true) ?> type="checkbox" name="createaccount" value="1" /> <label for="createaccount" class="checkbox"><?php _e( 'Create an account?', 'woocommerce' ); ?></label>
			</p>

		<?php endif; ?>

		<?php do_action( 'woocommerce_before_checkout_registration_form', $checkout ); ?>

		<?php if ( ! empty( $checkout->checkout_fields['account'] ) ) : ?>

			<div class="create-account">

				<p><?php _e( 'Create an account by entering the information below. If you are a returning customer please login at the top of the page.', 'woocommerce' ); ?></p>

				<?php foreach ( $checkout->checkout_fields['account'] as $key => $field ) : ?>

					<?php woocommerce_form_field( $key, $field, $checkout->get_value( $key ) ); ?>

				<?php endforeach; ?>

				<div class="clear"></div>

			</div>

		<?php endif; ?>

		<?php do_action( 'woocommerce_after_checkout_registration_form', $checkout ); ?>
		
	<?php endif; ?>
</div>