<?php
/**
 * Checkout coupon form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $woocommerce;

if ( ! WC()->cart->coupons_enabled() ) {
	return;
}

$info_message = apply_filters( 'woocommerce_checkout_coupon_message', __( 'Você tem um cupom de desconto?', 'woocommerce' ) );
$info_message .= ' <a href="#" class="showcoupon">' . __( 'Clique aqui e informe o código do cupom', 'woocommerce' ) . '</a>';
wc_print_notice( $info_message, 'notice' );
?>

<form class="checkout_coupon" method="post" style="display:none">

	<p class="form-row form-row-first">
		<input type="text" name="coupon_code" class="form-control input-text" placeholder="<?php _e( 'Código do cupom', 'woocommerce' ); ?>" id="coupon_code" value="" />
	</p>

	<p class="form-row form-row-last">
		<input type="submit" class="ml-auto mr-auto button" name="apply_coupon" value="<?php _e( 'Utilizar cupom', 'woocommerce' ); ?>" />
	</p>

	<div class="clear"></div>
	
	<div id="cupom-utilizado" style='font-weight:bold;color: #ff0000' style="display:none">Atenção: Um cupom já está sendo utilizado. Ao adicionar outro cupom o atual será removido.</div>
</form>

