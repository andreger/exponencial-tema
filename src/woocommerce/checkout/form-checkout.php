<?php 
if(!is_user_logged_in()) {
	header('Location: /cadastro-login/?ref=/checkout');
	exit();
}
else {
	KLoader::helper("CarrinhoHelper");
	$tem_assinatura_recorrente = CarrinhoHelper::tem_produto_assinatura_recorrente_no_carrinho();
	if(WC()->cart->subtotal != 0 || $tem_assinatura_recorrente) {
		$perfil_basico = ( (WC()->cart->subtotal == 0) && !$tem_assinatura_recorrente ) ? true : false;
		if(!dados_usuario_completos(get_current_user_id(), $perfil_basico)) {
			header('Location: ' . '/questoes/perfil/editar?from=checkout&perfil_basico=' . (int)$perfil_basico);
			exit;
		}
	}
}

/**
 * Código provisório para redirecionamento de carrinho entre os dias 28/08/19 e 31/08/19
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2712
 */
$agora = date("Y-m-d H:i:s");
$inicial = "2019-08-28 12:00:00";
$final = "2019-09-01 23:59:59";

if(($agora >= $inicial && $agora <= $final) && !is_administrador()) {
	header('Location: https://www.exponencialconcursos.com.br/eventos/100-assinaturas-gratis');
	exit;
}

$usuario = get_usuario_array();

?>

<?php $fb_value = number_format(WC()->cart->total, 2, ".", "") ?>
<!-- Google Code for Pedido no carrinho Conversion Page -->
<!-- Google Code for Compras Google 2016 Conversion Page -->
<script>
/* <![CDATA[ */
var google_conversion_id = 962569938;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "WMokCLil7mQQ0s3-ygM";
var google_conversion_value = <?php echo $fb_value?>;
var google_conversion_currency = "BRL";
var google_remarketing_only = false;
/* ]]> */
</script>
<script src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/962569938/?value=<?php echo $fb_value?>&amp;currency_code=BRL&amp;label=WMokCLil7mQQ0s3-ygM&amp;guid=ON&amp;script=0"/>
</noscript>
<input type="hidden" id="txtEmail" name="txtEmail" value="<?= $usuario['email'] ?>">
<div class="container">
<div class="row">

<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $woocommerce;

wc_print_notices();

//a chamada wp_add_notice não está funcionando no ponto em que essa informação é verificada por isso o workaround na sessão
if($recorrente = $_SESSION['recorrente_aviso'] ?? null){
	if($recorrente == 'nao-recorrente'){
		wc_print_notice("Existe um produto de assinatura no carrinho que não pode ser comprado juntamento com outros produtos. Para efetuar a compra remova todos os itens atuais do carrinho e adicione novamente o produto no carrinho.", "notice");
	}elseif($recorrente == 'recorrente'){
		wc_print_notice("O produto que está tentando comprar é uma assinatura e não pode ser comprado juntamente com outros produtos. Para efetuar a compra remova todos os itens atuais do carrinho e adicione novamente o produto no carrinho.", "notice");
	}
	$_SESSION['recorrente_aviso'] = null;
}

do_action( 'woocommerce_before_checkout_form', $checkout );

// If checkout registration is disabled and not logged in, the user cannot checkout
if ( ! $checkout->enable_signup && ! $checkout->enable_guest_checkout && ! is_user_logged_in() ) {
	echo apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) );
	return;
}

// filter hook for include new pages inside the payment method
$get_checkout_url = apply_filters( 'woocommerce_get_checkout_url', WC()->cart->get_checkout_url() ); ?>

<div class="sectiongap"></div>
	<form name="checkout" method="post" class="checkout w-100" action="<?php echo esc_url( $get_checkout_url ); ?>">
	
		<?php if ( sizeof( $checkout->checkout_fields ) > 0 ) : ?>
	
			<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>
	
			<div id="customer_details">	
				<div class="col-12">	
					<?php do_action( 'woocommerce_checkout_billing' ); ?>	
				</div>	
				<div class="col-12">	
					<?php do_action( 'woocommerce_checkout_shipping' ); ?>	
				</div>	
			</div>
			

			<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>
	
			<div class="mt-2 col-12 mb-3" id="order_review_heading"><h1 class="font-carrinho"><?php _e( 'Your order', 'woocommerce' ); ?></h1></div>
	
		<?php endif; ?>

		<?php do_action( 'woocommerce_checkout_order_review' ); ?>
	</form>
</div>
</div>	

		
<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>

<script>
jQuery(function() {
	jQuery(".showcoupon").click(function () {

		if(jQuery(".cart-discount").length > 0) {
			jQuery("#cupom-utilizado").show();
		}
		else {
			jQuery("#cupom-utilizado").hide();
		}
	});
});
</script>

<style>
.woocommerce-message {
    display: none;
}

.container .woocommerce-message {
    display: block;
    width: 100% !important;
}
</style>

