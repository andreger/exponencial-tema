<?php
// Removes the wlwmanifest link
remove_action( 'wp_head', 'wlwmanifest_link' );
// Removes the RSD link
remove_action( 'wp_head', 'rsd_link' );
// Removes the WP shortlink
remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );
// Removes the canonical links
remove_action( 'wp_head', 'rel_canonical' );
// Removes the links to the extra feeds such as category feeds
remove_action( 'wp_head', 'feed_links_extra', 3 );
// Removes links to the general feeds: Post and Comment Feed
remove_action( 'wp_head', 'feed_links', 2 );
// Removes the index link
remove_action( 'wp_head', 'index_rel_link' );
// Removes the prev link
remove_action( 'wp_head', 'parent_post_rel_link' );
// Removes the start link
remove_action( 'wp_head', 'start_post_rel_link' );
// Removes the relational links for the posts adjacent to the current post
remove_action( 'wp_head', 'adjacent_posts_rel_link' );
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head' );
// Removes the WordPress version i.e. -
remove_action( 'wp_head', 'wp_generator' );
// Removes the emoji support
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

ob_start ();
// Define constants
define ( 'SITE_URL', home_url () . '/' );
define ( 'AJAX_URL', admin_url ( 'admin-ajax.php' ) );
define ( 'THEME_PATH', get_template_directory () . '/' );
define ( 'CHILD_PATH', get_stylesheet_directory () . '/' );
define ( 'THEME_URI', get_template_directory_uri () . '/' );
define ( 'CHILD_URI', get_stylesheet_directory_uri () . '/' );
define ( 'THEMEX_PATH', THEME_PATH . 'framework/' );
define ( 'THEMEX_URI', THEME_URI . 'framework/' );
define ( 'THEMEX_PREFIX', 'themex_' );
// Set content width
$content_width = 1140;
// Load language files
load_theme_textdomain ( 'academy', THEME_PATH . 'languages' );
// Include theme functions
include (THEMEX_PATH . 'functions.php');
// Include configuration
include (THEMEX_PATH . 'config.php');
// Include core class
include (THEMEX_PATH . 'classes/themex.core.php');

// Create theme instance
$themex = new ThemexCore ( $config );

remove_filter ( 'the_content', 'wpautop' );
remove_filter ( 'the_excerpt', 'wpautop' );
function hide_add_new_custom_type() {
	global $submenu;
	// replace my_type with the name of your post type
	unset ( $submenu ['edit.php?post_type=product'] [10] );
}

add_action ( 'admin_menu', 'hide_add_new_custom_type' );
function hd_add_buttons() {
	global $pagenow;
	if (is_admin ()) {
		if (isset($_GET ['post_type']) && $_GET ['post_type'] == 'product') {
			echo '<style>.add-new-h2{display: none !important;}</style>';
		}
	}
}
add_action ( 'admin_head', 'hd_add_buttons' );

// custom coding for add courses start
function insert_attachment($file_handler, $post_id, $setthumb = 'false') {
	// check to make sure its a successful upload
	if ($_FILES [$file_handler] ['error'] !== UPLOAD_ERR_OK)
		__return_false ();

	require_once (ABSPATH . "wp-admin" . '/includes/image.php');
	require_once (ABSPATH . "wp-admin" . '/includes/file.php');
	require_once (ABSPATH . "wp-admin" . '/includes/media.php');

	$attach_id = media_handle_upload ( $file_handler, $post_id );

	if ($setthumb)
		update_post_meta ( $post_id, '_thumbnail_id', $attach_id );
	return $attach_id;
}

add_action ( 'show_user_profile', 'oneTarek_extra_user_profile_fields' );
add_action ( 'edit_user_profile', 'oneTarek_extra_user_profile_fields' );

function oneTarek_extra_user_profile_fields($user) {
	?>
<h3>Extra Custom Meta Fields</h3>

<table class="form-table">
	<tr>
		<th><label for="oneTarek_twitter">Nome da Mãe</label></th>
		<td><input type="text" id="oneTarek_mother_name"
			name="oneTarek_mother_name" size="20"
			value="<?php echo esc_attr( get_the_author_meta( 'oneTarek_mother_name', $user->ID )); ?>">

		</td>
	</tr>

	<tr>
		<th><label for="oneTarek_twitter">CPF</label></th>
		<td><input type="text" id="oneTarek_CPF" name="oneTarek_CPF" size="20"
			value="<?php echo esc_attr( get_the_author_meta( 'oneTarek_CPF', $user->ID )); ?>">

		</td>
	</tr>
	<tr>
		<th><label for="oneTarek_twitter">Data de nascimento</label></th>
		<td><input type="text" id="oneTarek_date" name="oneTarek_date"
			size="20"
			value="<?php echo esc_attr( get_the_author_meta( 'oneTarek_date', $user->ID )); ?>">

		</td>
	</tr>

	<tr>
		<th><label for="oneTarek_twitter">CEP</label></th>
		<td><input type="text" id="oneTarek_CEP" name="oneTarek_CEP" size="20"
			value="<?php echo esc_attr( get_the_author_meta( 'oneTarek_CEP', $user->ID )); ?>">

		</td>
	</tr>

	<tr>
		<th><label for="oneTarek_twitter">Bairro</label></th>
		<td><input type="text" id="oneTarek_nhb" name="oneTarek_nhb" size="20"
			value="<?php echo esc_attr( get_the_author_meta( 'oneTarek_nhb', $user->ID )); ?>">
		</td>
	</tr>

	<tr>
		<th><label for="oneTarek_twitter">Designation</label></th>
		<td><input type="text" id="oneTarek_designation"
			name="oneTarek_designation" size="20"
			value="<?php echo esc_attr( get_the_author_meta( 'oneTarek_designation', $user->ID )); ?>">
		</td>
	</tr>

	<tr>
		<th><label for="oneTarek_twitter">Tipo de Autor</label></th>
		<td><select id="oneTarek_author_type" name="oneTarek_author_type">
 <?php 	$author_opts = array('1' => 'Professor', '2' => 'Coaching', '3' => 'Professor e Coaching'); ?>
 <option value="">— Nenhum tipo associado —</option>
 <?php foreach($author_opts as $opt_key => $opt_value) : ?>
 	<option value="<?php echo $opt_key ?>"
					<?php echo esc_attr( get_the_author_meta( 'oneTarek_author_type', $user->ID )) == $opt_key ? 'selected=selected' : '' ?>><?php echo $opt_value ?></option>
 <?php endforeach ?>
 </select></td>
	</tr>
	<tr>
		<th><label for="slug">Slug</label></th>
		<td><input type="text" id="slug" name="slug" class="regular-text"
			value="<?= get_user_meta($user->ID, 'slug', TRUE); ?>">
		</td>
	</tr>
	<tr>
		<th><label for="mini_cv">Mini CV</label></th>
		<td><!--<textarea id="mini_cv" name="mini_cv" rows="6"><?php echo esc_attr(get_the_author_meta('mini_cv', $user->ID)); ?></textarea>-->
			<?= wp_editor(get_the_author_meta('mini_cv', $user->ID), 'mini_cv') ?>
		</td>
	</tr>
	<tr>
		<th><label for="cargo">Cargo</label></th>
		<td><input type="text" id="cargo" name="cargo" class="regular-text"
			value="<?php echo esc_attr(get_the_author_meta('cargo', $user->ID)); ?>">
		</td>
	</tr>
	<tr>
		<th><label for="cargo_coaching">Cargo Coaching</label></th>
		<td><input type="text" id="cargo_coaching" name="cargo_coaching" class="regular-text"
			value="<?php echo esc_attr(get_the_author_meta('cargo_coaching', $user->ID)); ?>">
		</td>
	</tr>
	<tr>
		<th><label for="cargo_concurso">Cargo Concurso</label></th>
		<td><input type="text" id="cargo_concurso" name="cargo_concurso" class="regular-text"
			value="<?php echo esc_attr(get_the_author_meta('cargo_concurso', $user->ID)); ?>">
		</td>
	</tr>
	<tr>
		<th><label for="cargo">Percentual Lucro</label></th>
		<td><input type="text" id="" name="lucro" class="regular-text"
			value="<?= get_valor_lucro_atual($user->ID); ?>">
		</td>
	</tr>
	<tr>
		<?php $ocultar = get_user_meta($user->ID, 'ocultar', TRUE); ?>
		<th><label for="cargo">Ocultar da página de professores</label></th>
		<td><input type="checkbox" id="ocultar" name="ocultar" class="checkbox double"
			value="1" <?= $ocultar ? 'checked' : '' ?> >
			<p class="description"><label for="ocultar">Remove o professor da página de professores</label></p>
		</td>
	</tr>
</table>
<?php

}
add_filter ( 'add_to_cart_text', 'woo_custom_cart_button_text' ); // < 2.1
add_filter ( 'woocommerce_product_single_add_to_cart_text', 'woo_custom_cart_button_text' ); // 2.1 +
function woo_custom_cart_button_text() {
	return __ ( 'Comprar', 'woocommerce' );
}

/* -- get blog list function for blog page-- */

// for logged in users
add_action ( 'wp_ajax_get_blog_list', 'get_blog_list' );
// for not logged in users
add_action ( 'wp_ajax_nopriv_get_blog_list', 'get_blog_list' );
function get_blog_list() {
	global $wpdb;
	if ($_REQUEST ['ptype'] != '') {
		$ptype = $_REQUEST ['ptype'];
	} else {
		$ptype = 'articles';
	}

	// $result_set = get_posts(array('order' => 'DESC', 'category' => $ct ));
	query_posts ( 'category_name=' . $ptype . ' & orderby=DESC' );

	echo '<ul>';
	// foreach($result_set as $reslist){
	while ( have_posts () ) :
		the_post ();
		?>
<li class="b_listing"><span class="testi_title"> <a
		href="<?php the_permalink(); ?>">
                        <?php the_title(); ?>
                    </a>
</span> <span class="testi_t2"><?php the_date('d-m-Y'); ?></span> <span
	class="testi_content"><?php the_excerpt();?> <?php the_content(); ?>
                    </span> <a href="<?php the_permalink(); ?>"> <img
		src="<?php echo THEME_URI; ?>images/leiamaisi.png" class="">
</a>
	<div class="clear"></div></li>
<?php
	endwhile
	;
	// } // foreach ends
	echo '</ul>';
	exit (); // always call exit at the end of a WordPress ajax function
}

global $current_user;
$user_id = $current_user->ID;
$user_level = get_user_meta ( $user_id, 'wp_user_level', true );

// add course codding ends
function remove_menus() {
	global $menu;
	global $current_user;
	get_currentuserinfo ();

	if ($current_user->user_level < 10) {
		$restricted = array (
				__ ( 'Pages' ),
				__ ( 'Links' ),

				__ ( 'Comments' ),
				__ ( 'Appearance' ),
				__ ( 'Plugins' ),

				__ ( 'Tools' ),
				__ ( 'Settings' ),
				__ ( 'Posts' ),
				__ ( 'Restrictions' ),
				__ ( 'Roles' )
		);
		end ( $menu );
		while ( prev ( $menu ) ) {
			$value = explode ( ' ', $menu [key ( $menu )] [0] );
			if (in_array ( $value [0] != NULL ? $value [0] : "", $restricted )) {
				unset ( $menu [key ( $menu )] );
			}
		} // end while
	} // end if
}
add_action ( 'admin_menu', 'remove_menus' );

register_nav_menus ( array (

		'secondary' => __ ( 'Secondary Menu', 'yourtheme' )
)
 );

add_action ( 'wp_logout', 'go_home' );
function go_home() {
	wp_redirect ( home_url () );
	exit ();
}

// remove wp version param from any enqueued scripts
function vc_remove_wp_ver_css_js($src) {
	if (strpos ( $src, 'ver=' ))
		$src = remove_query_arg ( 'ver', $src );
	return $src;
}
add_filter ( 'style_loader_src', 'vc_remove_wp_ver_css_js', 9999 );
add_filter ( 'script_loader_src', 'vc_remove_wp_ver_css_js', 9999 );

// Display Fields

add_filter ( 'woocommerce_continue_shopping_redirect', 'my_woocommerce_continue_shopping_redirect', 20 );
function my_woocommerce_continue_shopping_redirect($return_to) {
	return '/todos-cursos';
}

function expoconcursos_the_title_trim($title) {
	$title = attribute_escape($title);
	$findthese = array(
			'#Protected:#',
			'#Private:#',
			'#Privado:#',
	);
	$replacewith = array(
			'', // What to replace "Protected:" with
			'', // What to replace "Private:" with
			''
	);
	$title = preg_replace($findthese, $replacewith, $title);
	return $title;
}
add_filter('the_title', 'expoconcursos_the_title_trim');

function expoconcursos_breadcrumb_options() {
	// Home - default = true
	//$args['include_home']    = false;
	// Forum root - default = true
	$args['include_root']    = false;
	// Current - default = true
	//$args['include_current'] = true;

	return $args;
}

add_filter('bbp_before_get_breadcrumb_parse_args', 'expoconcursos_breadcrumb_options');

add_action( 'before_delete_post', 'expoconcursos_remove_forum_product' );
add_action( 'wp_trash_post', 'expoconcursos_remove_forum_product' );
function expoconcursos_remove_forum_product( $postid ){

	$post_type = get_post_type($postid);

	if ( $post_type == 'product' ) {

		$forum_id = get_post_meta($postid, 'forum_id', true );

		// Get the forum
		$forum = bbp_get_forum( $forum_id );

		// Bail if forum cannot be found
		if ( !empty( $forum ) ) {

			delete_post_meta($post_id, 'forum_id');
			delete_post_meta($forum_id, 'product_id');

			bbp_delete_forum( $forum_id );
			wp_delete_post($forum_id);
		}

	}
}

add_filter( 'bbp_get_dynamic_roles', 'my_bbp_custom_role_names');

function my_bbp_custom_role_names(){
	return array(

			// Keymaster
			bbp_get_keymaster_role() => array(
					'name'         => __( 'Prof. e Adm.', 'bbpress' ),
					'capabilities' => bbp_get_caps_for_role( bbp_get_keymaster_role() )
			),

			// Moderator
			bbp_get_moderator_role() => array(
					'name'         => __( 'Professor', 'bbpress' ),
					'capabilities' => bbp_get_caps_for_role( bbp_get_moderator_role() )
			),

			// Participant
			bbp_get_participant_role() => array(
					'name'         => __( 'Aluno', 'bbpress' ),
					'capabilities' => bbp_get_caps_for_role( bbp_get_participant_role() )
			),

			// Spectator
			bbp_get_spectator_role() => array(
					'name'         => __( 'Espectador', 'bbpress' ),
					'capabilities' => bbp_get_caps_for_role( bbp_get_spectator_role() )
			),

			// Blocked
			bbp_get_blocked_role() => array(
					'name'         => __( 'Bloqueado', 'bbpress' ),
					'capabilities' => bbp_get_caps_for_role( bbp_get_blocked_role() )
			)
	);
}

add_filter( 'bbp_get_author_link', 'remove_author_links', 10, 2);
add_filter( 'bbp_get_reply_author_link', 'remove_author_links', 10, 2);
add_filter( 'bbp_get_topic_author_link', 'remove_author_links', 10, 2);
function remove_author_links($author_link, $args) {
	$author_link = preg_replace(array('{<a[^>]*>}','{}'), array(""), $author_link);
	return $author_link;
}

 function wpse_77441_change_time_format( $anchor, $forum_id )
 {
     $last_active = get_post_meta( $forum_id, '_bbp_last_active_time', true );

     if ( empty( $last_active ) ) {
         $reply_id = bbp_get_forum_last_reply_id( $forum_id );

         if ( !empty( $reply_id ) ) {
             $last_active = get_post_field( 'post_date', $reply_id );
         } else {
             $topic_id = bbp_get_forum_last_topic_id( $forum_id );

             if ( !empty( $topic_id ) ) {
                 $last_active = bbp_get_topic_last_active_time( $topic_id );
             }
         }
     }

     $dt = date('d/m/Y \à\s H:i', strtotime($last_active));
     $time_since = bbp_get_forum_last_active_time( $forum_id );

     return str_replace( "$time_since</a>", "$dt</a>", $anchor );
 }
add_filter('bbp_get_forum_freshness_link', 'wpse_77441_change_time_format', 10, 2);
add_filter('bbp_get_topic_freshness_link', 'wpse_77441_change_time_format', 10, 2);

add_filter('bbp_get_reply_post_date', 'ntwb_bbpress_enable_date_translation', 10, 6);
add_filter('bbp_get_topic_post_date', 'ntwb_bbpress_enable_date_translation', 10, 6);

function ntwb_bbpress_enable_date_translation( $result, $reply_id, $humanize, $gmt, $date, $time ) {

	$date = get_post_time( 'd/m/Y', $gmt, $reply_id, $translate = true );
	$result = sprintf( _x( '%1$s &agrave;s %2$s', 'date at time', 'bbpress' ), $date, $time );
	return $result;
}

function get_professor_link($professor_id, $label, $style = "") {
	$url = get_autor_url($professor_id);
	return "<a class='{$style}' href='{$url}'>$label</a>";
}

function expoconcursos_title_update($post_ID, $post_after, $post_before){

	if (get_post_type($post_ID) == 'product') {

		// Altera o nome do forum quando o nome do post é atualizado
		if($post_after->post_title != $post_before->post_title) {

			$forum_id = get_post_meta($post_ID, 'forum_id', true );

			if(!empty( $forum_id )) {
				wp_update_post(array (
					'ID'            => $forum_id,
					'post_title'    => $post_after->post_title,
					'post_name'		=> sanitize_title($post_after->post_title)
				));
			}
		}

		if($post_after->post_status != $post_before->post_status) {

			$forum_id = get_post_meta($post_ID, 'forum_id', true );

			switch ($post_after->post_status) {
				case 'pending' : bbp_close_forum($forum_id); break;
				case 'draft'   : bbp_close_forum($forum_id); break;
				case 'publish' : bbp_open_forum($forum_id); break;
			}
		}
	}
}



add_action( 'post_updated', 'expoconcursos_title_update', 10, 3 );

// add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 10000;' ), 20 );

add_filter( 'wpseo_canonical', 'wpseo_canonical_exclude' );

function wpseo_canonical_exclude( $canonical ) {
	global $post;
	$canonical = false;
	return $canonical;
}

add_filter('show_admin_bar', '__return_false');

add_theme_support( 'title-tag' );

/*
 *  Removes <link rel="prefetch" for WP assets not used in the theme
 * */
function remove_dns_prefetch($hints, $relation_type)
{
    if ('dns-prefetch' === $relation_type) {
        return array_diff(wp_dependencies_unique_hosts(), $hints);
    }
    return $hints;
}

add_filter('wp_resource_hints', 'remove_dns_prefetch', 10, 2);

require_once __DIR__ . '/../vendor/autoload.php';

require_once 'includes/constantes.php';
require_once 'includes/simple_html_dom.php';
require_once 'includes/usuario.php';
require_once 'includes/acesso.php';
require_once 'includes/admin_menu.php';
require_once 'includes/administracao.php';
require_once 'includes/assinatura-sq.php';
require_once 'includes/autor.php';
require_once 'includes/carrinho.php';
require_once 'includes/cidade.php';
require_once 'includes/coaching.php';
require_once 'includes/concurso.php';
require_once 'includes/coordenador.php';
require_once 'includes/cupom.php';
require_once 'includes/email.php';
require_once 'includes/erros.php';
require_once 'includes/excel.php';
require_once 'includes/facebook-pixel.php';
require_once 'includes/folha-dirigida.php';
require_once 'includes/forum.php';
require_once 'includes/geral.php';
require_once 'includes/google.php';
require_once 'includes/imposto.php';
require_once 'includes/log.php';
require_once 'includes/questao.php';
require_once 'includes/mailchimp.php';
require_once 'includes/memcached.php';
require_once 'includes/menubar.php';
require_once 'includes/pacote_antigo.php';
require_once 'includes/pagseguro.php';
require_once 'includes/parceiros.php';
require_once 'includes/pedido.php';
require_once 'includes/wp_post.php';
require_once 'includes/produto.php';
require_once 'includes/recaptchalib.php';
require_once 'includes/routes.php';
require_once 'includes/seo.php';
require_once 'includes/sistema.php';
require_once 'includes/share-buttons.php';
require_once 'includes/tema.php';
require_once 'includes/vendas.php';
require_once 'includes/VerotUpload.php';
require_once 'includes/vimeo.php';
require_once 'includes/woocommerce-fields.php';
require_once 'includes/pdf_assinar.php';

require_once 'includes/libraries/Paginator.php';

require_once 'kcore/deprecated.php';
require_once 'kcore/kcore.php';

require_once 'includes/componentes.php';