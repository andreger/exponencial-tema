<?php 

    get_header();

    KLoader::model("ProdutoModel");
    KLoader::helper("UrlHelper");

    $orderby = $_GET['o'];
    
    $con_name = get_query_var('con_name');

    if(isset($con_name) && $con_name) {

        global $concurso;
        $title = $concurso->post_title;
        
        $url_base = UrlHelper::get_cursos_por_concurso_especifico_paginado_url($con_name);
        
        $filtro = isset($_GET['curso']) ? $_GET['curso'] : "";
        $filtro_param = "";
        if($filtro){
            $filtro_param = "curso=".$filtro."&";
        }
    
        // Montagem da paginação	
        $pg = isset($_GET['pg']) ? $_GET['pg'] : "1";
        $total = ProdutoModel::contar_por_concurso($con_name, null, null, null, null, null, null, $filtro);
        $pattern = $url_base."?{$filtro_param}pg=(:num)";
        $limit = LIMITE_POR_CONCURSO;
        $paginator = new Paginator($total, $limit, $pg, $pattern);
        $offset = $pg ? ($pg - 1) * $limit : 0;
    
        $cursos = ProdutoModel::listar_por_concurso($con_name, null, null, null, null, null, null, $filtro, $offset, $limit);

        echo get_cabecalho_secao('informacoes_cadastro_exponencial.png', 'Cursos - ' . $title);

?>
<div class="container">
	
	<div class="pt-3 col-md-6 offset-md-3 pl-sm-0">
		<div class="p-0 col-12 row ml-auto mr-auto mb-3 mt-3">
			<div class="col p-0 text-right">
			<input class="pesquisa-barra form-control input-pesquisa" type="text" placeholder="Procure pelo nome do curso" value="<?= $filtro ?>" /></div>
			<div class="w-b-l p-0 text-left">
			
				<a class="link-pesquisa" href="#">
					<div class="pesquisa-barra-lupa">
						<i class="fa fa-search mt-2 ml-2 text-white"></i>		
					</div>
				</a>
			
			</div>
		</div>    	
	</div>
	<div class="pt-4 d-none d-md-block col-12 text-right pr-0">
		<a class="pointer" id="pesquisa-linha-3"><img width="50" src="<?= get_tema_image_url('view-linha.png') ?>"></a>
		<a class="pointer" id="pesquisa-grid-3"><img  width="50" src="<?= get_tema_image_url('view-grid.png') ?>"></a>
	</div>
	
	<?php if($cursos) : ?>
			
		<div class="row pt-5">
			<?= $paginator ?>
		</div>
		
		<?= get_concursos_produtos_celulas($cursos, 'nao-pacote', TRUE); ?>
		
		<div class="row pb-5">
			<?= $paginator ?>
		</div>
		
	<?php else: ?>

		<div class="pb-5 text-center">Nenhum curso encontrado</div>
	
	<?php endif; ?>

</div>

<script>
	jQuery(".link-pesquisa").click(function(e){
		var valor = jQuery('.input-pesquisa').val();
		window.location.replace("<?= $url_base ?>?curso="+valor);
	});
	jQuery('.input-pesquisa').keyup(function(e){
		if(e.keyCode == 13)
		{
			window.location.replace("<?= $url_base ?>?curso="+this.value);
		}
	});
</script>

<?php
}    
else {
    echo "<script>window.location.href='/cursos-por-concurso'</script>";
}
?>

<?php get_footer();?>