<?php
/*
Template Name: Change Password
*/
if(!is_user_logged_in()) {
	header('Location: /recuperacao-de-senha/'); 
	exit;
}


$error = null;
$user = wp_get_current_user();

if(isset($_POST['submit'])) {
	$actual_pass = $_POST['actual_pass'];
	$new_pass = $_POST['new_pass'];
	$new_pass_confirmation = $_POST['new_pass_confirmation'];
	
	if($new_pass == $new_pass_confirmation) {
	
		if(wp_check_password($actual_pass, $user->data->user_pass, $user->ID)) {
			wp_set_password($new_pass, $user->ID );
		}
		else {
			$error = "Senha atual incorreta.";
		}
	}
	else {
		$error = "Senha e confirmação precisam ser iguais.";
	}
}

get_header();
?>

<div id="topform">
	<div class="container pt-1">		
		<div class="pb-2"><?php echo get_voltar_link() ?></div>
	
		<?php if($_POST['submit']) : ?>
			<?php if(is_null($error)) : ?>
				<div class="register-success text-center">Sua senha foi alterada</div>
			<?php else : ?>
				<div class="msg-error text-center"><?php echo $error ?></div>
			<?php endif; ?>
		<?php endif ?>
		
		<div class="container pt-1 pt-md-4">
			<h1>Alteração de senha</h1>
		</div>

    	<div class="row mt-5">
				<div class="col-12 col-md-2 col-lg-4"></div>
		    	<div class="col-12 col-md-5 col-lg-4">
	               	<div class="login_form contato">
		        		<form id="change-password-form" method="POST" action="/alteracao-de-senha">
		        			<input class="form-control" type="password" id="actual_pass" name="actual_pass" placeholder="Senha atual"  value=""/>
		        			<input class="mt-2 form-control" type="password" id="new_pass" name="new_pass" placeholder="Nova senha"  value=""/>
                			<input class="mt-2 form-control" type="password" id="new_pass_confirmation" name="new_pass_confirmation" placeholder="Confirmação de nova senha"  value=""/>
                			<div class="mt-3 text-right">
                			<input type="submit" value="Enviar" name="submit" class="btn u-btn-primary"/> 
                			</div>
            			</form>
           			</div>
        		</div>
                <div class="col-12 col-md-3">
	                <div class="loginimage"></div>
	            </div>
           	
        </div>
	 
	</div>
</div>

<?php
if(isset($_POST['submit'])) {
	$email = $_POST['email'];
	
	$user = get_user_by('email', $email);
	if($user) {
		$password = wp_generate_password();
		wp_set_password( $password, $user->ID );
		
	}
}
?>


<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="/wp-content/themes/academy/js/jquery.validate.min.js"></script>

<script>
$().ready(function() {
	$("#change-password-form").validate({
		rules: {
			actual_pass: "required",
			new_pass: {
				required: true,
				minlength: 5
			},
			new_pass_confirmation: {
				required: true,
				minlength: 5,
				equalTo: "#new_pass"
			}
		},
		messages: {
			actual_pass: "Senha atual é obrigatória",
			new_pass: {
				required: "Nova senha é obrigatória",
				minlength: "Nova senha deve ter, no mínimo, 5 caracteres"
			},
			new_pass_confirmation: {
				required: "Confirmação de senha é obrigatória",
				minlength: "Confirmação de senha deve ter, no mínimo, 5 caracteres",
				equalTo: "Confirmação de senha não está igual à senha"
			},
		},
		submitHandler: function(form) {
		    form.submit();
		}
	});
});
</script>	
<?php get_footer(); ?>