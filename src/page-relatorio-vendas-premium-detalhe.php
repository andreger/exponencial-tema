<?php
date_default_timezone_set('America/Sao_Paulo');

tem_acesso(array(
    ADMINISTRADOR
), ACESSO_NEGADO);

ini_set('memory_limit', '-1');
set_time_limit(600);

session_start();

KLoader::model("PremiumModel");
KLoader::model("ProfessorModel");
KLoader::helper("RelatorioVendasPremiumHelper");

// Monta dados para chamadas de modelo
$filtros = [
    "inicio" => $_GET['inicio'] . " 00:00:00",
    "fim" => $_GET['fim'] . " 23:59:59",
    'professor_id' => $_GET['professor_id']
];


$pedidos = PremiumModel::listar_pedidos_com_produto_premium($filtros);
$professores_pedido = [];
$total = [];
$professor = [];

foreach ($pedidos as $pedido) {
    $premium_info =  PremiumModel::get_premium_info($pedido->post_id, date("Ym", strtotime($pedido->ven_data)));
    
    $professores_pedido = PremiumModel::listar_premium_info_items_professores($premium_info->pri_id); 
      
    if($professores_pedido) {
        
        $total[$pedido->post_id] = PremiumModel::get_valor_total_produtos($pedido->post_id, date("Ym", strtotime($pedido->ven_data)));

        foreach($professores_pedido as $professor_pedido) {
            if($professor_pedido->user_id != $_GET['professor_id']) continue;
            
            $percentual = number_format($professor_pedido->pri_item_preco / $total[$pedido->post_id], 5) / 2;
            $item_valor = number_format($percentual * $pedido->ven_valor_venda, 2);
            
            $info = [
                'pedido_id' => $pedido->ven_order_id,
                'pedido_data' => $pedido->ven_data,
                'pedido_valor' => $pedido->ven_valor_venda,
                'item_id' => $professor_pedido->pro_item_id,
                'item_nome' => $professor_pedido->post_title,
                'item_percentual' => $percentual,
                'item_valor' => $item_valor,
            ];
            
            $professor['info'][] = $info;
            $professor['total'] += $item_valor;
        }
    }
}

// print_r($professor);exit;

if(isset($_POST['exportar-xls'])) {
//     header('Content-Type: application/vnd.ms-excel');
//     header('Content-Disposition: attachment;filename="stats.xls"');
    
//     echo RelatorioVendasPremiumHelper::exportar_xls($professores);
//     exit;
} else {
    get_header();
}

?>


<div class="container-fluid pt-1 pt-md-4">
	<div class="mb-5 col-12 text-center text-md-left text-blue">
		<h1>Detalhes de Vendas de Produtos Premium</h1>
	</div>
</div>
<div class="container">

	<div>
    <a class="btn u-btn-blue" href="/relatorio-vendas-premium?inicio=<?= converter_para_ddmmyyyy($_GET['inicio']) ?>&fim=<?= converter_para_ddmmyyyy($_GET['fim']) ?>&filtrar=Filtrar+Consulta"><i class="fa fa-undo"></i> Voltar</a> 
	</div>
	
	<div class="mt-4 mb-4">
	<?php if($professor) :?>	
		<div style="margin: 0 15px">
			<?= RelatorioVendasPremiumHelper::get_table_detalhe($professor); ?>		
		</div>
	<?php endif ?>
	</div>
</div>

<?php get_footer(); ?>