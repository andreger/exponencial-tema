<?php get_header(); ?>
<link rel="stylesheet" href="/wp-content/themes/academy/css/asPieProgress.min.css">
<link rel="stylesheet" href="/wp-content/themes/academy/css/assinaturas21.css">

<!--=== Chegou a assinatura vitalícia ===-->
<section class="container-fluid a21 a21-faq pt-5 pb-5">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-8">
                <div class="a21-faq-pretitulo text-uppercase text-white mt-5">Chegou a assinatura vitalícia</div>

                <h2 class="a21-vantagens-titulo  mb-5 text-white">Paque uma vez e acesse quando quiser!</h2>

                <div class="a21-destaque-texto">Todo o material que você precisa para ser aprovado por preço único.</div>

                <div class="mt-5 mb-4">
                    <a href class="a21-botao text-white">Aproveite <i class="fa fa-chevron-circle-right"></i></a>
                </div>
            </div>

            <div class="col-12 col-md-4">
                <img src="/wp-content/themes/academy/images/assinaturas-2021/destaque.png" class="w-100" />
            </div>
        </div>
    </div>
</section>
<!--=== Fim Chegou a assinatura vitalícia ===-->

<!--=== Conquiste a tão sonhada aprovação ===-->
<section class="container a21 pt-5 pb-5 mb-5">
    <div class="row mt-5 mb-5">
        <div class="col-12 text-center">
            <div class="a21-pretitulo text-uppercase">2022 começa agora</div>
            <h2 class="a21-titulo">Conquiste a tão sonhada aprovação!</h2>
        </div>
    </div>

    <div class="row">
        <div class="col-12 col-sm-6 col-md-3 text-white text-center mb-5">
            <div class="a21-box a21-box1">
                <div class="a21-box-titulo text-uppercase mt-4 mb-4">Assinatura<br>por Área</div>
                <div class="a21-box-preco-original">De: R$000,00</div>
                <div class="a21-box-preco font-weight-bold mt-2">12x R$00,00</div>
                <div class="mt-5 mb-4">
                    <a href class="a21-botao text-white">Aproveite <i class="fa fa-chevron-circle-right"></i></a>
                </div>
            </div>
        </div>

        <div class="col-12 col-sm-6 col-md-3 text-white text-center mb-5">
            <div class="a21-box a21-box2">
                <div class="a21-box-titulo text-uppercase mt-4 mb-4">Assinatura<br>todas as áreas</div>
                <div class="a21-box-preco-original">De: R$000,00</div>
                <div class="a21-box-preco font-weight-bold mt-2">12x R$00,00</div>
                <div class="mt-5 mb-4">
                    <a href class="a21-botao text-white">Aproveite <i class="fa fa-chevron-circle-right"></i></a>
                </div>
            </div>
        </div>

        <div class="col-12 col-sm-6 col-md-3 text-white text-center mb-5">
            <div class="a21-box a21-box3">
                <div class="a21-box-titulo text-uppercase mt-4 mb-4">Assinatura +<br>metas</div>
                <div class="a21-box-preco-original">De: R$000,00</div>
                <div class="a21-box-preco font-weight-bold mt-2">12x R$00,00</div>
                <div class="mt-5 mb-4">
                    <a href class="a21-botao text-white">Aproveite <i class="fa fa-chevron-circle-right"></i></a>
                </div>
            </div>
        </div>

        <div class="col-12 col-sm-6 col-md-3 text-white text-center mb-5">
            <div class="a21-box a21-box4">
                <div class="a21-box-titulo text-uppercase mt-4 mb-4">Assinatura +<br>coaching</div>
                <div class="a21-box-preco-original">De: R$000,00</div>
                <div class="a21-box-preco font-weight-bold mt-2">12x R$00,00</div>
                <div class="mt-5 mb-4">
                    <a href class="a21-botao text-white">Aproveite <i class="fa fa-chevron-circle-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>
<!--=== Fim Conquiste a tão sonhada aprovação ===-->

<!--=== O que está incluso? ===-->
<section class="container-fluid a21 a21-tabela pt-5 pb-5">
    <div class="container">
        <div class="row mt-5 mb-5">
            <div class="col-12 text-center">
                <h2 class="a21-titulo">O que está incluso?</h2>
            </div>
        </div>

        <div class="tabela d-none d-md-block">
            <div class="row text-center a21-tabela-linha-azul">
                <div class="col-2"></div>
                <div class="col-2 a21-tabela-header">Sistema de<br>Questões</div>
                <div class="col-2 a21-tabela-header">Assinatura por<br>área</div>
                <div class="col-2 a21-tabela-header">Assinatura todas<br>as áreas</div>
                <div class="col-2 a21-tabela-header">Assinatura +<br>Metas</div>
                <div class="col-2 a21-tabela-header">Assinaturas +<br>Coaching</div>
            </div>

            <div class="row a21-tabela-linha-cinza">
                <div class="col-2 a21-tabela-label">Cursos 5.0</div>
                <div class="col-2 a21-tabela-dash">-</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
            </div>

            <div class="row a21-tabela-linha-cinza">
                <div class="col-2 a21-tabela-label">PDF Esquematizado</div>
                <div class="col-2 a21-tabela-dash">-</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
            </div>

            <div class="row a21-tabela-linha-cinza">
                <div class="col-2 a21-tabela-label">Mapas Mentais</div>
                <div class="col-2 a21-tabela-dash">-</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
            </div>

            <div class="row a21-tabela-linha-cinza">
                <div class="col-2 a21-tabela-label">Fórum</div>
                <div class="col-2 a21-tabela-dash">-</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
            </div>

            <div class="row a21-tabela-linha-cinza">
                <div class="col-2 a21-tabela-label">Resumos</div>
                <div class="col-2 a21-tabela-dash">-</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
            </div>

            <div class="row a21-tabela-linha-cinza">
                <div class="col-2 a21-tabela-label">Videoaulas</div>
                <div class="col-2 a21-tabela-dash">-</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
            </div>

            <div class="row a21-tabela-linha-azul">
                <div class="col-2 a21-tabela-label">Acesso Irrestrito*</div>
                <div class="col-2 a21-tabela-dash">-</div>
                <div class="col-2 a21-tabela-dash">Por área</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
            </div>

            <div class="row a21-tabela-linha-cinza">
                <div class="col-2 a21-tabela-label"><strong>Sistema de Questões</strong></div>
                <div class="col-2 a21-tabela-dash">12x R$ 19,90</div>
                <div class="col-2 a21-tabela-bonus"><s>12X R$ 19,90</s><br><span class="font-weight-bold a21-tabela-bonus-texto">Bônus!</span></div>
                <div class="col-2 a21-tabela-bonus"><s>12X R$ 19,90</s><br><span class="font-weight-bold a21-tabela-bonus-texto">Bônus!</span></div>
                <div class="col-2 a21-tabela-bonus"><s>12X R$ 19,90</s><br><span class="font-weight-bold a21-tabela-bonus-texto">Bônus!</span></div>
                <div class="col-2 a21-tabela-bonus"><s>12X R$ 19,90</s><br><span class="font-weight-bold a21-tabela-bonus-texto">Bônus!</span></div>
            </div>

            <div class="row a21-tabela-linha-cinza">
                <div class="col-2 a21-tabela-label">350.000 Questões<br>Comentadas</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
            </div>

            <div class="row a21-tabela-linha-cinza">
                <div class="col-2 a21-tabela-label">Análise de<br>Desempenho</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
            </div>

            <div class="row a21-tabela-linha-cinza">
                <div class="col-2 a21-tabela-label">Questões Inéditas</div>
                <div class="col-2 a21-tabela-dash">-</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
            </div>

            <div class="row a21-tabela-linha-cinza">
                <div class="col-2 a21-tabela-label">Caderno de Questões</div>
                <div class="col-2 a21-tabela-dash">-</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
            </div>

            <div class="row a21-tabela-linha-cinza">
                <div class="col-2 a21-tabela-label">Simulados</div>
                <div class="col-2 a21-tabela-dash">-</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
            </div>

            <div class="row a21-tabela-linha-azul">
                <div class="col-2 a21-tabela-label">Guia de estudos<br>por questões</div>
                <div class="col-2 a21-tabela-dash">-</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
            </div>

            <div class="row a21-tabela-linha-cinza">
                <div class="col-2 a21-tabela-label">Raio-x</div>
                <div class="col-2 a21-tabela-dash">-</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
            </div>

            <div class="row a21-tabela-linha-cinza">
                <div class="col-2 a21-tabela-label">Cronograma de<br>Estudos</div>
                <div class="col-2 a21-tabela-dash">-</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
            </div>

            <div class="row a21-tabela-linha-azul">
                <div class="col-2 a21-tabela-label">Edital<br>Verticalizado</div>
                <div class="col-2 a21-tabela-dash">-</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
            </div>

            <div class="row a21-tabela-linha-cinza">
                <div class="col-2 a21-tabela-label">Meta de Estudo<br>Semanal por Área</div>
                <div class="col-2 a21-tabela-dash">-</div>
                <div class="col-2 a21-tabela-dash">-</div>
                <div class="col-2 a21-tabela-dash">-</div>
                <div class="col-2 a21-tabela-bonus"><s>R$ 997,00</s><br><span class="font-weight-bold a21-tabela-bonus-texto">Bônus!</span></div>
                <div class="col-2 a21-tabela-bonus"><s>R$ 997,00</s><br><span class="font-weight-bold a21-tabela-bonus-texto">Bônus!</span></div>
            </div>

            <div class="row a21-tabela-linha-cinza">
                <div class="col-2 a21-tabela-label">Módulo Técnicas<br>de Estudos</div>
                <div class="col-2 a21-tabela-dash">-</div>
                <div class="col-2 a21-tabela-dash">-</div>
                <div class="col-2 a21-tabela-dash">-</div>
                <div class="col-2 a21-tabela-check">✓</div>
                <div class="col-2 a21-tabela-check">✓</div>
            </div>

            <div class="row a21-tabela-linha-cinza">
                <div class="col-2 a21-tabela-label">Ciclo de Estudos<br>Personalizado</div>
                <div class="col-2 a21-tabela-dash">-</div>
                <div class="col-2 a21-tabela-dash">-</div>
                <div class="col-2 a21-tabela-dash">-</div>
                <div class="col-2 a21-tabela-dash">-</div>
                <div class="col-2 a21-tabela-check">✓</div>
            </div>

            <div class="row a21-tabela-linha-cinza">
                <div class="col-2 a21-tabela-label">Acompanhamento<br>de Coaching</div>
                <div class="col-2 a21-tabela-dash">-</div>
                <div class="col-2 a21-tabela-dash">-</div>
                <div class="col-2 a21-tabela-dash">-</div>
                <div class="col-2 a21-tabela-dash">-</div>
                <div class="col-2 a21-tabela-check">✓</div>
            </div>

            <div class="row a21-tabela-linha-cinza">
                <div class="col-2 a21-tabela-label">#EstudeCerto</div>
                <div class="col-2 a21-tabela-dash">-</div>
                <div class="col-2 a21-tabela-dash">-</div>
                <div class="col-2 a21-tabela-dash">-</div>
                <div class="col-2 a21-tabela-bonus"><s>R$ 997,00</s><br><span class="font-weight-bold a21-tabela-bonus-texto">Bônus!</span></div>
                <div class="col-2 a21-tabela-bonus"><s>R$ 997,00</s><br><span class="font-weight-bold a21-tabela-bonus-texto">Bônus!</span></div>
            </div>

            <div class="row a21-tabela-linha-cinza">
                <div class="col-2 a21-tabela-label">Assinatura Mensal</div>
                <div class="col-2 a21-tabela-dash">-</div>
                <div class="col-2 a21-tabela-dash">-</div>
                <div class="col-2 a21-tabela-dash">-</div>
                <div class="col-2 a21-tabela-dash">-</div>
                <div class="col-2 a21-tabela-check">✓</div>
            </div>
        </div>

        <div class="tabela-pequena d-block d-md-none">
            <div class="mb-5">
                <div class="row text-center a21-tabela-linha-azul">
                    <div class="col-12 a21-tabela-header">Sistema de Questões</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Cursos 5.0</div>
                    <div class="col-6 a21-tabela-dash">-</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">PDF Esquematizado</div>
                    <div class="col-6 a21-tabela-dash">-</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Mapas Mentais</div>
                    <div class="col-6 a21-tabela-dash">-</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Fórum</div>
                    <div class="col-6 a21-tabela-dash">-</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Resumos</div>
                    <div class="col-6 a21-tabela-dash">-</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Videoaulas</div>
                    <div class="col-6 a21-tabela-dash">-</div>
                </div>

                <div class="row a21-tabela-linha-azul">
                    <div class="col-6 a21-tabela-label">Acesso Irrestrito*</div>
                    <div class="col-6 a21-tabela-dash">-</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label"><strong>Sistema de Questões</strong></div>
                    <div class="col-6 a21-tabela-dash">12x R$ 19,90</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">350.000 Questões<br>Comentadas</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Análise de<br>Desempenho</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Questões Inéditas</div>
                    <div class="col-6 a21-tabela-dash">-</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Caderno de Questões</div>
                    <div class="col-6 a21-tabela-dash">-</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Simulados</div>
                    <div class="col-6 a21-tabela-dash">-</div>
                </div>

                <div class="row a21-tabela-linha-azul">
                    <div class="col-6 a21-tabela-label">Guia de estudos<br>por questões</div>
                    <div class="col-6 a21-tabela-dash">-</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Raio-x</div>
                    <div class="col-6 a21-tabela-dash">-</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Cronograma de<br>Estudos</div>
                    <div class="col-6 a21-tabela-dash">-</div>
                </div>

                <div class="row a21-tabela-linha-azul">
                    <div class="col-6 a21-tabela-label">Edital<br>Verticalizado</div>
                    <div class="col-6 a21-tabela-dash">-</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Meta de Estudo<br>Semanal por Área</div>
                    <div class="col-6 a21-tabela-dash">-</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Módulo Técnicas<br>de Estudos</div>
                    <div class="col-6 a21-tabela-dash">-</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Ciclo de Estudos<br>Personalizado</div>
                    <div class="col-6 a21-tabela-dash">-</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Acompanhamento<br>de Coaching</div>
                    <div class="col-6 a21-tabela-dash">-</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">#EstudeCerto</div>
                    <div class="col-6 a21-tabela-dash">-</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Assinatura Mensal</div>
                    <div class="col-6 a21-tabela-dash">-</div>
                </div>
            </div>
            <div class="mb-5">
                <div class="row text-center a21-tabela-linha-azul">
                    <div class="col-12 a21-tabela-header">Assinatura por área</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Cursos 5.0</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">PDF Esquematizado</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Mapas Mentais</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Fórum</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Resumos</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Videoaulas</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-azul">
                    <div class="col-6 a21-tabela-label">Acesso Irrestrito*</div>
                    <div class="col-6 a21-tabela-dash">Por área</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label"><strong>Sistema de Questões</strong></div>
                    <div class="col-6 a21-tabela-bonus"><s>12X R$ 19,90</s><br><span class="font-weight-bold a21-tabela-bonus-texto">Bônus!</span></div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">350.000 Questões<br>Comentadas</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Análise de<br>Desempenho</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Questões Inéditas</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Caderno de Questões</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Simulados</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-azul">
                    <div class="col-6 a21-tabela-label">Guia de estudos<br>por questões</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Raio-x</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Cronograma de<br>Estudos</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-azul">
                    <div class="col-6 a21-tabela-label">Edital<br>Verticalizado</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Meta de Estudo<br>Semanal por Área</div>
                    <div class="col-6 a21-tabela-dash">-</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Módulo Técnicas<br>de Estudos</div>
                    <div class="col-6 a21-tabela-dash">-</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Ciclo de Estudos<br>Personalizado</div>
                    <div class="col-6 a21-tabela-dash">-</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Acompanhamento<br>de Coaching</div>
                    <div class="col-6 a21-tabela-dash">-</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">#EstudeCerto</div>
                    <div class="col-6 a21-tabela-dash">-</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Assinatura Mensal</div>
                    <div class="col-6 a21-tabela-dash">-</div>
                </div>
            </div>
            <div class="mb-5">
                <div class="row text-center a21-tabela-linha-azul">
                    <div class="col-12 a21-tabela-header">Assinatura todas as áreas</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Cursos 5.0</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">PDF Esquematizado</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Mapas Mentais</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Fórum</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Resumos</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Videoaulas</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-azul">
                    <div class="col-6 a21-tabela-label">Acesso Irrestrito*</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label"><strong>Sistema de Questões</strong></div>
                    <div class="col-6 a21-tabela-bonus"><s>12X R$ 19,90</s><br><span class="font-weight-bold a21-tabela-bonus-texto">Bônus!</span></div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">350.000 Questões<br>Comentadas</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Análise de<br>Desempenho</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Questões Inéditas</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Caderno de Questões</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Simulados</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-azul">
                    <div class="col-6 a21-tabela-label">Guia de estudos<br>por questões</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Raio-x</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Cronograma de<br>Estudos</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-azul">
                    <div class="col-6 a21-tabela-label">Edital<br>Verticalizado</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Meta de Estudo<br>Semanal por Área</div>
                    <div class="col-6 a21-tabela-dash">-</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Módulo Técnicas<br>de Estudos</div>
                    <div class="col-6 a21-tabela-dash">-</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Ciclo de Estudos<br>Personalizado</div>
                    <div class="col-6 a21-tabela-dash">-</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Acompanhamento<br>de Coaching</div>
                    <div class="col-6 a21-tabela-dash">-</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">#EstudeCerto</div>
                    <div class="col-6 a21-tabela-dash">-</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Assinatura Mensal</div>
                    <div class="col-6 a21-tabela-dash">-</div>
                </div>
            </div>
            <div class="mb-5">
                <div class="row text-center a21-tabela-linha-azul">
                    <div class="col-12 a21-tabela-header">Assinatura + Metas</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Cursos 5.0</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">PDF Esquematizado</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Mapas Mentais</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Fórum</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Resumos</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Videoaulas</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-azul">
                    <div class="col-6 a21-tabela-label">Acesso Irrestrito*</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label"><strong>Sistema de Questões</strong></div>
                    <div class="col-6 a21-tabela-bonus"><s>12X R$ 19,90</s><br><span class="font-weight-bold a21-tabela-bonus-texto">Bônus!</span></div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">350.000 Questões<br>Comentadas</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Análise de<br>Desempenho</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Questões Inéditas</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Caderno de Questões</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Simulados</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-azul">
                    <div class="col-6 a21-tabela-label">Guia de estudos<br>por questões</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Raio-x</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Cronograma de<br>Estudos</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-azul">
                    <div class="col-6 a21-tabela-label">Edital<br>Verticalizado</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Meta de Estudo<br>Semanal por Área</div>
                    <div class="col-6 a21-tabela-bonus"><s>R$ 997,00</s><br><span class="font-weight-bold a21-tabela-bonus-texto">Bônus!</span></div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Módulo Técnicas<br>de Estudos</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Ciclo de Estudos<br>Personalizado</div>
                    <div class="col-6 a21-tabela-dash">-</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Acompanhamento<br>de Coaching</div>
                    <div class="col-6 a21-tabela-dash">-</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">#EstudeCerto</div>
                    <div class="col-6 a21-tabela-bonus"><s>R$ 997,00</s><br><span class="font-weight-bold a21-tabela-bonus-texto">Bônus!</span></div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Assinatura Mensal</div>
                    <div class="col-6 a21-tabela-dash">-</div>
                </div>
            </div>
            <div class="mb-5">
                <div class="row text-center a21-tabela-linha-azul">
                    <div class="col-12 a21-tabela-header">Assinaturas + Coaching</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Cursos 5.0</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">PDF Esquematizado</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Mapas Mentais</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Fórum</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Resumos</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Videoaulas</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-azul">
                    <div class="col-6 a21-tabela-label">Acesso Irrestrito*</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label"><strong>Sistema de Questões</strong></div>
                    <div class="col-6 a21-tabela-bonus"><s>12X R$ 19,90</s><br><span class="font-weight-bold a21-tabela-bonus-texto">Bônus!</span></div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">350.000 Questões<br>Comentadas</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Análise de<br>Desempenho</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Questões Inéditas</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Caderno de Questões</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Simulados</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-azul">
                    <div class="col-6 a21-tabela-label">Guia de estudos<br>por questões</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Raio-x</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Cronograma de<br>Estudos</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-azul">
                    <div class="col-6 a21-tabela-label">Edital<br>Verticalizado</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Meta de Estudo<br>Semanal por Área</div>
                    <div class="col-6 a21-tabela-bonus"><s>R$ 997,00</s><br><span class="font-weight-bold a21-tabela-bonus-texto">Bônus!</span></div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Módulo Técnicas<br>de Estudos</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Ciclo de Estudos<br>Personalizado</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Acompanhamento<br>de Coaching</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">#EstudeCerto</div>
                    <div class="col-6 a21-tabela-bonus"><s>R$ 997,00</s><br><span class="font-weight-bold a21-tabela-bonus-texto">Bônus!</span></div>
                </div>

                <div class="row a21-tabela-linha-cinza">
                    <div class="col-6 a21-tabela-label">Assinatura Mensal</div>
                    <div class="col-6 a21-tabela-check">✓</div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--=== Fim O que está incluso? ===-->

<!--=== Satisfação Garantida ===-->
<section class="container-fluid a21 a21-teste">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-7">
                <img src="/wp-content/themes/academy/images/assinaturas-2021/satisfacao-garantida.png" class="w-100" />
            </div>

            <div class="col-12 col-md-5">
                <h2 class="a21-teste-pretitulo mt-5 mb-3">Teste por 30 dias</h2>
                <div class="a21-teste-titulo mb-5">Satisfação<br>Garantida ou<br>seu Dinheiro de<br>Volta!</div>
                <div class="a21-teste-texto">Você tem até 30 dias da aquisição para solicitar o reembolso integral.</div>
                <div class="a21-teste-nota mt-3 mb-5">*Confira as regras no <a href="/termos-de-uso">termo de uso</a>.</div>
            </div>
        </div>
    </div>
</section>
<!--=== Fim Satisfação Garantida ===-->

<!--=== Conheça as vantagens ===-->
<section class="container a21 a21-vantagens mt-5">
    <div class="row">
        <div class="col-12 col-md-6">
            <img src="/wp-content/themes/academy/images/assinaturas-2021/vantagens.png" class="w-100" />
        </div>

        <div class="col-12 col-md-6">
            <h2 class="a21-vantagens-titulo mt-5 mb-5">Conheça as<br>Vantagens</h2>

            <div class="col-12 a21-vantagens-box mb-2">
                <a href="#" class="a21-vantagens-link">
                    <div class="d-flex justify-content-between a21-vantagens-item-titulo">
                        <div>Metodologia Exponencial</div>
                        <div>
                            <i class="fa fa-plus-circle a21-vantagens-bullet"></i>
                            <i class="fa fa-minus-circle a21-vantagens-bullet" style="display: none"></i>
                        </div>
                    </div>
                </a>

                <div class="a21-vantagens-item-descricao mt-2" style="display: none">
                    Your content goes here. Edit or remove this text inline or in the module Content settings.
                    You can also style every aspect of this content in the module Design settings and even apply
                    custom CSS to this text in the module Advanced settings.
                </div>
            </div>

            <div class="col-12 a21-vantagens-box mb-2">
                <a href="#" class="a21-vantagens-link">
                    <div class="d-flex justify-content-between a21-vantagens-item-titulo">
                        <div>+5.000 Cursos Online</div>
                        <div>
                            <i class="fa fa-plus-circle a21-vantagens-bullet"></i>
                            <i class="fa fa-minus-circle a21-vantagens-bullet" style="display: none"></i>
                        </div>
                    </div>
                </a>

                <div class="a21-vantagens-item-descricao mt-2" style="display: none">
                    Your content goes here. Edit or remove this text inline or in the module Content settings.
                    You can also style every aspect of this content in the module Design settings and even apply
                    custom CSS to this text in the module Advanced settings.
                </div>
            </div>

            <div class="col-12 a21-vantagens-box mb-2">
                <a href="#" class="a21-vantagens-link">
                    <div class="d-flex justify-content-between a21-vantagens-item-titulo">
                        <div>Download Ilimitado do PDF</div>
                        <div>
                            <i class="fa fa-plus-circle a21-vantagens-bullet"></i>
                            <i class="fa fa-minus-circle a21-vantagens-bullet" style="display: none"></i>
                        </div>
                    </div>
                </a>

                <div class="a21-vantagens-item-descricao mt-2" style="display: none">
                    Your content goes here. Edit or remove this text inline or in the module Content settings.
                    You can also style every aspect of this content in the module Design settings and even apply
                    custom CSS to this text in the module Advanced settings.
                </div>
            </div>

            <div class="col-12 a21-vantagens-box mb-2">
                <a href="#" class="a21-vantagens-link">
                    <div class="d-flex justify-content-between a21-vantagens-item-titulo">
                        <div>Atualização pós-edital</div>
                        <div>
                            <i class="fa fa-plus-circle a21-vantagens-bullet"></i>
                            <i class="fa fa-minus-circle a21-vantagens-bullet" style="display: none"></i>
                        </div>
                    </div>
                </a>

                <div class="a21-vantagens-item-descricao mt-2" style="display: none">
                    Your content goes here. Edit or remove this text inline or in the module Content settings.
                    You can also style every aspect of this content in the module Design settings and even apply
                    custom CSS to this text in the module Advanced settings.
                </div>
            </div>

            <div class="col-12 a21-vantagens-box mb-2">
                <a href="#" class="a21-vantagens-link">
                    <div class="d-flex justify-content-between a21-vantagens-item-titulo">
                        <div>Visualização ilimitada de Vídeos</div>
                        <div>
                            <i class="fa fa-plus-circle a21-vantagens-bullet"></i>
                            <i class="fa fa-minus-circle a21-vantagens-bullet" style="display: none"></i>
                        </div>
                    </div>
                </a>

                <div class="a21-vantagens-item-descricao mt-2" style="display: none">
                    Your content goes here. Edit or remove this text inline or in the module Content settings.
                    You can also style every aspect of this content in the module Design settings and even apply
                    custom CSS to this text in the module Advanced settings.
                </div>
            </div>
        </div>
    </div>
</section>
<!--=== Fim Conheça as vantagens ===-->

<!--=== Cursos 5.0: Sem Enrolação ===-->
<section class="container a21 a21-vantagens mt-5 mb-5">
    <div class="row">
        <div class="col-12 col-md-6">
            <h2 class="a21-vantagens-titulo mt-5 mb-5">Cursos 5.0: Sem<br>Enrolação</h2>

            <div class="col-12 a21-vantagens-box mb-2">
                <a href="#" class="a21-vantagens-link">
                    <div class="d-flex justify-content-between a21-vantagens-item-titulo">
                        <div>Videoaulas</div>
                        <div>
                            <i class="fa fa-plus-circle a21-vantagens-bullet"></i>
                            <i class="fa fa-minus-circle a21-vantagens-bullet" style="display: none"></i>
                        </div>
                    </div>
                </a>

                <div class="a21-vantagens-item-descricao mt-2" style="display: none">
                    Your content goes here. Edit or remove this text inline or in the module Content settings.
                    You can also style every aspect of this content in the module Design settings and even apply
                    custom CSS to this text in the module Advanced settings.
                </div>
            </div>

            <div class="col-12 a21-vantagens-box mb-2">
                <a href="#" class="a21-vantagens-link">
                    <div class="d-flex justify-content-between a21-vantagens-item-titulo">
                        <div>PDF's</div>
                        <div>
                            <i class="fa fa-plus-circle a21-vantagens-bullet"></i>
                            <i class="fa fa-minus-circle a21-vantagens-bullet" style="display: none"></i>
                        </div>
                    </div>
                </a>

                <div class="a21-vantagens-item-descricao mt-2" style="display: none">
                    Your content goes here. Edit or remove this text inline or in the module Content settings.
                    You can also style every aspect of this content in the module Design settings and even apply
                    custom CSS to this text in the module Advanced settings.
                </div>
            </div>

            <div class="col-12 a21-vantagens-box mb-2">
                <a href="#" class="a21-vantagens-link">
                    <div class="d-flex justify-content-between a21-vantagens-item-titulo">
                        <div>Resumos</div>
                        <div>
                            <i class="fa fa-plus-circle a21-vantagens-bullet"></i>
                            <i class="fa fa-minus-circle a21-vantagens-bullet" style="display: none"></i>
                        </div>
                    </div>
                </a>

                <div class="a21-vantagens-item-descricao mt-2" style="display: none">
                    Your content goes here. Edit or remove this text inline or in the module Content settings.
                    You can also style every aspect of this content in the module Design settings and even apply
                    custom CSS to this text in the module Advanced settings.
                </div>
            </div>

            <div class="col-12 a21-vantagens-box mb-2">
                <a href="#" class="a21-vantagens-link">
                    <div class="d-flex justify-content-between a21-vantagens-item-titulo">
                        <div>Caderno de Questões</div>
                        <div>
                            <i class="fa fa-plus-circle a21-vantagens-bullet"></i>
                            <i class="fa fa-minus-circle a21-vantagens-bullet" style="display: none"></i>
                        </div>
                    </div>
                </a>

                <div class="a21-vantagens-item-descricao mt-2" style="display: none">
                    Your content goes here. Edit or remove this text inline or in the module Content settings.
                    You can also style every aspect of this content in the module Design settings and even apply
                    custom CSS to this text in the module Advanced settings.
                </div>
            </div>

            <div class="col-12 a21-vantagens-box mb-2">
                <a href="#" class="a21-vantagens-link">
                    <div class="d-flex justify-content-between a21-vantagens-item-titulo">
                        <div>Mapas Mentais</div>
                        <div>
                            <i class="fa fa-plus-circle a21-vantagens-bullet"></i>
                            <i class="fa fa-minus-circle a21-vantagens-bullet" style="display: none"></i>
                        </div>
                    </div>
                </a>

                <div class="a21-vantagens-item-descricao mt-2" style="display: none">
                    Your content goes here. Edit or remove this text inline or in the module Content settings.
                    You can also style every aspect of this content in the module Design settings and even apply
                    custom CSS to this text in the module Advanced settings.
                </div>
            </div>
        </div>

        <div class="col-12 col-md-6">
            <img src="/wp-content/themes/academy/images/assinaturas-2021/cursos50.png" class="w-100" />
        </div>

    </div>
</section>
<!--=== Fim Cursos 5.0: Sem Enrolação ===-->

<!--=== Countdown ===-->
<!-- === Definir a data fim no formato AAAA/MM/DD HH:mm:ss === -->
<section class="container-fluid a21 a21-contador" id="countdown" data-date="2021/12/31 23:59:59">
    <div class="container text-center text-white pt-5 pb-4">
        <div class="row">
            <div class="col-12 a21-contador-titulo mb-4">
                Oferta por Tempo Limitado
            </div>
        </div>

        <div class="row mb-5">
            <div class="col-12 offset-md-2 col-md-2 mb-5">
                <div class="days a21-contador-numero">00</div>
                <div class="days_text a21-contador-label">00</div>
            </div>
            <div class="col-12 col-md-2 mb-5">
                <div class="hours a21-contador-numero">00</div>
                <div class="hours_text a21-contador-label">00</div>
            </div>
            <div class="col-12 col-md-2 mb-5">
                <div class="minutes a21-contador-numero">00</div>
                <div class="minutes_text a21-contador-label">00</div>
            </div>
            <div class="col-12 col-md-2 mb-5">
                <div class="seconds a21-contador-numero">00</div>
                <div class="seconds_text a21-contador-label">00</div>
            </div>
        </div>
    </div>
</section>
<!--=== Fim Countdown ===-->

<!--=== Alto índice de aprovação ===-->
<section class="container-fluid a21 a21-numeros pt-5 pb-5">
    <div class="container">
        <div class="row mt-5 mb-5">
            <div class="col-12 text-center">
                <div class="a21-pretitulo text-uppercase">Veja nossos números</div>
                <h2 class="a21-titulo">Alto índice de aprovação</h2>
            </div>
        </div>

        <div class="row mt-5 mb-5">
            <div class="col-12 offset-md-1 col-md-2">
                <div class="circle text-center" id="circles-1"></div>
                <div class="a21-numeros-texto text-center mb-5">SEFAZ CE</div>
            </div>
            <div class="col-12 col-md-2">
                <div class="circle text-center" id="circles-2"></div>
                <div class="a21-numeros-texto text-center mb-5">CGM Niterói</div>
            </div>
            <div class="col-12 col-md-2">
                <div class="circle text-center" id="circles-3"></div>
                <div class="a21-numeros-texto text-center mb-5">SEFAZ DF</div>
            </div>
            <div class="col-12 col-md-2">
                <div class="circle text-center" id="circles-4"></div>
                <div class="a21-numeros-texto text-center mb-5">TCM BA</div>
            </div>
            <div class="col-12 col-md-2">
                <div class="circle text-center" id="circles-5"></div>
                <div class="a21-numeros-texto text-center mb-5">TCE RS</div>
            </div>
        </div>

        <div class="row mt-5 mb-5">
            <div class="col-12 ">
                <div class="text-center">
                    <a href="/resultados" class="a21-veja-mais">Veja mais</a>
                </div>
            </div>
        </div>

    </div>
</section>
<!--=== Fim Alto índice de aprovação ===-->

<!--=== Furei a fila com o Exponencial ===-->
<section class="container a21 pt-5 pb-5 mb-5">
    <div class="row mt-5 mb-5">
        <div class="col-12 text-center">
            <div class="a21-pretitulo text-uppercase">DEPOIMENTO DOS APROVADOS</div>
            <h2 class="a21-titulo">Furei a fila com o Exponencial</h2>
        </div>
    </div>

    <div class="row">
        <div class="col-12 col-sm-6 mb-5 a21-depoimentos-externo">
            <div class="row a21-depoimentos-box">
                <div class="col-12 col-md-3 pl-2">
                    <div class="a21-depoimentos-foto"
                         style='background-image: url("/wp-content/themes/academy/images/assinaturas-2021/depoimento1.jpg");'>
                    </div>
                </div>

                <div class="col-12 col-md-9">
                    <div class="a21-depoimentos-box-texto">
                        O Exponencial me ajudou com questões que disponibilizou pelo Sistema de Questões,
                        bem como por meio de simulado. Não conhecia o Exponencial, porém, se eu tivesse conhecido antes,
                        teria adotado desde o início para incrementar na minha preparação.
                    </div>
                    <div class="a21-depoimentos-box-nome mt-5">
                        Sidney Anderson Teixeira
                    </div>
                    <div class="a21-depoimentos-box-cargo">
                        Aprovado para Investigador na PC-PA, 2021
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 col-sm-6 mb-5 a21-depoimentos-externo">
            <div class="row a21-depoimentos-box">
                <div class="col-12 col-md-3 pl-2">
                    <div class="a21-depoimentos-foto"
                         style='background-image: url("/wp-content/themes/academy/images/assinaturas-2021/depoimento2.jpg");'>
                    </div>
                </div>

                <div class="col-12 col-md-9">
                    <div class="a21-depoimentos-box-texto">
                        Foi fundamental pela qualidade do material e principalmente pela qualidade da seleção no
                        conteúdo a ser abordado. Quem estuda sabe a quantidade de matérias que precisamos dominar.
                        O Exponencial, contudo, foi cirúrgico no destaque dos pontos mais relevantes.
                    </div>
                    <div class="a21-depoimentos-box-nome mt-5">
                        Rodrigo Antônio da Cunha
                    </div>
                    <div class="a21-depoimentos-box-cargo">
                        1º lugar para Auditor no TCE-RS, 2018
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 col-sm-6 mb-5 a21-depoimentos-externo">
            <div class="row a21-depoimentos-box">
                <div class="col-12 col-md-3 pl-2">
                    <div class="a21-depoimentos-foto"
                         style='background-image: url("/wp-content/themes/academy/images/assinaturas-2021/depoimento3.jpg");'>
                    </div>
                </div>

                <div class="col-12 col-md-9">
                    <div class="a21-depoimentos-box-texto">
                        O Exponencial apareceu principalmente por causa dos resumos muito bem organizados.
                        E digo que na matéria de Análise da Informação foi determinante para minha aprovação no TCM-BA,
                        pois trata-se de uma matéria de difícil compreensão e que eu só comecei a entender após o material do Exponencial.
                    </div>
                    <div class="a21-depoimentos-box-nome mt-5">
                        Jane SIlva
                    </div>
                    <div class="a21-depoimentos-box-cargo">
                        TCE-PA, TRT8, TJ-PE e TCM-BA, 2018
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 col-sm-6 mb-5 a21-depoimentos-externo">
            <div class="row a21-depoimentos-box">
                <div class="col-12 col-md-3 pl-2">
                    <div class="a21-depoimentos-foto"
                         style='background-image: url("/wp-content/themes/academy/images/assinaturas-2021/depoimento4.jpg");'>
                    </div>
                </div>

                <div class="col-12 col-md-9">
                    <div class="a21-depoimentos-box-texto">
                        Eu usava muito os esquemas do Exponencial. Pegava, recortava todos os esquemas (mesmo que estivesse
                        utilizando outro material como base), imprimia e isso tudo já me economizava o tempo de ter que
                        escrever as informações que estavam ali. Aí eu ia anotando naqueles esquemas
                    </div>
                    <div class="a21-depoimentos-box-nome mt-5">
                        Dhiennifer Ferreira
                    </div>
                    <div class="a21-depoimentos-box-cargo">
                        TCE-PE e PB, SEFAZ-GO e SC e ISS Manaus
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="text-center">
        <a href='/depoimentos' class="a21-veja-mais">Veja mais</a>
    </div>
</section>
<!--=== Fim CFurei a fila com o Exponencial ===-->

<!--=== Você também será aprovado! ===-->
<section class="container-fluid a21 a21-numeros pt-5 pb-5">
    <div class="container">
        <div class="row mt-5 mb-5">
            <div class="col-12 text-center">
                <h2 class="a21-titulo">Você também será aprovado!</h2>
            </div>
        </div>

        <div class="row mt-5 mb-5">
            <div class="col-12 col-md-4 mb-5">
                <iframe width="325" height="183" src="https://www.youtube.com/embed/-fICKDE6TsU"
                        title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
            </div>
            <div class="col-12 col-md-4 mb-5">
                <iframe width="325" height="183" src="https://www.youtube.com/embed/JXpG3wSLII4"
                        title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
            </div>
            <div class="col-12 col-md-4 mb-5">
                <iframe width="325" height="183" src="https://www.youtube.com/embed/OvVY25AwiNU"
                        title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
            </div>
        </div>

        <div class="row mt-5 mb-5">
            <div class="col-12 ">
                <div class="text-center">
                    <a href="https://www.youtube.com/playlist?list=PLva2C7MtgiEfWyMaETKJH8yMDjXSNxnhp" class="a21-veja-mais">Veja mais</a>
                </div>
            </div>
        </div>

    </div>
</section>
<!--=== Fim Você também será aprovado! ===-->

<!--=== Perguntas Frequentes ===-->
<section class="container-fluid a21 a21-faq pt-5 pb-4">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="a21-faq-pretitulo text-uppercase text-white mt-5">Tem alguma dúvida?</div>

                <h2 class="a21-vantagens-titulo  mb-5 text-white">Perguntas Frequentes</h2>

                <div class="col-12 a21-vantagens-box mb-2">
                    <a href="#" class="a21-vantagens-link">
                        <div class="d-flex justify-content-between a21-vantagens-item-titulo">
                            <div>Quais as diferenças das assinaturas?</div>
                            <div>
                                <i class="fa fa-plus-circle a21-vantagens-bullet"></i>
                                <i class="fa fa-minus-circle a21-vantagens-bullet" style="display: none"></i>
                            </div>
                        </div>
                    </a>

                    <div class="a21-vantagens-item-descricao mt-2" style="display: none">
                        Your content goes here. Edit or remove this text inline or in the module Content settings.
                        You can also style every aspect of this content in the module Design settings and even apply
                        custom CSS to this text in the module Advanced settings.
                    </div>
                </div>

                <div class="col-12 a21-vantagens-box mb-2">
                    <a href="#" class="a21-vantagens-link">
                        <div class="d-flex justify-content-between a21-vantagens-item-titulo">
                            <div>Como funciona a metodologia exponencial?</div>
                            <div>
                                <i class="fa fa-plus-circle a21-vantagens-bullet"></i>
                                <i class="fa fa-minus-circle a21-vantagens-bullet" style="display: none"></i>
                            </div>
                        </div>
                    </a>

                    <div class="a21-vantagens-item-descricao mt-2" style="display: none">
                        Your content goes here. Edit or remove this text inline or in the module Content settings.
                        You can also style every aspect of this content in the module Design settings and even apply
                        custom CSS to this text in the module Advanced settings.
                    </div>
                </div>

                <div class="col-12 a21-vantagens-box mb-2">
                    <a href="#" class="a21-vantagens-link">
                        <div class="d-flex justify-content-between a21-vantagens-item-titulo">
                            <div>O que não está incluso na assinatura?</div>
                            <div>
                                <i class="fa fa-plus-circle a21-vantagens-bullet"></i>
                                <i class="fa fa-minus-circle a21-vantagens-bullet" style="display: none"></i>
                            </div>
                        </div>
                    </a>

                    <div class="a21-vantagens-item-descricao mt-2" style="display: none">
                        Your content goes here. Edit or remove this text inline or in the module Content settings.
                        You can also style every aspect of this content in the module Design settings and even apply
                        custom CSS to this text in the module Advanced settings.
                    </div>
                </div>

                <div class="col-12 a21-vantagens-box mb-2">
                    <a href="#" class="a21-vantagens-link">
                        <div class="d-flex justify-content-between a21-vantagens-item-titulo">
                            <div>Posso cancelar a qualquer momento?</div>
                            <div>
                                <i class="fa fa-plus-circle a21-vantagens-bullet"></i>
                                <i class="fa fa-minus-circle a21-vantagens-bullet" style="display: none"></i>
                            </div>
                        </div>
                    </a>

                    <div class="a21-vantagens-item-descricao mt-2" style="display: none">
                        Your content goes here. Edit or remove this text inline or in the module Content settings.
                        You can also style every aspect of this content in the module Design settings and even apply
                        custom CSS to this text in the module Advanced settings.
                    </div>
                </div>

                <div class="col-12 a21-vantagens-box mb-2">
                    <a href="#" class="a21-vantagens-link">
                        <div class="d-flex justify-content-between a21-vantagens-item-titulo">
                            <div>Como funciona as metas e mentoria?</div>
                            <div>
                                <i class="fa fa-plus-circle a21-vantagens-bullet"></i>
                                <i class="fa fa-minus-circle a21-vantagens-bullet" style="display: none"></i>
                            </div>
                        </div>
                    </a>

                    <div class="a21-vantagens-item-descricao mt-2" style="display: none">
                        Your content goes here. Edit or remove this text inline or in the module Content settings.
                        You can also style every aspect of this content in the module Design settings and even apply
                        custom CSS to this text in the module Advanced settings.
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-6">
                <img src="/wp-content/themes/academy/images/assinaturas-2021/faq.png" class="w-100" />
            </div>
        </div>
    </div>
</section>
<!--=== Fim Perguntas Frequentes ===-->

<!--=== WhatsApp ===-->
<section class="container-fluid a21 a21-whatsapp pt-5 pb-5">
    <div class="container pt-5">
        <div class="row">
            <div class="col-12 col-md-8">
                <div class="a21-pretitulo text-uppercase">Ainda com dúvidas?</div>
                <h2 class="a21-vantagens-titulo mb-5">Fale com o Exponencial<br>no WhatsApp!</h2>
            </div>

            <div class="col-12 col-md-4">
                <div class="mt-5">
                    <a target="_blank" href="https://api.whatsapp.com/send?phone=5512991195518&text=Ol%C3%A1!%20Preciso%20de%20ajuda!%20%3AD"
                       class="a21-estamos-online text-white">Estamos online <i class="fa fa-chevron-circle-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>
<!--=== Fim WhatsApp ===-->

<script>$ = jQuery;</script>
<script src="/wp-content/themes/academy/js/servertimecountdown.js"></script>
<script src="/wp-content/themes/academy/js/circles.min.js"></script>
<script>
    $(function() {
        var datadef = $('#countdown').data('date');
        $('#countdown').servertimecountdown({
            date: datadef
        });

        $('.a21-vantagens-link').click(function(e) {
            e.preventDefault();
            $(this).closest('.a21-vantagens-box').find('.a21-vantagens-item-descricao').first().toggle(300);
            $(this).find('.a21-vantagens-bullet').toggle();
        });

        Circles.create({
            id:           'circles-1',
            radius:       70,
            value:        70,
            maxValue:     100,
            width:        5,
            text:         function(value){return value + '%';},
            colors:       ['#ddd', '#00E070'],
            duration:     400,
        });

        Circles.create({
            id:           'circles-2',
            radius:       70,
            value:        50,
            maxValue:     100,
            width:        5,
            text:         function(value){return value + '%';},
            colors:       ['#ddd', '#00E070'],
            duration:     400,
        });

        Circles.create({
            id:           'circles-3',
            radius:       70,
            value:        52,
            maxValue:     100,
            width:        5,
            text:         function(value){return value + '%';},
            colors:       ['#ddd', '#00E070'],
            duration:     400,
        });

        Circles.create({
            id:           'circles-4',
            radius:       70,
            value:        100,
            maxValue:     100,
            width:        5,
            text:         function(value){return value + '%';},
            colors:       ['#ddd', '#00E070'],
            duration:     400,
        });

        Circles.create({
            id:           'circles-5',
            radius:       70,
            value:        50,
            maxValue:     100,
            width:        5,
            text:         function(value){return value + '%';},
            colors:       ['#ddd', '#00E070'],
            duration:     400,
        });

    });
</script>

<?php get_footer(); ?>
