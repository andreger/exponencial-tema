<?php
include_once $_SERVER['DOCUMENT_ROOT'] .'/wp-load.php';

$midia = isset($_GET['midia']) ? $_GET['midia'] : NULL;

$tipos_ids = NULL;
if($midia) {
	switch ($midia) {
		case 'news': $tipos_ids = [TIPO_NOTICIA]; break;
		case 'articles': $tipos_ids = [TIPO_ARTIGO]; break;
		case 'videos': $tipos_ids = [TIPO_VIDEO]; break;
	}
}


KLoader::model("BlogModel");
KLoader::helper("BlogHelper");
KLoader::helper("UrlHelper");
KLoader::helper("UiLabelHelper");

$data['blogs'] = BlogModel::listar($tipos_ids);

KLoader::view("home/blogs", $data);