
<?php
/*
Template Name: Add Courses
*/
?>
<?php get_header(); ?>
<div class="section_head">
        <div class="row">
            <div class="section_head_img">
                <img src="http://expo.com/wp-content/uploads/2014/05/add_sub_ico.png" alt="" class="alignnone">
            </div>
            <div class="header_text">
               Add cources
            </div>
        </div>
    </div>
    
  <div class="section_content">
        <div class="sectiongap"></div>
        
        <div class="row">
            <div class="eightcol m_auto wid_full">
                <div class="threecol fleft">
                	<label class="tname">Title</label>
                </div>
                <div class="ninecol fright">
                	<input type="text" class="input_field" placeholder="Title"/>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        
        <div class="row">
            <div class="eightcol m_auto wid_full">
                <div class="threecol fleft">
                	<label class="tname">Post Description</label>
                </div>
                <div class="ninecol fright">
                    <div class="txtarea_wid_adj">
                    	<textarea></textarea>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        
        <div class="row">
            <div class="eightcol m_auto wid_full">
                <div class="threecol fleft">
                	<label class="tname">Post Price</label>
                </div>
                <div class="ninecol fright">
                	<input type="text" class="input_field" placeholder="Post Price"/>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        
        <div class="row">
            <div class="eightcol m_auto wid_full">
                <div class="threecol fleft">
                	<label class="tname">Given Position</label>
                </div>
                <div class="ninecol fright">
                	<input type="text" class="input_field" placeholder="Given Position"/>
                    <span class="filed_head">
                        If you want to show in home page then give position 2-9 please 
                        remember that give position which is not assign to other cources.
                    </span>
                </div>
                <div class="clear"></div>
            </div>
        </div>
      <div class="sectiongap"></div>
        
       <div class="row">
      <div class="eightcol m_auto wid_full">
                <div class="threecol fleft">
                	<label class="tname">Add featured image</label>
                </div>
                <div class="ninecol fright">
                   	<div class="file_input_div fleft">
                        <input type="button" value="" class="file_input_button" />
                        <input type="file" name="uploaded_file" class="file_input_hidden"/>
                    </div>
                     <input type="text" class="input_field_brows right" readonly="readonly" value='No File Selected'>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        
        
        <div class="row">
            <div class="eightcol m_auto wid_full">
                <div class="threecol fleft">
                	<label class="tname">Post Price</label>
                </div>
                <div class="ninecol fright">
                	<select class="sel_op">
                    	<option>option  1</option>
                    	<option> option 2</option>
                    </select>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        
        <div class="row">
          <div class="eightcol m_auto wid_full">
                <div class="threecol fleft">
                </div>
                <div class="ninecol fright">
               <input type="submit" value="" class="browser_form_bt fright">
                </div>
                <div class="clear"></div>
          </div>
        </div>
               
</div>
<?php get_footer(); ?>