<?php
/*
Template Name: Recovery
*/

if(isset($_POST['submit'])) {
	$email = $_POST['email'];
	
	$user = get_user_by('email', $email);
	if($user) {
		$password = wp_generate_password();
		wp_set_password( $password, $user->ID );
		
// 		$from = 'senha@exponencialconcursos.com.br';
// 		$senha = 'G>>4Ve<h';
		
		$subject = "Recuperação de Senha";
		
// 		$message = "Sua senha foi redefinida para: <b>{$password}</b><br><br><br>";

		$mensagem = get_template_email('recuperacao-senha.php', ['senha' => $password]);
		
		enviar_email($email, $subject, $mensagem);
		
		header('Location: /senha-reenviada?uid=' . $user->ID);
		exit;
	} else {
		
get_header();
?>

<div class="sectiongap"></div>
<div id="topform">	
	<div class="row pt-1 pt-md-5">
		<?php if($_POST['submit']) : ?>
			<div class="register-nosuccess text-center ml-auto mr-auto mb-5 font-10">E-mail n&atilde;o est&aacute; cadastrado no sistema.</div>
		<?php endif ?>

<?php
	}
	
}
get_header();
?>
<div class="container pt-1 pt-md-5">

      <div class="text-blue mb-5">
            <h1>Esqueci minha senha</h1>
      </div>

      <div class="row">
    	<div class="col-md-9 col-lg-6">
        	<form class="form-group" id="recovery-form" method="POST" action="/recuperacao-de-senha">
        		<input class="form-control form-control-lg" type="text" name="email" placeholder="E-mail"  value=""/>
        		<button value="submit" name="submit" class="mt-3 btn u-btn-primary col-3 col-sm-2 float-right">Enviar</button>
    		</form>
		</div>
		<div class="col-lg-2"></div>
        <div class="mt-4 mt-lg-0 col-md-6 col-lg-4">
            <div class="loginimage"></div>
        </div>
      </div> 	

</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="/wp-content/themes/academy/js/jquery.validate.min.js"></script>

<script>
$().ready(function() {
	$("#recovery-form").validate({
		rules: {
			email: {
				required: true,
				email: true
			}
		},
		messages: {
			email: {
				required: "E-mail é obrigatório",
				email: "E-mail inválido"
			}
		},
		submitHandler: function(form) {
		    form.submit();
		}
	});
});
</script>	
<?php get_footer(); ?>