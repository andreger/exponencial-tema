<?php
get_header ();

redirecionar_se_nao_estiver_logado ();

global $wpdb;
global $current_user;
get_currentuserinfo ();
$user_id = get_current_user_id ();

if (isset ( $_GET ['id'] )) {
	include_once $_SERVER ['DOCUMENT_ROOT'] . '/corporativo/questoes.php';
	$questao_id = $_GET ['id'];
	$questao = Questao::get_by_id ( $questao_id );
	$opcoes = Questao::get_opcoes ( $questao_id );
	$assuntos = Questao::get_assuntos ( $questao_id );
	
	if ($_GET ['result'] != null) {
		switch ($_GET ['result']) {
			case 'criar' :
				$result = "Coment&aacute;rio criado com sucesso!";
				break;
			case 'editar' :
				$result = "Coment&aacute;rio editado com sucesso!";
				break;
			case 'excluir' :
				$result = "Coment&aacute;rio excluido com sucesso!";
				break;
		}
	}
	
	// print_r($questao);
	// print_r($opcoes);
	// print_r($asuntos);
	?>
<div class="row">
		<?php if($_GET['result'] !=null) echo "<div class='aviso'>{$result}</div>";?>
		<?php if($_POST['submit']) { echo get_resposta_questao($questao, $_POST['resposta']);	} ?>
		<div><?php echo get_disciplina_nome_from_assuntos($assuntos) . ' > ' . get_assuntos_str($assuntos) ?></div>
	<div>Ano: <?php echo $questao['pro_ano'] ?> </div>
	<div>Banca: <?php echo $questao['ban_nome'] ?></div>
	<div>Orgão: <?php echo $questao['org_nome'] ?></div>
	<div>Prova: <?php echo $questao['pro_nome'] ?></div>

	<div>
			<?php echo $questao['que_enunciado']?>
			<form method="post">
				<?php for($i = 1; $i <= count($opcoes); $i++) : ?>
				<div>
				<input type="radio" name="resposta" value="<?php echo $i ?>"> <?php echo $opcoes[$i-1]['qop_texto']?>
				</div>
				<?php endfor; ?>
				<div>
				<input type="submit" value="Responder" name="submit">
			</div>
		</form>
	</div>
		
<?php
	
include_once $_SERVER ['DOCUMENT_ROOT'] . '/corporativo/view/comentarios/comentarios_view.php';
	Comentarios_View::mostrar_Comentarios ( $questao_id );
	
	if ($_GET ['act'] == 'editar') {
		Comentarios_View::editar_comentario ( $_GET ['com_id'] );
	} else {
		Comentarios_View::criar_comentario ( $questao_id );
	}
	?>
	</div>

<?php
}

?>
</div>
<!-- #content -->
<div class="sectionlargegap"></div>
<?php get_footer(); ?>  
