<?php 
/**
 * Controller para organizar fluxos dos eventos do header
 * 
 * @package	KCore/Controllers
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K4
 */ 

class HeaderController {

	/**
	 * Gera o breadcrumb das páginas
	 * 
	 * @since K4
	 * 
	 * @return string HTML do breadcrumb gerado
	 */ 

	public static function breadcrumb()
	{
		$partes = [];

		// Cadastro e Login
		if(UrlHelper::is_url_cadastro_login()) {
			$partes[] = ['texto' => 'Cadastro e Login'];
		}

		// Cursos por Concurso
		if(UrlHelper::is_url_cursos_por_concurso()) {
			$partes[] = ['texto' => 'Cursos por Concurso'];
		}

		// Cursos por Concurso Específico com paginação
		elseif(UrlHelper::is_url_cursos_por_concurso_especifico_paginada()) {
			global $concurso;

			$partes[] = ['texto' => 'Cursos por Concurso', 'url' => UrlHelper::get_cursos_por_concurso_url()];
			$partes[] = ['texto' => $concurso->post_title];
		}

		// Cursos por Concurso Específico
		elseif(UrlHelper::is_url_cursos_por_concurso_especifico()) {
			global $concurso;

			$partes[] = ['texto' => 'Cursos por Concurso', 'url' => UrlHelper::get_cursos_por_concurso_url()];
			$partes[] = ['texto' => $concurso->post_title];
		}

		// Cursos por Matéria
		elseif(UrlHelper::is_url_cursos_por_materia()) {
			$partes[] = ['texto' => 'Cursos por Matéria'];
		}

		// Cursos por Matéria Específica
		elseif(UrlHelper::is_url_cursos_por_materia_especifica()) {
			global $materia;

			$partes[] = ['texto' => 'Cursos por Matéria', 'url' => UrlHelper::get_cursos_por_materia_url()];
			$partes[] = ['texto' => $materia->name];
		}

		// Cursos por Professor
		elseif(UrlHelper::is_url_cursos_por_professor()) {
			$partes[] = ['texto' => 'Cursos por Professor'];
		}

		// Cursos por Professor Específico
		elseif(UrlHelper::is_url_cursos_por_professor_especifico()) {
			global $professor;

			$partes[] = ['texto' => 'Cursos por Professor', 'url' => UrlHelper::get_cursos_por_professor_url()];
			$partes[] = ['texto' => $professor->display_name];
		}

		// Todos os Cursos
		elseif(UrlHelper::is_url_todos_cursos()) {
			$partes[] = ['texto' => 'Todos os Cursos', 'url' => UrlHelper::get_todos_cursos_url()];
		}

		// Cursos Grátis
		elseif(UrlHelper::is_url_cursos_gratis()) {
			$partes[] = ['texto' => 'Cursos Grátis', 'url' => UrlHelper::get_cursos_gratis_url()];
		}

		// Simulados Grátis
		elseif(UrlHelper::is_url_simulados_gratis()) {
			$partes[] = ['texto' => 'Simulados Grátis', 'url' => UrlHelper::get_simulados_gratis_url()];
		}

		// Mapas Mentais Grátis
		elseif(UrlHelper::is_url_mapas_mentais_gratis()) {
			$partes[] = ['texto' => 'Mapas Mentais Grátis', 'url' => UrlHelper::get_mapas_mentais_gratis_url()];
		}

		// Questões Comentadas Grátis
		elseif(UrlHelper::is_url_questoes_comentadas_gratis()) {
			$partes[] = ['texto' => 'Questões Comentadas Grátis', 'url' => UrlHelper::get_questoes_comentadas_gratis_url()];
		}

		// Audiobooks Grátis
		elseif(UrlHelper::is_url_audiobooks_gratis()) {
			$partes[] = ['texto' => 'Audiobooks Grátis', 'url' => UrlHelper::get_audiobooks_gratis_url()];
		}
		
		// Audiobooks Grátis
		elseif(UrlHelper::is_url_pesquisa()) {
		    $partes[] = ['texto' => 'Pesquisa', 'url' => UrlHelper::get_pesquisa_url()];
		}

		//Depoimentos
		elseif(UrlHelper::is_url_depoimentos())
		{
			$partes[] = ['texto' => 'Depoimentos e Entrevistas', 'url' => UrlHelper::get_depoimentos_url()];
		}
		elseif(UrlHelper::is_url_depoimentos_textos())
		{
			$partes[] = ['texto' => 'Depoimentos e Entrevistas', 'url' => UrlHelper::get_depoimentos_url()];
			$partes[] = ['texto' => 'Depoimentos em Texto', 'url' => UrlHelper::get_depoimentos_textos_url()];
		}
		elseif(UrlHelper::is_url_depoimentos_videos())
		{
			$partes[] = ['texto' => 'Depoimentos e Entrevistas', 'url' => UrlHelper::get_depoimentos_url()];
			$partes[] = ['texto' => 'Depoimentos em Vídeo', 'url' => UrlHelper::get_depoimentos_videos_url()];
		}
		elseif(UrlHelper::is_url_entrevistas_videos())
		{
			$partes[] = ['texto' => 'Depoimentos e Entrevistas', 'url' => UrlHelper::get_depoimentos_url()];
			$partes[] = ['texto' => 'Entrevistas em Vídeo', 'url' => UrlHelper::get_entrevistas_videos_url()];
		}
		elseif(UrlHelper::is_url_entrevistas_textos())
		{
			$partes[] = ['texto' => 'Depoimentos e Entrevistas', 'url' => UrlHelper::get_depoimentos_url()];
			$partes[] = ['texto' => 'Entrevistas em Texto', 'url' => UrlHelper::get_entrevistas_textos_url()];
		}
		elseif(UrlHelper::is_url_depoimento() || UrlHelper::is_url_entrevista())
		{
			KLoader::model('DepoimentoModel');
			$slug = get_query_var('nome');
			$depoimento = DepoimentoModel::get_by_slug($slug);

			$partes[] = ['texto' => 'Depoimentos e Entrevistas', 'url' => UrlHelper::get_depoimentos_url()];

			if(UrlHelper::is_url_depoimento())
			{
				$partes[] = ['texto' => 'Depoimentos em Texto', 'url' => UrlHelper::get_depoimentos_textos_url()];
			}
			elseif(UrlHelper::is_url_entrevista())
			{
				$partes[] = ['texto' => 'Entrevistas em Texto', 'url' => UrlHelper::get_entrevistas_textos_url()];	
			}
			$partes[] = ['texto' => $depoimento->post_title];

		}

		// Produto
		elseif(UrlHelper::is_url_produto()) {
			
			global $post;

			// Confere de qual link o usuário veio para montar o breadcrumb
			if(UrlHelper::is_url_cursos_gratis($_SERVER['HTTP_REFERER'])) {				
				$partes[] = ['texto' => 'Cursos Grátis', 'url' => UrlHelper::get_cursos_gratis_url()];				
			}

			elseif(UrlHelper::is_url_simulados_gratis($_SERVER['HTTP_REFERER'])) {			
				$partes[] = ['texto' => 'Simulados Grátis', 'url' => UrlHelper::get_simulados_gratis_url()];				
			}

			elseif(UrlHelper::is_url_mapas_mentais_gratis($_SERVER['HTTP_REFERER'])) {
				$partes[] = ['texto' => 'Mapas Mentais Grátis', 'url' => UrlHelper::get_mapas_mentais_gratis_url()];				
			}

			elseif(UrlHelper::is_url_questoes_comentadas_gratis($_SERVER['HTTP_REFERER'])) {
				$partes[] = ['texto' => 'Questões Comentadas Grátis', 'url' => UrlHelper::get_questoes_comentadas_gratis_url()];				
			}

			elseif(UrlHelper::is_url_audiobooks_gratis($_SERVER['HTTP_REFERER'])) {
				$partes[] = ['texto' => 'Audiobooks Grátis', 'url' => UrlHelper::get_audiobooks_gratis_url()];				
			}

			elseif(UrlHelper::is_url_cursos_por_concurso_especifico($_SERVER['HTTP_REFERER'])) {
				
				$concurso = ConcursoModel::get_by_cursos_por_concurso_url($_SERVER['HTTP_REFERER']);

				$partes[] = ['texto' => 'Cursos por Concurso', 'url' => UrlHelper::get_cursos_por_concurso_url()];
				$partes[] = ['texto' => $concurso->post_title, 'url' => $_SERVER['HTTP_REFERER']];
			}

			elseif(UrlHelper::is_url_cursos_por_materia_especifica($_SERVER['HTTP_REFERER'])) {
				
				$materia = MateriaModel::get_by_cursos_por_materia_url($_SERVER['HTTP_REFERER']);

				$partes[] = ['texto' => 'Cursos por Matéria', 'url' => UrlHelper::get_cursos_por_materia_url()];
				$partes[] = ['texto' => $materia->name, 'url' => $_SERVER['HTTP_REFERER']];
			}

			elseif(UrlHelper::is_url_cursos_por_professor_especifico($_SERVER['HTTP_REFERER'])) {
				
				$professor = ProfessorModel::get_by_cursos_por_professor_url($_SERVER['HTTP_REFERER']);

				$partes[] = ['texto' => 'Cursos por Professor', 'url' => UrlHelper::get_cursos_por_professor_url()];
				$partes[] = ['texto' => $professor->display_name, 'url' => $_SERVER['HTTP_REFERER']];
			}
			else {
				$partes[] = ['texto' => 'Todos os Cursos', 'url' => UrlHelper::get_todos_cursos_url()];
			}

			$partes[] = ['texto' => $post->post_title];

		}

		// monta breadcrumb se houver partes
		if($partes) {

			array_unshift($partes, ['texto' => 'Home', 'url' => '/']);
			
			$breadcrumb = "<div class='container'>
			<nav class='text-left' aria-label='breadcrumb'><ol class='breadcrumb bg-white'>";
			
			for($i = 0; $i < count($partes); $i++) {

				$texto = $partes[$i]['texto'];
				$url = isset($partes[$i]['url']) ? $partes[$i]['url'] : "";
				
				if($i == count($partes) - 1) {
					$breadcrumb .= "<li class='breadcrumb-item active' aria-current='page'>{$texto}</li>";
				}
				else {
					$breadcrumb .= "<li class='breadcrumb-item'><a href='{$url}'>{$texto}</a></li>";
				}

			}

			$breadcrumb .= "</ol></nav></div>";

			return $breadcrumb;
		}

		// se não houver partes, retorna vazio
		return "";
	}


}