<?php 
/**
 * Controller para organizar fluxos dos chamadas Ajax
 * 
 * @package	KCore/Controllers
 *
 * @author 	João Paulo Ferreira <jpaulofms@gmail.com>
 *
 * @since	L1 M3
 */ 

class XhrController {

	/**
	 * Retorna os pedidos de uma aba da área do aluno
	 * 
	 * @since L1 M3
	 * 
	 * @return string HTML do conteúdo da aba de pedido
	 */ 

	public static function micon_aba_ativa()
	{

        if( $user_id = get_current_user_id() )
        {

            KLoader::model("PedidoModel");

            //Parametros necessários
            $aba = $_POST['aba'];
            $tipo_busca = $_POST['tipo_busca'];
            $pg = $_POST['pg'];
            

            // Montagem da paginação
            $total = PedidoModel::contar_meus_pedidos($user_id, $tipo_busca);
            $pattern = "/minha-conta?a={$aba}&pg=(:num)";
            $limit = MINHA_CONTA_PEDIDOS_POR_PAGINA;
            $offset = $pg ? ($pg - 1) * $limit : 0;
            
            $data['paginator'] = new Paginator($total, $limit, $pg, $pattern);
            $data['total'] = $total;

            if($total)
            {
                $data['pedidos'] = PedidoModel::listar_meus_pedidos($user_id, $tipo_busca, $offset, $limit);
            }

            $data['user_id'] = $user_id;

            KLoader::view("minha_conta/micon/micon_aba_tabela", $data);
        }
        else
        {
            throw new Exception("Acesso Negado");
        }
    }

    /**
     * Retorna o cronograma de um produto
     * 
     * @since L1 M3
     * 
     * @return string HTML do conteúdo do cronograma do produto
     */

     public static function midet_cronograma()
     {
        $produto_id = $_POST['produto_id'];

        $produto = wc_get_product($produto_id);
        $post = get_post($produto_id);
        setup_postdata( $post );

        $data['product'] = $produto;
        $data['post'] = $post;
        $data['is_conta_usuario'] = $_POST['is_conta_usuario'];

        KLoader::view("produto/cronograma/cronograma_corpo", $data); 
     }

    /**
     * Retorna o conteúdo de um produto premium
    * 
    * @since L1 M3
    *
    * @return string HTML do conteúdo do produto premium
    */

    public static function midep_conteudo()
    {
        KLoader::model("ProdutoModel");
        KLoader::model("PremiumModel");
        KLoader::model("PedidoModel");
        KLoader::helper("UrlHelper");

        //Parâmetros necessários
        $produto_id = $_POST['produto_id'];
        $pedido_id = $_POST['pedido_id'];
        $pg = $_POST['pg'];
        
        $pedido_produto = PedidoModel::get_pedido_produto($pedido_id, $produto_id);
        
        if( $pedido_produto->usu_id == get_current_user_id() )
        {

            // Monta dados para chamadas de modelo
            $sessao_index = "form_minha_conta_pp_{$pedido_id}_{$produto_id}";
            $offset = ($pg - 1) * PRODUTO_PREMIUM_ITENS_POR_PAGINA;

            $data['filtros'] = [
                "concursos" => $_SESSION[$sessao_index]['concursos'],
                "materias" => $_SESSION[$sessao_index]['materias'],
                "professores" => $_SESSION[$sessao_index]['professores'],
                "pacotes" => $_SESSION[$sessao_index]['pacotes'],
                "tipos" => $_SESSION[$sessao_index]['tipos'],
                "texto" => $_SESSION[$sessao_index]['texto'],
                "ordem" => $_SESSION[$sessao_index]['ordem'],
                //"fixar" => $_SESSION[$sessao_index]['fixar'],
                'limit' => PRODUTO_PREMIUM_ITENS_POR_PAGINA,
                "offset" => $offset
            ];

            //Chamadas de modelo
            $data['items'] = ProdutoModel::listar_premium_items($produto_id, $pedido_id, $data['filtros']);
            $data['total'] = ProdutoModel::contar_premium_items($produto_id, $pedido_id, $data['filtros']);
            $data['fixados'] = PremiumModel::listar_itens_fixados($pedido_id, $produto_id);
            $data['acessos_requeridos'] = PremiumModel::Listar_acessos_requeridos($pedido_id, $produto_id);

            //Paginação
            $pattern = "/minha-conta-detalhe?o_id={$pedido_id}&p_id={$produto_id}&pg=(:num)";
            $data['paginator'] = new Paginator($data['total'], $data['filtros']['limit'], $pg, $pattern);
        
            $data['produto_id'] = $produto_id;
            $data['pedido_id'] = $pedido_id;

            KLoader::view("minha_conta/midep/midep_conteudo", $data);

        }

        return "";
    }

    /**
     * Retoana os artigos do professor
     * 
     * @since L1 M3
     * 
     * @return string HTML contendo os artigos do professor
     */

     public static function artigos_por_professor()
     {

        KLoader::model("BlogModel");
        KLoader::model("ProfessorModel");
        KLoader::helper("ProfessorHelper");
        KLoader::helper("UrlHelper");

        $professor_id = $_POST['professor_id'];
        $pg = $_POST['pg'];
        $slug = $_POST['slug'];

        $limit = 50;
        $offset = ($pg - 1) * $limit;

        $data['artigos'] = BlogModel::listar_por_professor($professor_id, [TIPO_ARTIGO], 'post_date DESC', $limit, $offset);

        $total =  BlogModel::contar_por_professor($professor_id, [TIPO_ARTIGO], 'post_date DESC');
        $pattern = "/artigos-por-professor/{$slug}?pg=(:num)";

        $data['paginator'] = new Paginator($total, $limit, $pg, $pattern);

        KLoader::view('main/artigos_conteudo', $data);

     }
}