<?php
/**
 * Controller para organizar fluxos dos eventos de relatórios
 *
 * @package	KCore/Controllers
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	L1
 */
session_start();

class RelatorioController {

    /**
     * Relatório de Pedido Premium
     *
     * @since L1
     */ 
    
    public static function pedido_premium()
    {
        /* === Controle de acesso === */
        tem_acesso(array(
            ADMINISTRADOR, REVISOR, ATENDENTE, APOIO
        ), ACESSO_NEGADO);
        
        /* === Includes === */
        KLoader::model("PremiumModel");
        KLoader::helper("RelatorioPedidosPremiumHelper");
                
        /* === Inicializa a sessão === */
        if(!$_SESSION['frm_relpedp'] || $_POST['limpar']) { 
            $_SESSION['frm_relpedp'] = [];
        }
        
        /* === Alimenta a sessão === */
        if (isset($_POST['filtrar']) || isset($_POST['export-excel'])) {   
            $_SESSION['frm_relpedp'] = [
                'pedido' => $_POST['pedido'] ?: null,
                'produto' => $_POST['produto'] ?: null,
                'item' => $_POST['item'] ?: null,
                'pedido_oculto' => $_POST['pedido_oculto'] ?: null,
                'data_req_ini' => $_POST['data_req_ini'] ?: null,
                'data_req_fim' => $_POST['data_req_fim'] ?: null,
            ];
        }
        
        /* === Monta dados para chamadas de modelo === */       
        $data["filtros"] = [
            "pedido" => $_SESSION['frm_relpedp']['pedido'],
            'produto' => $_SESSION['frm_relpedp']['produto'],
            'item' => $_SESSION['frm_relpedp']['item'],
            'pedido_oculto' => $_SESSION['frm_relpedp']['pedido_oculto'],
            'data_req_ini' => $_SESSION['frm_relpedp']['data_req_ini'],
            'data_req_fim' => $_SESSION['frm_relpedp']['data_req_fim'],
        ];

        /* === Execução da consulta === */
        if($_POST['export-excel']) {
            $limit = null;
            $offset = null;
        }
        else {
            /* === Montagem da paginação === */
            $pg = isset($_GET['pg']) ? $_GET['pg'] : "1";
            $total = PremiumModel::contar_pedidos_premium($data["filtros"]);
            $pattern = "/relatorio-pedidos-premium?pg=(:num)";
            $limit = RELATORIO_PEDIDOS_PREMIUM_POR_PAGINA;
            $paginator = new Paginator($total, $limit, $pg, $pattern);
            $offset = $pg ? ($pg - 1) * $limit : 0;

            $data['total'] = $total;
            $data['paginacao'] = $paginator;
        }
        
        $premiums = PremiumModel::listar_pedidos_premium($data['filtros'], $offset, $limit);
        
        if($_POST['export-excel']) {
            RelatorioPedidosPremiumHelper::exportar_xls($premiums);
        }else{
            $data["premiums"] = $premiums;        
            KLoader::view("relatorios/relpedp/relpedp", $data);
        }
        
    }
    
    /**
     * Relatório de Cálculo de percentual de professor
     *
     * @since L1
     */ 
    
    public static function calculo_percentual_professor()
    {
        /* === Controle de acesso === */
//         tem_acesso(array(
//             ADMINISTRADOR, COORDENADOR_AREA, PROFESSOR, ATENDENTE, APOIO
//         ), ACESSO_NEGADO);

        tem_acesso(array(
            ADMINISTRADOR
        ), ACESSO_NEGADO);
        
        /* === Includes === */
        KLoader::model("ProdutoModel");
        KLoader::model("PremiumModel");
        KLoader::helper("ProdutoHelper");
        KLoader::helper("NumeroHelper");
        KLoader::helper("UiSelectHelper");
        KLoader::helper("RelatorioCalculoPercentualProfessorHelper");
        
        /* === Fluxo principal === */
        $data["produtos_combo"] = UiSelectHelper::get_combo_options(PremiumModel::listar_produtos(PROFESSOR_EQUIPE_ASSINATURAS), "post_id", "post_title", false);
        $data["anos_meses_combo"] = UiSelectHelper::get_combo_options(PremiumModel::listar_anos_meses(), "chave", "valor", false);
        
        /* === Inicializa a sessão === */
        if(!$_SESSION['frm_relpp']) {
 
            $_SESSION['frm_relpp'] = [];
        }
        
        /* === Alimenta a sessão === */
        if (isset($_POST['filtrar']) || isset($_POST['export-excel'])) {   
            $_SESSION['frm_relpp'] = [
                'produtos' => $_POST['produtos_selecionados'] ?: null,
                'anos_meses' => $_POST['anos_meses_selecionados'] ?: null,
                'excluir_cadernos' => $_POST['excluir_cadernos'] ?: null,
                'excluir_gratuitos' => $_POST['excluir_gratuitos'] ?: null,
                'excluir_mapas' => $_POST['excluir_mapas'] ?: null,
            ];
        }
        
        /* === Monta dados para chamadas de modelo === */
        
        $default_anos_meses = null;
        if($data["anos_meses_combo"]) {
            $ak = array_keys($data["anos_meses_combo"]);
            $default_anos_meses = $ak[0];
        }
 
        
        $data["filtros"] = [
            "produtos" => $_SESSION['frm_relpp']['produtos'],
            'anos_meses' => $_SESSION['frm_relpp']['anos_meses'] ?: $default_anos_meses,
//             'professor_id' => PROFESSOR_EQUIPE_ASSINATURAS,
            'excluir_cadernos' => $_SESSION['frm_relpp']['excluir_cadernos'],
            'excluir_gratuitos' => $_SESSION['frm_relpp']['excluir_gratuitos'],
            'excluir_mapas' => $_SESSION['frm_relpp']['excluir_mapas'],
        ];
        
        /* === Execução da consulta === */
        if($_POST['export-excel']) {
            $limit = null;
            $offset = null;
        }
        
        $premiums = [];
        $premiums_info = PremiumModel::listar_premium_info($data["filtros"]);
        
        if($premiums_info) {
            foreach ($premiums_info as &$premium_info) {
                $total = PremiumModel::get_valor_total_produtos($premium_info->post_id, $data["filtros"]['anos_meses']);
                $ultimo_agendamento = PremiumModel::get_ultimo_agendamento($data["filtros"]['anos_meses'], $premium_info->post_id);
                
                $p_info = [
                    'ano_mes' =>$data["filtros"]['anos_meses'],
                    'nome' => $premium_info->post_title,
                    'id' => $premium_info->post_id,
                    'valor' => $total,
                    'reprocessado_em' => $ultimo_agendamento ? converter_para_ddmmyyyy_HHiiss($ultimo_agendamento->pra_data) : ""
                ];
                
                $premiums[] = $p_info;
            }  
        }
        
        if($_POST['export-excel']) {
            RelatorioCalculoPercentualProfessorHelper::exportar_xls($premiums);
        }
        else {
            $data['premiums'] = $premiums;
            
            KLoader::view("relatorios/calculo-percentual-professor/index", $data);
        }
    }
    
    /**
     * Relatório de detalhe de cálculo de percentual de professor
     *
     * @since L1
     */
    
    public static function calculo_percentual_professor_detalhe()
    {
        /* === Controle de acesso === */
//         tem_acesso(array(
//             ADMINISTRADOR, COORDENADOR_AREA, PROFESSOR, ATENDENTE, APOIO
//         ), ACESSO_NEGADO);
        
        tem_acesso(array(
            ADMINISTRADOR
        ), ACESSO_NEGADO);
        
        /* === Includes === */
        KLoader::model("ProdutoModel");
        KLoader::model("PremiumModel");
        KLoader::helper("PremiumHelper");
        KLoader::helper("ProdutoHelper");
        KLoader::helper("NumeroHelper");
        KLoader::helper("UiSelectHelper");
        KLoader::helper("RelatorioCalculoPercentualProfessorHelper");
        
        $filtros = [
            "premium_id" => $_GET['pid'],
            'ano_mes' => $_GET['am'],
        ];
        
        $data["produtos_combo"] = PremiumHelper::get_combo_options_com_ids("Selecione o produto");
        $data["anos_meses_combo"] = UiSelectHelper::get_combo_options(PremiumModel::listar_anos_meses(), "chave", "valor", false, "Selecione o período");
        
        $produto = ProdutoModel::get_by_id($filtros["premium_id"]);
        $items = PremiumModel::listar_produtos_items_professores($filtros["premium_id"], $filtros["ano_mes"]);
        $valor_total = PremiumModel::get_valor_total_produtos($filtros["premium_id"], $filtros["ano_mes"]);
        $ultimo_agendamento = PremiumModel::get_ultimo_agendamento($filtros["ano_mes"], $filtros["premium_id"]);
        $catalogo_areas = ProdutoHelper::listar_produtos_areas_str();
        $catalogo_concursos = ProdutoHelper::listar_produtos_concursos_str();
        
        $premium = [];
        $premium['info']['id'] = $filtros['premium_id'];
        $premium['info']['nome'] = $produto->post_title;
        $premium['info']['total'] = $valor_total;
        $premium['info']['data'] = periodo_mes_ano($filtros["ano_mes"]);
        $premium['info']['reprocessado_em'] = $ultimo_agendamento ? "<div class='text-left mb-2 ml-2'>Data de Processamento: " . converter_para_ddmmyyyy_HHiiss($ultimo_agendamento->pra_data) . "</div>" : "";
        
        if($items) {
            foreach ($items as $item) {
                
                $valor = $item->pro_preco * $item->pru_percentual / 100;
                $areas = $catalogo_areas[$item->post_id];
                $concursos = $catalogo_concursos[$item->post_id];
                
                $p_item = [
                    'data' => periodo_mes_ano($filtros["ano_mes"]),
                    'produto' => $item->post_title,
                    'produto_id' => $item->post_id,
                    'disponivel_ate' => converter_para_ddmmyyyy($item->pro_disponivel_ate),
                    'status' => ProdutoHelper::get_status_nome($item->post_status),
                    'areas' => $areas,
                    'concursos' => $concursos,
                    'professor' => $item->display_name,
                    'preco' => moeda($item->pro_preco),
                    'percentual' => porcentagem($item->pru_percentual, 4),
                    'valor' => moeda($valor, true, 4),
                    'cota' => porcentagem($valor / $valor_total * 100, 4)
                ];
                
                $premium['itens'][] = $p_item; 
            }
            
        }
        
        if($_GET['export-excel']) {
            RelatorioCalculoPercentualProfessorHelper::exportar_detalhe_xls($premium);
           
        }
        else {
            $data["premium"] = $premium;
            KLoader::view("relatorios/calculo-percentual-professor-detalhe/index", $data);
        }
    }
    
    /**
     * Relatório de detalhe de cálculo de percentual de professor
     *
     * @since L1
     */
    
    public static function pagamento_professor_premium()
    {
        /* === Controle de acesso === */
        //         tem_acesso(array(
        //             ADMINISTRADOR, COORDENADOR_AREA, PROFESSOR, ATENDENTE, APOIO
        //         ), ACESSO_NEGADO);
        
        tem_acesso(array(
            ADMINISTRADOR
        ), ACESSO_NEGADO);
        
        /* === Includes === */
        KLoader::model("ProdutoModel");
        KLoader::model("ProfessorModel");
        KLoader::model("PremiumModel");
        KLoader::helper("ProdutoHelper");
        KLoader::helper("NumeroHelper");
        KLoader::helper("UiSelectHelper");
        KLoader::helper("RelatorioPagamentoProfessorPremiumHelper");

        $ano_mes = $_GET['am'] ?? date("Ym");
        $professor_id = $_GET['prof'] ?? null;
        $exportar = $_GET['export-excel'] ?? null;
        
        $professores_premiums = RelatorioPagamentoProfessorPremiumHelper::get_dados($ano_mes, $professor_id);
        $somatorios = RelatorioPagamentoProfessorPremiumHelper::get_somatorios($professores_premiums);
        
        $data["anos_meses_combo"] = UiSelectHelper::get_combo_options(PremiumModel::listar_anos_meses(), "chave", "valor", false);
        $data["professores_combo"] = UiSelectHelper::get_combo_options(ProfessorModel::listar_por_tipo([COLABORADOR_PROFESSOR, COLABORADOR_PROFESSOR_CONSULTOR]), "ID", "display_name", false, "Selecione um professor...");
        
        if($exportar) {
            RelatorioPagamentoProfessorPremiumHelper::exportar_xls($professores_premiums, $somatorios);
        }
        else {
            $data["premium"] = $professores_premiums;
            $data["somatorios"] = $somatorios;
            
            KLoader::view("relatorios/pagamento-professor-premium/index", $data);
        }
    }
}