<?php
/**
 * Inicializa os models, helpers, views, hooks e controllers do tema
 * 
 * @since k3
 */  

require_once 'KLoader.php';

// inclusão dos hooks e shortcodes
$paths = ['/kcore/hooks/', '/kcore/shortcodes/'];

foreach($paths as $path) {
	$path = get_stylesheet_directory() . $path;

	$files = scandir($path);
	$files = array_diff(scandir($path), array('.', '..'));

	foreach($files as $file) {
		include_once $path . $file;
	}
}


/*
spl_autoload_register(function($class_name) {
   
    if(ends_with($class_name, "Model")) {
        include_once get_stylesheet_directory() . '/kcore/models/' . $class_name . '.php';
    }
    elseif(ends_with($class_name, "Helper")) {
        include_once get_stylesheet_directory() . '/kcore/helpers/' . $class_name . '.php';
    }
    elseif(ends_with($class_name, "Controller")) {
        include_once get_stylesheet_directory() . '/kcore/controllers/' . $class_name . '.php';
    }
    
});
*/