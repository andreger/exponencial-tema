<?php
include_once 'Api.php';

KLoader::model("AreaModel");

class AreaApi extends Api
{
    public static function salvar($dados)
    {
        $params = self::converterParaFormatoApi($dados);
        self::doRequest(self::REQUEST_POST, "/areas/save", $params);
    }

    public static function excluir($id)
    {
        self::doRequest(self::REQUEST_DELETE, "/areas/remove", ['id' => $id]);
    }

    public static function sincronizar()
    {
        $params = [];

        $items = AreaModel::listar();

        foreach ($items as $item) {
            $param = self::converterParaFormatoApi($item);
            $params[] = $param;
        }

        self::doRequest(self::REQUEST_POST, "/areas/sync", $params);
    }

    public static function converterParaFormatoApi($dados)
    {
        return [
            'id' => $dados->term_id,
            'nome' => $dados->name,
        ];
    }
}