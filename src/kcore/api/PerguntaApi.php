<?php
include_once 'Api.php';

KLoader::model("PerguntaModel");

class PerguntaApi extends Api
{
    public static function salvar($dados)
    {
        $params = self::converterParaFormatoApi($dados);
        self::doRequest(self::REQUEST_POST, "/perguntas/save", $params);
    }

    public static function excluir($id)
    {
        self::doRequest(self::REQUEST_DELETE, "/perguntas/remove", ['id' => $id]);
    }

    public static function sincronizar()
    {
        $params = [];

        $items = PerguntaModel::listar();

        foreach ($items as $item) {
            $tt = PerguntaModel::get_term_taxonomy($item->ID);



            $item->tipo = $tt ? $tt->term_taxonomy_id : null;
            $param = self::converterParaFormatoApi($item);
            $params[] = $param;
        }

        self::doRequest(self::REQUEST_POST, "/perguntas/sync", $params);
    }

    public static function converterParaFormatoApi($dados)
    {
        return [
            'id' => $dados->ID,
            'pergunta' => $dados->post_title,
            'resposta' => $dados->post_content,
            'ordem' =>  $dados->menu_order,
            'tipo' => $dados->tipo
        ];
    }
}