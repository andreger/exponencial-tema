<?php
include_once 'Api.php';

KLoader::model("UsuarioModel");
KLoader::model("ProfessorModel");

class UsuarioApi extends Api
{
    public static function salvar($dados)
    {
        $params = self::converterParaFormatoApi($dados);

        $usuario = self::doRequest(self::REQUEST_POST, "/usuarios/save", $params);

        return $usuario;
    }

    public static function excluir($id)
    {
        self::doRequest(self::REQUEST_DELETE, "/usuarios/remove", ['id' => $id]);
    }

    public static function sincronizar($ultimoId = 0)
    {
        $params = [];

        $items = UsuarioModel::listarProximos($ultimoId);

        $novoUltimo = 0;
        foreach ($items as $item) {
            $param = self::converterParaFormatoApi($item);
            $params[] = $param;

            $novoUltimo = $item['id'];
        }

        if($ultimoId == 0) {
            self::doRequest(self::REQUEST_DELETE, "/usuarios/destroy-all");
        }

        self::doRequest(self::REQUEST_POST, "/usuarios/sync-lazy", $params);

        return $novoUltimo;
    }

    public static function converterParaFormatoApi($dados)
    {
        $isProfessor = is_professor($dados['id']);

        $professor = null;
        if($isProfessor) {
            $professor = ProfessorModel::get_by_id($dados['id']);
        }

        return [
            'id' => $dados['id'],
            'slug' => $dados['slug'],
            'name' => $dados['nome_completo'] ?: "",
            'email' => $dados['email'],
            'password' => '12345678',
            'cpf' => $dados['cpf'],
            'imagem' =>  $professor ? $professor->col_foto_url_365 : null,
            'thumb' => $professor ? $professor->col_foto_url_200 : null,
            'descricao' => $professor ? $professor->col_descricao : null,
            'mini_cv' => $professor ? $professor->col_mini_cv : null,
            'cargo' => $professor ? $professor->col_cargo : null,
            'youtube' => $professor ? $professor->col_youtube : null,
            'linkedin' => $professor ? $professor->col_linkedin : null,
            'facebook' => $professor ? $professor->col_facebook : null,
            'twitter' => $professor ? $professor->col_twitter : null,
            'instagram' => $professor ? $professor->col_instagram : null,
            'oculto' => $professor ? $professor->col_oculto : null,
            'prioridade' => $professor ? $professor->col_prioridade : null,
            'qtde_cursos' => $professor ? $professor->col_qtde_cursos : null,
            'is_professor' => $isProfessor,
        ];
    }
}