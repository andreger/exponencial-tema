<?php
include_once 'Api.php';

KLoader::model("RespostaModel");

class RespostaApi extends Api
{
    public static function salvar($dados)
    {
        $params = self::converterParaFormatoApi($dados);
        self::doRequest(self::REQUEST_POST, "/respostas/save", $params);
    }

    public static function excluir($id)
    {
        self::doRequest(self::REQUEST_DELETE, "/respostas/remove", ['id' => $id]);
    }

    public static function sincronizar($ultimoId = 0)
    {
        $params = [];

        $items = RespostaModel::listarProximos($ultimoId);

        $novoUltimo = 0;
        foreach ($items as $item) {
            $param = self::converterParaFormatoApi($item);
            $params[] = $param;

            $novoUltimo = $param['id'];
        }

        if($ultimoId == 0) {
            self::doRequest(self::REQUEST_DELETE, "/respostas/destroy-all");
        }

        self::doRequest(self::REQUEST_POST, "/respostas/sync-lazy", $params);

        return $novoUltimo;
    }

    public static function converterParaFormatoApi($dados)
    {
        return [
            'id' => $dados->ID,
            'conteudo' => $dados->post_content,
            'status_wp' =>  $dados->post_status,
            'data' => $dados->post_date,
            'topico_id' => $dados->post_parent,
            'user_id' => $dados->post_author,
        ];
    }
}