<?php
include_once 'Api.php';

KLoader::model("DepoimentoModel");

class DepoimentoApi extends Api
{
    public static function salvar($dados)
    {
        $params = self::converterParaFormatoApi($dados);
        self::doRequest(self::REQUEST_POST, "/depoimentos/save", $params);
    }

    public static function excluir($id)
    {
        self::doRequest(self::REQUEST_DELETE, "/depoimentos/remove", ['id' => $id]);
    }

    public static function sincronizar()
    {
        $params = [];

        $items = DepoimentoModel::listar_depoimentos(false, false, false);

        foreach ($items as $item) {
            $param = self::converterParaFormatoApi($item);
            $params[] = $param;
        }

        self::doRequest(self::REQUEST_POST, "/depoimentos/sync", $params);
    }

    public static function converterParaFormatoApi($dados)
    {
        $imagem = null;
        if($imgId = get_field('imagem_depoimento', $dados->dep_id)) {
            $imagem = wp_get_attachment_image_src($imgId, 'full')[0];
        }

//        echo "Img Ids:<br>";
//        echo $imgId."<br>";
//        echo $imagem."<br>";
//        print_r($dados);
//        echo "<br><br>";

        return [
            'titulo' => $dados->post_title,
            'texto' => $dados->post_content,
            'slug' => $dados->dep_slug,
            'imagem' => $imagem,
            'link' => $dados->dep_url,
            'tipo' => $dados->dep_tipo ?: 1,
            'wp_id' => $dados->dep_id,
        ];
    }
}