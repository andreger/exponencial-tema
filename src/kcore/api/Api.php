<?php

class Api
{
    public const REQUEST_GET = "GET";
    public const REQUEST_POST = "POST";
    public const REQUEST_DELETE = "DELETE";

    public static function doRequest($tipo, $url, $params = null, $debug = false)
    {
        $ch = curl_init(self::getApiUrl() . $url);
        $params = $params ? json_encode($params) : null;

        curl_setopt( $ch, CURLOPT_HTTPHEADER, array
        (
            'Content-Type: application/json',
            'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiIzeHAwTjNuQzFATCJ9.J_-HyIZTva0cHJ_V9O04ZzsHVagVFhpZQTJPzmVDj-4',
        ) );

        curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, $tipo );
        curl_setopt( $ch, CURLOPT_TIMEOUT, 30 );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

        if($params) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        }

        $response = curl_exec( $ch );
        curl_close( $ch );

        if($debug) {
            echo "Tipo: " . $tipo . "<br><br>";
            echo "URL: " . $url . "<br><br>";
            echo "Params: " . $params . "<br><br>";
            echo "Response: " .  $response;
            exit;
        }

        return $response;
    }

    private static function getApiUrl()
    {
        if(is_homologacao()) {
            return "http://homol-core.exponencialconcursos.com.br/api";
        }

        if(is_producao()) {
            return "https://www.exponencialconcursos.com.br/api";
        }

        return "http://localhost:3000/api";
    }

}