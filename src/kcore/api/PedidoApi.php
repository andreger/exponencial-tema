<?php
include_once 'Api.php';

KLoader::model("PedidoModel");
KLoader::model("PedidoHelper");

class PedidoApi extends Api
{
    public static function salvar($dados)
    {
        $params = self::converterParaFormatoApi($dados);
        self::doRequest(self::REQUEST_POST, "/pedidos/save", $params);
    }

    public static function excluir($id)
    {
        self::doRequest(self::REQUEST_DELETE, "/pedidos/remove", ['id' => $id]);
    }

    public static function sincronizar($ultimoId = 0)
    {
        $params = [];

        $items = PedidoModel::listarProximos($ultimoId);

        $novoUltimo = 0;
        foreach ($items as $item) {
            $param = self::converterParaFormatoApi($item);
            $params[] = $param;

            $novoUltimo = $item->post_id;
        }

        if(!$ultimoId) {
            self::doRequest(self::REQUEST_DELETE, "/pedidos/destroy-all");
        }

        self::doRequest(self::REQUEST_POST, "/pedidos/sync-lazy", $params);

        return $novoUltimo;
    }

    public static function converterParaFormatoApi($dados)
    {
        $user_id = $dados->meta_value;
        return [
            'id' => $dados->post_id,
            'data' => $dados->post_date,
            'status_wp' => $dados->post_status,
            'user_id' => $user_id,
            'produtos' => self::anexarProdutos($dados->post_id, $user_id)
        ];
    }

    public static function anexarProdutos($pedidoId, $usuarioId)
    {
        $produtos = [];

        $items = PedidoModel::listar_produtos_do_pedido($pedidoId);

        foreach ($items as $item) {

            $data_expiracao = get_data_disponivel_ate($item->meta_value, $usuarioId, $pedidoId)
                ? converter_para_yyyymmdd(get_data_disponivel_ate($item->meta_value, $usuarioId, $pedidoId))
                : null;

            $produtos[] = [
                'produto_id' => $item->meta_value,
                'data_expiracao' => $data_expiracao
            ];
        }

        return $produtos;
    }
}