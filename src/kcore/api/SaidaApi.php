<?php
include_once 'Api.php';

KLoader::model("CapturaSaidaModel");

class SaidaApi extends Api
{
    public static function salvar($dados)
    {
        $params = self::converterParaFormatoApi($dados);
        self::doRequest(self::REQUEST_POST, "/saidas/save", $params);
    }

    public static function excluir($id)
    {
        self::doRequest(self::REQUEST_DELETE, "/saidas/remove", ['id' => $id]);
    }

    public static function sincronizar()
    {
        $params = [];

        $items = CapturaSaidaModel::listar_todas();

        foreach ($items as $item) {
            $param = self::converterParaFormatoApi($item);
            $params[] = $param;
        }

        self::doRequest(self::REQUEST_POST, "/saidas/sync", $params);
    }

    public static function converterParaFormatoApi($dados)
    {
        return [
            'id' => $dados->sai_id,
            'titulo' => $dados->sai_titulo,
            'descricao' => $dados->sai_html,
            'link' => $dados->sai_link,
            'fundo' => $dados->sai_fundo,
            'prioridade' => $dados->sai_prioridade,
            'data_inicio' =>  $dados->sai_data_inicio,
            'data_fim' =>  $dados->sai_data_fim,
            'urls_permitidas' => $dados->sai_urls,
            'palavras_permitidas' => $dados->sai_tags,
            'urls_proibidas' => $dados->sai_urls_negativas,
            'palavras_proibidas' => $dados->sai_palavras_negativas,
            'dias_expiracao' => $dados->sai_dias_expiracao,
//            'wp_id' => $dados->sai_id,
        ];
    }
}