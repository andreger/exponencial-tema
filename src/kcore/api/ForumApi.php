<?php
include_once 'Api.php';

KLoader::model("ForumModel");

class ForumApi extends Api
{
    public static function salvar($dados)
    {
        $params = self::converterParaFormatoApi($dados);
        self::doRequest(self::REQUEST_POST, "/forums/save", $params);
    }

    public static function excluir($id)
    {
        self::doRequest(self::REQUEST_DELETE, "/forums/remove", ['id' => $id]);
    }

    public static function sincronizar($ultimoId = 0)
    {
        $params = [];

        $items = ForumModel::listarProximos($ultimoId);

        $novoUltimo = 0;
        foreach ($items as $item) {
            $item->produto_id = ForumModel::getProdutoId($item->ID);

            $param = self::converterParaFormatoApi($item);
            $params[] = $param;

            $novoUltimo = $param['id'];
        }

        if($ultimoId == 0) {
            self::doRequest(self::REQUEST_DELETE, "/forums/destroy-all");
        }

        self::doRequest(self::REQUEST_POST, "/forums/sync-lazy", $params);

        return $novoUltimo;
    }

    public static function converterParaFormatoApi($dados)
    {
        return [
            'id' => $dados->ID,
            'nome' => $dados->post_title,
            'status_wp' =>  $dados->post_status,
            'produto_id' => $dados->produto_id
        ];
    }
}