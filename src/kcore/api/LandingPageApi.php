<?php
include_once 'Api.php';

KLoader::model("LandingPageModel");

class LandingPageApi extends Api
{
    public static function salvar($dados)
    {
        $params = self::converterParaFormatoApi($dados);
        self::doRequest(self::REQUEST_POST, "/landings/save", $params);
    }

    public static function excluir($id)
    {
        self::doRequest(self::REQUEST_DELETE, "/landings/remove", ['id' => $id]);
    }

    public static function sincronizar()
    {
        $params = [];

        $items = LandingPageModel::listar_publicadas();

        foreach ($items as $item) {
            $param = self::converterParaFormatoApi($item);
            $params[] = $param;
        }

        self::doRequest(self::REQUEST_POST, "/landings/sync", $params);
    }

    public static function converterParaFormatoApi($dados)
    {
        return [
            'id' => $dados->ID,
            'slug' => $dados->post_name,
            'titulo' => $dados->post_title,
            'conteudo' =>  $dados->post_content,
        ];
    }
}