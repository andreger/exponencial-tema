<?php
include_once 'Api.php';

KLoader::model("SlideModel");

class SlideApi extends Api
{
    public static function salvar($dados)
    {
        $params = self::converterParaFormatoApi($dados);
        self::doRequest(self::REQUEST_POST, "/slides/save", $params);
    }

    public static function excluir($id)
    {
        self::doRequest(self::REQUEST_DELETE, "/slides/remove", ['id' => $id]);
    }

    public static function sincronizar()
    {
        $params = [];

        $items = SlideModel::listar();

        foreach ($items as $item) {
            $p = json_decode($item->params);

            $item->imagem = $p->bg->image;
            $item->link = $p->seo->link;
            $item->is_ativo = $p->publish && $p->publish->state && $p->publish->state == 'unpublished' ? 0 : 1;

            $param = self::converterParaFormatoApi($item);
            $params[] = $param;
        }

        self::doRequest(self::REQUEST_POST, "/slides/sync", $params);
    }

    public static function converterParaFormatoApi($dados)
    {
        return [
            'id' => $dados->id,
            'imagem' => $dados->imagem,
            'link' => $dados->link,
            'ordem' => $dados->slide_order,
            'is_ativo' => $dados->is_ativo,
        ];
    }
}