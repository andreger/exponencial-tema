<?php
include_once 'Api.php';

KLoader::model("MateriaModel");

class MateriaApi extends Api
{
    public static function salvar($dados)
    {
        $params = self::converterParaFormatoApi($dados);

        self::doRequest(self::REQUEST_POST, "/materias/save", $params);
    }

    public static function excluir($id)
    {
        self::doRequest(self::REQUEST_DELETE, "/materias/remove", ['id' => $id]);
    }

    public static function sincronizar()
    {
        $params = [];

        $items = MateriaModel::listar_todas();

        foreach ($items as $item) {
            $param = self::converterParaFormatoApi($item);
            $params[] = $param;
        }

        self::doRequest(self::REQUEST_POST, "/materias/sync", $params);
    }

    public static function converterParaFormatoApi($dados)
    {
        return [
            'id' => $dados->term_id,
            'nome' => $dados->name,
            'slug' => $dados->slug,
            'qtde_cursos' => $dados->qtde_cursos ?: 0,
        ];
    }
}