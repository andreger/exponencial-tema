<?php
include_once 'Api.php';

KLoader::model("BlogModel");
KLoader::api("UsuarioApi");

class BlogApi extends Api
{
    public static function salvar($dados)
    {
        $params = self::converterParaFormatoApi($dados);
        self::doRequest(self::REQUEST_POST, "/blog-posts/save", $params);
    }

    public static function excluir($id)
    {
        self::doRequest(self::REQUEST_DELETE, "/blog-posts/remove", ['id' => $id]);
    }

    public static function sincronizar($ultimoId = 0)
    {
        $params = [];

        $items = BlogModel::listar_proximos($ultimoId);

        $novoUltimo = 0;
        foreach ($items as $item) {
            $param = self::converterParaFormatoApi($item);
            $params[] = $param;

            $novoUltimo = $item->blo_id;
        }

        if(!$ultimoId) {
            self::doRequest(self::REQUEST_DELETE, "/blog-posts/destroy-all");
        }

        self::doRequest(self::REQUEST_POST, "/blog-posts/sync-lazy", $params);

        return $novoUltimo;
    }

    public static function converterParaFormatoApi($dados)
    {
        $tipos = BlogModel::listar_tipos($dados->blo_id);

        return [
            'id' => $dados->blo_id,
            'slug' => $dados->blo_slug,
            'thumb' => $dados->blo_thumb,
            'imagem' => $dados->blo_thumb,
            'titulo' => $dados->post_title,
            'conteudo' =>  $dados->post_content,
            'resumo' =>  $dados->blo_resumo,
            'is_noticia' => in_array(TIPO_NOTICIA, $tipos),
            'is_artigo' => in_array(TIPO_ARTIGO, $tipos),
            'is_video' => in_array(TIPO_VIDEO, $tipos),
            'user_id' => $dados->user_id,
        ];
    }
}