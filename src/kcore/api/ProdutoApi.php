<?php
include_once 'Api.php';

KLoader::model("ProdutoModel");
KLoader::model("ProdutoHelper");

class ProdutoApi extends Api
{
    public static function salvar($dados)
    {
        $params = self::converterParaFormatoApi($dados);
        self::doRequest(self::REQUEST_POST, "/produtos/save", $params);
    }

    public static function excluir($id)
    {
        self::doRequest(self::REQUEST_DELETE, "/produtos/remove", ['id' => $id]);
    }

    public static function sincronizar($ultimoId = 0)
    {
        $params = [];

        $items = ProdutoModel::listarProximos($ultimoId);

        $novoUltimo = 0;
        foreach ($items as $item) {
            $param = self::converterParaFormatoApi($item);
            $params[] = $param;

            $novoUltimo = $item->post_id;
        }

        if(!$ultimoId) {
            self::doRequest(self::REQUEST_DELETE, "/produtos/destroy-all");
        }

        self::doRequest(self::REQUEST_POST, "/produtos/sync-lazy", $params);

        return $novoUltimo;
    }

    public static function anexarConfig($id)
    {
        $config = [
            'produto_id' => $id,
            'acao' => get_post_meta($id, 'chamada_acao', true) && get_post_meta($id, 'chamada_acao', true) == "1",
            'texto_acao' => get_post_meta($id, 'botao_comprar', true),
            'notificar_vendas' => get_post_meta($id, 'notificar_vendas', true) && get_post_meta($id, 'notificar_vendas', true) == 'yes',
            'mostrar_garantia' => get_post_meta($id, 'mostrar_garantia', true) && get_post_meta($id, 'mostrar_garantia', true) == 'yes',
            'liberar_aulas_gradualmente' => get_post_meta($id, 'liberar_gradualmente', true) && get_post_meta($id, 'liberar_gradualmente', true) == 'yes',
            'ocultar_tipo' => get_post_meta($id, 'produto_ocultar_tipo_produto', true) && get_post_meta($id, 'produto_ocultar_tipo_produto', true) == 'yes',
            'ocultar_professores' => get_post_meta($id, 'produto_ocultar_professores', true) && get_post_meta($id, 'produto_ocultar_professores', true) == 'yes',
            'ocultar_materias' => get_post_meta($id, 'produto_ocultar_materias', true) && get_post_meta($id, 'produto_ocultar_materias', true) == 'yes',
            'ocultar_concursos' => get_post_meta($id, 'produto_ocultar_concursos', true) && get_post_meta($id, 'produto_ocultar_concursos', true) == 'yes',
            'ocultar_quantidade' => get_post_meta($id, 'produto_ocultar_quantidade', true) && get_post_meta($id, 'produto_ocultar_quantidade', true) == 'yes',
            'ocultar_carga_horaria' => get_post_meta($id, 'produto_ocultar_carga_horaria', true) && get_post_meta($id, 'produto_ocultar_carga_horaria', true) == 'yes',
            'ocultar_parcelamento' => get_post_meta($id, 'produto_ocultar_parcelamento', true) && get_post_meta($id, 'produto_ocultar_parcelamento', true) == 'yes',
            'fundir_arquivos' => get_post_meta($id, 'produto_fundir_arquivos', true) && get_post_meta($id, 'produto_fundir_arquivos', true) == 'yes',
            'incluir_capa' => get_post_meta($id, 'produto_incluir_capa', true) && get_post_meta($id, 'produto_fundir_arquivos', true) == 'yes',
        ];

        return $config;
    }

    public static function anexarMaterias($id)
    {
        $materias = [];

        $items = ProdutoModel::listar_materias($id);

        foreach ($items as $item) {
            $materias[] = $item->mat_id;
        }

        return $materias;
    }

    public static function anexarConcursos($id)
    {
        $concursos = [];

        $items = ProdutoModel::listar_concursos($id);

        foreach ($items as $item) {
            $concursos[] = $item->con_id;
        }

        return $concursos;
    }

    public static function anexarUsuarios($id)
    {
        $professores = [];

        $items = ProdutoModel::listar_professores($id);

        foreach ($items as $item) {
            $professores[] = $item->ID;
        }

        return $professores;
    }

    public static function anexarAreas($id)
    {
        $areas = [];

        $items = ProdutoModel::listar_areas($id);

        foreach ($items as $item) {
            $areas[] = $item->term_id;
        }

        return $areas;
    }

    public static function converterParaFormatoApi($dados)
    {
        return [
            'id' => $dados->post_id,
            'slug' => $dados->post_name,
            'nome' => $dados->post_title,
            'descricao' =>  $dados->post_content,
            'imagem' =>  $dados->guid,
            'preco' =>  $dados->pro_preco_sem_desconto,
            'preco_final' =>  $dados->pro_preco,
            'vendas_ate' =>  $dados->pro_vendas_ate,
            'disponivel_ate' =>  $dados->pro_disponivel_ate,
            'tempo_acesso' =>  $dados->pro_tempo_acesso,
            'qtde_aulas' =>  $dados->pro_qtde_aulas,
            'qtde_esquemas' =>  $dados->pro_qtde_esquemas,
            'qtde_questoes' =>  $dados->pro_qtde_questoes,
            'publicado_em' =>  $dados->post_date,
            'carga_horaria' =>  $dados->pro_carga_horaria,
            'prioridade' =>  $dados->pro_prioridade,
            'oculto' =>  $dados->pro_is_visivel ? false : true,
            'is_pacote' =>  $dados->pro_is_pacote,
            'is_pacote_curso' =>  $dados->pro_is_pacote_curso,
            'is_pacote_sq' =>  $dados->pro_is_pacote_sq,
            'is_destaque' =>  $dados->pro_is_destaque,
            'is_simulado' =>  $dados->pro_is_simulado,
            'is_caderno' =>  $dados->pro_is_caderno,
            'is_mapa_mental' =>  $dados->pro_is_mapa_mental,
            'is_resumo' =>  $dados->pro_is_resumo,
            'is_audiobook' =>  $dados->pro_is_audiobook,
            'is_pdf' =>  $dados->pro_is_pdf,
            'is_video' =>  $dados->pro_is_video,
            'is_turma_coaching' =>  $dados->pro_is_turma_coaching,
            'is_assinatura_sq' =>  $dados->pro_is_assinatura_sq,
            'is_premium' =>  $dados->pro_is_premium,
            'is_coaching' =>  $dados->pro_is_coaching,
            'is_com_correcao' =>  $dados->pro_com_correcao,
            'config' => self::anexarConfig($dados->post_id),
            'materias' => self::anexarMaterias($dados->post_id),
            'concursos' => self::anexarConcursos($dados->post_id),
            'usuarios' => self::anexarUsuarios($dados->post_id),
            'areas' => self::anexarAreas($dados->post_id)
        ];
    }
}