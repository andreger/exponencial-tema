<?php
include_once 'Api.php';

KLoader::model("RodapeItemModel");

class RodapeLinkApi extends Api
{
    public static function sincronizar()
    {
        $params = [];

        for($i = 1; $i <= 3; $i++) {
            $items = RodapeItemModel::list($i);

            foreach ($items as $item) {

                $url_a = parse_url($item->roi_url);

                $link = [
                    'linha' => $item->roi_index,
                    'coluna' => $item->roi_coluna - 1,
                    'texto' => $item->roi_titulo,
                    'url' => $url_a['path']
                ];

                $params[] = $link;
            }
        }

        self::doRequest(self::REQUEST_POST, "/rodape-links/sync", $params);
    }

}