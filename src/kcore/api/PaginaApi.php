<?php
include_once 'Api.php';

KLoader::model("PaginaModel");

class PaginaApi extends Api
{
    public static function salvar($dados)
    {
        $params = self::converterParaFormatoApi($dados);
        self::doRequest(self::REQUEST_POST, "/paginas/save", $params);
    }

    public static function excluir($id)
    {
        self::doRequest(self::REQUEST_DELETE, "/paginas/remove", ['id' => $id]);
    }

    public static function sincronizar()
    {
        $params = [];

        $items = PaginaModel::listar();

        foreach ($items as $item) {
            $param = self::converterParaFormatoApi($item);
            $params[] = $param;
        }

        self::doRequest(self::REQUEST_POST, "/paginas/sync", $params);
    }

    public static function converterParaFormatoApi($dados)
    {
        return [
            'id' => $dados->ID,
            'slug' => $dados->post_name,
            'titulo' => $dados->post_title,
            'conteudo' =>  $dados->post_content,
        ];
    }
}