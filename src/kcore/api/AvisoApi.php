<?php
include_once 'Api.php';

KLoader::model("AvisoModel");

class AvisoApi extends Api
{
    public static function salvar($dados)
    {
        $params = self::converterParaFormatoApi($dados);
        self::doRequest(self::REQUEST_POST, "/avisos/save", $params);
    }

    public static function excluir($id)
    {
        self::doRequest(self::REQUEST_DELETE, "/avisos/remove", ['id' => $id]);
    }

    public static function sincronizar()
    {
        $params = [];

        $items = AvisoModel::listar_todos();

        foreach ($items as $item) {
            $param = self::converterParaFormatoApi($item);
            $params[] = $param;
        }

        self::doRequest(self::REQUEST_POST, "/avisos/sync", $params);
    }

    public static function converterParaFormatoApi($dados)
    {
        return [
            'id' => $dados['id'],
            'titulo' => $dados['titulo'],
            'descricao' => $dados['mensagem'],
            'link' => $dados['link'],
            'fundo' => $dados['fundo'],
            'data_inicio' =>  $dados['data_inicio'],
            'data_fim' =>  $dados['data_fim'],
            'urls_permitidas' => $dados['urls'],
            'palavras_permitidas' => $dados['palavras'],
            'urls_proibidas' => $dados['urls_negativas'],
            'palavras_proibidas' => $dados['palavras_negativas'],
            'dias_expiracao' => $dados['dias_expiracao'],
//            'wp_id' => $dados['id'],
        ];
    }
}