<?php
include_once 'Api.php';

KLoader::model("TopicoModel");

class TopicoApi extends Api
{
    public static function salvar($dados)
    {
        $params = self::converterParaFormatoApi($dados);
        self::doRequest(self::REQUEST_POST, "/topicos/save", $params);
    }

    public static function excluir($id)
    {
        self::doRequest(self::REQUEST_DELETE, "/topicos/remove", ['id' => $id]);
    }

    public static function sincronizar($ultimoId = 0)
    {
        $params = [];

        $items = TopicoModel::listarProximos($ultimoId);

        $novoUltimo = 0;
        foreach ($items as $item) {
            $param = self::converterParaFormatoApi($item);
            $params[] = $param;

            $novoUltimo = $param['id'];
        }

        if($ultimoId == 0) {
            self::doRequest(self::REQUEST_DELETE, "/topicos/destroy-all");
        }

        self::doRequest(self::REQUEST_POST, "/topicos/sync-lazy", $params);

        return $novoUltimo;
    }

    public static function converterParaFormatoApi($dados)
    {
        return [
            'id' => $dados->ID,
            'nome' => $dados->post_title,
            'status_wp' =>  $dados->post_status,
            'forum_id' => $dados->post_parent,
            'user_id' => $dados->post_author,
        ];
    }
}