<?php
include_once 'Api.php';

KLoader::model("ConcursoModel");

class ConcursoApi extends Api
{
    public static function salvar($dados)
    {
        $params = self::converterParaFormatoApi($dados);

        self::doRequest(self::REQUEST_POST, "/concursos/save", $params);
    }

    public static function excluir($id)
    {
        self::doRequest(self::REQUEST_DELETE, "/concursos/remove", ['id' => $id]);
    }

    public static function sincronizar()
    {
        $params = [];

        $items = ConcursoModel::listar_todos();

        foreach ($items as $item) {
            $param = self::converterParaFormatoApi($item);
            $params[] = $param;
        }

        self::doRequest(self::REQUEST_POST, "/concursos/sync", $params);
    }

    public static function converterParaFormatoApi($dados)
    {
        return [
            'id' => $dados->con_id,
            'nome' => $dados->post_title,
            'slug' => $dados->con_slug,
            'descricao' => $dados->post_content,
            'status' => $dados->con_status,
            'thumb' => $dados->con_thumb,
            'data_prova' => $dados->con_data_prova,
            'banca' => $dados->con_banca,
            'qtde_cursos' => $dados->con_qtde_cursos ?: 0,
        ];
    }
}