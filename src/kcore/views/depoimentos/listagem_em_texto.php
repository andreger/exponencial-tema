<div class="container <?= $bg_color ?>">

<?php
    if($titulo)
    {
        echo get_cabecalho_secao('', $titulo, '', 'text-blue');
    }
?>

<?php if($apresentacao): ?>
    <div class="row pb-5"><?= $apresentacao ?></div>
<?php endif; ?>

<?php if($depoimentos) : ?>
    
    <?php if($paginacao): ?>
        <!--<div class="row"><?= $paginacao ?></div>-->
    <?php endif; ?>
    
    <?php $i = 0; ?>
	<?php foreach ($depoimentos as $depoimento) : ?>
        <?php
            //Verifica o tipo de txto
            if($depoimento->dep_tipo == DEPOIMENTO_DEPOIMENTO_TEXTO)
            {
                $is_entrevista = FALSE;
            }
            elseif($depoimento->dep_tipo == DEPOIMENTO_ENTREVISTA_TEXTO)
            {
                $is_entrevista = TRUE;
            }
            else
            {   //Aqui só deveriam vir os "em texto"
                continue;
            }

            //Verifica a imagem do texto
            if(!($img_src = get_field('imagem_depoimento', $depoimento->dep_id)))
            {
                $img_src = get_tema_image_url('exponencial-simbolo.png');//Ver se precisa do PB
            }
            else
            {
                $img_src = wp_get_attachment_image_src($img_src, [100, 100])[0];
            }

        ?>
        <div class="row pb-3">
            <div class="col-2 text-center">
                <a href="<?= $is_entrevista ? UrlHelper::get_entrevista_url($depoimento->dep_slug) : UrlHelper::get_depoimento_url($depoimento->dep_slug)  ?>">
                    <img style="border-radius: 50%;" alt="<?= $depoimento->dep_slug ?>" src="<?= $img_src ?>" />
                </a>
            </div>
            <div class="col-10 align-self-center">
                
                <div class="col-12 font-weight-bold"><?= $depoimento->post_title ?></div>
                <div class="col-12"><?= limitar_texto(sanitizar_string($depoimento->post_content, TRUE), 300) ?></div>

            </div>
            <div class="col-12 text-right">
                <a href="<?= $is_entrevista ? UrlHelper::get_entrevista_url($depoimento->dep_slug) : UrlHelper::get_depoimento_url($depoimento->dep_slug)  ?>">
                    <img alt="veja-mais" src="<?= get_tema_image_url('botao-veja-mais.png'); ?>" />
                </a>
            </div>
        </div>
		
	<?php $i++; ?>
    <?php endforeach; ?>

    <?php if($paginacao): ?>
        <div class="row pb-3 pt-5"><?= $paginacao ?></div>
    <?php endif; ?>

<?php else : ?>
	<div class="text-center pb-3">Nenhum depoimento ou entrevista foi encontrado</div>
<?php endif ?>
    <?php if ($voltar): ?>
        <div class="col-12 text-center">
            <a class="btn u-btn-primary mb-4" href="#" onclick="history.back();">&nbsp;Voltar&nbsp;</a>
        </div>
    <?php endif; ?>
</div>