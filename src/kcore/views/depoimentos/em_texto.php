<?php

    get_header();

    //Verifica o tipo de txto
    $is_entrevista = FALSE;
    if($depoimento->dep_tipo == DEPOIMENTO_ENTREVISTA_TEXTO)
    {
        $is_entrevista = TRUE;
    }

    //Verifica a imagem do texto
    if(!($img_src = get_field('imagem_depoimento', $depoimento->dep_id)))
    {
        $img_src = get_tema_image_url('exponencial-simbolo.png');//TODO: Ver se precisa do PB
    }
    else
    {
        $img_src = wp_get_attachment_image_src($img_src)[0];
    }

    echo get_cabecalho_secao('', $is_entrevista?"Entrevista":"Depoimento");

?>

<div class="container">

<div class="row pb-3">
    <div class="col-2 text-center">
        <img style="border-radius: 50%;" alt="<?= $depoimento->dep_slug ?>" src="<?= $img_src ?>" />
    </div>
    <div class="col-10">
        
        <div class="col-12 font-weight-bold text-blue text-left pb-3"><?= $depoimento->post_title ?></div>
        <div class="col-12"><?= $depoimento->post_content ?></div>

    </div>
    <div class="col-12 text-center">
        <a class="btn u-btn-primary mb-4" href="#" onclick="history.back();">&nbsp;Voltar&nbsp;</a>
    </div>
</div>

</div>

<?php get_footer(); ?>