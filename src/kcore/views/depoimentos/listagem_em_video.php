<div class="container <?= $bg_color ?>">
<?php
    if($titulo)
    {
        echo get_cabecalho_secao('', $titulo, '', 'text-blue');
    }
?>
<?php if($apresentacao): ?>
    <div class="row pb-5"><?= $apresentacao ?></div>
<?php endif; ?>

<?php if($depoimentos) : ?>
    
    <?php if($paginacao): ?>
        <!--<div class="row pb-3"><?= $paginacao ?></div>-->
    <?php endif; ?>
    
    <div class="row align-items-end">

        <?php $i = 0; ?>
        <?php foreach ($depoimentos as $depoimento) : ?>
            <?php
                //Verifica o tipo de txto
                if($depoimento->dep_tipo == DEPOIMENTO_DEPOIMENTO_VIDEO)
                {
                    $is_entrevista = FALSE;
                }
                elseif($depoimento->dep_tipo == DEPOIMENTO_ENTREVISTA_VIDEO)
                {
                    $is_entrevista = TRUE;
                }
                else
                {   //Aqui só deveriam vir os "em video"
                    continue;
                }

                //Verifica a imagem do texto
                if(!($img_src = get_field('imagem_depoimento', $depoimento->dep_id)))
                {
                    $img_src = get_tema_image_url('exponencial-simbolo.png');//Ver se precisa do PB
                }
                else
                {
                    $img_src = wp_get_attachment_image_src($img_src, 'full')[0];
                }

            ?>
            <div class="col-4 pb-3 text-center">
                <div class="col-12">
                    <a href="<?= $depoimento->dep_url ?>" data-lity>
                        <img class="img-fluid" alt="<?= $depoimento->dep_slug ?>" src="<?= $img_src ?>" />
                    </a>
                </div>
                <div class="col-12 depoimento-video">
                    <a href="<?= $depoimento->dep_url ?>" data-lity>
                        <?= $depoimento->post_title ?>
                    </a>
                </div>
            </div>
            
        <?php $i++; ?>
        <?php endforeach; ?>

    </div>
    
    <?php if($paginacao): ?>
        <div class="row pb-3 pt-5"><?= $paginacao ?></div>
    <?php endif; ?>

<?php else : ?>
	<div class="text-center pb-3">Nenhum depoimento ou entrevista foi encontrado</div>
<?php endif ?>
    
    <?php if ($voltar): ?>
        <div class="col-12 text-center">
            <a class="btn u-btn-primary mb-4" href="#" onclick="history.back();">&nbsp;Voltar&nbsp;</a>
        </div>
    <?php endif; ?>
    
</div>

<?= vimeo_player_assets() ?>