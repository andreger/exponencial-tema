<form method="post" action="/relatorio-pedidos-premium">

	<div class="row">

		<div class="col-4">
			<strong>Pedido ID:</strong>
			<input class="form-control inteiro" type="text" name="pedido" value="<?= $filtros['pedido'] ?>" />
		</div>

		<div class="col-4">
			<strong>Produto Premium ID:</strong>
			<input class="form-control inteiro" type="text" name="produto" value="<?= $filtros['produto'] ?>" />
		</div>

		<div class="col-4">
			<strong>Item ID:</strong>
			<input class="form-control inteiro" type="text" name="item" value="<?= $filtros['item'] ?>" />
		</div>

	</div>
	
	<div class="row">

		<div class="col-4">
			<strong>Pedido Oculto ID:</strong>
			<input class="form-control inteiro" type="text" name="pedido_oculto" value="<?= $filtros['pedido_oculto'] ?>" />
		</div>
		<div class="col-4">
			<strong>Data de Requisição Início:</strong>
			<input class="form-control date-picker" type="text" name="data_req_ini" value="<?= $filtros['data_req_ini'] ?>" />
		</div>
		<div class="col-4">
			<strong>Data Requisição Fim:</strong>
			<input class="form-control date-picker" type="text" name="data_req_fim" value="<?= $filtros['data_req_fim'] ?>" />
		</div>

	</div>

	<div class="row mt-4">	
		<div class="col-12">
			<input class="btn u-btn-blue" type="submit" name="filtrar" value="Filtrar">
			<input class="btn u-btn-blue" type="submit" name="export-excel" value="Exportar XLS">
			<input class="btn u-btn-blue" type="submit" name="limpar" value="Limpar">
		</div>
	</div>

</form>

<?= get_date_picker(".date-picker") ?>