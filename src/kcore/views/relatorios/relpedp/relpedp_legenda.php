<div class="row">
    <div class="col-12 font-weight-bold">Legenda:</div>
    <div class="col-12"><span class="font-weight-bold">Pedido ID -</span> Número do pedido (somente pedidos que contém produto premium).</div>
    <div class="col-12"><span class="font-weight-bold">Produto Premium ID -</span> Id do produto premium.</div>
    <div class="col-12"><span class="font-weight-bold">Nome Produto Premium -</span> Nome do produto premium.</div>
    <div class="col-12"><span class="font-weight-bold">Item ID -</span> ID de um produto que está contido no produto premium.</div>
    <div class="col-12"><span class="font-weight-bold">Nome do ID -</span> No do produto representado pelo Item ID.</div>
    <div class="col-12"><span class="font-weight-bold">Pedido Oculto -</span> Número do pedido gerado, de forma transparente, na requisição de acesso ao item.</div>
    <div class="col-12"><span class="font-weight-bold">Data de Requisição -</span> Data de requisição de acesso ao item solicitada pelo usuário.</div>
</div>