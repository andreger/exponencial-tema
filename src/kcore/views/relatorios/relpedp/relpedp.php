<?php
get_header();
?>

<div class="container-fluid pt-1 pt-md-4">
	
	<div class="mb-5 col-12 text-center text-md-left text-blue">
		<h1>Relatório de Pedidos Premium</h1>
	</div>

	<div class="container">
		<?php KLoader::view("relatorios/relpedp/relpedp_filtro", ["filtros" => $filtros])?>
	</div>

	<?php KLoader::view("relatorios/relpedp/relpedp_tabela", ["premiums" => $premiums, "paginacao" => $paginacao]) ?>

	<?php KLoader::view("relatorios/relpedp/relpedp_legenda", [] ) ?>

</div>

<?php get_footer(); ?>