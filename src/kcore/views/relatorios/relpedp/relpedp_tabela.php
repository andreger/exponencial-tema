<div class="mt-5 text-center">
	<?php if(!$premiums) : ?>
		Não foram encontrados produtos com o filtro selecionado.
	<?php else : ?>
		
		<?= $paginacao ?>

		<table class='table-relatorio table table-bordered'>
			<tr class="font-10 bg-blue text-white">
				<th>Pedido ID</th>
				<th>Produto Premium ID</th>
				<th class="text-left w-50">Nome Produto Premium</th>
				<th>Item ID</th>
				<th class="text-left w-50">Nome do Item</th>
				<th>Pedido Oculto</th>
				<th>Data de Requisição</th>
			</tr>
			
			<?php foreach ($premiums as $premium) : ?>
			<tr >
				<td><?= $premium->pedido_id ?></td>
				<td><?= $premium->produto_id ?></td>
				<td class="text-left"><?= $premium->nome_produto ?></td>
				<td><?= $premium->item_id ?></td>
				<td class="text-left"><?= $premium->nome_item ?></td>
				<td><?= $premium->pedido_oculto_id ?></td>
				<td><?= converter_para_ddmmyyyy_HHiiss($premium->prl_data) ?></td>
			</tr>
			<?php endforeach; // <- End looping premiums ?>	
		</table>
		
		<?= $paginacao ?>

		<div class="mb-2 clearfix"></div>

	<?php endif; // <- End if premiums ?>
</div>
