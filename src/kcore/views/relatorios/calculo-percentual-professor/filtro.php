<form method="post" action="/relatorio-calculo-percentual-professor">	
    <div class="row mt-3">	
		<div class="col-md-6 col-sm-12 mb-2">
			<strong>Produtos Premium:</strong>
			<?= UiSelectHelper::get_select_html("produtos_selecionados[]", $produtos_combo, $filtros["produtos"], true, ["class" => "filtro", "style" => "width: 100%"]) ?>
		</div>
		<div class="col-md-6 col-sm-12 mb-2">
			<strong>Período:</strong>
			<?= UiSelectHelper::get_select_html("anos_meses_selecionados", $anos_meses_combo, $filtros["anos_meses"], false, ["class" => "filtro form-control", "style" => "width: 100%"]) ?>
		</div>
	</div>
	
	<div class="row mt-3">	
		<!-- <div class="col-xs-12 col-sm-12 mb-2">
			<div>
				<strong>Excluir:</strong>&nbsp;
			
			<input type="checkbox" id="excluir_cadernos" name="excluir_cadernos" value="1" <?= $filtros['excluir_cadernos'] ? 'checked' : '' ?> > <label class="mr-3" for="excluir_cadernos">Cadernos</label>
			<input type="checkbox" id="excluir_gratuitos" name="excluir_gratuitos" value="1" <?= $filtros['excluir_gratuitos'] ? 'checked' : '' ?> > <label class="mr-3" for="excluir_gratuitos">Produtos Grátis</label>
			<input type="checkbox" id="excluir_mapas" name="excluir_mapas" value="1" <?= $filtros['excluir_mapas'] ? 'checked' : '' ?> > <label class="mr-3" for="excluir_mapas">Mapas Mentais</label>
		</div> 
		</div>-->
		<div class="col-xs-12 col-sm-12 mb-2">
			<input class="btn u-btn-blue" type="submit" name="filtrar" value="Filtrar">
			<input class="btn u-btn-blue" type="submit" name="export-excel" value="Exportar XLS">
		</div>
    </div>
</form>