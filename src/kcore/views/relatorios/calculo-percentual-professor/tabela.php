<div class="container">
    <div class="mt-5 text-center">
    	<?php if(!$premiums) : ?>
    		Não foram encontrados produtos com o filtro selecionado.
    	<?php else : ?>
    		<table class='table-relatorio table table-bordered'>
    			<tr class="font-10 bg-blue text-white">
    				<th class="text-left">Período</th>
    				<th class="text-left">Produto</th>
    				<th class="text-left">Id do Produto</th>
    				<th>Valor</th>
    				<th>Data de Processamento</th>
    			</tr>
    			
    			<?php foreach ($premiums as $premium) : ?>
    			<tr>
    				<td><?=  periodo_mes_ano($premium['ano_mes']) ?> </td>
    				<td class="text-left">
    					<a href="/relatorio-calculo-percentual-professor-detalhe?pid=<?= $premium['id'] ?>&am=<?= $premium['ano_mes']?>"><?= $premium['nome'] ?></a>
    				</td>
    				<td><?= $premium['id'] ?></td>
    				<td><?= moeda($premium['valor']) ?></td>
    				<td><?= $premium['reprocessado_em'] ?></td>
    			</tr>
    			<?php endforeach; // <- End looping premiums ?>	
    		</table>
    	<?php endif; // <- End if premiums ?>
    </div>
</div>