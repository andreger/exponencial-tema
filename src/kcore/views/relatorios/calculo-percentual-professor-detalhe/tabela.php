<div class="mt-5 text-center">
	<?php if(!$premium) : ?>
		Não foram encontrados produtos com o filtro selecionado.
	<?php else : ?>
		<table class='table-relatorio table table-bordered'>
			<tr class="font-10 bg-blue text-white">
				<th class="text-left">Período</th>
				<th>Produto</th>
				<th>ID do Produto</th>
				<th>Status</th>
				<th>Expira em</th>
				<th>Áreas</th>
				<th>Concurso</th>
				<th>Professor</th>
				<th>Preço cadastrado</th>
				<th>% Professor</th>
				<th>Valor de produto a venda</th>
				<th>% Cota Professor</th>
			</tr>
			
			<tr class="font-weight-bold"> 
				<td><?= $premium['info']['data'] ?></td>
				<td><?= $premium['info']['nome'] ?></td>
				<td><?= $premium['info']['id'] ?></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td><?= moeda($premium['info']['total']) ?></td>
				<td>100 %</td>
			</tr>
			
			<?php foreach ($premium['itens'] as $item) : ?>
			<tr >
				<td><?= $item['data'] ?></td>
				<td><?= $item['produto'] ?></td>
				<td><?= $item['produto_id'] ?></td>
				<td><?= $item['status'] ?></td>
				<td><?= $item['disponivel_ate'] ?></td>
				<td><?= $item['areas'] ?></td>
				<td><?= $item['concursos'] ?></td>
				<td><?= $item['professor'] ?></td>
				<td><?= $item['preco'] ?></td>
				<td><?= $item['percentual'] ?></td>
				<td><?= $item['valor'] ?></td>
				<td><?= $item['cota'] ?></td>
			</tr>
			<?php endforeach; // <- End looping premiums ?>
		</table>
	<?php endif; // <- End if premiums ?>
</div>
