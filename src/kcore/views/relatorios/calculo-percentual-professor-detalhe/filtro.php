<form method="get" action="/relatorio-calculo-percentual-professor-detalhe">	
    <div class="row mt-3">	
		<div class="col-md-6 col-sm-12 mb-2">
			<strong>Produtos Premium:</strong>
			<?= UiSelectHelper::get_select_html("pid", $produtos_combo, $_GET['pid'], false, ["class" => "filtro form-control", "style" => "width: 100%"]) ?>
		</div>
		<div class="col-md-6 col-sm-12 mb-2">
			<strong>Período:</strong>
			<?= UiSelectHelper::get_select_html("am", $anos_meses_combo, $_GET['am'], false, ["class" => "filtro form-control", "style" => "width: 100%"]) ?>
		</div>
	</div>
	
	<div class="row mt-3">	
		<div class="col-xs-12 col-sm-12 mb-2">
			<input class="btn u-btn-blue" type="submit" name="filtrar" value="Filtrar">
			<input class="btn u-btn-blue" type="submit" name="export-excel" value="Exportar XLS">
		</div>
    </div>
</form>