<?php
get_header();
?>
<link href="/questoes/assets-admin/js/plugins/select2/select2.css" rel="stylesheet">
<link href="/questoes/assets-admin/js/plugins/select2/select2-bootstrap.css" rel="stylesheet">
<script src="/questoes/assets-admin/js/plugins/select2/select2.js"></script>

<div class="container-fluid pt-1 pt-md-4">
	<div class="mb-5 col-12 text-center text-md-left text-blue">
		<h1>Cálculo do % por Professor (detalhe)</h1>
	</div>
</div>

<div class="container">	
	<div class="row mt-3" style="display: none;">
		<form action="/relatorio-cursos" method="get">
			<input type="hidden" name="export-excel" value="1">
			<input type="submit" value="Exportar para Excel" name="submit" />
		</form>
	</div>

	<?php KLoader::view("relatorios/calculo-percentual-professor-detalhe/filtro", ["filtros" => $filtros, "produtos_combo" => $produtos_combo, "anos_meses_combo" => $anos_meses_combo])?>
</div>

<?php KLoader::view("relatorios/calculo-percentual-professor-detalhe/tabela", ["premium" => $premium]) ?>

<script>
jQuery(document).ready(function(){
	jQuery(".filtro").select2();

	jQuery(".relpp-pp").click(function (e) {
		e.preventDefault();
		var num = jQuery(this).data("num"),
			estilo = ".item-pp-" + num;

		jQuery(estilo).toggle();
	});
});
</script>

<?php get_footer(); ?>