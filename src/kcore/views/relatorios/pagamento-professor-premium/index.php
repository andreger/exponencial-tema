<?php
get_header();
?>
<link href="/questoes/assets-admin/js/plugins/select2/select2.css" rel="stylesheet">
<link href="/questoes/assets-admin/js/plugins/select2/select2-bootstrap.css" rel="stylesheet">
<script src="/questoes/assets-admin/js/plugins/select2/select2.js"></script>

<div class="container-fluid pt-1 pt-md-4">
	<div class="mb-5 col-12 text-center text-md-left text-blue">
		<h1>Relatório de Pagamento de Professor (Premium)</h1>
	</div>
</div>

<div class="container">	
	<div class="row mt-3" style="display: none;">
		<form action="/relatorio-cursos" method="get">
			<input type="hidden" name="export-excel" value="1">
			<input type="submit" value="Exportar para Excel" name="submit" />
		</form>
	</div>

	<?php KLoader::view("relatorios/pagamento-professor-premium/filtro", ["anos_meses_combo" => $anos_meses_combo, "professores_combo" => $professores_combo])?>
</div>

<?php KLoader::view("relatorios/pagamento-professor-premium/tabela", ["premium" => $premium, "somatorios" => $somatorios]) ?>

<script>
jQuery(document).ready(function(){
	jQuery(".filtro").select2();

	jQuery(".linha-principal").click(function (e) {
		e.preventDefault();
		var estilo = ".linha-secundaria-" + jQuery(this).data("professor-id");
		jQuery(estilo).toggle();
	});
});
</script>

<?php get_footer(); ?>