<div class="mt-5 mb-5 text-center">
	<?php if(!$premium) : ?>
		Nenhum dado a ser exibido.
	<?php else : ?>
		<table class='table-relatorio table table-bordered'>
			<tr class="font-10 bg-blue text-white">
				<th class="text-left">Professor</th>
				<th>Produto</th>
				<th>ID do Produto</th>
				<th>Período</th>
				<th>Total de Vendas</th>
				<th>Total Descontos</th>
				<th>Total Reembolsado</th>
				<th>Total Pago</th>
				<th>Total Gateway</th>
				<th>Total Afiliados</th>
				<th>Total Imposto</th>
				<th>Total Líquido</th>
				<th>Cota SQ</th>
				<th>Cota Professor</th>
				<th>Minha Cota</th>
				<th>Valor Minha Cota</th>
				<th>% Lucro Professor</th>
				<th>Total Professor</th>
				<th>Acumulado não pago</th>
				<th>Valor Acumulado próximo mês</th>
				<th>Valor a receber</th>
			</tr>
			
			<?php foreach ($premium as $item) : ?>
    			<tr class="font-weight-bold table-primary linha-principal" data-professor-id="<?= $item['professor_id'] ?>"> 
    				<td><?= $item['nome'] ?></td>
    				<td>Total</td>
    				<td></td>
    				<td><?= $item['periodo'] ?></td>
    				<td></td>
    				<td></td>
    				<td></td>
    				<td></td>
    				<td></td>
    				<td></td>
    				<td></td>
    				<td></td>
    				<td></td>
    				<td></td>
                    <td></td>
    				<td><?= moeda($item['subtotal'], true, 4) ?></td>
    				<td></td>
    				<td><?= moeda($item['total'], true, 4) ?></td>
    				<td><?= moeda($item['acumulado_nao_pago'], true, 4) ?></td>
    				<td><?= moeda($item['acumulado_proximo_mes'], true, 4) ?></td>
    				<td><?= moeda($item['valor_a_receber'], true, 4) ?></td>
    			</tr>
			
				<?php if($item['produtos']) : ?>
        			<?php foreach ($item['produtos'] as $linha) : ?>
            			<tr style="display:none" class="linha-secundaria-<?= $item['professor_id'] ?>"> 
            				<td><?= $item['nome'] ?></td>
            				<td><?= $linha['produto'] ?></td>
            				<td><?= $linha['id'] ?></td>
            				<td><?= $item['periodo'] ?></td>
            				<td><?= $linha['total_vendas'] ?></td>
            				<td><?= $linha['total_descontos'] ?></td>
            				<td><?= $linha['total_reembolsado'] ?></td>
            				<td class="font-weight-bold"><?= $linha['total_pago'] ?></td>
            				<td><?= $linha['total_pagseguro'] ?></td>
            				<td><?= $linha['total_afiliados'] ?></td>
            				<td><?= $linha['total_imposto'] ?></td>
            				<td class="font-weight-bold"><?= $linha['total_liquido'] ?></td>
            				<td><?= $linha['cota_sq'] ?></td>
            				<td><?= $linha['cota_professor'] ?></td>
            				<td><?= $linha['minha_cota'] ?></td>
            				<td><?= $linha['valor_minha_cota'] ?></td>
            				<td><?= $linha['percentual_lucro'] ?></td>
            				<td class="font-weight-bold"><?= $linha['total_professor'] ?></td>
            				<td></td>
            				<td></td>
            				<td></td>
            			</tr>
        			<?php endforeach; ?>
        		<?php endif; ?>
			<?php endforeach; ?>
			
			<tr class="font-weight-bold table-warning linha-total"> 
				<td></td>
				<td>Total Geral</td>
				<td></td>
				<td><?= $somatorios['periodo'] ?></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td><?= moeda($somatorios['subtotal'], true, 4) ?></td>
				<td></td>
				<td><?= moeda($somatorios['total'], true, 4) ?></td>
				<td><?= moeda($somatorios['acumulado_nao_pago'], true, 4) ?></td>
				<td><?= moeda($somatorios['acumulado_proximo_mes'], true, 4) ?></td>
				<td><?= moeda($somatorios['valor_a_receber'], true, 4) ?></td>
			</tr>
			
		</table>
	<?php endif; // <- End if premiums ?>
</div>
