<form method="get" action="/relatorio-pagamento-professor-premium">	
    <div class="row mt-3">	
		<div class="col-md-6 col-sm-12 mb-2">
			<strong>Período:</strong>
			<?= UiSelectHelper::get_select_html("am", $anos_meses_combo, $_GET['am'] ?? null, false, ["class" => "filtro form-control", "style" => "width: 100%"]) ?>
		</div>
		
		<div class="col-md-6 col-sm-12 mb-2">
			<strong>Professores:</strong>
			<?= UiSelectHelper::get_select_html("prof", $professores_combo, $_GET['prof'] ?? null, false, ["class" => "filtro form-control", "style" => "width: 100%"]) ?>
		</div>
	</div>
	
	<div class="row mt-3">	
		<div class="col-xs-12 col-sm-12 mb-2">
			<input class="btn u-btn-blue" type="submit" name="filtrar" value="Filtrar">
			<input class="btn u-btn-blue" type="submit" name="export-excel" value="Exportar XLS">
		</div>
    </div>
</form>