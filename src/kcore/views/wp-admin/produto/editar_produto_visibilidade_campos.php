<?php if(tem_acesso([ADMINISTRADOR, COORDENADOR, REVISOR, ATENDENTE, APOIO])) : ?>
    <div id="visibilidade-campos-produto">
        <h3 style="margin: 10px" class="collapse-sessao in">Visibilidade de Campos (Página do Produto)<span class="expand">-</span></h3>
        <div class="options_group collapse-sessao-item">
        
            <?php
                KLoader::view("wp-admin/ui/checkbox", [
                    'post_id' => $post->ID,
                    'campo_id' => PRODUTO_OCULTAR_TIPO_PRODUTO,
                    'campo_label' => 'Ocultar Tipo do Produto',
                    'campo_default' => NO,
                ]);
                
                KLoader::view("wp-admin/ui/checkbox", [
                    'post_id' => $post->ID,
                    'campo_id' => PRODUTO_OCULTAR_PROFESSORES,
                    'campo_label' => 'Ocultar Professores',
                    'campo_default' => NO,
                ]);
                
                KLoader::view("wp-admin/ui/checkbox", [
                    'post_id' => $post->ID,
                    'campo_id' => PRODUTO_OCULTAR_MATERIAS,
                    'campo_label' => 'Ocultar Materias',
                    'campo_default' => NO,
                ]);
                
                KLoader::view("wp-admin/ui/checkbox", [
                    'post_id' => $post->ID,
                    'campo_id' => PRODUTO_OCULTAR_CONCURSOS,
                    'campo_label' => 'Ocultar Concursos',
                    'campo_default' => NO,
                ]);
                
                KLoader::view("wp-admin/ui/checkbox", [
                    'post_id' => $post->ID,
                    'campo_id' => PRODUTO_OCULTAR_QUANTIDADE,
                    'campo_label' => 'Ocultar Quantidade',
                    'campo_default' => NO,
                ]);
                
                KLoader::view("wp-admin/ui/checkbox", [
                    'post_id' => $post->ID,
                    'campo_id' => PRODUTO_OCULTAR_CARGA_HORARIA,
                    'campo_label' => 'Ocultar Carga Horária',
                    'campo_default' => NO,
                ]);

                KLoader::view("wp-admin/ui/checkbox", [
                    'post_id' => $post->ID,
                    'campo_id' => PRODUTO_OCULTAR_PARCELAMENTO,
                    'campo_label' => 'Ocultar Parcelamento de Preço',
                    'campo_default' => NO,
                ]);
            ?>

        </div>
    </div>
<?php endif ?>