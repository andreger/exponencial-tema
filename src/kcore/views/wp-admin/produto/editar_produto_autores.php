<?php
/**
 * @todo Remover $wpdb. Criar modelo KWP_Post para manipular objetos posts
 * @todo Remover dependencia de objeto $post e $produto
 */
global $wpdb;

if(tem_acesso([ADMINISTRADOR, COORDENADOR, REVISOR, ATENDENTE, APOIO])) {
    // Múltiplos Selects
//    $query = "SELECT ID,display_name,user_nicename FROM wp_users u LEFT JOIN wp_usermeta um ON um.user_id = u.ID " . "WHERE um.meta_key LIKE 'oneTarek_author_type' AND (um.meta_value = '1' OR um.meta_value = '3') ORDER BY display_name";
//    $result = $wpdb->get_results ( $query );

    $result = get_professores();
    
    $options = array ();
    $options [- 1] = 'Selecione um autor';
    foreach ( $result as $professor ) {
        $oculto = $professor->oculto ? " (oculto)" : "";
        $options [$professor->ID] = $professor->display_name . $oculto;
    }
    
    $authors = get_post_meta($post->ID, '_authors', true);
    $percentuais = get_post_meta($post->ID, '_percentuais', true);
    
    echo "<div id='autores-produto'>";
    echo '<h3 style="margin: 10px" class="collapse-sessao in">Autores<span class="expand">-</span></h3>';
    echo '<div class="options_group collapse-sessao-item">';
    
    for($i = 0; $i <= count ( $authors ); $i ++) {
        
        echo '<div class="autor-div"><div>';
        // Select
        echo "<div style='float:left'>";
        woocommerce_wp_select ( array (
            'id' => 'authors',
            'name' => 'authors[]',
            'class' => 'campo-autor',
            'label' => 'Autor',
            'value' => $authors [$i],
            'options' => $options
        ) );
        echo "</div>";
        
        echo "<div style='float:left'>";
        woocommerce_wp_text_input ( array (
            'id' => 'percentuais',
            'name' => 'percentuais[]',
            'class' => 'campo-percentual',
            'label' => '% (somente número)',
            'value' => $percentuais [$i]
        ) );
        echo '</div>';
        
        echo "<div style='float:left;position: relative;top: 13px;right: 100px;'>";
        echo "<a href='' class='button' onclick='jQuery(this).parent().parent().parent().remove(); return false;'>Remover</a>";
        echo "</div>";
        
        echo '</div>';
        echo "<div style='clear:both'></div></div>";
    }
    echo "<div style='margin:15px;'><a  class='button' href='' onclick='var appendDiv=jQuery(\".autor-div:last\").clone();
			appendDiv.find(\"input\").val(\"\");
			appendDiv.insertAfter(\".autor-div:last\"); return false;'>Adicionar Autor</a></div>";
    echo '</div>';
    echo '</div>';
}