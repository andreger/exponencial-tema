<?php if(tem_acesso([ADMINISTRADOR, COORDENADOR, ATENDENTE, APOIO])) : ?>
<div style="margin: 0 0 20px 5px">
	<a id="importar-cronograma-open-btn" class='button' href='#'>Importar Cronograma</a>
	
	<div id="importar-cronograma-div" style="border: 1px solid #eee;padding: 20px;margin-top: 5px; display:none">
		<?php 
		  $url = get_edit_post_link($post->ID);
		  woocommerce_wp_text_input ([
            'id' => 'importar-cronograma-id',
            'label' => 'Id do Produto',
            'placeholder' => '(Digite o ID do produto de origem)',
            'custom_attributes' => [
                'maxlength' => 8,
            ]
        ]);
		  ?>
	    <div>
	  		<a id="importar-cronograma-btn" class='button' href='#' data-url="<?= $url ?>">Processar Importação</a>
	  		<a id="importar-cronograma-cancel-btn" class='button' href='#'>Cancelar</a>
	    </div>
	</div>
</div>
<?php endif ?>