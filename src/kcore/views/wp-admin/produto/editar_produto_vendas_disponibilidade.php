<?php if(tem_acesso([ADMINISTRADOR, COORDENADOR, REVISOR, ATENDENTE, APOIO])) : ?>

<div id="expiracao-curso">
	<h3 style="margin: 10px;" class="collapse-sessao in">Vendas e Disponibilidade de Produto<span class="expand">-</span></h3>
	
    <div class="options_group collapse-sessao-item">
    	<?php
            woocommerce_wp_text_input ( [
                'id' => PRODUTO_VENDAS_ATE_POST_META,
                'label' => 'Vendas Até',
                'class' => 'campo_data'
            ] );
            
            woocommerce_wp_text_input ( [
                'id' => PRODUTO_DISPONIVEL_ATE_POST_META,
                'label' => 'Disponível Até',
                'class' => 'campo_data'
            ] );
            
            woocommerce_wp_text_input ( [
                'id' => PRODUTO_TEMPO_DE_ACESSO,
                'label' => 'Tempo de Acesso',
                'class' => ''
            ] );
        ?>
	</div>
</div>

<?php endif ?>