<?php if(tem_acesso([ADMINISTRADOR, COORDENADOR, REVISOR, ATENDENTE, APOIO])) : ?>
    <div id="gestao-conteudo-produto">
        <h3 style="margin: 10px" class="collapse-sessao in">Gestão do Conteúdo<span class="expand">-</span></h3>
        <div class="options_group collapse-sessao-item">
        
            <?php
                KLoader::view("wp-admin/ui/checkbox", [
                    'post_id' => $post->ID,
                    'campo_id' => PRODUTO_FUNDIR_ARQUIVOS,
                    'campo_label' => 'Fundir PDFs da aula',
                    'campo_default' => NO,
                ]);
                
                KLoader::view("wp-admin/ui/checkbox", [
                    'post_id' => $post->ID,
                    'campo_id' => PRODUTO_INCLUIR_CAPA,
                    'campo_label' => 'Incluir Capa',
                    'campo_default' => NO,
                ]);
                
                woocommerce_wp_text_input ([
                    'id' => PRODUTO_CAPA_TEXTO_CONCURSO,
                    'label' => 'Capa Concurso',
                    'placeholder' => '(Máx: 20 caracteres)',
                    'custom_attributes' => [
                        'maxlength' => 20
                    ]
                ]);
                
                woocommerce_wp_text_input ([
                    'id' => PRODUTO_CAPA_TEXTO_DISCIPLINA,
                    'label' => 'Capa Disciplina',
                    'placeholder' => '(Máx: 20 caracteres)',
                    'custom_attributes' => [
                        'maxlength' => 20
                    ]
                ]);
          
            ?>

        </div>
    </div>
<?php endif ?>