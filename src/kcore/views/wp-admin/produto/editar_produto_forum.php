<?php
// Criação do fórum ocorre somente no momento da criação do produto
if($post->filter == 'raw') {
    echo '<div id="forum"><h3>Fórum</h3>';
    echo '<div class="options_group">';
    
    $check_options = array (
        'id' => 'forum',
        'label' => 'Criar Fórum',
        'value' => 'yes'
    );
    
    woocommerce_wp_checkbox($check_options);
    echo '</div></div>';
}