<?php 
$tipos = [
    1 => "Sempre",
    2 => "Produto",
    3 => "Categoria"  
];

$operadores = [ 
    1 => "É",
    2 => "Não É"
];


?>
<tr class="tr-and">
	<td class="" >
		<input type="hidden" class="premium-grupo" value="<?= $num_grupo ?>">
		<select class="premium-tipo">
			<?php foreach ($tipos as $key => $value) : ?>
			<option value="<?= $key ?>" <?= isset($tipo_selecionado) && $tipo_selecionado == $key ? "selected=selected" : "" ?>><?= $value ?>
			<?php endforeach; ?>
		</select> 
	</td>
	<td class="">
		<select class="premium-operador">
			<?php foreach ($operadores as $key => $value) : ?>
			<option value="<?= $key ?>" <?= isset($operador_selecionado) && $operador_selecionado == $key ? "selected=selected" : "" ?>><?= $value ?>
			<?php endforeach; ?>
		</select>
	</td>
	<td class="" style="width: 100%">
		<div class="premium-valores-produtos">
			<select class="wc-enhanced-select" style="width: 100%" multiple>
			<?php foreach($produtos as $item) : ?>
				<option value="<?= $item->ID ?>" <?= isset($valores_produtos_selecionados) && in_array($item->ID, $valores_produtos_selecionados) ? "selected=selected" : "" ?>><?= $item->post_title ?>
			<?php endforeach; ?>
			</select>
		</div>
		<div class="premium-valores-categorias">
			<select class="wc-enhanced-select" style="width: 100%" multiple>
			<?php foreach($categorias as $item) : ?>
				<option value="<?= $item->term_id ?>" <?= isset($valores_categorias_selecionadas) && in_array($item->term_id, $valores_categorias_selecionadas) ? "selected=selected" : "" ?>><?= $item->name ?> (<?= $item->slug ?>)
			<?php endforeach; ?>
			</select>
		</div>
	</td>
	<td class="">
		<button class="button premium-and" type="button">AND</button> 
		<button class="button premium-delete" type="button">Excluir</button>
	</td>
</tr>