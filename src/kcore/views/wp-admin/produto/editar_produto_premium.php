<?php if(tem_acesso([ADMINISTRADOR, COORDENADOR, REVISOR, ATENDENTE, APOIO])) : ?>


<?php
    KLoader::model("PostModel");
    KLoader::model("ProdutoModel");
    KLoader::model("CategoriaModel");
    
    $produtos = PostModel::listar(['tipo' => 'post', 'select' => 'ID, post_title', 'tipo' => 'product']);
    $categorias = CategoriaModel::listar(['select' => 't.term_id, t.name, t.slug']);
    $grupos_regras = ProdutoModel::get_premium_grupos_regras($post->ID);
?>

<div id="configuracoes-premium" style="border-bottom: 1px solid #eee; padding: 10px 0">
	<h3 style="margin: 10px">Configurações de Produto Premium</h3>
	
    <div class="options_group">
    	<?php
        	$check_options = array (
        	    'id' => PRODUTO_PREMIUM_EXIBIR_PAGINA_CONCURSO,
        	    'label' => 'Exibir nas páginas de concurso'
        	);
        	
        	// Definição do valor default
        	if(! metadata_exists('post', $post->ID, PRODUTO_PREMIUM_EXIBIR_PAGINA_CONCURSO)) {
        	    $check_options['value'] = 'no';
        	}
        	
        	woocommerce_wp_checkbox($check_options);
            
        	$check_options = array (
        	    'id' => PRODUTO_PREMIUM_EXIBIR_PAGINA_CURSO,
        	    'label' => 'Exibir nas páginas de curso'
        	);
        	
        	// Definição do valor default
        	if(! metadata_exists('post', $post->ID, PRODUTO_PREMIUM_EXIBIR_PAGINA_CURSO)) {
        	    $check_options['value'] = 'no';
        	}
        	
        	woocommerce_wp_checkbox($check_options);
        	
        	$check_options = array (
        	    'id' => PRODUTO_PREMIUM_CONCEDER_ACESSO_SQ,
        	    'label' => 'Conceder acesso ao SQ'
        	);
        	
        	// Definição do valor default
        	if(! metadata_exists('post', $post->ID, PRODUTO_PREMIUM_CONCEDER_ACESSO_SQ)) {
        	    $check_options['value'] = 'no';
        	}
        	
        	woocommerce_wp_checkbox($check_options);
        ?>
        
        <div class="">
    		<h4 style="margin-left: 11px">Selecionar produtos quando essas condições forem verdadeiras:</h4>
        </div>
        
        <table class="widefat premium-table" style="border: 0">
        	<tbody>
        		<?php
        		if($grupos_regras) {
        		    $num_grupo = 0;
        		    
        		    foreach ($grupos_regras as $grupo_regra) {
        		        if($num_grupo) {
        		            KLoader::view("wp-admin/produto/editar_produto_premium_regras_or",  ["num_grupo" => $num_grupo]);
        		        }
        		        
        		        foreach ($grupo_regra as $regra) {
        		            KLoader::view("wp-admin/produto/editar_produto_premium_regras", 
        		                ["num_grupo" => $num_grupo, "produtos" => $produtos, "categorias" => $categorias, "tipo_selecionado" => $regra["tipo"],
        		                    "operador_selecionado" => $regra["operador"], "valores_produtos_selecionados" => $regra["valores_produtos"],
        		                    "valores_categorias_selecionadas" => $regra["valores_categorias"]
        		                ]);
        		        }
        		        
        		        $num_grupo++;
        		    }
        		}
        		else {
        		  KLoader::view("wp-admin/produto/editar_produto_premium_regras", ["produtos" => $produtos, "categorias" => $categorias]);
        		}
        		?>
        	</tbody>
        	
        	<tfoot>
        		<tr>
        			<td colspan="4" style="border-top: 0;">
        				<div style="color: #555; font-size: 13px">
        					<strong>ou quando essas condições forem verdadeiras</strong>
        				</div>	
        				<div>
        					<button class="button premium-or" type="button">OR</button>
        				</div>	
        			</td>
        		</tr>
        	</tfoot>
        </table>
	
	</div>

</div>

<?php endif ?>