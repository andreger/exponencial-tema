<?php
/**
 * @todo Remover $wpdb. Criar modelo KWP_Post para manipular objetos posts
 * @todo Remover dependencia de objeto $post e $produto 
 */

global $wpdb;
echo "<div id='produto-informacoes'>";
echo '<h3 style="margin: 10px" class="collapse-sessao in">Informações do Curso<span class="expand">-</span></h3>';
echo '<div class="options_group collapse-sessao-item">';
woocommerce_wp_text_input ( array (
		'id' => 'qtde_aulas',
		'label' => 'Qtde de aulas' 
) );
woocommerce_wp_text_input ( array (
		'id' => 'qtde_esquemas',
		'label' => 'Qtde de esquemas' 
) );
woocommerce_wp_text_input ( array (
		'id' => 'qtde_questoes',
		'label' => 'Qtde de questões' 
) );
woocommerce_wp_text_input ( array (
		'id' => 'carga_horaria',
		'label' => 'Carga Horária' 
) );

//Professor não pode efetuar essas alterações: B2745
if(tem_acesso([ADMINISTRADOR, COORDENADOR, REVISOR, ATENDENTE, APOIO])) {
	woocommerce_wp_text_input ( array (
			'id' => 'botao_comprar',
			'label' => 'Botão Comprar',
			'placeholder' => '(Máx: 15 caracteres)',
			'custom_attributes' => [
				'maxlength' => 15
			]
	) );
}

//Professor não pode efetuar essas alterações: B2745
/**
 * Aula Demo / Instruções de Acesso
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2318
 * 
 */
if(tem_acesso([ADMINISTRADOR, COORDENADOR, REVISOR, ATENDENTE, APOIO])) {
	woocommerce_wp_select ( array (
			'id' => 'chamada',
			'name' => 'chamada',
			'label' => 'Chamada para Ação',
			'options' => [
				0 => 'Cursos',
				1 => 'Cadernos e Simulados' 
			] 
	) );
}

if(tem_acesso([ADMINISTRADOR, COORDENADOR, REVISOR, ATENDENTE, APOIO])) {

	$exibir_checkbox_notificar_vendas = TRUE;

	/**
	 * Coordenadores coaching só podem editar seus próprios produtos
	 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2420
	 */ 

	if(is_coordenador_coaching() && !is_coordenador_sq_ativo() && !is_coordenador_area_ativo() && !is_autor($post->ID, get_current_user_id())) {
		$exibir_checkbox_notificar_vendas = FALSE;
	}

	if($exibir_checkbox_notificar_vendas) {

	    $check_options = array (
	    		'id' => 'notificar_vendas',
	    		'label' => 'Notificar Vendas'
	    );
	    
	    $sql = "select * from wp_postmeta where post_id = {$post->ID} and meta_key = 'notificar_vendas'";

	    $result = $wpdb->get_results($sql);

	    if(count($result) == 0) {
	    	$check_options['value'] = 'yes';
	    }
	    
		woocommerce_wp_checkbox($check_options);
	}

}

if(tem_acesso([ADMINISTRADOR, COORDENADOR, REVISOR, ATENDENTE, APOIO])) {

	$exibir_checkbox_mostrar_garantia = TRUE;

	/**
	 * Coordenadores coaching só podem editar seus próprios produtos
	 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2420
	 */ 

	if(is_coordenador_coaching() && !is_coordenador_sq_ativo() && !is_coordenador_area_ativo() && !is_autor($post->ID, get_current_user_id())) {
		$exibir_checkbox_mostrar_garantia = FALSE;
	}

	if($exibir_checkbox_mostrar_garantia) {

	    $check_options = array (
	        'id' => PRODUTO_MOSTRAR_GARANTIA,
	    	'label' => 'Mostrar garantia'		    		
	    );
	    
		// Definição do valor default
	    if(! metadata_exists('post', $post->ID, PRODUTO_MOSTRAR_GARANTIA)) {
			$check_options['value'] = 'yes';
		}

		woocommerce_wp_checkbox($check_options);
	}
}

//Professor não pode efetuar essas alterações: B2745
/**
 * Exibe um checkbox para liberar as aulas gradualmente aṕos a data de compra
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/upgrades/items/364
 */
if(tem_acesso([ADMINISTRADOR, COORDENADOR, REVISOR, ATENDENTE, APOIO])) {
	$check_options = array (
			'id' => 'liberar_gradualmente',
			'label' => 'Liberar Aulas Gradualmente'
	);


	$sql = "select * from wp_postmeta where post_id = {$post->ID} and meta_key = 'liberar_gradualmente'";

	$result = $wpdb->get_results($sql);

	if(count($result) == 0) {
		$check_options['value'] = 'no';
	}


	woocommerce_wp_checkbox($check_options);
}

/**
 * Exibe um checkbox para marcação de edição livre de categorias
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2404
 */ 

if(is_pacote($produto)) {
	if(tem_acesso([ADMINISTRADOR, COORDENADOR, REVISOR, ATENDENTE, APOIO])) {
	    $check_options = array (
	    		'id' => CHECKBOX_EDICAO_MANUAL_CATEGORIAS,
	    		'label' => 'Edição manual de categorias'
	    );
	    
	    $sql = "select * from wp_postmeta where post_id = {$post->ID} and meta_key = '" . CHECKBOX_EDICAO_MANUAL_CATEGORIAS . "' and meta_value = 'yes' ";

	    $result = $wpdb->get_results($sql);

	    if(count($result) == 0) {
	    	$check_options['value'] = 'no';
	    }

		woocommerce_wp_checkbox($check_options);
	}
}

if(tem_acesso([ADMINISTRADOR, COORDENADOR, REVISOR, ATENDENTE, APOIO])) {
    
    $check_options = array (
        'id' => PRODUTO_PREMIUM,
        'label' => 'Produto Premium'
    );
    
    // Definição do valor default
    if(! metadata_exists('post', $post->ID, PRODUTO_PREMIUM)) {
        $check_options['value'] = 'no';
    }
    
    woocommerce_wp_checkbox($check_options);
}

echo '</div></div>';