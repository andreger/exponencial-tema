<?php
$check_options = array (
    'id' => $campo_id,
    'label' => $campo_label
);

// Definição do valor default
if(! metadata_exists('post', $post_id, $campo_id)) {
    $check_options['value'] = $campo_default;
}

woocommerce_wp_checkbox($check_options);