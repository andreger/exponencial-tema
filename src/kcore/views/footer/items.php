<?php KLoader::model('RodapeItemModel'); ?>

<footer class="container-fluid">
    <div class="row footer-fundo">

        <div class="col-12 mt-0 mb-2 mt-lg-2 d-lg-none">
            <?php KLoader::view("header/barra_principal/redes_sociais"); ?>
        </div>

        <div class="col-12 mt-0 mt-lg-2 d-lg-none">
            <div class="mt-0 mt-lg-4 pt-1 col-12 col-md-8 col-lg-9 col-xl-7 text-center text-md-right pl-3 pl-md-0">
                <div class="text-uppercase text-amarelo-2 font-15 font-weight-bold line-height-1">
                    Satisfação garantida<br>ou seu dinheiro de volta!
                </div>
                <div class="text-white">
                    Solicite <a href="/fale-conosco" class="text-white">AQUI</a> sem burocracia
                </div>
                <div class="text-white">
                    Confira <a href="/termos-de-uso" class="text-white">AQUI</a>  as regras e condições.
                </div>
            </div>
            <div class="pl-3 pl-md-0 col-12 col-md-4 col-lg-3 col-xl-5 text-center text-md-left">
                <div class="pl-3 mt-3 pl-md-0 img-rdp">
                    <span class="quatorze-dias d-inline-block align-bottom"></span>
                </div>
            </div>
        </div>

        <div class="text-center d-none d-lg-block col-3">
            <div class="pt-3 ml-auto pr-0 col-12">
                <img src="/core/resources/img/layout/logo-footer.png" class="img-fluid expo-img-footer" alt="Responsive image">
            </div>
            <?php KLoader::view("header/barra_principal/redes_sociais") ?>
        </div>
        <?php $rodape_itens = array(); ?>
        <?php $rodape_itens[] = RodapeItemModel::list(1); ?>
        <?php $rodape_itens[] = RodapeItemModel::list(2); ?>
        <?php $rodape_itens[] = RodapeItemModel::list(3); ?>
        <?php foreach ($rodape_itens as $itens): ?>
            <div class="pt-1 address font-13 text-center text-lg-left mb-3 mb-lg-4 mt-4 mt-lg-4 col-12 col-lg-2">
                <?php foreach ($itens as $i): ?>
                    <div><a href="<?= $i->roi_url; ?>"><?= $i->roi_titulo; ?></a></div>
                <?php endforeach; ?>
            </div>
        <?php endforeach; ?>
        <div class="col-12 col-lg-3">

            <div class="row mt-0 mt-lg-2 d-none d-lg-flex">
                <div class="mt-0 mt-lg-4 pt-1 col-12 col-md-8 col-lg-9 col-xl-7 text-center text-md-right pl-3 pl-md-0">
                    <div class="text-uppercase text-amarelo-2 font-15 font-weight-bold line-height-1">
                        Satisfação garantida<br>ou seu dinheiro de volta!
                    </div>
                    <div class="text-white">
                        Solicite <a href="/fale-conosco" class="text-white">AQUI</a> sem burocracia
                    </div>
                    <div class="text-white">
                        Confira <a href="/termos-de-uso" class="text-white">AQUI</a>  as regras e condições.
                    </div>

                </div>
                <div class="pl-3 pl-md-0 col-12 col-md-4 col-lg-3 col-xl-5 text-center text-md-left">
                    <div class="pl-3 mt-3 pl-md-0 img-rdp">
                        <span class="quatorze-dias d-inline-block align-bottom"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="mt-4 pt-1 col-12 col-md-8 col-lg-9 col-xl-7 text-center text-md-right">
                    <div class="text-uppercase text-white font-12 font-weight-bold line-height-1">
                        Tudo* em até 12x sem juros
                    </div>
                    <div class="text-uppercase text-white">
                        No cartão de crédito
                    </div>
                </div>
                <div class="pl-3 pl-md-0 col-12 col-md-4 col-lg-3 col-xl-5 text-center text-md-left">
                    <div class="pl-3 pl-md-0 mt-2 mt-md-4 img-rdp mb-3">
                        <span class="comum-cartao d-inline-block align-bottom"></span>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <!-- /navigation -->



    <div class="text-center p-3 text-info font-weight-bold">
        <?= date('Y') ?> - Exponencial Concursos. Todos os direitos reservados - <span class="text-nowrap">CNPJ: 20.094.693/0001-36</span>
    </div>
</footer>
<!-- /footer -->