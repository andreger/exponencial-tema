<?php
$post_id = $_GET['post_id'];
$post = get_post($post_id);

switch ($post->post_type) {
    
    case "post": {
        $exibir = true; break;
    }
    
    case "page": {
        $inclusoes = [
            'home',
            'sistema-de-questoes',
            'simulados',
            'professores',
            'blog-noticias',
            'blog-posts-noticias',
            'blog-artigos',
            'blog-posts-artigos',
            'blog-videos',
            'blog-posts-videos',
            'descontos-promocoes',
            'metodologia-exponencial',
            'quem-somos',
            'perguntas-frequentes-portal',
            'como-funciona',
            'parceiros',
        ];

        if(in_array($post->post_name, $inclusoes)) {
            $exibir = true;
        }

        break;
    }
}

if($exibir) :

    carregar_remodal();
    
    KLoader::model("MailchimpModel");
    
    $areas_options = MailChimpModel::listar_interesses(MC_LISTA_EXPONENCIAL_CONCURSOS, MC_INTERESSE_AREA);
?>

<div style="border-top: 1px solid #eee; padding: 30px 0">
    <div class="container">
    	<form method="post" action="/" id="frm-mc-footer">
            <div class="row">
            	<div class="col-md-4">
            		<div class="font-weight-bold text-blue pt-md-1 text-center font-30">Cadastro GRÁTIS</div>
            		<div class="font-weight-bold text-blue pt-md-1 font-20 text-center" style="line-height: 30px">Acesso exclusivo a notícias atualizadas e promoções.</div>
            	</div>
            	
                <div class="col-md-4">
                	
                	<input type="text" class="form-control" placeholder="Nome" name="nome">
                	<input type="text" class="form-control mt-3" placeholder="Telefone" name="telefone" id="telefone">
                	<input type="text" class="form-control mt-3" placeholder="E-mail" name="email">
                	
                	<select class="form-control mt-3" name="area">
                		<option value="">Sua área de interesse
                		<?php foreach ($areas_options as $item) : ?>
                		<option value="<?= $item->mca_id ?>" ><?= $item->mca_nome ?>
                		<?php endforeach; ?>
                	</select>
        
                </div>
                <div class="col-md-4">
                	<div style="width: 304px">
                    	<div>
                    		<?= carregar_recaptcha() ?>
                    	</div>
                    	<div style="font-size: 12px" class="text-justify">
                    		<input type="checkbox" name="concordo" id="concordo" checked="checked"> 
                    		Você concorda com a nossa <a href="/politica-de-privacidade">Política de Privacidade</a> e aceita receber informações adicionais do Exponencial Concursos.
                    	</div>
            			<button id="mc-cadastrar" type="submit" value="1" name="mc-submit" class="btn u-btn-darkblue font-14 font-arial font-weight-bold col-12">Cadastrar</button>
        			</div>
        		</div>
            </div>
        </form>
    </div>
</div>

<div class="remodal" data-remodal-id="mc-modal">
    <button data-remodal-action="close" class="remodal-close"></button>    
    
    <div class="mc-carregando contato form_outter" style="display: none">
        <div class="mensagem-panel">
    	   <div style="padding: 10px"><img alt="Carregando" width="80" src='/wp-content/themes/academy/images/carregando.gif'></div>
    	   <div style="padding: 10px">Aguarde só mais um momento...</div>
    	   <div style="padding: 10px">Estamos realizando seu cadastro!</div>
        </div>
    </div>

    <div class="mc-sucesso mc-response contato form_outter" style="display: none">
        <div class="mensagem-panel">
            <img alt="Confirmar" src="<?= get_tema_image_url("confirm-grande.png") ?>" width="150">
            <div class="p-5 font-20">
            	Seu cadastro foi realizado com sucesso.
            </div>
            <button data-remodal-action="cancel" class="btn btn-primary">Fechar</button>
        </div>
    </div>

    <div class="mc-erro mc-response contato form_outter" style="display: none">
        <div class="mensagem-panel">
            <img alt="Erro" src="<?= get_tema_image_url("erro-grande.png") ?>" width="150">
            <div class="p-5 font-20">
            	Você precisa selecionar o campo<br>"Não sou um robô".
            </div>
            <button data-remodal-action="cancel" class="btn btn-danger">Fechar</button>
        </div>
    </div>
</div>

<script src="/wp-content/themes/academy/js/jquery.validate.min.js"></script>
<script src="/wp-content/themes/academy/js/jquery.mask.js"></script>
<script>
jQuery.validator.addMethod("onlychar", function(value, element) {
        value = value.trim();
        return this.optional(element) || /^([a-zA-ZãõáéíóúâêôçÃÕÁÉÍÓÚÂÊÔÇ]+\s)*[a-zA-ZãõáéíóúâêôçÃÕÁÉÍÓÚÂÊÔÇ]+$/.test(value);
    }, "Nome inválido");
    
jQuery.validator.addMethod("brphone", function(value, element) {
        value = value.replace("_","");
        return this.optional(element) || /^([\(]{1}[1-9]{2}[\)]{1}[ ]{1}[0-9]{4,5}[\-]{1}[0-9]{4})$/.test(value);
    }, "Telefone inválido");
   

 jQuery(function() {

     jQuery("#frm-mc-footer").submit( function(e) {
			e.preventDefault();
         }).validate({
         rules: {
             email: {
                 required: true,
                 email: true,
             },
             nome: {
                 required: true,
                 onlychar: true
             },
             telefone: {
                 required: true,
                 brphone: true
             },
             area: {
				 required: true,
             },
             concordo: {
				 required: true,
             }
         },
         messages: {
             email: {
                 required: "E-mail é obrigatório",
                 email: "Formato do e-mail inválido",
             },
             nome: {
                 required: "Nome é obrigatório",
                 onlychar: "O nome deve conter apenas letras e espaços"
             },
             telefone: {
                 required: "Telefone é obrigatório",
                 brphone: "Formato do telefone inválido"
             },
             area: {
				 required: "Área de interesse é obrigatória",
             },
             concordo: {
				 required: "Você precisa concordar com a política de privacidade",
             }
         },
         errorPlacement: function(error, element) {
            error.insertAfter(element);
         },
         submitHandler: function( form ){
        	 	jQuery(".mc-response").hide();
        	 	jQuery(".mc-carregando").show();
             
        	 	var inst = jQuery('[data-remodal-id=mc-modal]').remodal();
        	 	inst.open();
        	 	
				var dados = jQuery( form ).serialize();

				jQuery.ajax({
					type: "POST",
					url: "/wp-content/themes/academy/ajax/enviar_mc_footer.php",
					data: dados,
					success: function( data )
					{
						jQuery(".mc-carregando").hide();
						
						if(data == "1") {
							jQuery(".mc-sucesso").show();
						}
						else {
							jQuery(".mc-erro").show();
						}

						grecaptcha.reset();
					}
				});

				return false;
			}
     });
 
     var SPMaskBehavior = function (val) {
     	return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
     },
     spOptions = {
        onKeyPress: function(val, e, field, options) {
            field.mask(SPMaskBehavior.apply({}, arguments), options);
          }
     };
 
     jQuery('#telefone').mask(SPMaskBehavior, spOptions);

     
     
});
</script>

<?php endif ?>