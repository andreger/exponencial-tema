<?php if(!UrlHelper::is_url_sistema_questoes()): ?>
	<script src="/wp-content/themes/academy/js/jquery-ui.min.js"></script>
	<link rel="stylesheet" href="/wp-content/themes/academy/css/jquery-ui.min.css" />
	<script src="/wp-content/themes/academy/js/jquery.validate.min.js"></script>
	<script src="/wp-content/themes/academy/js/jquery.maskedinput.min.js"></script>
	<script src="/wp-content/themes/academy/js/datepicker-pt-BR.js"></script>
<?php else: ?>
	<script>
		
		$.fn.datepicker.dates['pt'] = {
			format: 'dd/mm/yyyy',
			days: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
			daysMin: ['D','S','T','Q','Q','S','S','D'],
			daysShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
			months: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
			monthsShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
			today: 'Hoje',
			weekStart: 0
		};

	</script>
<?php endif; ?>
<script>
	
jQuery.validator.addMethod("birthdate", function(value, element) {

	
  return this.optional(element) || /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/.test(value);
}, "Data de nascimento inválida");

jQuery.validator.addMethod("onlychar", function(value, element) {
	value = value.trim();
	return this.optional(element) || /^([a-zA-ZãõáéíóúâêôçÃÕÁÉÍÓÚÂÊÔÇ]+\s)*[a-zA-ZãõáéíóúâêôçÃÕÁÉÍÓÚÂÊÔÇ]+$/.test(value);
}, "Nome inválido");

jQuery().ready(function() {
	jQuery('#confirm_email').bind("cut copy paste",function(e) {
        e.preventDefault();
    });

	jQuery('#confirm_senha').bind("cut copy paste",function(e) {
        e.preventDefault();
    });

    jQuery("#data_nascimento").mask("99/99/9999",{placeholder:" "});
	
	jQuery("#data_nascimento").datepicker({
		changeMonth: true,
      	changeYear: true,
		yearRange: "-100:+0",
		language: 'pt'
	});

	jQuery('#data_nascimento').change(function() {	
		jQuery('#register-form').validate().element('#data_nascimento')
	});

	jQuery("#register-form").validate({
		rules: {
			nome: {
				required: true,
				onlychar: true
			},
			sobrenome: {
				required: true,
				onlychar: true
			},
			data_nascimento: {
				required: true,
				birthdate: true
			},
			email: {
				required: true,
				email: true,
				remote: {
			        url: "/wp-content/themes/academy/check-email.php",
			        type: "post",
			        data: {
			          email: function() {
			            return jQuery("#email").val();
			          }
			        }
			    }
			},
			confirm_email: {
				required: true,
				email: true,
				equalTo: "#email"
			},
			senha: {
				required: true,
				minlength: 5
			},
			confirm_senha: {
				required: true,
				minlength: 5,
				equalTo: "#senha"
			},
			terms: "required",
			<?php if(!is_selenium()) : ?>
			captcha: {
				required: true,
				remote: {
					url: "/wp-content/captcha/process.php",
					type: "post",
					data: {
						r: function() {
							 return jQuery("#captcha_r").val();
						},
						k: function() {
							 return jQuery("#captcha").val();
						}
					}
				}
			}
			<?php endif; ?>
		},
		messages: {
			nome: {
				required: "Nome é obrigatório",
				onlychar: "O nome deve conter apenas letras e espaços"
			},
			sobrenome: {
				required: "Sobrenome é obrigatório",
				onlychar: "O sobrenome deve conter apenas letras e espaços"
			},
			data_nascimento: {
				required: "Data de nascimento é obrigatória",
				birthdate: "Data de nascimento inválida"
			},
			email: {
				required: "E-mail é obrigatório",
				email: "E-mail inválido",
				remote: "E-mail já cadastrado"
			},
			confirm_email: {
				required: "Confirmação de e-mail é obrigatória",
				email: "E-mail inválido",
				equalTo: "Confirmação de e-mail não está igual ao e-mail"
			},
			senha: {
				required: "Senha é obrigatória",
				minlength: "Senha deve ter, no mínimo, 5 caracteres"
			},
			confirm_senha: {
				required: "Confirmação de senha é obrigatória",
				minlength: "Confirmação de senha deve ter, no mínimo, 5 caracteres",
				equalTo: "Confirmação de senha não está igual à senha"
			},
			terms: "&nbsp;&nbsp;É necessário aceitar os termos do serviço",
			captcha: {
				required: "Código de verificação inválido",
				remote: "Código de verificação inválido"
			}
		},
	  	errorPlacement: function(error, element) {
		    if (element.attr("name") == "terms") {
		      error.insertAfter("#terms");
		    } else {
		      error.insertAfter(element);
		    }
		},
		submitHandler: function(form) {
			<?php if($is_ajax): ?>
				ajax_register_submit(jQuery(form));
			<?php else: ?>
		    	form.submit();
			<?php endif; ?>
		}
	});

	jQuery("#cadastro-login-form").validate({
		rules: {
			log: {
				required: true,
			},
			pwd: {
				required: true,
			}
		},
		messages: {
			log: {
				required: "E-mail é obrigatório",
			},
			pwd: {
				required: "Senha é obrigatória",
			}
		},
		errorPlacement: function(error, element) {
			if (element.attr("name") == "terms") {
				error.insertAfter("#terms");
			} else {
				error.insertAfter(element);
			}
		},
		submitHandler: function(form) {
			<?php if($is_ajax): ?>
				ajax_login_submit(jQuery(form));
			<?php else: ?>
				form.submit();
			<?php endif; ?>
		}
	});

	jQuery("#refreshimg").click(function(){
		jQuery.post('/wp-content/captcha/newsession.php',function(data) {
			jQuery("#captcha_r").val(data);
			jQuery("#refreshimg").load('/wp-content/captcha/image_req.php?k='+data);
		});
		return false;
	});

	jQuery('#submit-register').click(function(e){
		if(jQuery('#register-form').valid()){
			<?php if($is_ajax): ?>
				ajax_register_submit(jQuery('#register-form'));
			<?php else: ?>
				jQuery('#register-form').submit();
			<?php endif; ?>
		}
	});

	jQuery("#login-submit").click(function(e){
		if(jQuery('#cadastro-login-form').valid()){
			<?php if($is_ajax): ?>
				ajax_login_submit(jQuery('#cadastro-login-form'));
			<?php else: ?>
				jQuery('#cadastro-login-form').submit();
			<?php endif; ?>
		}
	});

});

<?php if($is_ajax): ?>
		
	function ajax_register_submit(form){
		var url = form.attr('action');
		jQuery.ajax({
			type: "POST",
			url: url,
			data: form.serialize(),
			dataType: 'json',
			success: function(data){
				if(data.erro){
					jQuery('#cadastro-error-msg p').html(data.erro);
					jQuery('#cadastro-error-msg').show();
					form_submit(form, false, false);
				}else if(data.redirect){
					form_submit(form, false, true);
					window.location.href = data.redirect;
				}			
			}
		});
		form_submit(form, true, false);
	}

	function ajax_login_submit(form){
		var url = "<?= get_ajax_url('login-ajax.php') ?>";
		jQuery.ajax({
			type: 'POST',
			url: url,
			data: form.serialize(),
			dataType: 'json',
			success: function(data){
				if (data.loggedin == true){
					form_submit(form, false, true);
					if(form.data('redirect')){
						window.location.href = form.data('redirect');
					}else{
						window.location.reload(true);
					}
				}else{
					form_submit(form, false, false);
				}			
			}
		});
		form_submit(form, true, false);
	}

	function form_submit(form, is_enviando, is_sucesso){
		if(is_enviando){
			jQuery('#'+form.attr('id') + ' .login-error-msg').hide();
			jQuery('#'+form.attr('id') + ' .login-loading-msg').show();
			jQuery('#login-modal :input').prop("disabled", true);
		}else if(is_sucesso){
			jQuery('#'+form.attr('id') + ' .login-loading-msg label').html('Redirecionando...');
		}else{
			jQuery('#'+form.attr('id') + ' .login-loading-msg').hide();
			jQuery('#'+form.attr('id') + ' .login-error-msg').show();
			jQuery('#login-modal :input').prop("disabled", false);
		}
	}

<?php endif; ?>

jQuery('#cadastro-login-form').keydown(function(e) {
	var key = e.which;
	if (key == 13) {
		// As ASCII code for ENTER key is "13"
		jQuery('#login-submit').click(); // Click form button
	}
});

jQuery('#register-form').keydown(function(e) {
	var key = e.which;
	if (key == 13) {
		// As ASCII code for ENTER key is "13"
		jQuery('#submit-register').click(); // Click form button
	}
});

jQuery('#frm-login-modal').keydown(function(e) {
	var key = e.which;
	if (key == 13) {
		// As ASCII code for ENTER key is "13"
		jQuery('#login-modal-entrar').click(); // Click form button
	}
});

</script>
<!-- validation -->
<script>
function valid_email(){
	var Email=document.getElementById('getemail').value;
	if(Email.length==0) {
      	document.getElementById('err').innerHTML='Introduza o e-mail';
    	return false; 
	}
	var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/; 
	if(!Email.match(mailformat)) {  
        document.getElementById('err').innerHTML='E-mail inv&aacute;lido';
    	return false;  
	}
	var n = Email.indexOf("@");
	n++;
	var email_domain = Email.slice(n);
	email_domain = email_domain.trim();
	email_domain = email_domain.toLowerCase();

	// ------ ARRAY COM DOMÍNIOS DE E-MAILS PROIBIDOS --------
	var forbiden_domains = ['trbvm.com', 'drdrb.net', 'mailinator.com'];
	// ------ ARRAY COM DOMÍNIOS DE E-MAILS PROIBIDOS --------
	// Teste com o ARRAY
	var forbiden_result = false;
	for (index = 0; index < forbiden_domains.length; ++index) {
	    if(email_domain == forbiden_domains[index]) {
	    	forbiden_result = true;
	    	document.getElementById('err').innerHTML='&Eacute; necess&aacute;rio um e-mail v&aacute;lido';
	    	return false;
	    }
	}
	if (forbiden_result) {
		document.getElementById('err').innerHTML='Aconteceu algo inesperado';
	    return false;
	}else{
 		jQuery("#cadastro").slideUp("slow");
		jQuery("#cadastro_form_area").show();
		var mail=jQuery("#getemail").val();
		jQuery("#email").val( mail );
 	}
}
</script>