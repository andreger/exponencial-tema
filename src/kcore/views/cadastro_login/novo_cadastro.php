<?php
	KLoader::helper("RecaptchaHelper");
?>
<div class="col-12 bg-gray p-4">
	<div class="text-blue-2 font-20 mb-3">Desejo me cadastrar</div>
	<form id="register-form" class="form-group details" action="<?= $register_url?$register_url:"" ?>"
		method="post" enctype="multipart/form-data">
		<div class="login-loading-msg" style="display: none;">
			<label class="font-10 font-weight-bold text-secondary mt-0">Enviando...</label>
		</div>
		<div class="row">
			<div class="col-md-6">
				<input type="text" placeholder="Nome" class="form-control mb-3"
					id="nome" name="nome" value="<?= isset($_POST['nome'])?$_POST['nome']:'' ?>" required />
			</div>
			<div class="col-md-6">
				<input type="text" class="form-control mb-3"
					placeholder="Sobrenome" id="sobrenome" name="sobrenome" value="<?= isset($_POST['sobrenome'])?$_POST['sobrenome']:'' ?>"
					required />
			</div>
			<div class="col-12">
				<input type="text" class="form-control mb-3"
					placeholder="Data de Nascimento" name="data_nascimento"
					id="data_nascimento" value="<?= isset($_POST['data_nascimento'])?$_POST['data_nascimento']:'' ?>" required />
			</div>
			<div class="col-md-6">
				<input type="email" placeholder="E-mail"
					class="form-control mb-3" name="email" id="email" value="<?= isset($_POST['email'])?$_POST['email']:'' ?>"
					required />
			</div>
			<div class="col-md-6">
				<input type="email" placeholder="Confirme seu e-mail"
					class="form-control mb-3" name="confirm_email"
					id="confirm_email" value="<?= isset($_POST['confirm_email'])?$_POST['confirm_email']:'' ?>" required />
			</div>
			<div class="col-12 col-md-6">
				<input type="password" placeholder="Senha"
					class="form-control mb-3" id="senha" name="senha" value="<?= isset($_POST['senha'])?$_POST['senha']:'' ?>"
					required />
			</div>
			<div class="col-12 col-md-6">
				<input type="password" placeholder="Confirme sua senha"
					class="form-control mb-3" name="confirm_senha"
					id="confirm_senha" value="<?= isset($_POST['confirm_senha'])?$_POST['confirm_senha']:'' ?>" required />
			</div>
			<div class="col-12 p-8">
				<div>
					<input width="30px" type="radio" name="terms" value="1"  <?= isset($_POST['terms'])?'checked':'' ?>
						id="terms" required /> <a class="ml-1 t-d-none text-blue"
						href="/termos-de-uso" target="_blank">Li e aceito os termos do
						serviço</a>
				</div>
			</div>
			<div class="col-12 p-8">
				<div>
					<input type="checkbox" name="check" value="checked" <?= isset($_POST['check'])?$_POST['check']:'' ?> /> <span
						class="ml-1"> Desejo receber e-mails informativos de técnicas
						de estudo, cursos e coaching.</span>
				</div>
			</div>
			<div class="col-12 text-center mt-2">
				<?= carregar_recaptcha() ?>
			</div>
			<div class="col-12 text-right mt-2">
				<button type="button" name="submit-register" value="<?= $is_ajax?'':'submit-register' ?>"
					id="submit-register" class="btn u-btn-primary font-14">Cadastrar</button>
			</div>

		</div>
		<?php if(!$is_ajax): ?>
			<input type="hidden" name="submit-register" value="1" />
		<?php endif; ?>
	</form>

</div>
