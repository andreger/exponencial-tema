<div class="container pt-4">
	<div class="h1-login pt-2">
		<h1>IDENTIFICAÇÃO</h1>
	</div>
	<div class="row pt-4 pb-5" id="cadastro">
	<?php if(isset($_GET['rs']) && $_GET['rs']) : ?>
		<div id="cadastro-success-msg" class="col-12 text-center">
			<p class="border border-success alert alert-success">Seu cadastro foi
				atualizado com sucesso! É necessário entrar novamente com seu e-mail
				e senha!</p>
		</div>
	<?php endif; ?>

	<?php if(!$erro_validacao && isset($_POST['submit-register']) && $_POST['submit-register']) : ?>
			<div id="cadastro-success-msg" class="col-12 text-center">
				<p class="border border-success alert alert-success">Obrigado por seu
					cadastro! Para acessar a sua conta entre com seu e-mail e senha!</p>
			</div>
		<?php endif; ?>

		<div id="cadastro-error-msg" class="col-12 text-center"  style="<?= $erro_validacao?'':'display: none;' ?>">
			<p class="border border-error alert alert-danger"><?= $erro_validacao ?></p>
		</div>
		
		<?php if($debug): ?>
			<div id="cadastro-debug-msg" class="col-12 text-center">
				<p class="border border-info alert alert-info"><?= $debug ?></p>
			</div>
		<?php endif; ?>
			
		<div class="col-12 col-lg-6">
			<?php KLoader::view('cadastro_login/ja_sou_aluno', [ 'is_ajax' => $is_ajax ]) ?>
			<?php KLoader::view('cadastro_login/login_redes_sociais') ?>		
		</div>

		<div class="col-12 col-lg-6 mt-4 mt-lg-0 mb-5 mb-lg-0">
			<?php KLoader::view('cadastro_login/novo_cadastro', ['register_url' => $register_url, 'is_ajax' => $is_ajax]) ?>
		</div>
	</div>

</div>