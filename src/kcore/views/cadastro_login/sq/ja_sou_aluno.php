<div class="col-lg-12 bg-gray p-4">
	<div class="text-blue font-20 mb-3">Já sou aluno</div>
	
	<div class="login_form">
    
        <form id="cadastro-login-form" method="POST" class="form-group" action="<?php echo wp_login_url($_GET['ref']) ?>">	
			
			<?php if((isset($_GET['login']) && $_GET['login'] == 'failed') || $is_ajax) : ?>
				<div class="login-error-msg" style="<?= $is_ajax?'display: none;':'' ?>">
					<label class="font-10 font-weight-bold text-danger mt-0">Por favor, informe seu usuário/senha corretamente.</label>
				</div>
			<?php endif; ?>
			<div class="login-loading-msg" style="display: none;">
				<label class="font-10 font-weight-bold text-secondary mt-0">Enviando...</label>
			</div>

    		<input class="form-control mb-3" id="login-email" type="text" name="log" placeholder="E-mail" value="" required="required" /> 
    		<input id="login-senha" class="form-control mb-2 col-md-6" type="password" name="pwd" placeholder="Senha" value="" required="required" />
    		<div class="esqueci mb-2">
    			<a class="text-blue" href="/recuperacao-de-senha">Esqueci minha senha</a>
    		</div>
    		<div class="text-right">
    			<button name="sub" type="button" id="login-submit"	class="btn font-14 btn-primary mb-2">Entrar</button>
    		</div>
    	</form>
	</div>
</div>