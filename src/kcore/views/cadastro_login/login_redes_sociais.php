<div class="col-12 bg-gray p-4 mt-4">
	<div class="text-blue-2 font-20 mb-3">Login com Rede Social</div>
	
	<a class="btn u-btn-facebook btn-lg font-12 w-100" href="/questoes/login/facebook">
		<i class="fa fa-facebook"></i>&nbsp;&nbsp;Login com Facebook
	</a>

    <a class="btn u-btn-google-plus btn-lg font-12 w-100 mt-3 text-white" href="/questoes/login/google">
        <i class="fa fa-google"></i>&nbsp;&nbsp;Login com Google
    </a>
</div>