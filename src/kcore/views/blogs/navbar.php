<?php
$b_top_user = 'b-top';
if($usuario_id = get_current_user_id()) {
    if(exibe_assinatura_bar($usuario_id)) {
        $b_top_user = 'b-top-ass-bar';
    }
}
?>
<div class="<?= $b_top_user ?> bg-blue col-12 pt-1 border-top b-t-b">
    <div class="row ml-auto mr-auto">
    	<div class="d-none d-md-block col-md-1 col-lg-3"></div>
    	<div class="col-12 col-md-3 col-lg-2 text-center">
        <a class="t-d-none" href="/blog-artigos">
	        <img title="" alt="Artigos" src="<?php echo get_template_directory_uri(); ?>/images/icon-artigos.png" height="35" class="" />
	        <span class="text-white font-weight-bold font-14">Artigos
        </span></a>
        </div>
        <div class="pl-4 pl-md-0 col-12 col-md-3 col-lg-2 text-center">
		<a class="t-d-none" href="/blog-noticias">
	        <img title="" alt="Notícias" src="<?php echo get_template_directory_uri(); ?>/images/icon-noticias.png" height="35" class="" />
	        <span class="text-white font-weight-bold font-14">Notícias</span></a>
	    </div>
	    <div class="pl-4 pl-md-0 col-12 col-md-3 col-lg-2 text-center">
        <a class="t-d-none" href="/blog-videos">
	        <img title="" alt="Vídeos" src="<?php echo get_template_directory_uri(); ?>/images/icon-videoss.png" height="35" class="mt-1" />
	        <span class="text-white font-weight-bold font-14">Vídeos</span></a>
	    </div>    
	</div>
</div>