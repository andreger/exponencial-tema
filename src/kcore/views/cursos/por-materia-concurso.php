<?php
$msg = "";
if(isset($_GET['expirado'])) {
	$msg = "<br><div class='alert alert-primary text-center'><strong>Atenção:</strong> O curso que está procurando não está disponível. Verifique abaixo os cursos existentes em nosso site. <br>Em caso de dúvidas, <a href='/fale-conosco'>clique aqui</a> e entre em contato conosco, por favor.</div>";

	$_SESSION['flash_expirado']	= NULL;
}
?>
<script src="<?php tema_js_url('filtro-cursos.js') ?>"></script>
<div class="container pb-5 p-0"> 
  <?= $msg ?>

  <div class="row ml-auto mr-auto">	
    <div class="col-7 ml-auto mr-auto col-md-4 col-lg-3 mt-3">	
      <?= get_nav_cursos() ?>
    </div>	
    <div class="col-12 col-md-8 col-lg-9">
      <div class="pl-2 pr-2 pl-md-0 pr-md-0 pl-lg-3 pr-lg-3 mt-4 mt-md-2 col-12 text-center">			
       <h1><?= get_h1() ?></h1>
     </div>
     <div class="row col-12 ml-auto mr-auto">	
      <div class="p-l-r pl-3 col-12 col-md-8 col-lg-9 col-xl-8">
        <div class="row mb-3 mt-3">
         <div class="col p-0 text-right">
           <input class="pesquisa-barra form-control" type="text" id="search" placeholder="<?= $placeholder ?>" /></div>
           <div class="w-b-l p-0 text-left">

            <div class="pesquisa-barra-lupa">
             <i class="fa fa-search mt-2 ml-2 text-white"></i>		
           </div>

         </div>
       </div>    	
     </div>
     <div class="d-none d-md-block mt-2 pt-md-0 pt-lg-1 col-12 col-md-4 col-lg-3 col-xl-4 text-md-right text-center pr-0">
       <a class="pr-1 pointer" id="cursos-linha"><img width="50" src="<?= get_tema_image_url('view-linha.png') ?>"></a>
       <a class="pointer" id="cursos-grid"><img  width="50" src="<?= get_tema_image_url('view-grid.png') ?>"></a>
     </div>
   </div>
   <div class="mt-3 mt-md-2 col-12 text-justify">O Exponencial Concursos possui os melhores cursos online para concursos. Nossa metodologia exclusiva é composta por muitas esquematizações e mapas mentais para que você consiga APRENDER de forma mais fácil, MEMORIZAR de forma mais efetiva, REVISAR de forma mais rápida.
   Entre na matéria e confira todos os cursos disponíveis.</div>
 </div>
</div>   	

<div class="row ml-auto mr-auto col-12 list">
  <?php foreach ($items as $item) : ?>

    <?php if(($item->name ?? null) == 'Simulados') continue; ?>

    <div class="linha-grade mt-4 col-12">
      <div class="borda-grade col-12 p-0">

        <a class="t-d-none" href="<?= $item->url ?>">
        	<div class="row ml-auto mr-auto font-roboto font-14 text-blue-2">
            <div class="nome_prof sub_name p-3 col-10 col-lg-11  border-cursos-1 text-dark"><?= $item->titulo ?></div>
            <div class="sub_name sub_number p-3 col-2 col-lg-1 text-center bg-agua-ulta-light border-cursos-2"><?= $item->qtde_cursos; ?></div>
          </div>							
          <div class="cursos-botao-grid col-12">
          </div>
        </a>
      </div>
    </div>
  <?php endforeach; ?>

</div>
</div>
<script>
  jQuery(function() {                       
    jQuery("#cursos-grid").click(function() {  
      jQuery(".linha-grade").addClass("col-4").removeClass("col-12");
      jQuery(".borda-grade").addClass("border rounded ");
      jQuery(".nome_prof").addClass("col h-grade-cursos")
      .removeClass("col-10 col-lg-11 border-cursos-1");
      jQuery(".sub_number").addClass("col-cursos-grade")
      .removeClass("col-2 col-lg-1 bg-agua-ulta-light border-cursos-2");
      jQuery(".cursos-botao-grid").html("<a class='col-12 btn u-btn-blue'>Veja os cursos</a>").addClass("bg-gray pt-3 pb-3 text-center");
    });
    jQuery("#cursos-linha").click(function() {  
      jQuery(".linha-grade").addClass("col-12").removeClass("col-4");
      jQuery(".borda-grade").removeClass("border rounded ");
      jQuery(".nome_prof").removeClass("col h-grade-cursos")
      .addClass("col-10 col-lg-11 border-cursos-1");
      jQuery(".sub_number").removeClass("col-cursos-grade")
      .addClass("col-2 col-lg-1 bg-agua-ulta-light border-cursos-2");
      jQuery(".cursos-botao-grid")
      .empty()
      .removeClass("bg-gray pt-3 pb-3 text-center");
    });
  });  

  jQuery(function() {                       
    if(jQuery(this).width() < 576){      
      jQuery(".borda-grade").addClass("border rounded ");
      jQuery(".nome_prof").addClass("col-9 h-grade-cursos")
      .removeClass("col-10 col-lg-11 border-cursos-1");
      jQuery(".sub_number").addClass("col-cursos-grade col-3")
      .removeClass("col-2 col-lg-1 bg-agua-ulta-light border-cursos-2");
      jQuery(".cursos-botao-grid").html("<a class='col-12 btn u-btn-blue'>Veja os cursos</a>").addClass("bg-gray pt-3 pb-3 text-center");
    }; 
  });  

</script>
<?php get_footer();?>