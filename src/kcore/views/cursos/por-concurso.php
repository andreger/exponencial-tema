<?php if( $premiums || $destaques || $pacotes || $cursos || $simulados || $pacotes_simulados || $cadernos ) : ?>

    <?php if($premiums) : ?>

        <div><a class="anchor" id="assinaturas"></a></div>
        <div class="col-12 text-center mt-3">	
            <h2><span class="font-pop-h2"> Assinaturas</span></h2>
        </div>

        <?= get_concursos_produtos_celulas($premiums, 'assinatura', TRUE, $qtd_premiums > LIMITE_POR_CONCURSO_ESPECIFICO_SESSAO ? $ver_todos_url : null); ?>

    <?php endif; ?>

    <?php if($destaques) : ?>

        <div><a class="anchor" id="destaques"></a></div>
        <div class="col-12 text-center mt-3">	
            <h2><span class="font-pop-h2"> Em Destaque - <?= $title ?></span></h2>
        </div>

        <?= get_concursos_produtos_celulas($destaques, 'destaque', TRUE, $qtd_destaques > LIMITE_POR_CONCURSO_ESPECIFICO_SESSAO ? $ver_todos_url : null); ?>

    <?php endif; ?>

    <?php if($pacotes) : ?>

        <div><a class="anchor" id="pacotes"></a></div>
        <div class="col-12 text-center mt-3">	
            <h2><span class="font-pop-h2"> Pacotes - <?= $title ?></span></h2>
        </div>

        <?= get_concursos_produtos_celulas($pacotes, 'pacote', TRUE, $qtd_pacotes > LIMITE_POR_CONCURSO_ESPECIFICO_SESSAO ? $ver_todos_url : null); ?>

    <?php endif; ?>

    <?php if($cursos) : ?>

        <div><a class="anchor" id="individuais"></a></div>
        <div class="col-12 text-center mt-3">	
            <h2><span class="font-pop-h2"> Cursos Individuais - <?= $title ?></span></h2>
        </div>

        <?= get_concursos_produtos_celulas($cursos, 'nao-pacote', TRUE, $qtd_cursos > LIMITE_POR_CONCURSO_ESPECIFICO_SESSAO ? $ver_todos_url : null); ?>

    <?php endif; ?>


    <?php if($simulados || $pacotes_simulados) : ?>

        <div><a class="anchor" id="simulados"></a></div>
        <div class="col-12 text-center mt-3">	
            <h2><span class="font-pop-h2"> Simulados - <?= $title ?></span></h2>
        </div>

    <?php endif; ?>

    <?php if($pacotes_simulados) : ?>

        <?= get_concursos_produtos_celulas($pacotes_simulados, 'pacote', TRUE, $qtd_pacotes_simulados > LIMITE_POR_CONCURSO_ESPECIFICO_SESSAO ? $ver_todos_url : null); ?>
    
        <?php endif; ?>

    <?php if($qtd_pacotes_simulados < LIMITE_POR_CONCURSO_ESPECIFICO_SESSAO && $simulados) : ?>
    
        <?= get_concursos_produtos_celulas(array_slice($simulados, 0, LIMITE_POR_CONCURSO_ESPECIFICO_SESSAO - $qtd_pacotes_simulados), 'nao-pacote', TRUE, ($qtd_pacotes_simulados + $qtd_simulados) > LIMITE_POR_CONCURSO_ESPECIFICO_SESSAO ? $ver_todos_url : null); ?>
    
        <?php endif; ?>

    <?php if($cadernos) : ?>

        <div><a class="anchor" id="questoes"></a></div>
        <div class="col-12 text-center mt-3">	
            <h2><span class="font-pop-h2"> Questões Online - <?= $title ?></span></h2>
        </div>

        <?= get_concursos_produtos_celulas($cadernos, 'nao-pacote', TRUE, $qtd_cadernos > LIMITE_POR_CONCURSO_ESPECIFICO_SESSAO ? $ver_todos_url : null); ?>

    <?php endif; ?>

<?php else: ?>

    <div class="pb-5 text-center">Nenhum curso encontrado</div>
    
<?php endif; ?>