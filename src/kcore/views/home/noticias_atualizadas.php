<?php
include_once $_SERVER['DOCUMENT_ROOT'] .'/wp-load.php';

KLoader::model("BlogModel");
KLoader::helper("BlogHelper");
KLoader::helper("UiLabelHelper");
KLoader::helper("UrlHelper");
KLoader::model("ColaboradorModel");
?>
<div class="col-12 text-center font-pop mb-3 mt-5">Notícias Atualizadas</div>

<div class="container mt-3 mb-3 font-14">
    <div class="col-12 text-center">
        <span class="d-block d-md-inline mr-2 ml-2">Filtrar por:</span>
        <span class="d-block d-md-inline mr-2 ml-2"><a id="filtro-noticias" class="text-dark t-d-none" href="#"> Notícias</a></span>
        <span class="d-block d-md-inline mr-2 ml-2"><a id="filtro-artigos" class="text-dark t-d-none" href="#"> Artigos</a></span>
        <span class="d-block d-md-inline mr-2 ml-2"><a id="filtro-videos" class="text-dark t-d-none" href="#"> Vídeos</a></span>
    </div>
</div>

<div class="container">
    <div class="row woocommerce home_midia mt-3 ml-auto mr-auto">
        <ul id="midias" class="products">
            <?php
            $data['blogs'] = BlogModel::listar();

            $i = 0;
            foreach ($data['blogs'] as $blog) :

                $url = UrlHelper::get_blog_post_url($blog);
                $tipos = BlogModel::listar_tipos($blog->blo_id);
                ?>
                <li class="<?= $i % 3 == 0 ? 'first-1' : ''?> product product-2 type-product status-publish has-post-thumbnail">
                    <div class="bg-white box-home-midia border p-2 rounded">
                        <div class="rounded">
                            <a href="<?= $url ?>">
                                <img alt="<?= $blog->blo_slug ?>" src="<?= UrlHelper::get_blog_post_thumb_url($blog) ?>">
                            </a>

                            <?= UiLabelHelper::get_blog_label_html($blog->blo_id) ?>

                        </div>
                        <div>

                            <?php if(trim($blog->post_title)): ?>
                                <h5 class="titulo-midia-home">
                                    <a href="<?= $url ?>">
                                        <?= $blog->post_title ?></a>
                                </h5>
                            <?php endif; ?>

                            <div class="mt-2 mb-3 ">
                                <?php if($colaborador = ColaboradorModel::get_by_id($blog->user_id)) : ?>
                                    <a class="font-10" href="<?= UrlHelper::get_professor_url($colaborador->col_slug) ?>"><?= $blog->display_name ?></a>
                                <?php else : ?>
                                    <span class="font-10"><?= $blog->display_name ?></span>
                                <?php endif ?>
                            </div>
                            <div class="font-10"><?= $blog->blo_resumo ?></div>
                        </div>
                        <div class="col-12 col-lg-11 text-center leia-mais-absolute mb-2">
                            <a class="ml-7-5 ml-md-4 ml-lg-5 btn u-btn-primary" href="<?= $url ?>">Leia mais</a>
                        </div>
                    </div>
                </li>
                <?php $i++; ?>
            <?php endforeach; ?>
        </ul>
    </div>
</div>

<div style="text-align: center; margin-bottom: 30px;"><a class="btn u-btn-darkblue font-12 font-arial font-weight-bold" href="/blog-noticias">+ Noticias</a>
    <a class="btn btn-warning font-12 font-arial font-weight-bold text-white" href="/blog-artigos">+ Artigos</a>
    <a class="btn btn-danger font-12 font-arial font-weight-bold" href="/blog-videos">+ Videos</a>
</div>