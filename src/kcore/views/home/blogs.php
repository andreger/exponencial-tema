<?php KLoader::model("ColaboradorModel") ?>
<?php KLoader::helper("UrlHelper") ?>

<?php $i = 0; ?>
<?php foreach ($blogs as $blog) : ?>
	<?php
		$url = UrlHelper::get_blog_post_url($blog);
		$tipos = BlogModel::listar_tipos($blog->blo_id);
	?>
	<li class="<?= $i % 3 == 0 ? 'first-1' : ''?> product product-2 type-product status-publish has-post-thumbnail">
		<div class="bg-white box-home-midia border p-2 rounded">
			<div class="rounded">
				<a href="<?= $url ?>">				
					<img alt="<?= $blog->blo_slug ?>" src="<?= UrlHelper::get_blog_post_thumb_url($blog) ?>">
				</a>
				
				<?= UiLabelHelper::get_blog_label_html($blog->blo_id) ?>
				
			</div>
			<div>			
				
			<?php if(trim($blog->post_title)): ?>
				<h5 class="titulo-midia-home">
					<a href="<?= $url ?>">
							<?= $blog->post_title ?></a>
				</h5>
			<?php endif; ?>

				<div class="mt-2 mb-3 ">
					<?php if($colaborador = ColaboradorModel::get_by_id($blog->user_id)) : ?>
						<a class="font-10" href="<?= UrlHelper::get_professor_url($colaborador->col_slug) ?>"><?= $blog->display_name ?></a>
					<?php else : ?>
						<span class="font-10"><?= $blog->display_name ?></span>
					<?php endif ?>
				</div>
				<div class="font-10"><?= $blog->blo_resumo ?></div>		
			</div>
			<div class="col-12 col-lg-11 text-center leia-mais-absolute mb-2">
				<a class="ml-7-5 ml-md-4 ml-lg-5 btn u-btn-primary" href="<?= $url ?>">Leia mais</a>
			</div>
		</div>
	</li>
	<?php $i++; ?>
<?php endforeach; ?>