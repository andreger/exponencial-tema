<?php if($concursos) : ?>
	<?php $i = 0; ?>
	<?php foreach ($concursos as $concurso) : ?>

		<?php $url = UrlHelper::get_cursos_por_concurso_especifico_url($concurso->con_slug); ?>

		<li class="<?= $i % 3 == 0 ? 'first' : ''?> product">
			<div class="bg-white box-home-concursos border p-2 rounded">
				<div class="rounded">
					<a href="<?= $url ?>">
						<img alt="<?= $concurso->con_slug ?>" src="<?= $concurso->con_thumb ?>">
					</a> 	
					<?= UiLabelHelper::get_concurso_label_html($concurso->con_status) ?>		
					
				</div>
				<div class="pl-2">
					<a class="link-azul text-blue font-weight-bold" href="<?= $url ?>">
						<?= $concurso->post_title ?></a>						
					<?php if($concurso->con_menor_preco > 0) : ?>
					<p class="margin-preço line-height-0 font-10">A partir de <?= moeda($concurso->con_menor_preco) ?></p>
					<?php endif ?>					
				</div>
			</div> 
		</li>
	<?php $i++; ?>
	<?php endforeach; ?>
<?php else : ?>
	<div>Não existem concursos para esse filtro.</div>
<?php endif ?>