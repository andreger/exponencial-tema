<script src="https://apis.google.com/js/platform.js?onload=renderOptIn" async defer></script>
<script>  
    window.renderOptIn = function() {    
        window.gapi.load('surveyoptin', function() {      
            window.gapi.surveyoptin.render({
                "merchant_id": 112461917,          
                "order_id": "<?= $order_id ?>",          
                "email": "<?= $user_email ?>",          
                "delivery_country": "<?= $user_country ?>",          
                "estimated_delivery_date": "<?= $order_date ?>",
            });    
        }); 
    }
</script>
<script>
window.___gcfg = {
  lang: 'pt_BR'
};
</script>