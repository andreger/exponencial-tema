<script src="https://apis.google.com/js/platform.js?onload=renderBadge" async defer></script>
<script>
    window.renderBadge = function() {    
        var ratingBadgeContainer = document.createElement("div");
        document.body.appendChild(ratingBadgeContainer);    
        window.gapi.load('ratingbadge', function() {      
            window.gapi.ratingbadge.render(
                ratingBadgeContainer, {
                    "merchant_id": 112461917
                });    
        });  
    }
</script>
<script>
  window.___gcfg = {
    lang: 'pt_BR'
  };
</script>