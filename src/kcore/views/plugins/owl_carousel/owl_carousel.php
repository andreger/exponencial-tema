<?php
/**
 * Carrega o css e js do Owl Carousel
 *
 * @see https://owlcarousel2.github.io/OwlCarousel2/
 * 
 * @since 10.1.0
 */
?>
<link href='/wp-content/themes/academy/css/owl.carousel.min.css' rel='stylesheet'>
<link href='/wp-content/themes/academy/css/owl.theme.default.css' rel='stylesheet'>
<script src='/wp-content/themes/academy/js/owl.carousel.min.js'></script>
