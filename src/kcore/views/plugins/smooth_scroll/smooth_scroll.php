<?php
/**
 * Plugin Smooth Scrool
 * 
 * @since Joule 2
 * 
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/1324
 * 
 * @return string HTML do componente
 */
?>


<script src='/wp-content/themes/academy/js/smooth-scroll.min.js'></script>
<script>var scroll = new SmoothScroll('.smooth_scroll');</script>