<?php 

KLoader::model("AvisoModel");
    
$avisos_ids	= [];
if($_COOKIE) { 
    foreach($_COOKIE as $key => $value) {
        if(starts_with($key, 'aviso-')) {
            $key_a = explode("-", $key); 
            $avisos_ids[] = $key_a[1];
        }
    }
}

$aviso = AvisoModel::get_aviso_valido($avisos_ids);

if($aviso) {
    $titulo = $aviso['titulo'];
    $mensagem = $aviso['mensagem'];
    $av = "toastr['info']('{$mensagem}', '{$titulo}');";
    $fundo = $aviso['fundo'] ?: "#FDF4BD";
    $cookies_str = $aviso['dias_expiracao'] ? 
      "Cookies.set('aviso-{$aviso['id']}', '1', {expires: {$aviso['dias_expiracao']} * 3600 * 24 })" : 
      "Cookies.set('aviso-{$aviso['id']}', '1')";

    $onclick = $aviso['link'] ? "toastr.options.onclick = function () {
                        window.location.href = '{$aviso['link']}';
                    };" : "";
    $onclose = "toastr.options.onCloseClick = function() {
                {$cookies_str}
        }";
                
    echo
        "<link href='/wp-content/themes/academy/css/toastr.min.css' rel='stylesheet'>
        <link href='/wp-content/themes/academy/css/toastr-ext.css' rel='stylesheet'>
        <style>.toast-info {background-color: {$fundo}; }</style>
        <script src='/wp-content/themes/academy/js/toastr.min.js'></script>
        <script>
            jQuery(function() {
                setTimeout(function() {
                    toastr.options = {
                      'closeButton': true,
                      'debug': false,
                      'newestOnTop': false,
                      'progressBar': false,
                      'positionClass': 'toast-bottom-right',
                      'preventDuplicates': false,
                      'onclick': null,
                      'showDuration': '1000',
                      'hideDuration': '1000',
                      'timeOut': '100000',
                      'extendedTimeOut': '100000',
                      'showEasing': 'swing',
                      'hideEasing': 'linear',
                      'showMethod': 'fadeIn',
                      'hideMethod': 'fadeOut',

                    }
                    {$onclose}
                    
                    {$onclick}							

                    {$av}

                }, 1000);
            });
        </script>";
    
}

?>