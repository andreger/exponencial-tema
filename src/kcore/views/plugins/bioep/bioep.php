<?php
/**
 * Componente de captura de saída
 *
 * @see http://beeker.io/exit-intent-popup-script-tutorial
 * 
 * @see https://github.com/beeker1121/exit-intent-popup
 */
?>

<?php 
    
    KLoader::model('CapturaSaidaModel'); 

    $saidas_ids	= [];
    
    if($_COOKIE) { 
        foreach($_COOKIE as $key => $value) {
            if(starts_with($key, 'saida-')) {
                $key_a = explode("-", $key); 
                $saidas_ids[] = $key_a[1];
            }
        }
    }
    
    $saida = CapturaSaidaModel::listar_ativa($saidas_ids);
    
	$conteudo = $saida['sai_html']; 
	$fundo = $saida['sai_fundo'] ?: "#FDF4BD";
	$link = $saida['sai_link'];
	$id = 'saida-' . $saida['sai_id'];
	$dias = $saida['sai_dias_expiracao'] ?: 0;
	
	if(!isset($_COOKIE[$id]) || !$_COOKIE[$id]): ?>

    	<?php if($link){ 
    		$onclick = "onclick=\"window.location.href=\'{$link}\';\"";
    		$cursor = "cursor: pointer;";
         } ?>
    	
    	<?php if($conteudo): ?>
        
        <script src='/wp-content/themes/academy/js/bioep.js'></script>
    				
        <script>
            var saida = bioEp.init({
                html: '<div id=\"bio_ep_content\" <?= $onclick ?>><div id=\"bio_ep_close\">X</div><?= $conteudo ?></div>',
                css: '#bio_ep{width: auto; height: auto; z-index: 100000; background-color: <?= $fundo ?>;} #bio_ep_content{padding: 20px; <?= $cursor ?>} #bio_ep_close{top: 0;}',
                cookieExp: 0,
                delay: 1,
                saida_id: '<?= $id ?>',
                duracao: <?= $dias ?>        
            });
        </script>
        
        <?php endif; ?>

	<?php endif; ?>