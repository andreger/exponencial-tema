<div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="bg-blue navbar-nav ml-auto mr-auto">

        <?php if(is_user_logged_in()): ?>
        <li class="mav-item active">
            <a class="nav-link font-roboto font-weight-bold d-lg-none" href="/minha-conta">
                ÁREA DO ALUNO
            </a>
        </li>
        <?php endif; ?>

 		<li class="nav-item active">
            <a class="nav-link mr-3 ml-3 mr-xl-4 ml-xl-4 font-roboto font-weight-bold" href="/assinaturas">
            	ASSINATURAS
            </a>
        </li>

        <li class="d-block  d-xl-none  nav-item bg-orange active bg-green">
            <a class="nav-link mr-3 ml-3 mr-xl-4 ml-xl-4 font-roboto font-weight-bold" href="/questoes/main/resolver_questoes">
            	SQ GRÁTIS
            </a>
        </li>
        
        <li class="d-none d-xl-block nav-item active">
            <div class="nav-link dropdownres">
                <a class="w-drop mr-0 ml-0 mr-xl-4 ml-xl-2 font-roboto font-weight-bold">CURSOS ONLINE</a>
                <div class="dropdown-content rounded p-1 mt-2">
                    <i class="fa fa-caret-down"></i>
                    <a class="dropdown-item" href="/cursos-por-concurso">Por concurso</a>
                    <a class="dropdown-item" href="/cursos-por-professor">Por professor</a>
                    <a class="dropdown-item" href="/cursos-por-materia">Por matéria</a>
                    <a class="dropdown-item" href="/todos-cursos">Todos os cursos</a>
                </div>
            </div>
        </li>

        <li class="d-none d-xl-block mt-0 nav-item active p-0">
            <div class="w-drop bg-green nav-link dropdownres">
                <a class="font-roboto font-weight-bold">MATERIAL GRÁTIS</a>
                <div class="bg-green-light dropdown-contentt rounded p-1 mt-2 mr-2">
                    <i class="fa fa-caret-down"></i>
                    <a class="dropdown-item" href="/cursos-gratis">Cursos Grátis</a>
                    <a class="dropdown-item" href="/simulados-gratis">Simulados Grátis</a>
                    <a class="dropdown-item" href="/mapas-mentais-gratis">Mapas Mentais Grátis</a>
                    <a class="dropdown-item" href="/questoes-comentadas-gratis">Questões Comentadas Grátis</a>
                    <a class="dropdown-item" href="/audiobooks-gratis">Audiobooks Grátis</a>
                </div>
			</div>
        </li>

        <li class="d-block bg-green d-xl-none nav-item dropdown active">
            <a class="font-roboto font-weight-bold nav-link dropdown-toggle mr-5 ml-5" href="#" id="menu-material-gratis" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
   			      MATERIAL GRÁTIS 
  			      </a>
            <div class="dropdown-menu text-center bg-green-light" aria-labelledby="menu-material-gratis">
                <a class="dropdown-item text-white" href="/cursos-gratis">Cursos Grátis</a>
                <a class="dropdown-item text-white" href="/simulados-gratis">Simulados Grátis</a>
                <a class="dropdown-item text-white" href="/mapas-mentais-gratis">Mapas Mentais Grátis</a>
                <a class="dropdown-item text-white" href="/questoes-comentadas-gratis">Questões Comentadas Grátis</a>
                <a class="dropdown-item text-white" href="/audiobooks-gratis">Audiobooks Grátis</a>
            </div>
        </li>

        <li class="d-block d-xl-none nav-item dropdown active">
            <a class="nav-link dropdown-toggle mr-5 ml-5 font-roboto font-weight-bold" href="#" id="menu-cursos-online" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                CURSOS ONLINE
            </a>
            <div class="dropdown-menu text-center bg-blue-light" aria-labelledby="menu-cursos-online">
                <a class="dropdown-item text-white" href="/cursos-por-concurso">Por concurso</a>
                <a class="dropdown-item text-white" href="/cursos-por-professor">Por professor</a>
                <a class="dropdown-item text-white" href="/cursos-por-materia">Por matéria</a>
                <a class="dropdown-item text-white" href="/todos-cursos">Todos os cursos</a>
            </div>
        </li>

        <li class="nav-item active">
            <a class="nav-link mr-3 ml-3 mr-xl-4 ml-xl-4 font-roboto font-weight-bold" href="/eventos/sistema-de-questoes-plataforma-100-gratuita">SISTEMA DE QUESTÕES</a>
        </li>

        <li class="nav-item active">
            <a class="nav-link mr-3 ml-3 mr-xl-4 ml-xl-4 font-roboto font-weight-bold" href="/simulados">SIMULADOS</a>
        </li>

        <li class="nav-item active">
            <a class="nav-link mr-3 ml-3 mr-xl-4 ml-xl-4 font-roboto font-weight-bold" href="/coaching/coaching">COACHING</a>
        </li>

        <li class="nav-item active">
            <a class="nav-link mr-3 ml-3 mr-xl-4 ml-xl-4 font-roboto font-weight-bold" href="/concurso/tropa-da-aprovacao">TROPA DA APROVAÇÃO</a>
        </li>

        <!-- <li class="nav-item active">
            <a class="nav-link mr-3 ml-3 mr-xl-4 ml-xl-4 font-roboto font-weight-bold" href="/professores">PROFESSORES</a>
        </li> -->
        <li class="nav-item active">
            <a class="nav-link mr-3 ml-3 mr-xl-4 ml-xl-4 font-roboto font-weight-bold" href="/blog-noticias">BLOG</a>
        </li>

        <li class="d-block  d-xl-none  nav-item active">
            <a class="nav-link mr-3 ml-3 mr-xl-4 ml-xl-4 font-roboto font-weight-bold" href="/fale-conosco">FALE CONOSCO</a>
        </li>
    </ul>
</div>