<?php
global $woocommerce;

$qtde_itens = $woocommerce->cart->cart_contents_count;

KLoader::view("header/barra_superior/carrinho/carrinho_botao", ["qtde_itens" => $qtde_itens]);

if($qtde_itens > 0) {
    KLoader::view("header/barra_superior/carrinho/carrinho_popover");
}