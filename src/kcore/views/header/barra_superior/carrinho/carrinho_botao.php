<span class="d-none d-md-inline-block header-botao toolbar-carrinho">
    <a class="mr-0 mr-lg-4 pr-lg-0 pr-2 pl-lg-3 pl-2 cart-button" href="/checkout">
        <span class="comum-carrinho d-inline-block align-bottom"></span>
		<span id="carrinho-qtde" data-qtde="<?= $qtde_itens ?>">
            <?= $qtde_itens == 0 ?
                    "Carrinho vazio" :
                    sprintf(_n('%d item no carrinho&nbsp;', '%d itens no carrinho', $qtde_itens, 'woothemes'), $qtde_itens);
            ?>
        </span>
	</a>              	
</span>

<span class="header-botao d-inline-block d-md-none">
	<a class="t-d-none text-dark cart-button" href="/checkout">
        <span class="comum-carrinho-mobile d-inline-block align-bottom"></span>
        <span class="text-white item-carrinho-nav font-8">
            <?= $qtde_itens == 0 ?
                    "0" :
                    sprintf(_n('%d', '%d', $qtde_itens, 'woothemes'), $qtde_itens);
            ?>
        </span>
	</a> 
</span>