<?php
	$redirect_to = get_url_atual();
	if(isset($_GET['ref']) && $_GET['ref'])
	{
		$redirect_to = $_GET['ref'];
	}
	elseif(isset($_GET['uri']) && $_GET['uri'])
	{
		$redirect_to = $_GET['uri'];
	}
	elseif(isset($_GET['redirect_to']) && $_GET['redirect_to'])
	{
		$redirect_to = $_GET['redirect_to'];
	}
?>
<div class="bg-gray rounded login-box position-absolute" id="login-content">
	<span class="login-close-btn">
		<a href="#">x</a></span>
	<?php KLoader::view("header/barra_superior/login/login_popover", ['redirect_to' => $redirect_to]) ?>
</div>