<form id='frm-login-box' action='/wp-login.php' method='post'>
	<input type='hidden' name='redirect_to' value='<?= $redirect_to ?>' />
	<div class='row ml-auto mr-auto'>
        <div class='col-5 p-1'>
            <input class='rounded form-control' type='text' name='log' style='margin-bottom:10px' placeholder='E-mail' />
        </div>
        <div class='col-5 p-1'>
            <input class='rounded form-control' type='password' name='pwd' style='margin-bottom:10px' placeholder='Senha' id="login-rapido-senha" />
        </div>
        <div class='col-2 p-1'>
            <a class='btn u-btn-primary mb-1' href='#' onclick="jQuery('#frm-login-box').submit()">
                Entrar
            </a>
        </div>
    </div>

	<div class='row ml-auto mr-auto'> 
		<div class='pl-1 col-6 login-esqueceu'><a class='text-dark font-10' href='/recuperacao-de-senha'>Esqueceu a senha?</a> </div>

        <div class="col-6">
            <div class='row'>
                <div class='col-12 text-right'>
                    <a class='btn u-btn-facebook btn-sm font-arial w-100' href='/questoes/login/facebook'>
                        Entrar pelo Facebook
                    </a>
                </div>
                <div class='col-12 text-right mt-1'>
                    <a class="btn u-btn-google-plus btn-sm font-arial w-100 text-white" href="/questoes/login/google">
                        </i>Entrar pelo Google
                    </a>
                </div>
            </div>
        </div>
    </div>
</form>