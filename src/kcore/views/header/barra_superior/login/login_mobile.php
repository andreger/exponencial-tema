<?php
/**
 * @todo Confirmar com Lucas se login via mobile está ok. Este arquivo não está sendo usado, pois popup de login em
 *       mobile foi desativado. Não dá para manter o link para ir para /cadastro-login e o popup ao mesmo tempo
 */
?>
<form id='frm-login-box-mobile' action='/wp-login.php' method='post'>
	<input type='hidden' name='redirect_to' value='<?= $redirect_to ?>' />
	<div><input type='text' name='log' placeholder='E-mail' /></div>
	<div><input type='password' name='pwd' placeholder='Senha' /></div>

	<div>
		<span class='login-esqueceu'><a href='/recuperacao-de-senha'>Esqueceu a senha?</a> </span> 
		<a href='#' onclick="jQuery('#frm-login-box-mobile').submit()">
         	<img class='login-entrar' src='/wp-content/themes/academy/images/1entrar.png' alt="Entrar">
        </a>
	</div>

	<div style='margin-top:30px'>
		<a href='/questoes/login/facebook'>
			<img src='/wp-content/themes/academy/images/entrar-facebook.png' alt="Login pelo Facebook">
		</a>
	</div>
</form>
