<?php 
if(count($notificacoes) > 0){
    foreach($notificacoes as $notificacao){
        $icon = $notificacao['not_data_lida']?'fa-bell-o':'fa-bell';
        $background = $notificacao['not_data_lida']?'':'nao-lida';
        $marcar = $notificacao['not_data_lida']?'marcar-nao-lido tooltipster':'marcar-lido tooltipster';
        $title = $notificacao['not_data_lida']?"title ='Marcar como não lida'":" title='Marcar como lida'";
?>
<div class="row notificacoes-linha <?= $background ?> notificacao notificacao-<?= $notificacao['not_id'] ?>">

    <div class="btn col-lg-1">
        <a href="#" data-id="<?= $notificacao['not_id'] ?>" <?= $title ?> onclick="<?= $notificacao['not_data_lida']?"marcar_nao_lido(this); return false;":"marcar_lido(this); return false;" ?>" class="marcar <?= $marcar ?> notificacao-sino notificacao-sino-<?= $notificacao['not_id'] ?>"><i class="fa <?= $icon ?> fa-lg"></i> </a>
    </div>
    
    <div class="col-lg-10">

        <a href="<?= get_questao_url($notificacao['que_id']) ?>" data-id="<?= $notificacao['not_id'] ?>" onclick="<?= $notificacao['not_data_lida']?"":"marcar_lido(this); return true;" ?>">
            <div class="col-lg-12" style="margin-top: 4px;">
                <b><?= get_usuario_nome_exibicao( $notificacao['not_remetente_id'] ) ?> comentou a questão <?= $notificacao['que_id'] . " - " . $notificacao['dis_nome'] ?></b>
            </div>
            <div class="col-lg-12 notificacao-resumo" style="margin-top: 2px;">
                <?= substr( sanitizar( $notificacao['com_texto'] ), 0, 100 ); ?>
            </div>
        </a>
        
    </div>
    
    <span class="pull-right text-muted small"><?= converter_para_ddmmyyyy_HHiiss($notificacao['not_data_criacao']) ?></span>

</div>
<div class="divider col-lg-12"></div>
<?php 
    }
}else{
    echo "<div class='row text-align-center'>Nenhuma notificação a ser mostrada</div>";
}
?>