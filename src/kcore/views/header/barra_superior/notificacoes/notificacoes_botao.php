<?php
KLoader::helper("UrlHelper");
$posicao = "";
if(UrlHelper::is_url_sistema_questoes_admin())
{
    $posicao = "label-admin";
}
?>

<a class="dropdown-toggle count-info" data-toggle="dropdown" href="#" aria-expanded="true">
    <i class="fa fa-bell fa-lg"></i>  <span id="qtd_notificacoes" class="label label-primary <?= $posicao ?>">0</span>
</a>