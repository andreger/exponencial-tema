<span class="dropdown notificacoes-dropdown">
    <?php KLoader::view('header/barra_superior/notificacoes/notificacoes_botao') ?>
    <ul id="notificacoes-dropdown-menu" class="dropdown-menu dropdown-alerts row" style="right: 0; left: auto;">
        <div class="col-lg-12 btn-group" style="margin-bottom: 4px;">
            <button class="btn btn-primary btn-nao-lidas col-lg-6" type="button">Não lidas</button>
            <button class="btn btn-white btn-todas col-lg-6" type="button">Todas</button>
        </div>
        <div id="notificacoes-nao-lidas" class="col-lg-12 notificacoes-scroll">
        </div>
        
        <div id="notificacoes-todas" class="col-lg-12 notificacoes-scroll">
        </div>
        <div class="col-lg-12 text-align-center">
            <a href="#" class="marcar-todas-lidas"><b>Marcar todas como lidas</b></a>
        </div>
    </ul>
</span>
<script>

    var j = $;
    if(!j){
        j = jQuery;
    }

    j(document).ready(function(){

        //Carrega os dados iniciais das notificações
        j.ajax({
            url: "/questoes/xhr/carregar_notificacoes?t=" + new Date().getTime() , 
            type: "GET",
            dataType: "JSON",
            cache: false
        }).success(function(data){

            j('#qtd_notificacoes').html(data.qtd_nao_lidas);
            j('#notificacoes-nao-lidas').html(data.nao_lidas);
            j('#notificacoes-nao-lidas').scrollTop(0);
            j('#notificacoes-todas').html(data.todas);
            j('#notificacoes-todas').scrollTop(0);
            j('#notificacoes-todas').hide();
            if(data.qtd_nao_lidas == 0){
                j(".marcar-todas-lidas").parent().hide();
            }

            iniciar_scroll(data.qtd_nao_lidas, data.qtd_todas);

            gerar_tooltipster();

        }).error(function (request, status, error) {
            //alert(error);
        });

        j(".btn-nao-lidas").click(function(e){
            e.preventDefault();
            j(".btn-nao-lidas").removeClass("btn-white");
            j(".btn-nao-lidas").addClass("btn-primary");

            j(".btn-todas").removeClass("btn-primary");
            j(".btn-todas").addClass("btn-white");

            j("#notificacoes-todas").hide();
            j("#notificacoes-nao-lidas").show();
        });

        j(".btn-todas").click(function(e){
            e.preventDefault();
            j(".btn-todas").removeClass("btn-white");
            j(".btn-todas").addClass("btn-primary");

            j(".btn-nao-lidas").removeClass("btn-primary");
            j(".btn-nao-lidas").addClass("btn-white");

            j("#notificacoes-nao-lidas").hide();
            j("#notificacoes-todas").show();
        });

        j(".marcar-todas-lidas").click(function(e){
            
            j(".notificacao-sino").removeClass("marcar-lido");
            j(".notificacao-sino").addClass("marcar-nao-lido");
            j(".notificacao-sino > i").removeClass("fa-bell");
            j(".notificacao-sino > i").addClass("fa-bell-o");
            j(".notificacao").removeClass("nao-lida");
            //j(".notificacao-sino").attr("onclick", "").unbind("click");
            j(".notificacao-sino").tooltipster("destroy");
            j(".notificacao-sino").attr("title", "Marcar como não lida");
            j(".notificacao-sino").attr("onclick", "marcar_nao_lido(this); return false;");

            gerar_tooltipster();

            j.get(
                '/questoes/xhr/marcar_todas_notificacoes_como_lidas',
                function(data){
                    j('#qtd_notificacoes').html("0");
                }
            );

        });

        j(document).on('click', '#notificacoes-dropdown-menu.dropdown-menu', function (e) {
            e.stopPropagation();
        });

    });

    function marcar_lido(el){
        var id = j(el).data("id");
        
        j(".notificacao-sino-" + id).removeClass("marcar-lido");
        j(".notificacao-sino-" + id).addClass("marcar-nao-lido");
        j(".notificacao-sino-" + id +" > i").removeClass("fa-bell");
        j(".notificacao-sino-" + id +" > i").addClass("fa-bell-o");
        j(".notificacao-" + id).removeClass("nao-lida");
        j(".notificacao-sino-" + id).tooltipster("destroy");
        //j(".notificacao-sino-" + id).removeClass("tooltipstered");
        //j(".notificacao-sino-" + id).addClass("tooltipster");
        j(".notificacao-sino-" + id).attr("title", "Marcar como não lida");
        j(".notificacao-sino-" + id).attr("onclick", "marcar_nao_lido(this); return false;");
        
        gerar_tooltipster();

        j.get(
            '/questoes/xhr/marcar_notificacao_como_lida_nao_lida/'+id+'/1',
            function(data){
                j('#qtd_notificacoes').html(data);
            }
        );
    }

    function marcar_nao_lido(el){
        var id = j(el).data("id");
        
        j(".notificacao-sino-" + id).addClass("marcar-lido");
        j(".notificacao-sino-" + id).removeClass("marcar-nao-lido");
        j(".notificacao-sino-" + id +" > i").addClass("fa-bell");
        j(".notificacao-sino-" + id +" > i").removeClass("fa-bell-o");
        j(".notificacao-" + id).addClass("nao-lida");
        j(".notificacao-sino-" + id).tooltipster("destroy");
        //j(".notificacao-sino-" + id).removeClass("tooltipstered");
        //j(".notificacao-sino-" + id).addClass("tooltipster");
        j(".notificacao-sino-" + id).attr("title", "Marcar como lida");
        j(".notificacao-sino-" + id).attr("onclick", "marcar_lido(this); return false;");
        
        gerar_tooltipster();

        j.get(
            '/questoes/xhr/marcar_notificacao_como_lida_nao_lida/'+id+'/0',
            function(data){
                j('#qtd_notificacoes').html(data);
            }
        );
    }

    var naoLidasOffset = <?= LIMITE_NOTIFICACOES ?>;
    var todasOffset = <?= LIMITE_NOTIFICACOES ?>;
    function iniciar_scroll(total_nao_lidas, total){

        j('#notificacoes-nao-lidas').bind('scroll', function(){
            
            if(naoLidasOffset >= total_nao_lidas) return;

            if(Math.round(j(this).scrollTop() + j(this).innerHeight()) == j(this)[0].scrollHeight){
                //Carrega os dados iniciais das notificações
                j.ajax({
                    url: "/questoes/xhr/carregar_mais_notificacoes/" + naoLidasOffset + "/0" , 
                    type: "GET",
                    dataType: "HTML",
                    cache: false
                }).success(function(data){
                    naoLidasOffset += <?= LIMITE_NOTIFICACOES ?>;
                    j('#notificacoes-nao-lidas').append(data);
                    gerar_tooltipster();
                }).error(function (request, status, error) {
                    //alert(error);
                }).done(function(data){
                });
            }
        });

        j('#notificacoes-todas').bind('scroll', function(){

            if(todasOffset >= total) return;

            if(Math.round(j(this).scrollTop() + j(this).innerHeight()) == j(this)[0].scrollHeight){
                //Carrega os dados iniciais das notificações
                j.ajax({
                    url: "/questoes/xhr/carregar_mais_notificacoes/" + todasOffset , 
                    type: "GET",
                    dataType: "HTML",
                    cache: false
                }).success(function(data){
                    todasOffset += <?= LIMITE_NOTIFICACOES ?>;
                    j('#notificacoes-todas').append(data);
                    gerar_tooltipster();
                }).error(function (request, status, error) {
                    //alert(error);
                }).done(function(data){
                });
            }
        });

    }

</script>