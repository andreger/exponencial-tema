<div class="col-12 col-md-12 header-social- text-center text-md-right mt-4 pr-md-5 pr-3">
	<div class="mt-2">
		<a href="https://www.facebook.com/exponencialconcursos" class="t-d-none descontos-promocoes">
            <span class="comum-facebook d-inline-block align-bottom"></span>
		</a>
		<a href="https://www.instagram.com/exponencial_concursos" class="t-d-none descontos-promocoes">
            <span class="comum-instagram d-inline-block align-bottom"></span>
		</a>
		<a href="https://www.youtube.com/channel/UCr9rg5WOPmXvZgOfBl-HEuw" class="t-d-none descontos-promocoes">
            <span class="comum-youtube d-inline-block align-bottom"></span>
		</a>
	</div>
</div>