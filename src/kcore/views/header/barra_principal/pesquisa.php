<?php 
KLoader::helper('UrlHelper');
KLoader::helper('PesquisaHelper');
KLoader::helper("StringHelper");
?>

<div class="p-0 mt-2 mt-md-4 col-12 col-md-5 col-xl-6 header-pesquisa d-none d-sm-block">
	<form method="get" id="pesquisa-form" action="<?= UrlHelper::get_pesquisa_url() ?>">
		<!--<div class="d-none">-->
    		<div class="easy-autocomplete-2">
    			<input class="ipt-scroll mt-2 ml-4" type="text" id="busca" name="q" value="<?= StringHelper::tratar_aspas( ( isset($_GET['q']) ? $_GET['q'] : '' ) ) ?>"  placeholder="Qual curso você está procurando?">
    		</div>
    		
    		<a href="#" id="header-buscar">
    			<div class="header-pesquisa-lupa mt-2 ml-4">
    				<i class="fa fa-search mt-2 ml-2 text-white"></i>		
    			</div>
    		</a>
    		
    		<input type="hidden" id="pesquisa-pagina" value="<?= PesquisaHelper::get_form_url() ?>">
    		<input type="hidden" id="pesquisa-texto" name="q2" value="<?= isset($_GET['q2']) ? htmlspecialchars($_GET['q2']) : ''?>">
    		<input type="hidden" id="pesquisa-tipo" name="t" value="<?= isset($_GET['t']) ? htmlspecialchars($_GET['t']) : ''?>">
    		<input type="hidden" id="pesquisa-filtro" name="f" value="<?= isset($_GET['f']) ? htmlspecialchars($_GET['f']) : ''?>">
    		<input type="hidden" id="pesquisa-ordem" name="o" value="<?= isset($_GET['o']) ? htmlspecialchars($_GET['o']) : ''?>">
    		<input type="submit" id="pesquisa-busca" value="Buscar" style="display: none">
    	<!--</div>-->
	</form>
</div>