<?php if($total > 0): ?>

    <table border="0" cellspacing="0" cellpadding="0" class="mt-3 border-none table">

        <tr class="text-blue font-14">
            <th>Nome do Produto</th>
            <th class="d-none d-lg-table-cell">Pedido de Compra</th>
            <th class="d-none d-lg-table-cell">Status da Compra</th>
            <th class="d-none d-lg-table-cell">Data de Expiração</th>
            <th>Fórum Tira-Dúvidas</th>
        </tr>
        <?php
            foreach($pedidos as $pedido){
                KLoader::view("minha_conta/micon/micon_linha_pedido", ['pedido' => $pedido, 'user_id' => $user_id]);
            }
        ?>
    </table>

    <?= $paginator ?>

<?php else: ?>

    <div style="padding: 15px 0px 0px 15px;">Nenhum pedido a ser exibido.</div>

<?php endif; ?>