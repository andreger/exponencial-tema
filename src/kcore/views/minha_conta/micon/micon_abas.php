<?php 
    $aba_ativa = isset($_GET['a'])?$_GET['a']:'ativos';
?>

<div class="tab-v1 mt-5 w-100">
    <ul class="nav nav-tabs">
        <li class="<?= $aba_ativa == MICON_ABA_ATIVOS?'active':'' ?>"><a href="<?= $aba_ativa == MICON_ABA_ATIVOS ? '#ativos': '/minha-conta?a='.MICON_ABA_ATIVOS ?>" >Ativos</a></li>
        <li class="<?= $aba_ativa == MICON_ABA_EXPIRADOS?'active':'' ?>"><a href="<?= $aba_ativa == MICON_ABA_EXPIRADOS ? '#expirados': '/minha-conta?a='.MICON_ABA_EXPIRADOS ?>" >Expirados</a></li>
        <li class="<?= $aba_ativa == MICON_ABA_CANCELADOS?'active':'' ?>"><a href="<?= $aba_ativa == MICON_ABA_CANCELADOS ? '#cancelados': '/minha-conta?a='.MICON_ABA_CANCELADOS ?>" >Cancelados</a></li>
    </ul>
    <div class="tab-content">
        
        <!-- Chamar Ativos -->
        <?php KLoader::view("minha_conta/micon/micon_aba", ['user_id' => $user_id, 'aba_ativa' => $aba_ativa, 'aba' => MICON_ABA_ATIVOS, 'tipo_busca' => MEUS_PEDIDOS_ATIVOS]) ?>

        <!-- Chamar Exprados -->
        <?php KLoader::view("minha_conta/micon/micon_aba", ['user_id' => $user_id, 'aba_ativa' => $aba_ativa, 'aba' => MICON_ABA_EXPIRADOS, 'tipo_busca' => MEUS_PEDIDOS_EXPIRADOS]) ?>
        
        <!-- Chamar Cancelados -->
        <?php KLoader::view("minha_conta/micon/micon_aba", ['user_id' => $user_id, 'aba_ativa' => $aba_ativa, 'aba' => MICON_ABA_CANCELADOS, 'tipo_busca' => MEUS_PEDIDOS_CANCELADOS]) ?>

    </div>
</div>