<?php
    if(is_produto_assinatura($pedido->product_id)) {
        $detalhe_url = '/questoes/main/resolver_questoes';
    }
    else {
        $detalhe_url = is_produto_indisponivel($pedido->product_id, $pedido->order_id) ? '#' : '/minha-conta-detalhe?o_id=' . $pedido->order_id . '&p_id=' . $pedido->product_id;
    }

    try {
        $order = new WC_Order($pedido->order_id);
        $produto = wc_get_product($pedido->product_id);
    }
    catch (Exception $e) {
        $produto = null;
    }

    //Verifica se o pedido tem algum estado relacionado a assinatura recorrente
    $status_recorrente = null;
    if(is_pedido_recorrente_suspenso($order->id)) {
        $status_recorrente = 'Suspenso';
    }
    elseif(is_pedido_recorrente_cancelado($order->id)) {
        $status_recorrente = 'Cancelado';
    }
    elseif(is_pedido_recorrente_expirado($order->id)) {
        $status_recorrente = 'Expirado';
    }
    elseif(is_pedido_recorrente_pendente($order->id)) {
        $status_recorrente = 'Pendente';
    }

    // todo: remover
    //$status_recorrente = hack_is_pedido_recorrente_cancelado($order->id) ? "Cancelado" : null;
?>

<?php if($produto) : ?>
    <tr class="tabela-pedidos-tr-bg-azul">
        <td class="text-detalhe">	                        
            <div>
                <b>
                    <?php if(is_pedido_produto_disponivel($order->id, $pedido->product_id) && !$status_recorrente) : ?>
                        <a class="link-produto" href="<?= $detalhe_url ?>">
                        
                        <?= $produto->get_title() ?></a>
                    <?php else : ?>
                        <?= $produto->get_title() ?>
                    <?php endif ?>
                </b>
            </div>
        </td>
        <td class="d-none d-lg-table-cell">Pedido #<?= $order->id;?></td>
        <td class="d-none d-lg-table-cell">
        <?php
        if(is_produto_estornado($order->id, $pedido->product_id, TRUE)) {
            $status = 'Estornado';
        }
        elseif($status_recorrente) {
            $status = $status_recorrente;
        }
        else {
            $status = $order->status;
            switch ($order->status) {
                case 'completed' :
                    $status = 'Concluído';
                    break;
                case 'refunded' ://Já foi visto acima que não foi revogado logo mantem concluído
                    $status = 'Concluído';
                    break;
                case 'on-hold' :
                case 'pending' :
                    $status = 'Pendente';
                    break;
                case 'cancelled' :
                    $status = 'Cancelado';
                    break;
                case 'processing' :
                    $status = 'Processando';
                    break;
            }
        }
    
        echo $status;
        ?>		
        <?php if(($order->status == 'pending') && (date('d/m/Y', strtotime($order->date_created)) < date("d/m/Y", strtotime("-30 days")))): ?>			
        <!--<form id="<?= $order->id;?>" action="/minha-conta" method="post">
            <input type="hidden" name="PedidoId" value="<?= $order->id;?>">
            <input class="btn btn-sm btn-danger text-white" type="submit" value="Cancelar" name="Cancelar" data-toggle="tooltip" title="Não se preocupe com um pedido com status pendente: não haverá qualquer tipo de cobrança! Mas se isto incomodar você, pressione o botão e o cancele. E se tiver alguma dúvida, entre em contato conosco. https://www.exponencialconcursos.com.br/fale-conosco">
        </form>-->
        <?php endif; ?>
        </td>
        <td class="d-none d-lg-table-cell">
        <?php 
        if($order->status == 'completed' || ($order->status == 'refunded' && !is_produto_estornado($order->id, $pedido->product_id, TRUE))) {
            
            if(is_produto_indisponivel($pedido->product_id, $pedido->order_id) ) {
                echo "Acesso expirado";
            }
            else {
                echo get_data_disponivel_ate($pedido->product_id, $user_id, $order->id);
            }
        }
        ?>
        </td>
    
        <td class="text-forum">
            <b>
            <?php
            if ($order->status == 'completed' || $order->status == 'refunded') {
                $product_id = $pedido->product_id;
                
                if(is_produto_estornado($order->id, $pedido->product_id, TRUE)) {
                    echo "Acesso revogado";
                }
                // forum de cursos antigos
                //elseif (is_pacote_antigo ( $item )) {
                elseif(strpos($pedido->order_item_name, 'Pacote') !== false){
                    $forums_array = array ();
                    $products_ids = get_produtos_ids_de_pacote_antigo ( $product_id );
                    
                    foreach ( $products_ids as $product_id ) {
                        $forum_id = get_post_meta ( $product_id, 'forum_id', true );
                        bbp_add_user_forum_subscription ( $user_ID, $forum_id ); // Adiciona permissões para o usuário acessar o fórum
                        array_push ( $forums_array, get_forum_link_from_product_id ( $product_id ) );
                    }
                    
                    echo implode ( "<br>", $forums_array ) ?  : 'Não possui fórum!';
                } 			// forum de cursos novos
                else {
                    $forum_id = get_post_meta ( $product_id, 'forum_id', true );
                    
                    bbp_add_user_forum_subscription ( $user_id, $forum_id ); // Adiciona permissões para o usuário acessar o fórum
                    
                    echo get_forum_link_from_product_id ( $product_id ) ?  : 'Não possui fórum!';
                }
            } else if ($order->status == 'cancelled') {
                echo "Pedido cancelado!";
            } else {
                echo "Pedido ainda em processo!";
            }
            ?>
            </b>
        </td>
	</tr>
<?php endif ?>