<div class="carregando-questoes col-6" id="carregando-pedidos">
    <div class='text-center'>
        <div style="padding: 10px"><img alt="Carregando..." width="40" src='/wp-content/themes/academy/images/carregando.gif'></div>
        <div style="padding: 10px">Aguarde só mais um momento...</div>
        <div style="padding: 10px">Estamos atualizando nosso banco de dados!</div>
    </div>
</div>
<script>
    setTimeout(function(){
        window.location.replace("/minha-conta?mig=1");
    }, 2000);
</script>