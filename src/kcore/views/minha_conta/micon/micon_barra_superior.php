<div class="col-12 text-center text-md-left text-blue">		
    <h1>Área do aluno</h1>
</div>
<div class="mt-3 minha-conta-botoes col-12 pl-3 pl-md-0">
    <a class="t-d-none" href="/questoes/perfil">
        <?= get_tema_image_tag('botao-meuperfil.png') ?>
    </a>
    <a class="t-d-none" href="/forum">
        <?= get_tema_image_tag('botao-forum.png') ?>
    </a>
    <?php if(is_administrador()) : ?>
    <a class="t-d-none" href="/painel-coaching/relatorios/inscricoes">
        <?= get_tema_image_tag('botao-painelcoaching.png') ?>
    </a>

    <?php elseif (!is_professor() || !is_consultor()) : ?>
    <a class="t-d-none" href="/painel-coaching/inscricao">
        <?= get_tema_image_tag('botao-painelcoaching.png') ?>
    </a>
    <?php endif; ?>
    
    <a class="t-d-none img-resolver" href="/questoes/main/resolver_questoes">
        <?= get_tema_image_tag('botao-sq.png') ?>
    </a>
    
</div>