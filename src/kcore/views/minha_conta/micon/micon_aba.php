<?php $is_aba_ativa = $aba_ativa == $aba; ?>
<div class="tab-pane fade in <?= $is_aba_ativa?'show active':'' ?>" id="<?= $aba ?>">
    <div id="id_<?= $aba ?>" class="row">
        <?php if($is_aba_ativa): ?>
            <div class='col-12 text-center'>
                <div style="padding: 10px"><img alt="Carregando..." width="40" src='/wp-content/themes/academy/images/carregando.gif'></div>
                <div style="padding: 10px">Carregando pedidos...</div>
            </div>
        <?php endif; ?>
    </div>
</div>
<?php if($is_aba_ativa): ?>

    <?php $pg = (isset($_GET['pg']) && $aba_ativa == $aba) ? $_GET['pg'] : "1"; ?>

    <script>
        
        jQuery(function(){

            jQuery.post("/kcore/xhr/micon_aba_ativa",
                {
                    user_id: '<?= $user_id ?>',
                    pg: '<?= $pg ?>',
                    aba: '<?= $aba ?>',
                    tipo_busca: '<?= $tipo_busca ?>',
                },
                function(data){
                    jQuery("#id_<?= $aba ?>").html(data);
                }
            );
        
        });

    </script>
    
<?php endif; ?>