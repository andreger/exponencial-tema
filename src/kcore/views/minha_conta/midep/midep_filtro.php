<?php
KLoader::model("CategoriaModel");
KLoader::model("ConcursoModel");
KLoader::model("ProfessorModel");
KLoader::helper("UiSelectHelper");

// carregamento das combos
$professores_combo = UiSelectHelper::get_combo_options(
    ProfessorModel::listar_por_tipo([COLABORADOR_PROFESSOR, COLABORADOR_PROFESSOR_CONSULTOR]), "ID", "display_name", false);
$disciplinas_combo = UiSelectHelper::get_combo_options(
    listar_todas_disciplinas(), "term_id", "name", false);
$concursos_combo = UiSelectHelper::get_combo_options(
    ConcursoModel::listar_todos(), "ID", "post_title", false);
$ordenacao_combo = [PESQUISA_ORDEM_A_Z => "A-Z", PESQUISA_ORDEM_Z_A => "Z-A", PESQUISA_MAIS_NOVOS => "Mais Novos"];
?>

<form method="post" action="<?= UrlHelper::get_minha_conta_detalhe_url($pedido_id, $produto_id) ?>" style="width: 100%">
	<div class="row">
        <div class="col-xs-12 col-md-6 col-lg-4 mb-2">
        	<strong>Concurso:</strong>
        	<?= UiSelectHelper::get_select_html("concursos_selecionados[]", $concursos_combo, $filtros["concursos"], true, ["class" => "filtro", "style" => "width: 100%"]) ?>
        </div>
        
        <div class="col-xs-12 col-md-6 col-lg-4 mb-2">
        	<strong>Matéria:</strong>
        	<?= UiSelectHelper::get_select_html("materias_selecionadas[]", $disciplinas_combo, $filtros["materias"], true, ["class" => "filtro", "style" => "width: 100%"]) ?>
        </div>		
        <div class="col-xs-12 col-md-6 col-lg-4 mb-2">
            <strong>Professor:</strong>
            <?= UiSelectHelper::get_select_html("professores_selecionados[]", $professores_combo, $filtros["professores"], true, ["class" => "filtro", "style" => "width: 100%"]) ?>
        </div>
 
        <div class="col-xs-12 col-md-6 col-lg-8 mb-2">
        	<strong>Buscar pelo texto:</strong>
        	<input type="text" style="width:100%; height: 36px; border: 1px solid #aaa; padding-left:10px" name="texto" value="<?= $filtros["texto"] ?>">
        </div>
        
         <div class="col-lg-4 col-xs-12 col-md-6 mb-2">
        	<strong>Ordenação:</strong>
        	<?= UiSelectHelper::get_select_html("ordem", $ordenacao_combo, $filtros["ordem"], false, ["class" => "filtro", "style" => "width: 100%;"]) ?>
        </div>
    </div>
    
    <!-- <div class="row">
     	<div class="col-12 mb-2">
    		<input type="checkbox" id="fixar-chk" name="fixar" value="1" <?= $filtros["fixar"] ? " checked=checked" : "" ?>> <label for="fixar-chk">Fixar no topo os cursos com acesso requerido</label>
    	</div>
    </div> -->
    
    <div class="row mb-3">
        <div class="col-8 mb-2">
			<a class="pointer" id="midep-filtro-linha"><img width="50" src="<?= get_tema_image_url('view-linha.png') ?>"></a>
			<a class="pointer" id="midep-filtro-grade"><img  width="50" src="<?= get_tema_image_url('view-grid.png') ?>"></a>
        	<input type="hidden" name="filtrar" value="1">
        	<button type="submit" name="filtrar_btn" value="1" class="btn u-btn-blue">Filtrar</button>
            <a target="_blank" href="https://www.youtube.com/watch?v=rjGfS5ahAIU&t=2s" class="btn u-btn-primary"><i class="fa fa-question-circle"></i></a>

        </div>
    </div>
</form>