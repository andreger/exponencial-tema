<?php if($items) : ?>
    <div class="row mt-3">
		<div class="col-sm-4 text-left">
			Cursos localizados: <strong><?= $total ?></strong>
		</div>

		<div class="col-sm-8 text-right">
    	   <?= $paginator ?>
    	</div>
	</div>

	<div class="row mt-3">
    	<?php foreach ($items as $item) :  
    	   $acesso_foi_requerido = in_array($item->ID, $acessos_requeridos);
    	   
    	   
    	   if(in_array($item->ID, $fixados)) {
    	       $fixado_estilo = "btn-warning text-white";
    	       $fixado_url = "&desfixar={$item->ID}";
    	   }
    	   else {
    	       $fixado_estilo = "btn-light";
    	       $fixado_url = "&fixar={$item->ID}";
    	   }
    	   
    	   $fixado_url = "/minha-conta-detalhe?o_id={$pedido_id}&p_id={$produto_id}" . $fixado_url;
    	   
    	   if($acesso_foi_requerido) {
    	       $log = PremiumModel::get_log($pedido_id, $produto_id, $item->ID);
    	       $url = UrlHelper::get_minha_conta_detalhe_url($log->pedido_oculto_id, $item->ID);
    	       $estilo = "u-btn-primary";
    	       $label = "Acessar";
    	   }
    	   else {
    	       $url = UrlHelper::get_requerer_acesso_item_premium_url($item->ID, $pedido_id, $produto_id);
    	       $estilo = "u-btn-blue pp-req-acesso";
    	       $label = "Requerer Acesso";
    	   }
    	?>
    	
    	<div class="col-12 mt-1 mb-1 bg-box-azul-light midep-cursos-linha " style="display: none">
			<div class="row pt-2 pb-2">
				<div class="col-xs-12 col-md-9 col-lg-9">
					<div class="text-blue font-14 font-weight-bold ml-3"><?= $item->post_title ?></div>
				</div>
				<div class="col-xs-12 col-md-3 col-lg-3 text-right">
					<div class="pp-req-div"> 
        				<a class="mt-1 mt-md-0 btn <?= $estilo ?>" href="<?= $url ?>"><?= $label ?></a>
        				<span class="pp-req-carregando" style="display:none"><img style="margin: 3px" width="30" src='/wp-content/themes/academy/images/carregando.gif'>&nbsp;Aguarde...</span>
        				<a href="<?= $fixado_url ?>" class="btn <?= $fixado_estilo ?>"><i class="fa fa-thumb-tack"></i></a>
						
        			</div>
				</div>
			</div>
		</div>
		
		<div class="col-sm-12 col-md-6 col-lg-4 mb-3 midep-cursos-grade" style="display: none">
			<div class="col-md-12 pt-2 pb-2  bg-box-azul-light">
				<div class="midep-grid-txt">
					<div class="text-blue font-14 font-weight-bold ml-3"><?= $item->post_title ?></div>
				</div>
				<div class="">
					<div class="text-center pp-req-div"> 
        				<a class="mt-1 mt-md-0 btn <?= $estilo ?>" href="<?= $url ?>"><?= $label ?></a>
        				<span class="pp-req-carregando" style="display:none"><img style="margin: 3px" width="30" src='/wp-content/themes/academy/images/carregando.gif'>&nbsp;Aguarde...</span>
        				<a class="btn btn-light"><i class="fa fa-thumb-tack"></i></a>
        			</div>
				</div>
			</div>
		</div>	
    	<?php endforeach; ?>	
    </div>

	<div class="row mt-3 mb-5 text-right w-100">
    	<?= $paginator ?>
	</div>
<?php else : ?>
	<div class="row text-center w-100 mt-5 mb-5">
		<div class="col-12">
			Não localizamos curso com os critérios selecionados, se tiver
			dúvida, entre em contato pelo&nbsp;<a href="/fale-conosco">Fale
				Conosco</a>
		</div>
	</div>
<?php endif ?>
