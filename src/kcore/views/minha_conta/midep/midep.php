<?php
redirecionar_se_nao_estiver_logado();

session_start();

KLoader::css("minha_conta");

get_header();

if (! isset($_GET['p_id']))
    redirecionar_para_acesso_negado();

KLoader::model("ProdutoModel");
KLoader::model("PremiumModel");

KLoader::helper("UrlHelper");
KLoader::helper("UiSelectHelper");



$produto_id = $_GET['p_id'];
$pedido_id = $_GET['o_id'];

$user_ID = get_current_user_id();

$order = new WC_Order ( $pedido_id );

// todo: remover
//if(hack_is_pedido_recorrente_cancelado($pedido_id)) {
//    redirecionar_para_acesso_negado();
//}

if($order->get_user_id() != get_current_user_id() ) {
    redirecionar_para_acesso_negado();
}

if($order->status != 'completed') {
    redirecionar_para_acesso_negado();
}

if(is_pedido_recorrente_suspenso($pedido_id) ) {
    redirecionar_para_acesso_negado();
}

if(is_pedido_recorrente_cancelado($pedido_id) ) {
    redirecionar_para_acesso_negado();
}

if(is_pedido_recorrente_pendente($pedido_id) ) {
    redirecionar_para_acesso_negado();
}

if(is_pedido_recorrente_expirado($pedido_id) ) {
    redirecionar_para_acesso_negado();
}

if($fixar_id = $_GET['fixar']) {
    PremiumModel::desfixar_item($pedido_id, $produto_id, $fixar_id);
    PremiumModel::fixar_item($pedido_id, $produto_id, $fixar_id);
}

if($desfixar_id = $_GET['desfixar']) {
    PremiumModel::desfixar_item($pedido_id, $produto_id, $desfixar_id);
}


// executa requisição de acesso
if (isset($_GET['i_id'])) {
    $item_id = $_GET['i_id'];

    // somente cria o pedido oculto se a requisição não tiver sido solicitada
    if (! PremiumModel::get_log($pedido_id, $produto_id, $item_id)) {
        $usuario = get_usuario_array();

        $address = array(
            'first_name' => $usuario["nome"],
            'last_name' => $usuario["sobrenome"],
            'email' => $usuario["email"],
            'phone' => $usuario["telefone"],
            'address_1' => $usuario["endereco"],
            'address_2' => $usuario["complemento"],
            'city' => $usuario["cidade"],
            'state' => $usuario["uf"],
            'postcode' => $usuario["cep"],
            'country' => "Brasil"
        );

        // Cria um novo pedido
        $order = wc_create_order([
            'customer_id' => $usuario["id"]
        ]);
        
        // Salva o log da requisição
        PremiumModel::salvar_log($pedido_id, $produto_id, $item_id, $order->get_id());
        
        // adiciona item requisitado ao novo pedido
        $order->add_product(get_product($item_id), 1);
        
        // preenche dados obrigatórios
        $order->set_address($address, 'billing');
        
        // cria observação no novo pedido (item requisitado)
        $obs = "Item do Premium. Produto: {$produto_id}, Pedido: {$pedido_id}";
        
        // completa o novo pedido
        $order->update_status("completed", $obs, TRUE);
        
        // adiciona desconto de 100% no item requisitado do novo pedido
        wc_order_add_discount($order->get_id(), $obs, '100%');
        
        PremiumModel::fixar_item($pedido_id, $produto_id, $item_id);
    }
}

// recupera dados do premium
$post = get_post($produto_id);

// Inicializa a sessão
$sessao_index = "form_minha_conta_pp_{$pedido_id}_{$produto_id}";
if (! $_SESSION[$sessao_index]) {
    $_SESSION[$sessao_index] = [];
}
// Alimenta a sessão
if (isset($_POST['filtrar'])) {

    $_SESSION[$sessao_index] = [
        'concursos' => $_POST['concursos_selecionados'] ?: null,
        'materias' => $_POST['materias_selecionadas'] ?: null,
        'professores' => $_POST['professores_selecionados'] ?: null,
        'pacotes' => $_POST['pacotes_selecionados'] ?: null,
        'tipos' => $_POST['tipos_selecionados'] ?: null,
        'texto' => $_POST['texto'] ?: null,
        'ordem' => $_POST['ordem'] ?: PESQUISA_ORDEM_A_Z,
//         'fixar' => $_POST['fixar'] ?: null
    ];
    
//     $fixar_valor = isset($_POST['fixar']) && $_POST['fixar'] ? YES : NO;
//     PremiumModel::set_fixar_requeridos_config($pedido_id, $produto_id, $fixar_valor);
}

$pg = isset($_GET['pg']) ? $_GET['pg'] : "1";

?>

<div class="container">
	<div class="row pt-1 pt-md-5">
		<a class="t-d-none text-dark font-14 font-weight-bold"
			href="/minha-conta"><?= get_tema_image_tag('voltar-pqn.png', 24, 24) ?> Voltar para lista de produtos</a>
	</div>

	<div class="row mt-3">
		<div class="col-12 text-center">
			<div class="text-darkblue font-15 font-weight-bold">
				<?= $post->post_title; ?>					
			</div>
			<div class="text-darkblue font-14">
				<?= get_autores_str($post)?>					
			</div>
		</div>
	</div>
	
	<div class="row mt-3">
		<div class="col-12">
			<div class="row mt-4">
				<div class="col-md-9"></div>
			</div>
		</div>
	</div>
	
	<div id="midep-loading" class='col-12 text-center'>
			<div style="padding: 10px"><img alt="Carregando..." width="40" src='/wp-content/themes/academy/images/carregando.gif'></div>
			<div style="padding: 10px">Carregando itens...</div>
		</div>
	</div>	

<link href="/questoes/assets-admin/js/plugins/select2/select2.css"
	rel="stylesheet">
<link
	href="/questoes/assets-admin/js/plugins/select2/select2-bootstrap.css"
	rel="stylesheet">
<script src="/questoes/assets-admin/js/plugins/select2/select2.js"></script>

<script>
// Exibição: Linha = 1 e Grade = 2	
function exibir_linha() {
	jQuery(".midep-cursos-linha").show();
	jQuery(".midep-cursos-grade").hide();
	localStorage.setItem('midep.exibicao', 1);
}

function exibir_grade() {
	jQuery(".midep-cursos-linha").hide();
	jQuery(".midep-cursos-grade").show();
	localStorage.setItem('midep.exibicao', 2);
}

jQuery(document).ready(function(){

	jQuery.post("/kcore/xhr/midep_conteudo",
		{
			pg: '<?= $pg ?>',
			produto_id: '<?= $produto_id ?>',
			pedido_id: '<?= $pedido_id ?>',
		},
		function(data){
			
			jQuery("#midep-loading").replaceWith(data);

			jQuery(".filtro").select2();

			jQuery(".pp-req-acesso").click(function() {
				jQuery(this).hide();
				jQuery(this).next().show();
			});

			jQuery("#midep-filtro-linha").click(function () {
				exibir_linha();
			});

			jQuery("#midep-filtro-grade").click(function () {
				exibir_grade();
			});

			if(localStorage.getItem('midep.exibicao') == 2) {
				exibir_grade();
			}
			else {
				exibir_linha();
			}

		}
	);

});
</script>
<?php 
	//B2700 Removido 21/10/2019
	/*if($order->get_meta('is_show_google_merchant')){
		KLoader::view("plugins/google_merchant/google_merchant_opt_in", 
			[
				'order_id' => $order->get_id(), 
				'user_email' => $order->get_billing_email(),
				'user_country' => $order->get_billing_country(),
				'order_date' => $order->get_date_created()->date('Y-m-d')
			]
		);
		$order->delete_meta_data('is_show_google_merchant');
		$order->save();
	}*/
?>
<?php get_footer(); ?>