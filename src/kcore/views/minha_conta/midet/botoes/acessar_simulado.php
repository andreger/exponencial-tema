<?php if(is_produto_simulado($produto_id)) : ?>
<?php 
    $simulado = SimuladoModel::get_simulado_by_produto($produto_id);
    $simulado_sufixo = $simulado ? "?p={$produto_id}" : ""; 
?>
<span class="ml-5">
    <a href="/questoes/simulados/meus<?= $simulado_sufixo ?>" target="_blank">
        <img src="<?= get_tema_image_url('acessar-simulados3.png',150,67)?>" alt="Acessar o Simulado">
    </a>
</span>
<?php endif ?>