<?php if(is_produto_caderno($produto_id)) : ?>

<?php $produto_caderno = get_produto_caderno_usuario($produto_id, get_current_user_id()) ?>

<?php 
    if(!$produto_caderno) {
        criar_caderno($produto_id, $pedido_id, TRUE, FALSE);

        $produto_caderno = get_produto_caderno_usuario($produto_id, get_current_user_id());
    }

?>

<span class="ml-5">
    <a href="/questoes/main/resolver_caderno/<?= $produto_caderno->cad_id ?>" target="_blank">
        <img class="minha-conta-img" src="<?= get_tema_image_url('acessar-caderno.png',150,67)?>" alt="Acessar o Caderno">
    </a>
</span>
<?php endif ?>