<?php if(is_produto_assinatura($produto_id)) : ?>
<div class="text-center w-100">
    <span>
        <a class="btn u-btn-primary" href="/questoes/main/resolver_questoes" target="_blank">
            Acessar Sistema de Questões
        </a>
    </span>
</div> 
<?php endif ?>