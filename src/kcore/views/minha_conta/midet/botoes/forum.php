<?php 
$forum_url = get_forum_url_from_product_id($produto_id);

if($forum_url) : ?>
<span class="ml-5 minha-conta-forum" style="vertical-align: top">
    <a class="t-d-none font-12" href="<?= $forum_url ?>" id="forum-btn">
        <span class="mobile-hide"><?= get_tema_image_tag('btn-forum.png', 20, 20) ?></span> Fórum Tira-Dúvidas
    </a>
</span>
<?php endif ?>