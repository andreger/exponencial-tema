<?php
redirecionar_se_nao_estiver_logado();

get_header ();

if(!isset($_GET['p_id'])) redirecionar_para_acesso_negado();

KLoader::model("PedidoModel");
KLoader::model("SimuladoModel");
KLoader::helper("PedidoHelper");
KLoader::helper("ProdutoHelper");

$produto_id = $_GET['p_id'];
$pedido_id = $_GET['o_id'];

global $post;
$post = new WC_Product($produto_id);
$order = new WC_Order ( $pedido_id );

if($order->get_user_id() != get_current_user_id() ) {
    redirecionar_para_acesso_negado();
}

if($order->status != 'completed') {
    redirecionar_para_acesso_negado();
}

if(is_pedido_recorrente_suspenso($pedido_id) ) {
    redirecionar_para_acesso_negado();
}

if(is_pedido_recorrente_cancelado($pedido_id) ) {
    redirecionar_para_acesso_negado();
}

if(is_pedido_recorrente_pendente($pedido_id) ) {
    redirecionar_para_acesso_negado();
}

if(is_pedido_recorrente_expirado($pedido_id) ) {
    redirecionar_para_acesso_negado();
}


////Só vira "refunded" quando o valor estornado é total
if(!(( $order->status == 'completed' || $order->status == 'refunded' ) && !is_produto_estornado($pedido_id, $produto_id, TRUE))){
    redirecionar_para_acesso_negado();
}

$user_ID = get_current_user_id ();

if(is_produto_indisponivel($produto_id, $pedido_id)) {
    echo "Acesso expirado";
    exit;
}

$post = get_post($produto_id);
setup_postdata( $post );
	
// $arquivos = PedidoModel::listar_arquivos_aulas_disponiveis_agrupados_por_aula($produto_id, $pedido_id);
// $links_zip = PedidoHelper::nivelar_arquivos_aulas_disponiveis($arquivos);
?>

<div class="container">
	<div class="row pt-1 pt-md-5">
		<a class="t-d-none text-dark font-14 font-weight-bold" href="/minha-conta"><?= get_tema_image_tag('voltar-pqn.png', 24, 24) ?> Voltar para lista de produtos</a>
	</div>

	<div class="row mt-3" id="minha-conta">	
		<div class="col-12 text-center">
			<div class="text-darkblue font-15 font-weight-bold">
				<?= $post->post_title ?>					
			</div>
			<div class="text-darkblue font-14">
				<?= get_autores_str($post)?>					
			</div>
		</div>
		<div class="col-12">
			<div class="row mt-4">
			
				<div class="col-md-3 mtproduct-2">
				</div>
				
				<div class="col-md-6 mt-2">

				<?php KLoader::view("minha_conta/midet/botoes/forum", ['produto_id' => $produto_id]) ?>
			
				<?php KLoader::view("minha_conta/midet/botoes/acessar_simulado", ['produto_id' => $produto_id]) ?>
				
				<?php KLoader::view("minha_conta/midet/botoes/acessar_sq", ['produto_id' => $produto_id]) ?>
				
				<?php KLoader::view("minha_conta/midet/botoes/acessar_caderno", ['produto_id' => $produto_id, 'pedido_id' => $pedido_id]) ?>
				
				</div>
				
				
				<div class="col-md-3 text-right mt-2">
					<?php if(ProdutoModel::tem_tipo_aula_disponivel($produto_id, AULA_UPLOAD_TIPO_PDF)): ?>
					<span class="botao-baixar-tudo d-none">
						<?php KLoader::view("minha_conta/midet/botoes/baixar_tudo", 
							['arquivos' => $arquivos, 'links_zip' => $links_zip, 'produto_id' => $produto_id, 'pedido_id' => $pedido_id]) ?>
					</span>
					<?php endif; ?>
				</div>
			</div>
		</div>
		
		<div class="col-12">
			<?php 
				$product = new WC_Product($produto_id);
				KLoader::view("produto/cronograma/cronograma", ["product" => $product, "is_conta_usuario" => TRUE]); 
			?>
		</div>
	</div>
</div>
<div class="sectionlargegap"></div>
</div>

<?= vimeo_player_assets() ?>

<script>
jQuery(function() {

	jQuery('.btn-baixar').click(function() {
		var $produto_id = jQuery(this).data('produto');
		var $nome = jQuery(this).data('nome');
		
		jQuery.post('/xhr-remover-atualizacao-aula', {
			produto_id: $produto_id,
			aula_nome: $nome
		});
	});

	/*jQuery('.cronograma-botao').on('click', function(e) {
		e.preventDefault();

		var i = jQuery(this).data("i");
		var p = jQuery(this).data("p");
		var t = jQuery(this).data("t");

		if(jQuery("#cronograma-detalhe-"+i+"-"+t).is(':visible')) {
			jQuery(".cronograma-detalhe-"+i).hide();
		}
		else {
			jQuery(".cronograma-detalhe-"+i).hide();
			jQuery("#cronograma-detalhe-"+i+"-"+t+" td").html("Carregando...");;
			jQuery("#cronograma-detalhe-"+i+"-"+t).show();

			jQuery.post("<?= TEMA_AJAX_PATH ?>listar_arquivos_aula.php", {'i': i, 'p': p, 't': t}, function(data) {
				jQuery("#cronograma-detalhe-"+i+"-"+t+" td").html(data);
			});
		}
	});*/

	jQuery(function () {
		
		jQuery(document).on('click', '.cronograma-botao', function(e) {
			e.preventDefault();

			var i = jQuery(this).data("i");
			var p = jQuery(this).data("p");
			var t = jQuery(this).data("t");
			var d = jQuery(this).data("d");

			if(jQuery("#cronograma-detalhe-"+p+"-"+i+"-"+t).is(':visible')) {
				jQuery(".cronograma-detalhe-"+p+"-"+i).hide();
			}
			else {
				jQuery(".cronograma-detalhe-"+p+"-"+i).hide();
				jQuery("#cronograma-detalhe-"+p+"-"+i+"-"+t).html("<div class='cronograma-detalhe-interno'>Carregando...</div>");
				jQuery("#cronograma-detalhe-"+p+"-"+i+"-"+t).show();

				jQuery.post("<?= TEMA_AJAX_PATH ?>listar_arquivos_aula.php", {'i': i, 'p': p, 't': t, 'd': d}, function(data) {
					jQuery("#cronograma-detalhe-"+p+"-"+i+"-"+t).html(data);
				});
			}
		});
	});
});
</script>
<?php 
	//B2700 Removido 21/10/2019
	/*if($order->get_meta('is_show_google_merchant')){
		KLoader::view("plugins/google_merchant/google_merchant_opt_in", 
			[
				'order_id' => $order->get_id(), 
				'user_email' => $order->get_billing_email(),
				'user_country' => $order->get_billing_country(),
				'order_date' => $order->get_date_created()->date('Y-m-d')
			]
		);
		$order->delete_meta_data('is_show_google_merchant');
		$order->save();
	}*/
?>
<?php get_footer(); ?>