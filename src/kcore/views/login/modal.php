<?php 
    KLoader::helper("UrlHelper");
    $is_sq = UrlHelper::is_url_sistema_questoes();

    $dataRedirect = isset($ref_url) && $ref_url ? "data-redirect='$ref_url'" : '';
?>
    <script src="https://www.google.com/recaptcha/api.js"></script>
<div id="login-modal" class="modal fade in" aria-hidden="true" <?= $dataRedirect ?>>
    <div class="modal-dialog modal-dialog-centered rounded">
        <div id="login-modal-panel" class="modal-content login-modal">
            <div class="modal-header altura-modal-entrar">
                <button type="button" class="close botao-fexar" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form id="frm-login-modal" action='/wp-login.php?redirect_to=<?php echo urlencode($ref_url) ?>' method='post'>
                    <div class="login-error-msg" style="display: none;">
                        <label class="font-10 font-weight-bold text-danger mt-0">Por favor, informe seu usuário/senha corretamente.</label>
                    </div>
                    <div class="login-loading-msg" style="display: none;">
                        <label class="font-10 font-weight-bold text-secondary mt-0">Enviando...</label>
                    </div>
                    <div class="row-login-a">
                        <div class="mr-2">
                            <input class="rounded form-control" type="text" name="log" placeholder="E-mail" />
                        </div>
                        <div class="mr-2">
                            <input class='rounded login-w form-control' type='password' name='pwd' placeholder='Senha' />

                        </div>                    
                        <div class="">
                            <a id="login-modal-entrar" class='btn btn-primary' href='#' onclick="ajax_login_submit(jQuery('#frm-login-modal'));">
                                Entrar
                            </a>
                        </div>
                    </div>

                    <div class='row-login-b'> 
                        <div class='login-esqueceu text-left'>
                                <a class='text-dark_ font-10_' href='/recuperacao-de-senha'>
                                Esqueceu a senha?
                        </a>
                        </div>
                        <?php if(!$is_sq): ?>
                            <div></div>
                        <?php endif; ?>                     
                        <div class="m-face text-right">
                            <a class='btn u-btn-facebook <?= $is_sq?'btn-sm':'' ?> font-arial' href='/questoes/login/facebook'>
                                <i class='fa fa-facebook'></i> Entrar</a>
                        </div>
                    </div>
                </form>
            </div>
            <div class='modal-footer text-center'>
                <div class="long-cg w-100">
                    <a id="login-modal-cadastrar" class="btn btn-primary" href="#">Cadastro GRATUITO</a>
                </div>
            </div>
        </div>
       
            <div id="cadastro-modal-panel" class="modal-content" style="display: none;">
                <div class="modal-header altura-modal-entrar">
                    <button type="button" class="close botao-fexar" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <?php if(!$is_sq): ?>
                            <?php KLoader::view('cadastro_login/cadastro_login', ['register_url' => $register_url, 'is_ajax' => TRUE]) ?>
                        <?php else: ?>
                            <?php KLoader::view('cadastro_login/sq/cadastro_login', ['register_url' => $register_url, 'is_ajax' => TRUE]) ?>
                        <?php endif; ?>
                    </div>
                </div>
                <div class='modal-footer text-center'>
                    <div class="long-cg">
                        <a id="login-modal-voltar" class="btn btn-primary" href="#">Voltar</a>
                    </div>
                </div>
            </div>
    </div>
</div>

<script>
     jQuery('#login-modal').on('show.bs.modal', function(e){
        var redirect = jQuery('#login-modal').data('redirect');
        if(redirect){
            jQuery('#frm-login-modal').attr('action', '/wp-login.php?redirect_to=' + redirect);
            jQuery('#register-form').attr('action', '/cadastro-login?is_ajax=1&ref=' + redirect);
            jQuery('#cadastro-login-form').attr('action', '/wp-login.php?redirect_to=' + redirect);

            jQuery('#frm-login-modal').attr('data-redirect', redirect);
            jQuery('#register-form').attr('data-redirect', redirect);
            jQuery('#cadastro-login-form').attr('data-redirect', redirect);
        }else{
            jQuery('#frm-login-modal').attr('action', '/wp-login.php?redirect_to=<?php echo urlencode($_SERVER['REQUEST_URI']) ?>');
            jQuery('#register-form').attr('action', '/cadastro-login?is_ajax=1&ref=<?php echo urlencode($_SERVER['REQUEST_URI']) ?>');
            jQuery('#cadastro-login-form').attr('action', '/wp-login.php?redirect_to=<?php echo urlencode($_SERVER['REQUEST_URI']) ?>');

            jQuery('#frm-login-modal').attr('data-redirect', '<?php echo $_SERVER['REQUEST_URI'] ?>');
            jQuery('#register-form').attr('data-redirect', '<?php echo $_SERVER['REQUEST_URI'] ?>');
            jQuery('#cadastro-login-form').attr('data-redirect', '<?php echo $_SERVER['REQUEST_URI'] ?>');
        }
        //grecaptcha.reset();
     });

    jQuery('#login-modal-cadastrar').click(function(e){
        jQuery('#login-modal-panel').hide();
        jQuery('#login-modal > div.modal-dialog').addClass('modal-dialog-grande');
        jQuery('#cadastro-modal-panel').show();
    });

    jQuery('#login-modal-voltar').click(function(e){
        jQuery('#cadastro-modal-panel').hide();
        jQuery('#login-modal > div.modal-dialog').removeClass('modal-dialog-grande');
        jQuery('#login-modal-panel').show();
    });

</script>

<?php 
    //Foi colocado depois do script pq estava interferindo no funcionamento do modal    
    KLoader::view('cadastro_login/cadastro_login_scripts', ['is_ajax' => TRUE]);    
?>