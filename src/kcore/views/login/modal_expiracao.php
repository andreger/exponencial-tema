
<?php if(isset($_SESSION['expo_bad_login_auth'])): ?>

    <?php 
        unset($_SESSION['expo_bad_login_auth']);
        $browser = $_SERVER['HTTP_USER_AGENT'];
        log_debug("INFO", "[" . session_id() . "] ({$browser}) Exibindo modal de expiracao e apagando variavel de expiracao");
    ?>


    <div id="login-expiracao-modal" class="modal fade in" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered rounded">
            <div class="modal-content">
                <div class="modal-header">
                    <?php if(UrlHelper::is_url_sistema_questoes()): ?>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Aviso</h4>
                    <?php else: ?>
                        <h4 class="modal-title">Aviso</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <?php endif; ?>
                </div>
                <div class="modal-body">
                    <p>
                    Você foi deslogado por inatividade ou por apresentar logins simultâneos. Entre em contato conosco se precisar de ajuda
                    </p>
                </div>
                <div class='modal-footer text-center'>
                    <a class="btn btn-primary" data-dismiss="modal" href="#">OK</a>
                </div>
            </div>
        </div>
    </div>

    <script>

        jQuery( document ).ready(function() {
            jQuery('#login-expiracao-modal').modal("show");
        });

    </script>
<?php endif; ?>