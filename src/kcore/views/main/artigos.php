<div class="container pt-5 pb-2">
	<div class="pb-5">
	<?= $professor ?>
	</div>
	<div class="pb-4 text-center">
	<h2>Artigos publicados</h2>
	</div>

	<div id="artigos-professor-carregando" class='col-12 text-center'>
		<div style="padding: 10px"><img alt="Carregando..." width="40" src='/wp-content/themes/academy/images/carregando.gif'></div>
		<div style="padding: 10px">Carregando artigos...</div>
	</div>	       

</div>

<script>
        
	jQuery(function(){

		jQuery.post("/kcore/xhr/artigos_por_professor",
			{
				professor_id: '<?= $professor_id ?>',
				pg: '<?= $pg ?>',
				slug: '<?= $slug ?>',
			},
			function(data){
				jQuery("#artigos-professor-carregando").replaceWith(data);
			}
		);
	
	});

</script>

<?php get_footer();?>