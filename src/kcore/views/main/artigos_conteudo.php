<ul class="p-0 list-type-none">
    <?php foreach ( $artigos as $artigo ) : setup_postdata( $artigo ); ?> 
        <li class="b_listing d-block p-2 text-dark border border-gray mt-2 rounded-3">
            <span>
                <a class="t-d-none text-blue font-16" href="<?= UrlHelper::get_blog_post_url($artigo); ?>"><?= $artigo->post_title ?></a>
            </span>
            <span class="text-blue font-12 d-block mb-2 mt-1">
                <?php echo get_the_date('d-m-Y', $artigo); ?>
            </span>
            <span class="d-block"><?= $artigo->blo_resumo ?></span> 
            <div class="text-right pr-1">
                <a class="mt-2 mb-1 btn u-btn-primary font-10" href="<?= UrlHelper::get_blog_post_url($artigo); ?>">
                    Leia mais
                </a>
            </div>
        </li>
    <?php endforeach; ?>
</ul>

<div class="ml-auto mr-auto row mt-5 mb-3">
    <?= $paginator ?>
</div>