<div class="icones-fixed">
	<div>
		<a href="#info"><?= get_tema_image_tag('aba-informacao.png') ?></a>
	</div>
	<div>
		<a href="<?= $product->get_permalink() ?>?add-to-cart=<?= $product->id ?>">
		<?= get_tema_image_tag('aba-compra.png') ?></a>
	</div>
	<div>
		<a href="#cronograma"><?= get_tema_image_tag('aba-cronograma.png') ?></a>
	</div>
</div>