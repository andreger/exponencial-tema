<div class="mt-3 mt-lg-0 pt-3 pb-3 ml-1 row col-12 box-amarelo-light rounded-4 text-left text-md-center text-lg-left pr-0">
	<div class="text-left col-2 col-md-1 col-lg-2 p-0">
		<img class="img-fluid" src="/wp-content/themes/academy/images/bancaria.png">
	</div>
	<div class="col-10 pl-1 spr-0 text-left text-md-center text-lg-left">
		<?php 
		$viewName = $product->is_type("subscription") ? "preco_recorrente" : "preco_normal";
		$viewName = "produto/informacoes/direita/preco/{$viewName}";
        KLoader::view($viewName, ["product" => $product]);
		?>
	</div>
</div>