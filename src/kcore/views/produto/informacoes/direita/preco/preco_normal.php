<?php 
    $preco = is_pacote($product) ? $product->get_bundle_price() : $product->get_price();
    $sem_desconto = $product->get_regular_price();
    $ocultar_parcelamento = get_post_meta($product->id, PRODUTO_OCULTAR_PARCELAMENTO, true) !== '' ? get_post_meta($product->id, PRODUTO_OCULTAR_PARCELAMENTO, true) : 'no';
?>

<?php if($sem_desconto > $preco) : ?>
	<div class="curso-sem-desconto">De: <?= moeda($sem_desconto) ?></div>
<?php endif ?>

<?php if($preco > 0) : ?>
	<div class="font-12">Por: <?= moeda($preco) ?></div>
<?php else : ?>
	<div class="font-12"><strong>GRÁTIS</strong></div>
<?php endif ?>
		
<?php if($sem_desconto > $preco) : ?>
	<div class="font-10 text-danger">Desconto de
		<?= moeda($sem_desconto - $preco) ?>
		(<?= porcentagem_desconto($preco, $sem_desconto) ?>)
	</div>
<?php endif ?>
		
<?php if($preco > 0 && $ocultar_parcelamento == 'no') : ?>
	<div class="font-15 font-weight-bold">12x de <?= moeda($preco / 12) ?></div>
	<div>sem juros</div>
<?php endif ?>