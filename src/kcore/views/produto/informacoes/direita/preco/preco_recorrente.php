<?php 
KLoader::helper("ProdutoHelper");

$preco_recorrente = ProdutoHelper::get_preco_recorrente( $product );

$preco = $preco_recorrente['preco'];
$duracao = $preco_recorrente['duracao'];
$sem_desconto = $preco_recorrente['sem_desconto'];
$periodicidade_str1 = $preco_recorrente['periodicidade_str1'];
$periodicidade_str2 = $preco_recorrente['periodicidade_str2'];

?>

<?php if($duracao) : ?>

    <?php if($sem_desconto > $preco) : ?>
    	<div class="curso-sem-desconto">De: <?= $duracao ?>x de <?= moeda($sem_desconto) ?> <?= $periodicidade_str2 ?></div>
    <?php endif ?>

	<div class="font-11 font-weight-bold"><?= $duracao ?>x de <?= moeda($preco) ?> <?= $periodicidade_str2 ?></div>
<?php else : ?>

	<?php if($sem_desconto > $preco) : ?>
    	<div class="curso-sem-desconto">De: <?= moeda($sem_desconto) ?> <?= $periodicidade_str1 ?></div>
    <?php endif ?>
    
	<div class="font-11 font-weight-bold"><?= moeda($preco) ?> <?= $periodicidade_str1 ?></div>
<?php endif ?>
