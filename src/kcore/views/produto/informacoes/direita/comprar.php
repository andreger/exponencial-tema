<a class="t-d-none" class="btn-adicionar-carrinho" id="botao-comprar-curso" href="<?= adicionar_produto_ao_carrinho_url($product->id) ?>">
	<div class="mt-3 row col-12 ml-1 font-weight-bold text-white box-verde">
		<div class="text-left col-2 col-md-1 col-lg-2 pb-0 pt-1 pl-2 pr-1">		
			<?= $preco > 0 ? get_tema_image_tag('icone-compra.png') :  get_tema_image_tag('icone-aulagratis.png'); ?>
		</div>
		<div class="col-10 pl-0 pl-xl-2 pb-2 pr-0 pt-0 text-white text-left text-md-center text-lg-left font-15">
			<?php 
			/**
			 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2311
			 */
			echo ProdutoHelper::get_botao_acao_text($product->id);
			?>										
		</div>
	</div>
</a>