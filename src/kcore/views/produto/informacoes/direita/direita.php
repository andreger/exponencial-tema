<div class="pr-0 pl-0 pl-lg-2 col-12 col-lg-3">
	<?php KLoader::view("produto/informacoes/direita/preco/preco", ["product" => $product])?>
	<?php KLoader::view("produto/informacoes/direita/comprar", ["preco" => $preco, "product" => $product]) ?>
	<?php KLoader::view("produto/informacoes/direita/mostrar_garantia", ["mostrar_garantia" => $mostrar_garantia]) ?>
	<?php KLoader::view("produto/informacoes/direita/download_acesso_liberado", ["produto_id" => $product->id]) ?>
    <?php KLoader::view("produto/informacoes/direita/nao_bloqueia_cartao", ["product" => $product]) ?>
</div>