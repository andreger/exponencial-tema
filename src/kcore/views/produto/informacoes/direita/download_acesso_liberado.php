<?php 
if(is_produto_pdf($produto_id)) : ?>
	<div class="pt-2 pb-2 ml-1 row col-12 border border-success mt-3 rounded-4 tooltipster"  data-tooltip-content="#tooltip_download_liberado">

		<div class="text-left col-2 col-md-1 col-lg-2 pb-0 pt-1 pl-2 pr-1">		<?= get_tema_image_tag('pdf.png'); ?>						
		</div>

		<div class="mt-3 col-10 pl-0 pl-xl-2 pb-2 pr-0 pt-0 text-blue text-left text-md-center text-lg-left font-11">
				DOWNLOAD <b>LIBERADO</b>					
			
		</div>
		
	</div>
<?php endif ?>

<?php if(is_produto_video($produto_id)) : ?>
	<div class="pt-2 pb-2 ml-1 row col-12 border border-success mt-3 rounded-4 tooltipster"  data-tooltip-content="#tooltip_acesso_ilimitado">

		<div class="mt-1 text-left col-2 col-md-1 col-lg-2 pb-0 pt-1 pl-2 pr-1">		<?= get_tema_image_tag('coaching2-video.png'); ?>						
		</div>

		<div class="mt-2 col-10 pl-0 pl-xl-2 pb-2 pr-0 pt-0 text-blue text-left text-md-center text-lg-left font-11">
				ACESSO <b>ILIMITADO</b>							
			
		</div>
		
	</div>					
<?php endif ?>