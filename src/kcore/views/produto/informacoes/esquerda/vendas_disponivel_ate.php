<?php
$data_vendas_ate = get_data_vendas_ate($product->id);
$data_disponivel_ate = get_data_disponivel_ate($product->id);
$tempo_acesso = get_post_meta($product->id, PRODUTO_TEMPO_DE_ACESSO, true);

if($data_disponivel_ate || $data_vendas_ate || $tempo_acesso) : ?>
	<div class="mt-3 ml-1 row col-12 box-azul font-weight-bold font-10">

		<div class="text-left col-2 col-md-1 col-lg-2 pb-0 pt-1 pl-2 pr-1">			
			<?= get_tema_image_tag('icone-calendario.png'); ?>
		</div>

		<div class="col-10 pl-0 pl-xl-2 pb-2 pr-0 pt-0 text-white text-left text-md-center text-lg-left">
			<?php if($data_vendas_ate) : ?>
				<div><b><?= $product->get_price() == 0 ? "Aquisição" : "Vendas" ?> até: </b><?= $data_vendas_ate ?></div>
			<?php endif ?>

			<?php if($tempo_acesso) : ?>
				<div><b>Tempo de acesso: </b><?= $tempo_acesso ?> <?= $tempo_acesso == 1 ? "dia" : "dias" ?></div>
			<?php elseif($data_disponivel_ate) : ?>
				<div><b>Disponível até: </b><?= $data_disponivel_ate ?></div>
			<?php endif ?>
		</div>
	</div>
<?php endif; ?>