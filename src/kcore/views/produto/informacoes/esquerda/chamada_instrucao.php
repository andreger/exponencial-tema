<?php if($arquivos_demo) : ?>
	<div class="ml-1 row col-12 box-azul font-weight-bold font-10 mt-3">

		<div class="text-left col-2 col-md-1 col-lg-2 pb-0 pt-1 pl-2 pr-1">			
			<?= get_tema_image_tag('icone-aulademo.png'); ?>
		</div>

		<div class="col-10 pl-0 pl-xl-2 pb-2 pr-0 pt-0 text-white text-left text-md-center text-lg-left">
			<a id="ir-aula-demo" href="#">
				<?php
				/**
				 * Aula Demo/ Instruções de Acesso
				 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2318
				 */
				?>
				<div class="text-uppercase"><?= $chamada_instrucao ? "Instruções de Acesso" : "Aula demonstrativa" ?></div>
				<div>Confira aqui!</div>
			</a>
		</div>

	</div>

<?php endif ?>