<a class="anchor" id="info"></a>
<div class="container">
<?php KLoader::view("produto/informacoes/titulo/titulo") ?>

	<div class="col-12 mt-3">
		<div class="row">
			<?php 
			KLoader::view("produto/informacoes/esquerda/esquerda", [
			    "product" => $product, 
			    "produto" => $produto, 
			    "arquivos_demo" => $arquivos_demo,
			    "chamada_instrucao" => $chamada_instrucao
			]) ?>
			<?php KLoader::view("produto/informacoes/meio/meio", ["product" => $product, "produto" => $produto]) ?>
			<?php KLoader::view("produto/informacoes/direita/direita", [
			    "product" => $product, 
			    "produto" => $produto, 
			    "preco" => $preco,
			    "mostrar_garantia" => $mostrar_garantia,
			]) ?>
    	</div>
    </div>
</div>