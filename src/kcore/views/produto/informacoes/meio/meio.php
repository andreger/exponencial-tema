<?php 
// calcula campo Quantidade

$aulas = $produto->pro_qtde_aulas;
$esquemas = $produto->pro_qtde_esquemas;
$questoes = $produto->pro_qtde_questoes;
$carga_horaria = $produto->pro_carga_horaria;
$qtde_a = array();

$qtde = "";
if($aulas == 1) $qtde = "1 Aula";
if($aulas > 1) $qtde = "{$aulas} Aulas";
if($qtde) array_push($qtde_a, $qtde);

$qtde = "";
if($esquemas == 1) $qtde = "1 Esquema";
if($esquemas > 1) $qtde = "{$esquemas} Esquemas";
if($qtde) array_push($qtde_a, $qtde);

$qtde = "";
if($questoes == 1) $qtde = "1 Questão Comentada";
if($questoes > 1) $qtde = "{$questoes} Questões Comentadas";
if($qtde) array_push($qtde_a, $qtde);

// exibe ou oculta campos

$ocultar_tipo_produto = get_post_meta($product->id, PRODUTO_OCULTAR_TIPO_PRODUTO, TRUE);
$tipos = ($ocultar_tipo_produto && $ocultar_tipo_produto == YES) ? null : get_produto_categorias_curso_str($product->id, 'tipo-curso');

$ocultar_professores = get_post_meta($product->id, PRODUTO_OCULTAR_PROFESSORES, TRUE);
$autores = ($ocultar_professores && $ocultar_professores == YES) ? null : get_autores_str($product->post, '', true);

$ocultar_materias = get_post_meta($product->id, PRODUTO_OCULTAR_MATERIAS, TRUE);
$materias = ($ocultar_materias && $ocultar_materias == YES) ? null : ProdutoHelper::get_produto_materias_str($product->id, 'discipline', TRUE);

$ocultar_concursos = get_post_meta($product->id, PRODUTO_OCULTAR_CONCURSOS, TRUE);
$concurso = ($ocultar_concursos && $ocultar_concursos == YES) ? null : ProdutoHelper::get_produto_concursos_str($product->id, true);

$ocultar_qtde = get_post_meta($product->id, PRODUTO_OCULTAR_QUANTIDADE, TRUE);
$qtde_a = ($ocultar_qtde && $ocultar_qtde == YES) ? null : $qtde_a;

$ocultar_carga_horaria = get_post_meta($product->id, PRODUTO_OCULTAR_CARGA_HORARIA, TRUE);
$carga_horaria = ($ocultar_carga_horaria && $ocultar_carga_horaria == YES) ? null : $produto->pro_carga_horaria;
?>

<div class="pl-2 col-12 col-lg-6 box-amarelo-light rounded-4 pt-2 pl-0 font-10 line-height-2 produto-conteudo">
	<?php if($tipos) : ?>
		<div>
			<strong class="text-dark">Tipo do Produto: </strong> <span><?= $tipos ?></span>
		</div>
	<?php endif ?>

	<?php if($autores) : ?>
		<div>
			<strong class="text-dark">Professor(es): </strong><span><?= $autores ?></span>
		</div>
	<?php endif ?>

	<?php if($materias) : ?>
		<div class="t-d-none">
			<strong class="text-dark">Matéria(s): </strong> <span><?= $materias ?></span>
		</div>
	<?php endif ?>

	<?php if($concurso) : ?>
		<div class="t-d-none">
			<strong class="text-dark">Concurso(s): </strong> <span><?= $concurso ?></span>
		</div>
	<?php endif ?>

	<?php if($qtde_a) : ?>
		<div class="text-dark">					
			<strong>Quantidade: </strong> <span><?= implode(', ', $qtde_a) ?></span>
		</div>
	<?php endif ?>

	<?php if($carga_horaria) : 
		$carga_horaria_str = $carga_horaria == 1 ? "1 hora" : $carga_horaria . " horas"; ?>
		<div class="text-dark">
			<strong>Carga Horária: </strong> <span><?= $carga_horaria_str ?> de estudo</span>
		</div>
	<?php endif ?>

	<div>					
		<?php the_content();?>
	</div>

</div>