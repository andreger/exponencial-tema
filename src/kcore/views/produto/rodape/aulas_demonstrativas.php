<?php if(!$arquivos_demo) : ?>
	<div class="col-12 col-lg-7"></div>
<?php else : ?>	
	<div class="col-12 col-lg-7 box-azul-o">
		<?php if($arquivos_demo) : ?>
			<h3 class="pl-3 text-center text-lg-left">
				<?php
				/**
				 * Aula Demo/ Instruções de Acesso
				 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2318
				 */
				?>
				<?= get_tema_image_tag('icone-aulademo-azul.png') ?> <?= $chamada_instrucao ? "Veja como Acessar" : "Aula demonstrativa" ?>
			</h3>
			<div class="row">
				<?php foreach ($arquivos_demo as $arquivo) : ?>

					<div class="curso-demo col-12 col-lg-4 ml-0 mr-0">
						<div>
							<?php $titulo = is_pacote($product) ? " title='{$arquivo['curso']}' " : ""; ?>

							<?php if($arquivo['tipo'] == 'vimeo') : ?>
								<a href="<?= $arquivo['arquivo'] ?>" <?= $titulo ?> data-lity>
									<?= get_tema_image_tag('icone-video-maior.png'); ?>
								</a>
							<?php elseif($arquivo['tipo'] == 'caderno') : ?>
								<a href="<?= "/questoes/cadernos/caderno_demo/" . $arquivo['arquivo'] . "/" . $product->id ?>" target="_blank" <?= $titulo ?> >
									<?= get_tema_image_tag('icone-caderno-demo.png', "69px", "69px"); ?>
								</a>
							<?php else : ?>
								<a href="<?= get_url_aula_demonstrativa($arquivo['arquivo']); ?>" target="_blank" <?= $titulo ?> >
									<?= get_tema_image_tag('icone-pdf-maior.png'); ?>
								</a>
							<?php endif; ?>

						</div>
						<div class="curso-demo-titulo font-10">
							<?php
							if($arquivo['tipo'] == 'vimeo') {
								$vimeo = get_vimeo_video_by_url($arquivo['arquivo']);
								$nome = $vimeo['nome'];
								$url = $arquivo['arquivo'];
								echo "<a href='$url' $titulo data-lity>$nome</a>";
							}
							elseif($arquivo['tipo'] == 'caderno') 
							{
								$url = "/questoes/cadernos/caderno_demo/" . $arquivo['arquivo'] . "/" . $product->id;
								$nome = $arquivo['curso'];
								echo "<a href='{$url}' target='_blank'>{$nome}</a>";
							}
							else {
								$nome = $arquivo['curso'];
								$url = get_url_aula_demonstrativa($arquivo['arquivo']);
								echo "<a href='$url' $titulo target='_blank'>$nome</a>";
							}
							?>
						</div>
					</div>

						<?php 
							/**
							 * Aula Demo/ Instruções de Acesso
							 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2318
							 * 
							 * Se for Instruções de Acesso e Pacote, não repetir as aulas demo
							 */
							if($chamada_instrucao && is_pacote($product)) break;
						?>
					<?php endforeach ?>
				</div>
			<?php endif ?>			
			</div>
		<?php endif ?>