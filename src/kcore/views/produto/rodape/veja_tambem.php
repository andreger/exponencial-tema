<?php if($relacionados) : ?>
<div class="col-12 col-lg-5 pl-0 pl-lg-3 pr-0">
	<div class="ml-0 curso-veja-mais">
		<h3 class="text-center text-lg-left">
			<?= get_tema_image_tag('icone-vejatambem.png') ?> Veja também
		</h3>

		<div>
			<?php foreach ( $relacionados as $relacionado ) : ?>
				<a href="<?= UrlHelper::get_produto_url($relacionado->post_name); ?>">
					<div class="curso-veja-mais-item">
						<span><?= $relacionado->post_title; ?></span>
					</div>
				</a>
			<?php endforeach; ?>
		</div>
	</div>
</div>
<?php endif ?>