<?php 
/************************************************************************ 
 * PACOTE
 ************************************************************************/ 
?>
<?php if(is_pacote($product)) : ?>
    
    <a class="anchor" id="cronograma"></a>

    <div class="col-12 p-0 mt-3">
        <h2 class="text-blue text-center text-lg-left">Produtos - <?= $post->post_title ?></h2>
    </div>

    <?php 

        $product_array = array();

        foreach ( $product->get_bundled_items() as $item ) {
            array_push ( $product_array, $item->get_product());
        }

        $index_o = 0;

        $produtos_do_pacote = ordenar_produtos_do_pacote ( $product_array );

        foreach ($produtos_do_pacote as $produto) : ?>

            <div class="pacote-produto row pl-2 pr-2">

                <div class="col-12 pacote-produto-titulo"><?= $produto->post->post_title ?></div>

                <?php $index = 0 ?>
                <?php foreach (get_cronograma($produto->post->ID) as $aula) :?>

                    <div style="display:none" class="pacote-produto-conteudo col-12">
                        <?php $produto_id = $produto->post->ID; ?>

                        <?php include $_SERVER['DOCUMENT_ROOT'] . '/wp-content/themes/academy/woocommerce/cronograma.php' ?>

                    </div>
                    <div style="clear: both"></div>
                    <?php $index++ ?>
                <?php endforeach ?>
            </div>
        
            <div style="clear: both"></div>

            <?php $index_o++ ?>

        <?php endforeach; ?>

<?php else : ?>
<?php 
/************************************************************************ 
 * NÃO PACOTE
************************************************************************/ 
?> 
    <?php if (has_cronograma ( $post->ID )) : ?>

        <a class="anchor" id="cronograma"></a>

        <div class="row mt-3 ml-1">
            <h2><?= get_tema_image_tag('icone-cronograma.png') ?> Cronograma</h2>
        </div>

        <div class="col-12 mt-3">
            
            <?php $index = 0 ?>
            
            <?php $data_pedido = get_data_pedido_usuario_comprou_produto(get_current_user_id(), $post->ID); ?>

            <?php foreach (get_cronograma($post->ID, $data_pedido) as $aula) :?>

                <?php $produto_id = $post->ID ?>

                <?php include $_SERVER['DOCUMENT_ROOT'] . '/wp-content/themes/academy/woocommerce/cronograma.php' ?>

                <div class="sectiongap"></div>

                <?php $index++ ?>

            <?php endforeach; ?>

        </div>

    <?php endif; ?>

<?php endif; ?>


<?php
/********************************************************************
* BEGIN - Workaround para exibição de cronogramas antigos
*********************************************************************/
    $the_slug = 'cronograma-' . $post->post_name;
    $args = array (
        'name' => $the_slug,
        'post_type' => 'page',
        'post_status' => 'publish',
        'numberposts' => 1 
    );
    $my_posts = get_posts ( $args );
?>
<?php if ($my_posts) :?>
    <div class="row">
        <span class="sec_head_text">
            Cronograma das Aulas -  <?php the_title();?>
        </span>
    </div>
    <?php echo $my_posts [0]->post_content; ?>
<?php endif;
/********************************************************************
* END - Workaround para exibição de cronogramas antigos
*********************************************************************/
?>