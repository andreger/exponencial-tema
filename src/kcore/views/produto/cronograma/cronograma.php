<?php global $post; ?>

<div id="produto-cronograma" class="container mt-5">

	<?php if( $tem_cronograma = is_pacote($product) ) : ?>
	
		<a class="anchor" id="cronograma"></a>
		<div class="col-12 p-0 mt-3">
			<h2 class="text-blue text-center text-lg-left">Produtos - <?= $post->post_title ?></h2>
		</div>

		<div class='col-12 text-center'>
			<div style="padding: 10px"><img alt="Carregando..." width="40" src='/wp-content/themes/academy/images/carregando.gif'></div>
			<div style="padding: 10px">Carregando cronograma...</div>
		</div>
	
	<?php elseif ( $tem_cronograma = has_cronograma ( $post->ID ) ) : ?>

		<a class="anchor" id="cronograma"></a>
		<div class="row mt-3 ml-1">
			<h2><?= get_tema_image_tag('icone-cronograma.png') ?> Cronograma</h2>
		</div>

		<div class='col-12 text-center'>
			<div style="padding: 10px"><img alt="Carregando..." width="40" src='/wp-content/themes/academy/images/carregando.gif'></div>
			<div style="padding: 10px">Carregando cronograma...</div>
		</div>
	       	
	<?php endif; ?>

</div>

<?php if($tem_cronograma): ?>
	<script>

		jQuery(function(){

			jQuery.post("/kcore/xhr/midet_cronograma",
				{
					produto_id: '<?= $post->ID ?>',
					is_conta_usuario: '<?= $is_conta_usuario?:0 ?>'
				},
				function(data){
					
					jQuery("#produto-cronograma").html(data);

					if(jQuery(".botao-baixar-tudo")){
						jQuery(".botao-baixar-tudo").removeClass("d-none");
					}

				}
			);

		});

	</script>
<?php endif; ?>