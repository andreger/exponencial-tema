<div class="tooltip_templates" style="display: none">
    <span id="tooltip_mostrar_garantia">
    	<p>
        	<strong>Para ser reembolsado</strong>
        </p>
        <p>
        	Envie um e-mail para <a href="mailto:contato@exponencialconcursos.com.br">contato@exponencialconcursos.com.br</a> mencionando: nome do curso, número do pedido (fica na área do aluno) data de compra e motivos da insatisfação.
        </p>
        <p>
            O prazo para solicitação é de 30 dias após a compra, para produtos com vigência maior que este prazo. O reembolso é feito pelas plataformas de pagamento utilizadas, sendo processadas normalmente em até 2 meses do pedido, conforme política destas plataformas.
        </p>
        <p>
        	Confira o <a href="/termos-de-uso">TERMO DE USO</a> com as condições de cancelamento.
        </p>
    </span>
    
    <span id="tooltip_download_liberado">
    	<p>
        	<strong>Download Liberado</strong>
        </p>
        <p>
        	Você pode fazer o download do material em PDF quantas vezes quiser.
        </p>
    </span>
    
    <span id="tooltip_acesso_ilimitado">
    	<p>
        	<strong>Acesso Ilimitado</strong>
        </p>
        <p>
        	Você pode assistir às videoaulas quantas vezes quiser.
        </p>
    </span>
</div>