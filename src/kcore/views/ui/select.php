<select name="<?= $name ?>" <?= isset($attr["class"]) ? " class='{$attr["class"]}' " : '' ?> <?= isset($attr["style"]) ? "style='{$attr["style"]}' " : '' ?> <?= $multiple ? "multiple='multiple'" : "" ?> >
	<?php foreach ($items as $key => $value) : ?>
		<?php 
		if($selected && !is_array($selected)) {
		    $selected = [$selected];
		}
		?>
		<option value="<?= $key ?>" <?= $selected && in_array($key, $selected) ? "selected=selected" : "" ?>><?= $value ?></option>
	<?php endforeach; ?>
</select>