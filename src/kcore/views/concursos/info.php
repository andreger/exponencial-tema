<?php
global $wpdb;

KLoader::model("ProdutoModel");
KLoader::helper("ProdutoHelper");

$slug = get_query_var("nome");

if(!$slug) {
wp_redirect('/cursos-por-concurso', 301);
}

$title = get_h1($concurso->ID, true);

$qtd_premiums = ProdutoModel::contar_por_concurso($slug, null, null, null, null, null, true);
$qtd_destaques = ProdutoModel::contar_por_concurso($slug, null, null, true, null, null, false);
$qtd_pacotes = ProdutoModel::contar_por_concurso($slug, null, true, null, null, null, false);
$qtd_cursos = ProdutoModel::contar_por_concurso($slug, false, false, null, null, false, false);
$qtd_simulados = ProdutoModel::contar_por_concurso($slug, true, false, null, null, false, false);
$qtd_pacotes_simulados = ProdutoModel::contar_por_concurso($slug, true, true, null, null, false, false);
$qtd_cadernos = ProdutoModel::contar_por_concurso($slug, false, false, null, null, true, false);

$premiums = ProdutoModel::listar_por_concurso($slug, null, null, null, null, null, true, null, 0, LIMITE_POR_CONCURSO_ESPECIFICO_SESSAO);
$destaques = ProdutoModel::listar_por_concurso($slug, null, null, true, null, null, false, null, 0, LIMITE_POR_CONCURSO_ESPECIFICO_SESSAO);
$pacotes = ProdutoModel::listar_por_concurso($slug, null, true, null, null, null, false, null, 0, LIMITE_POR_CONCURSO_ESPECIFICO_SESSAO);
$cursos = ProdutoModel::listar_por_concurso($slug, false, false, null, null, false, false, null, 0, LIMITE_POR_CONCURSO_ESPECIFICO_SESSAO);
$simulados = ProdutoModel::listar_por_concurso($slug, true, false, null, null, false, false, null, 0, LIMITE_POR_CONCURSO_ESPECIFICO_SESSAO);
$pacotes_simulados = ProdutoModel::listar_por_concurso($slug, true, true, null, null, false, false, null, 0, LIMITE_POR_CONCURSO_ESPECIFICO_SESSAO);
$cadernos = ProdutoModel::listar_por_concurso($slug, false, false, null, null, true, false, null, 0, LIMITE_POR_CONCURSO_ESPECIFICO_SESSAO);

$ver_todos_url = UrlHelper::get_cursos_por_concurso_especifico_paginado_url($slug);
?>

<div class="container-fluid concurso p-0">

    <?php if(!empty($concurso)) : ?>
    <div class="container">
        <div class="col-12">
            <h1><?= $title ?></h1>
        </div>
    </div>
    <div class="concurso-slide">
        <div>
            <div><?php concurso_slide_componente($concurso) ?></div>
        </div>
    </div>
    <div class="col-12 mt-3">
        <div class="text-center pb-3 font-pop">
            <img src="<?= get_tema_image_url('info-icone.png') ?>" alt="Informações Úteis">
            Informações Úteis</div>
    </div>
    <div class="container p-0">
        <div class="mr-auto ml-auto row col-12 p-0 font-10 concurso-link">
            <?php $video_componente = get_concurso_video_componente($concurso); ?>
            <?php if(empty($video_componente)) : ?>
                <div class="col-12 pr-0 pl-md-2 pl-lg-5">
                    <div class="text-center text-md-left pl-4 pr-4"><?= get_concurso_descricao($concurso) ?></div>
                </div>
            <?php else : ?>
                <div class="col-12 col-md-6 pl-2 pl-sm-5">
                    <div class="pl-5 pr-5"><?= get_concurso_descricao($concurso) ?></div>
                </div>

                <div class="mr-auto ml-auto ml-md-0 mr-md-0 col-12 col-sm-7 col-md-6 col-xl-5">
                    <div class="tamanho"><?= $video_componente ?></div>
                </div>
            <?php endif; ?>
        </div>


        <?php if($cursos) : ?>
            <div class="col-12 mb-3 pt-5 text-center">
		  		<span class="font-pop">
		  			Cursos - <?= $title ?>		
		  		</span>
            </div>
        <?php endif ?>
        <?php else : ?>
            <?php if($cursos) : ?>
                <div class="col-12 mt-3 pt-5 text-center">
                    <span class="font-pop">Cursos - <?= $title ?></span>
                </div>
            <?php endif ?>
        <?php endif; ?>

        <?php $search_placeholder = isset($search_placeholder) ? $search_placeholder : "Procure pelo nome do curso"; ?>

        <?php if($cursos || $pacotes || $destaques || $premiums) : ?>
            <div class="container">
                <div class="p-l-r pt-3 col-md-6 offset-md-3">
                    <div class="row mb-3 mt-3">
                        <div class="col p-0 text-right">
                            <input class="pesquisa-barra form-control" type="text" id="search-cursos-ajax" placeholder="<?= $search_placeholder  ?>" /></div>
                        <div class="w-b-l p-0 text-left">

                            <div class="pesquisa-barra-lupa pointer">
                                <i class="fa fa-search mt-2 ml-2 text-white"></i>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="pt-4 d-none d-md-block col-12 text-right pr-0">
                    <a class="pointer" id="pesquisa-linha-3"><img width="50" src="<?= get_tema_image_url('view-linha.png') ?>" alt='Linha'></a>
                    <a class="pointer" id="pesquisa-grid-3"><img  width="50" src="<?= get_tema_image_url('view-grid.png') ?>" alt='Grid'></a>
                </div>

            </div>
        <?php endif ?>
        <div id="page-concurso-result" class="container">

            <?php

            $data = array(

                'title'	=>	$title,
                'ver_todos_url'	=>	$ver_todos_url,

                'qtd_premiums'	=>	$qtd_premiums,
                'qtd_destaques'	=>	$qtd_destaques,
                'qtd_pacotes'	=>	$qtd_pacotes,
                'qtd_cursos'	=>	$qtd_cursos,
                'qtd_simulados'	=>	$qtd_simulados,
                'qtd_pacotes_simulados'	=>	$qtd_pacotes_simulados,
                'qtd_cadernos'	=>	$qtd_cadernos,

                'premiums'	=>	$premiums,
                'destaques'	=>	$destaques,
                'pacotes'	=>	$pacotes,
                'cursos'	=>	$cursos,
                'simulados'	=>	$simulados,
                'pacotes_simulados'	=>	$pacotes_simulados,
                'cadernos'	=>	$cadernos,

            );

            KLoader::view("cursos/por-concurso", $data);

            ?>

        </div>
    </div>
</div>

<script src="<?php tema_js_url('filtro-cursos.js') ?>"></script>
<script>

    jQuery().ready(function(){

        jQuery("#search-cursos-ajax").keydown (function(e) {
            if(e.keyCode == 13) {
                pesquisar_cursos();
            }
        });

        jQuery(".pesquisa-barra-lupa").click(function(e){
            pesquisar_cursos();
        });

    });

    function pesquisar_cursos(){

        jQuery("#page-concurso-result").html("<div class='text-center pb-4'><?= loading_img() ?></div>");

        var busca_str = jQuery("#search-cursos-ajax").val();
        var concurso_id = "<?= $concurso->ID ?>";
        var slug = "<?= $slug ?>";

        jQuery.ajax({
            type: "POST",
            url: "<?= get_ajax_url('listar_cursos_por_concurso.php') ?>",
            data: {
                busca: busca_str,
                concurso_id: concurso_id,
                slug: slug
            },
            success: function(data){
                jQuery("#page-concurso-result").html(data);
            },
        });
    }

</script>