<div class="d-block d-md-none mt-8"></div>

<div class="container pt-1 pt-md-5 pb-5">
	<div class="section_content">
		<div class="sectiongap"></div>
		
		<div class="row">
		
    		<?php if($erro_validacao) : ?>
    	  		<div class="alert alert-danger col-12 text-center"><?= $erro_validacao; ?></div>
      		<?php endif; ?>
		
          	<div class="col-12 contato form_outter">
          		<?php $skip = is_selenium() ? "?skip=" . SELENIUM_KEY : "" ?>
          		<div>
          			<h1 class="font-pop"><?= get_h1() ?></h1>
          		</div>
          		
        		<form id="contact-form" class="contact-form" action="/fale-conosco<?= $skip ?>" method="POST" enctype="multipart/form-data">
        			<div>
        				<a id="produtos"></a>
        				<div class="fale-conosco-titulo">1 - Assunto</div>
        			</div> 
        			<div class="row">

        				<div class="mt-3 mt-lg-0 col-12 col-md-3 col-lg text-center">
        					<div class="fale-conosco-assunto-img">
        						<img src="<?= get_tema_image_url('fc-problema-acesso.png') ?>">
        					</div>
        					<div class="fale-conosco-assunto-texto font-10 mt-1">Problema no acesso</div>
        					<div class="fale-conosco-assunto-input mt-0 mt-md-2">
        						<input type="radio" name="sessao_assunto" value="<?= ASSUNTO_PROBLEMA_ACESSO ?>" <?= isset($_POST['sessao_assunto']) && $_POST['sessao_assunto'] == ASSUNTO_PROBLEMA_ACESSO ? "checked" : ""?>>
        					</div>
        				</div>

        				<div class="mt-3 mt-lg-0 col-12 col-md-3 col-lg text-center">
        					<div class="fale-conosco-assunto-img">
        						<img src="<?= get_tema_image_url('fc-finalizar-pagamento.png') ?>">
        					</div>
        					<div class="fale-conosco-assunto-texto font-10 mt-1">Não consigo finalizar minha compra</div>
        					<div class="fale-conosco-assunto-input mt-2">
        						<input type="radio" name="sessao_assunto" value="<?= ASSUNTO_NAO_CONSIGO_FINALIZAR_COMPRA ?>" <?= isset($_POST['sessao_assunto']) && $_POST['sessao_assunto'] == ASSUNTO_NAO_CONSIGO_FINALIZAR_COMPRA ? "checked" : ""?>>
        					</div>
        				</div>

        				<div class="mt-3 mt-lg-0 col-12 col-md-3 col-lg text-center">
        					<div class="fale-conosco-assunto-img">
        						<img src="<?= get_tema_image_url('fc-feedback.png') ?>">
        					</div>
        					<div class="fale-conosco-assunto-texto font-10 mt-1">Fazer um elogio, feedback ou reclamação</div>
        					<div class="fale-conosco-assunto-input mt-2">
        						<input type="radio" name="sessao_assunto" value="<?= ASSUNTO_FAZER_ELOGIO ?>" <?= isset($_POST['sessao_assunto']) && $_POST['sessao_assunto'] == ASSUNTO_FAZER_ELOGIO ? "checked" : ""?>>
        					</div>
        				</div>

        				<div class="mt-3 mt-lg-0 col-12 col-md-3 col-lg text-center">
        					<div class="fale-conosco-assunto-img">
        						<img src="<?= get_tema_image_url('fc-esqueci-senha.png') ?>">
        					</div>
        					<div class="fale-conosco-assunto-texto font-10 mt-1">Esqueci minha senha</div>
        					<div class="fale-conosco-assunto-input mt-2">
        						<input type="radio" name="sessao_assunto" value="<?= ASSUNTO_ESQUECI_SENHA ?>" <?= isset($_POST['sessao_assunto']) && $_POST['sessao_assunto'] == ASSUNTO_ESQUECI_SENHA ? "checked" : ""?>>
        					</div>
        				</div>

        				<div class="mt-3 mt-lg-0 col-12 col-md-3 col-lg text-center">
        					<div class="fale-conosco-assunto-img">
        						<img src="<?= get_tema_image_url('fc-cancelamento.png') ?>">
        					</div>
        					<div class="fale-conosco-assunto-texto font-10 mt-1">Cancelamento</div>
        					<div class="fale-conosco-assunto-input mt-2">
        						<input type="radio" name="sessao_assunto" value="<?= ASSUNTO_CANCELAMENTO ?>" <?= isset($_POST['sessao_assunto']) && $_POST['sessao_assunto'] == ASSUNTO_CANCELAMENTO ? "checked" : ""?>>
        					</div>
        				</div>

        				<div class="mt-3 mt-lg-0 col-12 col-md-3 col-lg text-center">
        					<div class="fale-conosco-assunto-img">
        						<img src="<?= get_tema_image_url('fc-desconto.png') ?>">
        					</div>
        					<div class="fale-conosco-assunto-texto font-10 mt-1">Descontos</div>
        					<div class="fale-conosco-assunto-input mt-2">
        						<input type="radio" name="sessao_assunto" value="<?= ASSUNTO_DESCONTOS ?>" <?= isset($_POST['sessao_assunto']) && $_POST['sessao_assunto'] == ASSUNTO_DESCONTOS ? "checked" : ""?>>
        					</div>
        				</div>

        				<div class="mt-3 mt-lg-0 col-12 col-md-3 col-lg text-center">
        					<div class="fale-conosco-assunto-img">
        						<img src="<?= get_tema_image_url('fc-duvida-aluno.png') ?>">
        					</div>
        					<div class="fale-conosco-assunto-texto font-10 mt-1">Outras dúvidas: Já sou aluno do Exponencial</div>
        					<div class="fale-conosco-assunto-input mt-2">
        						<input type="radio" name="sessao_assunto" value="<?= ASSUNTO_DUVIDA_ALUNO?>" <?= isset($_POST['sessao_assunto']) && $_POST['sessao_assunto'] == ASSUNTO_DUVIDA_ALUNO ? "checked" : ""?>>
        					</div>
        				</div>

        				<div class="mt-3 mt-lg-0 col-12 col-md-3 col-lg text-center">
        					<div class="fale-conosco-assunto-img">
        						<img src="<?= get_tema_image_url('fc-duvida-nao-aluno.png') ?>">
        					</div>
        					<div class="fale-conosco-assunto-texto font-10 mt-1">Outras dúvidas: Não sou aluno do Exponencial</div>
        					<div class="fale-conosco-assunto-input mt-2">
        						<input type="radio" name="sessao_assunto" value="<?= ASSUNTO_NAO_DUVIDA_ALUNO ?>" <?= isset($_POST['sessao_assunto']) && $_POST['sessao_assunto'] == ASSUNTO_NAO_DUVIDA_ALUNO ? "checked" : ""?>>
        					</div>
        				</div>
        		</div>
        				

        			<div id="orientacao-assunto" class="ml-3 mr-3 ml-md-0 mr-md-0 row fale-conosco-orientacao pb-2" style="display:none">
        				<div class="text-center text-md-left col-12 col-md-2 col-xl-1 p-0">
        					<img class="mt-3 ml-0 ml-md-3" src="<?= get_tema_image_url('fc-orientacao.png') ?>">
        				</div>

        				<div class="col-12 col-md-10 font-10 text-justify fale-conosco-orientacao-texto">
        					<h3 class="text-dark text-center text-md-left">Orientação</h3>
        				
        					<div class="fc-ass-ori fc-ass-ori-1">
        						Verifique na "Área do Aluno" o status do seu pedido (se está ativo e vigente), além de confirmar se o seu navegador está atualizado.<br>
								Firefox: <a href="https://support.mozilla.org/pt-BR/kb/atualize-o-firefox">https://support.mozilla.org/pt-BR/kb/atualize-o-firefox</a>
								Google Chrome: <a href="https://support.google.com/chrome/answer/95414?co=GENIE.Platform%3DDesktop&hl=pt-BR">https://support.google.com/chrome/answer/95414?co=GENIE.Platform%3DDesktop&hl=pt-BR</a>
							</div>

							<div class="fc-ass-ori fc-ass-ori-2">
        						Certifique-se do seu cadastro estar completo e correto (Área do Aluno --> Meu Perfil) e os navegadores atualizados. Ademais, o PagSeguro tem uma sistemática de proteção que muitas vezes gera um bloqueio na compra com uso de cartão de terceiros, portanto, faz-se necessário contactá-los assim como a administradora do cartão de crédito para certificar-se se não está bloqueado por segurança.
							</div>

							<div class="fc-ass-ori fc-ass-ori-4">
        						Para recuperação de senha <a href="/recuperacao-de-senha">CLIQUE AQUI</a>!

								Caso não consiga, informar todos os seus dados no campo de descrição: Nome completo, Nome da Mãe, CPF, Endereço e telefone
							</div>

							<div class="fc-ass-ori fc-ass-ori-5">
        						Para cancelamento, verifique os Termos de Serviço do Exponencial, disponíveis <a href="/termos-de-uso">CLIQUE AQUI</a>!<br>
								Caso se enquadre nas condições, prossiga com a solicitação
							</div>

<!--							<div class="fc-ass-ori fc-ass-ori-6">-->
<!--        						Verifique nossa Política de Descontos <a href="/descontos_promocoes">CLIQUE AQUI</a>!<br>-->
<!--							</div>-->
        				</div>

        				<div style="clear: both"></div>
        			</div>
        			
					<div>						
        				<div class="fale-conosco-titulo">2 - Produtos</div>
        			</div>

        			<div class="row">
        				<div class="text-center col-12 col-md-4">
        					<div class="fale-conosco-produto-img ">
        						<img src="<?= get_tema_image_url('fc-ss-sq.png') ?>">
        					</div>
        					<div class="fale-conosco-produto-texto font-14 mt-1">Sistema de Questões, Cadernos e Simulados</div>
        					<div class="fale-conosco-produto-input mt-2">
        						<input type="checkbox" name="sessao_produto[]" value="<?= ASSUNTO_SQ_SS ?>" <?= isset($_POST['sessao_produto']) && in_array(ASSUNTO_SQ_SS, $_POST['sessao_produto']) ? "checked" : ""?>>
        					</div>
        				</div>

        				<div class="text-center col-12 col-md-4">
        					<div class="fale-conosco-produto-img">
        						<img src="<?= get_tema_image_url('fc-coaching.png') ?>">
        					</div>
        					<div class="fale-conosco-produto-texto font-14 mt-1">Coaching</div>
        					<div class="fale-conosco-produto-input mt-2">
        						<input type="checkbox" name="sessao_produto[]" value="<?= ASSUNTO_COACHING ?>" <?= isset($_POST['sessao_produto']) && in_array(ASSUNTO_COACHING, $_POST['sessao_produto']) || $_POST['entrevista'] ? "checked" : ""?>>
        					</div>
        				</div>


        				<div class="text-center col-12 col-md-4">
        					<div class="fale-conosco-produto-img">
        						<img src="<?= get_tema_image_url('fc-pdf.png') ?>">
        						<img style="height: 50px;" src="<?= get_tema_image_url('fc-videos.png') ?>">
        					</div>
        					<div class="fale-conosco-produto-texto font-14 mt-1">Cursos Online</div>
        					<div class="fale-conosco-produto-input">
        						<input type="checkbox" name="sessao_produto[]" value="<?= ASSUNTO_PDF_VIDEO ?>" <?= isset($_POST['sessao_produto']) && in_array(ASSUNTO_PDF_VIDEO, $_POST['sessao_produto']) ? "checked" : ""?>>
        					</div>
        				</div>

        				<div style="clear: both"></div>
        			</div>

        			<div>
        				<div class="fale-conosco-titulo">3 - Número do Pedido</div>
        			</div>

        			<div>
        				<div class="input-group mb-4">
        					<input type="text" class="mt-2 ml-auto mr-auto form-control col-8 col-md-5 col-lg-4" name="numero_do_pedido" value="<?= isset($_POST['numero_do_pedido']) ? $_POST['numero_do_pedido'] : '' ?>">
        				</div>

        				<div class="fale-conosco-numero-input">
        					<input type="checkbox" id="nao-achei-numero" name="nao_achei_numero" value="1" <?= isset($_POST['nao_achei_numero']) && $_POST['nao_achei_numero'] == 1 ? "checked" : ""?>><br/>
        					<label class="font-10 font-weight-bold mt-2" for="nao-achei-numero">Não achei o número do pedido</label>
        				</div>
        			</div>
        			
        			<div id="orientacao-numero" class="ml-3 mr-3 ml-md-0 mr-md-0 row text-center fale-conosco-orientacao pb-2 pt-1" style="display:none">
        				<div class="text-center text-md-left col-12  col-md-1 p-0">
        					<img class="ml-2" src="<?= get_tema_image_url('fc-orientacao.png') ?>">
        				</div>

        				<div class="ml-0 ml-md-4 ml-lg-0 col-12 col-md-10 font-10 text-justify"> 
        					<h3 class="text-dark text-center text-md-left">Orientação</h3>

        					<div>
        						Você encontra o número do seu pedido após fazer o login e entrar em "Área do Aluno".
							</div>
						</div>

						<div style="clear: both"></div>
					</div>

        			<div>
        				<div class="fale-conosco-titulo">4 - Descrição</div>
        			</div>
        		<div class="container">	
	                <input class="form-control col-11 col-md-6 col-lg-4  mb-3 mr-auto ml-auto" type="text" id="cname" name="nome" value="<?= isset($_POST['nome']) ? $_POST['nome'] : '' ?>" placeholder="Nome"/>
	                <input class="form-control col-11 col-md-6 col-lg-4  mb-3 mr-auto ml-auto" type="text" id="email" name="email" value="<?= isset($_POST['email']) ? $_POST['email'] : '' ?>" placeholder="E-mail"/>
	                <input class="form-control col-11 col-md-6 col-lg-4  mb-3 mr-auto ml-auto" type="text" id="confirm_email" name="confirm_email" value="<?= isset($_POST['confirm_email']) ? $_POST['confirm_email'] : '' ?>" placeholder="Confirme seu e-mail"/>
 					<input class="form-control col-11 col-md-6 col-lg-4  mb-3 mr-auto ml-auto" type="text" id="whatsapp" name="whatsapp" value="<?= isset($_POST['whatsapp']) ? $_POST['whatsapp'] : '' ?>" placeholder="Atendimento rápido? Informe seu WhatsApp =)"/>
	                
	                
	                <div id="imagem-divs">
		                <div class="imagem-div">
			                <input type="file" class="mb-3 mr-auto ml-auto form-control col-11 col-md-6 col-lg-4 imagem" name="imagem[]" style="visibility: hidden; position: absolute;" />
			                <input type="text" class="mb-3 mr-auto ml-auto form-control col-11 col-md-6 col-lg-4 imagem-txb" placeholder="Clique aqui para inserir uma imagem" readonly="" />
			            </div>
			        </div>
			        <div class="text-center mt-3 mb-3"><a href="#" id="imagem-add">Adicionar outra imagem</a></div>
			        
			        <span style="visibility:hidden; line-heigth: 0.1px">
						<label for="expo_campo">Expocampo</label>
						<input type="text" name="expo_campo" size="1" value="" />
					</span>
	                
	                <textarea class="mb-3 form-control col-11 col-lg-10 mr-auto ml-auto" rows="5" placeholder="Mensagem" id="content" name="content"><?= isset($_POST['content']) ? $_POST['content'] : '' ?></textarea>
	            
	            	<div class="row">
	            		<div class="text-center w-100">	                				
            				<button class="btn u-btn-primary" id="submit" type="submit" value="submit" name="submit_primario">Enviar</button>
            				<div id="loading" style="display:none"><img width="20" src="<?= CARREGANDO_IMG ?>"> Aguarde um momento...</div>
        				</div>
            		</div>    
	            
	            </div>    
	                <div>
        				<div class="fale-conosco-titulo fale-conosco-whatsapp"><img src="<?= get_tema_image_url('fc-whatsapp.png') ?>">Whatsapp</div>
        			</div>

        			<div class="text-center"><?= get_option(GERAL_WHATSAPP) ?></div>
	           	</form>
        	</div>
        	<div class="clear"></div>
    	</div>
	</div>
	<div class="sectiongap"></div>
</div>