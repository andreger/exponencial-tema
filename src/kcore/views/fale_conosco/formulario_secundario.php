<div class="d-block d-md-none mt-8"></div>

<div class="container pt-1 pt-md-5 pb-5">
	<div class="section_content">
		<div class="sectiongap"></div>
		
		<div class="row">
		
			<div class="col-12 contato form_outter">
          		<?php $skip = is_selenium() ? "?skip=" . SELENIUM_KEY : "" ?>
          		<div>
          			<h1 class="font-pop"><?= get_h1() ?></h1>
          		</div>
          		
          		<?php if(!isset($erro_validacao_sec)) : ?>
        		<form id="contact-form" class="contact-form" action="/fale-conosco<?= $skip ?>" method="POST" enctype="multipart/form-data">
        			<div>
        				<a id="produtos"></a>
        				<div class="fale-conosco-titulo">Verificação e Confirmação</div>
        			</div> 
        			  
	                <div class="row text-center mt-5 mb-5">	                	
	                	<div class="ml-auto mr-auto float-captcha"> 	
							<?= carregar_recaptcha() ?>
						</div>
					</div>
					
        			<div class="text-center">
        				<button class="btn u-btn-primary" id="submit" type="submit" value="submit" name="submit_secundario">Enviar</button>
        				<div id="loading" style="display:none"><img width="20" src="<?= CARREGANDO_IMG ?>"> Aguarde. Sua mensagem está sendo enviada...</div>
        			</div>     			         
				</form>
				<?php else : ?>
					<?php if($erro_validacao_sec) : ?>
					<div class="text-center mt-4">
    					<img src="<?= get_tema_image_url("erro-grande.png") ?>" width="150">
                        <div class="p-5 font-20">
                        	Ocorreu um erro ao verificar seus dados.<br>Tente novamente.
                        </div>
                        <a href="/fale-conosco" class="btn btn-primary">Tentar novamente</a>
                    </div>
                    <?php else : ?>
                    <div class="text-center mt-4">
                    	<img src="<?= get_tema_image_url("confirm-grande.png") ?>" width="150">
                        <div class="p-5 font-20">
                        	Sua mensagem foi enviada com sucesso.
                        </div>
                        <a href="/" class="btn btn-primary">Voltar para Home</a>
                    </div>
					<?php endif ?>
				
				<?php endif ?>
			</div>
			
		</div>
	</div>
</div>
<?php if(!is_null($validacao_ok)) : ?>
	  		<?php if($validacao_ok) :?>
	  			<div id="faleconosco-sucesso" class="alert alert-success col-12 text-center">Sua mensagem foi enviada com sucesso. Enviamos uma cópia da mensagem para seu e-mail.<br>Por favor, confira se a recebeu. Em breve lhe responderemos.</div>
	  		<?php else : ?>
	  			<div class="alert alert-danger col-12 text-center"><?php echo $erro_validacao; ?></div>
	  		<?php endif; ?>
  		<?php endif; ?>
		
		<?php 
		if($validacao_ok) {

			
		}
		?>    