<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="/wp-content/themes/academy/js/jquery.validate.min.js"></script>
<script>
$().ready(function() {
	$('#confirm_email').bind("cut copy paste",function(e) {
        e.preventDefault();
    });

	$(".fale-conosco-assunto-input input").click(function () {
		var assuntos = ["1","2","4","5","6"];
		var selecionado = $(this).val();

		$('#orientacao-assunto, .fc-ass-ori').hide();
		if(assuntos.indexOf(selecionado) != -1) {
			$('.fc-ass-ori-'+selecionado).show();
			$('#orientacao-assunto').show();
		}
	});

	$('#nao-achei-numero').click(function () {
		if($(this).is(":checked")) {
			$('#orientacao-numero').show();	
		}
		else {
			$('#orientacao-numero').hide();	
		}
		
	});

	$(document).on('click', '.imagem-txb', function() {
		$(this).parent().find(".imagem").trigger("click");
	});

	$(document).on('change', '.imagem', function() {
		var texto = $(this).val();

		if(texto == "") {
			texto = "Clique aqui para inserir uma imagem";

			if($(".imagem").length > 1) {
				$(this).parent().remove();
			}
		}
		$(this).parent().find(".imagem-txb").attr("placeholder", texto);
	});

	$("#imagem-add").click(function(e) {
		e.preventDefault();

		var conteudo = '<div class="imagem-div"><input type="file" class="mb-3 mr-auto ml-auto form-control col-11 col-md-6 col-lg-4 imagem" name="imagem[]" style="visibility: hidden; position: absolute;" /><input type="text" class="mb-3 mr-auto ml-auto form-control col-11 col-md-6 col-lg-4 imagem-txb" placeholder="Clique aqui para inserir uma imagem" readonly="" /></div></div>';

		$("#imagem-divs").append(conteudo);

		if($(".imagem").length == 5) {
			$(this).hide();
		}
	});

	$("#submit").click(function() {
		$(this).hide();
		$("#loading").show();
	})

});
</script>