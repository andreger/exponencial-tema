<div class='box-professor'>
	<div class='d-block d-md-inline-block'>
		<a class='t-d-none' href='<?= $link ?>'>
			<img src='<?= $imagem ?>'>
			<div class='text-center text-md-left d-block d-md-inline-block'>
				<h1 class='text-center text-md-left font-20 pl-2 position-relative' style='top:12px;'><?= $nome ?></h1>
			</div>
		</a>
	</div>
</div>