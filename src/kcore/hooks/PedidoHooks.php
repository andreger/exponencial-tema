<?php
/**
 * Hooks relacionados aos pedidos
 *
 * @package	KCore/Hooks
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	L1
 */

/**
 * Altera o status dos produtos de um pedido que teve o status alterado no Woocommerce para o usuário final
 * 
 * @since L1
 * 
 * @param int $order_id ID do pedido
 * @param int $old_status antigo status do pedido no Woocommerce
 * @param int $new_status novo status do pedido no Woocommerce
 */
function khook_pedido_alterou_status($order_id, $old_status, $new_status){

    KLoader::model("PedidoModel");

    PedidoModel::alterar_pedido_status_usuario($order_id);

}
add_action( 'woocommerce_order_status_changed', 'khook_pedido_alterou_status', 10, 3 );

/*
function khook_pedido_recebido_url( $order_received_url, $order ){
    if ( count( $order->get_items() ) == 1 ) {
        foreach( $order->get_items() as $item ){
            KLoader::helper("UrlHelper");
            $order_received_url = UrlHelper::get_minha_conta_detalhe_url($order->get_id(), $item['product_id']);
            $order_received_url .= "&empty-cart=1";
            //Como não vai para a página de agradecimento tem que forçar a exibição do Google Merchant na página do produto
            //B2700 removido 21/10/2019
            //$order->update_meta_data( 'is_show_google_merchant', true );
            //$order->save();
            break;
        }
    }
    return $order_received_url;
}
add_filter('woocommerce_get_checkout_order_received_url', 'khook_pedido_recebido_url', 10, 2);
*/
/**
 * Reprocessa o pedido para atualizar o relatório de vendas
 *
 * @param int $order_id Id do pedido
 */

function khook_pedido_reprocessar_pedido($order_id)
{
    $type = get_post_type($order_id);
    
    if($type == 'shop_order'){
        // exclui do relatório de vendas. O script se encarregará de reprocessar, caso aplicável
        excluir_vendas_por_pedido($order_id);
    }
}

add_action( 'woocommerce_order_status_refunded', 'khook_pedido_reprocessar_pedido' , 20, 2);
add_action( 'woocommerce_order_status_cancelled', 'khook_pedido_reprocessar_pedido' , 20, 2);
add_action( 'wp_trash_post', 'khook_pedido_reprocessar_pedido', 10, 1);
add_action( 'before_delete_post', 'khook_pedido_reprocessar_pedido', 10, 1);

/**
 * Prevent password protected products being added to the cart.
 *
 * @param  bool $passed     Validation.
 * @param  int  $product_id Product ID.
 * @return bool
 */
function khook_validar_add_to_cart( $passed, $product_id ){

    KLoader::helper("CarrinhoHelper");
    KLoader::model("ProdutoModel");
    
    if(!empty( WC()->cart->cart_contents )){

        //$is_produto_assinatura = WC_Subscriptions_Product::is_subscription( $product_id );
        //$cart_contains_subscription = WC_Subscriptions_Cart::cart_contains_subscription();
        
        $is_produto_assinatura = ProdutoModel::is_produto_premium($product_id);
        $cart_contains_subscription = CarrinhoHelper::tem_produto_premium_no_carrinho();
        
        //a chamada wp_add_notice não está funcionando nesse ponto por isso o workaround na sessão
        if($cart_contains_subscription && !$is_produto_assinatura){
            $passed = false;
            //wc_add_notice("Existe um produto de assinatura no carrinho que não pode ser comprado juntamento com outros produtos. Para efetuar a compra remova todos os itens atuais do carrinho e adicione novamente o produto no carrinho.", "notice");
            $_SESSION['recorrente_aviso'] = 'nao-recorrente';
        }//Já existe um produto não-recorrente e o novo é recorrente
        elseif(!$cart_contains_subscription && $is_produto_assinatura){
            $passed = false;
            //wc_add_notice("O produto que está tentando comprar é uma assinatura e não pode ser comprado juntamente com outros produtos. Para efetuar a compra remova todos os itens atuais do carrinho e adicione novamente o produto no carrinho.", "notice");
            $_SESSION['recorrente_aviso'] = 'recorrente';
        }

    }

    return $passed;
}

add_filter('woocommerce_add_to_cart_validation', 'khook_validar_add_to_cart', 10, 2);

/**
 * Desativa envio de e-mail para usuário caso seja item de produto premium
 */
/*
function khook_desativar_email_compra_concluida_produto_premium_item($recipient, $order) {
    
    if(isset($_GET['act']) && $_GET['act'] == 'premium_item') {  
        $recipient = "clientes@keydea.com.br";
    }
    
    return "clientes@keydea.com.br";
}

add_filter('woocommerce_email_recipient_customer_completed_order', 'khook_desativar_email_compra_concluida_produto_premium_item', 10, 2);
*/

/**
 * Altera as informações do pedido original quando uma assinatura é cancelada ou expirada
 * 
 * @since M4
 * 
 * @param $subscription A WC_Subscription object representing the subscription that just had its status changed
 */
function khook_verificar_pedido_assinatura( $subscription_id, $status_before, $status_after, $subscription )
{
    $pedido_assinatura = $subscription->get_parent();
    if($pedido_assinatura)
    {
        $pedido_original_id = $pedido_assinatura->get_id();
        KLoader::model("PedidoModel");
        PedidoModel::alterar_pedido_status_usuario( $pedido_original_id, $status_after );
    }
}
add_action( 'woocommerce_subscription_status_changed', 'khook_verificar_pedido_assinatura', 20, 4);