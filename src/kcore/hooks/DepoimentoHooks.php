<?php
/**
 * Hooks relacionados às rotinas de Depoimento
 * 
 * @package	KCore/Hooks
 *
 * @author 	João Paulo Ferreira <jpaulofms@gmail.com>
 *
 * @since	L1
 */ 

/**
 * Atualiza metadados de um depoimento
 *
 * @param int $post_id Id do post do depoimento
 * @param WP_Post $post sendo atualizado, só está disponível quando é chamada do 'save_post_depoimentos'
 * @param boolean $update se é atualização, só está disponível quando é chamada do 'save_post_depoimentos'
 */

function khook_depoimento_atualizar_metadados($post_id, $post = null, $update = null)
{   
    KLoader::model("DepoimentoModel");
//    KLoader::api("DepoimentoApi");

    if($update){//Quando essa informação é passada é pq veio do hook do WP
        DepoimentoModel::atualizar_metadados($post_id);

        $dados = DepoimentoModel::get_by_id($post_id);
//        DepoimentoApi::salvar($dados);
    }else{

        $post_db = PostModel::get_by_id($post_id);

        //Quando os dois últimos são vazios é pq veio do hook do ACF
        if($post_db->post_type == 'depoimentos' && is_null($post) && is_null($update))
        {
            DepoimentoModel::atualizar_metadados($post_id, TRUE);

            $dados = DepoimentoModel::get_by_id($post_id);
//            DepoimentoApi::salvar($dados);
        }

    }
}

add_action('save_post_depoimentos', 'khook_depoimento_atualizar_metadados' , 20, 3);
add_action('acf/save_post', 'khook_depoimento_atualizar_metadados', 20);

function khook_depoimento_delete($post_id)
{
    if(get_post_type($post_id) == "depoimentos") {

        KLoader::model("DepoimentoModel");
//        KLoader::api("DepoimentoApi");

        DepoimentoModel::excluir($post_id);
//        DepoimentoApi::excluir($post_id);

        remover_cache();
    }
}

add_action('wp_trash_post', 'khook_depoimento_delete' , 20, 2);
add_action('delete_post', 'khook_depoimento_delete' , 20, 2);