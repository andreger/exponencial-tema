<?php
/**
 * Hooks relacionados às
 * 
 * @package	KCore/Hooks
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K4
 */ 

/**
 * Atualiza metadados de um cursos
 *
 * @param int $post_id Id da post do concurso
 */

function khook_concurso_atualizar_metadados($post_id)
{
	KLoader::model("ConcursoModel");
    KLoader::api("ConcursoApi");

	ConcursoModel::atualizar_metadados($post_id);
	
	// atualiza histórico de slugs
	$concurso = ConcursoModel::get_by_id($post_id);
	if($concurso && $concurso->con_slug) {
	    ConcursoModel::salvar_slug_historico($concurso->con_slug, $post_id);
	    ConcursoApi::salvar($concurso);
	}

//    remover_cache_concurso($post_id);


    remover_cache();
}
add_action('save_post_concurso', 'khook_concurso_atualizar_metadados' , 20);
add_action('acf/save_post_concurso', 'khook_concurso_atualizar_metadados', 20);


function khook_concurso_delete($post_id)
{
    KLoader::model("ConcursoModel");
//    KLoader::api("ConcursoApi");


    if(get_post_type($post_id) == "concurso" ||
        get_post_type($post_id) == "product"
    ) {

        if(get_post_type($post_id) == "concurso") {
            ConcursoModel::excluir($post_id);
//            ConcursoApi::excluir($post_id);
        }

        //        remover_cache_concurso($post_id);
        remover_cache();
    }
}

add_action('wp_trash_post', 'khook_concurso_delete' , 20, 2);
add_action('delete_post', 'khook_concurso_delete' , 20, 2);