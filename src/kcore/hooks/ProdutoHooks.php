<?php
/**
 * Hooks relacionados aos produtos
 * 
 * @package	KCore/Hooks
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K5
 */ 


add_action( 'save_post_product', 'khook_atualizar_produto', 15, 3 ); 

/**
 * Atualizar os metadados de um produto
 *
 * @since K5
 * 
 * @param int $post_id Id do post.
 * @param WP_Post $post Objeto do post
 * @param bool $update Informa se é um objeto novo ou existente.
 */

function khook_atualizar_produto($post_id, $post, $update)
{   
	if ($update) {
		KLoader::model("ProdutoModel");
		ProdutoModel::atualizar_metadados($post_id);
 	}

    remover_cache();
}

/**
 * Executado ao alterar o status de um post para verificar se existe um simulado ativo vinculado a um produto não publicado
 * Caso ocorra, o simulado é inativado
 * 
 * @param string $new_status
 * @param string $old_status
 * @param WP_Post $post
 * 
 */
function khook_alterar_status_produto( $new_status, $old_status, $post )
{
	log_wp( "INFO" , "[ProdutoHooks::khook_alterar_status_produto] Produto " . $post->ID . " mudando o status de {$old_status} para {$new_status}" );
	if( is_produto_simulado( $post->ID ) )
	{
		//Verifica se o produto não está publicado e o simulado está ativo
		KLoader::model("SimuladoModel");
		$simulado = SimuladoModel::get_simulado_by_produto( $post->ID );
		if($new_status != 'publish' && !is_null($simulado) && $simulado->sim_status == 1)
		{
			SimuladoModel::tornar_simulado_inconsistente( $simulado->sim_id );
			log_wp( "INFO", "[ProdutoHooks::khook_alterar_status_produto] O produto " . $post->ID . ", com novo status {$new_status}, esta associado ao simulado ativo " . $simulado->sim_id . ". O simulado foi transformado em inconsistente" );
		}
	}

    remover_cache();
}
add_action( 'transition_post_status', 'khook_alterar_status_produto', 15, 3 );


add_action( 'woocommerce_duplicate_product', 'khook_duplicar_produto', 10, 3 ); 
/**
 * Duplica um produto
 *
 * @since K5
 * 
 * @see https://www.sitepoint.com/displaying-errors-from-the-save_post-hook-in-wordpress/
 * 
 * @param int $produto_id Id do produto.
 */

function khook_duplicar_produto($produto_id, $original) 
{ 
	KLoader::model("ProdutoModel");

	// Corrige o SKU
	update_post_meta($produto_id, '_sku', $produto_id);

	// Cria fórum caso produto de origem tenha fórum
	if(ProdutoModel::get_forum_id($original->ID)) {
		criar_forum($produto_id);	
	}

    remover_cache();
}; 

/**
 * Inclui filtros na query de gerenciamento de produtos
 * 
 * @since L1
 * 
 * @param $query WP Query
 */
function khook_filtrar_produtos_wp_admin( $query )
{
	global $pagenow;
	
	$post_type = isset( $_GET['post_type'] ) ? $_GET['post_type'] : '';
	if ( $pagenow == 'edit.php' && $post_type == 'product' ) //É a listagem de produtos para edição: /wp-admin/edit.php?post_type=product
	{

		//É professor então só vê os produtos em que é autor
		if(!tem_acesso([ADMINISTRADOR, COORDENADOR, REVISOR, ATENDENTE, APOIO]) && tem_acesso([PROFESSOR]))
		{
			//Não há como filtrar via query quando o meta_value é um objeto serializado, então usa-se um like sobre o campo serializado
			$query->query_vars['meta_key'] = '_authors';
			$query->query_vars['meta_value'] = sprintf(':"%s";', get_current_user_id());
			$query->query_vars['meta_compare'] = 'LIKE';
		}
	  
	}

}
add_filter( 'parse_query', 'khook_filtrar_produtos_wp_admin' );

/**
 * Bloqueia a edição de um produto
 *
 * @since L1
 *
 * @param bool    $use_block_editor Whether the post can be edited or not.
 * @param WP_Post $post             The post being checked.
 */
function khook_bloquear_produto_wp_admin( $use_block_editor, $post )
{
	//É professor então só vê os produtos em que é autor
	if(!tem_acesso([ADMINISTRADOR, COORDENADOR, REVISOR, ATENDENTE, APOIO]) && tem_acesso([PROFESSOR]))
	{
		$autores_ids = get_post_meta($post->ID, '_authors', TRUE);

		$use_block_editor = !in_array(get_current_user_id(), $autores_ids);
	}else{
		$use_block_editor = !tem_acesso([ADMINISTRADOR, COORDENADOR, REVISOR, ATENDENTE, APOIO]);
	}
	return $use_block_editor;
}
add_filter( 'use_block_editor_for_post', 'khook_bloquear_produto_wp_admin', 10, 2 );

function khook_produto_download_alterar_filename_por_aula( $filename, $product_id )
{

	$tipo_aula = NULL;
	$nome_aula = get_nome_aula_by_file( $product_id, $filename, TRUE, $tipo_aula );

	if($nome_aula)
	{

		if($tipo_aula == TIPO_AULA_RESUMO)
		{
			$nome_aula = "Resumo-" . $nome_aula;
		}
		elseif($tipo_aula == TIPO_AULA_MAPA)
		{
			$nome_aula = "Mapa_Mental-" . $nome_aula;
		}

		$filename = str_replace(" ", "-", $nome_aula . "-" . $filename );
	}

	return $filename;
}
add_filter( 'woocommerce_file_download_filename', 'khook_produto_download_alterar_filename_por_aula', 10 , 2);