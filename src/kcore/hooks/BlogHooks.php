<?php

use Exponencial\Core\Cache\CacheFactory;

/**
 * Hooks relacionados aos blogs
 * 
 * @package	KCore/Hooks
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K4
 */

add_action('save_post_post', 'khook_blog_atualizar_metadados', 10, 3);

/**
 * Atualiza metadados de um blog post
 *
 * @param int $post_id Id do post.
 * @param WP_Post $post Objeto do post
 * @param bool $update Informa se é um objeto novo ou existente.
 */

function khook_blog_atualizar_metadados($post_id, $post, $update)
{
	KLoader::model("BlogModel");
//	KLoader::api("BlogApi");

	BlogModel::atualizar_metadados($post_id);

	// atualiza histórico de slugs
	$blog =  BlogModel::get_by_id($post_id);
	if($blog) {
	    BlogModel::salvar_slug_historico($blog->blo_slug, $post_id);
	}

//    BlogApi::salvar($blog);
}

add_action('save_post_post', 'khook_blog_limpar_cache', 10, 3);
add_action('delete_post_post', 'khook_blog_limpar_cache', 10, 3);
function khook_blog_limpar_cache($post_id, $post, $update)
{
    $cache = new CacheFactory();

    $cache->getMemcached()->delete(CacheFactory::HOME_NOTICIAS_ATUALIZADAS);
}

add_action('save_post_post', 'khook_blog_limpar_cache_professor', 15, 1);
add_action('delete_post_post', 'khook_blog_limpar_cache_professor', 5, 1);
function khook_blog_limpar_cache_professor($post_id)
{
	$blog =  BlogModel::get_by_id($post_id);

	if( $blog )
	{
		$professor_id = $blog->user_id;

		$cache = new CacheFactory();

		$cache_name = CacheFactory::PROFESSOR_ARTIGOS . $professor_id;
		$cache_name_total = CacheFactory::PROFESSOR_TOTAL_ARTIGOS . $professor_id;

		$memcached = $cache->getMemcached();
		$memcached->delete($cache_name);
		$memcached->delete($cache_name_total);

		log_kcore("DEBUG", "Cache {$cache_name} e {$cache_name_total} foram apagados");
	}
}

function khook_blog_delete($post_id)
{
    KLoader::model("BlogModel");
//    KLoader::api("BlogApi");

    if(get_post_type($post_id) == "post") {
        BlogModel::excluir($post_id);
//        BlogApi::excluir($post_id);
    }
}

add_action('wp_trash_post', 'khook_blog_delete' , 20, 1);
add_action('delete_post', 'khook_blog_delete' , 20, 1);