<?php
/**
 * Hooks relacionados aos usuários
 * 
 * @package	KCore/Hooks
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K4
 */ 

add_action( 'personal_options_update', 'khook_usuario_atualizar_metadados' , 20, 1);
add_action( 'edit_user_profile_update', 'khook_usuario_atualizar_metadados' , 20, 1);

/**
 * Atualiza metadados de usuários
 *
 * @param int $user_id Id do usuário
 */

function khook_usuario_atualizar_metadados($user_id)
{
	KLoader::model("ColaboradorModel");
	KLoader::model("UsuarioModel");
//    KLoader::api("UsuarioApi");

	UsuarioModel::atualizar_metadados($user_id);

	$dados = get_usuario_array($user_id);
//    UsuarioApi::salvar($dados);

	// se for colaborador atualiza histórico de slugs
	$colaborador = ColaboradorModel::get_by_id($user_id);
	if($colaborador) {
		ColaboradorModel::salvar_slug_historico($colaborador->col_slug, $user_id);
	}
}


function khook_excluir_usuario($id)
{
    KLoader::model("ColaboradorModel");
//    KLoader::api("UsuarioApi");

    ColaboradorModel::excluir($id);
//    UsuarioApi::excluir($id);
}

add_action( 'delete_user', 'khook_excluir_usuario' , 20, 1);