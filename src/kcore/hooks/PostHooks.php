<?php
/**
 * Hooks relacionados às
 *
 * @package	KCore/Hooks
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K4
 */

/**
 * Atualiza metadados de um cursos
 *
 * @param int $post_id Id da post do concurso
 */

function khook_post_atualizar_seo_h1($post_id)
{
    KLoader::model("SeoH1Model");
    KLoader::helper("SeoH1Helper");
    
    if($dados = SeoH1Helper::parse_html($post_id)) {
        SeoH1Model::salvar($dados);
    }  
}

add_action('save_post', 'khook_post_atualizar_seo_h1' , 20, 2);

/**
 * Dispara um acf/save_post específico para o tipo do post
 * No hook do WP save_post_{$post_type} os meta dados ainda não estão atualizados
 * Nesse do ACF eles já estão salvos no banco e podem ser utilizados
 * 
 * Note que esse hook só faz sentido para WP_Posts que utilizem o plugin ACF
 * 
 * @since M4
 * 
 * @param int $post_id ID do post
 */
function khook_acf_save_post( $post_id )
{
    $post_type = get_post_type( $post_id );
    do_action( "acf/save_post_{$post_type}", $post_id );
}
add_action("acf/save_post", "khook_acf_save_post", 20);