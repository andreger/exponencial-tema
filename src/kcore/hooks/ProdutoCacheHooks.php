<?php
/**
 * Hooks relacionados aos caches de produtos
 * 
 * @package	KCore/Hooks
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	L1
 * 
 * @see https://developer.wordpress.org/reference/hooks/before_delete_post/
 * @see https://codex.wordpress.org/Plugin_API/Action_Reference/before_delete_post
 */ 

add_action( 'save_post_product', 'khook_excluir_cache_produto', 30, 3 );
add_action( 'before_delete_post', 'khook_excluir_cache_produto', 30, 3 );

/**
 * Exclui o cache de um produto
 *
 * @since L1
 *
 * @param int $produto_id Id do produto.
 */

function khook_excluir_cache_produto($produto_id)
{
    $produto = get_post($produto_id);
    
    if($produto->post_type == 'product') {
        delete_transient("CACHE_PRODUTO_AULAS_DEMO_DATA" . $produto_id);
        delete_transient("CACHE_PRODUTO_AULAS_DEMO_HTML" . $produto_id);
    }
};
         