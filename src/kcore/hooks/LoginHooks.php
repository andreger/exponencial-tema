<?php
/**
 * Hooks relacionados às tarefas de login, logout, etc
 *
 * @package	KCore/Hooks
 *
 * @author 	João Paulo Ferreira <jpaulofms@gmail.com>
 *
 * @since	L1
 */

 /**
 * Verifica a situação do login a cada requisição para saber se o usuário foi deslogado por algum motivo.
 * Caso tenha sido cria uma informação na sessão para passar a informação para a view
 * 
 * @since L1
 *  
 */
function khook_init_login()
{
    /**
     * Inicialmente verifica se o token do navegador ainda é valido
     */
    $browser = $_SERVER['HTTP_USER_AGENT'];

    $user_id = get_current_user_id();
    $token = wp_get_session_token();

    //log_debug("INFO", "[" . session_id() . "] ({$browser}) InitLogin UserID: {$user_id} Token: {$token}");
    
    if($user_id && $token)
    {
        $sessions = WP_Session_Tokens::get_instance( $user_id );
        if(!$sessions->verify($token))
        {
            //log_debug("INFO", "[" . session_id() . "] ({$browser}) Usuario logado utilizando um token invalido: Setando variavel de expiracao");
            $_SESSION['expo_bad_login_auth'] = TRUE;
        }
        elseif($last_access = get_user_meta( $user_id, 'expo_last_access', true ))
        {
            //Verifica se o tempo entre esse acesso e o último acesso é maior que o tempo de expiração da sessão
            if ( ( time() - $last_access ) >= EXPIRACAO_COOKIE_AUTENTICACAO )
            {
                //log_debug("INFO", "[" . session_id() . "] ({$browser}) Usuario logado utilizando um token valido mas com tempo de sessao expirado: Limpando cookies de autenticacao, destruindo sessao e setando variavel de expiracao");
                wp_clear_auth_cookie();
                $sessions->destroy_all();
                wp_set_current_user( 0 );
                $_SESSION['expo_bad_login_auth'] = TRUE;
            }
        }
    }
    elseif(!$user_id && $token)
    {
        if (!isset($_SESSION['expo_bad_login_auth'])) 
        {
            //log_debug("INFO", "[" . session_id() . "] ({$browser}) Usuario logado inexistente utilizando um token: Limpando cookies de autenticacao e setando variavel de expiracao");
            wp_clear_auth_cookie();
            $_SESSION['expo_bad_login_auth'] = TRUE;
        }
        else
        {
            //log_debug("INFO", "[" . session_id() . "] ({$browser}) Usuario logado inexistente utilizando um token: Variavel de expiracao ja esta setada");
        }
    }
    elseif( $user_id && !$token )
    {
        //log_debug("ERRO", "[" . session_id() . "] ({$browser}) Usuario logado existente mas sem token: Acho que nao deveria existir essa possibilidade, pode ser que acabe atualizando o ultimo acesso do usuario {$user_id}");
    }


    /**
     * Extende o tempo de expiração do cookie de autendicação a cada requisição em que está OK
     */
    if ( is_user_logged_in() && !$_SESSION['expo_bad_login_auth'] /*is_user_logged_in()*/ ) 
    {
        //Removido para tentar fazer a expiração sem cookie através do user meta
        //wp_set_auth_cookie($user_id);
        ////log_debug("INFO", "Extendeu o cookie de {$user_id}");

        //Salva o último acesso
        update_user_meta( $user_id, 'expo_last_access', time() );
        //log_debug("INFO", "[" . session_id() . "] ({$browser}) Atualizou dado de ultimo acesso para o usuario {$user_id}");
    }

}
add_action("init", "khook_init_login", 11);

/**
 * Descrição do filtro:
 * 
 * Filters whether a set of user login credentials are valid
 * 
 * A WP_User object is returned if the credentials authenticate a user. WP_Error or null otherwise.
 * 
 * @since L1
 * 
 * @param $user (null|WP_User|WP_Error) WP_User if the user is authenticated. WP_Error or null otherwise.
 * @param $username (string) Username or email address.
 * @param $password (string) User password
 */
function khook_login_unico( $user, $username ) 
{

    if( $user && !is_wp_error( $user ) )
    {
        $browser = $_SERVER['HTTP_USER_AGENT'];
        $sessions = WP_Session_Tokens::get_instance( $user->ID );
        if($sessions)
        {
            //log_debug("INFO", "[" . session_id() . "] ({$browser}) Entrou no khook_login_unico, destruindo sessoes ativas do usuario {$username}");
            $sessions->destroy_all();
        }

        //Salva o último acesso
        update_user_meta($user->ID, 'expo_last_access', time() );
        //log_debug("INFO", "[" . session_id() . "] ({$browser}) Atualizou dado de ultimo acesso para o usuario {$user->ID} - {$username}");

    }

	return $user;
}
add_filter( 'authenticate', 'khook_login_unico', 30, 2 );

/**
 * Altera o tempo de expiração de um cookie de autenticação
 * 
 * @since L1
 * 
 * @param int $expire expiração em segundos
 * 
 * @return int Um novo tempo de expiração em segundos
 */
function khook_tempo_expiracao_cookie_autenticacao( $expire ) {    
    $expire = EXPIRACAO_COOKIE_AUTENTICACAO;
	return $expire;
}
//Removido para tentar fazer a expiração fora do cookie utilizando o user meta @see LoginHooks::khook_init_login
//add_filter( 'auth_cookie_expiration', 'khook_tempo_expiracao_cookie_autenticacao' );