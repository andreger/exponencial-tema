<?php
/**
 * Hooks relacionados aos blogs
 * 
 * @package	KCore/Hooks
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K4
 */ 

//add_action('save_post_landing_pages', 'khook_salvar_landing_page', 10, 3);
add_action('post_updated', 'khook_salvar_landing_page', 10, 3);//É necessário ter a informação anterior à edição para fazer o histórico

/**
 * Salvar dados da landing page
 *
 * @param int $post_id Id do post.
 * @param WP_Post $post Objeto do post
 * @param bool $update Informa se é um objeto novo ou existente.
 */

//function khook_salvar_landing_page($post_id, $post, $update)
function khook_salvar_landing_page($post_id, $post_after, $post_before)
{
	KLoader::model("LandingPageModel");
	
	// atualiza histórico de slugs
	$lp =  LandingPageModel::get_by_id($post_id);
	if($lp && $lp->post_name) {

	    if($post_before && $post_before->post_name) {
            LandingPageModel::salvar_slug_historico($post_before->post_name, $post_id);
        }
	}
}