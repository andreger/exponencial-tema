<?php
/**
 * Hooks relacionados às categorias
 * 
 * @package	KCore/Hooks
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K4
 */ 

/**
 * Atualiza metadados de categoria
 *
 * @param int $term_id Id da categoria
 */

function khook_categoria_atualizar_metadados($term_id)
{
	KLoader::model("CategoriaModel");

	CategoriaModel::atualizar_metadados($term_id);
}

add_action( 'edited_product_cat', 'khook_categoria_atualizar_metadados' , 20, 2);

/**
 * Cria metadados de categorias
 *
 * @param int $term_id Id da categoria
 */

function khook_categoria_criar_metadados($term_id)
{
	KLoader::model("CategoriaModel");

	CategoriaModel::criar_metadados($term_id);
}

add_action( 'created_product_cat', 'khook_categoria_criar_metadados' , 20, 2);

/**
 * Exclui metadados de categoria
 *
 * @param int $term Id da categoria
 */


function khook_categoria_excluir($term, $tt_id, $deleted_term, $object_ids)
{
    KLoader::model("CategoriaModel");

    CategoriaModel::excluir_metadados($deleted_term);
}

add_action( 'delete_product_cat', 'khook_categoria_excluir' , 20, 4);

/**
 * Ordena a listagem de categorias
 *
 * @param array         $terms      Array of found terms.
 * @param array         $taxonomies An array of taxonomies.
 * @param array         $term_query_vars       An array of get_terms() arguments.
 * @param WP_Term_Query $term_query The WP_Term_Query object.
 */

function khook_categoria_listar($terms, $taxonomies, $term_query_vars = null, $term_query = null)
{	
	KLoader::helper("CategoriaHelper");
	return CategoriaHelper::reordenar_categorias($terms);
}
// add_filter( 'get_terms', 'khook_categoria_listar' , 20, 4);

/**
 * Altera a ordenação da função "get_terms"
 * 
 * @param $args Argumentos para a função "get_terms"
 * 
 * @return $args com os parâmetros alterados
 */
function khook_categoria_ordenar( $args ) 
{
	$args['orderby'] = 'name';
    $args['order'] = 'ASC';

    return $args;
}
add_filter( 'get_terms_args', 'khook_categoria_ordenar', 10, 1 );