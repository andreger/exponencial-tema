<?php
/**
 * Hooks relacionados às páginas
 * 
 * @package	KCore/Hooks
 *
 * @author 	João Paulo Ferreira <jpaulofms@gmail.com>
 *
 * @since	M4
 */ 

/**
 * Retorna o ID da página "Minha Conta"
 *
 * @since M4
 */

function khook_page_minha_conta_id()
{
    $page = get_page_by_path("minha-conta");
    if ($page) {
        return $page->ID;
    } else {
        return null;
    }
}
add_filter('woocommerce_get_myaccount_page_id', 'khook_page_minha_conta_id', 10, 0);