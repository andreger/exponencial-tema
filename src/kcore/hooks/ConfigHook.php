<?php
/**
 * Hooks relacionados a configuração do WordPress
 *
 * @package	KCore/Hooks
 *
 * @author 	João Paulo Ferreira <jpaulofms@gmail.com>
 *
 * @since	L1
 */

/**
 * WordPress não usa sessão então a sessão não é iniciada automaticamente. Para usar sessção no WordPress, é necessário iniciá-la manualmente
 * 
 * @see http://www.kanasolution.com/2011/01/session-variable-in-wordpress/
 * 
 * @since L1
 * 
 */
function khook_init_session()
{
    if (!session_id()){
        session_start();
    }
}
add_action('init', 'khook_init_session', 1);
