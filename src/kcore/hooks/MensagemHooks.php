<?php
/**
 * Hooks relacionados às mensagens do WP-Admin
 * 
 * @package	KCore/Hooks
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K5
 */ 

add_action('admin_notices', 'khook_mensagem_erro_produto', 10, 3 );

/**
 * Valida os dados de um produto
 *
 * @since K5
 * 
 * @see https://codex.wordpress.org/Plugin_API/Action_Reference/admin_notices
 * @see https://www.sitepoint.com/displaying-errors-from-the-save_post-hook-in-wordpress/
 */

function khook_mensagem_erro_produto()
{
	// KLoader::model("BlogModel");
	if (array_key_exists( 'kcore-erro', $_GET) ) {

        switch($_GET['kcore-erro']) {
            case ERRO_PERCENTUAL_DIFERENTE_DE_100: {
                $erro = 'A somatória do percentual dos autores do curso deve ser igual a 100.';
                break;
            }
        }

        echo "<div class='error'>{$erro}</div>";
    }
}


