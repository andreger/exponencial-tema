<?php
/**
 * Hooks relacionados ao head da página
 * 
 * @package	KCore/Hooks
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	L1
 */ 

remove_action('template_redirect', 'rest_output_link_header', 11, 0);
remove_action('wp_head', 'rest_output_link_wp_head', 10);
remove_action('wp_head', 'wp_oembed_add_discovery_links', 10);


/**
 * Altera a descrição do título da página com base no Yoast
 * 
 * @since L1
 * 
 * @param string $desc descrição
 * 
 * @return string descrição modificada/personalizada
 */
function khook_head_descricao_yoast( $desc ) 
{
	return khook_parse_texto_yoast($desc, 'metadesc', FALSE);
}
add_filter( 'wpseo_metadesc', 'khook_head_descricao_yoast', 10, 1 ); 

/**
 * Altera o título da página com base no Yoast
 * 
 * @since L1
 * 
 * @param string $title título da página
 * 
 * @return string título modificado para a página
 */
function khook_head_titulo_yoast($title){
	return khook_parse_texto_yoast($title, 'title', FALSE);
}
add_filter('wpseo_title', 'khook_head_titulo_yoast');

/**
 * Altera o texto passado com base no Yoast
 * 
 * @since L1
 * 
 * @param string $texto texto base
 * @param string $wpseo_meta_key Meta Key do SEO no Yoast
 * @param bool $is_description Se é descrição, do contrário é título
 * 
 * @return string texto modificado para a página
 */
function khook_parse_texto_yoast($texto, $wpseo_meta_key, $is_description = FALSE)
{
	KLoader::helper("UrlHelper");
    KLoader::helper("YoastHelper");

	if(UrlHelper::is_url_cursos_por_concurso_especifico() || UrlHelper::is_url_cursos_por_concurso_especifico_paginada())
	{
		KLoader::model("ConcursoModel");
		$nome_concurso = get_query_var("nome");
		$concurso = ConcursoModel::get_by_slug($nome_concurso);

		$meta = WPSEO_Meta::get_value($wpseo_meta_key, $concurso->con_id );

		if(!$meta) {
			$meta = YoastHelper::get_valor_default($is_description?META_YOAST_CONCURSO_DESCRICAO:META_YOAST_CONCURSO_TITULO);
		}

		$meta = YoastHelper::parse_concurso_meta($meta, $concurso);
		
		return $meta;
	}
	elseif(UrlHelper::is_url_cursos_por_materia_especifica())
	{
		KLoader::model("PostModel");
		KLoader::model("MateriaModel");

		$cat_name = get_query_var('cat_name');
		$materia = MateriaModel::get_by_slug($cat_name);

		$pagina = PostModel::get_by_slug('cursos-por-materia-especifica');

		$meta = WPSEO_Meta::get_value($wpseo_meta_key, $pagina->ID);

		if(!$meta) {
			$meta = YoastHelper::get_valor_default($is_description?META_YOAST_MATERIA_DESCRICAO:META_YOAST_MATERIA_TITULO);
		}

		$meta = YoastHelper::parse_materia_meta($meta, $materia, $pagina->ID);
		
		return $meta;
	}
	elseif(UrlHelper::is_url_cursos_por_professor_especifico())
	{
		KLoader::model("PostModel");
		KLoader::model("ProfessorModel");

		$slug = get_query_var('nome');
		$professor = ProfessorModel::get_by_slug($slug);

		$pagina = PostModel::get_by_slug('cursos-por-professor-especifico');

		$meta = WPSEO_Meta::get_value($wpseo_meta_key, $pagina->ID);

		if(!$meta) {
			$meta = YoastHelper::get_valor_default($is_description?META_YOAST_PROFESSOR_DESCRICAO:META_YOAST_PROFESSOR_TITULO);
		}

		$meta = YoastHelper::parse_professor_meta($meta, $professor, $pagina->ID);
		
		return $meta;
		
	}
	else 
	{
		return $texto;
	}
}
/**
 * Altera o título da página
 * 
 * @since L1
 * 
 * @param string $title Título da página
 * 
 * @return string título modificado para a página
 */
function khook_render_title($title){
    KLoader::helper("HeadHelper");
    return HeadHelper::render_title($title);
}
add_filter( 'pre_get_document_title', 'khook_render_title', 100);

function khook_deregister_styles() {
    wp_deregister_style('wp-block-library');
    wp_deregister_style('wc-block-style');
    wp_deregister_style('woocommerce-layout');
    wp_deregister_style('woocommerce-general');
    wp_deregister_style('woocommerce-smallscreen');
    wp_deregister_style('woocommerce-inline');
//    wp_deregister_style('dashicons');
}

add_action( 'wp_print_styles', 'khook_deregister_styles', 100 );

function khook_deregister_scripts() {
    //wp_deregister_script('jquery-core');
}

add_action( 'wp_print_scripts', 'khook_deregister_scripts', 100 );