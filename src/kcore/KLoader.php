<?php

use Exponencial\Core\Cache\CacheFactory;

/**
 * Classe responsável por carregar componentes do sistema
 * 
 * @package	Loader
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K4
 */ 

class KLoader {

	/**
	 * Carrega o controller especificado.
	 * 
	 * @since K4
	 * 
	 * @param string $nome Nome do controller
	 */ 

	static function controller($nome)
	{
		include_once get_stylesheet_directory() . '/kcore/controllers/' . $nome . '.php';
	}

	/**
	 * Carrega o helper especificado.
	 * 
	 * @since K4
	 * 
	 * @param string $nome Nome do helper
	 */ 

	static function helper($nome)
	{
		include_once get_stylesheet_directory() . '/kcore/helpers/' . $nome . '.php';
	}

	/**
	 * Carrega o model especificado.
	 * 
	 * @since K4
	 * 
	 * @param string $nome Nome do model
	 */ 

	static function model($nome)
	{
		include_once get_stylesheet_directory() . '/kcore/models/' . $nome . '.php';
	}

    /**
     * Carrega a API especificada.
     *
     * @since N
     *
     * @param string $nome Nome da API
     */

    static function api($nome)
    {
        include_once get_stylesheet_directory() . '/kcore/api/' . $nome . '.php';
    }


    /**
	 * Carrega a view especificada.
	 * 
	 * @since K4
	 * 
	 * @param string 	$nome Nome da view
	 * @param array 	$data Variáveis a serem utilizadas na view
	 * @param bool 		$retornar Flag para retornar ou escrever o conteúdo da view na tela.
	 * @param string    $cache_name Chave que armazenará o Cache
	 * @param int       $cache_time Tempo de expiração do cache em segundos
	 *
	 * @return string HTML da view
	 */

	static function view($view_nome, $data = null, $retornar = false, $cache_name = null, $cache_time = HOUR_IN_SECONDS)
	{
	    $cacheFactory = new CacheFactory();
	    $memcached = $cacheFactory->getMemcached();

	    // procura por cache
        $cache = null;
	    if($cache_name) {
           $cache = get_memcached_entry($cache_name);
        }

	    if(!$cache) {
    		if($data) {
    			extract($data);
    		}
    		
    		ob_start();
    		include get_stylesheet_directory() . '/kcore/views/' . $view_nome . '.php';
    		$cache = ob_get_clean();
    		
    		if($cache_name) {
    		    set_memcached_entry($cache_name, $cache, $cache_time);
    		}
	    }

		if($retornar) {
			return $cache;
		}
		else {
			echo $cache;
		}
	}
	
	/**
	 * Inclui o CSS na fila de renderização. Deve ser chamado antes do get_header().
	 *
	 * @since L1
	 *
	 * @param string $nome Nome do arquivo. Relativo a pasta assets/css
	 *
	 * @return string HTML da view
	 */
	
	static function css($nomes)
	{  
	    /* === Tratamento para aceitar arrays === */
	    if(!is_array($nomes)) {
	        $nomes = [$nomes];
	    }
	    
	    /* === Fluxo principal === */
	    foreach ($nomes as $nome) {
    	    add_action("wp_enqueue_scripts", function() use ($nome) {
    	        wp_enqueue_style($nome, get_template_directory_uri() . "/assets/css/{$nome}.css?v=" . VERSAO);
    	    });
	    }
	}

	/**
	 * Inicia as variáveis globais
	 * 
	 * @since K4
	 */

	static function init_globals()
	{
		self::helper("UrlHelper");
		self::model("ConcursoModel");
		self::model("MateriaModel");
		self::model("LandingPageModel");
		self::model("ProfessorModel");
		
		self::init_cursos_por_concurso_especifico();
		self::init_cursos_por_concurso_especifico_paginado();
		self::init_cursos_por_materia_especifica();
		self::init_cursos_por_professor_especifico();
		self::init_detalhe_professor();
		self::init_artigos_por_professor_especifico();
		self::init_landing_pages();
		self::init_blogs();

	}
	
	/**
	 * Inicia cursos por concurso específico
	 *
	 * @since L1
	 */
    static private function init_cursos_por_concurso_especifico()
    {
        if(UrlHelper::is_url_cursos_por_concurso_especifico()) {
            
            global $concurso;
            
            $slug = get_query_var("nome");

            $concurso = ConcursoModel::get_by_slug($slug);
            
            if(!$concurso) {
                $historico = ConcursoModel::get_by_slug_historico($slug);
                
                if($historico) {
                    wp_redirect(UrlHelper::get_cursos_por_concurso_especifico_url($historico->post_name), 301);
                    exit;
                }
                else {
                    wp_redirect(UrlHelper::get_cursos_por_concurso_url(), 301);
                    exit;
                }
            }
        }
    }
    
    /**
     * Inicia cursos por concurso específico paginado
     *
     * @since L1
     */
    static private function init_cursos_por_concurso_especifico_paginado()
    {
        if(UrlHelper::is_url_cursos_por_concurso_especifico_paginada()) {
            
            global $concurso;
            
            $concurso = ConcursoModel::get_by_slug(get_query_var("con_name"));
        }
    }
    
    /**
     * Inicia cursos por matéria específica
     *
     * @since L1
     */
    static private function init_cursos_por_materia_especifica()
    {
        // Cursos por matéria específica
        if(UrlHelper::is_url_cursos_por_materia_especifica()) {
            
            global $materia;
            
            $materia = MateriaModel::get_by_slug(get_query_var("cat_name"));
        }
    }
    
    /**
     * Inicia cursos por professor específico
     *
     * @since L1
     */
    static private function init_cursos_por_professor_especifico()
    {
        // Cursos por professor específico
        if(UrlHelper::is_url_cursos_por_professor_especifico()) {
            global $professor;
            $professor = ProfessorModel::get_by_slug(get_query_var("nome"));
            
            if(!$professor) {
                $historico = ColaboradorModel::get_by_slug_historico(get_query_var("nome"));
                
                if($historico) {
                    wp_redirect(UrlHelper::get_cursos_por_professor_especifico_url($historico->col_slug));
                }
                else {
                    wp_redirect(UrlHelper::get_cursos_por_professor_url());
                }
            }elseif($professor->col_oculto){
                wp_redirect(UrlHelper::get_cursos_por_professor_url());
            }
        }
    }
    
    /**
     * Inicia página de detalhe de professor
     *
     * @since L1
     */
    static private function init_detalhe_professor()
    {
        // Página de detalhe de professor
        if(UrlHelper::is_url_professor()) {
            global $professor;
            $professor = ProfessorModel::get_by_slug(get_query_var("nome"));
            
            if(!$professor) {
                $historico = ColaboradorModel::get_by_slug_historico(get_query_var("nome"));
                
                if($historico) {
                    wp_redirect(UrlHelper::get_professor_url($historico->col_slug));
                }
                else {
                    wp_redirect(UrlHelper::get_professores_url());
                }
            }
        }
    }
    
    /**
     * Inicia artigos por professor específico
     *
     * @since L1
     */
    static private function init_artigos_por_professor_especifico()
    {
        if(UrlHelper::is_url_artigos_por_professor_especifico()) {
            global $professor;
            $professor = ProfessorModel::get_by_slug(get_query_var("nome"));
            
            if(!$professor) {
                $historico = ColaboradorModel::get_by_slug_historico(get_query_var("nome"));
                
                if($historico) {
                    wp_redirect(UrlHelper::get_artigos_por_professor_especifico_url($historico->col_slug));
                }
                else {
                    wp_redirect(UrlHelper::get_professores_url());
                }
            }
        }
    }
    
    /**
     * Inicia conteúdo de blog
     *
     * @since L1
     */
    static private function init_blogs()
    {
        if($slug = UrlHelper::get_blog_ou_pagina_slug_by_url()) {
            
            $blog = BlogModel::get_by_slug($slug);
            
            if(!$blog) {
                $historico = BlogModel::get_by_slug_historico($slug);
                
                if($historico) {
                    wp_redirect('/' . $historico->post_name, 301);
                    exit;
                }
                else {
                    wp_redirect('/blog-noticias', 301);
                    exit;
                }
            }
        }
    }
    
    /**
     * Inicia conteúdo de landing pages
     *
     * @since L1
     */
    static private function init_landing_pages()
    {
        
        if(UrlHelper::is_url_landing_page()) {

            global $landing_page;
            
            $slug = get_query_var("nome");
            
            $landing_page = LandingPageModel::get_by_slug($slug);
            
            if(!$landing_page) {
                $historico = LandingPageModel::get_by_slug_historico($slug);
                //print_r($historico);    
                if($historico) {
                    wp_redirect(UrlHelper::get_landing_page_url($historico->post_name), 301);
                    exit;
                }
                /*else {
                    wp_redirect(UrlHelper::get_cursos_por_concurso_url(), 301);
                    exit;
                }*/
            }

        }
    }

}