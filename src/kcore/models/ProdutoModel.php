<?php
/**
 * Model para organizar rotinas relacionadas aos Produtos
 *
 * @package	KCore/Models
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K4
 */

KLoader::model("PostModel");

class ProdutoModel extends PostModel {
    
    /**
     * Atualiza os metadados de um produto
     *
     * @since K4
     *
     * @param int $id Id do produto
     */
    
    public static function atualizar_metadados($id, $atualizar_concurso = true)
    {
        // atualiza básicos
        self::atualizar_metadados_basicos($id);
        
        // atualiza básicos
        self::atualizar_metadados_areas($id);
        
        // atualiza professores
        self::atualizar_metadados_professores($id);
        
        // atualiza matérias
        self::atualizar_metadados_materias($id);
        
        // atualiza concursos
        self::atualizar_metadados_concursos($id, $atualizar_concurso);
        
        // atualiza aulas
        self::atualizar_metadados_aulas($id);
        
        // atualiza relacionados
        //self::atualizar_produtos_relacionados($id);
    }
    
    
    /**
     * Atualiza os metadados de áreas de produto
     *
     * @since K4
     *
     * @param int $id Id do produto
     */
    
    public static function atualizar_metadados_areas($id)
    {
        global $wpdb;
        
        $areas = self::listar_categorias($id, CATEGORIA_AREA);
        
        $wpdb->delete('produtos_areas', ['post_id' => $id]);
        if($areas) {
            foreach ($areas as $item) {
                $area = [
                    'post_id' => $id,
                    'area_id' => $item->term_id,
                ];
                
                $wpdb->insert('produtos_areas', $area);
            }
        }
    }
    
    /**
     * Atualiza os metadados das aulas de um produto
     *
     * @since K4
     *
     * @param int $id Id do produto
     */
    
    public static function atualizar_metadados_aulas($id)
    {
        global $wpdb;
        
        $wpdb->delete('produtos_aulas', ['post_id' => $id]);
        
        $aulas_arquivo = get_post_meta($id, 'aulas_arquivo', true);
        $aulas_video = get_post_meta($id, 'aulas_vimeo', true);
        $aulas_caderno = get_post_meta($id, 'aulas_caderno', true);
        $aulas_resumo = get_post_meta($id, 'aulas_resumo', true);
        $aulas_mapa = get_post_meta($id, 'aulas_mapa', true);
        $aulas_data = get_post_meta($id, 'aulas_data', true);
        
        if($aulas_data) {
            $num_aula = 0;
            foreach ($aulas_data as $i => $data) {
                $item = [
                    'post_id' => $id,
                    'pau_num' => $num_aula, // O $i não pode ser usado, pois em alguns produtos o array vem com índices com saltos, porém crescentes.
                    'pau_data' => converter_para_yyyymmdd($data),
                    'pau_tem_arquivo' => $aulas_arquivo[$i] ? 1 : 0,
                    'pau_tem_video' => $aulas_video[$i] ? 1 : 0,
                    'pau_tem_caderno' => $aulas_caderno[$i] ? 1 : 0,
                    'pau_tem_resumo' => $aulas_resumo[$i] ? 1 : 0,
                    'pau_tem_mapa' => $aulas_mapa[$i] ? 1 : 0,
                ];
                
                $wpdb->insert('produtos_aulas', $item);
                
                $num_aula++;
            }
        }
        
    }
    
    
    public static function atualizar_metadados_basicos($id)
    {
        try {
            global $wpdb;
            
            KLoader::helper("ProdutoHelper");
            
            // recupera o produto
            $produto = new WC_Product($id);
            $busca = $produto->get_title();
            
            $concursos = self::listar_categorias($id, CATEGORIA_CONCURSO);
            foreach ($concursos as $item) {
                $busca .= " " . $item->name;
            }
            
            $professores = get_autores($id);
            foreach ($professores as $item) {
                $busca .= " " . $item->first_name . " " . $item->last_name;
            }
            
            $is_pacote = is_pacote($produto) ? 1 : 0;
            $is_destaque = is_area_destaque($id) ? 1 : 0;
            $is_simulado = is_produto_simulado($id) ? 1 : 0;
            $is_caderno = is_produto_caderno_questoes_online($id) ? 1 : 0;
            $is_mapa_mental = is_mapa_mental($id) ? 1 : 0;
            $is_visivel = $produto->get_catalog_visibility() == 'hidden' ? 0 : 1;
            $is_resumo = is_resumo_questoes_comentadas($id) ? 1 : 0;
            $is_audiobook = is_produto_audiobook($id) ? 1 : 0;
            $is_pdf = is_produto_pdf($id) ? 1 : 0;
            $is_video = is_produto_video($id) ? 1 : 0;
            $is_turma_coaching = is_produto_turma_coaching($id) ? 1 : 0;
            $is_pacote_curso = is_produto_pacote_curso($id) ? 1 : 0;
            $is_pacote_sq = is_produto_pacote_sq($id) ? 1 : 0;
            $is_premium = get_post_meta($id, PRODUTO_PREMIUM, true) == 'yes' ? 1 : 0;
            $is_assinatura_sq = is_produto_assinatura($id) ? 1 : 0;
            $is_com_correcao = is_com_correcao($id) ? 1 : 0;
            $is_coaching = is_produto_coaching($id) ? 1 : 0;
            
            $data_vendas_ate_DMY = get_data_vendas_ate($id);
            $data_disponivel_ate_DMY = get_data_disponivel_ate($id);
            
            $data_vendas_ate_YMD = $data_vendas_ate_DMY ? converter_para_yyyymmdd_HHiiss($data_vendas_ate_DMY . " 23:59:59") : NULL;
            $data_disponivel_ate_YMD = $data_disponivel_ate_DMY ?converter_para_yyyymmdd_HHiiss($data_disponivel_ate_DMY . " 23:59:59") : NULL;
            $tempo_acesso = get_post_meta($id, PRODUTO_TEMPO_DE_ACESSO, true);
            
            $forum_id = get_post_meta($id, 'forum_id', TRUE) ?: NULL;
            
            $preco_sem_desconto = $produto->get_regular_price();
            $preco = $produto->get_price();
            
            $aulas = get_post_meta($id, 'qtde_aulas', TRUE) ?: NULL;
            $esquemas = get_post_meta($id, 'qtde_esquemas', TRUE) ?: NULL;
            $questoes = get_post_meta($id, 'qtde_questoes', TRUE) ?: NULL;
            $carga_horaria = get_post_meta($id, 'carga_horaria', TRUE) ?: NULL;
            $botao_comprar = get_post_meta($id, 'botao_comprar', TRUE) ?: NULL;
            
            $upload_nao_realizado = ProdutoHelper::tem_upload_nao_realizado($id, AULA_UPLOAD_TIPO_PDF) ? 1 : 0;
            $upload_pendente = ProdutoHelper::tem_upload_pendente($id, AULA_UPLOAD_TIPO_PDF) ? 1 : 0;
            $upload_video_nao_realizado = ProdutoHelper::tem_upload_nao_realizado($id, AULA_UPLOAD_TIPO_VIDEO) ? 1 : 0;
            $upload_video_pendente = ProdutoHelper::tem_upload_pendente($id, AULA_UPLOAD_TIPO_VIDEO) ? 1 : 0;
            $upload_caderno_nao_realizado = ProdutoHelper::tem_upload_nao_realizado($id, AULA_UPLOAD_TIPO_CADERNO) ? 1 : 0;
            $upload_caderno_pendente = ProdutoHelper::tem_upload_pendente($id, AULA_UPLOAD_TIPO_CADERNO) ? 1 : 0;
            
            if($is_destaque) {
                $prioridade = 2;
            }
            elseif($is_pacote) {
                $prioridade = 1;
            }
            else {
                $prioridade = 0;
            }
            
            
            $dados = [
                'post_id' => $id,
                'pro_busca' => $busca,
                'pro_is_pacote' => $is_pacote,
                'pro_is_destaque' => $is_destaque,
                'pro_is_simulado' => $is_simulado,
                'pro_prioridade' => $prioridade,
                'pro_vendas_ate' => $data_vendas_ate_YMD,
                'pro_disponivel_ate' => $data_disponivel_ate_YMD,
                'pro_qtde_aulas' => $aulas,
                'pro_qtde_esquemas' => $esquemas,
                'pro_qtde_questoes' => $questoes,
                'pro_preco_sem_desconto' => $preco_sem_desconto,
                'pro_preco' => $preco,
                'pro_publicado_em' => $produto->post->post_date,
                'pro_is_caderno' => $is_caderno,
                'pro_is_visivel' => $is_visivel,
                'pro_carga_horaria' => $carga_horaria,
                'pro_botao_comprar' => $botao_comprar,
                'pro_forum_id' => $forum_id,
                'pro_upload_nao_realizado' => $upload_nao_realizado,
                'pro_upload_pendente' => $upload_pendente,
                'pro_upload_video_nao_realizado' => $upload_video_nao_realizado,
                'pro_upload_video_pendente' => $upload_video_pendente,
                'pro_upload_caderno_nao_realizado' => $upload_caderno_nao_realizado,
                'pro_upload_caderno_pendente' => $upload_caderno_pendente,
                'pro_is_mapa_mental' => $is_mapa_mental,
                'pro_is_audiobook' => $is_audiobook,
                'pro_is_resumo' => $is_resumo,
                'pro_is_pdf' => $is_pdf,
                'pro_is_video' => $is_video,
                'pro_is_turma_coaching' => $is_turma_coaching,
                'pro_is_pacote_curso' => $is_pacote_curso,
                'pro_is_pacote_sq' => $is_pacote_sq,
                'pro_is_premium' => $is_premium,
                'pro_is_assinatura_sq' => $is_assinatura_sq,
                'pro_com_correcao' => $is_com_correcao,
                'pro_is_coaching' => $is_coaching,
                'pro_tempo_acesso' => $tempo_acesso
            ];        
            
            $wpdb->replace('produtos', $dados);
        }
        catch(Exception $e) {
            log_wp("ERROR", "Produto ID {$id} está na base de metadados, porém não existe mais no WP");
        }
    }
    
    /**
     * Atualiza os metadados de concursos de produto
     *
     * @since K4
     *
     * @param int $id Id do produto
     */
    
    public static function atualizar_metadados_concursos($id, $atualizar_concurso = true)
    {
        global $wpdb;
        KLoader::model("CategoriaModel");
        KLoader::model("ConcursoModel");
        
        $concursos = self::listar_categorias($id, CATEGORIA_CONCURSO);
        
        $wpdb->delete('produtos_exames', ['post_id' => $id]);
        if($concursos) {
            foreach ($concursos as $item) {
                $concurso = [
                    'post_id' => $id,
                    'exa_id' => $item->term_id,
                ];
                
                $wpdb->insert('produtos_exames', $concurso);
                
                if($atualizar_concurso) {
                    $concurso = ConcursoModel::get_by_exame($item->term_id);
                    ConcursoModel::atualizar_metadados_qtde_produtos($concurso->con_id);
                }
            }
        }
    }
    
    /**
     * Atualiza os metadados de matérias de produto
     *
     * @since K4
     *
     * @param int $id Id do produto
     */
    
    public static function atualizar_metadados_materias($id)
    {
        global $wpdb;
        KLoader::model("CategoriaModel");
        
        $materias = self::listar_categorias($id, CATEGORIA_MATERIA);
        
        $wpdb->delete('produtos_materias', ['post_id' => $id]);
        if($materias) {
            foreach ($materias as $item) {
                $materia = [
                    'post_id' => $id,
                    'mat_id' => $item->term_id,
                ];
                
                $wpdb->insert('produtos_materias', $materia);
                
                CategoriaModel::atualizar_quantidade_cursos($item->term_id);
            }
        }
    }
    
    /**
     * Atualiza os metadados de professores de produto
     *
     * @since K4
     *
     * @param int $id Id do produto
     */
    
    public static function atualizar_metadados_professores($id, $atualizar_qtde_cursos = TRUE)
    {
        global $wpdb;
        KLoader::model("ColaboradorModel");
        
        $wpdb->delete('produtos_professores', ['post_id' => $id]);
        
        $autores_ids = get_post_meta($id, '_authors', true);
        $autores_percentuais = get_post_meta($id, '_percentuais', true);
        
        for($i = 0; $i < count($autores_ids); $i++) {
            
            $autores_percentuais[$i] = str_replace(",", ".", $autores_percentuais[$i]);
            
            $autor = [
                'post_id' => $id,
                'user_id' => $autores_ids[$i],
                'pru_percentual' => $autores_percentuais[$i]
            ];
            
            $wpdb->insert('produtos_professores', $autor);
            
            
            if($atualizar_qtde_cursos) {
                // atualizar qtde de cursos do professor
                if($autor['user_id'] > 0) { // ignora autores id -1 ou 0
                    //$qtde_cursos = contar_produtos_por_autor($autor['user_id']);
                    $qtde_cursos = self::contar_por_professor($autor['user_id']);
                    ColaboradorModel::atualizar_quantidade_cursos($autor['user_id'], $qtde_cursos);
                }
            }
        }
    }
    
    /**
     * Atualiza metadados de upload não realizado e upload pendente
     *
     * @since K4
     *
     * @param int $id Id do produto
     */
    
    public static function atualizar_metadados_status_aulas($id)
    {
        global $wpdb;
        
        KLoader::helper("ProdutoHelper");
        
        $produto = self::get_by_id($id);
        
        $upload_nao_realizado = ProdutoHelper::tem_upload_nao_realizado($id, AULA_UPLOAD_TIPO_PDF) ? 1 : 0;
        $upload_pendente = ProdutoHelper::tem_upload_pendente($id, AULA_UPLOAD_TIPO_PDF) ? 1 : 0;
        $upload_video_nao_realizado = ProdutoHelper::tem_upload_nao_realizado($id, AULA_UPLOAD_TIPO_VIDEO) ? 1 : 0;
        $upload_video_pendente = ProdutoHelper::tem_upload_pendente($id, AULA_UPLOAD_TIPO_VIDEO) ? 1 : 0;
        $upload_caderno_nao_realizado = ProdutoHelper::tem_upload_nao_realizado($id, AULA_UPLOAD_TIPO_CADERNO) ? 1 : 0;
        $upload_caderno_pendente = ProdutoHelper::tem_upload_pendente($id, AULA_UPLOAD_TIPO_CADERNO) ? 1 : 0;
        
        $dados = [];
        
        // verifica se o dado houve atualização nas informações
        if($produto->pro_upload_nao_realizado != $upload_nao_realizado) {
            $dados['pro_upload_nao_realizado'] = $upload_nao_realizado;
        }
        
        if($produto->pro_upload_pendente != $upload_pendente) {
            $dados['pro_upload_pendente'] = $upload_pendente;
        }
        
        if($produto->pro_upload_video_nao_realizado != $upload_video_nao_realizado) {
            $dados['pro_upload_video_nao_realizado'] = $upload_video_nao_realizado;
        }
        
        if($produto->pro_upload_video_pendente != $upload_video_pendente) {
            $dados['pro_upload_video_pendente'] = $upload_video_pendente;
        }
        
        if($produto->pro_upload_caderno_nao_realizado != $upload_caderno_nao_realizado) {
            $dados['pro_upload_caderno_nao_realizado'] = $upload_caderno_nao_realizado;
        }
        
        if($produto->pro_upload_caderno_pendente != $upload_caderno_pendente) {
            $dados['pro_upload_caderno_pendente'] = $upload_caderno_pendente;
        }
        
        // se houver nova informação, atualiza os dados
        if($dados) {
            $wpdb->update('produtos', $dados, ['post_id' => $id]);
        }
    }
    
    /**
     * Atualiza produtos relacionados
     *
     * @since K4
     *
     * @param int $id Id do produto
     */
    
    public static function atualizar_produtos_relacionados($id)
    {
        global $wpdb;
        
        $relacionados = get_produtos_relacionados($id, 4);
        
        $wpdb->delete('produtos_relacionados', ['post_id' => $id]);
        
        if($relacionados) {
            foreach ($relacionados as $item) {
                $relacionado = [
                    'post_id' => $id,
                    'rel_id' => $item->get_id()
                ];
                
                $wpdb->insert('produtos_relacionados', $relacionado);
            }
        }
    }
    
    /**
     * Recupera produto através do seu Id
     *
     * @since K5
     *
     * @param int $id Id do produto
     *
     * @return array Array associativo do produto
     */
    
    public static function get_by_id($id)
    {
        global $wpdb;
        
        $sql = "SELECT * FROM produtos pr
					JOIN wp_posts p ON pr.post_id = p.ID
				WHERE pr.post_id = {$id}";
        
        return $wpdb->get_row($sql);
    }
    
    /**
     * Recupera o Id do fórum associado a um determinado produto
     *
     * @since K5
     *
     * @param int $id Id do produto
     *
     * @return int Id do fórum
     */
    
    public static function get_forum_id($id)
    {
        global $wpdb;
        
        $sql = "SELECT pro_forum_id FROM produtos
				WHERE post_id = {$id}";
        
        return $wpdb->get_var($sql);
    }
    
    /**
     * Busca por produtos através de filtros
     *
     * @since K5
     *
     * @param array $filtros Filtros de busca
     *
     * @return array Lista de produtos
     */
    
    public static function buscar($filtros, $limit, $offset, $visivel = null)
    {
        $select = "*";
        if ($visivel) {
            return self::buscar_sql_builder($filtros, $select, $limit, $offset, NULL);
        }
        return self::buscar_sql_builder($filtros, $select, $limit, $offset);
    }
    
    /**
     * Conta produtos através de filtros
     *
     * @since K5
     *
     * @param array $filtros Filtros de busca
     *
     * @return array Lista de produtos
     */
    
    public static function buscar_contar($filtros, $visivel = null)
    {
        $select = " COUNT(*) as total ";
        if ($visivel) {
            $linhas = self::buscar_sql_builder($filtros, $select, NULL, NULL, NULL);
        }else{
            $linhas = self::buscar_sql_builder($filtros, $select);
        }
        
        return $linhas[0]->total;
    }
    
    /**
     * Buscar produtos por concurso
     *
     * @since K5
     *
     * @param boolean $contar Flag indicando se é para retornar um total ou todo o resultado
     * @param string $slug Slug do concurso
     *
     * @return array Lista de produtos | int Total de produtos
     */
    
    public static function buscar_por_concurso($contar, $slug, $simulados = null, $pacotes = null, $destaques = null, $gratuitos = null, $cadernos = null, $premium = null, $nome = null, $offset = null, $limite = null)
    {
        global $wpdb;
        
        $where_simulados = "";
        if(!is_null($simulados)) {
            $where_simulados = $simulados ? " AND pro_is_simulado = 1 " : " AND pro_is_simulado = 0 ";
        }
        
        $where_pacotes = "";
        if(!is_null($pacotes)) {
            $where_pacotes = $pacotes ? " AND (pro_is_pacote_curso = 1 OR pro_is_pacote_sq = 1) " : " AND (pro_is_pacote_curso = 0 AND pro_is_pacote_sq = 0) ";
        }
        
        $where_destaques = "";
        if(!is_null($destaques)) {
            $where_destaques = $destaques ? " AND pro_is_destaque = 1 " : " AND pro_is_destaque = 0 ";
        }
        
        $where_gratuitos = "";
        if(!is_null($gratuitos)) {
            $where_simulados = $gratuitos ? " AND pro_preco = 0 " : " AND pro_preco > 0 ";
        }
        
        $where_cadernos = "";
        if(!is_null($cadernos)) {
            $where_cadernos = $cadernos ? " AND pro_is_caderno = 1 " : " AND pro_is_caderno = 0 ";
        }
        
        $where_premium = "";
        if(!is_null($premium)) {
            $where_premium = $premium ? " AND pro_is_premium = 1 " : " AND pro_is_premium = 0 ";
        }

        $where_nome = "";
        if($nome){
            $where_nome = " AND po.post_title LIKE '%" . $nome . "%' ";
        }
        
        if($contar == TRUE){
            $select = "COUNT(*) as total ";
        }else{
            $select = "*";
            if($limite){
                $paginacao = " LIMIT {$limite} OFFSET {$offset} ";
            }
        }
        
        $sql = "SELECT {$select} FROM produtos pr
					JOIN produtos_exames pe ON pr.post_id = pe.post_id
					JOIN concursos c ON c.cat_id = pe.exa_id
					JOIN wp_posts po ON pr.post_id = po.id
				WHERE c.con_slug = '{$slug}'
					{$where_simulados}
					{$where_pacotes}
					{$where_destaques}
					{$where_gratuitos}
					{$where_cadernos} 
                    {$where_premium} 
                    {$where_nome} 
					AND (pr.pro_vendas_ate IS NULL OR pr.pro_vendas_ate >= CURDATE())
					AND po.post_status = 'publish'
					AND pr.pro_is_visivel = 1
                ORDER BY po.post_title
                {$paginacao} ";
					
		$result = $wpdb->get_results($sql);

		if($contar == TRUE) {
		    return $result[0]->total;
		} else {
		    return $result;
		}
    }
    
    /**
     * Buscar produtos por matéria
     *
     * @since K5
     *
     * @param boolean $contar Flag indicando se é para retornar um total ou todo o resultado
     * @param string $slug Slug do matéria
     *
     * @return array Lista de produtos | int Total de produtos
     */
    protected static function buscar_por_materia($contar, $slug, $nome = null, $offset = 0, $limite = LIMITE_POR_MATERIA)
    {
        global $wpdb;
        
        $where_nome = "";
        if($nome){
            $where_nome = " AND po.post_title LIKE '%" . $nome . "%' ";
        }
                
        $paginacao = "";

        if($contar == TRUE){
            $select = "COUNT(*) as total ";
        }else{
            $select = "*";
            if($limite){
                $paginacao = " LIMIT {$limite} OFFSET {$offset} ";
            }
        }
        
        $sql = "SELECT {$select} FROM produtos pr
					JOIN produtos_materias pm ON pr.post_id = pm.post_id
					JOIN materias m ON m.mat_id = pm.mat_id
					JOIN wp_posts po ON pr.post_id = po.id
				WHERE m.mat_slug = '{$slug}'
                    AND pro_is_simulado = 0 
					AND pro_is_pacote = 0
					AND (pr.pro_vendas_ate IS NULL OR pr.pro_vendas_ate >= CURDATE())
					AND po.post_status = 'publish'
					AND pr.pro_is_visivel = 1
                    {$where_nome}
                ORDER BY po.post_title
                {$paginacao}";
		
        $result = $wpdb->get_results($sql);
        
        //log_wp("INFO", "Materia SQL: ".$sql);
        if($contar == TRUE){
            //	log_wp("INFO", "Materia SQL: ".$result[0]->total);
            return $result[0]->total;
        }else{
            //	log_wp("INFO", "Materia SQL: ".count($result));
            return $result;
        }
					
    }
    
    /**
     * Buscar produtos por professor
     *
     * @since K5
     *
     * @param boolean $contar Flag indicando se é para retornar um total ou todo o resultado
     * @param int $id Id do professor
     *
     * @return array Lista de produtos | int Total de produtos
     *
     */
    
    public static function buscar_por_professor($contar, $professor_id, $nome = null, $order_by = 'post_title ASC', $offset = 0, $limit = NULL)
    {
        global $wpdb;
        
        $where_nome = "";
        if($nome){
            $where_nome = " AND po.post_title LIKE '%" . $nome . "%' ";
        }

        if($contar == TRUE) {
            $select = " COUNT(DISTINCT pr.post_id) as total ";
            $offset = "";
            $limit = "";
        } else {
            $select = " DISTINCT pr.*, po.*, pp.* ";
            $limit = $limit ? " LIMIT {$limit} ": "";
            $offset = $offset ? " OFFSET {$offset} ": "";
            $order = " ORDER BY " . $order_by;
        }
        
        $where_professor = $professor_id ? " AND pp.user_id = {$professor_id} " : "";
        
        $sql = "SELECT {$select} FROM produtos pr
					JOIN produtos_professores pp ON pr.post_id = pp.post_id
					JOIN wp_posts po ON pr.post_id = po.id
					JOIN produtos_materias pm ON pr.post_id = pm.post_id
				WHERE 1=1 
                    {$where_professor} 
					AND (pr.pro_vendas_ate IS NULL OR pr.pro_vendas_ate >= CURDATE())
					AND po.post_status = 'publish'
					AND pr.pro_is_pacote = 0
					AND pr.pro_is_visivel = 1
                    {$where_nome}
				{$order}
				{$limit}
                {$offset} ";

//        echo $sql;exit;
				
        $result = $wpdb->get_results($sql);
        
		return $contar ? $result[0]->total : $result;

    }
    
    /**
     * Conta produtos através de filtros
     *
     * @since K5
     *
     * @param array $filtros Filtros de busca
     *
     * @return array Lista de produtos
     */
    
    public static function buscar_somas($filtros)
    {
        $select = " SUM(pro_qtde_esquemas) as total_esquemas, SUM(pro_qtde_questoes) as total_questoes, SUM(pro_preco) as total_preco ";
        
        $linhas = self::buscar_sql_builder($filtros, $select);
        
        return $linhas[0];
    }
    
    
    /**
     * Monta o SQL dos métodos "buscar"
     *
     * @since K5
     *
     * @param array $filtros Filtros de busca
     * @param string|null $limit Limit
     * @param string|null $offset Offset
     *
     * @return array Lista de produtos
     */
    
    private static function buscar_sql_builder($filtros, $select = "*", $limit = null, $offset = null)
    {
        KLoader::helper("ProdutoHelper");
        
        global $wpdb;
        
        $join_disciplinas = $filtros['disciplinas'] ? " JOIN produtos_materias pm ON pm.post_id = pr.post_id" : "";
        $join_professores = $filtros['professores'] || $filtros['coordenador'] ? " JOIN produtos_professores pp ON pp.post_id = pr.post_id" : "";
        $join_concursos = ( $filtros['concursos'] || $filtros['bancas'] ) ? " JOIN produtos_exames pe ON pe.post_id = pr.post_id JOIN concursos c ON pe.exa_id = c.cat_id" : "";
        $join_sem_cronograma = $filtros['sem_cronograma'] ? " LEFT JOIN produtos_aulas pa ON pa.post_id = pr.post_id AND pa.pau_num = 1 " :  "";
        
        $where_expirados = $filtros['expirados'] ? "" : " AND (pro_vendas_ate IS NULL OR pro_vendas_ate >= '" . date("Y-m-d") . "') ";
        $where_disciplinas = $filtros['disciplinas'] ? " AND mat_id IN (" . implode(",", $filtros['disciplinas']) . ")" : "";
        $where_professores = str_replace('-1', '0', $filtros['professores'] ? " AND user_id IN (" . implode(",", $filtros['professores']) . ")" : "");
        $where_concursos = $filtros['concursos'] ? " AND con_id IN (" . implode(",", $filtros['concursos']) . ")" : "";
        $where_bancas = $filtros['bancas'] ? " AND con_banca IN ('" . implode("','", $filtros['bancas']) . "')" : "";
        $where_areas = $filtros['areas'] ? " AND pr.post_id IN (SELECT post_id FROM produtos_areas WHERE area_id IN (" . implode(",", $filtros['areas']) . ")) " : "";
        $where_status = $filtros['status'] ? " AND post_status IN ( '" . implode("','", $filtros['status']) . "' )" : "";
        
        $where_excluir_simulados = $filtros['excluir_simulados'] ? " AND pro_is_simulado = 0 " : "";
        $where_somente_simulados = $filtros['somente_simulados'] ? " AND pro_is_simulado = 1 AND (pr.post_id IN (SELECT prd_id FROM exponenc_corp.simulados WHERE sim_status = 1) OR pro_is_pacote_sq OR pro_is_pacote_curso) " : "";
        $where_excluir_pacotes = $filtros['excluir_pacotes'] ? " AND pro_is_pacote = 0 " : "";
        $where_excluir_cadernos = $filtros['excluir_cadernos'] ? " AND pro_is_caderno = 0 " : "";
        $where_excluir_gratuitos = $filtros['excluir_gratuitos'] ? " AND pro_preco > 0 " : "";
        $where_somente_gratuitos = $filtros['somente_gratuitos'] ? " AND pro_preco = 0 " : "";
        $where_mapas = $filtros['excluir_mapas'] ? " AND pro_is_mapa_mental = 0 " : "";
        $where_disponivel_ate = $filtros['disponivel_ate'] ? " AND pro_disponivel_ate <= '{$filtros['disponivel_ate']}' " : "";
        $where_coordenador = $filtros['coordenador'] ? "AND ( pr.post_id IN (SELECT post_id FROM produtos_areas pa JOIN coordenadores_areas ca ON pa.area_id = ca.area_id WHERE user_id = {$filtros['coordenador']} AND data_fim IS NULL) || user_id = {$filtros['coordenador']} ) " : "";
        $where_titulo = $filtros['titulo'] ? " AND post_title LIKE '%{$filtros['titulo']}%' " : "";
        $where_sem_cronograma = $filtros['sem_cronograma'] ? " AND (pa.pau_data IS NULL || pa.pau_data = '') " : "";
        $where_incluir_audiobooks = $filtros['incluir_audiobooks'] ? " AND pro_is_audiobook = 1 " : "";
        $where_incluir_pacotes = $filtros['incluir_pacotes'] ? " AND pro_is_pacote = 1 " : "";
        $where_incluir_mapas_mentais = $filtros['incluir_mapas_mentais'] ? " AND pro_is_mapa_mental = 1 " : "";
        $where_incluir_resumos = $filtros['incluir_resumo'] ? " AND pro_is_resumo = 1 " : "";
        $where_incluir_cadernos = $filtros['incluir_cadernos'] ? " AND pro_is_caderno = 1 " : "";
        $where_incluir_pdfs = $filtros['incluir_pdfs'] ? " AND pro_is_pdf = 1 " : "";
        $where_incluir_videos = $filtros['incluir_videos'] ? " AND pro_is_video = 1 " : "";
        $where_incluir_turma_coaching = $filtros['incluir_turma_coaching'] ? " AND pro_is_turma_coaching = 1 " : "";
        $where_somente_visiveis = $filtros['somente_visiveis'] ? " AND pro_is_visivel = 1 " :  "";
        $where_excluir_pacotes_cat = $filtros['excluir_pacotes_cat'] ? " AND (pro_is_pacote_curso = 0 AND pro_is_pacote_sq = 0) " : "";
        $where_incluir_pacotes_cat = $filtros['incluir_pacotes_cat'] ? " AND (pro_is_pacote_curso = 1 OR pro_is_pacote_sq = 1) " : "";
        
        $where_prazos_alerta = "";
        if($filtros['prazos_alerta']) {
            $campo_prazos_alerta = ProdutoHelper::get_nome_campo_tem_conteudo_aula($filtros['tipo']);
            $where_prazos_alerta =  " AND pr.post_id IN (SELECT post_id from produtos_aulas where {$campo_prazos_alerta} = 0 AND pau_data BETWEEN '". date("Y-m-d") . "' AND '" . date("Y-m-d", strtotime("+7 days")) . "') ";
        }
        
        $where_unr = "";
        if($filtros['somente_upload_nao_realizado']) {
            $campo_unr = ProdutoHelper::get_nome_campo_upload_nao_realizado($filtros['tipo']);
            $where_unr = " AND {$campo_unr} = 1 ";
        }
        
        $where_up = "";
        if($filtros['somente_upload_pendente']) {
            $campo_up = ProdutoHelper::get_nome_campo_upload_pendente($filtros['tipo']);
            $where_up = " AND {$campo_up} = 1 ";
        }
        
        $where_busca = "";
        if($filtros['busca']) {
            $where_busca = PesquisaHelper::busca_binaria($filtros['busca'], "pro_busca");
        }
        
        if($limit) {
            $order_by = $filtros["order_by"] ?: "po.post_title ASC";
            
            $limit_offset = $limit ? " ORDER BY {$order_by} LIMIT {$offset}, {$limit} " : "";
        }
        
        $sql = "SELECT {$select} FROM produtos pr
					JOIN wp_posts po ON pr.post_id = po.id
					{$join_disciplinas}
					{$join_concursos}
					{$join_professores}
                    {$join_sem_cronograma}
				WHERE 1=1
					{$where_expirados} {$where_disciplinas} {$where_professores} {$where_concursos} {$where_bancas} {$where_areas}
                    {$where_status} {$where_unr} {$where_up} {$where_excluir_simulados} {$where_somente_simulados} {$where_busca}
                    {$where_excluir_pacotes} {$where_excluir_cadernos} {$where_excluir_gratuitos} {$where_somente_gratuitos} {$where_mapas}
					{$where_disponivel_ate} {$where_coordenador} {$where_titulo} {$where_sem_cronograma} {$where_prazos_alerta}
                    {$where_incluir_audiobooks} {$where_incluir_pacotes} {$where_incluir_mapas_mentais} {$where_incluir_resumos} {$where_incluir_cadernos}
                    {$where_incluir_pdfs} {$where_incluir_videos} {$where_incluir_turma_coaching} {$where_somente_visiveis}
                    {$where_excluir_pacotes_cat} {$where_incluir_pacotes_cat}
				{$limit_offset}";
		return $wpdb->get_results($sql);
    }
    
    /**
     * Contar produtos por concurso
     *
     * @since K5
     *
     * @param string $slug Slug do concurso
     *
     * @return int Total de produtos
     */
    
    public static function contar_por_concurso($slug, $simulados = null, $pacotes = null, $destaques = null, $gratuitos = null, $cadernos = null, $premium = null, $nome = null)
    {
        return self::buscar_por_concurso(TRUE, $slug, $simulados, $pacotes, $destaques, $gratuitos, $cadernos, $premium, $nome);
    }
    
    /**
     * Contar produtos por matéria
     *
     * @since K5
     *
     * @param string $slug Slug do matéria
     *
     * @return int Total de produtos
     */
    
    public static function contar_por_materia($slug, $nome = null)
    {
        return self::buscar_por_materia(TRUE, $slug, $nome);
    }
    
    /**
     * Contar produtos por professor
     *
     * @since K5
     *
     * @param int $professor_id Id do professor
     *
     * @return int Total de produtos
     */
    
    public static function contar_por_professor($professor_id, $nome = null)
    {
        return self::buscar_por_professor(TRUE, $professor_id, $nome);
    }
    
    /**
     * Recupera as regras de um produto premium
     *
     * @since K4
     *
     * @param int $produto_id Id do produto
     *
     * @return string HTML do preço formatado
     */
    
    public static function get_premium_grupos_regras($produto_id)
    {
        if(self::is_produto_premium($produto_id)) {
            $regras = [];
            $regras_db = get_post_meta($produto_id, PRODUTO_PREMIUM_REGRAS, true);
            
            foreach ($regras_db as $item) {
                
                $regras[$item['grupo']][] = [
                    'tipo' => $item['tipo'],
                    'operador' => $item['operador'],
                    'valores_produtos' => $item['valores_produtos'],
                    'valores_categorias' => $item['valores_categorias']
                ];
                
            }
            
            return $regras;
        }
        
        return null;
    }
    
    /**
     * Recupera a data mais próxima de um concurso associado o produto
     *
     * @since K5
     *
     * @param int $id Id do produto
     *
     * @return string Data da próxima prova no formato yyyy-mm-dd
     */
    
    public static function get_proxima_data_prova($id)
    {
        global $wpdb;
        
        $sql = "SELECT min(con_data_prova) FROM produtos_exames pe
					JOIN concursos c ON pe.exa_id = c.cat_id
				WHERE pe.post_id = {$id} GROUP BY pe.post_id";
        
        return $wpdb->get_var($sql);
    }

    public static function get_bancas($id)
    {
        global $wpdb;

        $sql = "SELECT GROUP_CONCAT(DISTINCT c.con_banca ORDER BY c.con_banca ASC SEPARATOR ', ' )
                FROM produtos_exames pe
                    JOIN concursos c ON pe.exa_id = c.cat_id
                WHERE pe.post_id = {$id}";
        
        return $wpdb->get_var($sql);
    }
    
    /**
     * verifica se um produto é premium
     *
     * @since L1
     *
     * @param int $produto_id Id do produto
     *
     * @return bool
     */
    
    public static function is_produto_premium($produto_id)
    {
        return get_post_meta($produto_id, PRODUTO_PREMIUM, true) == 'yes';
    }
    
    
    /**
     * Lista professores de um produto
     *
     * @since K4
     *
     * @param int $id Id do produto
     *
     * @return array Lista de professores
     */
    
    public static function listar_professores($id)
    {
        global $wpdb;
        
        $sql = "SELECT * FROM produtos_professores pc
					JOIN wp_users u ON pc.user_id = u.ID
					JOIN colaboradores c ON pc.user_id = c.user_id
				WHERE pc.post_id = {$id}";
        
        return $wpdb->get_results($sql);
    }
    
    /**
     * Lista produtos para montagem de gtm
     *
     * @since K4
     *
     * @return array Lista de produtos
     */
    
    public static function listar_para_gtm()
    {
        global $wpdb;
        
        $sql = "SELECT post_id, pro_preco FROM produtos WHERE pro_is_visivel = 1 ";
        
        return $wpdb->get_results($sql);
    }
    
    /**
     * Buider da listagem de items de um produto premium
     *
     * @since L1
     *
     * @param int $produto_id Id do produto
     * @param int $pedido_id Id do pedido
     * @param array $filtros Filtros da busca
     *
     * @return string SQL da busca
     */
    
    private static function listar_premium_items_builder($produto_id, $pedido_id, $filtros, $contar = false)
    {
        $where_premium_or_a = [];
        
        $grupos = self::get_premium_grupos_regras($produto_id);
        foreach ($grupos as $regras) {
            
            // nível AND
            $where_premium_and_a = [];
            
            foreach ($regras as $regra) {
                
                $operador = $regra['operador'] == PRODUTO_PREMIUM_OPERADOR_E ? " IN " : " NOT IN ";
                
                switch ($regra['tipo']) {
                    
                    case PRODUTO_PREMIUM_TIPO_SEMPRE : {
                        continue 2;
                    }
                    
                    case PRODUTO_PREMIUM_TIPO_PRODUTO : {
                        if($regra['valores_produtos']) {
                            $where_premium_and_a[] = " po.ID {$operador} (" . implode(" , ", $regra['valores_produtos']) . ") ";
                        }
                        break;
                    }
                    
                    case PRODUTO_PREMIUM_TIPO_CATEGORIA : {
                        $i = $i ? $i+1 : 1;
                        if($regra['valores_categorias']) {
//                             $where_premium_and_a[] = " tt.term_id {$operador} (" . implode(" , ", $regra['valores_categorias']) . ") ";
//                             $tem_join_categorias = true;
                            $where_premium_and_a[] = " po.ID {$operador} (SELECT object_id FROM wp_term_relationships tr{$i} 
                                JOIN wp_term_taxonomy tt{$i} ON tr{$i}.term_taxonomy_id = tt{$i}.term_taxonomy_id
                                WHERE tt{$i}.term_id IN (" . implode(" , ", $regra['valores_categorias']) . ")) ";
                        }
                        break;
                    }
                }
                
            }
            
            if($where_premium_and_a) {
                $where_premium_or_a[] = " (" . implode(" AND ", $where_premium_and_a) . " )";
            }
 
        }
        
        $join_materias = $filtros['materias'] ? " JOIN produtos_materias pm ON pm.post_id = p.post_id" : "";
        $join_professores = $filtros['professores'] || $filtros['coordenador'] ? " JOIN produtos_professores pp ON pp.post_id = p.post_id" : "";
        $join_concursos = $filtros['concursos'] ? " JOIN produtos_exames pe ON pe.post_id = p.post_id JOIN concursos c ON pe.exa_id = c.cat_id" : "";

        if($where_premium_or_a) {
            $where_premium_or = " AND ( " . implode(" OR ", $where_premium_or_a) . " ) ";
        }
        else {
            $where_premium_or = "";
        }
        
        $where_texto = $filtros['texto'] ? " AND post_title LIKE '%{$filtros['texto']}%' " : "";
        $where_materias = $filtros['materias'] ? " AND mat_id IN (" . implode(",", $filtros['materias']) . ")" : "";
        $where_professores = str_replace('-1', '0', $filtros['professores'] ? " AND user_id IN (" . implode(",", $filtros['professores']) . ")" : "");
        $where_concursos = $filtros['concursos'] ? " AND con_id IN (" . implode(",", $filtros['concursos']) . ")" : "";
        $where_pacotes = $filtros['pacotes'] ? " AND pro_is_pacote IN (" . implode(",", $filtros['pacotes']) . ")" : " ";
        $where_cursos = $filtros['tipos'] ? " AND p.post_id IN (SELECT object_id FROM wp_term_relationships str 
                JOIN wp_term_taxonomy stt ON str.term_taxonomy_id = stt.term_taxonomy_id
                WHERE stt.term_id IN (" . implode(",", $filtros['tipos']) . "))" : " ";
        $where_excluir_expirados = " AND (pro_disponivel_ate IS NULL OR pro_disponivel_ate >= '" . date("Y-m-d") . "') ";
        $where_excluir_simulados = $filtros['excluir_simulados'] ? " AND pro_is_simulado = 0 " : "";
        $where_excluir_cadernos = $filtros['excluir_cadernos'] ? " AND pro_is_caderno = 0 " : "";
        $where_post_status = " AND po.post_status = '" . STATUS_POST_PUBLICADO ."' ";
        
        if($contar) {
            $order_by_limit = "";  
        }
        else {
            $order_by = " po.post_title ASC ";
            switch($filtros["ordem"]) {
                case PESQUISA_ORDEM_A_Z: $order_by = " po.post_title ASC "; break;
                case PESQUISA_ORDEM_Z_A: $order_by = " po.post_title DESC "; break;
                case PESQUISA_MAIS_NOVOS: $order_by = " p.post_id DESC "; break;
            }
            
            $limit = isset($filtros["offset"]) ? " LIMIT {$filtros['offset']}, {$filtros['limit']} " : "";
//             $fixar = $filtros["fixar"] ? " acesso desc, " : "";
            
            $order_by_limit = " ORDER BY fixado DESC, {$order_by} {$limit} ";  
        }
        
        $sql = "SELECT DISTINCT p.*, po.*, 
                    CASE WHEN pl.prl_data IS NULL THEN 0 ELSE 1 END AS acesso,
                    CASE WHEN pf.pro_id IS NULL THEN 0 ELSE 1 END AS fixado
                 FROM produtos p
                    {$join_materias} {$join_professores} {$join_concursos}
                    JOIN wp_posts po ON p.post_id = po.ID
                    LEFT JOIN premium_log pl ON pl.produto_id = {$produto_id} AND pl.pedido_id = {$pedido_id} AND pl.item_id = p.post_id
                    LEFT JOIN premium_itens_fixados pf ON pf.pro_id = {$produto_id} AND pf.ped_id = {$pedido_id} AND pf.ite_id = p.post_id
                 WHERE pro_is_pacote = 0 AND pro_is_premium = 0 
                    {$where_post_status} {$where_premium_or}
                    {$where_texto} {$where_materias} {$where_professores} {$where_concursos} {$where_pacotes} {$where_cursos} 
                    {$where_excluir_expirados} {$where_excluir_simulados} {$where_excluir_cadernos} 
                 {$order_by_limit}";
        
//                  AND pro_com_correcao = 0 AND pro_is_turma_coaching = 0
//                  AND pro_is_simulado = 0 AND pro_is_caderno = 0 AND pro_is_coaching = 0
        if($_GET["k"]) {
            echo $sql; exit;
        }
        return $sql;
    }
    
    /**
     * Lista items de um produto premium
     *
     * @since L1
     *
     * @param int $produto_id Id do produto
     * @param int $pedido_id Id do pedido
     * @param array $filtros Filtros da busca
     *
     * @return array Lista de produtos
     */
    
    public static function listar_premium_items($produto_id, $pedido_id, $filtros)
    {
        global $wpdb;
        
        $sql = self::listar_premium_items_builder($produto_id, $pedido_id, $filtros, false);
        
        return $wpdb->get_results($sql);
    }
    
    /**
     * Conta items de um produto premium
     *
     * @since L1
     *
     * @param int $produto_id Id do produto
     * @param int $pedido_id Id do pedido
     * @param array $filtros Filtros da busca
     *
     * @return int Número de itens
     */
    
    public static function contar_premium_items($produto_id, $pedido_id, $filtros)
    {
        global $wpdb;
        
        $sql = self::listar_premium_items_builder($produto_id, $pedido_id, $filtros, true);
        $sql = "SELECT COUNT(*) as num FROM ( $sql ) tmp";
        
        return $wpdb->get_var($sql);
    }
    
    
    /**
     * Lista produtos por concurso
     *
     * @since K4
     *
     * @param string $slug Slug do concurso
     *
     * @return array Lista de produtos
     */
    
    public static function listar_por_concurso($slug, $simulados = null, $pacotes = null, $destaques = null, $gratuitos = null, $cadernos = null, $premium = null, $nome = null, $offset = null, $limite = null)
    {
        return self::buscar_por_concurso(FALSE, $slug, $simulados, $pacotes, $destaques, $gratuitos, $cadernos, $premium, $nome, $offset, $limite);
    }
    
    /**
     * Lista produtos por matéria
     *
     * @since K4
     *
     * @param string $slug Slug do matéria
     *
     * @return array Lista de produtos
     */
    
    public static function listar_por_materia($slug, $nome = null, $offset = 0, $limite = LIMITE_POR_MATERIA)
    {
        return self::buscar_por_materia(FALSE, $slug, $nome, $offset, $limite);
    }
    
    /**
     * Lista produtos por professor
     *
     * @since K4
     *
     * @param int $id Id do professor
     */
    
    public static function listar_por_professor($professor_id, $nome = null, $order_by = 'post_title ASC', $offset = 0, $limit = NULL)
    {
        return self::buscar_por_professor(FALSE, $professor_id, $nome, $order_by, $offset, $limit);
    }
    
    /**
     * Lista produtos relacionados
     *
     * @since K4
     *
     * @param int $id Id do produto
     */
    
    public static function listar_relacionados($id)
    {
        global $wpdb;
        
        $sql = "SELECT * FROM produtos pro
					JOIN produtos_relacionados prr ON pro.post_id = prr.rel_id
					JOIN wp_posts po ON pro.post_id = po.id
				WHERE prr.post_id = {$id}
				AND pro.pro_is_visivel = 1 ";
        
        return $wpdb->get_results($sql);
    }
    
    /**
     * Verifica se usuário tem permissão para baixar determinado produto
     *
     * @since K5
     *
     * @param int $usuario_id Id do usuário
     * @param int $produto_id Id do produto
     *
     * @return bool
     */
    
    public static function tem_permissao($usuario_id, $produto_id)
    {
        global $wpdb;
        
        $agora = date("Y-m-d H:i:s");
        
        $sql = "SELECT * FROM wp_woocommerce_downloadable_product_permissions
				WHERE user_id = {$usuario_id}
				AND product_id = {$produto_id}
                AND (downloads_remaining = '' OR downloads_remaining > 0)
                AND (access_expires IS NULL OR access_expires <= '{$agora}') ";
        
        return $wpdb->get_results($sql) ? true : false;
    }
       
    /**
     * Recupera a expiração do produto de um pedido
     *
     * @since L1
     *
     * @param int $produto_id Id do produto.
     * @param int $pedido_id Id do pedido.
     *
     * @return array
     */
    
    public static function get_produto_expiracao($produto_id, $pedido_id)
    {
        global $wpdb;
        
        $sql = "SELECT * FROM produtos_expiracao WHERE pedido_id = {$pedido_id} AND produto_id = {$produto_id}";
        
        return $wpdb->get_row($sql);
    }
    
    
    /**
     * Recupera a expiração do produto de um pedido
     *
     * @since L1
     *
     * @param int $produto_id Id do produto.
     * @param int $pedido_id Id do pedido.
     *
     * @return array
     */
    
    public static function salvar_produto_expiracao($produto_id, $pedido_id, $data)
    {
        global $wpdb;
        
        $dados = [
            'pedido_id' => $pedido_id,
            'produto_id' => $produto_id,
            'data_expiracao' => $data
        ];
        
        $wpdb->insert("produtos_expiracao", $dados);
    }
    
    /**
     * Listas as disciplinas de um produto
     *
     * @since L1
     *
     * @param int $produto_id Id do produto.
     *
     * @return array
     */
    
    public static function listar_materias($produto_id)
    {
        global $wpdb;
        
        $sql = "SELECT * FROM wp_terms t
                JOIN produtos_materias pm ON pm.mat_id = t.term_id
                JOIN materias m ON m.mat_id = pm.mat_id
                WHERE pm.post_id = {$produto_id}";

        return $wpdb->get_results($sql);
    }
    
    /**
     * Lista as áreas de um produto
     *
     * @since L1
     *
     * @param int $produto_id Id do produto.
     *
     * @return array
     */
    
    public static function listar_areas($produto_id)
    {
        global $wpdb;
        
        $sql = "SELECT * FROM wp_terms
                WHERE term_id IN (SELECT area_id FROM produtos_areas WHERE post_id = {$produto_id})";

        return $wpdb->get_results($sql);
    }
    
    /**
     * Lista os concursos de um produto
     *
     * @since L1
     *
     * @param int $produto_id Id do produto.
     *
     * @return array
     */
    
    public static function listar_concursos($produto_id)
    {
        global $wpdb;
        
        $sql = "SELECT * FROM wp_posts p
                JOIN concursos c ON c.con_id = p.ID
                WHERE c.cat_id IN (SELECT exa_id FROM produtos_exames WHERE post_id = {$produto_id})";
        
        return $wpdb->get_results($sql);
    }
    
    /**
     * Lista produtos e o nome de suas áreas concatenados
     *
     * @since L1
     *
     * @return array Id do produto e nome das áreas concatenados
     */
    
    public static function listar_produtos_areas_str()
    {
        global $wpdb;
        
        $sql = "SELECT post_id, GROUP_CONCAT(name SEPARATOR ', ') AS areas_str FROM wp_terms t JOIN produtos_areas pa ON pa.area_id = t.term_id GROUP BY post_id";
        
        return $wpdb->get_results($sql);
    }
    
    /**
     * Lista produtos e o nome de seus concursos concatenados
     *
     * @since L1
     *
     * @return array Id do produto e nome dos concursos concatenados
     */
    
    public static function listar_produtos_concursos_str()
    {
        global $wpdb;
        
        $sql = "SELECT pe.post_id, GROUP_CONCAT(p.post_title SEPARATOR ', ') AS concursos_str FROM wp_posts p
				JOIN concursos c ON c.con_id = p.ID
				JOIN produtos_exames pe WHERE pe.exa_id = c.cat_id
				GROUP BY pe.post_id";
        
        return $wpdb->get_results($sql);
    }
    
    /**
     * Recupera o percentual de um professor em um produto
     *
     * @since L1
     *
     * @param int $produto_id Id do produto.
     * @param int $professor_id Id do professor.
     *
     * @return array
     */
    
    public static function get_percentual_professor($produto_id, $professor_id)
    {
        global $wpdb;
        
        $sql = "SELECT * FROM produtos_professores pf
                    JOIN produtos p ON p.post_id = pf.post_id
                WHERE pf.post_id = {$produto_id} AND pf.user_id = {$professor_id}";
        echo $sql;
        return $wpdb->get_row($sql);
    }
    
    /**
     * Retorna se um produto possui alguma aula disponível para um determinado tipo de aula
     * 
     * @since L1
     * 
     * @param int $produto_id ID do produto
     * @param int $tipo_aula Tipo da aula
     * 
     * @return int número de aulas disponíveis para esse tipo de aula
     */
    public static function tem_tipo_aula_disponivel($produto_id, $tipo_aula)
    {
        global $wpdb;

        KLoader::helper("ProdutoHelper");

        $campo_aula = ProdutoHelper::get_nome_campo_tem_conteudo_aula($tipo_aula);

        if(!$campo_aula){
            return false;
        }

        $hoje = hoje_yyyymmdd();

        $sql = "SELECT COUNT(*) FROM produtos_aulas WHERE post_id = {$produto_id} AND pau_data <= '{$hoje}' AND {$campo_aula} = 1";

        return $wpdb->get_var($sql);
    }

    public function listarProximos($ultimoId = 0)
    {
        global $wpdb;

        $sql = "SELECT * FROM produtos JOIN wp_posts ON ID = post_id WHERE post_id > {$ultimoId} AND post_status = 'publish' LIMIT 100";

        return $wpdb->get_results($sql);

//        $produtos = [];
//        if($resultado) {
//            foreach ($resultado as $item) {
//                $produtos;
//            }
//        }
//        return $produtos;
    }
}
