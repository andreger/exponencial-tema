<?php 
/**
 * Model para organizar rotinas relacionados aos colaboradores
 * 
 * @package	KCore/Models
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K4
 */ 

KLoader::model("UsuarioModel");

class ColaboradorModel extends UsuarioModel {

	/**
	 * Atualiza a quantidade de cursos por professor
	 * 
	 * @since K5
	 * 
	 * @param int $user_id Id do usuário professor
	 * @param int $qtde_cursos Quantidade de cursos do professor
	 */
	public static function atualizar_quantidade_cursos($user_id, $qtde_cursos)
	{	
		global $wpdb;
		$wpdb->update('colaboradores', ['col_qtde_cursos' => $qtde_cursos], ['user_id' => $user_id]);
	}
	/**
	 * Recupera um colaborador através do seu id
	 * 
	 * @since K5
	 * 
	 * @param int $id Id do colaborador.
	 *
	 * @return array
	 */ 

	public static function get_by_id($id)
	{	
		global $wpdb;

		$sql = "SELECT * FROM colaboradores c
					JOIN wp_users u ON c.user_id = u.ID 
				WHERE c.user_id = {$id}";

		return $wpdb->get_row($sql);
	}

	/**
	 * Recupera um colaborador através do seu slug
	 * 
	 * @since K4
	 * 
	 * @param string $slug Slug do colaborador.
	 *
	 * @return array
	 */ 

	public static function get_by_slug($slug)
	{	
		global $wpdb;

		$sql = "SELECT * FROM colaboradores c
					JOIN wp_users u ON c.user_id = u.ID 
				WHERE col_slug = '{$slug}'";

		return $wpdb->get_row($sql);
	}

	/**
	 * Recupera um colaborador através do seu slug histórico
	 * 
	 * @since K5
	 * 
	 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2543
	 * 
	 * @param string $slug Slug histórico do colaborador.
	 *
	 * @return array
	 */ 

	public static function get_by_slug_historico($slug_historico)
	{	
		global $wpdb;

		$sql = "SELECT c.*, u.* FROM colaboradores c
					JOIN colaboradores_slugs_historico h ON c.user_id = h.user_id
					JOIN wp_users u ON c.user_id = u.ID 
				WHERE h.col_slug = '{$slug_historico}'";

		return $wpdb->get_row($sql);
	}

	/**
	 * Lista todos os colaboradores de tipos especificados
	 * 1 - Professor
	 * 2 - Consultor
	 * 3 - Professor e Consultor
	 * 
	 * @since K4
	 * 
	 * @global $wpdb
	 * 
	 * @param int|array $tipos Tipo ou lista de tipos de colaboradores
	 *
	 * @return array Lista de colaboradores
	 */ 

	public static function listar_por_tipo($tipos = NULL, $select = "*", $where = NULL, $order_by = "display_name")
	{	
		global $wpdb;
		
		if(!is_null($tipos) && !is_array($tipos)) {
		    $tipos = [$tipos];
		}

		$where_tipos = $tipos ? " AND col_tipo IN (" . implode(",", $tipos) . ") " : "";
		$where = $where ? " AND {$where} " : "";

		$sql = "SELECT {$select} FROM colaboradores c 
					JOIN wp_users u ON u.ID = c.user_id 
				WHERE 1=1 {$where_tipos} {$where} 
				ORDER BY {$order_by}";

		return $wpdb->get_results($sql);
	}

	/**
	 * Salva ou atualiza um histórico de slug de um colaborador
	 * 
	 * @since K5
	 * 
	 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2543
	 * 
	 * @global $wpdb
	 * 
	 * @param string $slug Slug do colaborador
	 * @param int $user_id Id do colaborador
	 */

	public static function salvar_slug_historico($slug, $user_id)
	{
		global $wpdb;

		if(!$slug || !$user_id) {
			throw new InvalidArgumentException();
		}

		$wpdb->replace('colaboradores_slugs_historico', 
			['col_slug' => $slug, 'user_id' => $user_id]
		);
	}

    public static function excluir($id)
    {
        global $wpdb;

        $sql = "DELETE FROM colaboradores WHERE user_id = {$id}";

        $wpdb->query($sql);
    }

    public function listarWpProdutosComAutor($autorId)
    {
        global $wpdb;

        $sql = "SELECT post_id FROM exponenc_db.wp_postmeta where meta_key = '_authors' and meta_value like '%{$autorId}%'";

        $ids = [];
        if($resultado = $wpdb->get_results($sql)) {
            foreach ($resultado as $linha) {
                $ids[] = $linha->post_id;
            }
        }

        return $ids;
    }
}