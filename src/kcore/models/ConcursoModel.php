<?php 
/**
 * Model para organizar rotinas relacionadas aos Concursos
 * 
 * @package	KCore/Models
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K4
 */ 

KLoader::model("PostModel");
KLoader::model("ExameModel");

class ConcursoModel extends PostModel {

	/**
	 * Atualiza os metadados de um concurso
	 * 
	 * @since K4
	 * 
	 * @param string $url URL de cursos por concurso específico
	 *
	 * @return object WP_Post
	 */ 

	public static function atualizar_metadados($post_id)
	{
		global $wpdb;

		// recupera o post do wp
		$post = PostModel::get_by_id($post_id);

		// recupera metadados para montagem dos dados do concurso	
		$status = get_post_meta($post_id, 'status_do_concurso', true);
		$data_prova = get_post_meta($post_id, 'data_da_prova', true) ?: null;
		$thumb = get_the_post_thumbnail_url($post_id);
		$banca = get_post_meta( $post_id, 'banca', true );

		
		// montagem dos dados a serem gravados na tabela concursos
        $dados = [
        	'con_id' => $post_id,
            'con_slug' => $post->post_name,
            'con_status' => $status,
            'con_thumb' => $thumb,
            'cat_id' => get_post_meta($post_id, 'categoria_associada_id', true),
			'con_data_prova' => $data_prova,
			'con_banca' => $banca
        ];

     	// verifica se é inserção ou update
		if(self::get_by_id($post_id)) {
			// se já existe concurso, atualiza tabela concursos
			$wpdb->update('concursos', $dados, ['con_id' => $post_id]);
		}
		else {
			// senão cria novo registro na tabela concursos
			$wpdb->insert('concursos', $dados);
		}

        // atualiza vínculos entre produtos e exames	       
	    self::atualizar_metadados_produtos_exames($post_id);
	    self::atualizar_metadados_qtde_produtos($post_id);
	}

	/**
	 * Atualiza os metadados da relação produto x exame
	 * 
	 * @since K4
	 * 
	 * @param int $concurso_id Id do concurso
	 */ 

	public static function atualizar_metadados_produtos_exames($concurso_id)
	{
	    KLoader::model("ProdutoModel");
	    
		global $wpdb;

		// recupera concurso
		$concurso = self::get_by_id($concurso_id);

		// remove todos os vínculos antigos
		ExameModel::desvincular_todos_produtos($concurso->cat_id);

		// recupera exame vinculado ao concurso
		$exame = ExameModel::get_by_id($concurso->cat_id);

		if($exame) {
			// recupera cursos através do slug da categoria tipo exame
			$cursos = get_cursos_por_categoria($exame->slug, FALSE, TRUE, FALSE, FALSE, FALSE);

			// criar os vínculos produtos x exames
			if($cursos) {
				foreach ($cursos as $item) {
					
					$dados = [
			        	'post_id' => $item->get_id(),
			            'exa_id' => $concurso->cat_id,
			        ];
					
					$wpdb->insert('produtos_exames', $dados);
				}
			}
			
		}

	}
	
	/**
	 * Atualiza os metadados de quantidade de cursos de um concurso
	 *
	 * @since K4
	 *
	 * @param int $concurso_id Id do concurso
	 */
	
	public static function atualizar_metadados_qtde_produtos($concurso_id)
	{
	    global $wpdb;
	    
	    KLoader::model("ProdutoModel");
	    
	    $concurso = self::get_by_id($concurso_id);
	    $qtde = ProdutoModel::contar_por_concurso($concurso->con_slug, false, false);   
	    
	    $wpdb->update('concursos', ['con_qtde_cursos' => $qtde], ['con_id' => $concurso_id]);    
	}

	/**
	 * Recupera um concurso através da URL
	 * 
	 * @since K4
	 * 
	 * @param string $url URL de cursos por concurso específico
	 *
	 * @return object WP_Post
	 */ 

	public static function get_by_cursos_por_concurso_url($url)
	{
		if($url_a = explode("/", $url)) {

			if(isset($url_a[4])) {

				return self::get_by_slug($url_a[4]);
			
			}

		}
		
		return NULL;
	}

	/**
	 * Recupera um concurso através do seu ID
	 * 
	 * @since K4
	 * 
	 * @param int $id Id do concurso. Referente à coluna term_id da tabela wp_terms.
	 *
	 * @return object WP_Post
	 */ 

	public static function get_by_id($id)
	{
		global $wpdb;

		$sql = "SELECT * FROM concursos c
					JOIN wp_posts p ON c.con_id = p.ID
				WHERE c.con_id = {$id}";

		return $wpdb->get_row($sql);
	}
	
	/**
	 * Recupera um post através do seu slug
	 *
	 * @since K4
	 *
	 * @param string $slug Slug do post. Referente à coluna post_name da tabela wp_posts.
	 *
	 * @return object WP_Post
	 */
	
	public static function get_by_slug($slug)
	{
	    global $wpdb;
	    
	    $sql = "SELECT * FROM concursos c
					JOIN wp_posts p ON c.con_id = p.ID
				WHERE con_slug = '{$slug}'";
	    
	    return $wpdb->get_row($sql);
	}
	
	/**
	 * Recupera um concurso através do seu exame_id
	 * 
	 * @since K5
	 *
	 * @param int $exame_id Id do Exame
	 *
	 * @return object WP_Post
	 */
	
	public static function get_by_exame($exame_id)
	{
	    global $wpdb;
	    
	    $sql = "SELECT * FROM concursos c
					JOIN wp_posts p ON c.con_id = p.ID
				WHERE cat_id= {$exame_id}";
	    
	    return $wpdb->get_row($sql);
	}

	/**
	 * Recupera o menor preço dentre os produtos de um concurso
	 *
	 * @since L1
	 *
	 * @param int $id Id do concurso
	 *
	 * @return float Menor preço
	 */
	
	public static function get_menor_preco_concurso($id, $ignorar_gratis = true)
	{
	    global $wpdb;
	    
	    $where_ignorar_gratis = $ignorar_gratis ? " AND pr.pro_preco > 0 " : "";
	    $sql = "SELECT pro_preco FROM produtos pr
                    JOIN produtos_exames pe ON pr.post_id = pe.post_id
					JOIN concursos c ON c.cat_id = pe.exa_id
				WHERE c.con_id = $id {$where_ignorar_gratis} 
                    ORDER BY pr.pro_preco ASC LIMIT 1";
	    
	    $resultado = $wpdb->get_var($sql);
	    
	    if(is_null($resultado)) {
	        $resultado = 0;
	    }
	    return $resultado;
	}

	/**
	 * Lista todos os concursos que possuem cursos
	 * 
	 * @since K4
	 * 
	 * @return array Lista de concursos
	 */ 

	public static function listar_concursos_com_cursos()
	{
		global $wpdb;

		$sql = "SELECT * FROM concursos c
					JOIN wp_posts p ON c.con_id = p.ID 
				WHERE con_qtde_cursos > 0 AND p.post_status = 'publish' 
				ORDER BY p.post_title";

		return $wpdb->get_results($sql);
	}

	/**
	 * Lista concursos mais vendidos
	 * 
	 * @since K4
	 * 
	 * @return array Lista de concursos
	 */ 

	public static function listar_mais_vendidos($limit = 6, $offset = 0, $status = NULL)
	{
		global $wpdb;

		$where_status = $status ? " AND c.con_status = {$status} " : "";
		$offset = (int)$offset;

		$sql = "SELECT * FROM concursos c
					JOIN wp_posts p ON c.con_id = p.ID 
				WHERE con_qtde_cursos > 0 
					{$where_status} 
				ORDER BY con_vendas DESC
				LIMIT {$limit} OFFSET {$offset}";

		return $wpdb->get_results($sql);
	}

	/**
	 * Lista todos os concursos
	 * 
	 * @since K4
	 * 
	 * @return array Lista de concursos
	 */ 

	public static function listar_todos()
	{
		global $wpdb;

		$sql = "SELECT * FROM concursos c
					JOIN wp_posts p ON c.con_id = p.ID 
				ORDER BY p.post_title";

		return $wpdb->get_results($sql);
	}
	
	/**
	 * Salva ou atualiza um histórico de slug de um concurso
	 *
	 * @since L1
	 *
	 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2593
	 *
	 * @global $wpdb
	 *
	 * @param string $slug Slug do concurso
	 * @param int $id Id do concurso
	 */
	
	public static function salvar_slug_historico($slug, $id)
	{
	    global $wpdb;
	    
	    if(!$slug || !$id) {
	        throw new InvalidArgumentException();
	    }
	    
	    $wpdb->replace('concursos_slugs_historico',
	        ['con_slug' => $slug, 'con_id' => $id]
	        );
	}
	
	/**
	 * Recupera um concurso através do seu slug histórico
	 *
	 * @since L1
	 *
	 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2593
	 *
	 * @param string $slug Slug histórico do concurso.
	 *
	 * @return array
	 */
	
	public static function get_by_slug_historico($slug_historico)
	{
	    global $wpdb;
	    
	    $sql = "SELECT * FROM concursos_slugs_historico h 
                    JOIN wp_posts p ON p.ID = h.con_id
				WHERE h.con_slug = '{$slug_historico}'";

	    return $wpdb->get_row($sql);
	}

	public static function listar_bancas(){
		global $wpdb;

		$sql = "SELECT DISTINCT con_banca FROM concursos ORDER BY con_banca";

		return $wpdb->get_results($sql);
	}

    public static function excluir($id){
        global $wpdb;

        $sql = "DELETE FROM concursos WHERE con_id = {$id}";

        return $wpdb->query($sql);
    }
}