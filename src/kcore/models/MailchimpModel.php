<?php

/**
 * Model para organizar rotinas relacionadas ao MailChimp
 * 
 * @package	KCore/Models
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K5
 */ 

class MailChimpModel {

    /**
     * Cria um grupo de interesse em uma lista
     *
     * @since L1
     *
     * @param string $lista Id da Lista
     * 
     * @return string Id do grupo
     */
    
    public static function criar_grupo_interesse_areas_remoto($lista_id)
    {
        $data = [
            'title' => MAILCHIMP_INTERESSE_POR_AREAS,
            'type' => 'checkboxes'
        ];

        $grupo = json_decode(self::do_request("POST", "/lists/{$lista_id}/interest-categories", $data));
        
        if($grupo && $grupo->id)
        {
            return $grupo->id;
        } 
        else {
            return null;
        }
    }
    
    /**
     * Cria um interesse em um grupo de interesse
     *
     * @since L1
     *
     * @param string $lista Id da Lista
     */
    
    public static function criar_interesse_areas_remoto($lista_id, $interest_id, $nome)
    {
        $data = [
            'name' => $nome,
        ];
        
        self::do_request("POST", "/lists/{$lista_id}/interest-categories/{$interest_id}/interests", $data);
    }
    
	/**
	 * Realiza uma requisição à API do Mailchimp
	 * 
	 * @since K5
	 * 
	 * @see https://developer.mailchimp.com/documentation/mailchimp/reference/overview/
	 * 
	 * @param string $type Tipo da requisição POST, PUT, DELETE, GET, PATCH
	 * @param string $target Endereço da requisição
	 *
	 * @return string Retorno da requisição
	 */ 

	private static function do_request($type, $target, $data = false, $debug = false)
	{
	    
	    
		$ch = curl_init('https://us1.api.mailchimp.com/3.0/' . $target );

		curl_setopt( $ch, CURLOPT_HTTPHEADER, array
		(
			'Content-Type: application/json', 
			'Authorization: WP 9a8bb3e1c8eba8bb248fffcc29a842d1-us1',
		) );
	 
		curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, $type );
		curl_setopt( $ch, CURLOPT_TIMEOUT, 5 );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_USERAGENT, 'YOUR-USER-AGENT' );
	 
		if( $data )
			curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $data ) );
	 
		$response = curl_exec( $ch );
		curl_close( $ch );
		
		return $response;
	}

	/**
	 * Recupera um contato de uma lista do Mailchimp
	 * 
	 * @since K5
	 * 
	 * @param string $lista Id da Lista
	 * @param array $email E-mail do contato a ser recuperado
	 */ 

	public static function get_contato($lista, $email) 
	{
		$busca = json_decode(self::do_request("GET", "/search-members?query={$email}&list_id={$lista}"));

		if($busca && $busca->exact_matches && $busca->exact_matches->members)
		{
			return $busca->exact_matches->members[0];
		}

		else {
			return null;
		}
	}
	
	/**
	 * Lista os interesses de uma determinada lista e campo
	 *
	 * @since K5
	 *
	 * @param string $lista_id Id da lista
	 * @param string $campo_id Id do campo
	 * 
	 * @param array Lista de interesses
	 */
	
	public static function listar_interesses($lista_id, $campo_id)
	{
	    global $wpdb;
	    
	    $sql = "SELECT * FROM mc_areas_interesses WHERE mca_lista_id = '{$lista_id}' AND mca_campo_id = '{$campo_id}' ORDER BY mca_nome";
	    
	    return $wpdb->get_results($sql);
	}
	
	/**
	 * Lista interesses de uma lista do MailChimp
	 *
	 * @since L1
	 * 
	 * @param string $lista_id Id da lista no MailChimp
	 *
	 * @param array Listas do MailChimp
	 */
	
	public static function listar_interesses_remotos($lista_id)
	{
	    return json_decode(self::do_request("GET", "/lists/{$lista_id}/interest-categories"));
	}
	
	/**
	 * Lista todas as listas do MailChimp
	 *
	 * @since L1
	 *
	 * @param array Listas do MailChimp
	 */
	
	public static function listar_listas_remotas()
	{
	    return json_decode(self::do_request("GET", "/lists/"));
	}
	
	/**
	 * Sincroniza as áreas de interesse do MailChimp com base local
	 *
	 * @since L1
	 *
	 * @param array Listas do MailChimp
	 */
	
	public static function sincronizar_areas_de_interesse($lista_id, $campo_id) 
	{
	    global $wpdb;
	    
	    $areas = json_decode(self::do_request("GET", "/lists/{$lista_id}/interest-categories/{$campo_id}/interests"));
	    
	    $wpdb->delete("mc_areas_interesses", ["mca_lista_id" => $lista_id, "mca_campo_id" => $campo_id]);
	    
	    if ($areas && $areas->interests) {
	        
	        foreach ($areas->interests as $item) {
	           
	            $dados = [
	                'mca_lista_id' => $lista_id,
	                'mca_campo_id' => $campo_id,
	                'mca_id' => $item->id,
	                'mca_nome' => $item->name
	            ];
	            
	            $wpdb->insert("mc_areas_interesses", $dados);
	        }
	        
	    }
    }

	/**
	 * Cria ou atualiza um contato em uma lista do Mailchimp
	 * 
	 * @since K5
	 * 
	 * @param string $lista Id da Lista
	 * @param array $data Array representando o membro
	 */ 
	
	public static function salvar_contato($lista, $data)
	{
		$subscriber_hash = md5(strtolower($data['email_address']));

		self::do_request("PUT", "/lists/{$lista}/members/{$subscriber_hash}", $data);
	}
}