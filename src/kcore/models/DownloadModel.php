<?php
/**
 * Model para organizar rotinas relacionadas aos downloads de exportação
 * 
 * @package	KCore/Models
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K5
 */ 

class DownloadModel {
	
    /**
     * Atualiza os dados de um download
     *
     * @since K5
     *
     * @param array $dados Dados do download
     */
    
    public static function atualizar($dados)
    {
        global $wpdb;
        
        $wpdb->update("downloads", $dados, ["dow_id" => $dados['dow_id']]);
    }
    
	/**
	 * Cria um novo download
	 * 
	 * @since K5
	 * 
	 * @param array $dados Dados do download
	 * 
	 */ 
	
	public static function criar($dados)
	{
		global $wpdb;
		
		$wpdb->insert("downloads", $dados);
	}
	
	
	/**
	 * Remove um download
	 * 
	 * @param int $download_id Id do download
	 *
	 * @since K5
	 */
	
	public static function remover($download_id)
	{
	    global $wpdb;
	    
	    $sql = "DELETE FROM downloads WHERE dow_id = {$download_id}";
	    
	    $wpdb->query($sql);
	}
	/**
	 * Exclui todos downloads expirados
	 *
	 * @since K5
	 */
	
	public static function excluir_expirados()
	{
	    global $wpdb;
	    
	    $expiracao = date("Y-m-d H:i:s", strtotime(DOWNLOAD_TEMPO_EXPIRACAO));
	    
	    $sql = "DELETE FROM downloads WHERE dow_data < '{$expiracao}'";
	    
	    $wpdb->query($sql);
	}
	
	/**
	 * Recupera download através do id
	 *
	 * @since K5
	 *
	 * @param int $download_id Id do Download
	 * 
	 * @return array Array associativo da tabela downloads
	 */
	
	public static function get_by_id($download_id)
	{
	    global $wpdb;
    
	    $sql = "SELECT * FROM downloads WHERE dow_id = {$download_id}";
	    
	    return $wpdb->get_row($sql);
	}
	
	/**
	 * Recupera download pronto
	 *
	 * @since K5
	 *
	 * @param int $download_id Id do Download
	 *
	 * @return array Array associativo da tabela downloads
	 */
	
	public static function get_download_pronto($user_id, $relatorio_id, $filtros)
	{
	    global $wpdb;
	    
	    $filtros = serialize($filtros);
	    
	    $sql = "SELECT * FROM downloads 
                WHERE dow_status = 2 
                    AND user_id = {$user_id}
                    AND dow_relatorio_id = {$relatorio_id} 
                    AND dow_filtros = '{$filtros}' ";
	    
	    return $wpdb->get_row($sql);
	}
	
	/**
	 * Recupera download pronto
	 *
	 * @since K5
	 *
	 * @param int $download_id Id do Download
	 *
	 * @return array Array associativo da tabela downloads
	 */
	
	public static function get_por_usuario_relatorio_filtro($user_id, $relatorio_id, $filtros)
	{
	    global $wpdb;
	    
	    $filtros = serialize($filtros);
	    
	    $sql = "SELECT * FROM downloads
                WHERE user_id = {$user_id}
                    AND dow_relatorio_id = {$relatorio_id}
                    AND dow_filtros = '{$filtros}' ORDER BY dow_status DESC";
	    
	    return $wpdb->get_row($sql);
	}
	
	/**
	 * Recupera primeiro download agendado não expirado, nem com mais 3 ou mais tentativas de execução
	 *
	 * @since K5
	 *
	 * @return array Array associativo da tabela downloads
	 */
	
	public static function get_primeiro()
	{
	    global $wpdb;
	    
	    $expiracao = date("Y-m-d H:i:s", strtotime(DOWNLOAD_TEMPO_EXPIRACAO));
	    
	    $sql = "SELECT * FROM downloads WHERE dow_status = " . DOWNLOAD_STATUS_PENDENTE . 
	           " AND dow_data >= '{$expiracao}' AND dow_tentativas < " . DOWNLOAD_MAX_TENTATIVAS . " ORDER BY dow_tentativas ASC LIMIT 1";
	    
	    return $wpdb->get_row($sql);
	}
	
	/**
	 * Incrementa o número de tentativas de execução
	 *
	 * @since K5
	 *
	 * @param int $download_id Id do download
	 */
	
	public static function incrementa_tentativas($download_id)
	{
	    global $wpdb;
	    
	    $download = self::get_by_id($download_id);
	    
	    $tentativas = $download->dow_tentativas + 1;
	    
	    $wpdb->update("downloads", ["dow_tentativas" => $tentativas], ["dow_id" => $download_id]);
	}
	
	/**
	 * Listar downloads agendados não expirados por usuário
	 * 
	 * @since K5
	 *
	 * @param int $usuario_id Id do usuário
	 * @param int|null $status Id do status
	 * 
	 * @return array Lista de downloads agendados não expirados
	 */
	
	public static function listar($usuario_id, $status = null)
	{
	    global $wpdb;
	    
	    $where_status = $status ? " AND dow_status = {$status} " : "";
	    
	    $expiracao = date("Y-m-d H:i:s", strtotime(DOWNLOAD_TEMPO_EXPIRACAO));
	    
	    $sql = "SELECT * FROM downloads WHERE user_id = {$usuario_id} AND dow_data >= '{$expiracao}' 
                {$where_status} ORDER BY dow_data DESC";
	    
	    return $wpdb->get_results($sql);
	}

}