<?php 
/**
 * Model para organizar rotinas relacionadas aos Depoimentos
 * 
 * @package	KCore/Models
 *
 * @author 	João Paulo Ferreira <jpaulofms@gmail.com>
 *
 * @since	L1
 */ 

KLoader::model("PostModel");

class DepoimentoModel extends PostModel {

    /**
	 * Atualiza os metadados de um depoimento
	 * 
	 * @since L1
	 * 
	 * @param int $post_id ID do post do depoimento
	 *
	 */
	public static function atualizar_metadados($post_id, $com_meta_dados = FALSE)
	{
		global $wpdb;

		// recupera o post do wp
        $post = PostModel::get_by_id($post_id);

		// recupera metadados para montagem dos dados do depoimento	        
		$tipo = get_field( 'tipo_depoimento', $post_id );
		$link = get_field( 'link_depoimento', $post_id, TRUE );

		// montagem dos dados a serem gravados na tabela depoimentos
        $dados = [
        	'dep_id' => $post_id,
            'dep_slug' => $post->post_name,
		];
		
		//Se vai gravar os meta dados do ACF
		if($com_meta_dados)
		{
			$dados['dep_tipo'] = $tipo;
			$dados['dep_url'] = $link?:null;
		}

		if($post->post_status == 'publish')
		{
			if(self::get_by_id($post_id))
			{
				$wpdb->update('depoimentos', $dados, ['dep_id' => $post_id]);
			}
			else
			{
				$wpdb->insert('depoimentos', $dados);
			}
		}
		else//Se não está publicado apaga
		{
			$wpdb->delete('depoimentos', ['dep_id' => $post_id]);
		}
        
	}

	/**
	 * Monta a query básica para a busca de depoimentos
	 * 
	 * @since L1
	 * 
	 * @param Array $tipos Array contendo os tipos de depoimentos a serem buscados
	 * 
	 * @return string contendo a query básica para listagem e contagem de depoimentos
	 */	
	public static function buscar_query($tipos, $is_join = TRUE)
	{

		$query = 
			" FROM depoimentos dep " .
			($is_join?" INNER JOIN wp_posts p ON p.ID = dep.dep_id ":"") .
			" WHERE 1=1 ";
		
		if(!empty($tipos))
		{
			$tipos_str = implode(", ", $tipos);
			$query .= " AND dep.dep_tipo IN ($tipos_str) ";
		}

		return $query;
	}

	/**
	 * Conta os depoimentos com filtro de tipos
	 * 
	 * @since L1
	 * 
	 * @param array $tipos de depoimentos a serem filtrados
	 * 
	 * @return int total de depoimentos encontrados
	 */
	public static function contar_depoimentos($tipos)
	{
		global $wpdb;

		$query = self::buscar_query($tipos, FALSE);

		$query = "SELECT COUNT(*) " . $query;

		return $wpdb->get_var($query);
	}

	/**
	 * Recupera um depoimento através do seu slug
	 * 
	 * @since L1
	 * 
	 * @param string $slug Slug do post. Referente à coluna post_name da tabela wp_posts.
	 *
	 * @return object WP_Post e Depoimento
	 */
	public static function get_by_id($dep_id)
	{
		global $wpdb;

		$query = "SELECT * FROM depoimentos dep INNER JOIN wp_posts p ON p.ID = dep.dep_id WHERE dep.dep_id = {$dep_id}";

		return $wpdb->get_row($query);
	}

	/**
	 * Recupera um depoimento através do seu slug
	 * 
	 * @since L1
	 * 
	 * @param string $slug Slug do post. Referente à coluna post_name da tabela wp_posts.
	 *
	 * @return object WP_Post e Depoimento
	 */
	public static function get_by_slug($slug)
	{
		global $wpdb;

		$query = "SELECT * FROM depoimentos dep INNER JOIN wp_posts p ON p.ID = dep.dep_id WHERE dep.dep_slug = '{$slug}'";

		return $wpdb->get_row($query);
	}

	/**
	 * Lista os depoimentos com paginação e filtro de tipos
	 * 
	 * @since L1
	 * 
	 * @param array $tipos de depoimentos a serem filtrados
	 * @param int $offset da busca
	 * @param int $limite da busca
	 * 
	 * @return Array<Object> contendo os depoimentos encontrados
	 */
	public static function listar_depoimentos($tipos, $offset = 0, $limite = LIMITE_DEPOIMENTOS)
	{
		global $wpdb;

		$query = self::buscar_query($tipos, TRUE);

		$limitOffset = "";
		if($limite) {
		    $limitOffset = "LIMIT {$limite} OFFSET {$offset}";
        }

		$query = "SELECT * " . $query . " ORDER BY dep.dep_id DESC  {$limitOffset}";

		return $wpdb->get_results($query);

	}

    public static function excluir($id)
    {
        global $wpdb;

        $sql = "DELETE FROM depoimentos WHERE dep_id = {$id}";

        return $wpdb->query($sql);
    }
    
}