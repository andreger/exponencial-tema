<?php 
/**
 * Model para organizar rotinas relacionados aos professores
 * 
 * @package	KCore/Models
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K4
 */ 

KLoader::model("ColaboradorModel");

class ProfessorModel extends ColaboradorModel {

	/**
	 * Atualiza os dados editáveis pelo professor em seu perfil no SQ
	 * 
	 * @since L1
	 * 
	 * @param #id ID do professor
	 * @param $professor Array contendo os dados professor
	 * 
	 */
	public static function atualizar_dados_professor($id, $professor)
	{			
		update_user_meta ( $id, 'mini_cv', $professor['mini_cv'] );
		update_user_meta ( $id, 'description', $professor['descricao'] );
		update_user_meta ( $id, FACEBOOK, $professor[FACEBOOK] );
		update_user_meta ( $id, INSTAGRAM, $professor[INSTAGRAM] );
		update_user_meta ( $id, YOUTUBE, $professor[YOUTUBE] );

		self::atualizar_metadados($id);

		return $professor;
	}

	/**
	 * Recupera a quantidade de cursos de um professor
	 * 
	 * @since K4
	 * 
	 * @param int $professor_id Id do professor
	 *
	 * @return int Quantidade de cursos
	 */ 

	public static function get_qtde_cursos($professor_id)
	{
		global $wpdb;

		$sql = "SELECT COUNT(p.post_id) AS num FROM produtos_professores pp JOIN produtos p ON p.post_id = pp.post_id WHERE user_id = {$professor_id}
			AND (pro_vendas_ate IS NULL OR pro_vendas_ate >= CURDATE() )";
 
		return $wpdb->get_var($sql);
	}

	/**
	 * Recupera um professor através da URL
	 * 
	 * @since K4
	 * 
	 * @param string $url URL de cursos por professor específico.
	 *
	 * @return object WP_Term
	 */ 

	public static function get_by_cursos_por_professor_url($url)
	{
		if($url_a = explode("/", $url)) {

			if(isset($url_a[4])) {

				return self::get_by_slug($url_a[4]);
			
			}

		}
		
		return NULL;
	}

	/**
	 * Retorna os dados editáveis pelo professor em seu perfil no SQ
	 * 
	 * @since L1
	 * 
	 * @param $id ID do professor
	 * 
	 * @return array contendo os dados editáveis
	 */
	public static function get_dados_professor($id)
	{
		$professor = array();
		
		$professor['mini_cv'] = get_user_meta($id, 'mini_cv', TRUE);
		$professor['descricao'] = get_user_meta($id,'description', TRUE);
		$professor[FACEBOOK] = get_user_meta($id, FACEBOOK, TRUE);
		$professor[INSTAGRAM] = get_user_meta($id, INSTAGRAM, TRUE);
		$professor[YOUTUBE] = get_user_meta($id, YOUTUBE, TRUE);

		return $professor;
	}

	/**
	 * Lista todos os professores em formato combo options
	 * 
	 * @since K4
	 * 
	 * @return array Lista de professores combo optoins
	 */ 

	public static function listar_professores_combo_options()
	{
		$items = self::listar_por_tipo([COLABORADOR_PROFESSOR, COLABORADOR_PROFESSOR_CONSULTOR], "ID, display_name");

		$options = [];
		foreach ($items as $item) {
			$options[$item->ID] = $item->display_name;
		}

		return $options;
	}

	/**
	 * Lista todos os professores que possuem cursos
	 * 
	 * @since K4
	 * 
	 * @return array Lista de professores
	 */ 

	public static function listar_professores_com_cursos()
	{
		return self::listar_por_tipo([COLABORADOR_PROFESSOR, COLABORADOR_PROFESSOR_CONSULTOR], "*", "col_qtde_cursos > 0 AND col_oculto = 0", "display_name ASC");
	}

	/**
	 * Lista todos os professores não ocultos
	 * 
	 * @since K4
	 * 
	 * @return array Lista de professores
	 */ 

	public static function listar_professores_nao_ocultos()
	{
		return self::listar_por_tipo([COLABORADOR_PROFESSOR, COLABORADOR_PROFESSOR_CONSULTOR], "*", "col_oculto = 0", "col_prioridade DESC, display_name ASC");
	}
}