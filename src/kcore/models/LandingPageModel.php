<?php 
/**
 * Model para organizar rotinas relacionadas aos Blogs
 * 
 * @package	KCore/Models
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K4
 */ 

KLoader::model("PostModel");

class LandingPageModel extends PostModel {
    
    /**
    * Recupera uma landing page pelo ID
    *
    * @since K4
    *
    * @param int $id Id da LP
    *
    * @return object WP_Post
    */
    
    public static function get_by_id($id)
    {
        global $wpdb;
        
        $sql = "SELECT * FROM wp_posts p
				WHERE p.ID = {$id} AND p.post_type = 'landing_pages'";
        
        return $wpdb->get_row($sql);
    }
	
    /**
     * Recupera uma landing page pelo slug
     *
     * @since K4
     *
     * @param string $slug Slug da LP
     *
     * @return object WP_Post
     */
    
    public static function get_by_slug($slug)
    {
        global $wpdb;
        
        $sql = "SELECT * FROM wp_posts p
				WHERE p.post_name = '{$slug}' AND p.post_type = 'landing_pages'";
        
        return $wpdb->get_row($sql);
    }
    
	/**
	 * Salva ou atualiza um histórico de slug de uma LP
	 *
	 * @since L1
	 *
	 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2593
	 *
	 * @global $wpdb
	 *
	 * @param string $slug Slug da LP
	 * @param int $id Id da LP
	 */
	
	public static function salvar_slug_historico($slug, $id)
	{
	    global $wpdb;
	    
	    if(!$slug || !$id) {
	        throw new InvalidArgumentException();
	    }
	    
	    $wpdb->replace('landing_pages_slugs_historico',
	        ['lap_slug' => $slug, 'lap_id' => $id]
	        );
	}
	
	/**
	 * Recupera uma LP através do seu slug histórico
	 *
	 * @since L1
	 *
	 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2593
	 *
	 * @param string $slug Slug histórico da LP.
	 *
	 * @return array
	 */
	
	public static function get_by_slug_historico($slug_historico)
	{
	    global $wpdb;
	    
	    $sql = "SELECT * FROM landing_pages_slugs_historico h
                    JOIN wp_posts p ON p.ID = h.lap_id
				WHERE h.lap_slug = '{$slug_historico}'";

	    return $wpdb->get_row($sql);
	}

    public static function listar_publicadas()
    {
        global $wpdb;

        $sql = "SELECT * FROM wp_posts WHERE post_type = 'landing_pages' and post_status = 'publish'";

        return $wpdb->get_results($sql);
    }

}