<?php 
/**
 * Model para organizar rotinas relacionadas aos Pedidos
 * 
 * @package	KCore/Models
 *
 * @author 	Fábio Siqueira 
 *
 * @since	K5
 */ 

class PedidoModel {



	/**
	 * Altera as informações de um pedido para o usuário
	 * 
	 * @since L1
	 * 
	 * @param int $pedido_id ID do pedido (order_id)
	 * @param int $produto_id ID do produto do item do pedido
	 * @param int $status_usuario Status do produto desse pedido para o usuário: PEDIDO_ATIVO ou PEDIDO_CANCELADO ou PEDIDO_PENDENTE
	 * @param int $user_id ID do usuário
	 * @param int $item_id ID do item do pedido relacionado ao produto
	 * @param string $nome_item nome do item do pedido relacionado ao produto
	 * @param date $data_base data de conclusão do pedido para fins de cálculo da data de expiração, quando for o caso
	 * @param int $recorrente_id ID da assinatura recorrente associada ao pedido
	 */
	private static function alterar_pedido_produto($pedido_id, $produto_id, $status_usuario, $user_id, $item_id, $nome_item, $data_base, $recorrente_id){

		global $wpdb;

		$result = $wpdb->delete('pedido_produto', ['pedido_id' => $pedido_id, 'produto_id' => $produto_id]);
		log_kcore("INFO", "Deleted pedido_id {$pedido_id}, produto_id {$produto_id}: {$result}");

		if($status_usuario){
			$result = $wpdb->insert('pedido_produto', [
			                                     'pedido_id' => $pedido_id, 
			                                     'produto_id' => $produto_id, 
			                                     'status_usuario' => $status_usuario, 
			                                     'usu_id' => $user_id, 
			                                     'order_item_id' => $item_id,
												 'ppr_nome_item' => $nome_item,
												 'ped_data_base' => $data_base,
                                                 'ppr_recorrente_id' => $recorrente_id
			    
			]);
			log_kcore("INFO", "Inserted: {$result}");
		}

	}

	/**
	 * Altera o status do pedido para o usuário
	 * 
	 * @since M4
	 * 
	 * @param int $pedido_id ID do pedido (order_id)
	 * @param int $produto_id ID do produto do item do pedido
	 * @param int $status_usuario Status do produto desse pedido para o usuário: PEDIDO_ATIVO ou PEDIDO_CANCELADO ou PEDIDO_PENDENTE
	 */
	private static function alterar_somente_status_pedido_produto($pedido_id, $produto_id, $status_usuario)
	{
		global $wpdb;

		if($status_usuario)
		{
			$result = $wpdb->update('pedido_produto', ['status_usuario' => $status_usuario], ['pedido_id' => $pedido_id, 'produto_id' => $produto_id]);
			log_kcore("INFO", "Updated pedido_id {$pedido_id}, produto_id {$produto_id}: {$result}");
		}
	}

	/**
	 * Retorna os dados de um pedido_produto
	 * 
	 * @since L1 M3
	 * 
	 * @param int $pedido_id ID do pedido
	 * @param int $produto_id ID do produto
	 * 
	 * @return Array dados do pedido_produto
	 */
	public static function get_pedido_produto($pedido_id, $produto_id)
	{

		global $wpdb;

		$sql = "SELECT * FROM pedido_produto WHERE pedido_id = {$pedido_id} AND produto_id = {$produto_id}";
		
		$result = $wpdb->get_row($sql);

		return $result;
	}

	/**
	 * Atuliza o status através do seu ID e do novo status
	 * 
	 * @since K5
	 * 
	 * @param int $id Id do post. Referente à coluna ID da tabela wp_posts.
	 *
	 * @return object WP_Post
	 */ 

	public static function alterar_status($id,$status)
	{
		global $wpdb;

		$wpdb->update('wp_posts', ['post_status' => $status,], ['ID' => $id]);		
	}

	/**
	 * Conta os pedidos do usuário para cada tipo MEUS_PEDIDOS_ATIVOS, MEUS_PEDIDOS_EXPIRADOS e MEUS_PEDIDOS_CANCELADOS
	 * 
	 * @global $wpdb
	 * 
	 * @since L1
	 * 
	 * @param int $user_id Id do usuário. Referente à coluna ID da tabela wp_users
	 * @param string $tipo tipo da busca. Podem ser MEUS_PEDIDOS_ATIVOS, MEUS_PEDIDOS_EXPIRADOS e MEUS_PEDIDOS_CANCELADOS
	 * 
	 * @return int quantidade de pedidos
	 */
	public static function contar_meus_pedidos($user_id, $tipo = NULL){
		
		global $wpdb;

		$sql = "SELECT COUNT(*) ";

		$sql .= self::get_query_meus_pedidos($user_id, $tipo);
		
		$count = $wpdb->get_var($sql);

		return $count;
	}

	/**
	 * Gera a consulta básica para listar ou contar os pedidos de um usuário
	 * 
	 * @since L1
	 * 
	 * @param int $user_id ID do usuário. Referente à coluna ID da tabela wp_users
	 * @param string $tipo tipo da busca. Podem ser MEUS_PEDIDOS_ATIVOS, MEUS_PEDIDOS_EXPIRADOS e MEUS_PEDIDOS_CANCELADOS
	 * 
	 * @return string Base da query para consulta ou contagem dos pedidos de um usuário
	 */
	private static function get_query_meus_pedidos($user_id, $tipo = NULL)
	{
		$sql = " FROM produtos prod,
					pedido_produto pp
				WHERE pp.produto_id = prod.post_id 
					AND pp.usu_id='{$user_id}' 
					AND pp.pedido_id NOT IN (SELECT pedido_oculto_id FROM exponenc_db.premium_log)  
					AND prod.pro_is_pacote = 0 
					AND pp.ppr_recorrente_id IS NULL
				";

		if($tipo == MEUS_PEDIDOS_ATIVOS){
			$sql .= " AND (
							(pp.status_usuario = " . PEDIDO_ATIVO 
							//. " 	AND (prod.pro_disponivel_ate >= CURRENT_DATE() OR prod.pro_disponivel_ate IS NULL) ) "
							. " AND IF(prod.pro_tempo_acesso > 0, DATE_ADD(pp.ped_data_base, INTERVAL prod.pro_tempo_acesso DAY), IFNULL(prod.pro_disponivel_ate, CURRENT_DATE())) >= CURRENT_DATE() )"
							. " OR pp.status_usuario = ".PEDIDO_PENDENTE 
						.")";
		}elseif($tipo == MEUS_PEDIDOS_EXPIRADOS){
			$sql .= " AND ( ( pp.status_usuario = " . PEDIDO_ATIVO 
					 . " AND IF(prod.pro_tempo_acesso > 0, DATE_ADD(pp.ped_data_base, INTERVAL prod.pro_tempo_acesso DAY), IFNULL(prod.pro_disponivel_ate, CURRENT_DATE())) < CURRENT_DATE() )"
					 . " OR pp.status_usuario = " . PEDIDO_EXPIRADO . " ) ";
		}elseif($tipo == MEUS_PEDIDOS_CANCELADOS){
			$sql .= " AND pp.status_usuario = " . PEDIDO_CANCELADO . " ";
		}

		return $sql;
	}
	
	/**
	 * Lista os pedidos do usuário para cada tipo MEUS_PEDIDOS_ATIVOS, MEUS_PEDIDOS_EXPIRADOS e MEUS_PEDIDOS_CANCELADOS para o limite e offset informados.
	 * 
	 * @global $wpdb
	 * 
	 * @since L1
	 * 
	 * @param int $user_id Id do usuário. Referente à coluna ID da tabela wp_users
	 * @param string $tipo tipo da busca. Podem ser MEUS_PEDIDOS_ATIVOS, MEUS_PEDIDOS_EXPIRADOS e MEUS_PEDIDOS_CANCELADOS
	 * @param int $offset offset informando a partir de qual dado será retornado
	 * @param int $limite limite de dados retornados na busca
	 * 
	 * @return array IDs dos pedidos e do produto relacionado
	 */
	public static function listar_meus_pedidos($user_id, $tipo = NULL, $offset = 0, $limite = MINHA_CONTA_PEDIDOS_POR_PAGINA){

		global $wpdb;

		$sql = "SELECT pp.produto_id as product_id, pp.pedido_id as order_id, pp.ppr_nome_item as order_item_name ";
		
		$sql .= self::get_query_meus_pedidos($user_id, $tipo);

		$sql .= " ORDER BY pp.pedido_id DESC, pp.order_item_id	LIMIT {$limite} OFFSET {$offset}";

		$result = $wpdb->get_results($sql);

		return $result;
	}

	/**
	 * Lista os IDs dos pedidos realizados pelo usuário
	 * 
	 * @since L1
	 * 
	 * @param int $user_ID ID do usuário
	 * 
	 * @return array Array contendo os IDs dos pedidos do usuário
	 */
	public static function listar_pedidos_usuario($user_ID){

		global $wpdb;

		$sql = "SELECT post_id 
					FROM wp_postmeta 
					WHERE meta_key='_customer_user' 
						AND meta_value='{$user_ID}' 
						AND post_id NOT IN (SELECT pedido_oculto_id FROM premium_log)  
					ORDER BY meta_id DESC";

		return $wpdb->get_results($sql);
	}

    public static function listarProximos($ultimoId = 0)
    {
        global $wpdb;

        $sql = "SELECT * 
					FROM wp_postmeta 
                    JOIN wp_posts ON ID = post_id
					WHERE meta_key='_customer_user' 
						AND post_id NOT IN (SELECT pedido_oculto_id FROM premium_log)  
                        AND post_id > {$ultimoId}
					ORDER BY post_id ASC LIMIT 100";

        return $wpdb->get_results($sql);
    }

    public static function listar_produtos_do_pedido($pedidoId)
    {
        global $wpdb;

        $sql = "SELECT * FROM wp_woocommerce_order_items oi 
                    JOIN wp_woocommerce_order_itemmeta im ON im.order_item_id = oi.order_item_id
                    WHERE oi.order_id = {$pedidoId}
                    AND im.meta_key = '_product_id' ";

        return $wpdb->get_results($sql);
    }

	/**
	 * Verifica se o status do pedido para o usuário é PEDIDO_ATIVO ou PEDIDO_CANCELADO e atualiza essa informação
	 * 
	 * @since L1
	 * 
	 * @param int $pedido_id Id do pedido (order_id)
	 * @param string $force_this_status Status a ser utilizado na função independentemente do que for verificado no pedido
	 */
	public static function alterar_pedido_status_usuario($pedido_id, $force_this_status = NULL){

		log_kcore("INFO", "====> Order: " . $pedido_id);

		$order = new WC_Order($pedido_id);	

		log_kcore("INFO", "EncontrouOrder: " . $order->get_id());
		
		foreach ( $order->get_items () as $item ) {

			if ($item ['bundled_items']) {
				log_kcore("INFO", "Eh pacote");
				continue;//ignora pacotes porque os itens de verdade vem depois desse
			}

			if(!$item['product_id']) {
				log_kcore("INFO", "ProdutoID inválido: {$item['product_id']}");
				continue;//ignora produtos inexistentes
			}

			try{
				$produto = wc_get_product($item['product_id']);
			}catch (Exception $e){
				log_kcore("INFO", "Erro ao buscar produto {$item['product_id']}: " . $e);
				continue;//ignora produtos com erro
			}

			log_kcore("INFO", "ProdutoEncontrado: " . $produto->get_id());

			$order_status = $order->status;
			$pedido_original_id = null;

			log_kcore("INFO", "OrderStatus: {$order_status}");

			//Vem preenchido quando é um pedido gerado por uma assinatura recorrente
			$recorrente_id = get_pedido_recorrente_id($pedido_id);
			log_kcore("INFO", "Pedido recorrente = {$recorrente_id}");

			//Vem preenchido quando é um pedido que originou uma assinatura recorrente
			$subscription_id = $recorrente_id?:get_subscription_from_pedido($pedido_id);

			if($subscription_id)
			{
				log_kcore("INFO", "Eh assinatura recorrente {$subscription_id}");

				$subscription = wcs_get_subscription($subscription_id);

				if( $subscription )
				{
					log_kcore("INFO", "Assinatura recorrente encontrada com status: " . $subscription->status );

					$order_status = $subscription->status;
					
					$pedido_assinatura = $subscription->get_parent();
					if($pedido_assinatura)
					{
						$pedido_original_id = $pedido_assinatura->get_id();
					}
				}
			}
	
			if(!$produto || $order->post_status == 'trash'){ // para produtos que foram removidos do sistema ou pedidos na lixeira
				$status = NULL;	
			}				
			elseif(is_produto_estornado($order->id, $item['product_id'], TRUE)) {
				$status = PEDIDO_CANCELADO;
			}
			else {

				log_kcore("INFO", "OrderStatus: " . $order_status);

				if($force_this_status)
				{
					$order_status = $force_this_status;
					log_kcore("INFO", "OrderStatus foi forçado a ser alterado para: {$force_this_status}");
				}

				switch ($order_status) {
					case 'completed' :
					case 'active' :
						$status = PEDIDO_ATIVO;
						break;
					case 'refunded' ://Já foi visto acima que não foi revogado logo mantem ativo
						$status = PEDIDO_ATIVO;
						break;
					case 'on-hold' :
					case 'pending' :
					case 'failed' :
						$status = PEDIDO_PENDENTE;
						break;
					case 'cancelled' :
					case 'pending-cancel' :
						$status = PEDIDO_CANCELADO;
						break;
					case 'processing' :
						$status = PEDIDO_PENDENTE;
						break;
					case 'switched' :
					case 'expired' :
						$status = PEDIDO_EXPIRADO;//A princípio só vai acontecer para assinaturas porque os pedidos normais não possuem esses status
						break;
				}
			}

			log_kcore("INFO", "STATUS: " . $status );
			
            // recupera a data do pedido
            $post = get_post($pedido_id);
            $data_base = date("Y-m-d", strtotime($post->post_date));

			self::alterar_pedido_produto($pedido_id, $item['product_id'], $status, $order->get_customer_id(), $item->get_id(), $item['name'], $data_base, $recorrente_id);

			if($pedido_original_id && ( $pedido_original_id != $order->get_id() ) )
			{
				self::alterar_somente_status_pedido_produto($pedido_original_id, $item['product_id'], $status);
			}
		}

	}
	
	/**
	 * Lista os arquivos das aulas do produto agrupados por aulas
	 *
	 * @since L1
	 *
	 * @param int $produto_id Id do produto.
	 * @param int $pedido_id Id do pedido.
	 *
	 * @return array Lista de aulas e arquivos
	 */
	
	public static function listar_arquivos_aulas_disponiveis_agrupados_por_aula($produto_id, $pedido_id, $is_demo = FALSE)
	{
		KLoader::model("CadernoModel");

	    $arquivos = array();
	    
	    $produto = wc_get_product($produto_id);
	    
	    $files = $produto->get_downloads();
		
		if($pedido_id)
		{
			$order = wc_get_order($pedido_id);
			$downloadable_items = $order->get_downloadable_items();
		}
	    
	    foreach ($files as $download_id => $file) {
			
			$tipo_aula = null;
	        if ($nome_aula = get_nome_aula_by_file($produto_id, $file['file'], FALSE, $tipo_aula)) {
	            
	            $download_url = "";
	            foreach ($downloadable_items as $downloadable_item) {	                
	                if($downloadable_item['download_id'] == $download_id && $produto_id == $downloadable_item["product_id"]) {
	                    $download_url = $downloadable_item['download_url'];
	                    break;
	                }	                
				}
				
				$filename = basename($file['file']);
				
				if($is_demo)
				{
					$arquivos[$nome_aula][] = [
	                    'tipo' => $tipo_aula,
	                    'download_url' => get_url_aula_demonstrativa($file['file']),
	                    'file' => $file['file'],
					    'filename' => $filename,
	                    'curso' => $produto->get_name()
					];
					
				}elseif (is_aula_disponivel_para_download($produto_id, $nome_aula, $pedido_id)) 
				{
	                $arquivos[$nome_aula][] = [
	                    'tipo' => $tipo_aula,
	                    'download_url' => $download_url,
	                    'filename' => $filename,
	                    //'download_url' => get_download_url($pedido_id, $produto_id, $download_id),
	                    'file' => $file['file'],
	                    'curso' => $produto->get_name()
	                ];
	            }
	        }
	    }
		
	    if ($cronograma = get_cronograma($produto_id)) {
	        $temp = $arquivos;
	        $arquivos = array();
	        
	        $index = 0;
			$aulas_vimeo = get_post_meta($produto_id, 'aulas_vimeo', true);
			$aulas_caderno = get_post_meta($produto_id, 'aulas_caderno', true);
	        
	        foreach ($cronograma as $aula) {
	            
	            if ($aula && isset($temp[$aula['nome']]) && is_array($temp)) {
	                foreach ($temp[$aula['nome']] as $item) {
	                    $arquivos[$aula['nome']][] = $item;
	                }
	            }
	            
	            if ($vimeo = $aulas_vimeo[$index]) {
	                foreach ($vimeo as $item) {
	                    $arquivos[$aula['nome']][] = [
	                        'tipo' => TIPO_AULA_VIDEO,
	                        'download_url' => $item,
	                        'file' => $item
	                    ];
	                }
	            }
				
				if ($cadernos = $aulas_caderno[$index]) {
	                foreach ($cadernos as $item) {						
						
						$caderno = CadernoModel::get_caderno($item, TRUE, $produto_id, $pedido_id);
						if(!$caderno){//Se não encontrou tem que regerar os cadernos desse usuário para esse produto
							criar_caderno($produto_id, $pedido_id, FALSE, TRUE);							
							$caderno = CadernoModel::get_caderno($item, TRUE, $produto_id, $pedido_id);
						}

						if($caderno){
							$arquivos[$aula['nome']][] = [
								'tipo' => TIPO_AULA_CADERNO,
								'cad_id' => $caderno->cad_id,
								'nome' => $caderno->cad_nome,
								'url' => "/questoes/main/resolver_caderno/{$caderno->cad_id}?zf=1"
							];
						}
					}
	            }

	            if (! $arquivos[$aula['nome']]) {
	                $arquivos[$aula['nome']] = [];
	            }
	            
	            $index ++;
	        }
	    }

	    return $arquivos;
	}
	
	/**
	 * Recupera o desconto de um pedido
	 *
	 * @since L1
	 *
	 * @param WC_Order $order Pedido no Walker
	 * 
	 * @return float
	 */
	
	public static function get_pedido_desconto($order)
	{
	    
	    $desconto_wp = $order->get_total_discount();
	    
	    KLoader::helper("PagarMeGatewayHelper");
	    
	    $order_id = $order->get_id();
	    
	    // Descontos no plugin de desconto
	    if(PagarMeGatewayHelper::is_gateway_pedido_pagarme($order_id)) {
	        
	        $items = $order->get_items(['fee']);
	        
	        if($items) {
	            $desconto = 0;
	            
	            foreach ($items as $item) {
	                $desconto += abs($item["total"]);
	            }
	            
	            return $desconto_wp + $desconto;
	        }

	        else {
	            return $desconto_wp;
	        }
	        
	    }
	    // Descontos no PagSeguro
	    else {
	        return $order->get_total_discount();
	    } 
	    
	}
	
	/**
	 * Verifica se um pedido foi reembolsado totalmente
	 * 
	 * Obs.: Foi separado inicialmente por peciliaridades do Pagar.me mas aparentemente também é válido pro PagSeguro
	 *
	 * @since L2
	 *
	 * @param WC_Order $order Pedido
	 * @param double $total Total do pedido
	 * 
	 * @return boolean
	 */
	public static function is_pedido_reembolsado_totalmente($order, $total = NULL)
	{
		KLoader::helper("PagarMeGatewayHelper");

		if(PagarMeGatewayHelper::is_gateway_pedido_pagarme($order->get_id()))
		{
			return (($order->get_total_refunded() > 0) && ($order->get_total_refunded() == $order->get_total())) ? true : false;
		}
		else
		{
			/*if(is_null($total))
			{
				$total_desconto = self::get_pedido_desconto($order);
				$total = $order->get_total() + $total_desconto;
			}
			return (($order->get_total_refunded() > 0) && ($order->get_total_refunded() == $total)) ? true : false;*/

			//Usava a forma acima mas nos testes não fez sentido somar o desconto para verificar reembolso, o reembolso só pode ser realizado no valor que foi pago e nunca maior
			//segue separado para caso seja necessário fazer ajustes específicos até o deploy. JP
			return (($order->get_total_refunded() > 0) && ($order->get_total_refunded() == $order->get_total())) ? true : false;
		}
	}
	
	/**
	 * Contabiliza o download quando ele é feito através do "Baixar todos"
	 *
	 * Para saber sobre o critério de escolha do arquivo consultar Produto::listar_arquivos_aulas_disponiveis_agrupados_por_aula
	 *
	 * @since K6
	 *
	 * @param int $pedido_id ID do pedido
	 * @param int $produto_id ID do produto
	 */
	public static function registrar_download_curso($pedido_id, $produto_id, $aula_index = null, $tipo_aula = TIPO_AULA_PDF){
	    KLoader::model("AulaModel");
	    
	    $order = new WC_Order ($pedido_id);
// 	    $produto = new WC_Product($produto_id);
	    
	    $data_store   = WC_Data_Store::load( 'customer-download' );
	    
	    $current_user_id = get_current_user_id();
	    $current_user_email = get_usuario_email( $current_user_id );
	    $ip_address = WC_Geolocation::get_ip_address();
	    
	    foreach ( $order->get_items() as $item ) {
	        
	        if ($item ['product_id'] == $produto_id) {
	            
	            $files = $item->get_item_downloads();
	            
	            foreach ($files as $download_id => $file) {
	                
	                // rotina para considerar apenas arquivos de uma determinada aula
	                if($aula_index) {
	                    $arquivos = AulaModel::listar_arquivos_urls_por_aula($produto_id, $aula_index, $tipo_aula);
	                    
	                    if(!in_array($file["file"], $arquivos)) {
	                        continue;
	                    }
	                }
	                
	                if($nome_aula = get_nome_aula_by_file($produto_id, $file['file'])) {
	                    
	                    if(is_aula_disponivel_para_download($item['product_id'], $nome_aula)) {
	                        
	                        $download_ids = $data_store->get_downloads( array(
	                            'user_id' 	  => $current_user_id,
	                            'user_email'  => $current_user_email,
	                            'order_key'   => $order->get_order_key(),
	                            'order_id'    =>  $order->get_id(),
	                            'product_id'  => $produto_id,
	                            'download_id' => $download_id,
	                            'orderby'     => 'downloads_remaining',
	                            'order'       => 'DESC',
	                            'limit'       => 1,
	                            'return'      => 'ids',
	                        ) );
	                        
	                        $download = new WC_Customer_Download( current($download_ids) );
	                        //print_r($download);
	                        // Track the download in logs and change remaining/counts.
	                        $download->track_download( $current_user_id > 0 ? $current_user_id : null, ! empty( $ip_address ) ? $ip_address : null );
	                    }
	                    
	                }
	                
	            }
	            
	        }
	    }
	    
	    /*global $wpdb;
	    
	    foreach ( $downloads as $download ) {
	    
	    $wpdb->update(
	    'wp_woocommerce_downloadable_product_permissions',
	    array(
	    'download_count' => 'ifnull(download_count, 0) + 1',
	    'downloads_remaining' => "IF( downloads_remaining = '', '', GREATEST( 0, downloads_remaining - 1 ) )"
	    ),
	    array(
	    'download_id' => $download['download_id'],
	    'user_id' => $download['user_id'],
	    'product_id' => $download['product_id'],
	    'order_id' => $download['order_id']
	    )
	    
	    );
	    }*/
	    
	}
}