<?php 
/**
 * Model para organizar rotinas relacionadas aos Blogs
 * 
 * @package	KCore/Models
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K4
 */

use Exponencial\Core\Cache\CacheFactory;

KLoader::model("PostModel");

class BlogModel extends PostModel {

	/**
	 * Atualiza os metadados de um blog
	 * 
	 * @since K4
	 * 
	 * @param int $id Id do produto
	 */ 

	public static function atualizar_metadados($id)
	{
		global $wpdb;


		$post = get_post($id);
		
		$dados = [
			'blo_id' => $id,
			'blo_slug' => $post->post_name,
			'blo_resumo' => get_excerpt_by_id($post->ID),
			'user_id' => $post->post_author,
			'blo_thumb' => get_the_post_thumbnail_url($post->ID)
		];

		$wpdb->replace('blogs', $dados);

		$wpdb->delete('blogs_tipos', ['blo_id' => $id]);
		$tipos = get_midia_categorias_ids($id);

		if($tipos) {

			foreach ($tipos as $tipo) {
				$item = [
					'blo_id' => $id,
					'blo_tipo' => $tipo
				];

				$wpdb->insert('blogs_tipos', $item);
			}
		}
	}

	/**
	 * Contar entradas do blog por professor
	 * 
	 * @since K4
	 * 
	 * @param int $id Id do professor
	 * @param array $tipos_ids Tipo das entradas (artigos, notícias, vídeos)
	 * 
	 * @return int
	 */ 

	public static function contar_por_professor($id, $tipos_ids = null)
	{
		$cacheFactory = new CacheFactory();
		$cache_name = CacheFactory::PROFESSOR_TOTAL_ARTIGOS . $id;
	    $memcached = $cacheFactory->getMemcached();

		$cache = $cache_name ? $memcached->get($cache_name) : null;
		
		if(!$cache)
		{
			$resultado = self::listar_por_professor_sql_builder($id, $tipos_ids, null, null, null, "COUNT(*) AS num");
			$cache = $resultado[0]->num;
			$memcached->set($cache_name, $cache);

			log_kcore("DEBUG", "Cache {$cache_name} não foi encontrado mas foi recriado");
		}

		return $cache;
	}

	/**
	 * Lista por professor
	 * 
	 * @since K4
	 * 
	 * @param int $id Id do professor
	 * @param array $tipos_ids Tipo das entradas (artigos, notícias, vídeos)
	 * @param string $order_by Ordenação da busca
	 * @param int $limit Limit
	 * @param int $offset Offset
	 * 
	 * @return array
	 */ 

	public static function listar_por_professor_sql_builder($id, $tipos_ids = null, $order_by = 'post_date DESC', $limit = null, $offset = 0, $select = "DISTINCT b.*, p.*, c.*, u.*")
	{
		global $wpdb;

		$join_tipos = $tipos_ids ? " JOIN blogs_tipos bt ON b.blo_id = bt.blo_id " : "";
		$where_tipos = $tipos_ids ? " AND blo_tipo IN ( " . implode(",", $tipos_ids) . ")" : "";

		$limit_clause = $limit ? " LIMIT {$offset}, {$limit} " : "";

		$order_by_clause = $order_by ? " ORDER BY {$order_by} " : "";

		$sql = "SELECT {$select} FROM blogs b 
					{$join_tipos}
					JOIN wp_posts p ON b.blo_id = p.id
					JOIN wp_users u ON u.ID = b.user_id 
					JOIN colaboradores c ON u.ID = c.user_id 
				WHERE b.user_id = {$id}
					AND p.post_status = 'publish' 
					{$where_tipos}
				{$order_by_clause} 
				{$limit_clause}";

		return $wpdb->get_results($sql);
	}

	/**
	 * Recupera um post de blog através do seu ID
	 * 
	 * @since K4
	 * 
	 * @param int $id Id do post de blog. Referente à coluna ID da tabela wp_posts.
	 *
	 * @return object WP_Post
	 */ 

	public static function get_by_id($id)
	{
		global $wpdb;

		$sql = "SELECT * FROM wp_posts p 
					JOIN blogs b ON b.blo_id = p.ID 
					JOIN wp_users u ON b.user_id = u.ID 
					JOIN colaboradores c ON c.user_id = u.ID 
				WHERE p.ID = {$id}";

		return $wpdb->get_row($sql);
	}

	/**
	 * Lista as entradas dos blogs
	 * 
	 * @since K4
	 * 
	 * @param int|array|null $tipos Tipo dos blogs
	 * @param int $limit Limit da busca
	 * @param int $offset Offset da busca
	 * 
	 * @return array Lista de entradas de blog. Tabela wp_posts
	 */ 

	public static function listar($tipos_ids = NULL, $limit = 6, $offset = 0)
	{
	    /* === tratamento dos parâmetros === */
	    
	    if(filter_var($limit, FILTER_VALIDATE_INT) === false) throw new InvalidArgumentException("Argumento 'limit' precisa ser inteiro");
	    if(filter_var($offset, FILTER_VALIDATE_INT) === false) throw new InvalidArgumentException("Argumento 'offset' precisa ser inteiro");
	    
	    /* === fluxo normal === */
	    
		global $wpdb;

		$where_tipos = "";
		$join_tipos = "";
		if($tipos_ids) {
		    $tipos_ids = is_array($tipos_ids) ? $tipos_ids : [$tipos_ids];
		    $where_tipos = " AND blo_tipo IN ( " . implode(",", $tipos_ids) . ")";
		    $join_tipos = " JOIN blogs_tipos bt ON b.blo_id = bt.blo_id ";   
		}	

		$sql = "SELECT DISTINCT b.*, p.*, u.* FROM blogs b 
					{$join_tipos}
					JOIN wp_posts p ON b.blo_id = p.id 
					JOIN wp_users u ON b.user_id = u.ID 
				WHERE p.post_status = 'publish' 
					{$where_tipos}
				ORDER BY p.post_date DESC 
				LIMIT {$offset}, {$limit}";
				
		return $wpdb->get_results($sql);
	}

	/**
	 * Lista os tipos de um blog post
	 * 
	 * @since K4
	 * 
	 * @param int $id Id do blog post
	 * 
	 * @return array Lista de tipos
	 */ 

	public static function listar_tipos($id)
	{
		global $wpdb;

		$sql = "SELECT blo_tipo FROM blogs_tipos b WHERE blo_id = {$id}";

		return $wpdb->get_col($sql);
	}

	/**
	 * Lista por professor
	 * 
	 * @since K4
	 * 
	 * @param int $id Id do blog post
	 * @param array $tipos_ids Tipo das entradas (artigos, notícias, vídeos)
	 * @param string $order_by Ordenação da busca
	 * @param int $limit Limit
	 * @param int $offset Offset
	 * 
	 * @return array
	 */ 

	public static function listar_por_professor($id, $tipos_ids = null, $order_by = 'post_date DESC', $limit = null, $offset = 0)
	{
		$cacheFactory = new CacheFactory();
		$cache_name = CacheFactory::PROFESSOR_ARTIGOS . $id;
	    $memcached = $cacheFactory->getMemcached();

		$cache = $cache_name ? $memcached->get($cache_name) : null;
		
		if(!$cache)
		{
			$cache = self::listar_por_professor_sql_builder($id, $tipos_ids, $order_by, $limit, $offset);
			$memcached->set($cache_name, $cache);
			log_kcore("DEBUG", "Cache {$cache_name} não foi encontrado mas foi recriado");
		}
	
		return $cache;
	}
	
	/**
	 * Salva ou atualiza um histórico de slug de um blog
	 *
	 * @since L1
	 *
	 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2593
	 *
	 * @global $wpdb
	 *
	 * @param string $slug Slug do blog
	 * @param int $id Id do blog
	 */
	
	public static function salvar_slug_historico($slug, $id)
	{
	    global $wpdb;
	    
	    if(!$slug || !$id) {
	        return;
	    }
	    
	    $wpdb->replace('blogs_slugs_historico',
	        ['blo_slug' => $slug, 'blo_id' => $id]
	    );
	}
	
	/**
	 * Recupera um blog através do seu slug histórico
	 *
	 * @since L1
	 *
	 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2593
	 *
	 * @param string $slug Slug histórico do blog.
	 *
	 * @return array
	 */
	
	public static function get_by_slug_historico($slug_historico)
	{
	    global $wpdb;
	    
	    $sql = "SELECT * FROM blogs_slugs_historico h
                    JOIN wp_posts p ON p.ID = h.blo_id
				WHERE h.blo_slug = '{$slug_historico}'";
	    
	    return $wpdb->get_row($sql);
	}

    public static function excluir($id)
    {
        global $wpdb;

        $sql = "DELETE FROM blogs WHERE blo_id = {$id}";

        $wpdb->query($sql);
    }

    public function listar_proximos($ultimoId)
    {
        global $wpdb;

        $sql = "SELECT DISTINCT b.*, p.*, u.* FROM blogs b 
					JOIN wp_posts p ON b.blo_id = p.id 
					JOIN wp_users u ON b.user_id = u.ID 
				WHERE p.post_status = 'publish' 
                    AND p.id > {$ultimoId} 
				ORDER BY p.id ASC 
				LIMIT 10";

        return $wpdb->get_results($sql);
    }
}