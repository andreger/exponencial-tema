<?php
/**
 * Model para organizar rotinas relacionadas às Categorias de Áreas
 * 
 * @package	KCore/Models
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K4
 */ 

KLoader::model("CategoriaModel");

class AreaModel extends CategoriaModel {
	
	/**
	 * Lista todas as áreas
	 * 
	 * @since K4
	 * 
	 * @return array Lista de categorias. Registros da tabela wp_terms.
	 */ 
	
	public static function listar()
	{
		return self::listar_filhas(CATEGORIA_AREA);
	}

}