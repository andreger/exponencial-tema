<?php 

class RespostaModel {

    public function listarProximos($ultimoId)
    {
        global $wpdb;

        $sql = "SELECT * FROM wp_posts WHERE post_type = 'reply' AND ID > {$ultimoId} ORDER BY ID LIMIT 100";

        return $wpdb->get_results($sql);
    }

}