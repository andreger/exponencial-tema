<?php
/**
 * Model para organizar rotinas relacionadas ao SEO H1
 *
 * @package	KCore/Models
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K5
 */

class SeoH1Model {
     
    /**
     * Recupera informações de H1 específico
     *
     * @since K5
     *
     * @param int $id Id do H1
     *
     * @return array
     */
    
    public static function get_by_id($id)
    {
        global $wpdb;
        
        $sql = "SELECT * from seo_h1 WHERE sh1_id = {$id}";
        
        $resultado = $wpdb->get_row($sql, ARRAY_A);
        
        return $resultado ?: null;
    }
    
    /**
     * Atualiza informações de H1 de um conteúdo
     *
     * @since K5
     *
     * @param array $dados Dados do H1
     */
    
    public static function salvar($dados)
    {
        global $wpdb;
        
        if(self::get_by_id($dados['sh1_id'])) {      
            $wpdb->update('seo_h1', $dados, ['sh1_id' => $dados['sh1_id']]);
        }
        else {
            $wpdb->insert('seo_h1', $dados);
        }
    }
    
}