<?php
/**
 * Model para organizar rotinas relacionadas às assinaturas
 *
 * @package	KCore/Models
 *
 * @author 	João Paulo Ferreira <jpaulofms@gmail.com>
 *
 * @since	L1
 */

class AssinaturaModel  {


    /**
     * Conta as assinaturas de alunos
     *
     * @global $wpdb
     *
     * @since L1
     *
	 * @param int $expiracao Data de expiração da assinatura: data_fim <= $expiracao
	 * @param string $somente_ativos Se TRUE retorna somente assinaturas ativas: data_fim >= hoje
     *
     * @return int quantidade de assinaturas existentes com base nos fitros.
     */
	public static function contar_alunos_assinaturas($expiracao, $somente_ativos = false, $somente_expirados = false){

        global $wpdb;

        $form = self::get_query_alunos_assinaturas($expiracao, $somente_ativos, $somente_expirados);

        $sql = " SELECT COUNT(distinct u.ID) " . $form;

        $count = $wpdb->get_var($sql);

		return $count;

    }

    /**
	 * Gera a consulta básica para listar ou contar as assinaturas de alunos
	 *
	 * @since L1
	 *
	 * @param int $expiracao Data de expiração da assinatura: data_fim <= $expiracao
	 * @param string $somente_ativos Se TRUE retorna somente assinaturas ativas: data_fim >= hoje
	 *
	 * @return string Base da query para consulta ou contagem das assinaturas de alunos
	 */
    private static function get_query_alunos_assinaturas($expiracao, $somente_ativos, $somente_expirados){

        $where = "";
        $hoje = date('Y-m-d');

        if($expiracao) {
            $where = " where data_fim <= '$expiracao' and data_fim >= '{$hoje}' ";
        }

        if($somente_ativos) {
            $where = " where data_fim >= '{$hoje}' ";
        }
        if($somente_expirados) {
            $where = " where data_fim < '{$hoje}' ";
        }

        $sql = " FROM assinaturas a JOIN wp_users u ON a.user_id = u.ID {$where}";

        return $sql;
    }

    /**
     * Lista os alunos com assinatura do SQ
     *
     * @global $wpdb
     *
     * @since L1
     *
     * @param int $expiracao Data de expiração da assinatura: data_fim <= $expiracao
	 * @param string $somente_ativos Se TRUE retorna somente assinaturas ativas: data_fim >= hoje
     * @param int $offset Início dos resultado a serem exibidos
     * @param int $limite Quantidade mãxima de itens a serem retornados
     *
     * @return array Array associativo da tabela assinaturas.
     */

    public static function listar_alunos_assinaturas($expiracao, $somente_ativos = false, $somente_expirados = false, $offset = 0, $limite = LIMITE_RELATORIO_ASSINATURAS)
    {
        global $wpdb;

        $from = self::get_query_alunos_assinaturas($expiracao, $somente_ativos, $somente_expirados);

        $sql = "SELECT user_id,
                    display_name,
                    user_email,
                    sum(a.tempo) as tempo,
                    count(a.tempo) as qtde,
                    min(a.data_inicio) as data_inicio,
                    max(a.data_fim) as data_fim,
                    sum(a.preco) as preco
                FROM (
                    SELECT distinct u.ID as user_id,
                            u.display_name,
                            u.user_email,
                            a.tempo as tempo,
                            a.tempo as qtde,
                            a.data_inicio as data_inicio,
                            a.data_fim as data_fim,
                            a.preco as preco " .

                    $from . 
                
                ") a ";

        $sql .= " group by user_id
                 order by display_name ";

        if($limite){
            $sql .= " LIMIT {$limite} OFFSET {$offset}";
        }

        return $wpdb->get_results($sql);
    }

}