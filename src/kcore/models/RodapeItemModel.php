<?php

use Exponencial\Core\Cache\CacheFactory;

/**
 * Model para organizar itens do rodapé
 *
 * @package	KCore/Models
 *
 * @author 	André paiva
 *
 * @since	24/08/2019
 */

class RodapeItemModel {


	public static function list($coluna)
	{
		global $wpdb;

		$sql = "SELECT * FROM rodape_itens where roi_coluna = {$coluna} ORDER BY roi_index ASC";

		return $wpdb->get_results($sql);
	}

	private static function get_nova_posicao($coluna)
	{
		global $wpdb;

		$sql = "SELECT coalesce(max(roi_index) + 1, 0) as roi_index FROM rodape_itens where roi_coluna = {$coluna}";

		return $wpdb->get_row($sql)->roi_index;
	}

	public static function salvar($titulo, $url, $coluna)
	{

		global $wpdb;

		$wpdb->insert(
				'rodape_itens',
				array(
						'roi_titulo' => $titulo,
						'roi_url' => $url,
						'roi_coluna' => $coluna,
						'roi_index' => self::get_nova_posicao($coluna)
				)
		);

        self::excluir_cache();
	}

	public static function editar($id, $titulo, $url, $index)
	{

		global $wpdb;

		$wpdb->update(
				'rodape_itens',
				array(
						'roi_titulo' => $titulo,
						'roi_url' => $url,
						'roi_index' => $index
				),
				array(
						'roi_id' => $id
				)
		);

        self::excluir_cache();
	}

	function excluir($roi_id)
	{
		global $wpdb;

		$sql = "DELETE FROM rodape_itens WHERE roi_id = {$roi_id}";

		$wpdb->query($sql);

		self::excluir_cache();
	}

	private static function excluir_cache()
    {
        $cache = new CacheFactory();

        $cache->getMemcached()->delete(CacheFactory::COMUM_FOOTER);
    }
}