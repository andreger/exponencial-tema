<?php 
/**
 * Model para organizar rotinas relacionadas aos Posts
 * 
 * @package	KCore/Models
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K4
 */ 

class PostModel {

	/**
	 * Recupera um post através do seu ID
	 * 
	 * @since K4
	 * 
	 * @param int $id Id do post. Referente à coluna ID da tabela wp_posts.
	 *
	 * @return object WP_Post
	 */ 

	public static function get_by_id($id)
	{
		global $wpdb;

		$sql = "SELECT * FROM wp_posts WHERE ID = '{$id}'";

		return $wpdb->get_row($sql);
	}

	/**
	 * Recupera um post através do seu slug
	 * 
	 * @since K4
	 * 
	 * @param string $slug Slug do post. Referente à coluna post_name da tabela wp_posts.
	 *
	 * @return object WP_Post
	 */ 

	public static function get_by_slug($slug)
	{
		global $wpdb;

		$sql = "SELECT * FROM wp_posts WHERE post_name = '{$slug}'";

		return $wpdb->get_row($sql);
	}

	/**
	 * Lista as categorias de um post
	 * 
	 * @since K4
	 * 
	 * @param int $id Id do post.
	 * @param int|NULL $parent Id da categoria-mãe
	 *
	 * @return array Lista de categorias. Registros da tabela wp_terms.
	 */ 
	
	public static function listar_categorias($id, $categoria_mae_id = NULL)
	{
		global $wpdb;

		$where_categoria_mae = $categoria_mae_id ? " AND tt.parent = {$categoria_mae_id} " : "";

		$sql = "SELECT t.* FROM wp_terms t 
					JOIN wp_term_taxonomy tt ON t.term_id = tt.term_id 
					JOIN wp_term_relationships tr ON tt.term_taxonomy_id = tr.term_taxonomy_id
				WHERE tr.object_id = {$id} {$where_categoria_mae} 
				ORDER BY t.name";

		return $wpdb->get_results($sql);
	}
	
	/**
	 * Lista os wp_post
	 *
	 * @since L1
	 *
	 * @param array $args Parâmetros da busca
	 *
	 * @return array Lista de posts. Registros da tabela wp_posts.
	 */
	
	public static function listar($args)
	{
	    global $wpdb;
	    
	    $select = isset($args['select']) ? $args['select'] : "*";
	    
	    $where_titulo = isset($args["titulo"]) ? "AND post_title LIKE '%{$args["titulo"]}%' " : "" ;
	    $where_tipo = isset($args["tipo"]) ? "AND post_type = '{$args["tipo"]}' " : "" ;
	    
	    $args['status'] = isset($args["status"]) && $args["status"] ? $args["status"] : [STATUS_POST_PUBLICADO];
	    $where_status = "AND post_status IN ('" . implode("','", $args['status']) . "')"; 
	       
	    $sql = "SELECT {$select} FROM wp_posts 
                WHERE 1=1 {$where_titulo} {$where_tipo} {$where_status}
                ORDER BY post_title ASC";
	    
	    return $wpdb->get_results($sql);
	}

}