<?php 
/**
 * Model para organizar rotinas relacionadas ao relatório de vendas por professor
 * 
 * @package	KCore/Models
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K5
 */ 

class RelatorioVendasPorProfessorModel {

	/**
	 * Calcula os acumulados do período
	 * 
	 * @since K5
	 * 
	 * @param string $data_ref Data em formato interpretável pelo strtotime
	 */ 

	public static function calcular_acumulados($data_ref)
	{
	    log_pedido("info", "Calculando acumulados professores");
	    
	    $mes_ano_data_ref = date('Y-m', strtotime($data_ref));
	    $mes_ano_atual = date('Y-m');
	    
	    while($mes_ano_data_ref != $mes_ano_atual) {
	        
	        log_pedido("info", "Mês/Ano: {$mes_ano_data_ref}");
		
    	    $inicio = date('Y-m-01', strtotime($data_ref));
    	    $fim =  date('Y-m-t', strtotime($data_ref));
    	    
    	    $mva = get_minimo_valor_acumulado_atual();
    // 	    echo "Mínimo valor acumulado atual: {$mva}<br>";
    // 	    echo "Data inicio: {$inicio}<br>";
    // 	    echo "Data fim: {$fim}<br>";
    	    
    	    $cache_id = CACHE_VENDAS_AGRUPADAS_POR_PROFESSOR_DATA . $mes_ano_data_ref;
    	    $cache = get_transient($cache_id);
    	    
    	    if(!$cache) {
    	        $cache = listar_vendas_agrupadas_por_professor($inicio, $fim);       
    	        set_transient($cache_id, $cache, HOUR_IN_SECONDS);
    	    }
    	    
    	    $lines = $cache;
    	    
    	    $data_acumulado = date('Y-m-01', strtotime($inicio));
    	    $data_anterior = date('Y-m-01', strtotime($inicio . ' -15 days'));
    	    
    // 	    echo "Data acumulado: {$data_acumulado}<br>";
    // 	    echo "Data anterior: {$data_anterior}<br>";
    	    
    // 	    echo "Excluindo acumulados da data: {$data_acumulado}<br>";
    	    excluir_pagamentos_acumulados_por_data($data_acumulado);
    	    
    	    foreach ($lines as $line) {
    	        
    	        if($line['professor_id']) {
    	            $acumulado_anterior = get_valor_pagamento_acumulado($line['professor_id'], $data_anterior);
    	            
    	            $soma = $line['lucro_professor'] + $acumulado_anterior;
    // 	            echo "Professor {$line['professor_id']} => Lucro do mês: {$line['lucro_professor']}, Acumulado anterior: {$acumulado_anterior}, Soma: {$soma}<br>";
    	            
    	            if($soma < $mva) {
    // 	                echo "Salvando valor acumulado: {$valor_acumulado}<br>";
    	                salvar_pagamento_acumulado($line['professor_id'], $data_acumulado, $soma);
    	            }
    	            
    	        }
    	    }
    	    
    	    // incrementa para o próximo mês
    	    $data_ref = date('Y-m-01', strtotime($inicio . " +32 days"));
    	    $mes_ano_data_ref = date('Y-m', strtotime($data_ref));
    	    
    	    log_pedido("info", "Finalizado cálculo de acumulados");
	    }
	}
	
}