<?php 

class ForumModel {

    public function listarProximos($ultimoId)
    {
        global $wpdb;

        $sql = "SELECT * FROM wp_posts WHERE post_type = 'forum' AND ID > {$ultimoId} ORDER BY ID LIMIT 100";

        return $wpdb->get_results($sql);
    }

    public function getProdutoId($forumId)
    {
        global $wpdb;

        $sql = "SELECT * FROM wp_postmeta WHERE meta_key = 'forum_id' AND meta_value = '{$forumId}'";

        if($row = $wpdb->get_row($sql)) {
            return $row->post_id;
        }

        return null;
    }

}