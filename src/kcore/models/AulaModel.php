<?php
/**
 * Model para organizar rotinas relacionadas às aulas dos cursos
 * 
 * @package	KCore/Models
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	M1
 */ 

class AulaModel {
	
    /**
     * Lista as aulas de um produto
     *
     * @since M1
     *
     * @param int $produto_id Id do produto
     * @param int $pedido_id Id do pedido
     *  
     * @return array Lista de arquivos
     */
    
    public static function listar_aulas($produto_id, $pedido_id = null, $tipo_aula = TIPO_AULA_PDF, $apenas_disponiveis = FALSE)
    {
        $aulas = [];
        $nomes = get_post_meta($produto_id, "aulas_nome", true);
        
        foreach ($nomes as $aula_index => $nome) {
            
            $arquivos = self::listar_arquivos_por_aula($produto_id, $aula_index, $tipo_aula);
            
            $aula = [
                'index' => $aula_index,
                'nome' => $nome,
                'arquivos' => $arquivos
            ];
            
            if($pedido_id) {
                $aula['disponivel'] = is_aula_disponivel_para_download($produto_id, $nome, $pedido_id);
            }
			
			if( $apenas_disponiveis == FALSE || $aula['disponivel'] )
			{
				$aulas[] = $aula;
			}
        }
        
        return $aulas;
    }
    
    
	/**
	 * Lista as URLs dos arquivos de uma aula de um produto
	 * 
	 * @since M1
	 * 
	 * @param int $produto_id Id do produto
	 * @param int $aula_index Index da aula
	 * 
	 * @return array Lista de arquivos
	 */ 
	
	public static function listar_arquivos_urls_por_aula($produto_id, $aula_index, $tipo_aula = TIPO_AULA_PDF)
	{
		$arquivos = self::get_aulas_por_tipo($produto_id, $tipo_aula);
		
		if(isset($arquivos) && isset($arquivos[$aula_index])) {
		    return $arquivos[$aula_index];
		}
	}

	public static function get_aulas_por_tipo($produto_id, $tipo_aula)
	{
		$arquivos = [];

		if($tipo_aula == TIPO_AULA_PDF)
		{
			$arquivos = get_post_meta($produto_id, "aulas_arquivo", true);
		}
		elseif($tipo_aula == TIPO_AULA_MAPA)
		{
			$arquivos = get_post_meta($produto_id, "aulas_mapa", true);
		}
		elseif($tipo_aula == TIPO_AULA_RESUMO)
		{
			$arquivos = get_post_meta($produto_id, "aulas_resumo", true);
		}

		return $arquivos;
	}
	
	/**
	 * Lista os arquivos de uma aula de um produto
	 *
	 * @since M1
	 *
	 * @param int $produto_id Id do produto
	 * @param int $aula_index Index da aula
	 *
	 * @return array Lista de arquivos
	 */
	
	public static function listar_arquivos_por_aula($produto_id, $aula_index, $tipo_aula = TIPO_AULA_PDF)
	{
	    KLoader::helper("UrlHelper");
	    
	    $urls = self::listar_arquivos_urls_por_aula($produto_id, $aula_index, $tipo_aula);
	    $arquivos = [];
	    
	    foreach ($urls as $url) {
	        $arquivo = $_SERVER["DOCUMENT_ROOT"] . UrlHelper::remover_dominio($url);
	        $arquivos[] = $arquivo;
	    }
	    
	    return $arquivos;
	}

}