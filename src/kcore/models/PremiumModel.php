<?php
/**
 * Model para organizar rotinas específicas dos produtos premiums
 *
 * @package	KCore/Models
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	L1
 */
KLoader::model("ProdutoModel");

class PremiumModel {
    
    /**
     * Verifica se o acesso de um item de um premium foi requerido ou não
     *
     * @since L1
     *
     * @param int $pedido_id Id do pedido
     * @param int $produto_id Id do produto
     * @param int $item_id Id do item do premium
     *
     * @return bool
     */
    
    public static function acesso_foi_requerido($pedido_id, $produto_id, $item_id)
    {
        global $wpdb;
        
        $sql = "SELECT * FROM premium_log WHERE pedido_id = {$pedido_id} AND produto_id = {$produto_id} AND item_id = {$item_id}";
        
        return $wpdb->get_results($sql) ? true : false;
        
    }
    
    /**
     * Lista os acessos requeridos de um produto em um pedido
     *
     * @since L1
     *
     * @param int $pedido_id Id do pedido
     * @param int $produto_id Id do produto
     *
     * @return array Lista de IDs de itens com acesso requerido
     */
    
    public static function Listar_acessos_requeridos($pedido_id, $produto_id)
    {
        global $wpdb;
        
        $sql = "SELECT * FROM premium_log WHERE pedido_id = {$pedido_id} AND produto_id = {$produto_id}";
        
        $resultado = $wpdb->get_results($sql);
        $items = [];
        
        if($resultado) {
            foreach ($resultado as $linha) {
                $items[] = $linha->item_id;
            }
        }
        
        return $items;
    }
    
    /**
     * Realiza o agendamento de um processamento de produto premium
     *
     * @since L1
     *
     * @param int $ano_mes Ano/mês do processamento desejado
     * @param int $produto_id Id do produto
     */
    
    public static function agendar($ano_mes, $produto_id)
    {
        global $wpdb;
        
        /* === tratamento dos parâmetros === */
        
        if(!(filter_var($ano_mes, FILTER_VALIDATE_INT))) throw new InvalidArgumentException("Argumento 'ano_mes' precisa ser inteiro");
        if(!(filter_var($produto_id, FILTER_VALIDATE_INT))) throw new InvalidArgumentException("Argumento 'produto_id' precisa ser inteiro");
        
        /* === fluxo principal === */
        
        $info = [
            'pro_id' => $produto_id,
            'pra_ano_mes' => $ano_mes,
            'pra_status' => PREMIUM_STATUS_PENDENTE,
            'pra_data' => date("Y-m-d H:i:s")
        ];
        
        $wpdb->insert("premium_agendamentos", $info);
        
    }
    
    /**
     * Atualiza o status do agendamento
     *
     * @since L1
     *
     * @param int $agendamento_id Id do agendamento
     * @param int $status Status do agendamento
     */
    
    public static function atualizar_status_agendamento($agendamento_id, $status)
    {
        global $wpdb;
        
        $wpdb->update('premium_agendamentos', ['pra_status' => $status], ['pra_id' => $agendamento_id]);
    }
    
    /**
     * Lista os agendamentos de produto premium
     *
     * @since L1
     *
     * @return array
     */
    
    public static function listar_agendamentos($filtros = null)
    {
        global $wpdb;
        
        $where_status = "";
        $limit = 1000;
        
        if($filtros) {
            $where_status = isset($filtros['status']) ? " AND pra_status = {$filtros['status']} " : "";
            $limit = isset($filtros['status']) ? "{$filtros['limit']}" : 1000;
        }
        
        $sql = "SELECT * FROM premium_agendamentos pa JOIN wp_posts p ON p.ID = pa.pro_id WHERE 1=1 {$where_status} ORDER BY pra_data DESC LIMIT {$limit}";
        
        return $wpdb->get_results($sql);
        
    }
    
    /**
     * Excluir a informação ano/mês de um produto premium
     *
     * @since L1
     *
     * @param int $ano_mes Ano / mês
     * @param int $produto_id Id do produto
     */
    
    public static function excluir_info($ano_mes, $produto_id)
    {
        global $wpdb;
        
        $info = [
            'pri_ano_mes' => $ano_mes,
            'pro_id' => $produto_id,
        ];
        
        $wpdb->delete("premium_info", $info);
    }
    
    /**
     * Excluir a informação de professores de ano/mês de um produto premium
     *
     * @since L1
     *
     * @param int $ano_mes Id do pedido
     * @param int $produto_id Id do produto
     */
    
    public static function excluir_info_professores($ano_mes, $produto_id)
    {
        global $wpdb;
        
        $info = [
            'pip_ano_mes' => $ano_mes,
            'pro_id' => $produto_id,
        ];
        
        $wpdb->delete("premium_info_professores", $info);
    }
    
    /**
     * Lista os itens fixados de um produto premium de um pedido
     * 
     * @since L1
     *
     * @param int $pedido_id Id do pedido
     * @param int $produto_id Id do produto
     * 
     * @return array Lista de IDs de itens fixados
     */
    
    public static function listar_itens_fixados($pedido_id, $produto_id)
    {
        global $wpdb;
        
        $sql = "SELECT * FROM premium_itens_fixados 
                WHERE ped_id = {$pedido_id} AND pro_id = {$produto_id}"; 
        
        $resultado = $wpdb->get_results($sql);
        $items = [];
        
        if($resultado) {
            foreach ($resultado as $linha) {
                $items[] = $linha->ite_id;
            }
        }
        
        return $items;
    }
    
    /**
     * Fixa o item de um produto premium de um pedido
     *
     * @since L1
     *
     * @param int $pedido_id Id do pedido
     * @param int $produto_id Id do produto
     * @param int $item_id Id do item
     */
    
    public static function fixar_item($pedido_id, $produto_id, $item_id)
    {
        global $wpdb;
        
        $wpdb->insert('premium_itens_fixados', [
            'ped_id' => $pedido_id,
            'pro_id' => $produto_id,
            'ite_id' => $item_id
        ]);   
    }
    
    /**
     * Desfixa o item de um produto premium de um pedido
     *
     * @since L1
     *
     * @param int $pedido_id Id do pedido
     * @param int $produto_id Id do produto
     * @param int $item_id Id do item
     */
    
    public static function desfixar_item($pedido_id, $produto_id, $item_id)
    {
        global $wpdb;
        
        $wpdb->delete('premium_itens_fixados', [
            'ped_id' => $pedido_id,
            'pro_id' => $produto_id,
            'ite_id' => $item_id
        ]);
    }
    
    /**
     * Excluir a informação ano/mês de um produto premium
     *
     * @since L1
     *
     * @param int $ano_mes Id do pedido
     * @param int $produto_id Id do produto
     */
    
    /**
     * Verifica pedido é pedido oculto premium, ou seja, se o pedido faz parte de um item de premium
     *
     * @since L1
     *
     * @param int $pedido_id Id do pedido
     *
     * @return bool
     */
    
    public static function is_pedido_oculto($pedido_id)
    {
        global $wpdb;
        
        $sql = "SELECT * FROM premium_log WHERE pedido_oculto_id = {$pedido_id}";
        
        return $wpdb->get_row($sql) ? true : false;
    }
    
    
    /**
     * Processa as informações de um produto premium em um determinado mês/ano
     *
     * @since L1
     * 
     * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/kb/items/70
     *
     * @param int $ano_mes Ano e mês no formato YYYYMM
     * @param int $produto_id Id do produto
     */
    
    public static function processar_info($ano_mes, $produto_id)
    {
        /* === tratamento dos parâmetros === */
        
        if(!(filter_var($ano_mes, FILTER_VALIDATE_INT))) throw new InvalidArgumentException("Argumento 'ano_mes' precisa ser inteiro");
        if(!(filter_var($produto_id, FILTER_VALIDATE_INT))) throw new InvalidArgumentException("Argumento 'produto_id' precisa ser inteiro");
        
        /* === fluxo principal === */
        
        $produto = ProdutoModel::get_by_id($produto_id);
        
        if(!$produto->pro_is_premium) throw new UnexpectedValueException("Produto {$produto_id} não é premium");
       
        self::excluir_info($ano_mes, $produto_id);
        $info_id = self::salvar_info($ano_mes, $produto_id, $produto->pro_preco);
       
        $filtros = [
            'excluir_simulados' => true,
            'excluir_cadernos' => true
        ];
        
        $items = ProdutoModel::listar_premium_items($produto_id, -1, $filtros);
       
        $mes = substr($ano_mes, 4, 2);
        $ano = substr($ano_mes, 0, 4);
       
        $produto_post_data_max = date("Y-m-t 23:59:59", strtotime($ano . "-" . $mes . "-" . "01"));
        $infos = [];
        $total_premium = 0;

        foreach ($items as $item) {
            $produto_vendas_ate = get_data_vendas_ate($item->post_id)
                ? converter_para_yyyymmdd(get_data_vendas_ate($item->post_id)) . " 23:59:59"
                : date('Y-m-d 00:00:00', strtotime("+ 100 years")); // se não tem data limite de vendas, define esta data com uma data futura distante

            // verifica se produto foi lançado (post_date) antes da data máxima (último dia do mês a ser considerado)
            // e se a data máxima de vendas é anterior à data máxima (último dia do mês a ser considerado)
            if($item->post_date <= $produto_post_data_max && $produto_post_data_max <= $produto_vendas_ate) {
                $total_premium += $item->pro_preco;
                
                $info =  [
                    'pri_id' => $info_id,
                    'pro_item_id' => $item->post_id,
                    'pri_item_preco' => $item->pro_preco,
                ];

                array_push($infos, $info);
           }
       }
       
       foreach ($infos as $info) {
           $info['pri_item_percentual'] = $info['pri_item_preco'] / $total_premium * 100;
           self::salvar_info_item($info);
       }
       
       return true;
       
    }
    
    /**
     * Processa as informações de um produto premium em um determinado mês/ano
     *
     * @since L1
     *
     * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/kb/items/70
     *
     * @param int $ano_mes Ano e mês no formato YYYYMM
     * @param int $produto_id Id do produto
     */
    
    public static function processar_info_professores($ano_mes, $produto_id)
    {
        /* === tratamento dos parâmetros === */
        
        if(!(filter_var($ano_mes, FILTER_VALIDATE_INT))) throw new InvalidArgumentException("Argumento 'ano_mes' precisa ser inteiro");
        if(!(filter_var($produto_id, FILTER_VALIDATE_INT))) throw new InvalidArgumentException("Argumento 'produto_id' precisa ser inteiro");
        
        /* === fluxo principal === */
        
        $produto = ProdutoModel::get_by_id($produto_id);
        
        if(!$produto->pro_is_premium) throw new UnexpectedValueException("Produto {$produto_id} não é premium");
        
        self::excluir_info_professores($ano_mes, $produto_id);
        
        $parciais = self::listar_valor_parcial($ano_mes, $produto_id);
        
        if($parciais) {
            $total_itens = self::get_valor_total_produtos($produto_id, $ano_mes);
            
            foreach ($parciais as $parcial) {
                $info = [
                    'pro_id' => $produto_id,
                    'pip_ano_mes' => $ano_mes,
                    'usu_id' => $parcial->user_id,
                    'pip_soma' => $parcial->total,
                    'pip_percentual' => $parcial->total / $total_itens * 100
                ];
                
                self::salvar_info_professores($info);
            }
        }
        
        return true;
    }
    
    /**
     * Recupera o log de um item de produto de pedido
     *
     * @since L1
     *
     * @param int $pedido_id Id do pedido
     * @param int $produto_id Id do produto
     * @param int $item_id Id do item do premium
     *
     * @return bool
     */
    
    public static function get_log($pedido_id, $produto_id, $item_id)
    {
        global $wpdb;
        
        $sql = "SELECT * FROM premium_log WHERE pedido_id = {$pedido_id} AND produto_id = {$produto_id} AND item_id = {$item_id}";
        
        return $wpdb->get_row($sql);
    }
    
    /**
     * Lista os anos/meses
     *
     * @since L1
     *
     * @return array Lista de anos/meses
     */
    
    public static function listar_anos_meses()
    {
//         global $wpdb;
        
//         $sql = "SELECT DISTINCT pri_ano_mes AS chave, CONCAT(SUBSTR(pri_ano_mes, 1, 4),'-', SUBSTR(pri_ano_mes, 5, 4)) AS valor FROM premium_info ORDER BY chave DESC ";
        
//         return $wpdb->get_results($sql);

            $meses_anos = [];
            
            $inicio = "2019-09-01";
            $atual = date("Y-m-01", strtotime("-1 month"));
            
            while ($atual >= $inicio) {
                $mes_ano = new stdClass();
                $mes_ano->chave = date("Ym", strtotime($atual));
                $mes_ano->valor = date("Y-m", strtotime($atual));
                
                $meses_anos[] = $mes_ano;
                
                $atual = date("Y-m-01", strtotime("{$atual} -1 month"));
            }
            
            return $meses_anos;
    }
    
    /**
     * Builder da operação de listar informações dos produtos premiums
     *
     * @since L1
     * 
     * @param array $filtros Filtros
     *
     * @return string SQL da consulta
     */
    
    public static function listar_premium_info_builder($filtros)
    {
        $where_produtos = $filtros["produtos"] ? " AND p.post_id IN (" . implode(",", $filtros["produtos"]) . ") " : "";
        $where_anos_meses = $filtros["anos_meses"] ? " AND pi.pri_ano_mes = {$filtros['anos_meses']} " : "";
        $where_status = " AND post_status = '" . STATUS_POST_PUBLICADO ."' ";
        
        $join_professor = $filtros["professor_id"] ? " JOIN produtos_professores pp ON pp.post_id = pi.pro_id " : "";
        $where_professor = $filtros["professor_id"] ? " AND pp.user_id = {$filtros["professor_id"]} " : "";
        
        $sql = "SELECT * FROM premium_info pi 
                    JOIN produtos p ON pi.pro_id = p.post_id
                    JOIN wp_posts po ON po.id = p.post_id 
                    {$join_professor} 
                WHERE pro_is_premium = 1 {$where_status} {$where_produtos} {$where_anos_meses} 
                    {$where_professor} ORDER BY post_title ";
        
        return $sql;
    }
    
    /**
     * Lista as informações dos produtos premiums
     *
     * @since L1
     * 
     * @param array $filtros Filtros
     *
     * @return array Lista com arrays associativos de produtos
     */
    
    public static function listar_premium_info($filtros = null)
    {
        global $wpdb;
        
        $sql = self::listar_premium_info_builder($filtros);
        
        return $wpdb->get_results($sql);
    }
    
    
    /**
     * Lista as informações dos itens dos produtos premiums
     *
     * @since L1
     *
     * @param int $info_id Id da informação
     * @param array $filtros Filtros
     *
     * @return array Lista com arrays associativos de produtos
     */
    
    public static function listar_premium_info_items($info_id, $filtros)
    {
        global $wpdb;
        
        $where_excluir_cadernos = $filtros['excluir_cadernos'] ? " AND pro_is_caderno = 0 " : "";
        $where_excluir_gratuitos = $filtros['excluir_gratuitos'] ? " AND pri_item_preco > 0 " : "";
        $where_excluir_mapas_mentais = $filtros['excluir_mapas'] ? " AND pro_is_mapa_mental = 0 " : "";

        $sql = "SELECT * FROM premium_info_itens pi
                    JOIN produtos p ON pi.pro_item_id = p.post_id
                    JOIN wp_posts po ON po.id = p.post_id
                WHERE pri_id = {$info_id} {$where_excluir_cadernos} {$where_excluir_gratuitos} {$where_excluir_mapas_mentais} ORDER BY post_title";
        
        return $wpdb->get_results($sql);
    }
    
    /**
     * Conta as informações dos produtos premiums
     *
     * @since L1
     * 
     * @param array $filtros Filtros
     *
     * @return int Número de produtos
     */
    
    public static function contar_premium_info($filtros = null)
    {
        global $wpdb;
        
        $sql = self::listar_premium_info_builder($filtros);
        $sql = "SELECT COUNT(*) FROM ($sql) tmp";
        
        return $wpdb->get_var($sql);
    }
    
    /**
     * Lista os produtos premiums
     *
     * @since L1
     *
     * @param array $filtros Filtros
     *
     * @return array Lista com arrays associativos de produtos
     */
    
    public static function listar_produtos($professor_id = null)
    {
        global $wpdb;

        $where_status = " AND post_status = '" . STATUS_POST_PUBLICADO ."' ";

        $join_professor = $professor_id ? " JOIN produtos_professores pp ON pp.post_id = p.post_id " : "";
        $where_professor = $professor_id ? " AND pp.user_id = $professor_id " : "";
       
        $sql = "SELECT * FROM produtos p 
                    JOIN wp_posts po ON po.id = p.post_id {$join_professor}
                WHERE pro_is_premium = 1 {$where_status} {$where_professor} ORDER BY post_title ";

        log_temp("Listar produtos premiums query: " . $sql);

        return $wpdb->get_results($sql);
    }
    
    
    /**
     * Salva a requisição de acesso a um item na tabela premium_log
     *
     * @since L1
     *
     * @param int $pedido_id Id do pedido
     * @param int $produto_id Id do produto
     * @param int $item_id Id do item do premium
     * @param int $pedido_oculto_id Id do pedido oculto gerado pela requisição de acesso
     */
    
    public static function salvar_log($pedido_id, $produto_id, $item_id, $pedido_oculto_id)
    {
        global $wpdb;
        
        $log = [
            'pedido_id' => $pedido_id,
            'produto_id' => $produto_id,
            'item_id' => $item_id,
            'pedido_oculto_id' => $pedido_oculto_id,
            'prl_data' => get_data_hora_agora()
        ];
        
        $wpdb->replace("premium_log", $log);
    }
    
    /**
     * Salva a informação ano/mês de um produto premium
     *
     * @since L1
     *
     * @param int $ano_mes Id do pedido
     * @param int $produto_id Id do produto
     * @param float $preco Id do item do premium
     * 
     * @return int Id da informação
     */
    
    public static function salvar_info($ano_mes, $produto_id, $preco)
    {
        global $wpdb;
        
        $info = [
            'pri_ano_mes' => $ano_mes,
            'pro_id' => $produto_id,
            'pri_preco' => $preco,
        ];
        
        $wpdb->insert("premium_info", $info);
        
        return $wpdb->insert_id;
    }
    /**
     * Salva a informação ano/mês dos itens de um produto premium
     *
     * @since L1
     *
     * @param array $info Dados do item do premium
     */
    
    public static function salvar_info_item($info)
    {
        global $wpdb;
        
        $wpdb->insert("premium_info_itens", $info);
    }
    
    /**
     * Salva a informação de professores ano/mês dos itens de um produto premium
     *
     * @since L1
     *
     * @param array $info Dados do item do premium
     */
    
    public static function salvar_info_professores($info)
    {
        global $wpdb;
        
        $wpdb->insert("premium_info_professores", $info);
    }
    
    /**
     * Calcula o valor total do preço dos itens de um produto premium
     *
     * @since L1
     *
     * @param int $produto_id Id do produto
     *
     * @return float Valor total do preço dos itens
     */
    
    public static function get_valor_total_items($produto_id)
    {
        $items = ProdutoModel::listar_premium_items($produto_id, -1, []);
        
        $total = 0;
        foreach ($items as $item) {
            $total += $item->pro_preco;
        }
        
        return $total;
    }

	/**
	 * Monta a cláusula WHERE para o relatório de pedidos premium
	 * 
	 * @since L1
	 * 
	 * @param Array $filtro contendo os filtros
	 * 
	 * @return string contendo os filtros compilados
	 */
	private static function query_where_pedidos_premium($filtro)
	{
		$where_pedido = "";
		if($filtro['pedido']){
			$where_pedido = " AND pp.pedido_id = " . $filtro['pedido'];
		}

		$where_produto = "";
		if($filtro['produto']){
			$where_produto = " AND pp.produto_id = " . $filtro['produto'];
		}

		$where_item = "";
		if($filtro['item']){
			$where_item = " AND pp.item_id = " . $filtro['item'];
		}

		$where_pedido_oculto = "";
		if($filtro['pedido_oculto']){
			$where_pedido_oculto = " AND pp.pedido_oculto_id = " . $filtro['pedido_oculto'];
		}

		$where_data_req_ini = "";
		if($filtro['data_req_ini']){
			$where_data_req_ini = " AND pp.prl_data >= '" . converter_para_yyyymmdd( $filtro['data_req_ini'] ) . " 00:00:00' ";
		}

		$where_data_req_fim = "";
		if($filtro['data_req_fim']){
			$where_data_req_fim = " AND pp.prl_data <= '" . converter_para_yyyymmdd( $filtro['data_req_fim'] ) . " 23:59:59' ";
		}

		$where = 
			" WHERE 1=1
				{$where_pedido}
				{$where_produto}
				{$where_item}
				{$where_pedido_oculto}
				{$where_data_req_ini}
				{$where_data_req_fim}
			";

		return $where;
	}

	/**
	 * Conta os pedidos premium com base no filtro enviado
	 * 
	 * @since L!
	 * 
	 * @param Array $filtro contendo os filtros
	 * 
	 * @return int total de pedidos premium encontrados com base no filtro
	 */
	public static function contar_pedidos_premium($filtro)
	{
		global $wpdb;

		$where = self::query_where_pedidos_premium($filtro);

		$sql = "SELECT COUNT(*) FROM premium_log pp {$where} ";

		return $wpdb->get_var($sql);
	}

	/**
	 * Lista pedidos premium
	 * 
	 * @since L1
	 * 
	 * @param Array $filtro contendo os filtros
	 * @param int $offset Offset da busca
     * @param int $limite Limite de resultados da busca
	 */
	public static function listar_pedidos_premium($filtro, $offset = 0, $limite = RELATORIO_PEDIDOS_PREMIUM_POR_PAGINA)
	{
		global $wpdb;

		$where = self::query_where_pedidos_premium($filtro);

		$sql = 
			"SELECT pp.pedido_id
					, pp.produto_id, pp_prod.post_title as nome_produto
					, pp.item_id
					, pp_item.post_title as nome_item
					, pp.pedido_oculto_id
					, pp.prl_data
			FROM premium_log pp
				INNER JOIN wp_posts pp_prod ON pp_prod.ID = pp.produto_id
				INNER JOIN wp_posts pp_item ON pp_item.ID = pp.item_id			
			{$where}
			ORDER BY pp.pedido_id ASC
        ";
        
        if($limite)
        {
			$sql .= " LIMIT {$limite}";
        }

        if($offset)
        {
			$sql .= " OFFSET {$offset}";
        }
        
		return $wpdb->get_results($sql);

	}
	
	/**
	 * Lista os pedidos com produtos premium
	 *
	 * @since L1
	 *
	 * @param string $inicio Data inicial
	 * @param string $fim Data final
	 *
	 * @return array Array contendo os pedidos
	 */
	public static function listar_pedidos_com_produto_premium($filtros = []){
	    
	    global $wpdb;
	    
	    if(!$filtros['inicio']) {
	        $filtros['inicio'] = date('Y-m-d 00:00:00');
	    }
	    
	    if(!$filtros['fim']) {
	        $filtros['fim'] = date('Y-m-d 23:59:59');
	    }
	    
	    $sql = "SELECT * FROM wp_vendas v
	               JOIN produtos p ON p.post_id = v.ven_curso_id
                WHERE
                   v.ven_data >= '{$filtros['inicio']}' AND v.ven_data <= '{$filtros['fim']}'
	               AND p.pro_is_premium = 1";
	    return $wpdb->get_results($sql);
	}
	
	/**
	 * Recupera o valor total dos produtos do premium em um perído
	 *
	 * @since L1
	 *
	 * @param int $produto_id Id do produto
	 * @param string $ano_mes Período no formato yyyymm
	 *
	 * @return float
	 */
	public static function get_valor_total_produtos($produto_id, $ano_mes){
	    
	    global $wpdb;
	    
	    $sql = "SELECT SUM(pri_item_preco) FROM premium_info pi
	               JOIN premium_info_itens pii ON pi.pri_id = pii.pri_id
                   JOIN produtos p ON p.post_id = pii.pro_item_id

                WHERE
                   pi.pro_id = {$produto_id} AND pi.pri_ano_mes = '{$ano_mes}'";

	    return $wpdb->get_var($sql);
	}
	
	/**
	 * Lista os professores dos itens dos produtos premiums
	 *
	 * @since L1
	 *
	 * @param int $info_id Id da informação
	 * @param int $professor_id Id do professor
	 *
	 * @return array Lista com arrays associativos de produtos
	 */
	
	public static function listar_premium_info_items_professores($info_id, $professor_id = null)
	{
	    global $wpdb;
	    
	    $where_professor = $professor_id ? " AND pp.user_id = {$professor_id} " : "";
	    
	    $sql = "SELECT pi.*, pp.*, u.display_name, p.post_title FROM premium_info_itens pi
                    JOIN produtos_professores pp ON pi.pro_item_id = pp.post_id
                    JOIN wp_users u ON pp.user_id = u.ID
                    JOIN wp_posts p ON p.ID = pi.pro_item_id 
                    JOIN produtos pr ON pr.post_id = pi.pro_item_id
                WHERE pri_id = {$info_id}
                    {$where_professor}";

	    return $wpdb->get_results($sql);
	}
	
	/**
	 * Recupera o valor total dos produtos do premium em um perído
	 *
	 * @since L1
	 *
	 * @param int $produto_id Id do produto
	 * @param string $anomes Período no formato yyyymm
	 *
	 * @return float
	 */
	public static function get_premium_info($produto_id, $ano_mes){
	    
	    global $wpdb;
	    
	    $sql = "SELECT * FROM premium_info pi
                WHERE
                   pi.pro_id = {$produto_id} AND pi.pri_ano_mes = '{$ano_mes}'";
	    
	    return $wpdb->get_row($sql);
	}
	
	/**
	 * Lista os itens e professores de um produto premium
	 *
	 * @since L1
	 *
	 * @param int $premium_id Id do produto premium
	 * @param int $ano_mes Ano e mês
	 *
	 * @return array Lista com arrays associativos de produtos
	 */
	
	public static function listar_produtos_items_professores($premium_id, $ano_mes)
	{
	    global $wpdb;
	    
	    $sql = "SELECT pi.*, pp.*, pd.pro_preco, pd.pro_disponivel_ate, p.post_status, u.display_name, p.post_title FROM premium_info_itens pi
                    JOIN premium_info pr ON pr.pri_id = pi.pri_id
                    JOIN produtos_professores pp ON pi.pro_item_id = pp.post_id
                    JOIN produtos pd ON pd.post_id = pp.post_id
                    JOIN wp_users u ON pp.user_id = u.ID
                    JOIN wp_posts p ON p.ID = pi.pro_item_id
                WHERE pr.pri_ano_mes = {$ano_mes} AND pro_id = {$premium_id}";
       
        return $wpdb->get_results($sql);
	}
	
    /**
     * Lista os itens e professores de um produto premium
     *
     * @since L1
     *
     * @param int $premium_id Id do produto premium
     * @param int $ano_mes Ano e mês
     *
     * @return array Lista com arrays associativos de produtos
     */
        
    public static function get_total_preco_produtos_items_professores($premium_id, $ano_mes)
    {
        global $wpdb;
        
        $sql = "SELECT pi.*, pp.*, pd.pro_preco, u.display_name, p.post_title FROM premium_info_itens pi
                JOIN premium_info pr ON pr.pri_id = pi.pri_id
                JOIN produtos_professores pp ON pi.pro_item_id = pp.post_id
                JOIN produtos pd ON pd.post_id = pp.post_id
                JOIN wp_users u ON pp.user_id = u.ID
                JOIN wp_posts p ON p.ID = pi.pro_item_id
            WHERE pr.pri_ano_mes = {$ano_mes} AND pro_id = {$premium_id}";
        
        // 	    echo $sql;
        return $wpdb->get_results($sql);
	}
	
	/**
	 * Lista premiuns, itens e professores por período
	 *
	 * @since L1
	 *
	 * @param int $ano_mes Ano e mês
	 *
	 * @return array Lista com arrays associativos de info
	 */
	
	public static function listar_premium_item_professores_por_periodo($ano_mes, $professor_id = null)
	{
	    global $wpdb;

        $where_professor = $professor_id ? " AND pp.user_id = {$professor_id} " : "";
	    
	    $sql = "SELECT DISTINCT pp.user_id, pi.pro_id, u.display_name, p.post_title FROM premium_info pi
                	JOIN premium_info_itens pii ON pi.pri_id = pii.pri_id
                	JOIN produtos_professores pp ON pii.pro_item_id = pp.post_id
                    JOIN wp_users u ON u.ID = pp.user_id 
                    JOIN wp_posts p ON p.ID = pi.pro_id 
                    JOIN produtos pd ON pd.post_id = p.ID
                WHERE pri_ano_mes = '{$ano_mes}' {$where_professor}
                    ORDER BY user_id
                ";
	    
// 	    AND pro_com_correcao = 0 AND pro_is_turma_coaching = 0
// 	    AND pro_is_simulado = 0 AND pro_is_caderno = 0 AND pro_is_coaching = 0
	    
	    return $wpdb->get_results($sql);
	}
	
	/**
	 * Lista pedidos de premiuns agrupados por período
	 *
	 * @since L1
	 *
	 * @param string $inicio Data de início
	 * @param string $fim Data final
	 *
	 * @return array Lista com arrays associativos de info
	 */
	
	public static function listar_pedidos_premium_agrupados_por_produtos($inicio, $fim, $id = null) 
	{
	    global $wpdb;
	    
	    if($id) {
	        $where = " AND v.ven_order_id = $id ";
	    }
	    else {
	        $where = " AND ven_data >= '$inicio' AND ven_data <= '$fim' ";
	    }
	    
	    $sql = "SELECT v.ven_curso_id,
        	    sum(ven_valor_venda) as total_valor_venda,
        	    sum(ven_desconto) as total_desconto,
        	    sum((ven_valor_venda - ven_desconto - ven_reembolso) * ven_aliquota / 100) as total_imposto,
        	    sum(ven_pagseguro) as total_pagseguro,
        	    sum((ven_valor_venda - ven_desconto - ven_reembolso) * ven_folha_dirigida) as total_folha_dirigida,
        	    sum(ven_reembolso) as total_reembolso
    	    FROM wp_vendas v
    	       JOIN produtos p ON p.post_id = v.ven_curso_id
    	    WHERE
    	       pro_is_premium = 1 {$where}
            GROUP BY v.ven_curso_id";
	        
	    return $wpdb->get_results($sql);
	}
	
	/**
	 * Somar valores parciais de itens de um professor
	 *
	 * @since L1
	 *
	 * @param int $premium_id Id do produto premium
	 * @param int $ano_mes Ano e mês
	 *
	 * @return float Valor parcial de item de um professor
	 */
	
	public static function somar_valor_parcial_por_professor($premium_id, $ano_mes, $professor_id)
	{
	    global $wpdb;
	    
	    $sql = "SELECT SUM(pd.pro_preco * pru_percentual / 100) as total FROM premium_info_itens pi
                    JOIN premium_info pr ON pr.pri_id = pi.pri_id
                    JOIN produtos_professores pp ON pi.pro_item_id = pp.post_id
                    JOIN produtos pd ON pd.post_id = pp.post_id
                WHERE pr.pri_ano_mes = {$ano_mes} AND pro_id = {$premium_id} AND pp.user_id = $professor_id";
	    
	    return $wpdb->get_var($sql);
	}
	
	/**
	 * Listar valores parciais de itens dos professores
	 *
	 * @since L1
	 *
	 * @param int $ano_mes Ano e mês
	 * @param int $premium_id Id do produto premium
	 *
	 * @return array Lista com valores parciais de professores
	 */
	
	public static function listar_valor_parcial($ano_mes, $premium_id)
	{
	    global $wpdb;
	    
	    $sql = "SELECT user_id, SUM(pd.pro_preco * pru_percentual / 100) as total FROM premium_info_itens pi
                    JOIN premium_info pr ON pr.pri_id = pi.pri_id
                    JOIN produtos_professores pp ON pi.pro_item_id = pp.post_id
                    JOIN produtos pd ON pd.post_id = pp.post_id
                WHERE pr.pri_ano_mes = '{$ano_mes}' AND pro_id = {$premium_id} GROUP BY pp.user_id";
	    
	    return $wpdb->get_results($sql);
	}
	
	/**
	 * Insere configurações de premiums
	 *
	 * @since L1
	 *
	 * @param array $dados Configurações dos premiums
	 */
	
	public static function set_configs($dados)
	{
	    global $wpdb;
	    
	    if(self::get_configs($dados['ppc_data'])) {
	       $wpdb->update('premium_configs', $dados, ['ppc_data' => $dados['ppc_data']]);
	    }
	    else {
	       $wpdb->insert('premium_configs', $dados);
	    }
	}
	
	/**
	 * Recupera uma configuração
	 *
	 * @since L1
	 *
	 * @param string $data Data da configuração
	 * 
	 * @return array Array associativo das configurações
	 */
	
	public static function get_configs($data)
	{
	    global $wpdb;
	    
	    $sql = "SELECT * FROM premium_configs 
                WHERE ppc_data = '{$data}'";
	    
	    return $wpdb->get_row($sql);
	}
	
	/**
	 * Recupera uma configuração por data
	 *
	 * @since L1
	 *
	 * @param array $dados Configurações dos premiums
	 */
	
	public static function get_config_atual()
	{
	    global $wpdb;
	    
	    $sql = "SELECT * FROM premium_configs
                WHERE ORDER BY ppc_data DESC LIMIT 1";
	    
	    return $wpdb->get_row($sql);
	}
	
	/**
	 * Recupera a configuração ativa em uma determinada data
	 *
	 * @since L1
	 *
	 * @param string $data Data da configuração
	 *
	 * @return array Array associativo das configurações
	 */
	
	public static function get_config_ativa_por_data($data)
	{
	    global $wpdb;
	    
	    $sql = "SELECT * FROM premium_configs
                WHERE ppc_data <= '{$data}' ORDER BY ppc_data DESC LIMIT 1";
	    
	    return $wpdb->get_row($sql);
	}
	
	/**
	 * Recupera uma configuração por data
	 *
	 * @since L1
	 *
	 * @param array Lista de arrays associativos das configurações
	 */
	
	public static function listar_configs()
	{
	    global $wpdb;
	    
	    $sql = "SELECT * FROM premium_configs
                ORDER BY ppc_data DESC";
	    
	    return $wpdb->get_results($sql);
	}

    /**
     * Recupera o acumulado de um professor em um período
     *
     * @since L1
     *
     * @param int $mes_ano Período a ser consultado
     * @param int $professor_id Id do professor
     *
     * @return array Array associativo das configurações
     */
    
    public static function get_acumulado($mes_ano, $professor_id)
    {
        global $wpdb;
        
        $sql = "SELECT * FROM premium_pgtos_acumulados
                WHERE ppa_periodo = '{$mes_ano}' AND user_id = {$professor_id}";
        
        return $wpdb->get_row($sql);
    }
    
    /**
     * Lista os professores com acumulados no mes
     *
     * @since L1
     *
     * @param int $mes_ano Período a ser consultado
     *
     * @return array Array associativo de professores
     */
    
    public static function listar_professores_com_acumulado_no_mes($mes_ano)
    {
        global $wpdb;
        
        $sql = "SELECT * FROM premium_pgtos_acumulados ppa
                    JOIN wp_users u ON u.ID = ppa.user_id
                WHERE ppa_periodo = '{$mes_ano}' AND ppa_valor_acumulado > 0";
        
        return $wpdb->get_results($sql);
    }

    /**
     * Salva uma informação de valor acumulado
     *
     * @since L1
     *
     * @param int $mes_ano Período
     * @param int $professor_id Id do professor
     * @param float $valor Valor acumulado
     */
    
    public static function salvar_acumulado($mes_ano, $professor_id, $valor)
    {
        global $wpdb;

        $dados = [
            'ppa_periodo' => $mes_ano,
            'user_id' => $professor_id,
            'ppa_valor_acumulado' => $valor
        ];

        $wpdb->insert("premium_pgtos_acumulados", $dados);
    }
    
    /**
     * Exclui uma informação de valor acumulado
     *
     * @since L1
     *
     * @param int $mes_ano Período
     * @param int $professor_id Id do professor
     */
    
    public static function excluir_acumulado($mes_ano, $professor_id)
    {
        global $wpdb;
        
        $dados = [
            'ppa_periodo' => $mes_ano,
            'user_id' => $professor_id,
        ];
        
        $wpdb->delete("premium_pgtos_acumulados", $dados);
    }
    
    /**
     * Lista as informações de professores de um determinado período
     *
     * @since L1
     * 
     * @param int $mes_ano Período a ser consultado
     *
     * @return array Informações de professores
     */
    
    public static function listar_info_professores_por_periodo($mes_ano)
    {
        global $wpdb;
        
        $sql = "SELECT * FROM premium_info_professores WHERE pip_ano_mes = '{$mes_ano}' ";
        
        $resultado = $wpdb->get_results($sql);
        
        if($resultado) {
            $info = [];
            
            foreach ($resultado as $linha) {
                $info[$linha->pro_id][$linha->usu_id]['soma'] = $linha->pip_soma;
                $info[$linha->pro_id][$linha->usu_id]['percentual'] = $linha->pip_percentual;
            }
            
            return $info;
        }
    }
    
    /**
     * Lista o último agendamento de um produto premium
     *
     * @since L1
     *
     * @param int $mes_ano Período a ser consultado
     * @param int $produto_id Id do produto
     *
     * @param array Array associativo do último agendamento
     */
    
    public static function get_ultimo_agendamento($mes_ano, $produto_id)
    {
        global $wpdb;
        
        $sql = "SELECT * FROM premium_agendamentos WHERE pra_ano_mes = '{$mes_ano}' AND pro_id = {$produto_id} AND pra_status = 2 ORDER BY pra_data DESC LIMIT 1";
        
        return $wpdb->get_row($sql);
    }
}