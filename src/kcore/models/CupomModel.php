<?php
/**
 * Model para organizar rotinas relacionadas aos Cupons
 * 
 * @package	KCore/Models
 *
 * @author 	Fábio Siqueira
 *
 * @since	K5
 */ 

	class CupomModel {


		public static function get_by_slug($slug)
  		{
  			global $wpdb;

			$sql = "SELECT * FROM wp_posts 
			WHERE post_name = '{$slug}' AND post_type = 'shop_coupon'";

			return $wpdb->get_row($sql);

 		}

 	}