<?php
/**
 * Model para organizar rotinas relacionadas aos avisos
 *
 * @package	KCore/Models
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K5
 */

class AvisoModel  {
    
    /**
     * Lista todos os avisos
     *
     * @since K5
     *
     * @return array Array associativo da tabela saidas.
     */
    
    public static function listar_todos()
    {
        global $wpdb;
        
        return $wpdb->get_results("SELECT * FROM avisos ORDER BY data_inicio DESC", ARRAY_A);
    }
    
    /**
     * Recupera um aviso valido
     *
     * @since K5
     *
     * @return array Array associativo da tabela saidas.
     */
    
    public static function get_aviso_valido($excluir_ids = null)
    {
        global $wpdb;
        
        $url_atual = strtolower(get_url_atual());
        
        $exc = "";
        if($excluir_ids) {
            $exc = "AND id NOT IN (" . implode(",", $excluir_ids) . ")";
        }
        
        $agora = get_data_hora_agora();
        $sql = "SELECT * FROM avisos WHERE data_inicio <= '{$agora}' && data_fim >= '{$agora}' {$exc} ORDER BY RAND()";
        $resultado = $wpdb->get_results($sql, ARRAY_A);
        
        foreach ($resultado as $item) {
            $ok_url = false;
            if($urls = $item['urls']) {
                $urls_a = explode(',', $urls);
                
                foreach ($urls_a as $url) {
                    $url_atual_s = remover_ultimo_char_se($url_atual, "/");
                    $url_s = remover_ultimo_char_se(strtolower(trim($url)),"/");
                    
                    if($url_atual_s == $url_s) {
                        $ok_url = true;
                    }
                }
            }
            else {
                
                $ok_url = true;
            }
            
            $ok_palavra = false;
            if($palavras = $item['palavras']) {
                $palavras_a = explode(',', $palavras);
                
                foreach ($palavras_a as $palavra) {
                    if(strpos($url_atual, strtolower(trim($palavra))) !== false) {
                        $ok_palavra = true;
                    }
                }
            }
            else {
                $ok_palavra = true;
            }
            
            $ok_url_neg = true;
            if($urls = $item['urls_negativas']) {
                $url_atual_s = remover_ultimo_char_se(strtolower($url_atual), "/");
                
                $urls_a = explode(',', $urls);
                
                foreach ($urls_a as $url) {
                    $url_s = remover_ultimo_char_se(strtolower(trim($url)),"/");
                    if($url_atual_s == $url_s) {
                        $ok_url_neg = false;
                    }
                }
            }
            
            $ok_tag_neg = true;
            if($negs = $item['palavras_negativas']) {
                
                $negs_a = explode(',', $negs);
                
                foreach ($negs_a as $neg) {
                    if(strpos($url_atual, trim($neg)) !== false) {
                        $ok_tag_neg = false;
                    }
                }
            }
            
            if($ok_url && $ok_palavra && $ok_url_neg && $ok_tag_neg) {
                return $item;
            }
            
        }
        
        return null;
    }
    
    
    /**
     * Cadastra um novo aviso no site
     *
     * @see https://podio.com/profinfnetedubr/lslc/apps/upgrades/items/318
     *
     * @since Joule 2
     *
     * @param array $dados Dados do aviso
     */
    
    public static function salvar($dados)
    {
        global $wpdb;
        
        if($dados['id']) {
            $wpdb->update('avisos', $dados, ['id' => $dados['id']]);
        }
        else {
            $wpdb->insert('avisos', $dados);
        }
    }
    
    /**
     * Exclui um aviso existente
     *
     * @see https://podio.com/profinfnetedubr/lslc/apps/upgrades/items/318
     *
     * @since Joule 2
     *
     * @param int $id
     */
    
    public static function excluir($id)
    {
        global $wpdb;
        
        $wpdb->delete('avisos', ['id' => $id]);
    }
    
    /**
     * Recupera um aviso através do id
     *
     * @see https://podio.com/profinfnetedubr/lslc/apps/upgrades/items/318
     *
     * @since Joule 2
     *
     * @param int $id
     *
     * @return array Array associativo da tabela avisos
     */
    
    public static function get_by_id($id)
    {
        global $wpdb;
        
        return $wpdb->get_row("SELECT * FROM avisos WHERE id = {$id}");
    }
    
}