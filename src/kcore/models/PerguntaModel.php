<?php 

class PerguntaModel {

    public function listar()
    {
        global $wpdb;

        return $wpdb->get_results("SELECT * FROM wp_posts WHERE post_type = 'question'");
    }

    public function get_term_taxonomy($id)
    {
        global $wpdb;

        return $wpdb->get_row("SELECT * FROM wp_term_relationships where object_id = $id");
    }
}