<?php
/**
 * Model para organizar rotinas relacionadas às capturas de saída
 *
 * @package	KCore/Models
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K5
 */

class CapturaSaidaModel  {
    
    /**
     * Lista todas as capturas de saída
     *
     * @since K5
     *
     * @return array Array associativo da tabela saidas.
     */
    
    public static function listar_todas()
    {
        global $wpdb;
        
        $sql = "SELECT * FROM saidas ORDER BY sai_prioridade DESC";
        
        return $wpdb->get_results($sql);
    }
    
    /**
     * Salva os dados de uma captura de saída
     *
     * @since K5
     *
     * @param array $dados Dados da captura de saída.
     */
    
    public static function salvar($dados)
    {
        global $wpdb;
        
        if($dados['sai_id']) {
            $wpdb->update('saidas', $dados, ['sai_id' => $dados['sai_id']]);   
        }
        else {
            $wpdb->insert('saidas', $dados);
        }

    }
    
    /**
     * Exclui uma captura de saída a partir do seu ID
     *
     * @since K5
     *
     * @param int $saida_id Id da saida.
     */
    
    public static function excluir($saida_id)
    {
        global $wpdb;
        
        $sql = "DELETE FROM saidas WHERE sai_id = {$saida_id}";
        
        $wpdb->query($sql);
    }
    
    /**
     * Recupera uma captura de saída a partir do seu ID
     *
     * @since K5
     *
     * @param int $saida_id Id da saida.
     */
    
    public static function get_by_id($saida_id)
    {
        global $wpdb;
        
        $sql = "SELECT * FROM saidas WHERE sai_id = {$saida_id}";
        
        return $wpdb->get_row($sql);
    }   
    
    /**
     * Recupera uma saída ativa de maior prioridade
     *
     * @since K5
     *
     * @param array Array associativo da captura de saída.
     */
    
    public static function listar_ativa($excluir_ids = null)
    {
        global $wpdb;
        
        $url_atual = get_url_atual();

        $exc = "";
        if($excluir_ids) {
            $exc = "AND sai_id NOT IN (" . implode(",", $excluir_ids) . ")";
        }
        
        $agora = get_data_hora_agora();
        
        $sql = "SELECT * FROM saidas WHERE sai_data_inicio <= '{$agora}' AND sai_data_fim >= '{$agora}' {$exc} ORDER BY sai_prioridade DESC";
        
        $resultado = $wpdb->get_results($sql, ARRAY_A);
        
        foreach ($resultado as $item) {
            
            $ok_url = false;
            if($urls = $item['sai_urls']) {
                $url_atual_s = remover_ultimo_char_se(strtolower($url_atual), "/");
                
                $urls_a = explode(',', $urls);
                
                foreach ($urls_a as $url) {
                    $url_s = remover_ultimo_char_se(strtolower(trim($url)),"/");
                    if($url_atual_s == $url_s) {
                        $ok_url = true;
                    }
                }
            }
            else {
                $ok_url = true;
            }
            
            $ok_tag = false;
            if($tags = $item['sai_tags']) {
                
                $tags_a = explode(',', $tags);
                
                foreach ($tags_a as $tag) {
                    if(strpos($url_atual, trim($tag)) !== false) {
                        $ok_tag = true;
                    }
                }
            }
            else {
                $ok_tag = true;
            }
            
            $ok_url_neg = true;
            if($urls = $item['sai_urls_negativas']) {
                $url_atual_s = remover_ultimo_char_se(strtolower($url_atual), "/");
                
                $urls_a = explode(',', $urls);
                
                foreach ($urls_a as $url) {
                    $url_s = remover_ultimo_char_se(strtolower(trim($url)),"/");
                    if($url_atual_s == $url_s) {
                        $ok_url_neg = false;
                    }
                }
            }
            
            $ok_tag_neg = true;
            if($negs = $item['sai_palavras_negativas']) {
                
                $negs_a = explode(',', $negs);
                
                foreach ($negs_a as $neg) {
                    if(strpos($url_atual, trim($neg)) !== false) {
                        $ok_tag_neg = false;
                    }
                }
            }
            
            if($ok_url && $ok_tag && $ok_url_neg && $ok_tag_neg) {
                return $item;
            }
            
        }
        
        return null;
    }
    
}