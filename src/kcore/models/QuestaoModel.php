<?php
/**
 * Model para organizar rotinas relacionadas às Questões
 *
 * @package	KCore/Models
 *
 * @author 	João Paulo Ferreira <jpaulofms@gmail.com>
 *
 * @since	L1
 */

class QuestaoModel {
    
    /**
     * Conta questões que tenha determinado conteúdo nos texto-base, enunciado ou nas opções das questões
     * 
     * @since L1
     * 
     * @param $conteudo string
     * @param $exclude string a ser ignorada
     */
    
    static function contar_questoes_com_conteudo($conteudo, $exclude = null)
    {
        $db = new wpdb(DB_USER_CORP, DB_PASSWORD_CORP, DB_NAME_CORP, DB_HOST_CORP);
        
        if($exclude)
        {
            $where_texto_base = " AND replace(q.que_texto_base, '{$exclude}', '') LIKE '%{$conteudo}%'";
            $where_enunciado = " AND replace(q.que_enunciado, '{$exclude}', '') LIKE '%{$conteudo}%'";
            $where_opcoes = " AND replace(qo.qop_texto, '{$exclude}', '') LIKE '%{$conteudo}%'";
        }
        
        $sql = "SELECT COUNT( DISTINCT q.que_id) 
                FROM questoes q 
                    INNER JOIN questoes_opcoes qo ON qo.que_id = q.que_id 
                WHERE (q.que_texto_base LIKE '%{$conteudo}%' {$where_texto_base}) 
                    OR (q.que_enunciado LIKE '%{$conteudo}%' {$where_enunciado})
                    OR (qo.qop_texto LIKE '%{$conteudo}%' {$where_opcoes})";
        
    	return $db->get_var($sql);
    }
    
    /**
     * Lista questões que tenha determinado conteúdo nos texto-base, enunciado ou nas opções das questões
     *
     * @since L1
     *
     * @param $conteudo string
     * @param $exclude string a ser ignorada
     */
    
    static function listar_questoes_com_conteudo($conteudo, $exclude = null)
    {
        $db = new wpdb(DB_USER_CORP, DB_PASSWORD_CORP, DB_NAME_CORP, DB_HOST_CORP);
        
        if($exclude)
        {
            $where_texto_base = " AND replace(q.que_texto_base, '{$exclude}', '') LIKE '%{$conteudo}%'";
            $where_enunciado = " AND replace(q.que_enunciado, '{$exclude}', '') LIKE '%{$conteudo}%'";
            $where_opcoes = " AND replace(qo.qop_texto, '{$exclude}', '') LIKE '%{$conteudo}%'";
        }
        
        $sql = "SELECT DISTINCT q.que_id, q.que_data_atualizacao
                FROM questoes q
                    INNER JOIN questoes_opcoes qo ON qo.que_id = q.que_id
                WHERE (q.que_texto_base LIKE '%{$conteudo}%' {$where_texto_base})
                    OR (q.que_enunciado LIKE '%{$conteudo}%' {$where_enunciado})
                    OR (qo.qop_texto LIKE '%{$conteudo}%' {$where_opcoes})";
        
        return $db->get_results($sql);
    }
    
    /**
     * Substitui conteúdo antigo pelo novo em todas as questões: texto-base, enunciado e opções das questões
     *
     * @since L1
     *
     * @param $antigo string
     * @param $novo string
     */
    
    function replace_conteudo_de_questoes($antigo, $novo)
    {
        $db = new wpdb(DB_USER_CORP, DB_PASSWORD_CORP, DB_NAME_CORP, DB_HOST_CORP);
        
        $sql = 
        "UPDATE questoes 
            SET que_texto_base = REPLACE(que_texto_base, '{$antigo}', '{$novo}'), 
                que_enunciado = REPLACE(que_enunciado, '{$antigo}', '{$novo}') 
        WHERE que_texto_base LIKE '%{$antigo}%' 
                OR que_enunciado LIKE '%{$antigo}%'";
        
        $db->query($sql);
        
        $sql =
        "UPDATE questoes_opcoes
            SET qop_texto = REPLACE(qop_texto, '{$antigo}', '{$novo}')
        WHERE qop_texto LIKE '%{$antigo}%'";
        
        $db->query($sql);
    }
    
}