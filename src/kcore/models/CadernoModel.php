<?php
/**
 * Model para organizar rotinas relacionadas aos cadernos fora do SQ
 *
 * @package	KCore/Models
 *
 * @author 	João Paulo Ferreira <jpaulofms@gmail.com>
 *
 * @since	L1
 */

class CadernoModel  {
    
    /**
     * Retorna a quantidade de cadernos comercializáveis de um professor
     * 
     * @since L1
     * 
     * @param $professor_id ID do professor
     * 
     * @return int Total de cadernos comercializáveis do professor
     */
    static public function contar_cadernos_professor($professor_id){
        global $wpdb;

        $sql = "SELECT COUNT(*) FROM exponenc_corp.cadernos cad WHERE cad.cad_comercializavel = 1 AND cad.usu_id = {$professor_id}";

        return $wpdb->get_var($sql);
    }

    /**
     * Retorna os dados básicos de um caderno
     * 
     * @since L1
     * 
     * @param $caderno_id ID do caderno ou de seu caderno referência
     * 
     * @return Object contendo os dados solicitados
     */
    static public function get_caderno($caderno_id, $referencia = FALSE, $produto_id = NULL, $order_id = NULL){
        global $wpdb;

        $cad_col = "cad.cad_id";
        if($referencia){
            $cad_col = "cad.usu_id = " . get_current_user_id() . " AND cad.cad_ref_id";
        }

        $sql = "SELECT cad.cad_id, cad.cad_nome, cad.usu_id 
                FROM exponenc_corp.cadernos cad 
                    LEFT JOIN exponenc_corp.cadernos_pedidos cpe
                        ON cpe.cad_id = cad.cad_id
                WHERE {$cad_col} = {$caderno_id} 
                ";
        if($produto_id){
            $sql .= " AND cpe.cpe_prd_id = {$produto_id} ";
        }

        if($order_id){
            $sql .= " AND cpe.cpe_ped_id = {$order_id} ";
        }

        return $wpdb->get_row( $sql );
    }

    /**
     * Lista os cadernos de um determinado professor
     * 
     * @since L1
     * 
     * @param $professor_id ID do professor
     * @param $offset Offset da busca
     * @param $limite Limite de resultados da busca
     * 
     * @return Array<Object> Com os dados básicos dos cadernos encontrados
     */
    static public function listar_cadernos_professor($professor_id, $offset = null, $limite = null){
        global $wpdb;

        $sql = "SELECT cad.cad_id, cad.cad_nome 
                FROM exponenc_corp.cadernos cad 
                WHERE cad.cad_comercializavel = 1 
                    AND cad.usu_id = {$professor_id} 
                ORDER BY cad.cad_nome 
                ";
        if($limite > 0){
            $sql .= " LIMIT {$limite} ";
        }

        if($offset){
            $sql .= " OFFSET {$offset} ";
        }

        return $wpdb->get_results( $sql );
    }

}