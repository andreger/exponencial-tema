<?php
/**
 * Model para organizar rotinas relacionadas aos Simulados
 *
 * @package	KCore/Models
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	L1
 */

class SimuladoModel {
    
    /**
     * Concede acesso para um usuário a um simulado
     *
     * @since L1
     *
     * @param int $usuario_id Id do usuário
     * @param int $produto_id Id do produto
     */
    
    public static function concede_acesso_a_simulado($usuario_id, $produto_id)
    {
        $db = new wpdb(DB_USER_CORP, DB_PASSWORD_CORP, DB_NAME_CORP, DB_HOST_CORP);
        $linha = $db->get_row("select sim_id from simulados where prd_id = $produto_id", ARRAY_A);
        
        if($linha) {
            $db->insert('simulados_usuarios', array('usu_id' => $usuario_id, 'sim_id' => $linha['sim_id']));
        }
    }
    
    /**
     * Recupera um simulado através de um produto id
     *
     * @since L1
     *
     * @param int $produto_id Id do produto
     * 
     * @return object Linha associativa da tabela simulados
     */
    
    public static function get_simulado_by_produto($produto_id)
    {
        $db = new wpdb(DB_USER_CORP, DB_PASSWORD_CORP, DB_NAME_CORP, DB_HOST_CORP);
        return $db->get_row("select * from simulados where prd_id = $produto_id");
    }
    
    /**
     * Lista todos os produtos ids associados a simulados
     *
     * @since L1
     *
     * @return array Lista de objetos com produto_id (prd_id)
     */
    
    public static function listar_todos_produtos_simulados()
    {
        $db = new wpdb(DB_USER_CORP, DB_PASSWORD_CORP, DB_NAME_CORP, DB_HOST_CORP);
        
        return $db->get_results("select distinct prd_id from simulados");
    }
    
    /**
     * Torna um simulado incosistente
     * 
     * OBS.: Ao alterar esse método @see questoes\models\Simulado_model::alterar_status
     * 
     * @since L1
     * 
     */
    public static function tornar_simulado_inconsistente( $simulado_id )
    {
        $db = new wpdb(DB_USER_CORP, DB_PASSWORD_CORP, DB_NAME_CORP, DB_HOST_CORP);

        /** 	
		 * [JOULE] UPGRADE - Controle de prioridade dos simulados
		 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/upgrades/items/340
		 *
		 * [JOULE] Simulado recém inconsistente deve ficar na primeira posição de prioridade
		 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2084
		 */

         //Altera a prioridade de todos os simulados que possuem prioridade
        $sql = "UPDATE simulados
                    SET sim_prioridade = sim_prioridade + 1
                WHERE sim_prioridade IS NOT NULL
                    AND sim_prioridade > 0";

        $db->query($sql);

        //Torna o simulado inconsistente e atualiza sua prioridade
        $hoje = hoje_yyyymmdd();

        $sql = "UPDATE simulados 
                    SET sim_status = 3
                        , sim_data_inconsistencia = '{$hoje}'
                        , sim_prioridade = 1 
                WHERE sim_id = {$simulado_id}";

        $db->query($sql);        

    }
}