<?php 
/**
 * Model para organizar rotinas relacionadas aos Usuários
 * 
 * @package	KCore/Models
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K4
 */ 

class UsuarioModel {

	/**
	 * Atualiza os metadados de um usuário
	 * 
	 * @since K4
	 * 
	 * @param int $id Id do usuário
	 */ 

	public static function atualizar_metadados($id)
	{
        KLoader::model("ColaboradorModel");

		global $wpdb;

		// metadados de colaboradores
        $tipo = get_user_meta($id, "oneTarek_author_type", TRUE);
        
        if($tipo) {
	        $qtde_cursos = contar_produtos_por_autor($id);
	        $slug = get_user_meta($id, "slug", TRUE);
	        $mini_cv = get_user_meta($id, 'mini_cv', TRUE);
	        $cargo = get_user_meta($id, 'cargo', TRUE);
	        $oculto = get_user_meta($id, 'ocultar', TRUE) ?: 0;
	        $descricao = get_user_meta($id,'description', TRUE);
			
	        $linkedin = get_user_meta($id, LINKEDIN, TRUE);
	        $twitter = get_user_meta($id, TWITTER, TRUE);
	        $facebook = get_user_meta($id, FACEBOOK, TRUE);
	        $googleplus = get_user_meta($id, GOOGLEPLUS, TRUE);
	        $instagram = get_user_meta($id, INSTAGRAM, TRUE);
	        $youtube = get_user_meta($id, YOUTUBE, TRUE);

			$img = str_get_html(get_avatar($id, 200));
			$foto_url_200 = $img->find("img", 0)->src;

			$img = str_get_html(get_avatar($id, 365));
			$foto_url_365 = $img->find("img", 0)->src;

			switch ($id) {
				case 1: $prioridade = 2; break;
				case 192: $prioridade = 1; break;
				default: $prioridade = 0; break;
			}

			$dados = [
				'user_id' => $id,
				'col_tipo' => $tipo,
				'col_qtde_cursos' => $qtde_cursos,
				'col_slug' => $slug,
				'col_oculto' => $oculto,
				'col_cargo' => $cargo,
				'col_mini_cv' => $mini_cv,
				'col_foto_url_200' => $foto_url_200,
				'col_foto_url_365' => $foto_url_365,
				'col_prioridade' => $prioridade,
				'col_youtube' => $youtube,
				'col_facebook' => $facebook,
				'col_linkedin' => $linkedin,
				'col_twitter' => $twitter,
				'col_googleplus' => $googleplus,
				'col_instagram' => $instagram,
				'col_descricao' => $descricao
				
			];

			$wpdb->replace('colaboradores', $dados);

//			$produtosIds = ColaboradorModel::listarWpProdutosComAutor($id);
//
//			if($produtosIds) {
//                KLoader::model("ProdutoModel");
//
//			    foreach ($produtosIds as $produtoId) {
//                    ProdutoModel::atualizar_metadados_professores($produtoId);
//                }
//            }

        }
//        else {
//            ColaboradorModel::excluir($id);
//        }
        
	}

	/**
	 * Recupera um usuário através do seu slug
	 * 
	 * @since K4
	 * 
	 * @param string $slug Slug do usuário.
	 *
	 * @return array
	 */ 

	public static function get_by_slug($slug)
	{	
		global $wpdb;

		$id = $wpdb->get_var("SELECT user_id FROM wp_usermeta WHERE meta_key = 'slug' AND meta_value = '{$slug}'");

		if($id) {
			return get_usuario_array($id);
		}

		return NULL;
	}

    public static function get_by_id($id)
    {
        global $wpdb;

        return get_usuario_array($id);
    }

    public function listarProximos($ultimoId = 0)
    {
        global $wpdb;

        $sql = "SELECT ID FROM wp_users WHERE 1=1 AND ID > {$ultimoId} LIMIT 100";

        $resultado = $wpdb->get_results($sql);

        $usuarios = [];
        if($resultado) {
            foreach ($resultado as $item) {
                $usuarios[] = get_usuario_array($item->ID);
            }
        }
        return $usuarios;
	}
}