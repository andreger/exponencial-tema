<?php
/**
 * Model para organizar rotinas específicas dos produtos premiums
 *
 * @package	KCore/Models
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	L1
 */

class RecorrenteModel {
    
    /**
     * Recupera a duração de um produto recorrente
     *
     * @since L1
     *
     * @param int $produto_id Id do produto
     *
     * @return float
     */
    
    public static function get_duracao($produto_id)
    {
        global $wpdb;
        
        $sql = "SELECT meta_value FROM wp_postmeta WHERE post_id = {$produto_id} AND meta_key = '_subscription_length' ";
        
        return $wpdb->get_var($sql);
    }
    
    /**
     * Recupera a periodicidade de um produto recorrente
     *
     * @since L1
     *
     * @param int $produto_id Id do produto
     *
     * @return float
     */
    
    public static function get_periodicidade($produto_id)
    {
        global $wpdb;
        
        $sql = "SELECT meta_value FROM wp_postmeta WHERE post_id = {$produto_id} AND meta_key = '_subscription_period' ";
        
        return $wpdb->get_var($sql);
    }
    
    /**
     * Recupera o preço de um produto recorrente
     *
     * @since L1
     *
     * @param int $produto_id Id do produto
     *
     * @return float
     */
    
    public static function get_preco($produto_id)
    {
        global $wpdb;
        
        $sql = "SELECT meta_value FROM wp_postmeta WHERE post_id = {$produto_id} AND meta_key = '_subscription_price' ";
        
        return $wpdb->get_var($sql);
    }
    
    
    
}