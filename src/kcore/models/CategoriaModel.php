<?php 
/**
 * Model para organizar rotinas relacionadas às Categorias
 * 
 * @package	KCore/Models
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K4
 */ 

class CategoriaModel {

	/**
	 * Atualiza os metadados de uma categoria
	 * 
	 * @since K4
	 * 
	 * @param int $id Id da categoria
	 */ 

	public static function atualizar_metadados($id)
	{
        KLoader::api("MateriaApi");

		global $wpdb;

		$categoria = self::get_by_id($id);
        $qtdeCursos = 0;

		if($categoria) {
			$qtdeCursos = self::atualizar_quantidade_cursos($id);
		}

		if(self::is_categoria_filha($id, CATEGORIA_MATERIA)) {
			$wpdb->update('materias', ['mat_slug' => $categoria->slug,], ['mat_id' => $id]);

	        $categoria->qtde_cursos = $qtdeCursos;
			MateriaApi::salvar($categoria);
		}
	}

	/**
	 * Atualiza a quantidade de cursos de uma categoria
	 * 
	 * @since K5
	 * 
	 * @param int $id Id da categoria
	 */
	public static function atualizar_quantidade_cursos($id)
    {
		global $wpdb;

		KLoader::model("ProdutoModel");

		$categoria = self::get_by_id($id);
        $qtde = 0;

		if(self::is_categoria_filha($id, CATEGORIA_CONCURSO)) {
		 	$qtde = ProdutoModel::contar_por_concurso($categoria->slug);
		 	log_wp('INFO', 'Concurso: ' . $categoria->slug . '('.$qtde.')');
		 	$wpdb->update('concursos', ['con_qtde_cursos' => $qtde], ['con_id' => $id]);
		}
		if(self::is_categoria_filha($id, CATEGORIA_MATERIA)) {
			$qtde = ProdutoModel::contar_por_materia($categoria->slug);
			// log_wp('INFO', 'Materia: ' . $categoria->slug . '('.$qtde.')');
			$wpdb->update('materias', ['mat_qtde_cursos' => $qtde], ['mat_id' => $id]);
		}

		return $qtde;
	}

	/**
	 * Atualiza o slug de uma categoria na tabela wp_terms
	 * 
	 * @since K4
	 * 
	 * @param int $id Id da categoria
	 */ 

	public static function atualizar_wp_term($id, $nome, $slug)
	{
		global $wpdb;
		
		$wpdb->update('wp_terms', ['name' => $nome, 'slug' => $slug], ['term_id' => $id]);
	}

	/**
	 * Cria os metadados de uma categoria
	 * 
	 * @since K4
	 * 
	 * @param int $id Id da categoria
	 */ 

	public static function criar_metadados($id)
	{
        KLoader::api("MateriaApi");

		global $wpdb;

		// if(self::is_categoria_filha($id, CATEGORIA_CONCURSO)) {
		// 	$categoria = self::get_by_id($id);
		// 	$wpdb->insert('concursos', ['con_id' => $id, 'con_slug' => $categoria->slug]);
		// }

		if(self::is_categoria_filha($id, CATEGORIA_MATERIA)) {
			$categoria = self::get_by_id($id);
			$wpdb->insert('materias', ['mat_id' => $id, 'mat_slug' => $categoria->slug]);

			$qtdeCursos = 0;
            if($categoria) {
                $qtdeCursos = self::atualizar_quantidade_cursos($id);
            }

            $categoria->qtde_cursos = $qtdeCursos;
            MateriaApi::salvar($categoria);
		}
	}

	/**
	 * Excluir os metadados de uma categoria
	 * 
	 * @since K4
	 * 
	 * @param int $id Id da categoria
	 */ 

	public static function excluir_metadados($deletedTerm)
	{
        KLoader::api("MateriaApi");

		global $wpdb;

		// if(self::is_categoria_filha($id, CATEGORIA_CONCURSO)) {
		// 	$categoria = self::get_by_id($id);
		// 	$wpdb->delete('concursos', ['con_id' => $id]);
		// }

		if($deletedTerm->parent == CATEGORIA_MATERIA) {
            $id = $deletedTerm->term_id;
			$wpdb->delete('materias', ['mat_id' => $id]);

            MateriaApi::excluir($id);
		}
	}

	/**
	 * Recupera uma categoria através do seu id
	 * 
	 * @since K4
	 * 
	 * @param int $id Id da categoria. Referente à coluna term_id da tabela wp_terms.
	 *
	 * @return object WP_Term
	 */ 

	public static function get_by_id($id)
	{
		global $wpdb;

		$sql = "SELECT * FROM wp_terms where term_id = '{$id}'";

		return $wpdb->get_row($sql);
	}

	/**
	 * Recupera uma categoria através do seu slug
	 * 
	 * @since K4
	 * 
	 * @param string $slug Slug da categoria. Referente à coluna slug da tabela wp_terms.
	 *
	 * @return object WP_Term
	 */ 

	public static function get_by_slug($slug)
	{
		global $wpdb;

		$sql = "SELECT * FROM wp_terms where slug = '{$slug}'";

		return $wpdb->get_row($sql);
	}
	
	/**
	 * Recupera a URL da imagem da categoria
	 *
	 * @since K4
	 *
	 * @param int $categoria_id Id da categoria
	 *
	 * @return string
	 */
	
	public static function get_imagem_url($categoria_id)
	{
	    $cat_thumb_id = get_term_meta($categoria_id, 'thumbnail_id', true);
	    
	    return wp_get_attachment_url($cat_thumb_id);
	}

	/**
	 * Verifica se uma categoria é filha de outra
	 * 
	 * @since K4
	 * 
	 * @param int $categoria_id Id da categoria.
	 * @param int $categoria_mae_id Id da categoria mãe.
	 *
	 * @return bool
	 */ 
	
	public static function is_categoria_filha($categoria_id, $categoria_mae_id)
	{
		global $wpdb;

		$sql = "SELECT t.term_id FROM wp_terms t JOIN wp_term_taxonomy tt ON t.term_id = tt.term_id WHERE t.term_id = {$categoria_id} AND  tt.parent = {$categoria_mae_id} ORDER BY t.name";

		return $wpdb->get_row($sql) ? TRUE : FALSE;
	}


	/**
	 * Lista todas as categorias filhas de uma categoria
	 * 
	 * @since K4
	 * 
	 * @param int $id Id da categoria mãe. Referente à coluna term_id da tabela wp_terms
	 *
	 * @return array Lista de categorias. Registros da tabela wp_terms.
	 */ 
	
	public static function listar_filhas($id)
	{
		global $wpdb;

		$sql = "SELECT t.* FROM wp_terms t JOIN wp_term_taxonomy tt ON t.term_id = tt.term_id WHERE tt.parent = {$id} ORDER BY t.name";

		return $wpdb->get_results($sql);
	}
	
	
	/**
	 * Lista os wp_terms
	 *
	 * @since L1
	 *
	 * @param array $args Parâmetros da busca
	 *
	 * @return array Lista de categorias. Registros da tabela wp_terms.
	 */
	
	public static function listar($args)
	{
	    global $wpdb;
	    
	    $select = isset($args['select']) ? $args['select'] : "*";
	    
	    $where_nome = isset($args["nome"]) ? "AND name LIKE '%{$args["name"]}%' " : "" ;
	    
	    $sql = "SELECT {$select} FROM wp_terms t 
                    JOIN wp_term_taxonomy tt ON t.term_id = tt.term_id
                WHERE tt.taxonomy = 'product_cat' 
                    {$where_nome}
                ORDER BY name ASC";
	    
	    return $wpdb->get_results($sql);
	}
	

}