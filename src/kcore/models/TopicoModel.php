<?php 

class TopicoModel {

    public function listarProximos($ultimoId)
    {
        global $wpdb;

        $sql = "SELECT * FROM wp_posts WHERE post_type = 'topic' AND ID > {$ultimoId} ORDER BY ID LIMIT 100";

        return $wpdb->get_results($sql);
    }

}