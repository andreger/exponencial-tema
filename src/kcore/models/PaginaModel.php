<?php 
/**
 * Model para organizar rotinas relacionadas às Páginas
 * 
 * @package	KCore/Models
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K4
 */ 

class PaginaModel {

	/**
	 * Recupera uma página através do seu ID
	 * 
	 * @since K4
	 * 
	 * @param int $id Id da página. Referente à coluna ID da tabela wp_posts.
	 *
	 * @return object WP_Post
	 */ 

	public static function get_by_id($id)
	{
		global $wpdb;

		$sql = "SELECT * FROM wp_posts WHERE ID = '{$id}'";

		return $wpdb->get_row($sql);
	}

	/**
	 * Recupera uma página através do seu slug
	 * 
	 * @since K4
	 * 
	 * @param string $slug Slug da página. Referente à coluna post_name da tabela wp_posts.
	 *
	 * @return object WP_Post
	 */ 

	public static function get_by_slug($slug)
	{
		global $wpdb;

		$sql = "SELECT * FROM wp_posts WHERE post_name = '{$slug}'";

		return $wpdb->get_row($sql);
	}
	
	/**
	 * Lista as páginas
	 *
	 * @since L1

	 * @return array Lista de páginas. Registros da tabela wp_posts, post_type = page.
	 */
	
	public static function listar()
	{
	    global $wpdb;
	       
	    $sql = "SELECT * FROM wp_posts WHERE post_type = 'page' and post_name != '' and post_status = 'publish' and post_content != ''
                    and post_name not like 'cronograma%' and post_name not like 'depoimento%'
                    and post_content != '' 
                    and ID not in (2161, 2647, 2648, 3143, 3429, 3452, 3693, 28908, 145882, 145883, 145884, 376657,338826)";
	    
	    return $wpdb->get_results($sql);
	}

}