<?php 
/**
 * Model para organizar rotinas relacionadas às Matérias
 * 
 * @package	KCore/Models
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K4
 */ 

KLoader::model("CategoriaModel");

class MateriaModel extends CategoriaModel {

	/**
	 * Recupera uma matéria através da URL
	 * 
	 * @since K4
	 * 
	 * @param string $url URL de cursos por matéria específica.
	 *
	 * @return object WP_Term
	 */ 

	public static function get_by_cursos_por_materia_url($url)
	{
		if($url_a = explode("/", $url)) {

			if(isset($url_a[4])) {

				return self::get_by_slug($url_a[4]);
			
			}

		}
		
		return NULL;
	}

	/**
	 * Lista todas as matérias que possuem cursos
	 * 
	 * @since K4
	 * 
	 * @return array Lista de matérias
	 */ 

	public static function listar_materias_com_cursos()
	{
		global $wpdb;

		$sql = "SELECT m.mat_id, m.mat_qtde_cursos, m.mat_slug, t.name FROM materias m 
					JOIN wp_terms t ON t.term_id = m.mat_id 
				WHERE mat_qtde_cursos > 0 
				ORDER BY t.name";

		return $wpdb->get_results($sql);
	}

    public static function listar_todas()
    {
        global $wpdb;

        $sql = "SELECT m.mat_id as term_id, m.mat_qtde_cursos as qtde_cursos, m.mat_slug as slug, t.name FROM materias m 
					JOIN wp_terms t ON t.term_id = m.mat_id 
				ORDER BY t.name";

        return $wpdb->get_results($sql);
	}
}