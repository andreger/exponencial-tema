<?php
/**
 * Model para organizar rotinas relacionadas ao SEO HTTPS Externo
 *
 * @package	KCore/Models
 *
 * @author 	João Paulo Ferreira <jpaulofms@gmail.com>
 *
 * @since	L1
 */

class SeoHTTPSExternoModel {
     
    /**
     * Conta as informações de Seo Https Externo
     *
     * @since L1
     *
     * @return int
     */
    
    public static function contar()
    {
        global $wpdb;
        
        $sql = "SELECT COUNT(*) from seo_https_externo";
        
        $total = $wpdb->get_var($sql);
        
        return $total;
    }
    
    /**
     * Lista informações de Seo Https Externo
     *
     * @since L1
     *
     * @param int $offset
     * @param int $limit
     *
     * @return array
     */
    
    public static function listar($offset, $limit = LIMITE_SEO_HTTPS_EXTERNO)
    {
        global $wpdb;
        
        $sql = "SELECT * from seo_https_externo WHERE 1=1 ORDER BY she_data_atualizacao DESC LIMIT {$limit} OFFSET {$offset}";
        
        $resultado = $wpdb->get_results($sql, ARRAY_A);
        
        return $resultado;
    }
    
    /**
     * Atualiza informações de HTTPS Externo de um conteúdo, 
     * apagando todos os dados e posteriormente inserindo dados novos
     *
     * @since L1
     *
     * @param array<array> $dados Dados do HTTPS Externo
     * @param string $tipo tipo do HTTPS Externo
     */
    
    public static function salvar_todos($dados, $tipo)
    {
        global $wpdb;
        
        $wpdb->delete('seo_https_externo', ['she_tipo' => $tipo]);
        
        foreach($dados as $dado)
        {
            $dado['she_tipo'] = $tipo;
            $wpdb->insert('seo_https_externo', $dado);
        }
    }
    
}