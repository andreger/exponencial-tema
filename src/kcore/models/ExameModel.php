<?php 
/**
 * Model para organizar rotinas relacionadas aos Exames (Categorias que representam Concursos)
 * 
 * @package	KCore/Models
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K5
 */ 

KLoader::model("CategoriaModel");

class ExameModel extends CategoriaModel {


	/**
	 * Desvincula todas as relações entre o exame especificado e produtos
	 * 
	 * @since K4
	 * 
	 * @param int $concursos_id Id do concurso
	 */ 

	public static function desvincular_todos_produtos($exame_id)
	{
		global $wpdb;

		$wpdb->delete('produtos_exames', ['exa_id' => $exame_id]);
	}
}
