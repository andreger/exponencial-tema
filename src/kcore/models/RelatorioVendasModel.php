<?php 
/**
 * Model para organizar rotinas relacionadas ao relatório de vendas
 * 
 * @package	KCore/Models
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K5
 */ 

class RelatorioVendasModel {

	/**
	 * Recupera os dados do relatório de vendas através de um filtro passado
	 * 
	 * @since K4
	 * 
	 * @param array $filtros Filtros do relatório
	 *
	 * @return array Linhas do relatório
	 */ 

	public static function get_dados($filtros, $contar = false)
	{
		global $wpdb;
		
		$user_id = $filtros["user_id"];

		if(isset($filtros['professor_id']) && $filtros['professor_id'] > 0) {
	        $query_professor = "AND vp.vep_professor_id = {$filtros['professor_id']}";
	    }
		
		if(!is_administrador($user_id)) {
		    
		    $query_professor = "AND ( vp.vep_professor_id = {$user_id}";
		    
		    if(is_coordenador_area($user_id) && !$filtros['meus_produtos']) {
		        $query_professor .=  " OR v.ven_curso_id IN ( SELECT object_id FROM wp_term_relationships tr JOIN wp_term_taxonomy tt ON tt.term_taxonomy_id = tr.term_taxonomy_id WHERE term_id IN (
        			SELECT area_id FROM coordenadores_areas WHERE user_id = {$user_id}
        		) ) ";
		    }
		    
		    if(is_coordenador_sq($user_id) && !$filtros['meus_produtos']) {
		        // Os numeros se referem aos ids das categorias de:
		        // assinaturas -> 211, 212, 213, 214
		        // cadernos -> 418, 303
		        // simulados -> 124
		        
		        $query_professor .=  " OR v.ven_curso_id IN ( SELECT object_id FROM wp_term_relationships tr JOIN wp_term_taxonomy tt ON tt.term_taxonomy_id = tr.term_taxonomy_id WHERE term_id IN (418, 303, 124, 211, 212, 213, 214
		                              ) ) ";
		    }
		    
		    if(is_coordenador_coaching($user_id) && !$filtros['meus_produtos']) {
		        $query_professor .= " OR v.ven_curso_id IN ( SELECT object_id FROM wp_term_relationships tr JOIN wp_term_taxonomy tt ON tt.term_taxonomy_id = tr.term_taxonomy_id WHERE term_id = 413
		                              ) ";
		    }
		    
		    $query_professor .= " )";
		}
		
		$select_coordenador = "";
		$join_coordenador = "";
		$where_coordenador = "";
		
		if(is_administrador($user_id)) {
		    $join_coordenador = " LEFT JOIN wp_vendas_coordenadores vc ON v.ven_id = vc.ven_id ";
		    $select_coordenador = "vc.*,";
		    $where_coordenador = " AND (vc.vec_coordenador_id IS NULL OR vc.vec_coordenador_id = -1)";
		}
		elseif(is_coordenador($user_id) && !$filtros['meus_produtos']) {
		    $join_coordenador = " LEFT JOIN wp_vendas_coordenadores vc ON v.ven_id = vc.ven_id ";
		    $select_coordenador = "vc.*,";
		    $where_coordenador = " AND (vc.vec_coordenador_id IS NULL OR (vc.vec_coordenador_id = {$user_id} AND (vp.vep_professor_id IS NULL OR vp.vep_professor_id != {$user_id})) OR (vp.vep_professor_id = {$user_id} AND vc.vec_coordenador_id = -1))";
		}
		
		$query_start_date = $filtros['start_date'] ? " AND v.ven_data >= '{$filtros['start_date']} 00:00:00' " : "";
		$query_end_date = $filtros['end_date'] ? " AND v.ven_data <= '{$filtros['end_date']} 23:59:59' " : "";
		$where_valor_pago = $filtros['exibir_gratis'] ? "" : " AND v.ven_valor_venda > 0";
		$where_pedido_id = $filtros['pedido_id'] ? " AND v.ven_order_id = {$filtros['pedido_id']} " : "";
		$where_nome_aluno = $filtros['nome_aluno'] ? " AND v.ven_aluno_nome LIKE '%{$filtros['nome_aluno']}%' " : "";
		$where_email_aluno = $filtros['email_aluno'] ? " AND v.ven_aluno_email LIKE '%{$filtros['email_aluno']}%' " : "";
		$where_apenas_descontos = $filtros['exibir_apenas_descontos'] ? " AND v.ven_desconto > 0 " : "";
		$where_apenas_estornos = $filtros['exibir_apenas_estornos'] ? " AND v.ven_reembolso > 0 " : "";
		$where_valor_pago = $filtros['exibir_gratis'] ? "" : " AND v.ven_valor_venda > 0";
		
		$limit_offset = is_null($filtros['limit']) || $contar ? "" : " LIMIT {$filtros['offset']}, {$filtros['limit']} ";
		
		$select = $contar ? "SELECT COUNT(*) AS total " : "SELECT DISTINCT v.*, vp.*, {$select_coordenador} p.post_title as nome_curso ";
		$order_by = $contar ? "" : " ORDER BY ven_data DESC, ven_order_id DESC, ven_curso_id DESC, ven_item_tipo ASC ";
		
		$query = "{$select} 
			FROM wp_vendas v
			LEFT JOIN wp_vendas_professores vp ON v.ven_id = vp.ven_id
			{$join_coordenador}
			LEFT JOIN wp_posts p ON v.ven_curso_id = p.ID
			WHERE v.ven_order_id NOT IN (SELECT pedido_oculto_id FROM premium_log) 
			{$where_valor_pago} {$where_pedido_id} {$where_nome_aluno} {$where_email_aluno} {$where_apenas_descontos}
            {$where_apenas_estornos} {$where_coordenador} {$query_start_date} {$query_end_date} {$query_professor}
			{$order_by} {$limit_offset}";
			      
        if($contar) {
            return $wpdb->get_var($query);
        }
        else {
    		$result = $wpdb->get_results($query, ARRAY_A);
    		
    		$lines = [];
    		
    		foreach ($result as $row) {
    		    
    		    /**
    		     * critérios para identificar se uma coluna deve ser somada ou não ao
    		     * resultado final
    		     */
    		    
    		    // Página do professor: Sempre somar, pois só são exibidos seus produtos
    		    if($filtros['professor_id'] > 0 || !(is_administrador($user_id) || is_coordenador($user_id))) {
    		        $somar = 1;
    		    }
    		    
    		    // Página de admin e coordenador
    		    else {
    		        // Flag não somar:
    		        if($row['vep_nao_somar']) {
    		            $somar = 0;
    		        } elseif($row['ven_item_tipo'] == ITEM_RAIZ_PACOTE) {
    		            $somar = 0;
    		        }else {
    		            $somar = 1;
    		        }
    		    }
    		    
    		    if(empty($row['vep_participacao'])) $row['vep_participacao'] = 100;
    		    
    		    $line = array(
    		        'order_id'		=> $row['ven_order_id'],
    		        'curso_id'		=> $row['ven_curso_id'],
    		        'date' 			=> converter_para_ddmmyyyy($row['ven_data']),
    		        'user_name' 	=> $row['ven_aluno_nome'],
    		        'user_cpf' 		=> $row['ven_aluno_cpf'],
    		        'user_telefone' => $row['ven_aluno_telefone'],
    		        'user_cidade'	=> $row['ven_aluno_cidade'],
    		        'user_estado'	=> $row['ven_aluno_uf'],
    		        'user_email'	=> $row['ven_aluno_email'],
    		        'professor' 	=> $row['ven_item_tipo'] == ITEM_RAIZ_PACOTE ? '' : $row['vep_professor_nome'],
    		        'name'			=> $row['nome_curso'],
    		        'parcelas'		=> $row['ven_parcelamento'],
    		        'metodo'		=> $row['ven_forma_pgto'],
    		        'total'			=> $row['ven_valor_venda'] * ($row['vep_participacao'] /100),
    		        'discount'		=> $row['ven_desconto'] * ($row['vep_participacao'] /100),
    		        'reembolso'	   	=> $row['ven_reembolso'] * ($row['vep_participacao'] /100),
    		        'parceiro'		=> $row['ven_parceiro'],
    		        'aliquota'		=> $row['ven_aliquota'],
    		        'perc_professor'=> $row['vep_professor_lucro'],
    		        'perc_coordenadores' => $row['vec_participacao'],
    		        'pagseguro'		=> $row['ven_pagseguro']  * ($row['vep_participacao'] /100),
    		        'folha_dirigida'	=> $row['ven_folha_dirigida'],
    		        'professor_id'	=> $row['vep_professor_id'],
    		        'item_tipo'		=> $row['ven_item_tipo'],
    		        'areas'			=> $row['ven_curso_areas'],
                    'transacao_id'  => $row['ven_transacao_id'],
    		        'somar'			=> $somar,
    		    );
    		    
    		    array_push($lines, $line);
    		}
    		
    		return $lines;
        }
	}
	
}