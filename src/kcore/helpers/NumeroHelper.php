<?php
/**
 * Helper para organizar rotinas relacionadas a números
 *
 * @package	KCore/Helpers
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	L1
 */

class NumeroHelper {
    
    /**
     * Transforma número us para número br
     *
     * @since L1
     *
     * @param float $input Número formato us
     * @param int $decimais Casas decimais
     *
     * @return string Número formato br
     */
    
    public static function numero_br($input, $decimais = 2)
    {
        return number_format($input, $decimais, ",", ".");
    }
    
}