<?php
/**
 * Helper para organizar rotinas relacionadas aos relatórios
 *
 * @package	KCore/Helpers
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K5
 */

class RelatorioHelper {
    
    /**
     * Retorna a tabela com os dados do relatório
     *
     * @since K5
     *
     * @param string $table Componente <table> com dados do relatório
     *
     * @return string HTML da tabela do relatório formatado para exportação 
     */ 
    
    public static function converter_table_para_xls($table)
    {
        $table = str_replace("<table", "<table style='border-collapse:collapse; border: 1px solid black;'", $table);
        $table = str_replace("<tr", "<tr style='border: 1px solid black;'", $table);
        $table = str_replace("<td", "<td style='border: 1px solid black;'", $table);
        
        $list = get_html_translation_table(HTML_ENTITIES);
        unset($list['"']);
        unset($list['<']);
        unset($list['>']);
        unset($list['&']);
        
        $search = array_keys($list);
        $values = array_values($list);
        $search = array_map('utf8_encode', $search);
        
        $out =  str_replace($search, $values, $table);
        
        return "<html xmlns:x='urn:schemas-microsoft-com:office:excel'>
        	<head>
        	<meta http-equiv='Content-Type' content='text/html;charset=UTF-8' />
        	<!--[if gte mso 9]>
        	<xml>
        	<x:ExcelWorkbook>
        	<x:ExcelWorksheets>
        	<x:ExcelWorksheet>
        	<x:Name>Sheet 1</x:Name>
        	<x:WorksheetOptions>
        	<x:Print>
        	<x:ValidPrinterInfo/>
        	</x:Print>
        	</x:WorksheetOptions>
        	</x:ExcelWorksheet>
        	</x:ExcelWorksheets>
        	</x:ExcelWorkbook>
        	</xml>
        	<![endif]-->
        	</head>
                    
        	<body>
        	" . $out . "
        	</body></html>  ";
    }
}