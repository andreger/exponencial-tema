<?php
/**
 * Helper para organizar rotinas relacionadas ao Pagar.me
 *
 * @package	KCore/Helpers
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	L1
 */

class PagarMeGatewayHelper {
    
    /**
     * Verifica se o pedido foi feito no Pagar.me
     *
     * @since L1
     *
     * @param int $order_id Id do pedido
     *
     * @return bool
     */
    
    public static function is_gateway_pedido_pagarme($order_id)
    {
        return get_post_meta($order_id, '_payment_method', true) == 'pagarme-banking-ticket' ? true : false;
    }
    
    /**
     * Retorna as taxas do Pagar.me
     *
     * @since L1
     *
     * @param WC_Order $order Pedido no WP
     *
     * @return float
     */
    
    public static function get_taxas($order)
    {
        $api_key = self::get_api_key();

        if($api_key)
        {
        
            $url = "https://api.pagar.me/1/transactions/116354765/payables?api_key={$api_key}";
            
            $ch = curl_init();
            
            $options = array(
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_HTTPHEADER => array('Content-type: application/json'),
                CURLOPT_URL => $url
            );
            
            curl_setopt_array($ch, $options);
            $return = curl_exec($ch);
            curl_close($ch);
            
            $pagarme = json_decode($return);
            
            if($pagarme && $pagarme[0] && $pagarme[0]->fee) {
                $taxa = $pagarme[0]->fee / 100;
                return number_format($taxa, 2);
            }

        }
        
        return 0;
    }
        
    /**
     * Retorna a API Key de acordo com o ambiente
     *
     * @since L1
     *
     * @return string
     */
    
    public static function get_api_key()
    {
//         if(is_producao()) {
            return PAGARME_API_KEY_PRD;
//         }
        
//         return "";
    }
}