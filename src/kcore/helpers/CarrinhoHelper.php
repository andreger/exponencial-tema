<?php
/**
 * Helper para organizar rotinas relacionadas ao carrinho
 * 
 * @package	KCore/Helpers
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	L1
 */ 

class CarrinhoHelper {

	/**
	 * Retorna a estilo css de um determinado tipo de blog
	 * 
	 * @since K4
	 * 
	 * @param int $tipo_id Id do tipo do blog
	 * 
	 * @return string Estilo do tipo do blog
	 */ 

	static public function tem_produto_premium_no_carrinho()
	{
	    global $woocommerce;
	    
	    KLoader::model("ProdutoModel");
	    
	    $carrinho = $woocommerce->cart->cart_contents;
	    
	    foreach ($carrinho as $item) {
	        if(ProdutoModel::is_produto_premium($item['product_id'])) {
	           return true;
	        }
	    }
	    
	    return false;
	}

	static public function tem_produto_assinatura_recorrente_no_carrinho()
	{
		global $woocommerce;
	    
	    $carrinho = $woocommerce->cart->cart_contents;
	    
	    foreach ($carrinho as $item) {
			$product = wc_get_product($item['product_id']);
			if($product->is_type("subscription"))
			{
				return true;
			}
	    }
	    
	    return false;
	}
}