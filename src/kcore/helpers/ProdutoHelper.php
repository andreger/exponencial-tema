<?php
/**
 * Helper para organizar rotinas relacionados aos produtos
 * 
 * @package	KCore/Helpers
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K4
 */ 
KLoader::model("ProdutoModel");

class ProdutoHelper {

	/**
	 * Formata a exibição de nome de professores de um produto
	 * 
	 * @since K4
	 * 
	 * @param array $professores Lista de professores. Necessário, no mínimo, display_name e col_slug
	 * 
	 * @return array Lista ou lista de arrays de saída.
	 */ 

	public static function formatar_professores($professores)
	{
		$str_a = [];
		foreach ($professores as $professor) {
				$url =	UrlHelper::get_professor_url($professor->col_slug);
				$str_a[] = "<a href='{$url}' class='author'>{$professor->display_name}</a>";
		}

		$str = implode(', ', $str_a);
		$str = str_replace_last(', ', ' e ', $str); 

		$t = count($professores) > 1 ? "Professores: " : "Professor: ";
		$str = "<span class='font-weight-bold'>$t</span>" . $str;

		return $str; 
	}

	/**
	 * Recupera o preço formatado pelo WP. Necessário, pois os plugins de descontos atuam nesse campo.
	 * 
	 * @since K4
	 * 
	 * @param int $produto_id Id do produto
	 * 
	 * @return string HTML do preço formatado
	 */ 

	public static function get_botao_acao_text($produto_id)
	{
		$produto = ProdutoModel::get_by_id($produto_id);

		/**
		 * @todo Mudar a captura de preço para o objeto $produto. Verificar a influência dos plugins de desconto e pacotes
		 */ 
		$wp_product = wc_get_product($produto_id);
		$preco = is_pacote($wp_product) ? $wp_product->get_bundle_price() : $wp_product->get_price();

		if($produto->pro_botao_comprar) {
			return $produto->pro_botao_comprar;
		}
		else {
			return $preco > 0 ? "Compre agora" : "Adquirir";
		}
	}

	/**
	 * Recupera o preço formatado pelo WP. Necessário, pois os plugins de descontos atuam nesse campo.
	 * 
	 * @since K4
	 * 
	 * @param int $produto_id Id do produto
	 * 
	 * @return string HTML do preço formatado
	 */ 

	public static function get_preco_produto_wp_html($produto_id)
	{
		if(class_exists("WCCT_API")) {
			return WCCT_API::get_price_html($produto_id);
		}
		else {
			$produto = new WC_Product($produto_id);
			return $produto->get_price_html();
		}
	}

	/**
	 * Verifica se um produto possui algum upload não realizado, ou seja, uma aula já deveria estar disponível para o aluno, mas o arquivo ainda não foi inserido pelo professor
	 * 
	 * @since K5
	 * 
	 * @param int $produto_id Id do produto
	 * @param int $tipo Tipo da aula
	 * 
	 * @return bool
	 */ 

	public static function tem_upload_nao_realizado($produto_id, $tipo)
	{
		$aulas_data = get_post_meta($produto_id, 'aulas_data', true);
		$campo = self::get_nome_campo_aulas_conteudo_wp($tipo);
		
		if($aulas_data) {

			$aulas_arquivo = get_post_meta($produto_id, $campo, true);

			foreach ($aulas_data as $i => $data) {

				if(!$aulas_arquivo[$i] && (date('Y-m-d') > converter_para_yyyymmdd($data))) {
					return true;
				}
			}

		}

		return false;
	}

	/**
	 * Verifica se um produto possui algum upload pendente, ou seja, possui uma aula qualquer que o professor ainda não inseriu arquivos
	 * 
	 * @since K5
	 * 
	 * @param int $produto_id Id do produto
	 * 
	 * @return bool
	 */ 

	public static function tem_upload_pendente($produto_id, $tipo)
	{
		$aulas_data = get_post_meta($produto_id, 'aulas_data', true);
		$campo = self::get_nome_campo_aulas_conteudo_wp($tipo);
		
		if($aulas_data) {

		    $aulas_arquivo = get_post_meta($produto_id, $campo, true);

			foreach ($aulas_data as $i => $data) {
					
				if(!$aulas_arquivo[$i]) {
					return true;
				}
			}

		}

		return false;
	}
	
	/**
	 * Recupera o texto das matérias associadas ao produto
	 *
	 * @since L1
	 *
	 * @param int $produto_id Id do produto
	 *
	 * @return string
	 */ 
	
	public static function get_materias_texto($produto_id)
	{
	    $materias_a = [];
	    
	    $materias = ProdutoModel::listar_materias($produto_id);
	    foreach ($materias as $materia) {
	        $materias_a[] = $materia->name;
	    }
	    
	    return $materias_a ? implode(", ", $materias_a) : "";
	}
	
	/**
	 * Recupera o texto das áreas associadas ao produto
	 *
	 * @since L1
	 *
	 * @param int $produto_id Id do produto
	 *
	 * @return string
	 */ 
	
	public static function get_areas_texto($produto_id)
	{
	    $areas_a = [];
	    
	    $areas = ProdutoModel::listar_areas($produto_id);
	    foreach ($areas as $area) {
	        $areas_a[] = $area->name;
	    }
	    
	    return $areas_a ? implode(", ", $areas_a) : "";
	}
	
	/**
	 * Lista os produtos e o nome das áreas concatenadas
	 *
	 * @since L1
	 *
	 * @return array
	 */
	
	public static function listar_produtos_areas_str()
	{
	    $retorno = [];
	    
	    $items = ProdutoModel::listar_produtos_areas_str();
	    foreach ($items as $item) {
	        $retorno[$item->post_id] = $item->areas_str;
	    }
	    
	    return $retorno;
	}
	
	/**
	 * Lista os produtos e o nome dos concatenados
	 *
	 * @since L1
	 *
	 * @return array
	 */
	
	public static function listar_produtos_concursos_str()
	{
	    $retorno = [];
	    
	    $items = ProdutoModel::listar_produtos_concursos_str();
	    foreach ($items as $item) {
	        $retorno[$item->post_id] = $item->concursos_str;
	    }
	    
	    return $retorno;
	}
	
	
	/**
	 * Extrai uma aula de um cronograma através do nome da aula
	 *
	 * @since L1
	 *
	 * @param array $cronograma Lista com as aulas do produto
	 * @param string $nome_aula Nome da aula a ser extraida
	 *
	 * @return array Aula
	 */
	
	public static function extrair_aula_de_cronograma($cronograma, $nome_aula)
	{
	    if ($cronograma) {
	        foreach ($cronograma as $aula) {
	            if (trim($nome_aula) == trim($aula['nome'])) {
	                return $aula;
	            }
	        }
	    }
	    
	    return null;
	}

	/**
	 * Verifica se o caderno está cadastrado em uma aula demo para o produto informado
	 * 
	 * @since L1
	 * 
	 * @param int $caderno_id Id do caderno
	 * @param int $produto_id ID do produto
	 * 
	 * @return boolean TRUE se o caderno está cadastrado em uma aula demo do produto, FALSE do contrário
	 */
	public static function is_caderno_aula_demo_produto($caderno_id, $produto_id)
	{
		$aula_demo = get_post_meta ( $produto_id, 'aula_demo', TRUE );

		if(!is_null($aula_demo))
		{
			$aulas_caderno = get_post_meta($produto_id, 'aulas_caderno', TRUE);
			
			if($aulas_caderno[$aula_demo]){
				
				foreach($aulas_caderno[$aula_demo] as $key => $item) 
				{	
					if($item === $caderno_id)
					{
						return TRUE;
					}
				}

			}
		}

		return FALSE;
	}
	
	/**
	 * Retorna string com listagem de nomes de concursos
	 *
	 * @since L1
	 *
	 * @param int $produto_id ID do produto
	 * @param bool $link Insere link do concurso
	 *
	 * @return string
	 */
	
	public static function get_produto_concursos_str($produto_id, $link = false)
	{
	    
	    $nomes = [];
	    $concursos = ProdutoModel::listar_concursos($produto_id);
	    
	    if($concursos) {
	        foreach ($concursos as $concurso) {
	            
	            if($link) {
	                $nomes[] = "<a href='/concurso/{$concurso->con_slug}'>{$concurso->post_title}</a>";
	            }
	            else {
	                $nomes[] = $concurso->post_title;
	            }
	        }
	        
	        return implode(', ', $nomes);
	    }
	    
	    return "";
	}
	
	/**
	 * Retorna string com listagem de nomes de matérias
	 *
	 * @since L1
	 *
	 * @param int $produto_id ID do produto
	 * @param bool $link Insere link da matéria
	 *
	 * @return string
	 */
	
	public static function get_produto_materias_str($produto_id, $link = false)
	{
	    
	    $nomes = [];
	    $materias = ProdutoModel::listar_materias($produto_id);
	    
	    if($materias) {
	        foreach ($materias as $materia) {
	            
	            if($link) {
	                $nomes[] = "<a href='/cursos-por-materia/{$materia->slug}'>{$materia->name}</a>";
	            }
	            else {
	                $nomes[] = $materia->name;
	            }
	        }
	        
	        return implode(', ', $nomes);
	    }
	    
	    return "";
	}
	
	/**
	 * Retorna string com nome do status do produto
	 *
	 * @since L1
	 *
	 * @param string $status Status do produto no WP
	 *
	 * @return string
	 */
	
	public static function get_status_nome($status) 
	{
	    switch($status) {
	        case 'publish': return 'Ativo';
	        case 'pending': return 'Revisão Pendente';
	        case 'draft': return 'Rascunho';
	    }
	    
	    return "";
	}
	
	/**
	 * Retorna o nome do campo upload não realizado por tipo da aula
	 *
	 * @since L1
	 *
	 * @param int $tipo da aula
	 *
	 * @return string
	 */
	
	public static function get_nome_campo_upload_nao_realizado($tipo)
	{
	    switch($tipo) {
	        case AULA_UPLOAD_TIPO_PDF: return 'pro_upload_nao_realizado';
	        case AULA_UPLOAD_TIPO_VIDEO: return 'pro_upload_video_nao_realizado';
	        case AULA_UPLOAD_TIPO_CADERNO: return 'pro_upload_caderno_nao_realizado';
	    }
	    
	    return "";
	}
	
	/**
	 * Retorna o nome do campo upload pendente por tipo da aula
	 *
	 * @since L1
	 *
	 * @param int $tipo da aula
	 *
	 * @return string
	 */
	
	public static function get_nome_campo_upload_pendente($tipo)
	{
	    switch($tipo) {
	        case AULA_UPLOAD_TIPO_PDF: return 'pro_upload_pendente';
	        case AULA_UPLOAD_TIPO_VIDEO: return 'pro_upload_video_pendente';
	        case AULA_UPLOAD_TIPO_CADERNO: return 'pro_upload_caderno_pendente';
	    }
	    
	    return "";
	}
	
	/**
	 * Retorna o nome do campo de conteúdo da aula no WP
	 *
	 * @since L1
	 *
	 * @param int $tipo da aula
	 *
	 * @return string
	 */
	
	public static function get_nome_campo_aulas_conteudo_wp($tipo)
	{
	    switch($tipo) {
	        case AULA_UPLOAD_TIPO_PDF: return 'aulas_arquivo';
	        case AULA_UPLOAD_TIPO_VIDEO: return 'aulas_vimeo';
	        case AULA_UPLOAD_TIPO_CADERNO: return 'aulas_caderno';
	    }
	    
	    return "";
	}
	
	/**
	 * Retorna o nome do campo de conteúdo da aula no WP
	 *
	 * @since L1
	 *
	 * @param int $tipo da aula
	 *
	 * @return string
	 */
	
	public static function get_nome_campo_tem_conteudo_aula($tipo)
	{
	    switch($tipo) {
	        case AULA_UPLOAD_TIPO_PDF: return 'pau_tem_arquivo';
	        case AULA_UPLOAD_TIPO_VIDEO: return 'pau_tem_video';
	        case AULA_UPLOAD_TIPO_CADERNO: return 'pau_tem_caderno';
	    }
	    
	    return "";
	}

	/**
	 * Retorna as informações sobre um preço de produto recorrente
	 * 
	 * @since M4
	 * 
	 * @param WC_Product $product
	 * 
	 * @return array contendo as informações do preço
	 */
	public static function get_preco_recorrente( $product )
	{
		KLoader::model("RecorrenteModel");

		$preco = is_pacote($product) ? $product->get_bundle_price() : $product->get_price();
		$sem_desconto = $product->get_regular_price();
		$duracao = RecorrenteModel::get_duracao($product->get_id());
		$periodicidade = RecorrenteModel::get_periodicidade($product->get_id());

		switch ($periodicidade) {
			
			case RECORRENTE_DIARIO: {
				$periodicidade_str1 = "por dia";
				$periodicidade_str2 = "diários";
				break;
			}
			
			case RECORRENTE_SEMANAL: {
				$periodicidade_str1 = "por semana";
				$periodicidade_str2 = "semanais";
				break;
			}
			
			case RECORRENTE_MENSAL: {
				$periodicidade_str1 = "por mês";
				$periodicidade_str2 = "mensais";
				break;
			}
			
			case RECORRENTE_ANUAL: {
				$periodicidade_str1 = "por ano";
				$periodicidade_str2 = "anuais";
				break;
			}
		}

		$preco_recorrente['preco'] = $preco;
		$preco_recorrente['sem_desconto'] = $sem_desconto;
		$preco_recorrente['duracao'] = $duracao;
		$preco_recorrente['periodicidade'] = $periodicidade;
		$preco_recorrente['periodicidade_str1'] = $periodicidade_str1;
		$preco_recorrente['periodicidade_str2'] = $periodicidade_str2;

		return $preco_recorrente;
	}
	
}