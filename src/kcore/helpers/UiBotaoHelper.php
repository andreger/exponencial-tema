<?php
/**
 * Helper para organizar rotinas relacionadas ao botões da interface
 * 
 * @package	KCore/Helpers
 *
 * @author André Gervásio <andre.gervasio@keydea.com.br>
 *
 * @since	L1
 */ 

class UiBotaoHelper {

	/**
	 * Botões para a tela de checkout
	 * 
	 * @since L1
	 * 
	 * @param string $id_continuar ID do botão "Adicionar mais itens ao carrinho"
     * @param string $id_finalizar ID do botão "Finalizar"
	 * 
	 * @return string HTML do label configurado.
	 */ 

    public static function finalizar_compra($id_continuar = "btn-continuar", $id_finalizar = "btn-finalizar")
    {
        $continuar_url = continuar_comprando_url();
        
        return KLoader::view("ui/botao/finalizar_compra", [
            'continuar_url' => $continuar_url,
            'id_continuar' => $id_continuar,
            'id_finalizar' => $id_finalizar
        ]);
	}

}	