<?php
/**
 * Helper para organizar rotinas relacionadas ao Vimeo
 * 
 * @package	KCore/Helpers
 *
 * @author 	João Paulo Ferreira <jpaulofms@gmail.com>
 *
 * @since	L1
 */ 

class VimeoHelper {

    /**
     * Retorna se a url dada é um vídeo válido do Vimeo
     * 
     * @since L1
     * 
     * @param string Vimeo url a ser testada
     * 
     * @return bool TRUE se for uma url válida do Vimeo e FALSO caso contrário
     */
    public static function is_vimeo_url($url){
        if(get_vimeo_video_by_url($url)['nome']){
            return true;
        }else{
            return false;
        }

    }

}