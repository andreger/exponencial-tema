<?php
/**
 * Helper para organizar rotinas relacionadas ao relatório de cursos
 *
 * @package	KCore/Helpers
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K5
 */
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;

KLoader::model("RelatorioVendasModel");

class RelatorioVendasHelper{
    
    /* Retorna a tabela com os dados do relatório
    *
    * @since K5
    * 
    * @uses RelatorioVendasHelper::get_table_body para recuperar o corpo da tabela
    * @uses RelatorioVendasHelper::get_table_foot para recuperar o footer da tabela
    * 
    * @param array $lines Linhas do relatório
    *
    * @return string HTML da tabela do relatório
    */
    
    public static function get_table($filtros)
    {
        $table = "<table style='overflow-x: scroll;display:block' border='0' cellspacing='0' cellpadding='0' class='table table-striped table-bordered'>".
            "<tr class='font-10 bg-blue text-white'>".
            "<td>Pedido</td>" .
            "<td>Data</td>".
            "<td>Aluno</td>";
        
        $table .= tem_acesso([ADMINISTRADOR, COORDENADOR]) ? "<td>CPF</td>" : "";
        $table .= tem_acesso([ADMINISTRADOR, COORDENADOR, ATENDENTE, APOIO, REVISOR_SQ]) ? "<td>Telefone</td>" : "";
        $table .= tem_acesso([ADMINISTRADOR, COORDENADOR]) ? "<td>E-mail</td>" : "";
        $table .= tem_acesso([ADMINISTRADOR]) ? "<td>Professor</td>" : "";
        $table .= tem_acesso([ADMINISTRADOR]) ? "<td>Convênio</td>" : "";
        $table .= "<td>Curso</td>";
        $table .= "<td>Forma de Pgto</td>";
        $table .= "<td>Parcelamento</td>";
        $table .= "<td>Valor de Venda</td>";
        $table .= "<td>Desconto / Cupom</td>";
        $table .= "<td>Reembolso</td>";
        $table .= "<td>Valor Pago</td>";
        $table .= "<td>Alíquota Simples</td>";
        $table .= "<td>Valor Imposto</td>";
        $table .= "<td>PagSeguro</td>";
        $table .= "<td>Afiliados</td>";
        $table .= "<td>Líquido da Venda</td>";
        $table .= "<td>Percentual do Professor</td>";
        $table .= "<td>Valor Líquido para o Professor</td>";
        
        if(tem_acesso([ADMINISTRADOR, COORDENADOR])) {
            $table .= "<td>Valor Líquido pós Professor</td>";
            $table .= "<td>Percentual do Coordenador</td>";
            $table .= "<td>Valor Líquido para o Coordenador</td>";
        }
        
        $table .= "</tr>";
        
        $table .= self::get_table_body($filtros);
        $table .= self::get_table_foot($filtros);
        
        $table .=  "</table>";
        
        return $table;
    }
    
    /**
     * Retorna o body da tabela com os dados do relatório
     *
     * @since K5
     * 
     * @uses RelatorioVendasModel::get_dados() para recuperar os dados do relatório
     *
     * @param array $filtros Filtros do relatório
     *
     * @return string HTML do body da tabelas
     */ 
    
    private static function get_table_body($filtros)
    {
        $lines = RelatorioVendasModel::get_dados($filtros);
        
        foreach ($lines as $line) {
            $reembolso = $line['reembolso'];
            $valor_pago = $line['total'] - $line['discount'] - $reembolso;
            // 		$aliquota = get_imposto_por_data($line['date']);
            $aliquota = $line['aliquota'];
            $valor_imposto = $aliquota / 100 * $valor_pago;
            $pagseguro = $line['pagseguro'];
            $folha_dirigida = $valor_pago * $line['folha_dirigida'];
            $liquido = $valor_pago - $valor_imposto - $pagseguro - $folha_dirigida;
            // 		$percentual_professor = get_lucro_por_data($line['professor_id'], $line['date']) * ($line['participacao'] / 100);
            
            $percentual_professor = $line['perc_professor'];
            $lucro_professor = ($percentual_professor / 100) * $liquido;
            $liquido_pos_professor = $liquido - $lucro_professor;
            
            $percentual_coordenadores = $line['perc_coordenadores'];
            $lucro_coordenadores = $line['item_tipo'] == 1 ? 0 : $percentual_coordenadores / 100 * $liquido_pos_professor;
            
            $table .= "<tr>" .
                "<td>{$line['order_id']}</td>" .
                "<td>{$line['date']}</td>".
                "<td>" . $line['user_name'] . "</td>";
            
            $cpf = $line['user_cpf'] ? desformatar_cpf($line['user_cpf']) : "";
            
            $table .= tem_acesso([ADMINISTRADOR, COORDENADOR]) ? "<td>" .  $cpf . "</td>" : "";   
            $table .= tem_acesso([ADMINISTRADOR, COORDENADOR, ATENDENTE, APOIO, REVISOR_SQ]) ? "<td>" . $line['user_telefone'] . "</td>" : "";
            $table .= tem_acesso([ADMINISTRADOR, COORDENADOR]) ? "<td>" . $line['user_email'] . "</td>" : "";
            $table .= tem_acesso([ADMINISTRADOR]) ? "<td>" . $line['professor'] . "</td>" : "";
            $table .= tem_acesso([ADMINISTRADOR]) ? "<td>" . get_parceiro_by_id($line['parceiro']) . "</td>" : "";
            $table .= "<td>" . $line['name'] . "</td>";
            $table .= "<td>" . $line['metodo'] . "</td>";
            $table .= "<td>" . $line['parcelas'] . "</td>";
            $table .= "<td>" . format_moeda($line['total']) . "</td>";
            $table .= $line['discount'] != 0 ? "<td>" . format_moeda($line['discount']) . "</td>" : "<td></td>";
            $table .= "<td>" . format_moeda($reembolso) . "</td>";
            $table .= "<td>" . format_moeda($valor_pago) . "</td>";
            $table .= "<td>" . porcentagem($aliquota) . "</td>";
            $table .= "<td>" . format_moeda($valor_imposto) . "</td>";
            $table .= "<td>" . format_moeda($pagseguro) . "</td>";
            $table .= "<td>" . format_moeda($folha_dirigida) . "</td>";
            $table .= "<td>" . format_moeda($liquido) . "</td>";
            $table .= "<td>" . porcentagem($percentual_professor) . "</td>";
            $table .= "<td>" . format_moeda($lucro_professor) . "</td>";
            
            if(tem_acesso([ADMINISTRADOR, COORDENADOR])) {
                $table .= "<td>" . format_moeda($liquido_pos_professor) . "</td>";
                $table .= "<td>" . porcentagem($percentual_coordenadores) . "</td>";
                $table .= "<td>" . format_moeda($lucro_coordenadores) . "</td>";
            }

            $table .= "</tr>";
        }
        
        return $table;
    }
    
    /**
     * Retorna o foot da tabela com os dados do relatório
     *
     * @since K5
     *
     * @uses RelatorioVendasModel::get_dados() para recuperar os dados do relatório
     *
     * @param array $filtros Filtros do relatório
     *
     * @return string HTML do foot da tabelas
     */ 
    
    private static function get_table_foot($filtros)
    {
        $filtros['limit'] = null;
        
        $lines = RelatorioVendasModel::get_dados($filtros);
        
        $soma_valor_de_venda = 0;
        $soma_desconto = 0;
        $soma_reembolso = 0;
        $soma_valor_pago = 0;
        $soma_valor_imposto = 0;
        $soma_pag_seguro = 0;
        $soma_folha_dirigida = 0;
        $soma_liquido_venda = 0;
        $soma_valor_liquido_professor = 0;
        $soma_valor_liquido_coordenadores = 0;
        
        $soma = array();
        
        foreach ($lines as $line) {
            $reembolso = $line['reembolso'];
            $valor_pago = $line['total'] - $line['discount'] - $reembolso;
            // 		$aliquota = get_imposto_por_data($line['date']);
            $aliquota = $line['aliquota'];
            $valor_imposto = $aliquota / 100 * $valor_pago;
            $pagseguro = $line['pagseguro'];
            $folha_dirigida = $valor_pago * $line['folha_dirigida'];
            $liquido = $valor_pago - $valor_imposto - $pagseguro - $folha_dirigida;
            // 		$percentual_professor = get_lucro_por_data($line['professor_id'], $line['date']) * ($line['participacao'] / 100);
            
            $percentual_professor = $line['perc_professor'];
            $lucro_professor = ($percentual_professor / 100) * $liquido;
            $liquido_pos_professor = $liquido - $lucro_professor;
            
            $percentual_coordenadores = $line['perc_coordenadores'];
            $lucro_coordenadores = $line['item_tipo'] == 1 ? 0 : $percentual_coordenadores / 100 * $liquido_pos_professor;
            
            if($line['somar']) {
                $soma_valor_de_venda += $line['total'];
                $soma_desconto += $line['discount'];
                $soma_reembolso += $reembolso;
                $soma_valor_pago += $valor_pago;
                $soma_valor_imposto += $valor_imposto;
                $soma_pag_seguro += $pagseguro;
                $soma_folha_dirigida += $folha_dirigida;
                $soma_liquido_venda += $liquido;
                $soma_valor_liquido_coordenadores += $lucro_coordenadores;
            }
            
            if($lucro_professor) {
                $soma_valor_liquido_professor += $lucro_professor;
            }
            
        }
        
        if(!empty($lines)) {
            
            $table .= "<tfoot><tr class='bg-secondary text-white font-11'>".
                "<td></td>" .
                "<td></td>".
                "<td></td>";
            
            $table .= tem_acesso([ADMINISTRADOR, COORDENADOR]) ? "<td></td>" : "";
            $table .= tem_acesso([ADMINISTRADOR, COORDENADOR, ATENDENTE, APOIO, REVISOR_SQ]) ? "<td></td>" : "";
            $table .= tem_acesso([ADMINISTRADOR, COORDENADOR]) ? "<td></td>" : "";
            $table .= tem_acesso([ADMINISTRADOR]) ? "<td></td>" : "";
            $table .= tem_acesso([ADMINISTRADOR]) ? "<td></td>" : "";
            $table .= "<td></td>";
            $table .= "<td></td>";
            $table .= "<td></td>";
            $table .= "<td>" . format_moeda($soma_valor_de_venda) . "</td>";
            $table .= "<td>" . format_moeda($soma_desconto) . "</td>";
            $table .= "<td>" . format_moeda($soma_reembolso) . "</td>";
            $table .= "<td>" . format_moeda($soma_valor_pago) . "</td>";
            $table .= "<td></td>";
            $table .= "<td>" . format_moeda($soma_valor_imposto) . "</td>";
            $table .= "<td>". format_moeda($soma_pag_seguro) . "</td>";
            $table .= "<td>". format_moeda($soma_folha_dirigida) . "</td>";
            $table .= "<td>". format_moeda($soma_liquido_venda) . "</td>";
            $table .= "<td></td>";
            $table .= "<td>" . format_moeda($soma_valor_liquido_professor) . "</td>";
            
            if(tem_acesso([ADMINISTRADOR, COORDENADOR])) {
                $table .= "<td>" . format_moeda($soma_liquido_venda - $soma_valor_liquido_professor) . "</td>";
                $table .= "<td></td>";
                $table .= "<td>" . format_moeda($soma_valor_liquido_coordenadores) . "</td>";
            }

            $table .= "</tr></tfoot>";
        }
        
        return $table;
    }
    
    /**
     * Exporta um relatório de vendas para XLSX e grava em arquivo
     *
     * @since K5
     *
     * @uses RelatorioVendasModel::get_dados() para recuperar os dados do relatório
     *
     * @param array $linhas Linhas a serem exportadas
     *
     * @return string Endereço relativo do arquivo (ex: /wp-content/temp/relatorio-de-vendas-99999999.xlsx
     */ 
    
    public static function exportar_xls($linhas, $user_id)
    {
        $arquivo = '/wp-content/temp/relatorio-de-vendas-' . time() . '.xlsx';

        $writer = WriterFactory::create(Type::XLSX);
        $writer->openToFile($_SERVER['DOCUMENT_ROOT'] . $arquivo);
        
        $row = array();
        array_push($row, "Pedido");
        array_push($row, "Data");
        array_push($row, "Aluno");
        
        if(is_administrador($user_id) || is_coordenador($user_id)) {
            array_push($row, "CPF");
        }
        
        if(is_administrador($user_id) || is_coordenador($user_id) || is_atendente($user_id) || is_apoio($user_id) || is_revisor_sq($user_id)) {
            array_push($row, "Telefone");
        }
        
        array_push($row, "Categoria");
        array_push($row, "Áreas");
        array_push($row, "Cidade");
        array_push($row, "Estado");
        
        if(is_administrador($user_id) || is_coordenador($user_id)) {
            array_push($row, "E-mail");
        }
        
        if(is_administrador($user_id) || is_coordenador($user_id)) {
            array_push($row, "Professor");
            array_push($row, "Convênio");
        }
        
        array_push($row, "Curso");
        array_push($row, "Forma de Pgto");
        array_push($row, "Parcelamento");
        array_push($row, "Valor de Venda");
        array_push($row, "Desconto / Cupom");
        array_push($row, "Reembolso");
        array_push($row, "Valor Pago");
        array_push($row, "Valor Pago no Somatório");
        array_push($row, "Alíquota Simples");
        array_push($row, "Valor Imposto");
        array_push($row, "PagSeguro");
        array_push($row, "% Afiliados");
        array_push($row, "Líquido da Venda");
        array_push($row, "Percentual do Professor");
        array_push($row, "Valor Líquido para o Professor");
        
        if(is_administrador($user_id) || is_coordenador($user_id)) {
            array_push($row, "Valor Líquido pós Professor");
            array_push($row, "Percentual do Coordenador");
            array_push($row, "Valor Líquido para o Coordenador");
        }

        if(is_administrador($user_id)) {
            array_push($row, "ID da Transação");
        }
        
        $writer->addRow($row);
        
        $soma_valor_de_venda = 0;
        $soma_desconto = 0;
        $soma_reembolso = 0;
        $soma_valor_pago = 0;
        $soma_valor_pago_somatorio = 0;
        $soma_valor_imposto = 0;
        $soma_pag_seguro = 0;
        $soma_folha_dirigida = 0;
        $soma_liquido_venda = 0;
        $soma_valor_liquido_professor = 0;
        
        foreach ($linhas as $line) {
            /**
             * Valor Pago no Somatório 
             * 
             * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2681
             * 
             * Basicamente, o campo "valor pago no somatório" aparece nas seguinte situações:
             * 1- Linha GLOBAL da venda do curso com +1 prof
             * 2- Linha da venda de curso com APENAS 1 prof
             * 
             * Não aparece:
             * 1- Na linha da distribuição de % entre professores em curso com +1 prof
             * 2- Na linha do pacote
             */
            
            $exibir_valor_pago_somatorio = false;
            
            if($line['item_tipo'] == ITEM_RAIZ_PACOTE) { // Linha global de pacotes
                $exibir_valor_pago_somatorio = true;
            }
            elseif($line['item_tipo'] == ITEM_NAO_RAIZ_PACOTE) { // Linhas individuais de pactes
                $exibir_valor_pago_somatorio = false;
            }
            else { // Casos de produto não-pacotes
                if($line['professor_id'] == -1) { // Linhas individuais de produtos com múltiplos autores
                    $exibir_valor_pago_somatorio = false;
                }
                else {
                    $exibir_valor_pago_somatorio = true; // Linha global de produto com múltiplos autores ou linha única de produto com 1 autor
                }
            }
            
            $reembolso = $line['reembolso'];
            $valor_pago = $line['total'] - $line['discount'] - $reembolso;
            $valor_pago_somatorio = $exibir_valor_pago_somatorio ? $valor_pago : 0;
            $aliquota = $line['aliquota'];
            $valor_imposto = $aliquota / 100 * $valor_pago;
            $pagseguro = $line['pagseguro'];
            $folha_dirigida = $valor_pago * $line['folha_dirigida'];
            $liquido = $valor_pago - $valor_imposto - $pagseguro - $folha_dirigida;
            
            $percentual_professor = $line['item_tipo'] == ITEM_RAIZ_PACOTE ? 0 : $line['perc_professor'];
            $lucro_professor = $line['item_tipo'] == ITEM_RAIZ_PACOTE ? 0 : ($percentual_professor / 100) * $liquido;
            $liquido_pos_professor = $liquido - $lucro_professor;
            
            $percentual_coordenadores = $line['perc_coordenadores'];
            $lucro_coordenadores = $line['item_tipo'] == ITEM_RAIZ_PACOTE ? 0 : $percentual_coordenadores / 100 * $liquido;
            
            if($line['somar']) {
                $soma_valor_de_venda += $line['total'];
                $soma_desconto += $line['discount'];
                $soma_reembolso += $reembolso;
                $soma_valor_pago += $valor_pago;
                $soma_valor_imposto += $valor_imposto;
                $soma_pag_seguro += $pagseguro;
                $soma_folha_dirigida += $folha_dirigida;
                $soma_liquido_venda += $liquido;
                $soma_valor_liquido_coordenadores += $lucro_coordenadores;
            }
            
            // A lógica do valor pago no somatório é diferente. Neste caso basta somar todas as linhas
            $soma_valor_pago_somatorio += $valor_pago_somatorio;
            
            if($lucro_professor) {
                $soma_valor_liquido_professor += $lucro_professor;
                
            }
            
            $row = array();
            array_push($row, $line['order_id']);
            array_push($row, $line['date']);
            array_push($row, $line['user_name']);
            
            if(is_administrador($user_id) || is_coordenador($user_id)) {
                array_push($row, $line['user_cpf']);
            }
            
            if(is_administrador($user_id) || is_coordenador($user_id) || is_atendente($user_id) || is_apoio($user_id) || is_revisor_sq($user_id)) {
                array_push($row, $line["user_telefone"]);
            }
            
            array_push($row, ucfirst(get_categorias_str($line['curso_id'], TRUE)));
            array_push($row, $line['areas']);
            array_push($row, $line['user_cidade']);
            array_push($row, $line['user_estado']);
            
            if(is_administrador($user_id) || is_coordenador($user_id)) {
                array_push($row, $line['user_email']);
            }
            
            if(is_administrador($user_id) || is_coordenador($user_id)) {
                array_push($row, $line['professor']);
                array_push($row, $line['parceiro']);
            }
            
            array_push($row, $line['name']);
            array_push($row, $line['metodo']);
            array_push($row, $line['parcelas']);
            array_push($row, format_moeda($line['total']));
            
            if($line['discount'] != 0) {
                array_push($row, format_moeda($line['discount']));
            }
            else {
                array_push($row, "");
            }
            
            if($line['reembolso'] != 0) {
                array_push($row, format_moeda($line['reembolso']));
            }
            else {
                array_push($row, "");
            }
            
            array_push($row, format_moeda($valor_pago));
            
            if(is_administrador($user_id) || is_coordenador($user_id)) {
                array_push($row, format_moeda($valor_pago_somatorio));
            }
            
            array_push($row, porcentagem($aliquota));
            array_push($row, format_moeda($valor_imposto));
            array_push($row, format_moeda($pagseguro));
            array_push($row, format_moeda($folha_dirigida));
            array_push($row, format_moeda($liquido));
            array_push($row, porcentagem($percentual_professor));
            array_push($row, format_moeda($lucro_professor));
            
            if(is_administrador($user_id) || is_coordenador($user_id)) {
                array_push($row, format_moeda($liquido_pos_professor));
                array_push($row, porcentagem($percentual_coordenadores));
                array_push($row, format_moeda($lucro_coordenadores));
            }

            if(is_administrador($user_id)) {
                array_push($row, $line['transacao_id']);
            }
            
            $writer->addRow($row);
        }
        
        if(!empty($linhas)) {
            $row = array();
            array_push($row, "");
            array_push($row, "");
            array_push($row, "");
            
            
            if(is_administrador($user_id) || is_coordenador($user_id)) {
                array_push($row, ""); // CPF
            }
            
            if(is_administrador($user_id) || is_coordenador($user_id) || is_atendente($user_id) || is_apoio($user_id) || is_revisor_sq($user_id)) {
                array_push($row, ""); // Telefone
            }
            
            array_push($row, ""); // Areas
            array_push($row, "");
            array_push($row, "");
            array_push($row, "");
            
            if(is_administrador($user_id) || is_coordenador($user_id)) {
                array_push($row, ""); // E-mail
            }
            
            if(is_administrador($user_id) || is_coordenador($user_id)) {
                array_push($row, ""); // Professor
                array_push($row, ""); // Convênio
            }
            
            array_push($row, "");
            array_push($row, "");
            array_push($row, "");
            
            array_push($row, format_moeda($soma_valor_de_venda));
            array_push($row, format_moeda($soma_desconto));
            array_push($row, format_moeda($soma_reembolso));
            array_push($row, format_moeda($soma_valor_pago));
            
            if(is_administrador($user_id) || is_coordenador($user_id)) {
                array_push($row, format_moeda($soma_valor_pago_somatorio));
            }
            
            array_push($row, "");
            array_push($row, format_moeda($soma_valor_imposto));
            array_push($row, format_moeda($soma_pag_seguro));
            array_push($row, format_moeda($soma_folha_dirigida));
            array_push($row, format_moeda($soma_liquido_venda));
            array_push($row, "");
            array_push($row, format_moeda($soma_valor_liquido_professor));
            
            if(is_administrador($user_id) || is_coordenador($user_id)) {
                array_push($row, format_moeda($soma_liquido_venda - $soma_valor_liquido_professor));
                array_push($row, "");
                array_push($row, format_moeda($soma_valor_liquido_coordenadores));
            }

            if(is_administrador($user_id)) {
                array_push($row, "");
            }
            
            $writer->addRow($row);
        }
        $writer->close();
        
        return $arquivo;
    }
} 