<?php
/**
 * Helper para organizar rotinas relacionadas às requisições de URL
 * 
 * @package	KCore/Helpers
 *
 * @author 	João Paulo Ferreira <jpaulofms@gmail.com>
 *
 * @since	L1
 */ 

class UrlRequestHelper {

    /**
     * Retorna o conteúdo de uma requisição 
     * 
     * @since L1
     * 
     * @param string $url Url solicitada
     * @param string $useragent User agent
     * @param array $headers Headers
     * @param bool $follow_redirects Flag para follow redirects
     * @param bool $debug Flag para debug
     * 
     * @return string Conteúdo HTML
     */
    
    public static function get_conteudo($url, $useragent='cURL', $headers = false, $follow_redirects = true, $debug = false)
    {
        // initialise the CURL library
        $ch = curl_init();
        
        // specify the URL to be retrieved
        curl_setopt($ch, CURLOPT_URL,$url);
        
        // we want to get the contents of the URL and store it in a variable
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        
        // specify the useragent: this is a required courtesy to site owners
        curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
        
        // ignore SSL errors
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        
        // return headers as requested
        if ($headers==true){
            curl_setopt($ch, CURLOPT_HEADER,1);
        }
        
        // only return headers
        if ($headers=='headers only') {
            curl_setopt($ch, CURLOPT_NOBODY ,1);
        }
        
        // follow redirects - note this is disabled by default in most PHP installs from 4.4.4 up
        if ($follow_redirects==true) {
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        }
        
        // if debugging, return an array with CURL's debug info and the URL contents
        if ($debug == true) {
            $result = [
                'contents' => curl_exec($ch),
                'info' => curl_getinfo($ch)
            ];
        }
        
        // otherwise just return the contents as a variable
        else {
            $result=curl_exec($ch);
        }
        
        // free resources
        curl_close($ch);
        
        // send back the data
        return $result;
    }
    
    /**
     * Retorna o código da requisição de uma URL
     *
     * @since L1
     *
     * @param string $url Url solicitada
     *
     * @return int Código de retorno
     */
    
    public static function get_codigo_resposta($url)
    {
        $ch = curl_init($url);
        
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        
        curl_close($ch);
        
        return $code;
    }

}