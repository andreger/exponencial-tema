<?php
/**
 * Helper para organizar rotinas relacionadas às interfaces de labels
 * 
 * @package	KCore/Helpers
 *
 * @author 	Ivan Duarte <ivan.duarte@keydea.com.br>
 *
 * @since	K4
 */ 

class UiLabelHelper {

	/**
	 * Efetua a busca do tipo de label de acordo com o id do post.
	 * 
	 * @since K4
	 * 
	 * @param int $post_id ID do posts.
	 * 
	 * @return string HTML do label configurado.
	 */ 

	public static function get_blog_label_html($post_id)
	{
		$html = "";
		$class = "flt-left";	
		$j = -1; 
		foreach (get_midia_categorias($post_id) as $estilo => $titulo)
		{ 
			$j++;
			$top = 25 + ($j * 33);
			$style = "top: {$top}px";
			$html .= self::get_label_html($class." ".$estilo, $titulo, $style);
		}

		return $html;
	}

	/**
	 * Efetua a busca do tipo de label de acordo com o status do concurso.
	 * 
	 * @since K4
	 * 
	 * @param int $con_status numero do status do concurso.
	 * 
	 * @return string HTML do label configurado.
	 */ 

	public static function get_concurso_label_html($con_status)
	{
		$html = "";
		if($label = ConcursoHelper::get_status_titulo($con_status))
		{	
			$class = "home-concurso-float";	
			$estilo = ConcursoHelper::get_status_estilo($con_status);
			$html .= self::get_label_html($class." ".$estilo, $label, "");
		}

		return $html;
	}

	/**
	 * Recebe os parametros para configuração do label e retorna o seu html.
	 * 
	 * @since K4
	 * 
	 * @param string $estilo classe css.
	 * @param string $tipo tipo do post para identificar no label.
	 * @param string $style configuração style quando houver.
	 * 
	 * @return string HTML do label.
	 */ 

	public static function get_label_html($class, $label, $style)
	{
		return "<div class='course-price product-price '>
					<div class='pt-1 pb-1 pr-3 pl-3 rounded {$class}' style='{$style}'>
						<span class='amount'>{$label}</span>				
					</div>
				</div>";
	}
}	