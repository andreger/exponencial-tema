<?php
/**
 * Helper para organizar rotinas relacionadas às interfaces de mensagens
 * 
 * @package	KCore/Helpers
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K5
 */ 

class UiMensagemHelper {

	/**
	 * Retorna um box de mensagem de erro
	 * 
	 * @since K5
	 * 
	 * @param string $texto Texto da mensagem.
	 * 
	 * @return string HTML do label configurado.
	 */ 

	public static function get_mensagem_erro_html($texto)
	{
		$html = "<div class='alert alert-danger'>
                    {$texto}
                </div>";

		return $html;
	}

	/**
	 * Retorna um box de mensagem de informações
	 * 
	 * @since K5
	 * 
	 * @param string $texto Texto da mensagem.
	 * 
	 * @return string HTML do label configurado.
	 */ 

	public static function get_mensagem_info_html($texto)
	{
		$html = "<div class='alert alert-info'>
                    {$texto}
                </div>";

		return $html;
	}

	/**
	 * Retorna um box de mensagem de sucesso
	 * 
	 * @since K5
	 * 
	 * @param string $texto Texto da mensagem.
	 * 
	 * @return string HTML do label configurado.
	 */ 

	public static function get_mensagem_sucesso_html($texto)
	{
		$html = "<div class='alert alert-success'>
                    {$texto}
                </div>";

		return $html;
	}
	
	/**
	 * Retorna um box com imagem e texto de carregando
	 *
	 * @since K5
	 *
	 * @param string $texto Texto da mensagem.
	 *
	 * @return string HTML do label configurado.
	 */
	
	public static function get_carregando_html($texto = "Carregando...")
	{
	    $html = "<div class='loading'>
                    <img src='/wp-content/themes/academy/images/carregando.gif'> {$texto}
                </div>";
                    
        return $html;
	}

}