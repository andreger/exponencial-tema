<?php
/**
 * Helper para organizar rotinas relacionadas aos professores
 * 
 * @package	KCore/Helpers
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	L1
 */ 

class ProfessorHelper {

	/**
	 * Retorna o html do avatar do professor
	 * 
	 * @since L1
	 * 
	 * @param int $tipo_id Id do tipo do blog
	 * 
	 * @return string HTML do avatar do professor
	 */ 

	static public function get_avatar_box($professor_id)
	{
	    KLoader::helper("UrlHelper");
	    
	    $professor = get_usuario_array($professor_id);
	    
	    $data = [
	        "nome" => $professor['nome_completo'],
	        "link" => get_autor_url($professor_id),
	        "imagem" => get_avatar_url($professor_id, 100)
	    ];

	    return KLoader::view("professor/avatar/avatar", $data, true);
	}

}