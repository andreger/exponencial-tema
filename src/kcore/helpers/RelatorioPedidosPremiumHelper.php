<?php
/**
 * Helper para organizar rotinas relacionadas ao relatório de pedidos premium
 *
 * @package	KCore/Helpers
 *
 * @author 	João Paulo Ferreira <jpaulofms@gmail.com>
 *
 * @since	L1
 */
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;

class RelatorioPedidosPremiumHelper {
    
    /**
     * Exporta um relatório de pedidos premium para XLSX e grava em arquivo
     *
     * @since L1
     *
     * @param array $linhas Linhas a serem exportadas
     *
     * @return string Endereço relativo do arquivo (ex: /wp-content/temp/relatorio-pedidos-premium-99999999.xlsx
     */ 
    
    public static function exportar_xls($premiums)
    {        
        $arquivo = '/wp-content/temp/relatorio-pedidos-premium-' . time() . '.xlsx';

        $writer = WriterFactory::create(Type::XLSX);
        $writer->openToBrowser($_SERVER['DOCUMENT_ROOT'] . $arquivo);
        $row = ["Pedido ID", "Produto Premium ID", "Nome Produto Premium", "Item ID", "Nome do Item", "Pedido Oculto", "Data de Requisição"];
        $writer->addRow($row);
        
        foreach($premiums as $premium) {

            $row = [ 
                    $premium->pedido_id, 
                    $premium->produto_id, 
                    $premium->nome_produto, 
                    $premium->item_id, 
                    $premium->nome_item, 
                    $premium->pedido_oculto_id, 
                    converter_para_ddmmyyyy_HHiiss($premium->prl_data) 
                ];

            $writer->addRow($row);
        }
        $writer->close();
        
        return $arquivo;
    }
}