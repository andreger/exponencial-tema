<?php
/**
 * Helper para organizar rotinas relacionadas à URLs
 *
 * @package	KCore/Helpers
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K5
 */

class SeoH1Helper {
    
    /**
     * Extrai as informações de H1 do html
     *
     * @since K5
     *
     * @param int $id Id do post
     *
     * @return array Dados sobre o H1
     */
    
    public static function parse_html($id)
    {      
        KLoader::helper("UrlRequestHelper");
        
        $permalink = get_permalink($id);
             
        $html = str_get_html(UrlRequestHelper::get_conteudo($permalink));
             
        if($html) {
            $title = $html->find("title", 0);
            
            $titulo = "";
            if($title) {
                $titulo = $title->innertext;
            }
            
            // ignora as páginas inexistentes
            if(starts_with($titulo, "Página não encontrada")) {
                return null;
            }
            
            $h1s = $html->find("h1");
            
            $h1_info = "";
            $h1_num = 0;
            
            if($h1s) {
                $h1_num = count($h1s);
                
                $h1_a = [];
                foreach ($h1s as $h1) {
                    if($texto = $h1->innertext) {
                        array_push($h1_a, $texto);
                    }
                }
                
                $h1_info = implode(" ; ", $h1_a);
            }
            
            $edit_link = "/wp-admin/post.php?post={$id}&action=edit";
            
            $dados = [
                'sh1_id' => $id,
                'sh1_url' => $permalink,
                'sh1_titulo' => $titulo,
                'sh1_h1' => $h1_info,
                'sh1_num' => $h1_num,
                'sh1_edit_url' => $edit_link
            ];
            
            return $dados;
        }
    }
}
    