<?php
/**
 * Helper para organizar rotinas relacionados aos pedidos
 * 
 * @package	KCore/Helpers
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	L1
 */ 

class PedidoHelper {

	/**
	 * Extrai um URL de download de um pedido
	 * 
	 * @since L1
	 * 
	 * @param array $downloadable_items Lista de itens de downloads de um pedido
	 * @param string $download_id Id de um item de download
	 * 
	 * @return string URL de um item de download
	 */ 

	public static function extrair_download_url($downloadable_items, $download_id)
	{
		$str_a = [];
		foreach ($professores as $professor) {
				$url =	UrlHelper::get_professor_url($professor->col_slug);
				$str_a[] = "<a href='{$url}' class='author'>{$professor->display_name}</a>";
		}

		$str = implode($str_a, ', ');
		$str = str_replace_last(', ', ' e ', $str); 

		$t = count($professores) > 1 ? "Professores: " : "Professor: ";
		$str = "<span class='font-weight-bold'>$t</span>" . $str;

		return $str; 
	}
	
	/**
	 * Migra os dados de pedidos de um usuário para a nova tabela pedido_produto
	 * 
	 * @since L1
	 * 
	 * @param int $user_ID ID do usuário que terá os pedidos migrados
	 * @param bool $is_migrar_agora TRUE se for para migrar agora
	 * 
	 * @return bool TRUE se tem pedidos a serem migrados
	 */
	public static function migrar_pedidos($user_ID, $is_migrar_agora){
		
		KLoader::model('PedidoModel');

		$qtd_pedidos = PedidoModel::contar_meus_pedidos($user_ID);
		$is_migrar_pedidos = $qtd_pedidos == 0;

		if($is_migrar_pedidos && $is_migrar_agora){

			//Migrar pedidos do usuário se não possui pedidos e tem a flag
			$result = PedidoModel::listar_pedidos_usuario($user_ID);

			foreach ( $result as $row ) {
				$post_id = $row->post_id;
				PedidoModel::alterar_pedido_status_usuario($post_id);
			}
		}

		return $is_migrar_pedidos;

	}

	/**
	 * Nivela a lista de aulas e arquivos de um produto para um lista de aulas
	 *
	 * @since L1
	 *
	 * @param array $aulas_agrupadas Lista de aulas e arquivos de um produto.
	 *
	 * @return array Lista de arquivos
	 */
	
	public static function nivelar_arquivos_aulas_disponiveis($aulas_agrupadas)
	{
	    $resultado = [];
	    
	    foreach ($aulas_agrupadas as $aula) {
	        
	        foreach ($aula as $item) {
	            if ($item['tipo'] == TIPO_AULA_PDF) {
	                $resultado[] = $item['file'];
	            }
	        }
	    }
	    
	    return $resultado;
	}
	
}