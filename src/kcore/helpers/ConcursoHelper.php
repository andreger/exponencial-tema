<?php
/**
 * Helper para organizar rotinas relacionadas aos concursos
 * 
 * @package	KCore/Helpers
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K4
 */ 

class ConcursoHelper {

	/**
	 * Retorna a estilo css de um determinado status de concurso
	 * 
	 * @since K4
	 * 
	 * @param int $tipo_id Id do tipo do blog
	 * 
	 * @return string Estilo do tipo do blog
	 */ 

	static public function get_status_estilo($status)
	{
		switch ($status) {
			case EDITAL_DIVULGADO: return ""; 
			case EDITAL_PROXIMO: return "midia-artigo-yellow-2";
			default: return "";
		}
	}

	/**
	 * Retorna o titulo de um status de concurso
	 * 
	 * @since K4
	 * 
	 * @param int $tipo_id Id do tipo do blog
	 * 
	 * @return string Estilo do tipo do blog
	 */ 

	static public function get_status_titulo($status)
	{
		switch ($status) {
			case EDITAL_DIVULGADO: return "Edital Divulgado";
			case EDITAL_PROXIMO: return "Edital Próximo";
			default: return NULL;
		}
	}

}