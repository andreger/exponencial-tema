<?php
/**
 * Helper para organizar rotinas relacionadas ao head
 * 
 * @package	KCore/Helpers
 *
 * @author 	João Paulo Ferreira <jpaulofs@gmail.com>
 *
 * @since	L1
 */ 

class HeadHelper {

    /**
     * Gera e retorna a canonical tag para evitar conteúdo duplicado
     * 
     * @since L1
     * 
     * @return string canonical tag
     */
    static public function get_canonical() 
    {
        $url = get_url_atual();

        // remove parametros da querystring que não seja de busca
        $url_a = parse_url($url);
        $params = "";

        if(isset($url_a['query'])) {
            if($qs = $url_a['query']) {
                parse_str($qs, $params);
                
                foreach($params as $key => $value) {
                    if($key != "q" || !$value) {
                        unset($params[$key]);
                    }
                }
            }
        }

        $params_str = $params ? http_build_query($params) : "";

        $protocol = is_producao() ? "https://" : "http://";
        $url = $protocol . $url_a['host'] . $url_a['path'] . $params_str;

        /**
         * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2588
         * 
         * Remove a barra do final da url, se houver
         */
        $url = rtrim($url, "/");

        return "<link rel='canonical' href='$url' />";
    }

    /**
     * Retorna o título apropriado para cada página do sistema
     * 
     * @since L1
     * 
     * @param string $title o título da página
     * 
     * @return string o título apropriado para a página
     */
    static public function render_title( $title )
    {
        KLoader::helper('UrlHelper');

        $sufixo = " | Exponencial Concursos";

        if(UrlHelper::is_url_professor()) {
            global $professor;
            $title = $professor->display_name . $sufixo;
        }
        elseif(UrlHelper::is_url_cursos_por_professor_especifico()) {
            global $professor;
            $title = "Cursos - " . $professor->display_name . $sufixo;
        }
        elseif(UrlHelper::is_url_cursos_por_materia_especifica()) {
            global $materia;
            $title = "Cursos - " . $materia->name . $sufixo;
        }
        elseif(UrlHelper::is_url_meu_perfil()) {
            global $materia;
            $title = "Meu Perfil" . $sufixo;
        }elseif(UrlHelper::is_url_artigos_por_professor_especifico()){
            global $professor;
            $title = "Artigos do Professor - ". $professor->display_name . $sufixo;
        }elseif(UrlHelper::is_url_cursos_por_concurso_especifico_paginada()){
            global $concurso;
            $title = "Concurso - " . $concurso->post_title;
        }

        return $title;
    }
    
    /**
     * Retorna os metadados de produto
     *
     * @since L1
     */
    static public function get_produto_meta()
    {
        KLoader::helper('UrlHelper');
        KLoader::helper("ProdutoHelper");
        
        $url = get_url_atual();
        if(UrlHelper::is_url_produto($url)) {
            global $post;
            
            if($post) { // verificação necessária
                $product = wc_get_product($post->ID);
                
                //$titulo = $post->post_title;
                //$descricao = "Curso para Concurso - {$titulo}";
                $preco = is_pacote($product) ? $product->get_bundle_price() : $product->get_price();
                
                $html = "";
                
                #$html .= "<meta property='og:title' content='{$titulo}'>\n";
                #$html .= "<meta property='og:description' content='{$descricao}'>\n";
                $html .= "<meta property='og:url' content='{$url}'>\n";
                #$html .= "<meta property='og:image' content='https://example.org/facebook.jpg'>";      
                $html .= "<meta property='product:brand' content='Exponencial Concursos'>\n";     
                $html .= "<meta property='product:availability' content='in stock'>\n";
                $html .= "<meta property='product:condition' content='new'>\n";
                $html .= "<meta property='product:price:amount' content='{$preco}'>\n";
                $html .= "<meta property='product:price:currency' content='BRL'>\n";
                $html .= "<meta property='product:retailer_item_id' content='{$post->ID}'>\n";
                
                return $html;
            }
            
        }
        
       return "";
    }

}