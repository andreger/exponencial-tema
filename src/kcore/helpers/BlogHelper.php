<?php
/**
 * Helper para organizar rotinas relacionadas aos Blogs
 * 
 * @package	KCore/Helpers
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K4
 */ 

class BlogHelper {

	/**
	 * Retorna a estilo css de um determinado tipo de blog
	 * 
	 * @since K4
	 * 
	 * @param int $tipo_id Id do tipo do blog
	 * 
	 * @return string Estilo do tipo do blog
	 */ 

	static public function get_tipo_estilo($tipo_id)
	{
		switch ($tipo_id) {
			case TIPO_ARTIGO: return "midia-artigo-yellow";
			case TIPO_NOTICIA: return "midia-noticia-blue";
			case TIPO_VIDEO: return "midia-video-red";
			default: return "";
		}
	}

	/**
	 * Retorna o título de um determinado tipo de blog
	 * 
	 * @since K4
	 * 
	 * @param int $tipo_id Id do tipo do blog
	 * 
	 * @return string Título do tipo do blog
	 */ 

	static public function get_tipo_titulo($tipo_id)
	{
		switch ($tipo_id) {
			case TIPO_ARTIGO: return "Artigo";
			case TIPO_NOTICIA: return "Notícia";
			case TIPO_VIDEO: return "Vídeo";
			default: return "";
		}
	}

	/**
	 * Verifica se um blog post possui determinado tipo
	 * 
	 * @since K4
	 * 
	 * @param int $tipo_id Id do tipo do blog
	 * @param int $blog_tipos Lista de ids de tipos do blog post
	 * 
	 * @return bool
	 */ 

	static public function is_blog_tipo($tipo_id, $blog_tipos)
	{
		return in_array($tipo_id, $blog_tipos) ? TRUE : FALSE;
	}
	
	/**
	 * Recupera o tipo do blog através da URL
	 *
	 * @since L1
	 *
	 * @param string $path Path da url do blog
	 *
	 * @throws InvalidArgumentException
	 *
	 * @return int Id do tipo do blog
	 */
	
	static public function get_blog_tipo_from_url_path($path)
	{
	    /* === fluxo normal === */
	    if(starts_with($path, '/blog-posts-artigos')) {
	        return TIPO_ARTIGO;
	    }
	    elseif(starts_with($path, '/blog-artigos')) {
	        return TIPO_ARTIGO;
	    }
	    elseif(starts_with($path, '/blog-posts-videos')) {
	        return TIPO_VIDEO;
	    }
	    elseif(starts_with($path, '/blog-videos')) {
	        return TIPO_VIDEO;
	    }
	    elseif(starts_with($path, '/blog-posts-noticias')) {
	        return TIPO_NOTICIA;
	    }
	    elseif(starts_with($path, '/blog-noticias')) {
	        return TIPO_NOTICIA;
	    }
	    else { 
    	    /* === tratamento de url que não se enquadra em um tipo pré-definido === */
    	    throw new InvalidArgumentException("Url de blog de tipo inválido");
	    }
	}
	
	/**
	 * Retorna a URL de blog posts de um determinado tipo de blog
	 *
	 * @since K4
	 *
	 * @param int $tipo_id Id do tipo do blog
	 *
	 * @return string URL do blog posts do tipo do blog
	 */
	
	static public function get_blog_posts_url($tipo_id)
	{
	    if($tipo_id == TIPO_ARTIGO) {
	        return "/blog-posts-artigos";
	    }
	    elseif($tipo_id == TIPO_VIDEO) {
	        return "/blog-posts-videos";
	    }
	    elseif($tipo_id == TIPO_NOTICIA){
	        return "/blog-posts-noticias";
	    }
	    else {
	        /* === tratamento de url que não se enquadra em um tipo pré-definido === */
	        throw new InvalidArgumentException("Tipo de blog inválido");
	    }
	}
	
}