<?php
/**
 * Helper para organizar rotinas relacionadas ao relatório de cursos
 *
 * @package	KCore/Helpers
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K5
 */

KLoader::helper("RelatorioHelper");

class RelatorioCursosHelper extends RelatorioHelper {
    
    /**
     * Retorna a tabela com os dados do relatório
     *
     * @since K5
     *
     * @param array $lines Linhas do relatório
     * @param bool $is_excel Indica se é para exportação para Excel ou não
     * @param int $max_aulas Máximo de colunas de aulas que serão exibidas no relatório
     *
     * @return string HTML da tabela do relatório
     */ 
    
    public static function get_table($lines, $is_excel = false, $max_aulas = 0)
    {
        $aulas_index = [];
        for($i = 0; $i < $max_aulas; $i++) {
            array_push($aulas_index, $i);
        }
        
        $table = "<table style='width: 99%; overflow-x:scroll; display:block' border='0' cellspacing='0' cellpadding='0' class='table-relatorio table table-bordered'>";
        $table .= "<tr class='font-10 bg-blue text-white'>";
        
        if(!$is_excel) {
            $table .= "<th class='checkbox-td' style='display:none'><input type='checkbox' id='all-chb' checked=checked></th>";
        }
        
        $table .= "<th class='text-nowrap'>Concurso e Fórum</th>";
        $table .= "<th class='text-nowrap'>Áreas</th>";
        $table .= "<th>Professor&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>";
        $table .= "<th>Matéria</th>";
        $table .= "<th class='text-nowrap'>Data que expira</th>";
        $table .= "<th class='text-nowrap'>Data da Prova</th>";
        $table .= "<th class='text-nowrap'>Banca</th>";
        $table .= "<th>Preço&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>";
        $table .= "<th>Qtde de Esquemas</th>";
        $table .= "<th>Qtde de Questões</th>";
        
        foreach ($aulas_index as $index) {
            $table .= "<th style='text-align:center'>$index</th>";
        }
        $table .= "</tr>";
        
        foreach ($lines as $line) {
            
            switch ($line['status']) {
                case 'draft': {
                    $estilo_curso = "style='background-color:#ffb8ad'";
                    break;
                }
                
                case 'pending': {
                    $estilo_curso = "style='background-color:#fff486'";
                    break;
                }
                
                default: {
                    $estilo_curso = "";
                    break;
                }
            }
            
            $table .= "<tr class='$sem_erro'>";
            
            if(!$is_excel) {
                $table .= "<td class='checkbox-td' style='display:none'><input type='checkbox' class='produto-chb' name='alterar_data_ids[]' value='{$line['id']}' checked=checked></td>";
            }
            
            $table .= "<td {$estilo_curso}>";
            
            if($line['forum'] == null) {
                $table .= "{$line['concurso']}";
            }
            else {
                $table .= "<a href='{$line['forum']}'>{$line['concurso']}</a>";
            }
            $table .= "</td>";
            $table .= "<td>" . $line['areas'] . "</td>";
            $table .= "<td>" . $line['professores'] . "</td>";
            $table .= "<td><a target='_blank' href='{$line['edit_link']}'>" . $line['curso'] . "</a></td>";
            $table .= "<td>" . $line['data_expiracao'] . "</td>";
            $table .= "<td>" . ($line['data_prova'] ? converter_para_ddmmyyyy($line['data_prova']) : '') . "</td>";
            $table .= "<td>" . $line['banca'] . "</td>";
            $table .= "<td style='text-align:center'>" . moeda((float)$line['preco'], true) . "</td>";
            $table .= "<td style='text-align:center'>" . numero_inteiro($line['qtde_esquemas']) . "</td>";
            $table .= "<td style='text-align:center'>" . numero_inteiro($line['qtde_questoes']) . "</td>";
            
            $soma_preco += $line['preco'];
            $soma_qtde_esquemas += $line['qtde_esquemas'];
            $soma_qtde_questoes += $line['qtde_questoes'];
            
            foreach ($aulas_index as $index) {
                
                switch ($line['aulas'][$index]['erro']) {
                    
                    case ERRO_UPLOAD_ATRASADO: {
                        $estilo_erro = "background-color:#fff486;";
                        break;
                    }
                    
                    case ERRO_UPLOAD_NAO_REALIZADO: {
                        $estilo_erro = "background-color:#ffb8ad;";
                        break;
                    }
                    
                    default: {
                        if($line['aulas'][$index]['uploaded']) {
                            $estilo_erro = "background-color:#90c59e;";
                        }
                        else {
                            $estilo_erro = "";
                        }
                        break;
                    }
                }
                
                $estilo_uploaded = !$line['aulas'][$index]['uploaded'] ? "font-weight: bold;" : "";
                
                $table .= "<td style='{$estilo_erro} {$estilo_uploaded}'> {$line['aulas'][$index]['data']}</td>";
            }
            $table .= "</tr>";
            
        }
        
        $table .= "<tr>";
        
        if(!$is_excel) {
            $table .= "<th class='checkbox-td' style='display:none'></th>";
        }
        
        $table .= "<th></th>";
        $table .= "<th></th>";
        $table .= "<th></th>";
        $table .= "<th></th>";
        $table .= "<th></th>";
        $table .= "<th></th>";
        $table .= "<th></th>";
        $table .= "<th class='text-nowrap' style='text-align:center'>" . moeda($soma_preco) . "</th>";
        $table .= "<th class='text-nowrap' style='text-align:center'>" . numero_inteiro($soma_qtde_esquemas) . "</th>";
        $table .= "<th class='text-nowrap' style='text-align:center'>" . numero_inteiro($soma_qtde_questoes) . "</th>";
        
        foreach ($aulas_index as $index) {
            $table .= "<th></th>";
        }
        $table .= "</tr>";
        
        $table .=  "</table>";
        
        return $table;
    }
    
    /**
     * Retorna as opções de combo de tipo de aula
     *
     * @since L1
     *
     * @return array Lista de combo options
     */ 
    
    public function get_tipo_aula_combo_options()
    {
        return [
            AULA_UPLOAD_TIPO_PDF => "PDF",
            AULA_UPLOAD_TIPO_VIDEO=> "Vídeo",
            AULA_UPLOAD_TIPO_CADERNO => "Cadernos"
        ];
    }
    
}