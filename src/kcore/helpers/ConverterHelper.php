<?php
/**
 * Helper para organizar rotinas relacionadas às conversões
 * 
 * @package	KCore/Helpers
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K4
 */ 

class ConverterHelper {

	/**
	 * Converte objeto (ou lista de objetos) em array (ou lista de arrays)
	 * 
	 * @since K4
	 * 
	 * @param object|array $input Objeto ou lista de objetos de entrada.
	 * 
	 * @return array Lista ou lista de arrays de saída.
	 */ 

	public static function object_to_array($input)
	{
		
		if(is_array($input)) {

			$output = [];
			foreach ($input as $item) {
				$output[] = (array)$item;
			}

			return $output;
		}
		else {
			return (array)$input;
		}

	}

	

}