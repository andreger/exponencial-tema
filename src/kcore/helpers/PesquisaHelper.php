<?php
/**
 * Helper para organizar rotinas relacionadas à barra de pesquisa e páginas de material grátis
 *
 * @package	KCore/Helpers
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K5
 */

class PesquisaHelper {
    
    /**
     * Modifica a string de busca binária para ficar mais abrangente
     * 
     * @since L1
     * 
     * @param string String de busca
     * 
     * @return string string de busca modificada
     */
    public static function ampliar_texto_busca($busca)
    {
        if(is_array($busca))
        {
            $busca = implode("|", $busca);
        }

        $busca = str_replace(" ", "|", $busca);
        $busca = str_replace('"', "", $busca);
        $busca = str_replace("'", "", $busca);
        $busca = trim(stripslashes($busca));

        return $busca;
    }

    /**
     * Montar busca binária de texto
     * 
     * @since L1
     * 
     * @param string|array $buscas Texto(s) da busca
     * @param string $campo Campo em que a busca será realizada
     * 
     * @return string cláusulas WHERE para a busca ser executada
     */
    public static function busca_binaria($busca, $campo)
    {
        KLoader::helper("StringHelper");

        // declaração de caracteres especiais
        $kespaco = "[KESPACO]";
        $kpipe = "[KPIPE]";
        $kand = "[KAND]";

        if(is_array($busca))
        {
            $busca = implode(" ", $busca);
        }

        $busca = trim(stripslashes($busca));

        // buscas os termos exatos
        $exatas = StringHelper::get_substrings($busca, '"', '"');

        // substitui caracteres especiais nos termos exatos
        foreach($exatas as $item) {
            $item_modificado = str_replace(" ", $kespaco, $item);
            $item_modificado = str_replace("|", $kpipe, $item_modificado);
            $busca = str_replace($item, $item_modificado, $busca);
        }

        $or_a_match = [];
        $or_a_like = [];
        $busca_or = explode("|", $busca);
        foreach ($busca_or as $item_or) {

            $and_a_match = [];
            $and_a_like = [];

            $busca_and = explode(" ", $item_or);

            foreach ($busca_and as $item_and) {
                $item_and = trim($item_and);

                if($item_and) {
                    $and_a_match[] = "$item_and";
                    $and_a_like[] = "{$campo} LIKE '%{$item_and}%'";
                }
            }

            $or_a_match[] = implode($kand, $and_a_match);
            $or_a_like[] = implode($kand, $and_a_like);
        }

        // se não houver OR adiciona um + na frente do primeiro termo
        $busca_match = count($or_a_match) == 1 ? "+" . $or_a_match[0] : implode(" ", $or_a_match);
        $busca_like = implode(" OR ", $or_a_like);

        // retorna com os caracteres especiais
        $busca_match = str_replace($kespaco, " ", $busca_match);
        $busca_match = str_replace($kpipe, "|", $busca_match);
        $busca_match = str_replace($kand, " +", $busca_match);

        $busca_like = str_replace('"', "", $busca_like);

        $busca_like = str_replace($kespaco, " ", $busca_like);
        $busca_like = str_replace($kpipe, "|", $busca_like);
        $busca_like = str_replace($kand, " AND ", $busca_like);

        $query_where = " AND MATCH({$campo}) AGAINST ('{$busca_match}') ";

        $query_where .= " AND {$busca_like} ";

        return $query_where;
    }

    /**
     * Extrai as informações de H1 do html
     *
     * @since K5
     *
     * @param int $id Id do post
     *
     * @return array Dados sobre o H1
     */
    
    public static function get_form_url()
    {      
        KLoader::helper("UrlHelper");
        
        $url = "/pesquisa";
        
        if(UrlHelper::is_url_audiobooks_gratis()) {
            $url = "/audiobooks-gratis";  
        }
        elseif(UrlHelper::is_url_cursos_gratis()) {
            $url = "/cursos-gratis";
        }
        elseif(UrlHelper::is_url_simulados_gratis()) {
            $url = "/simulados-gratis";
        }
        elseif(UrlHelper::is_url_mapas_mentais_gratis()) {
            $url = "/mapas-mentais-gratis";
        }
        elseif(UrlHelper::is_url_questoes_comentadas_gratis()) {
            $url = "/questoes-comentadas-gratis";
        }
        elseif(UrlHelper::is_url_todos_cursos()) {
            $url = "/todos-cursos";
        }
        
        return $url;
    }
    
    /**
     * Trata o texto de input da busca
     *
     * @since K5
     *
     * @param string $texto Texto de input
     *
     * @return string Texto tratado
     */
    
    public static function tratar_input($texto)
    {
        $texto = str_replace("–", "-", $texto);

        return $texto;
    }
}
    