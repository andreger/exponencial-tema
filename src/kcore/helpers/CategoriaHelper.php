<?php
/**
 * Helper para organizar rotinas relacionados às categorias
 * 
 * @package	KCore/Helpers
 *
 * @author 	João Paulo Ferreira <jpaulofms@gmail.comr>
 *
 * @since	K5
 */ 

class CategoriaHelper {

	/**
	 * Compara um campo da categoria
	 * 
	 * @since K5
	 * 
	 * @param Object $categoria_a Categoria a ser comparada
     * @param Object $categoria_b A outra categoria a ser comparada
	 * 
	 * @return int resultado da comparação.
	 */

	public static function comparar_por_nome($categoria_a, $categoria_b)
	{   
        if(!is_object($categoria_a) || !is_object($categoria_b)){
            return 0;
        }
        $nome_a = strtr($categoria_a->name, "áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ", "aaaaeeiooouucAAAAEEIOOOUUC");
        $nome_b = strtr($categoria_b->name, "áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ", "aaaaeeiooouucAAAAEEIOOOUUC");
        return strcasecmp($nome_a, $nome_b);
    }

    /**
	 * Reordena as categorias
	 * 
	 * @since K5
	 * 
	 * @param array $categorias Categorias a serem reordenadas
	 * 
	 * @return array As categorias na nova ordenação
	 */ 

	public static function reordenar_categorias($categorias)
	{
        usort($categorias, function($a, $b){
            return CategoriaHelper::comparar_por_nome($a, $b);
        });
        return $categorias;
	}
	
	public static function categoria_constante_id($slug)
	{

		if(!$slug) return null;

		if(defined('CATEGORIA_' . $slug )){
			return constant('CATEGORIA_ID_' . $slug);
		}

		KLoader::model("CategoriaModel");
		$categoria = CategoriaModel::get_by_slug($slug);

		if(!$categoria) return null;

		define('CATEGORIA_ID_' . $slug, $categoria->term_id);

		return $categoria->term_id;
	}

}