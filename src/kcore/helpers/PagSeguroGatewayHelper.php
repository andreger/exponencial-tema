<?php
/**
 * Helper para organizar rotinas relacionadas ao PagSeguro
 *
 * @package	KCore/Helpers
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	L1
 */

class PagSeguroGatewayHelper {
    
    /**
     * Retorna as taxas do PagSeguro
     *
     * @since L1
     *
     * @param WC_Order $order Pedido no WP
     * @param bool $force_pagseguro Flag para indicar se é para forçar a atualização de taxas
     *
     * @return float
     */
    
    public static function get_taxas($order, $force_pagseguro)
    {
        $taxas_pagseguro = get_post_meta($order->get_id(), 'PagSeguro Total Taxes', TRUE);
        
        if(empty($taxas_pagseguro) || $force_pagseguro) {
            
            // recupera o id da transação no pagseguro
            $pagseguro_id = get_pagseguro_transaction_id($order->get_id());
            
            $base = "https://ws.pagseguro.uol.com.br/v3/transactions/". $pagseguro_id ;
            $data = "email=leonardo.coelho@exponencialconcursos.com.br&token=67E47689AEB74EF685A9B47B2FCB1E4A";
            
            $url = $base . '?' . $data;
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_URL, $url );
            $return = curl_exec($ch);
            curl_close($ch);
            
            $xml = str_get_html($return);
            
            $installmentFeeAmout = $xml && $xml->find('installmentFeeAmount', 0) ? $xml->find('installmentFeeAmount', 0)->plaintext : 0;
            $intermediationRateAmount = $xml && $xml->find('intermediationRateAmount', 0) ? $xml->find('intermediationRateAmount', 0)->plaintext : 0;
            $intermediationFeeAmount = 	$xml && $xml->find('intermediationFeeAmount', 0) ? $xml->find('intermediationFeeAmount', 0)->plaintext : 0;
            $taxas_pagseguro = 	$installmentFeeAmout + $intermediationFeeAmount + $intermediationRateAmount;
        }
        
        return $taxas_pagseguro;
    }
    
}