<?php
use \setasign\Fpdi\Fpdi;

require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-content/pdftools/fpdf/fpdf.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-content/pdftools/fpdi/src/autoload.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-content/pdftools/FPDI_PDF-Parser/src/autoload.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-content/pdftools/fpdi-protection/src/autoload.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-content/pdftools/rfpdi/src/rfpdi.php';

/**
 * Helper para organizar rotinas relacionados aos PDFs
 * 
 * @package	KCore/Helpers
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	L1
 */ 

class PDFHelper {
    
    /**
     * Gera a capa de um produto
     *
     * @since L1
     *
     * @param int $produto_id Id do produto
     *
     * @return string
     */
    
    public static function gerar_capa($produto_id)
    {
        try {
            // recupera concursos e disciplinas
            KLoader::helper("ProdutoHelper");
            

            $capa_disciplina = get_post_meta($produto_id, PRODUTO_CAPA_TEXTO_DISCIPLINA, true);
            
            if(!$capa_disciplina) {
                $capa_disciplina = strtoupper(ProdutoHelper::get_produto_materias_str($produto_id));
            }

            $capa_disciplina =  stripslashes($capa_disciplina);
            $capa_disciplina = iconv('UTF-8', 'windows-1252', $capa_disciplina);
            
            
            $capa_concurso = get_post_meta($produto_id, PRODUTO_CAPA_TEXTO_CONCURSO, true);
            
            if(!$capa_concurso) {
                $capa_concurso = strtoupper(ProdutoHelper::get_produto_concursos_str($produto_id));
            }

            $capa_concurso =  stripslashes($capa_concurso);
            $capa_concurso = iconv('UTF-8', 'windows-1252', $capa_concurso);
            
            
            $nome = $_SERVER["DOCUMENT_ROOT"] . PDF_CURSO_CAPA;
            $capa_nome = $_SERVER["DOCUMENT_ROOT"] . "/wp-content/temp/capa-" . time() . ".pdf"; 
            
            $file = new FPDI();
            $file->setSourceFile($nome);
            
            // configurações de fonte
            $pdf = new RFPDI();
            $pdf->SetTextColor(255, 255, 255);
            $pdf->SetFont('Helvetica');
            
            $pdf->setSourceFile($nome);
                
            $tplIdxFile = $file->importPage(1);
                
            $size = $file->getTemplateSize($tplIdxFile);
                
            $pdf->AddPage($size['orientation'],  array($size['width'], $size['height']));

            if($imagem = self::get_imagem_capa($produto_id)) {
                $pdf->Image($imagem, 0, 0, $size['width'], $size['height']);
            }
            
            $pdf->SetFontSize('30');
            $pdf->setX(0);
            $pdf->setY(150);
            $pdf->Cell(0, 10, $capa_disciplina, 0, 0, 'C');
            
            $pdf->SetFontSize('26');
            $pdf->setX(0);
            $pdf->setY(180);
            $pdf->Cell(0, 10, $capa_concurso, 0, 0, 'C'); 
            
            $pdf->Output($capa_nome,'F', true);
            
            return $capa_nome;
            
        } catch (Exception $e) {
            
            // print_r($e);
            
        }
        
    }
    
    /**
     * Retorna string com URL da capa da imagem na ordem de prioridade: 1) Concurso 2) Matéria
     *
     * @since L1
     *
     * @param int $produto_id Id do produto
     *
     * @return string
     */
    
    private static function get_imagem_capa_url($produto_id)
    {
        KLoader::model("CategoriaModel");
        KLoader::model("ProdutoModel");
        
        // Tenta achar a capa através do concurso
        if($items = ProdutoModel::listar_concursos($produto_id)) {
            $categoria_id = $items[0]->cat_id;
            
            $url = CategoriaModel::get_imagem_url($categoria_id);
            
            if($url) {
                return $url;
            }
        }
        
        // Tenta achar a capa através da matéria
        if($items = ProdutoModel::listar_materias($produto_id)) {
            $categoria_id = $items[0]->cat_id;
            
            $url = CategoriaModel::get_imagem_url($categoria_id);
            
            if($url) {
                return $url;
            }  
        }
        
        
        return null;
    }
    
    /**
     * Retorna string com caminho da capa da imagem na ordem de prioridade: 1) Concurso 2) Matéria
     *
     * @since L1
     *
     * @param int $produto_id Id do produto
     *
     * @return string
     */
    
    private static function get_imagem_capa($produto_id)
    {
        KLoader::helper("UrlHelper");
        
        $url = self::get_imagem_capa_url($produto_id);
        
        if($url) {
            return $_SERVER['DOCUMENT_ROOT'] . UrlHelper::remover_dominio($url);
        }
        
        return null;
    }
    
    /**
     * Retorna string com nome do status do produto
     *
     * @since L1
     *
     * @param int $produto_id Id do produto
     * @param int $index Index da aula
     *
     * @return string
     */
    
    public static function get_nome_pdf_merge($produto_id, $index, $tipo_aula = TIPO_AULA_PDF)
    {
        KLoader::helper("StringHelper");
        $descricao_a = get_post_meta($produto_id, "aulas_descricao", true);
        
        $descricao = $descricao_a[$index];

        if($tipo_aula == TIPO_AULA_MAPA)
        {
            $descricao = "Mapa_Mental-" . $descricao;
        }
        elseif($tipo_aula == TIPO_AULA_RESUMO)
        {
            $descricao = "Resumo-" . $descricao;
        }

        $descricao = StringHelper::slugify($descricao, AULA_TAMANHO_MAXIMO_MERGE_STR);
        
        return $index . "-" . $descricao . ".pdf";
        
    }

	/**
	 * Realiza o merge de arquivos PDFs
	 * 
	 * @since L1
	 * 
	 * @param array $arquivos Lista de arquivos PDFs
	 * 
	 * @return RFPDI
	 */ 

	public static function merge($arquivos)
	{
        $pdf = new RFPDI();
        
        $num_arquivos = 0;

	    foreach($arquivos as $source) {
	        if(!$source) {
	            continue;
	        }
	        
	        if(get_extensao($source) != "pdf") {
	            continue;
	        }
	        
	        if(!file_exists($source)) {
	            continue;
	        }

	        $file = new FPDI();
	        $file->setSourceFile($source);
	        $pagesCount = $pdf->setSourceFile($source);
	        
	        for ($i = 1; $i <= $pagesCount; $i++) {
	            
	            $tplIdxFile = $file->importPage($i);
	            
	            $size = $file->getTemplateSize($tplIdxFile);
	            
	            $pdf->AddPage($size['orientation'],  array($size['width'], $size['height']));
	            
	            $tplIdx = $pdf->importPage($i);
	            
	            $pdf->useTemplate($tplIdx);
            }
            
            $num_arquivos++;
	        
	    }
        
        if($num_arquivos > 0)
        {
            return $pdf;
        }

        return null;
	}
		
	
	

	
}