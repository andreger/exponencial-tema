<?php
/**
 * Helper para organizar rotinas relacionadas ao relatório de produtos premium
 *
 * @package	KCore/Helpers
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	L1
 */
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;

class RelatorioProdutosPremiumHelper {
    
    /**
     * Exporta um relatório de produtos premium para XLSX e grava em arquivo
     *
     * @since L1
     *
     * @param array $linhas Linhas a serem exportadas
     *
     * @return string Endereço relativo do arquivo (ex: /wp-content/temp/relatorio-produtos-premium-99999999.xlsx
     */ 
    
    public static function exportar_xls($premiums)
    {
        KLoader::helper("NumeroHelper");
        
        $arquivo = '/wp-content/temp/relatorio-produtos-premium-' . time() . '.xlsx';

        $writer = WriterFactory::create(Type::XLSX);
        $writer->openToBrowser($_SERVER['DOCUMENT_ROOT'] . $arquivo);
        
        $row = ["Produto", "Valor", "%", "Áreas", "Disciplinas"];
        $writer->addRow($row);
        
        foreach($premiums as $premium) {
            $row = [$premium->post_title, $premium->pro_preco, "100,00", "", ""];
            $writer->addRow($row);
            
            if($items = $premium->items) {
                foreach ($items as $item) {
                    $row = [
                        $item["nome"], 
                        NumeroHelper::numero_br($item["preco"]), 
                        NumeroHelper::numero_br($item["percentual"]*100),
                        $item["areas"],
                        $item["materias"]
                    ];
                    
                    $writer->addRow($row);
                }
            }
        }
        $writer->close();
        
        return $arquivo;
    }
}