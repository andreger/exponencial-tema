<?php
/**
 * Helper para organizar rotinas relacionadas ao relatório de vendas de produtos premium
 *
 * @package	KCore/Helpers
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K5
 */
use Box\Spout\Writer\WriterFactory;

use Box\Spout\Common\Type;


class RelatorioVendasPremiumHelper {
    
    /* Retorna a tabela com os dados do relatório
    *
    * @since K5
    * 
    * @param array $lines Linhas do relatório
    *
    * @return string HTML da tabela do relatório
    */
    
    public static function get_table($linhas)
    {
        $table = "<table border='0' cellspacing='0' cellpadding='0' class='table table-striped table-bordered'>".
            "<tr class='font-10 bg-blue text-white'>".
            "<td>Professor</td>";
        

//         $table .= "<td>Pedido</td>";
//         $table .= "<td>Data</td>";
//         $table .= "<td>Valor Pago</td>";
//         $table .= "<td>ID do Produto</td>";
//         $table .= "<td>Nome do Produto</td>";
//         $table .= "<td>% do Item no Premium</td>";
        $table .= "<td>Total</td>";
        $table .= "</tr>";
        
        $table .= self::get_table_body($linhas);
        $table .= self::get_table_foot($linhas);
        
        $table .=  "</table>";
        
        return $table;
    }
    
    /* Retorna a tabela com os dados do relatório
     *
     * @since K5
     *
     * @param array $lines Linhas do relatório
     *
     * @return string HTML da tabela do relatório
     */
    
    public static function get_table_detalhe($linhas)
    {
        $table = "<table border='0' cellspacing='0' cellpadding='0' class='table table-striped table-bordered'>".
            "<tr class='font-10 bg-blue text-white'>";
            
            
        $table .= "<td>Pedido</td>";
        $table .= "<td>Data</td>";
        $table .= "<td>Valor Pago</td>";
        $table .= "<td>ID do Produto</td>";
        $table .= "<td>Nome do Produto</td>";
        $table .= "<td>% do Item no Premium</td>";
        $table .= "<td>Total</td>";
        $table .= "</tr>";
        
        $table .= self::get_table_detalhe_body($linhas);
        //         $table .= self::get_table_foot($filtros);
        
        $table .=  "</table>";
        
        return $table;
    }
    
    /**
     * Retorna o body da tabela com os dados do relatório
     *
     * @since L1
     *
     * @param array $filtros Filtros do relatório
     *
     * @return string HTML do body da tabelas
     */ 
    
    private static function get_table_body($linhas_professores)
    {
        $table = "";

        foreach ($linhas_professores as $nome_professor => $linha_professor) {
            $inicio = converter_para_yyyymmdd($_GET['inicio']);
            $fim = converter_para_yyyymmdd($_GET['fim']);
            
            $url = "/relatorio-vendas-premium-detalhe?inicio={$inicio}&fim={$fim}&professor_id={$linha_professor['id']}";
            $total = format_moeda($linha_professor['total']);
            
            $table .= "<tr>";
            $table .= "<td><a href='{$url}'>{$nome_professor}</a></td>";
//             $table .= "<td></td>";
//             $table .= "<td></td>";
//             $table .= "<td></td>";
//             $table .= "<td></td>";
//             $table .= "<td></td>";
//             $table .= "<td></td>";
            $table .= "<td>{$total}</td>";
            $table .= "</tr>";
            
//             foreach ($linha_professor['info'] as $info) {
//                 $data = converter_para_ddmmyyyy_HHiiss($info['pedido_data']);
//                 $pedido_valor = format_moeda($info['pedido_valor']);
//                 $percentual = number_format($info['item_percentual'], 5, ",", ".");
//                 $item_valor = format_moeda($info['item_valor']);
                
//                 $table .= "<tr>";
//                 $table .= "<td></td>";
//                 $table .= "<td>{$info['pedido_id']}</td>";
//                 $table .= "<td>{$data}</td>";
//                 $table .= "<td>{$pedido_valor}</td>";
//                 $table .= "<td>{$info['item_id']}</td>";
//                 $table .= "<td>{$info['item_nome']}</td>";
//                 $table .= "<td>{$percentual}</td>";
//                 $table .= "<td>{$item_valor}</td>";
//                 $table .= "</tr>";
//             }
            
        }
        
        return $table;
    }
    
    /**
     * Retorna o body da tabela com os dados do relatório
     *
     * @since L1
     *
     * @param array $filtros Filtros do relatório
     *
     * @return string HTML do body da tabelas
     */
    
    private static function get_table_detalhe_body($linha_professor)
    {
        $total = format_moeda($linha_professor['total']);
        
        $table = "<tr>";
        $table .= "<td></td>";
        $table .= "<td></td>";
        $table .= "<td></td>";
        $table .= "<td></td>";
        $table .= "<td></td>";
        $table .= "<td></td>";
        $table .= "<td><b>{$total}</b></td>";
        $table .= "</tr>";
        
        foreach ($linha_professor['info'] as $info) {
            $data = converter_para_ddmmyyyy_HHiiss($info['pedido_data']);
            $pedido_valor = format_moeda($info['pedido_valor']);
            $percentual = number_format($info['item_percentual'], 5, ",", ".");
            $item_valor = format_moeda($info['item_valor']);

            $table .= "<tr>";
            $table .= "<td>{$info['pedido_id']}</td>";
            $table .= "<td>{$data}</td>";
            $table .= "<td>{$pedido_valor}</td>";
            $table .= "<td>{$info['item_id']}</td>";
            $table .= "<td>{$info['item_nome']}</td>";
            $table .= "<td>{$percentual}</td>";
            $table .= "<td>{$item_valor}</td>";
            $table .= "</tr>";
        }
        
        return $table;
    }
    
    /**
     * Retorna o foot da tabela com os dados do relatório
     *
     * @since K5
     *
     * @uses RelatorioVendasModel::get_dados() para recuperar os dados do relatório
     *
     * @param array $filtros Filtros do relatório
     *
     * @return string HTML do foot da tabelas
     */ 
    
    private static function get_table_foot($linhas_professores)
    {
        $total_geral = 0;

        foreach ($linhas_professores as $nome_professor => $linha_professor) {
            $total_geral += $linha_professor['total'];
        }

        $total = format_moeda($total_geral);

        $table = "<tr>";
        $table .= "<td><strong>Total</a></td>";
        $table .= "<td><strong>{$total}</strong></td>";
        $table .= "</tr>";

        return $table;
    }
    
    /**
     * Exporta um relatório de vendas para XLSX e grava em arquivo
     *
     * @since K5
     *
     * @uses RelatorioVendasModel::get_dados() para recuperar os dados do relatório
     *
     * @param array $linhas Linhas a serem exportadas
     *
     * @return string Endereço relativo do arquivo (ex: /wp-content/temp/relatorio-de-vendas-99999999.xlsx
     */ 
    
    public static function exportar_xls($linhas)
    {
        $arquivo = '/wp-content/temp/relatorio-de-vendas-premium-' . time() . '.xlsx';

        $writer = WriterFactory::create(Type::XLSX);
        $writer->openToBrowser($_SERVER['DOCUMENT_ROOT'] . $arquivo);
        
        $row = [
            "Professor",
//             "Pedido", 
//             "Data", 
//             "Valor Pago", 
//             "ID do Produto", 
//             "Nome do Produto", 
//             "% do Item no Premium", 
            "Total"
        ];
        
        $writer->addRow($row);
        
        foreach ($linhas as $nome_professor => $linha_professor) {
            $total = format_moeda($linha_professor['total']);
            
            $row = [
                $nome_professor,
//                 "",
//                 "",
//                 "",
//                 "",
//                 "",
//                 "",
                $total
            ];
            
            $writer->addRow($row);
            
//             foreach ($linha_professor['info'] as $info) {
//                 $data = converter_para_ddmmyyyy_HHiiss($info['pedido_data']);
//                 $pedido_valor = format_moeda($info['pedido_valor']);
//                 $percentual = number_format($info['item_percentual'], 5, ",", ".");
//                 $item_valor = format_moeda($info['item_valor']);
                
//                 $row = [
//                     "",
//                     $info['pedido_id'],
//                     $data,
//                     $pedido_valor,
//                     $info['item_id'],
//                     $info['item_nome'],
//                     $percentual,
//                     $item_valor
//                 ];
                
//                 $writer->addRow($row);
//             }
            
        }

        $writer->close();
        
//         return $arquivo;
    }
}