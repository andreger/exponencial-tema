<?php
/**
 * Helper para organizar rotinas relacionadas aos filtros
 * 
 * @package	KCore/Helpers
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K5
 */ 

class FiltroHelper {

	/**
	 * Verifica se os filtros possuem valor aplicado
	 * 
	 * @since K5
	 * 
	 * @param array $filtros Filtros 
	 * 
	 * @return bool 
	 */

	public static function tem_filtro_aplicado($filtros)
	{
		foreach ($filtros as $filtro) {
			if(!is_null($filtro) && $filtro != "") {
				return true;
			}
		}

		return false;
	}

}