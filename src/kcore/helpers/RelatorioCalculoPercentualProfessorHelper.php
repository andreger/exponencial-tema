<?php
/**
 * Helper para organizar rotinas relacionadas ao relatório de produtos premium
 *
 * @package	KCore/Helpers
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	L1
 */
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;

class RelatorioCalculoPercentualProfessorHelper {
    
    /**
     * Exporta um relatório de produtos premium para XLSX e grava em arquivo
     *
     * @since L1
     *
     * @param array $linhas Linhas a serem exportadas
     *
     * @return string Endereço relativo do arquivo (ex: /wp-content/temp/relatorio-produtos-premium-99999999.xlsx
     */ 
    
    public static function exportar_xls($premiums)
    {
        $arquivo = '/wp-content/temp/relatorio-calculo-percentual-professor-' . time() . '.xlsx';

        $writer = WriterFactory::create(Type::XLSX);
        $writer->openToBrowser($_SERVER['DOCUMENT_ROOT'] . $arquivo);
        
        $row = ["Período", "Produto", "Id do Produto", "Valor"];
        $writer->addRow($row);
        
        foreach($premiums as $premium) {
            $row = [
                periodo_mes_ano($premium['ano_mes']), 
                $premium['nome'],
                $premium['id'],
                moeda($premium['valor'], "")
            ];

            $writer->addRow($row);
        }
        $writer->close();
        exit;
    }
    
    /**
     * Exporta um relatório de produtos premium para XLSX e grava em arquivo
     *
     * @since L1
     *
     * @param array $linhas Linhas a serem exportadas
     *
     * @return string Endereço relativo do arquivo (ex: /wp-content/temp/relatorio-produtos-premium-99999999.xlsx
     */
    
    public static function exportar_detalhe_xls($premium)
    {
        $arquivo = '/wp-content/temp/relatorio-calculo-percentual-professor-detalhe-' . time() . '.xlsx';
        
        $writer = WriterFactory::create(Type::XLSX);
        $writer->openToBrowser($_SERVER['DOCUMENT_ROOT'] . $arquivo);
        
        $row = ["Data", "Produto", "Id do Produto", "Status", "Expira em", "Áreas", "Concurso", "Professor", "Preço cadastrado", 
            "% Professor", "Valor de produto a venda", "% Cota Professor"];
        $writer->addRow($row);
        

        $row = [
            $premium['info']['data'],
            $premium['info']['nome'],
            $premium['info']['id'],
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            moeda($premium['info']['total'], ""),
            "100%"
        ];
        
        $writer->addRow($row);
        
        foreach($premium['itens'] as $item) {
            $row = [
                $item['data'],
                $item['produto'],
                $item['produto_id'],
                $item['status'],
                $item['disponivel_ate'],
                $item['areas'],
                $item['concursos'],
                $item['professor'],
                $item['preco'],
                $item['percentual'],
                $item['valor'],
                $item['cota']
            ];
            
            $writer->addRow($row);
        }

        $writer->close();
        exit;
    }
}