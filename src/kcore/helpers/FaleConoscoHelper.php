<?php
/**
 * Helper para organizar rotinas relacionadas ao Fale Conosco
 *
 * @package	KCore/Helpers
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K5
 */

class FaleConoscoHelper {
    
    /**
     * Enviar os e-mails do Fale Conosco
     */
    
    public static function enviar_email()
    {
        $images_tags = "";
        $embed_images = [];
        $assuntos_a = [
            ASSUNTO_PROBLEMA_ACESSO => "Problema no acesso",
            ASSUNTO_NAO_CONSIGO_FINALIZAR_COMPRA => "Não consigo finalizar minha compra",
            ASSUNTO_FAZER_ELOGIO => "Fazer um elogio, feedback ou reclamação",
            ASSUNTO_ESQUECI_SENHA => "Esqueci minha senha",
            ASSUNTO_CANCELAMENTO => "Cancelamento",
            ASSUNTO_DESCONTOS => "Descontos",
            ASSUNTO_DUVIDA_ALUNO => "Outras dúvidas: Já sou aluno do Exponencial",
            ASSUNTO_DUVIDA_NAO_ALUNO => "Outras dúvidas: Não sou aluno do Exponencial",
            ASSUNTO_SQ_SS => "Sistema de Questões e Simulados",
            ASSUNTO_COACHING => "Coaching",
            ASSUNTO_PDF_VIDEO => "Cursos Online"
        ];
        
        $_POST = $_SESSION['faleconosco_post'];
        $embed_images = $_SESSION['faleconosco_imagens'];

        if($embed_images) {
            foreach ($embed_images as $embed_image) {
                $images_tags .= "<br><br><img src='cid:{$embed_image["cid"]}'>";
            }
        }
        
        $titulo_a = ["Fale Conosco"];
        $message_assunto = "";
        if(trim($_POST['sessao_assunto'])) {
            $assunto_str = $assuntos_a[$_POST['sessao_assunto']];
            array_push($titulo_a, $assunto_str);
            
            $message_assunto = "Assunto: $assunto_str <br><br>";
        }
        
        if(isset($_POST['sessao_produto']) && $_POST['sessao_produto']) {
            $produto_a = [];
            
            foreach ($_POST['sessao_produto'] as $item) {
                if(trim($item)) {
                    $produto_str = $assuntos_a[$item];
                    array_push($titulo_a, $produto_str);
                    array_push($produto_a, $produto_str);
                }
            }
            
            if($produto_a) {
                $message_produto .= "Produtos: " . implode(", ", $produto_a) . "<br><br>";
            }
        }
        
        $titulo = implode(" - ", $titulo_a);
        
        $message = "Contato realizado pelo formulário Fale Conosco:<br><br><br>";
        $message .= "Nome: {$_POST['nome']}<br><br>";
        $message .= "Email: {$_POST['email']}<br><br>";
        
        if($_POST['whatsapp']) {
            $message .= "WhatsApp: {$_POST['whatsapp']}<br><br>";
        }
        
        if($_POST['numero_do_pedido']) {
            $message .= "Número do Pedido: {$_POST['numero_do_pedido']}<br><br>";
        }
        
        $message .= $message_assunto;
        $message .= $message_produto;
        $message .= "Mensagem: {$_POST['content']}";
        $message .= $images_tags;

        /**
         * Concentrar envios de e-mails do Fale Conosco para contato@expo conforme solicitado em reunião de 26/05/22
         */
        /*
        $enderecos = [];
        
        if(isset($_POST['sessao_produto']) && in_array(ASSUNTO_SQ_SS, $_POST['sessao_produto'])) {
            array_push($enderecos, 'questoes@exponencialconcursos.com.br');
        }
        if(isset($_POST['sessao_produto']) && in_array(ASSUNTO_COACHING, $_POST['sessao_produto'])) {
            array_push($enderecos, 'coaching@exponencialconcursos.com.br');
        }
        if(isset($_POST['sessao_produto']) && in_array(ASSUNTO_PDF_VIDEO, $_POST['sessao_produto'])) {
            array_push($enderecos, 'contato@exponencialconcursos.com.br');
        }

        if(!$enderecos) {
            array_push($enderecos, 'contato@exponencialconcursos.com.br');
        }
        
        foreach ($enderecos as $endereco) {
            enviar_email($endereco, $titulo, $message, null, EMAIL_LEONARDO, $embed_images);
        }
        */

        enviar_email('contato@exponencialconcursos.com.br', $titulo, $message, null, EMAIL_LEONARDO, $embed_images);

        $message_aluno = "Olá, segue a mensagem que você nos enviou. Iremos processá-la e retornaremos em breve.<br><br><br>";
        $message_aluno .= "Nome: {$_POST['nome']}<br><br>";
        $message_aluno .= "Email: {$_POST['email']}<br><br>";   
        
        if($_POST['whatsapp']) {
            $message_aluno .= "WhatsApp: {$_POST['whatsapp']}<br><br>";
        }
        
        if($_POST['numero_do_pedido']) {
            $message_aluno .= "Número do Pedido: {$_POST['numero_do_pedido']}<br><br>";
        }
        
        $message_aluno .= $message_assunto;
        $message_aluno .= $message_produto;
        $message_aluno .= "Mensagem: {$_POST['content']}<br><br>";
        $message_aluno .= $images_tags;

        enviar_email($_POST['email'], 'Fale Conosco Exponencial: Confirmação de mensagem enviada', $message_aluno, null, EMAIL_LEONARDO, $embed_images);

        unset($_POST['nome']);
        unset($_POST['email']);
        unset($_POST['confirm_email']);
        unset($_POST['whatsapp']);
        unset($_POST['content']);
        unset($_POST['sessao_arquivo']);
        unset($_POST['sessao_produto']);
        unset($_POST['sessao_assunto']);
        unset($_POST['numero_do_pedido']);
        unset($_SESSION['faleconosco_post']);
        unset($_SESSION['faleconosco_files']);
    }
    
    /**
     * Valida o formulário primário
     *
     * @return string Texto do erro da validação ou string vazia, se não houver erro
     */
    
    public static function erro_validacao_primaria()
    {   
        $erro_validacao = "";
        
        if(!isset($_POST['nome']) || strlen(trim($_POST['nome'])) == 0) {
            $erro_validacao = "Nome precisa ser informado";
        }
        elseif(!isset($_POST['email']) || strlen(trim($_POST['email'])) == 0) {
            $erro_validacao = "E-mail precisa ser informado";
        }
        elseif(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            $erro_validacao = "E-mail inválido";
        }
        elseif(!isset($_POST['confirm_email']) || strlen(trim($_POST['confirm_email'])) == 0) {
            $erro_validacao = "Confirmação de e-mail precisa ser informada";
        }
        elseif(trim($_POST['confirm_email']) != trim($_POST['email'])) {
            $erro_validacao = "Confirmação de e-mail precisa ser igual ao e-mail";
        }
        elseif(!isset($_POST['content']) || strlen(trim($_POST['content'])) == 0) {
            $erro_validacao = "Mensagem precisa ser informada";
        }
        elseif($_POST['expocampo']) {
            $erro_validacao = "Ocorreu um erro ao realizar a operação";
        }
        elseif(strlen(trim($_POST['content'])) < 10) {
            $erro_validacao = "Mensagem deve ter, no mínimo, 10 caracteres";
        }
        elseif(self::is_spam()) {
            $erro_validacao = "Suspeita de spam. Utilize somente texto simples nos campos.";
        }
        elseif(isset($_FILES['imagem'])) {
            
            $tamanho = 0;
            for($i = 0; $i < count($_FILES['imagem']['name']); $i++) {
                
                if($_FILES['imagem']['size'][$i] == 0) continue;
                
                if(!is_imagem($_FILES['imagem']['name'][$i])) {
                    $erro_validacao = "Formato inválido de imagem";
                    break;
                }
                
                $tamanho += $_FILES['imagem']['size'][$i];
            }
            
            if($tamanho > 10000000) {
                $erro_validacao = "Tamanho total das imagens deve ser de, no máximo, 10 MB";
            }

        }
        
        return $erro_validacao;
    }
    
    /**
     * Valida o formulário secundário
     *
     * @return bool
     */
    
    public static function erro_validacao_secundaria()
    {
        KLoader::helper("RecaptchaHelper");
        
        if(!is_selenium() && !is_desenvolvimento()) {
            $response = RecaptchaHelper::get_response($_POST['g-recaptcha-response']);
            
            if ($response != null && $response->success) {
                return false;
            }
            
            return true;
        }
        
        return false;
    }
    
    
    /**
     * Valida se o conteúdo dos dados do formulário é spam
     * 
     * @see https://www.maujor.com/tutorial/spam-em-formularios.php
     *
     * @return bool
     */
    
    private static function is_spam(){
        
        $is_spam = false;
        
        if (preg_match( "/bcc:|cc:|multipart|\[url|Content-Type:/i", implode($_POST))) {
            $is_spam = true;
        }
        
        if (preg_match_all("/<a|http:/i", implode($_POST), $out) > 0) {
            $is_spam = true;
        }
        
        if(!empty($_POST["expo_campo"])){
            $is_spam = true;
        }
        
        if((isset($_SERVER["HTTP_REFERER"]) && !stristr($_SERVER["HTTP_REFERER"],$_SERVER["HTTP_HOST"]))) {
            $is_spam = true;
        }
        
        return $is_spam;
    }
    
    /**
     * Realiza o upload das imagens que serão anexadas ao e-mail
     *
     * @return array Lista de arrays contendo informações sobre as embed images
     */
    
    public static function upload_embed_images()
    {
        $embed_images = [];

        if($_FILES) {
            for($i = 0; $i < count($_FILES['imagem']['name']); $i++) {
                if($_FILES['imagem']['size'][$i] == 0) {
                    continue;
                }

                $filename = "fc-" . time() . "-$i." . get_extensao($_FILES['imagem']['name'][$i]);
                $path = $_SERVER['DOCUMENT_ROOT'] . '/wp-content/temp/' . $filename;
                move_uploaded_file($_FILES['imagem']['tmp_name'][$i], $path);
               
                $embed_image = ['path' => $path, 'cid' => "imagem-$i", 'name' => $filename];
                $embed_images[] =  $embed_image;
            }
        }
        
        return $embed_images;
    }
    
}