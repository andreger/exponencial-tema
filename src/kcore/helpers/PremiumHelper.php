<?php
/**
 * Helper para organizar rotinas relacionadas aos produtos premium
 *
 * @package	KCore/Helpers
 *
 * @author 	André Gervásio <andre.gervasio@keydea.com.br>
 *
 * @since	L1
 */

class PremiumHelper {
    
    /**
     * Retorna o nome do status do agendamento
     *
     * @since L1
     *
     * @param int $status_id Id do status
     *
     * @return string
     */
    
    static public function get_status_agendamento_nome($status_id, $estilo = true)
    {
        switch ($status_id) {
            case PREMIUM_STATUS_PENDENTE: {
                $nome = "Pendente";
                $cor = "#d0a70e";
                break;
            }
            case PREMIUM_STATUS_CONCLUIDO: {
                $nome = "Concluído";
                $cor = "#459c04";
                break;
            }
            default: {
                $nome = "";
                $cor = "#000000";
            }
        }
        
        if($estilo) {
            return "<span style='color:{$cor}'>$nome</span>";
        }
        else {
            return $nome;
        }
    }
    
    /**
     * Formata o ano/mês com separador
     *
     * @since L1
     *
     * @param int $ano_mes Ano/mês
     *
     * @return string
     */
    
    static public function formatar_ano_mes($ano_mes)
    {
        return substr($ano_mes, 0, 4) . "-" . substr($ano_mes, 4, 2);
    }
    
    /**
     * Atualiza os acumulados de um determinado período
     *
     * @param int $ano_mes_inicial Período inicial
     * @param int $ano_mes_final Período final
     * @param int $professor_id ID do professor
     */
    
    static public function atualizar_acumulados($ano_mes_inicial, $ano_mes_final, $professor_id = null)
    {
        KLoader::model("ColaboradorModel");
        KLoader::model("PremiumModel");
        KLoader::helper("RelatorioPagamentoProfessorPremiumHelper");
        
        $ano_mes = $ano_mes_inicial;
        
        while($ano_mes <= $ano_mes_final) {
            
            // recupera acumulado mês anterior
            $data_mes_atual = substr($ano_mes, 0, 4) . "-" . substr($ano_mes, 4, 2) . "-01";
            $ano_mes_anterior = date("Ym", strtotime($data_mes_atual . " -1 month"));
            
            // recupera a configuração de pagamento míniimo
            $config = PremiumModel::get_config_ativa_por_data($data_mes_atual);
            $ppc_pagamento_minimo = $config ? $config->ppc_minimo_pgto : 300;

            // recupera os dados de pagamento de professores
            $professores_premiums = RelatorioPagamentoProfessorPremiumHelper::get_dados($ano_mes, $professor_id);
            
            $professores = ColaboradorModel::listar_por_tipo();
            
            foreach ($professores as $professor) {
                
                // filtro por professor
                if($professor_id && $professor->ID != $professor_id) {
                    continue;
                }
                
                $valor_acumulado_anterior = 0;
                if($acumulado_anterior = PremiumModel::get_acumulado($ano_mes_anterior, $professor->ID)) {
                    $valor_acumulado_anterior = $acumulado_anterior->ppa_valor_acumulado;
                }
                
                // recupera valores do mes atual
                $valor_mes_atual = 0;
                if($professores_premiums[$professor->display_name]) {
                    $valor_mes_atual = $professores_premiums[$professor->display_name]["total"];
                }
                
                $novo_acumulado = $valor_acumulado_anterior + $valor_mes_atual;
                
                $novo_acumulado = $novo_acumulado < $ppc_pagamento_minimo ? $novo_acumulado : 0;

                PremiumModel::excluir_acumulado($ano_mes, $professor->ID);
                PremiumModel::salvar_acumulado($ano_mes,  $professor->ID, $novo_acumulado);
            }
            
            $ano_mes_formatado = PremiumHelper::formatar_ano_mes($ano_mes) . '-01';
            $ano_mes = date('Ym', strtotime("{$ano_mes_formatado} +1 month"));
        }
    }
    
    /**
     * Retorna um array com opções para selects de premiums com ids
     *
     * @since L1
     *
     * @param string $default Texto default do combo
     *
     * @return array Array com combo options
     */
    
    public static function get_combo_options_com_ids($default = null)
    {
        KLoader::model("PremiumModel");
        
        $lista = PremiumModel::listar_produtos(PROFESSOR_EQUIPE_ASSINATURAS);
        
        $options = [];
        
        if($default) {
            $options[0] = $default;
        }
        
        foreach ($lista as $item) {
            $o_key = $item->post_id;
            $o_value = $item->post_title . ' - ' . $item->post_id;
            
            $options[$o_key] = $o_value;
        }
        
        return $options;
    }
}