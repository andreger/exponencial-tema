<?php
/**
 * Helper para organizar rotinas relacionadas ao plugin Yoast
 * 
 * @package	KCore/Helpers
 *
 * @author 	João Paulo Ferreira <jpaulofms@gmail.com>
 *
 * @since	L1
 */ 

class YoastHelper {



    /**
     * Retorna o valor padrão de um metadado do Yoast
     * 
     * @since L1
     * 
     * @param string $meta metadado
     * 
     * @return string valor padrão do metadado
     */
    public static function get_valor_default($meta)
    {
        $option = get_option("wpseo_titles");
    
        return $option[$meta];
    }

    /**
	 * Realiza o parse dos metadados do título de concurso
	 * 
	 * @since L1
	 * 
	 * @param string $title Formato do título
     * 
     * @param object $concurso Objeto do concurso
	 * 
	 * @return string metadados alterados do título de concurso
	 */ 
	public static function parse_concurso_meta($title, $concurso)
    {
        if($concurso) {
            $title = str_replace("%%title%%",  get_the_title($concurso->con_id), $title);
            $title = str_replace("%%page%%",  "", $title);
            $title = str_replace("%%sitename%%", get_bloginfo("name"), $title);
        }
        
        return $title;
    }

    /**
     * Realiza o parse dos metadados da descrição de matéria
     * 
     * @since L1
     * 
     * @param string $descricao Formato da descrição
     * @param object $professor Objeto da matéria
     * @param int $pagina_id ID da página da matéria
     * 
     * @return string metados alterados da descrição da matéria
     */
    public static function parse_materia_meta($descricao, $materia, $pagina_id = null)
    {
        if($materia)
        {   
            $descricao = str_replace("%%title%%",  $pagina_id ? get_the_title($pagina_id) : "", $descricao);
            $descricao = str_replace("%%sitename%%",  get_bloginfo("name"), $descricao);
            $descricao = str_replace("%%primary_category%%", $materia->name, $descricao);
        }
        
        return $descricao;
    }

    /**
     * Realiza o parse dos metadados da descrição de professor
     * 
     * @since L1
     * 
     * @param string $descricao Formato da descrição
     * @param object $professor Objeto do professor (colaborador)
     * @param int $pagina_id ID da página do professor
     * 
     * @return string metados alterados da descrição do professor
     */
    public static function parse_professor_meta($descricao, $professor, $pagina_id = null)
    {
        if($professor)
        {   
            $descricao = str_replace("%%title%%",  $pagina_id ? get_the_title($pagina_id) : "", $descricao);
            $descricao = str_replace("%%sitename%%",  get_bloginfo("name"), $descricao);
            $descricao = str_replace("%%primary_category%%", $professor->display_name, $descricao);
        }
        
        return $descricao;
    }

    public static function render_keyword() 
    {
        return "";
        KLoader::helper("UrlHelper");

        $concurso = "";
        if(UrlHelper::is_url_cursos_por_concurso_especifico()) {
            $nome_concurso = get_query_var("nome");
            $concurso = ConcursoModel::get_by_slug($nome_concurso);

            $meta = WPSEO_Meta::get_value('focuskw_text_input', $concurso->con_id );
        }
        else {
            global $post;

            $meta = WPSEO_Meta::get_value('focuskw_text_input', $post->ID);
        }

        $meta = self::parse_concurso_meta($meta, $concurso);

        return $meta ? "<meta name='keywords' content='$meta'>" : "";
    }

}