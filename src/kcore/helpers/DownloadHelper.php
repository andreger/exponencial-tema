<?php
/**
 * Helper para organizar rotinas relacionadas aos downloads agendados
 *
 * @package	KCore/Helpers
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K5
 */

class DownloadHelper {
    
    /**
     * Recupera o id da notificação
     *
     * @param int $usuario_id Id do usuário
     *
     * @return string Id da notificação
     */
    
    static public function get_notificacao_id($usuario_id)
    {
        return DOWNLOAD_NOTIFICACAO_PREFIXO . $usuario_id;
    }
    
    /**
     * Recupera o nome de um relatório
     * 
     * @param int $relatorio_id Id do relatório
     * 
     * @return string Nome do relatório
     */
    
    static public function get_relatorio_nome($download_id)
    {
        KLoader::model("DownloadModel");
        $download = DownloadModel::get_by_id($download_id);
        
        switch ($download->dow_relatorio_id) {
            
            case DOWNLOAD_RELATORIO_VENDAS: {
                return "Relatório de Vendas";
                break;
            }
            
            case DOWNLOAD_BAIXAR_TODAS: {
                
                $filtros = unserialize($download->dow_filtros);
                
                $produto = wc_get_product($filtros['produto_id']);
                $titulo = $produto->get_title();
                
                return "Aulas: {$titulo}";
                break;
            }
        }
        
        return "";
    }
    
    /**
     * Recupera o nome de um status
     *
     * @param int $status_id Id do status
     *
     * @return string Nome do status
     */
    
    static public function get_status_nome($status_id)
    {
        switch ($status_id) {
            
            case DOWNLOAD_STATUS_PENDENTE: {
                return "Pendente";
            }
            
            case DOWNLOAD_STATUS_CONCLUIDO: {
                return "Concluído";
            }
            
            case DOWNLOAD_STATUS_ERRO: {
                return "Erro";
            }
        }
        
        return "";
    }
    
}