<?php
/**
 * Helper para organizar rotinas relacionadas à URLs
 * 
 * @package	KCore/Helpers
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K4
 */ 

class UrlHelper {
    
	/**
	 * Retorna a URL da operação de adição de produto ao carrinho
	 * 
	 * @since K4
	 * 
	 * @param int $produto Id do produto
	 * 
	 * @return string URL da operação de adição de produto ao carrinho
	 */ 

	public static function get_adicionar_produto_ao_carrinho_url($produto_id, $force_login = TRUE)
	{
	    $wc_product= wc_get_product($produto_id);
	    
	    $url = $wc_product->get_permalink() . "?add-to-cart=" . $produto_id;
	    
	    // se usuário não estiver logado joga para tela de login
	    if($force_login && !is_usuario_logado()) {
	        return login_url($url);
	    }
	    
	    return $url;
	}

	/**
	 * Retorna a URL da página de audiobooks grátis
	 * 
	 * @since K4
	 * 
	 * @return string URL da página de audiobooks grátis
	 */ 

	public static function get_audiobooks_gratis_url()
	{
		return "/audiobooks-gratis/";
	}
	
	/**
	 * Retorna a URL da página de pesquisa
	 *
	 * @since K5
	 *
	 * @return string URL da página de pesquisa
	 */
	
	public static function get_pesquisa_url()
	{
	    return "/pesquisa/";
	}
	

	/**
	 * Retorna a URL do blog post
	 * 
	 * @since K4
	 * 
	 * @param object $blog Objeto do blog post
	 * 
	 * @return string URL do blog post
	 */ 

	public static function get_blog_post_url($blog)
	{
		return "/" . $blog->blo_slug;
	}

	/**
	 * Retorna a URL do thumb do blog post
	 * 
	 * @since K4
	 * 
	 * @param object $concurso
	 * 
	 * @return string URL do blog post
	 */ 

	public static function get_blog_post_thumb_url($blog)
	{
		return $blog->blo_thumb ?: "/wp-content/themes/academy/images/default_book.jpg";
	}

	/**
	 * Retorna a URL da página de cursos grátis
	 * 
	 * @since K4
	 * 
	 * @return string URL da página de cursos grátis
	 */ 

	public static function get_cursos_gratis_url()
	{
		return "/cursos-gratis/";
	}

	/**
	 * Retorna a URL da página de cursos por concurso
	 * 
	 * @since K4
	 * 
	 * @return string URL da página de cursos por concursos
	 */ 

	public static function get_cursos_por_concurso_url()
	{
		return "/cursos-por-concurso/";
	}

	/**
	 * Retorna a URL da página de cursos por concurso específico
	 * 
	 * @since L1
	 * 
	 * @param int|string|array $input Id, slug ou array associativo do concurso
	 * 
	 * @return string URL da página de cursos por professor
	 */ 

	public static function get_cursos_por_concurso_especifico_paginado_url($input)
	{
		$slug = "";

		if(is_string($input)) {
			$slug = $input;
		}
		elseif(is_int($input)) {
			// implementar
		}
		elseif(is_array($input)) {
			// implementar
		}
		else {
			throw new Error("Argumento inválido.");
		}

		return "/cursos-por-concurso/$slug";
	}

	/**
	 * Retorna a URL da página de cursos por concurso específico
	 * 
	 * @since K4
	 * 
	 * @param int|string|array $input Id, slug ou array associativo do concurso
	 * 
	 * @return string URL da página de cursos por professor
	 */ 

	public static function get_cursos_por_concurso_especifico_url($input)
	{
		$slug = "";

		if(is_string($input)) {
			$slug = $input;
		}
		elseif(is_int($input)) {
			// implementar
		}
		elseif(is_array($input)) {
			// implementar
		}
		else {
			throw new Error("Argumento inválido.");
		}

		return "/concurso/$slug";
	}

	/**
	 * Retorna a URL da página de cursos por professor
	 * 
	 * @since K4
	 * 
	 * @return string URL da página de cursos por professor
	 */ 

	public static function get_cursos_por_professor_url()
	{
		return "/cursos-por-professor/";
	}

	/**
	 * Retorna a URL da página de cursos por professor de um usuário específico
	 * 
	 * @since K4
	 * 
	 * @param int|string|array $input Id, slug ou array associativo do professor
	 * 
	 * @return string URL da página de cursos por professor
	 */ 

	public static function get_cursos_por_professor_especifico_url($input)
	{
		$slug = "";

		if(is_string($input)) {
			$slug = $input;
		}
		elseif(is_int($input)) {
			// implementar
		}
		elseif(is_array($input)) {
			// implementar
		}
		else {
			throw new Error("Argumento inválido.");
		}

		return "/cursos-por-professor/$slug";
	}

	/**
	 * Retorna a URL da página de artigos por professor de um usuário específico
	 * 
	 * @since K5
	 * 
	 * @param int|string|array $input Id, slug ou array associativo do professor
	 * 
	 * @return string URL da página de cursos por professor
	 */ 

	public static function get_artigos_por_professor_especifico_url($input)
	{
		$slug = "";

		if(is_string($input)) {
			$slug = $input;
		}
		elseif(is_int($input)) {
			// implementar
		}
		elseif(is_array($input)) {
			// implementar
		}
		else {
			throw new Error("Argumento inválido.");
		}

		return "/artigos-por-professor/$slug";
	}

	/**
	 * Retorna a URL da página de cursos por matéria
	 * 
	 * @since K4
	 * 
	 * @return string URL da página de cursos por matéria
	 */ 

	public static function get_cursos_por_materia_url()
	{
		return "/cursos-por-materia/";
	}

	/**
	 * Retorna a URL da página de cursos por matéria específica
	 * 
	 * @since K4
	 * 
	 * @param int|string|array $input Id, slug ou array associativo da matéria
	 * 
	 * @return string URL da página de cursos por matéria
	 */ 

	public static function get_cursos_por_materia_especifica_url($input)
	{
		$slug = "";

		if(is_string($input)) {
			$slug = $input;
		}
		elseif(is_int($input)) {
			// implementar
		}
		elseif(is_array($input)) {
			// implementar
		}
		else {
			throw new Error("Argumento inválido.");
		}

		return "/cursos-por-materia/$slug";
	}

	/**
	 * Retorna a URL da página de um depoimento em texto
	 * 
	 * @since L1
	 * 
	 * @param string $input
	 * 
	 * @return string URL da página de um depoimento em texto
	 */
	public static function get_depoimento_url($input)
	{
		$slug = "";

		if(is_string($input)) {
			$slug = $input;
		}
		elseif(is_int($input)) {
			// implementar
		}
		elseif(is_array($input)) {
			// implementar
		}
		else {
			throw new Error("Argumento inválido.");
		}

		return "/depoimento/$slug";
	}

	/**
	 * Retorna a URL da página de depoimentos
	 * 
	 * @since L1
	 * 
	 * @return string URL da página de depoimentos
	 */ 

	public static function get_depoimentos_url()
	{
		return "/depoimentos/";
	}

	/**
	 * Retorna a URL da página das depoimentos em texto
	 * 
	 * @since L1
	 * 
	 * @return string URL da página dos depoimentos em texto
	 */
	public static function get_depoimentos_textos_url()
	{
		return "/depoimentos-textos/";
	}

	/**
	 * Retorna a URL da página dos depoimentos em video
	 * 
	 * @since L1
	 * 
	 * @return string URL da página dos depoimentos em video
	 */
	public static function get_depoimentos_videos_url()
	{
		return "/depoimentos-videos/";
	}

	/**
	 * Retorna a URL da página de uma entrevista em texto
	 * 
	 * @since L1
	 * 
	 * @param string $input
	 * 
	 * @return string URL da página de uma entrevista em texto
	 */
	public static function get_entrevista_url($input)
	{
		$slug = "";

		if(is_string($input)) {
			$slug = $input;
		}
		elseif(is_int($input)) {
			// implementar
		}
		elseif(is_array($input)) {
			// implementar
		}
		else {
			throw new Error("Argumento inválido.");
		}

		return "/entrevista/$slug";
	}

	/**
	 * Retorna a URL da página das entrevistas em texto
	 * 
	 * @since L1
	 * 
	 * @return string URL da página das entrevistas em texto
	 */
	public static function get_entrevistas_textos_url()
	{
		return "/entrevistas-textos/";
	}

	/**
	 * Retorna a URL da página das entrevistas em video
	 * 
	 * @since L1
	 * 
	 * @return string URL da página das entrevistas em video
	 */
	public static function get_entrevistas_videos_url()
	{
		return "/entrevistas-videos/";
	}

	/**
	 * Retorna a URL da página de mapas mentais grátis
	 * 
	 * @since K4
	 * 
	 * @return string URL da página de mapas mentais grátis
	 */ 

	public static function get_mapas_mentais_gratis_url()
	{
		return "/mapas-mentais-gratis/";
	}

	/**
	 * Retorna a URL da página de um produto
	 * 
	 * @since K4
	 * 
	 * @param string $input
	 * 
	 * @return string URL da página de um produto
	 */ 

	public static function get_produto_url($input)
	{
		$slug = "";

		if(is_string($input)) {
			$slug = $input;
		}
		elseif(is_int($input)) {
			// implementar
		}
		elseif(is_array($input)) {
			// implementar
		}
		elseif(is_object($input)) {
			$slug = $input->post_name;
		}
		else {
			throw new Error("Argumento inválido.");
		}

		return "/produto/$slug";
	}


	/**
	 * Retorna a URL do professor (página de detalhe)
	 * 
	 * @since K4
	 * 
	 * @param object $input
	 * 
	 * @return string URL da página do professor
	 */ 

	public static function get_professor_url($input)
	{
		$slug = "";

		if(is_string($input)) {
			$slug = $input;
		}
		elseif(is_int($input)) {
			// implementar
		}
		elseif(is_array($input)) {
			// implementar
		}
		elseif(is_object($input)) {
			$slug = $input->col_slug;
		}
		else {
			throw new Error("Argumento inválido.");
		}

		return "/professor/$slug";
	}

	/**
	 * Retorna a URL da página de listagem de professores
	 * 
	 * @since K4
	 * 
	 * @return string URL da página de professores
	 */ 

	public static function get_professores_url()
	{
		return "/professores/";
	}


	/**
	 * Retorna a URL da página de questões comentadas grátis
	 * 
	 * @since K4
	 * 
	 * @return string URL da página de questões comentadas grátis
	 */ 

	public static function get_questoes_comentadas_gratis_url()
	{
		return "/questoes-comentadas-gratis/";
	}

	/**
	 * Retorna a URL da página de simulados grátis
	 * 
	 * @since K4
	 * 
	 * @return string URL da página de simulados grátis
	 */ 

	public static function get_simulados_gratis_url()
	{
		return "/simulados-gratis/";
	}

	/**
	 * Retorna a URL da página de cursos online
	 * 
	 * @since K4
	 * 
	 * @return string URL da página de cursos online
	 */ 

	public static function get_todos_cursos_url()
	{
		return "/cursos-online/";
	}

	/**
	 * Função genérica para verificar se uma URL bate com um padrão especificado
	 * 
	 * @since K4
	 * 
	 * @param string 		$padrao Padrão da URL a ser verificado.
	 * @param string|NULL 	$url Url a ser consultada. Se não for informada, a URL atual será utilizada
	 * 
	 * @return bool
	 */ 

	private static function is_url($padrao, $url = NULL)
	{
		$url = $url ?: $_SERVER['REQUEST_URI'];
		
		$url_a = parse_url($url);

		return preg_match($padrao, $url_a['path']);
	}

	/**
	 * Verifica se uma URL é a de audiobooks grátis
	 * 
 	 * @since K4
	 * 
	 * @param string|NULL 	$url Url a ser consultada. Se não for informada, a URL atual será utilizada
	 * 
	 * @return bool
	 */ 

	public static function is_url_audiobooks_gratis($url = NULL)
	{
		return self::is_url("/\/audiobooks-gratis(\/.*)?/", $url);
	}
	
	/**
	 * Verifica se uma URL é a de pesquisa
	 *
	 * @since K5
	 *
	 * @param string|NULL 	$url Url a ser consultada. Se não for informada, a URL atual será utilizada
	 *
	 * @return bool
	 */
	
	public static function is_url_pesquisa($url = NULL)
	{
	    return self::is_url("/\/pesquisa(\/.*)?/", $url);
	}

	/**
	 * Verifica se uma URL é a de cursos grátis
	 * 
 	 * @since K4
	 * 
	 * @param string|NULL 	$url Url a ser consultada. Se não for informada, a URL atual será utilizada
	 * 
	 * @return bool
	 */ 

	public static function is_url_cursos_gratis($url = NULL)
	{
		return self::is_url("/\/cursos-gratis(\/.*)?/", $url);
	}

	/**
	 * Verifica se uma URL é a de cursos por concurso
	 * 
 	 * @since K4
	 * 
	 * @param string|NULL 	$url Url a ser consultada. Se não for informada, a URL atual será utilizada
	 * 
	 * @return bool
	 */ 

	public static function is_url_cursos_por_concurso($url = NULL)
	{
		return self::is_url("/\/cursos-por-concurso(\/)?$/", $url);
	}

	/**
	 * Verifica se uma URL é a da página de cursos de um concurso específico com paginação dos cursos
	 * 
	 * @since L1
	 * 
	 * @param string|NULL 	$url Url a ser consultada. Se não for informada, a URL atual será utilizada
	 * 
	 * @return bool
	 */ 

	public static function is_url_cursos_por_concurso_especifico_paginada($url = NULL)
	{
		return self::is_url("/\/cursos-por-concurso\/.+/", $url);
	}

	/**
	 * Verifica se uma URL é a da página de cursos de um concurso específico
	 * 
	 * @since K4
	 * 
	 * @param string|NULL 	$url Url a ser consultada. Se não for informada, a URL atual será utilizada
	 * 
	 * @return bool
	 */ 

	public static function is_url_cursos_por_concurso_especifico($url = NULL)
	{
		return self::is_url("/\/concurso\/.+/", $url);
	}

	/**
	 * Verifica se uma URL é a de cursos por matéria
	 * 
 	 * @since K4
	 * 
	 * @param string|NULL 	$url Url a ser consultada. Se não for informada, a URL atual será utilizada
	 * 
	 * @return bool
	 */ 
	
	public static function is_url_cursos_por_materia($url = NULL)
	{
		return self::is_url("/\/cursos-por-materia(\/)?$/", $url);
	}

	/**
	 * Verifica se a página corrente é a de cursos de uma matéria específica
	 * 
	 * @since K3
	 * 
	 * @param string|NULL 	$url Url a ser consultada. Se não for informada, a URL atual será utilizada
	 * 
	 * @return bool
	 */
	 
	public static function is_url_cursos_por_materia_especifica($url = NULL)
	{
		return self::is_url("/\/cursos-por-materia\/.+/", $url);
	}

	/**
	 * Verifica se uma URL é a de cursos por professor
	 * 
 	 * @since K4
	 * 
	 * @param string|NULL 	$url Url a ser consultada. Se não for informada, a URL atual será utilizada
	 * 
	 * @return bool
	 */ 
	
	public static function is_url_cursos_por_professor($url = NULL)
	{
		return self::is_url("/\/cursos-por-professor(\/)?$/", $url);
	}

	/**
	 * Verifica se a página corrente é a de cursos de um professor específico
	 * 
	 * @since K3
	 * 
	 * @param string|NULL 	$url Url a ser consultada. Se não for informada, a URL atual será utilizada
	 * 
	 * @return bool
	 */
	 
	public static function is_url_cursos_por_professor_especifico($url = NULL)
	{
		return self::is_url("/\/cursos-por-professor\/.+/", $url);
	}

	/**
	 * Verifica se a página corrente é a de artigos de um professor específico
	 * 
	 * @since K5
	 * 
	 * @param string|NULL 	$url Url a ser consultada. Se não for informada, a URL atual será utilizada
	 * 
	 * @return bool
	 */
	 
	public static function is_url_artigos_por_professor_especifico($url = NULL)
	{
		return self::is_url("/\/artigos-por-professor\/.+/", $url);
	}

	/**
	 * Verifica se uma URL é a de mapas mentais grátis
	 * 
 	 * @since K4
	 * 
	 * @param string|NULL 	$url Url a ser consultada. Se não for informada, a URL atual será utilizada
	 * 
	 * @return bool
	 */ 

	public static function is_url_mapas_mentais_gratis($url = NULL)
	{
		return self::is_url("/\/mapas-mentais-gratis(\/.*)?/", $url);
	}
	
	/**
	 * Verifica se uma URL é a do meu perfil
	 *
	 * @since K5
	 *
	 * @param string|NULL 	$url Url a ser consultada. Se não for informada, a URL atual será utilizada
	 *
	 * @return bool
	 */
	
	public static function is_url_meu_perfil($url = NULL)
	{
	    return self::is_url("/\/questoes\/perfil\/?/", $url);
	}

	/**
	 * Verifica se a página corrente é a de um produto específico
	 * 
	 * @since K4
	 * 
	 * @param string|NULL 	$url Url a ser consultada. Se não for informada, a URL atual será utilizada
	 * 
	 * @return bool
	 */

	public static function is_url_produto($url = NULL)
	{
	    if($url) {
	        $url_a = parse_url($url);
	        
	        if($url_a) {
	            $url = $url_a["host"] . $url_a["path"];
	        }
	    }
		return self::is_url("/\/produto\/.+/", $url);
	}

	/**
	 * Verifica se uma URL é a de detalhe de professor
	 * 
 	 * @since K4
	 * 
	 * @param string|NULL 	$url Url a ser consultada. Se não for informada, a URL atual será utilizada
	 * 
	 * @return bool
	 */ 
	
	public static function is_url_professor($url = NULL)
	{
		return self::is_url("/\/professor\/.+/", $url);
	}

	/**
	 * Verifica se uma URL é a de questões comentadas grátis
	 * 
 	 * @since K4
	 * 
	 * @param string|NULL 	$url Url a ser consultada. Se não for informada, a URL atual será utilizada
	 * 
	 * @return bool
	 */ 

	public static function is_url_questoes_comentadas_gratis($url = NULL)
	{
		return self::is_url("/\/questoes-comentadas-gratis(\/.*)?/", $url);
	}

	/**
	 * Verifica se uma URL é a de simulados grátis
	 * 
 	 * @since K4
	 * 
	 * @param string|NULL 	$url Url a ser consultada. Se não for informada, a URL atual será utilizada
	 * 
	 * @return bool
	 */ 

	public static function is_url_simulados_gratis($url = NULL)
	{
		return self::is_url("/\/simulados-gratis(\/.*)?/", $url);
	}

	/**
	 * Verifica se uma URL é a de cadastro e login
	 * 
 	 * @since K4
	 * 
	 * @param string|NULL 	$url Url a ser consultada. Se não for informada, a URL atual será utilizada
	 * 
	 * @return bool
	 */ 

	public static function is_url_cadastro_login($url = NULL)
	{
		//return self::is_url("/\/cadastro-login(\/)?$/", $url);
		return self::is_url("/\/cadastro-login(\/.*)?/", $url);
	}
	
	/**
	 * Verifica se uma URL é a de todos os cursos
	 *
	 * @since L1
	 *
	 * @param string|NULL 	$url Url a ser consultada. Se não for informada, a URL atual será utilizada
	 *
	 * @return bool
	 */
	
	public static function is_url_todos_cursos($url = NULL)
	{
	    return self::is_url("/\/todos-cursos(\/.*)?/", $url);
	}
	
	/**
	 * Verifica se a página corrente é uma página do SQ
	 * 
	 * @since L1
	 * 
	 * @param string|NULL 	$url Url a ser consultada. Se não for informada, a URL atual será utilizada
	 * 
	 * @return bool
	 */
	 
	public static function is_url_sistema_questoes($url = NULL)
	{
		return self::is_url("/\/questoes\/.+/", $url);
	}

	/**
	 * Verifica se a página corrente é uma página do SQ/Admin
	 * 
	 * @since L1
	 * 
	 * @param string|NULL 	$url Url a ser consultada. Se não for informada, a URL atual será utilizada
	 * 
	 * @return bool
	 */
	public static function is_url_sistema_questoes_admin($url = NULL)
	{
		return self::is_url("/\/questoes\/admin\/.+/", $url);
	}
	
	/**
	 * Recupera slug de URL de blog ou página
	 *
	 * @since L1
	 *
	 * @param string|NULL 	$url Url a ser consultada. Se não for informada, a URL atual será utilizada
	 *
	 * @return string
	 */
	
	public static function get_blog_ou_pagina_slug_by_url($url = NULL)
	{
	    $url_a = parse_url($url);
	    
	    if($url_a) {
	        if($url['path'] && strpos($url['path'], '/') === false) {
	            return $url['path'];
	        }
	        else {
	            return null;
	        }
	    }
	}

	/**
	 * Retorna a URL de requisição de acesso a item de um premium
	 *
	 * @since L1
	 *
	 * @param int $item_id Id do item do premium
	 *
	 * @return string
	 */
	
	public static function get_requerer_acesso_item_premium_url($item_id, $pedido_id, $produto_id)
	{
	    return self::get_minha_conta_detalhe_url($pedido_id, $produto_id) . "&act=premium_item&i_id=" . $item_id;
	}
		
	/**
	 * Retorna a URL do detalhe de um produto no minha conta
	 *
	 * @since L1
	 *
	 * @param int $pedido_id Id do pedido
	 * @param int $produto_id Id do produto
	 *
	 * @return string
	 */
	
	public static function get_minha_conta_detalhe_url($pedido_id, $produto_id)
	{
	    return "/minha-conta-detalhe?o_id={$pedido_id}&p_id={$produto_id}";
	}
	
	/**
	 * Verifica se a página corrente é a de um produto específico
	 *
	 * @since K4
	 *
	 * @param string|NULL 	$url Url a ser consultada. Se não for informada, a URL atual será utilizada
	 *
	 * @return bool
	 */
	
	public static function is_url_landing_page($url = NULL)
	{
	    if($url) {
	        $url_a = parse_url($url);
	        
	        if($url_a) {
	            $url = $url_a["host"] . $url_a["path"];
	        }
	    }
	    return self::is_url("/\/eventos\/.+/", $url);
	}
	
	/**
	 * Retorna a URL da LP
	 *
	 * @since L1
	 *
	 * @param string $input
	 *
	 * @return string URL da LP
	 */
	
	public static function get_landing_page_url($slug)
	{
	    return "/eventos/$slug";
	}

	/**
	 * Verifica se uma URL é a de depoimentos
	 * 
 	 * @since L1
	 * 
	 * @param string|NULL 	$url Url a ser consultada. Se não for informada, a URL atual será utilizada
	 * 
	 * @return bool
	 */ 

	public static function is_url_depoimentos($url = NULL)
	{
		return self::is_url("/\/depoimentos(\/)?$/", $url);
	}

	/**
	 * Verifica se uma URL é a de depoimentos em vídeo
	 * 
 	 * @since L1
	 * 
	 * @param string|NULL 	$url Url a ser consultada. Se não for informada, a URL atual será utilizada
	 * 
	 * @return bool
	 */ 

	public static function is_url_depoimentos_videos($url = NULL)
	{
		return self::is_url("/\/depoimentos-videos(\/)?$/", $url);
	}

	/**
	 * Verifica se uma URL é a de depoimentos em texto
	 * 
 	 * @since L1
	 * 
	 * @param string|NULL 	$url Url a ser consultada. Se não for informada, a URL atual será utilizada
	 * 
	 * @return bool
	 */ 

	public static function is_url_depoimentos_textos($url = NULL)
	{
		return self::is_url("/\/depoimentos-textos(\/)?$/", $url);
	}

	/**
	 * Verifica se uma URL é a de entrevistas em vídeo
	 * 
 	 * @since L1
	 * 
	 * @param string|NULL 	$url Url a ser consultada. Se não for informada, a URL atual será utilizada
	 * 
	 * @return bool
	 */ 

	public static function is_url_entrevistas_videos($url = NULL)
	{
		return self::is_url("/\/entrevistas-videos(\/)?$/", $url);
	}

	/**
	 * Verifica se uma URL é a de entrevistas em texto
	 * 
 	 * @since L1
	 * 
	 * @param string|NULL 	$url Url a ser consultada. Se não for informada, a URL atual será utilizada
	 * 
	 * @return bool
	 */ 

	public static function is_url_entrevistas_textos($url = NULL)
	{
		return self::is_url("/\/entrevistas-textos(\/)?$/", $url);
	}

	/**
	 * Verifica se uma URL é a da página de depoimento específico
	 * 
	 * @since L1
	 * 
	 * @param string|NULL 	$url Url a ser consultada. Se não for informada, a URL atual será utilizada
	 * 
	 * @return bool
	 */ 

	public static function is_url_depoimento($url = NULL)
	{
		return self::is_url("/\/depoimento\/.+/", $url);
	}

	/**
	 * Verifica se uma URL é a da página de depoimento específico
	 * 
	 * @since L1
	 * 
	 * @param string|NULL 	$url Url a ser consultada. Se não for informada, a URL atual será utilizada
	 * 
	 * @return bool
	 */ 

	public static function is_url_entrevista($url = NULL)
	{
		return self::is_url("/\/entrevista\/.+/", $url);
	}
	
	/**
	 * Recupera a URL de um caderno
	 *
	 * @since L1
	 *
	 * @param int $caderno_id Id do caderno
	 * @param int $pagina Página do caderno
	 * @param bool 	$zerar_filtros Flag informando se é para zerar filtros ou não
	 *
	 * @return string
	 */ 

	public static function get_caderno_url($caderno_id, $pagina = NULL, $zerar_filtros = FALSE)
	{
		$base_url = get_site_url() . '/questoes/';

		$pagina_fragmento = !is_null($pagina) ? "/" . $pagina : "";

		$sufixo = $zerar_filtros ? "?zf=1" : "";
		
		return $base_url . 'main/resolver_caderno/' . $caderno_id . $pagina_fragmento . $sufixo;
	}
	
	/**
	 * Remove um domínio de uma URL
	 * 
	 * @since M1
	 *
	 * @param string $url URL de input
	 *
	 * @return string
	 */ 
	
	public static function remover_dominio($url) 
	{
	    $url_a = parse_url($url);
	    
	    $sem_dominio = $url_a['path'];
	    
	    if(isset($url_a['query'])) {
	        $sem_dominio .= '?' . $url_a['query'];
	    }
	    
	    
	    return $sem_dominio;
	}
}