<?php
/**
 * Helper para organizar rotinas relacionadas às strings
 *
 * @package	KCore/Helpers
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K5
 */

class StringHelper {
    
    /**
     * Retorna as substrings de uma string
     *
     * @since K5
     *
     * @param string $str String de input
     * @param int $startDelimiter Delimitador do início da substring
     * @param int $endDelimiter Delimitador do fim da substring
     *
     * @return string HTML da tabela do relatório
     */
    
    public static function get_substrings($str, $startDelimiter, $endDelimiter)
    {
        $contents = array();
        $startDelimiterLength = strlen($startDelimiter);
        $endDelimiterLength = strlen($endDelimiter);
        $startFrom = $contentStart = $contentEnd = 0;
        while (false !== ($contentStart = strpos($str, $startDelimiter, $startFrom))) {
            $contentStart += $startDelimiterLength;
            $contentEnd = strpos($str, $endDelimiter, $contentStart);
            if (false === $contentEnd) {
                break;
            }
            $contents[] = substr($str, $contentStart, $contentEnd - $contentStart);
            $startFrom = $contentEnd + $endDelimiterLength;
        }
        
        return $contents;
     }
     
     /**
      * Retorna a string no singular ou plural dependendo do número de itens
      *
      * @since L1
      *
      * @param int $num Número a ser considerado
      * @param string $singular String no singular
      * @param string $plural String no plural
      * @param string $zero String caso num seja 0. Se definido como null, função retornará plural.
      * 
      * @throws InvalidArgumentException
      *
      * @return string
      */
     
     public static function pluralizar($num, $singular, $plural, $zero = null)
     {
         /* === tratamento dos parâmetros === */
         if(is_null($num)) throw new InvalidArgumentException("Argumento 'num' não poder ser nulo");
         if(is_null($singular)) throw new InvalidArgumentException("Argumento 'singular' não poder ser nulo");
         if(is_null($plural)) throw new InvalidArgumentException("Argumento 'plural' não poder ser nulo");
         if(!is_numeric($num)) throw new InvalidArgumentException("Num precisa ser numérico");
         
         /* === lógica principal === */
         
         // tratamento caso especial do zero
         if($num == 0 && $zero ) {
             return $zero;
         }
         
         // fluxo natural
         return $num == 1 ? $singular : $plural;
     }
     
     /**
      * Remove os acentos de uma string
      *
      * @since L1
      *
      * @param string $input String de input
      *
      * @return string
      *
      */
     
     public static function remover_acentos($input)
     {
         $array1 = array( "á", "à", "â", "ã", "ä", "é", "è", "ê", "ë", "í", "ì", "î", "ï", "ó", "ò", "ô", "õ", "ö", "ú", "ù", "û", "ü", "ç"
             , "Á", "À", "Â", "Ã", "Ä", "É", "È", "Ê", "Ë", "Í", "Ì", "Î", "Ï", "Ó", "Ò", "Ô", "Õ", "Ö", "Ú", "Ù", "Û", "Ü", "Ç" );
         $array2 = array( "a", "a", "a", "a", "a", "e", "e", "e", "e", "i", "i", "i", "i", "o", "o", "o", "o", "o", "u", "u", "u", "u", "c"
             , "A", "A", "A", "A", "A", "E", "E", "E", "E", "I", "I", "I", "I", "O", "O", "O", "O", "O", "U", "U", "U", "U", "C" );
         
         return str_replace( $array1, $array2, $input);
     }
     
     /**
      * Remove todas as contrabarras de uma string
      *
      * @since K5
      *
      * @param string $str String de input
      *
      * @return string
      */
     
     public static function remover_contrabarras($str)
     {         
         return str_replace("\\", "", $str);
     }
     
     /**
      * Formata uma string em formato de slug
      *
      * @since L1
      *
      * @param string $str String a ser tratada
      *
      * @return string
      */
     
     public static function slugify($str, $tamanho = null)
     {
         if($tamanho) {
             $str = substr($str, 0, $tamanho);
         }
         
         $str = trim($str);
         $str = self::strtolower_sem_acento($str);
         $str = str_replace(" ", "-", $str);
         
         return $str;
     }
     
     /**
      * Converte uma string em minúscula e remove os acentos
      *
      * @since L1
      *
      * @param string $input String a ser tratada
      * @param int $tamanho Tamanho máximo da string
      *
      * @return string
      */
     
     public static function strtolower_sem_acento($input)
     {
         $str = strtolower($input);
         return self::remover_acentos($str);
     }
     

     /**
      * Encoda as aspas para exibição no input
      * 
      * @since L1
      *
      * @param string $str String a ser tratada
      *
      * @return string
      */
     
     public static function tratar_aspas($str)
     {
        
        $str = htmlentities( $str, ENT_QUOTES, "UTF-8");
        $str = stripslashes($str);

        return $str;
     }
     
    
     
}