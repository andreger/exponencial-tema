<?php
/**
 * Helper para organizar rotinas relacionadas aos grupos de acesso
 *
 * @package	KCore/Helpers
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K5
 */

class AcessoGrupoHelper {
    
    /**
     * Grupo de acesso para adicionar comentários em vídeo
     * 
     * @since L1
     * 
     * @return bool
     */

     static public function adicionar_comentarios_videos(){
         return tem_acesso([ADMINISTRADOR, COORDENADOR_SQ, PROFESSOR, CONSULTOR, REVISOR]);
     }

    /**
     * Grupo de acesso para a duplicação de cadernos SQ > Meus Cadernos > (Duplicar)
     *
     * @return bool
     */
    
    static public function caderno_coaching_acessar()
    {
        return tem_acesso([ALUNO_COACH]) || parceiro_ls();
    }

    /**
     * Grupo de acesso para a duplicação de cadernos SQ > Meus Cadernos > (Duplicar)
     *
     * @return bool
     */
    
    static public function cadernos_duplicar()
    {
        return tem_acesso([ADMINISTRADOR, COORDENADOR, CONSULTOR, PROFESSOR]);
    }
    
    /**
     * Grupo de acesso para a listagem de questões inativas em cadernos
     * 
     * @return bool
     */
    
    static public function cadernos_questoes_inativas()
    {
        return tem_acesso([ADMINISTRADOR, COORDENADOR_SQ, PROFESSOR]);
    }
    
    /**
     * Grupo de acesso para Painel Coaching > Admin > Duplicar Meta
     *
     * @return bool
     */
    
    static public function coaching_metas($redirecionar = null)
    {
        return tem_acesso([ADMINISTRADOR, COORDENADOR_COACHING, CONSULTOR], $redirecionar);
    }

    /**
     * Grupo de acesso para visualizar comentários
     * 
     * @since L1
     * 
     * @return bool
     */

    static public function comentarios_acessar($simulado_id, $questao_id){
       
        $tem_acesso = FALSE;
       
        if(tem_acesso(array(/*ADMINISTRADOR, COORDENADOR_SQ, PROFESSOR, CONSULTOR, ASSINANTE_SQ, REVISOR*/ USUARIO_LOGADO))) {
            $tem_acesso = TRUE;
        }
        else {
            if($simulado_id) {
                $tem_acesso = tem_acesso_aos_comentarios_do_simulado($simulado_id, get_current_user_id());
            }
            else {
                $tem_acesso = is_questao_em_caderno_comprado($questao_id);
            }
        }
        return $tem_acesso;
    }

    static public function copiar_ids_questoes($redirecionar = null)
    {
        return tem_acesso(array(ADMINISTRADOR,COORDENADOR,COORDENADOR_SQ,COORDENADOR_AREA,COORDENADOR_COACHING,PROFESSOR), $redirecionar);
    }

    /**
     * Grupo de acesso para edição de questão
     * 
     * @return bool
     */
    static public function editar_questao($questao_id, $redirecionar = null){

        $tem_acesso = tem_acesso(array(
                        ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR, PROFESSOR	
                    ), $redirecionar);

        if($questao_id){//Quando a questão foi marcada como repetida e inativada somente o administrador e o coordenador do SQ podem editar
            $CI = &get_instance();
	        $CI->load->model('questao_model');
			$questao = $CI->questao_model->get_by_id($questao_id);
			if($questao && $CI->questao_model->is_questao_repetida($questao_id)){
				$tem_acesso = tem_acesso(array(
                                ADMINISTRADOR, COORDENADOR_SQ	
                            ), $redirecionar);
			}
        }

        return $tem_acesso;
    }

    static public function limpar_formatacao($redirecionar = null)
    {
        return tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ, REVISOR, ATENDENTE), $redirecionar);
    }

    /**
     * Grupo de acesso para professor comentar questão
     * 
     * @return bool
     */
    static public function professor_pode_comentar($questao_id, $professor_id = null){

        if(is_null($professor_id)){
            $professor_id = get_current_user_id();
        }

        $CI = &get_instance();
        $CI->load->model('comentario_model');
        $professor_pode_comentar = $CI->comentario_model->professor_pode_comentar($questao_id, $professor_id);
		
		if($professor_pode_comentar){
            $CI->load->model('questao_model');
			$questao = $CI->questao_model->get_by_id($questao_id);
			if($questao && $CI->questao_model->is_questao_repetida($questao_id)){
				$professor_pode_comentar = tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ));
			}
		}

        return $professor_pode_comentar;
    }

    /**
     * Grupo de acesso para o relatório de Vendas
     * 
     * @return bool
     */
    
    static public function relatorio_de_vendas($redirecionar = null)
    {
        return tem_acesso([ADMINISTRADOR, COORDENADOR, PROFESSOR], $redirecionar);
    }
    
    
    /**
     * Grupo de acesso para WP-Admin > Landing Pages
     * 
     * @return bool
     */
    
    static public function wp_admin_landing_pages()
    {
        return tem_acesso([ADMINISTRADOR, COORDENADOR, ATENDENTE, APOIO]);
    }
    
}