<?php
/**
 * Helper para organizar rotinas relacionadas ao relatório de pagamento de professor premium
 *
 * @package	KCore/Helpers
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since L1
 */

use Box\Spout\Common\Type;
use Box\Spout\Writer\WriterFactory;

KLoader::helper("RelatorioHelper");
KLoader::model("PremiumModel");

class RelatorioPagamentoProfessorPremiumHelper extends RelatorioHelper {
    
    /**
     * Retorna a tabela com os dados do relatório
     *
     * @since K5
     *
     * @param array $lines Linhas do relatório
     * @param bool $is_excel Indica se é para exportação para Excel ou não
     * @param int $max_aulas Máximo de colunas de aulas que serão exibidas no relatório
     *
     * @return string HTML da tabela do relatório
     */ 
    
    public static function get_dados($ano_mes, $professor_id = null, $detalhes = true, $id = null)
    {
        // prepara os dados de início e fim do período a ser consultado
        $ano_mes_f = substr($ano_mes, 0, 4) . "-" . substr($ano_mes, 4, 2) . "-01";
        $inicio = date("Y-m-01 00:00:00", strtotime($ano_mes_f));
        $fim = date("Y-m-t 23:59:59", strtotime($ano_mes_f));

         // prepara os dados dos pedidos de produtos premiums
        $pedidos_premiums = [];
        $retorno_pedidos = PremiumModel::listar_pedidos_premium_agrupados_por_produtos($inicio, $fim, $id);
        
        // recupera as configurações do período
        $config = PremiumModel::get_config_ativa_por_data($ano_mes_f);
        $cota_sq_percentual = $config ? $config->ppc_cota_sq / 100 : 0.2;
        $cota_professor_percentual = $config ? $config->ppc_cota_professor / 100 : 0.8;
        $minimo_pgto = $config ? $config->ppc_minimo_pgto : 300;
        
        foreach ($retorno_pedidos as $item) {
            $pedidos_premiums[$item->ven_curso_id] = $item;
        }




        if($retorno_pedidos) {
            // prepara os dados dos professores
            $retorno = PremiumModel::listar_premium_item_professores_por_periodo($ano_mes, $professor_id);
            
            $info_professores = PremiumModel::listar_info_professores_por_periodo($ano_mes);
            
            $professores_premiums = [];
            $professores_ids = [];
                    
            foreach ($retorno as $item) {
                
                if(!isset($professores_premiums[$item->display_name]['produtos'][$item->pro_id])) {
                    
                    $professores_ids[] = $item->user_id;
                    
                    $cota = $info_professores[$item->pro_id][$item->user_id]['percentual'] / 100;
                    $total_vendas = $pedidos_premiums[$item->pro_id]->total_valor_venda;
                    
                    if($total_vendas <= 0) continue;
                    
                    $percentual_lucro = get_lucro_por_data($item->user_id, $ano_mes_f) ?: PARTICIPACAO_PROFESSOR_DEFAULT;
                    
                    $total_descontos = $pedidos_premiums[$item->pro_id]->total_desconto;
                    $total_reembolsado = $pedidos_premiums[$item->pro_id]->total_reembolso;
                    $total_pago = $total_vendas - $total_descontos - $total_reembolsado;
                    $total_imposto = $total_pago > 0 ? $pedidos_premiums[$item->pro_id]->total_imposto : 0;
                    $total_pagseguro = $total_pago > 0 ? $pedidos_premiums[$item->pro_id]->total_pagseguro : 0;
                    $total_afiliados = $total_pago > 0 ? $pedidos_premiums[$item->pro_id]->total_folha_dirigida : 0;
                    $total_liquido = $total_pago - $total_pagseguro - $total_imposto - $total_afiliados;
                    $cota_professor = $cota_professor_percentual * $total_liquido;
                    $valor_minha_cota = $cota * $cota_professor;
                    $total_professor = $cota * $cota_professor * ($percentual_lucro / 100);
                    
                    if($detalhes) {

                        $linha = [
                            'id' => $item->pro_id,
                            'produto' => $item->post_title,
                            'periodo' => periodo_mes_ano($ano_mes),
                            'total_vendas' => moeda($total_vendas),
                            'total_descontos' => moeda($total_descontos),
                            'total_reembolsado' => moeda($total_reembolsado),
                            'total_pago' => moeda($total_pago),
                            'total_imposto' => moeda($total_imposto),
                            'total_pagseguro' => moeda($total_pagseguro),
                            'total_afiliados' => moeda($total_afiliados),
                            'total_liquido' => moeda($total_liquido),
                            'cota_sq' => moeda($cota_sq_percentual * $total_liquido),
                            'cota_professor' => moeda($cota_professor),
                            'minha_cota' => porcentagem($cota * 100, 4),
                            'valor_minha_cota' => moeda($valor_minha_cota, 4),
                            'percentual_lucro' => porcentagem($percentual_lucro),
                            'total_professor' => moeda($total_professor, true, 4)
                        ];
                        
                        $professores_premiums[$item->display_name]['produtos'][$item->pro_id] = $linha;
                    }

                    $professores_premiums[$item->display_name]['total'] =  isset($professores_premiums[$item->display_name]['total']) ?
                            $professores_premiums[$item->display_name]['total'] + $total_professor : $total_professor;
                    $professores_premiums[$item->display_name]['subtotal'] =  isset($professores_premiums[$item->display_name]['subtotal']) ?
                            $professores_premiums[$item->display_name]['subtotal'] + $valor_minha_cota : $valor_minha_cota;
                    $professores_premiums[$item->display_name]['nome'] = $item->display_name;
                    $professores_premiums[$item->display_name]['professor_id'] = $item->user_id;
                    $professores_premiums[$item->display_name]['periodo'] = periodo_mes_ano($ano_mes);
                }
             
            }

            $mes_ano_anterior = date('Ym', strtotime("{$ano_mes_f} -1 month"));
            
            // Adiciona professores com acumulado no mês anterior
            $professores_acumulados = PremiumModel::listar_professores_com_acumulado_no_mes($mes_ano_anterior);
            foreach ($professores_acumulados as $professor_acumulado) {
                if(!in_array($professor_acumulado->ID, $professores_ids)) {
                    $professores_premiums[$professor_acumulado->display_name] = [
                        'produtos' => [],
                        'total' => 0,
                        'subtotal' => 0,
                        'nome' => $professor_acumulado->display_name,
                        'professor_id' => $professor_acumulado->ID,
                        'periodo' => periodo_mes_ano($ano_mes)
                    ];
                }
            }
            
            foreach ($professores_premiums as $professor_nome => &$professor_premium) {
                $acumulado = PremiumModel::get_acumulado($mes_ano_anterior, $professor_premium['professor_id']);
                $acumulado_nao_pago = $acumulado ? $acumulado->ppa_valor_acumulado : 0;
                $novo_total = $professor_premium['total'] + $acumulado_nao_pago;
                
                $acumulado_proximo_mes = $novo_total < $minimo_pgto ? $novo_total : 0;
                $valor_a_receber = $novo_total < $minimo_pgto ? 0 : $novo_total;

                $professor_premium['acumulado_nao_pago'] = $acumulado_nao_pago;
                $professor_premium['acumulado_proximo_mes'] = $acumulado_proximo_mes;
                $professor_premium['valor_a_receber'] = $valor_a_receber;
            }
            
        }
        
        ksort($professores_premiums);

        return $professores_premiums;
    }
    
    /**
     * Recupera os somatórios do relatório
     *
     * @since L1
     *
     * @param array $premiums Linhas do relatório
     *
     * @return array Array com os somatórios
     */
    
    public static function get_somatorios($premiums)
    {
        $soma = [
            'total' => 0,
            'subtotal' => 0,
            'acumulado_nao_pago' => 0,
            'acumulado_proximo_mes' => 0,
            'valor_a_receber' => 0,
            'periodo' => ''
        ];
        
        foreach ($premiums as $item) {
            $soma['total'] += $item['total'];
            $soma['acumulado_nao_pago'] += $item['acumulado_nao_pago'];
            $soma['acumulado_proximo_mes'] += $item['acumulado_proximo_mes'];
            $soma['valor_a_receber'] += $item['valor_a_receber'];
            $soma['periodo'] = $item['periodo'];
        }
        
        return $soma;
    }
    
    /**
     * Exporta um relatório de pagamento de professor premium para XLSX e grava em arquivo
     *
     * @since L1
     *
     * @param array $premiums Linhas a serem exportadas
     * @param array $somatorios Somatórios das colunas dos professores
     *
     * @return string Endereço relativo do arquivo (ex: /wp-content/temp/relatorio-pagamento-professor-premium-99999999.xlsx
     */
    
    public static function exportar_xls($premiums, $somatorios)
    {
        //         KLoader::helper("NumeroHelper");
        
        $arquivo = '/wp-content/temp/relatorio-pagamento-professor-premium-' . time() . '.xlsx';
        
        $writer = WriterFactory::create(Type::XLSX);
        $writer->openToBrowser($_SERVER['DOCUMENT_ROOT'] . $arquivo);
        
        $row = ["Professor", "Produto", "ID do Produto", "Período", "Total de Vendas", "Total Descontos", "Total Reembolso", "Total Pago", "Total Gateway",
            "Total Afiliados", "Total Imposto", "Total Líquido", "Cota SQ", "Cota Professor", "Minha Cota", "Valor Minha Cota",
            "% Lucro Professor", "Total Professor", "Acumulado não pago", "Valor Acumulado próximo mês", "Valor a receber"];
        $writer->addRow($row);
        
        
        foreach($premiums as $item) {
            $row = [$item['nome'], "Total", "", $item['periodo'], "", "", "", "", "", "", "", "", "", "", "", moeda($item['subtotal'], true, 4), "", moeda($item['total'], true, 4),
                moeda($item['acumulado_nao_pago'], true, 4), moeda($item['acumulado_proximo_mes'], true, 4), moeda($item['valor_a_receber'], true, 4)
            ];
            
            $writer->addRow($row);
            
            if($produtos = $item['produtos']) {
                foreach ($produtos as $linha) {
                    $row = [
                        $item["nome"],
                        $linha['produto'],
                        $linha['id'],
                        $linha['periodo'],
                        $linha['total_vendas'],
                        $linha['total_descontos'],
                        $linha['total_reembolsado'],
                        $linha['total_pago'],
                        $linha['total_pagseguro'],
                        $linha['total_afiliados'],
                        $linha['total_imposto'],
                        $linha['total_liquido'],
                        $linha['cota_sq'],
                        $linha['cota_professor'],
                        $linha['minha_cota'],
                        $linha['valor_minha_cota'],
                        $linha['percentual_lucro'],
                        $linha['total_professor'],
                        '',
                        '',
                        ''
                    ];
                    
                    $writer->addRow($row);
                }
            }
        }
        
        // Somatório geral
        $row = [
            "",
            "Total Geral",
            "",
            $somatorios['periodo'],
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            moeda($somatorios['subtotal'], true, 4),
            "",
            moeda($somatorios['total'], true, 4),
            moeda($somatorios['acumulado_nao_pago'], true, 4),
            moeda($somatorios['acumulado_proximo_mes'], true, 4),
            moeda($somatorios['valor_a_receber'], true, 4)
        ];
        
        $writer->addRow($row);
        
        $writer->close();
        
        exit;
    }
}