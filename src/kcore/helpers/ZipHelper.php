<?php
/**
 * Helper para organizar rotinas relacionadas ao zip
 * 
 * @package	KCore/Helpers
 *
 * @author 	André Gervásio
 *
 * @since	L1
 */ 

class ZipHelper {

    static function download_all_pdf_produto_zip($arquivos, $titulo)
    {
        // criar pasta temporária se não existir
        $temp_folder = $_SERVER['DOCUMENT_ROOT'] . '/wp-content/temp/';
        if (!file_exists($temp_folder)) {
            mkdir($temp_folder, 0777, true);
        }
        
        $dir =  '/wp-content/temp/';
    	$filename = $dir . $titulo . '.zip';
    	$path = $_SERVER['DOCUMENT_ROOT'] . $filename;
    	
    	log_zip("INFO", "Iniciando zip: " . $path);
    	if(self::create_zip($arquivos, $path, true) ){
    	    log_zip("INFO", "Retornando zip: " . $filename);
    	    return $filename;
    	}
    	
    }
    
    
    static function create_zip($files = array(), $destination = '',$overwrite = false) 
    {
        if(file_exists($destination) && !$overwrite) { return false; }
    
    	$valid_files = array();
    
    	if(is_array($files)) {
    
    		foreach($files as $file) {
    
    			if(file_exists($file)) {
    				$valid_files[] = $file;
    			}
    		}
    	}
    
    	if(count($valid_files)) {
    	    
    		$zip = new ZipArchive();
    		
    		$res = $zip->open($destination, ZIPARCHIVE::CREATE | ZIPARCHIVE::OVERWRITE);
    		if($res !== TRUE) {
    		    log_zip("ERROR", "Erro abrindo zip: " . $destination);
    			return false;
    		}
    
    		foreach($valid_files as $file) {
    			$zip->addFile($file,basename($file));
    		}
    		
    		log_zip("INFO", "Zip criado: " . $destination);
    		$zip->close();
    
    		//check to make sure the file exists
    		return file_exists($destination);
    	}
    	else
    	{
    	    log_zip("INFO", "Nao existe arquivo valido: " . $destination);
    		return false;
    	}
    }

}