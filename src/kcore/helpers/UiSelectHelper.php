<?php
/**
 * Helper para organizar rotinas relacionadas às interfaces de selects
 * 
 * @package	KCore/Helpers
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	L1
 */ 

class UiSelectHelper {

	/**
	 * Retorna uma tag select
	 * 
	 * @since L1
	 * 
	 * @param string $name Atributo name
	 * @param array $items Array, no formato key => value, dos valores do combo
	 * @param string $selected Valor selecionado
	 * @param bool $multiple Indica se o select permite seleção múltipla ou não
	 * @param array $attr Atributos da tag
	 * 
	 * @return string HTML do select
	 */ 

    public static function get_select_html($name, $items, $selected = null, $multiple = true, $attr = null)
	{
        // carrega a view com o HTML correspondente
		return KLoader::view("ui/select", ['name' => $name, 'items' => $items, 'selected' => $selected, 'multiple' => $multiple, 'attr' => $attr]);
	}
	
	/**
	 * Retorna um array com opções para selects
	 *
	 * @since L1
	 *
	 * @param array $lista Input de dados
	 * @param string $key Índice a ser utilizado como chave
	 * @param string $value Índice a ser utilizado como valor
	 * @param bool $is_lista_array Flag para indicar se input é array ou objeto
	 *
	 * @return array Array com combo options
	 */ 

	public static function get_combo_options($lista, $key, $value, $is_lista_array = true, $default = null)
	{
	    $options = [];
	    
	    if($default) {
	        $options[0] = $default;
	    }
	    
	    foreach ($lista as $item) {
	        
	        if($is_lista_array) {
	            $o_key = $item[$key];
	            $o_value = $item[$value];  
	        }
	        else {
	            $o_key = $item->$key;
	            $o_value = $item->$value;
	        }
	        
	        $options[$o_key] = $o_value;
	    }
	    
	    return $options;
	}
}	