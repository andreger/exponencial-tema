<?php
/**
 * Helper para organizar rotinas relacionadas à Seo HTPS
 *
 * @package	KCore/Helpers
 *
 * @author 	João Paulo Ferreira <jpaulofms@gmail.com>
 *
 * @since	L1
 */

class SeoHTTPSHelper {
    
    /**
     * Array contento os tipos de conteúdo Seo HTTPS
     * 
     * @since L1
     * 
     * @var array
     */
    private static $tipos = [
        'post' => 'Blogs',
        'page' => 'Páginas',
        'product' => 'Produtos',
        'concurso' => 'Concursos',
        'forum' => 'Fóruns',
        'topic' => 'Tópicos',
        'reply' => 'Respostas',
        'slide' => 'Slides',
        'questions' => 'FAQ',
        'revisions' => 'Histórico',
        'usermeta' => 'Usermeta',
        'questao' => 'Questão',
    ];
    
    /**
     * Retorna um nome amigável para o tipo de conteúdo Seo HTTPS
     *
     * @since L1
     *
     * @param string $tipo Tipo do conteúdo
     *
     * @return string nome amigável para o tipo do conteúdo
     */
    
    public static function get_tipo_nome($tipo)
    {      
        return SeoHTTPSHelper::$tipos[$tipo];
    }
}
    