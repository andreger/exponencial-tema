<?php
/**
 * Shortcodes relacionados aos botoes
 * 
 * @package	KCore/Shortcodes
 *
 * @author 	Fábio Siqueira
 *
 * @since	K5 
 */ 

/**
 * Exibe listagem dos botoes
 *
 * @return string HTML do fragmento
 */

function kshort_botoes()
{
	return KLoader::view("main/botao");
}

 add_shortcode('botao', 'kshort_botoes');