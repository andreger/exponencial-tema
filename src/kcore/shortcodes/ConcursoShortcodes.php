<?php
/**
 * Shortcodes relacionados aos concursos
 * 
 * @package	KCore/Shortcodes
 *
 * @author 	André Gervásio <andre@keydea.com.br>
 *
 * @since	K4 
 */ 

/**
 * Exibe listagem dos concursos mais vendidos na Home
 *
 * @return string HTML do fragmento
 */

function kshort_home_concursos($status = NULL, $offset = 0)
{
	KLoader::model("ConcursoModel");
	KLoader::helper("ConcursoHelper");
	KLoader::helper('UiLabelHelper');
	KLoader::helper('UrlHelper');

	$data['concursos'] = ConcursoModel::listar_mais_vendidos(6, $offset, $status);

	return KLoader::view("home/concursos", $data, TRUE);
}

 add_shortcode('home_concursos', 'kshort_home_concursos');