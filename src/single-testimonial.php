<?php get_header(); ?>
<section id="online_courses">
    <div class="section_head">
        <div class="row">
            <div class="section_head_img">
            	<img src="icon/artigos.png" alt="" class="alignnone">
          </div>
          <div class="header_text">Deplomentas</div>
        </div>
    </div>
</section>

	  <?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>	

<div class="section_content secbg_2">

	<div class="row">
          <div class="fleft"> 
            	<img src="icon/voltar_depoimentos.png" alt="" class="alignnone">
                </div>
          <div class="header_text fleft" style="color:#FFF; font-size:24px; line-height:50px; margin-left:10px; font-weight:bold;"><?php the_title(); ?></div>
   </div>
     
    <div class="section_content">

      <div class="row testimonials">
      	<div class="threecol fleft">
        	<div class="pic_outter">
            	<?php
if( has_post_thumbnail() ) { ?>

<?php the_post_thumbnail();?>

<?php } else { ?>
<img src="<?php echo get_template_directory_uri(); ?>/src/images/default_avatar.jpg" />
<?php } ?>
            </div>
        </div>
        <div class="ninecol fright">
       	  <div class="pre_text">
            <?php the_content();?>
            </div>
            
        </div>
        <div class="clear"></div>
      </div>
      
      
      
      <?php endwhile; ?>
      
      
      
      
      
      
      <div class="sectionlargegap"></div>
    </div>
    
    
    
    
    
</div>

    
    
    
<?php get_footer(); ?>