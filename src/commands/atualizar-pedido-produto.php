<?php
include_once $_SERVER['DOCUMENT_ROOT'] .'/wp-load.php';

KLoader::model("PedidoModel");

$pedido_id = $_GET['o'] ?? null;

if(!$pedido_id) {
    echo "Necessário passar o id do pedido";
}

PedidoModel::alterar_pedido_status_usuario($pedido_id);