<?php

use Exponencial\Core\Helpers\Doctrine\EntityManagerFactory;
use Exponencial\Core\Models\Blog;

require_once __DIR__ . '/../vendor/autoload.php';

$entityManagerFactory = new EntityManagerFactory();
$entityManager = $entityManagerFactory->getEntityManager();

$blog = $entityManager->find(Blog::class, 4041);

var_dump($blog);