<?php
/*
Template Name: Blog Post Category
*/

get_header(); 

global $post;

KLoader::model("BlogModel");
KLoader::helper("BlogHelper");

$url_a = parse_url(get_url_atual());
$path = $url_a['path'];

$blog_tipo_id = BlogHelper::get_blog_tipo_from_url_path($path);

$limit = 20;
$offset = isset($_GET['o']) ? $_GET['o'] : 0;
$blogs = BlogModel::listar($blog_tipo_id, $limit, $offset);
$tem_proxima = BlogModel::listar($blog_tipo_id, 1, $limit + $offset + 1);
?>

<?php KLoader::view("blogs/navbar") ?>

<div class="container pt-5">
    <div class="row pt-4" id="postlist">
    	<div class="col-12">
        	<h1><?= get_h1($post->ID) ?></h1>
        </div>
    	
        <ul class="p-0 list-type-none">
    		<?php foreach ( $blogs as $blog ) : ?> 
    			<?php
            		$url = UrlHelper::get_blog_post_url($blog);
            	?>
    			<li class="b_listing d-block p-2 text-dark border-bottom border-dark">
    				<span>
                        <a class="t-d-none text-blue font-20" href="<?= $url ?>"><?= $blog->post_title ?></a>
                    </span>
                    <span class="text-blue font-10 d-block mb-2 mt-1">
    					<?= converter_para_ddmmyyyy($blog->post_date) ?>, por
        				<?php if($colaborador = ColaboradorModel::get_by_id($blog->user_id)) : ?>
    						<a class="font-10" href="<?= UrlHelper::get_professor_url($colaborador->col_slug) ?>"><strong><?= $blog->display_name ?></strong></a>
    					<?php else : ?>
    						<span class="font-10"><strong><?= $blog->display_name ?></strong></span>
    					<?php endif ?>
                    </span>
                    <span class="d-block"><?= $blog->blo_resumo ?></span>
                    <a href="<?= $url ?>">
                        <button class="mt-2 mb-1 btn u-btn-primary font-10">Leia mais</button>
                    </a>
                    <div class="clear"></div>
    			</li>
    		<?php endforeach; ?>
    	</ul>
    </div>

    <?php if($tem_proxima) : ?>
    <div class="row">
        <a class="ml-auto mr-auto mt-1" href="<?= $path ?>?o=<?= $offset + $limit ?>">
        	 <button class="btn btn-lg u-btn-blue mt-3 mb-5 font-10">Mostrar mais</button>
        </a>
    </div>
    <?php endif ?>
</div>

<div class="sectionlargegap"></div>

<?php get_footer();?>