<?php if($usuario_id = get_current_user_id()) : ?>

	<?php if(exibe_assinatura_bar($usuario_id)) : ?>

		<?php $validade = get_validade_assinatura_sq($usuario_id);	?>
		
		<div class="ass-b assinatura-bar toolbar text-center text-md-right">
					<?php if($validade) : ?>
						<?php if(is_assinatura_sq_expirada($usuario_id)) : ?>
							<span class="assinatura-bar-msg">Assinatura do Sistema de Questões - Expirado </span>
							<a href="/sistema-de-questoes#planos" class="img-bar"><?= get_tema_image_tag("renovar.png") ?></a>
						<?php else : ?>
							<span class="assinatura-bar-msg">Assinatura do Sistema de Questões - Valido até: <?= converter_para_ddmmyyyy($validade) ?></span>
						<?php endif; ?>
					<?php else : ?>
						<span class="assinatura-bar-msg">Assinatura do Sistema de Questões - Assine Já </span>
						<a href="/sistema-de-questoes#planos" class="img-bar"><?= get_tema_image_tag("planos-com-desconto.png") ?></a>
					<?php endif ?>
				<span class="assinatura-close"><a href="#"> X </a></span>			
		</div>
		<div class="fixed-hidden assinatura-height"></div>

<script>
		jQuery(function() {
			jQuery('.anchor').addClass('anchor-bar')
			jQuery('.assinatura-bar a').click(function() {
				jQuery('.assinatura-bar').hide();
				jQuery.ajax("<?= get_ajax_url('assinatura_sq_esconder.php') ?>");
				jQuery(".hrtb").addClass("header-topbar").removeClass("header-topbar-ass-bar");
 				jQuery(".n-b-i").addClass("navbar-index").removeClass("navbar-index-ass-bar");
				jQuery(".p-absolute").addClass("logo-index").removeClass("logo-index-ass-bar");
				jQuery(".fixed-hidden").removeClass("assinatura-height");
				jQuery(".s-b-c").addClass("secao-barra-clara").removeClass("secao-barra-clara-ass-bar");
				jQuery(".b-t-b").addClass("b-top").removeClass("b-top-ass-bar");
				jQuery(".featured-content").removeClass("featured-content-ass-bar");
			});
		});
</script>

	<?php endif; ?>

<?php endif; ?>