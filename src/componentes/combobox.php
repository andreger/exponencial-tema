<select class="custom-select" id="<?= $nome ?>" name="<?= $nome ?>">
	<?php foreach ($opcoes as $key => $value) : ?>
		<option value="<?= $key ?>" <?= $selecionado == $key ? "selected" : "" ?>><?= $value ?></option>
	<?php endforeach ?>
</select>