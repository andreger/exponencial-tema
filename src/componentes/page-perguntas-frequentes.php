<div id="accordion-1" class="container coaching-concurso-2 mt-5 col-12 col-xl-10">
  <div class="card mb-0">
    <div class="card-header" id="headingUm">
      <h5 class="mb-0">
        <button class="btn btn-link text-dark" data-toggle="collapse" data-target="#collapseUm" aria-expanded="true" aria-controls="collapseUm">
          <ul class="d-none d-lg-block"><li><h4>O Coaching do Exponencial indica materiais de outros cursos ou só materias do próprio Exponencial?</h4></li></ul>
          <ul class="d-none d-md-block d-lg-none"><li><h5 class="font-11">O Coaching do Exponencial indica materiais de outros cursos ou só materias do próprio Exponencial?</h5></li></ul>
          <ul class="d-block d-md-none text-justify font-11"><li>O Coaching do Exponencial indica materiais<br/> de outros cursos ou só materias do próprio<br/> Exponencial?</li></ul>
        </button>
      </h5>
    </div>

    <div id="collapseUm" class="collapse show" aria-labelledby="headingUm" data-parent="#accordion-1">
      <div class="card-body pl-5 pl-md-3 pt-0 text-justify font-10">
      	<p>O objetivo principal do serviço de Coaching do Exponencial é se adequar ao perfil de cada aluno, provendo-lhe meios de ser o mais eficiente possível em seus estudos.</p>
<p>E apesar de fazermos o maior esforço possível para que nossos materiais sejam ricos didaticamente, reconhecemos que certos alunos não se adaptam bem a alguns deles. Ou mesmo, há alunos que já vêm estudando e investiram em bons materiais de outros cursos, sendo inviável o investimento em novos cursos.</p>
<p>No início de seus estudos no Coaching Exponencial, o consultor irá dedicar uma parte de seu tempo discutindo estas questões com o aluno, analisando prós e contras dos materiais disponíveis e oferecendo de 2 a 4 alternativas de cursos para cada matéria, sempre buscando dar opções de tipos de materiais diferentes: PDF, Vídeo, Presencial, Questões, etc.</p>
<p>Pode haver casos de alunos que preferem manter-se estudando em materiais não recomendados pelo Exponencial. Nestes casos, nosso acompanhamento será mais firme em relação a suas metas e resultados, de forma a intervirmos o mais cedo possível se o aluno não estiver obtendo a performance desejada por conta desta decisão.</p> 
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header" id="headingDois">
      <h5 class="mb-0">
        <button class="btn btn-link text-dark" data-toggle="collapse" data-target="#collapseDois" aria-expanded="true" aria-controls="collapseDois">
          <ul class="d-none d-lg-block"><li><h4>Como funciona o Coaching oferencido pelo Exponencial?</h4></li></ul>
          <ul class="d-none d-md-block d-lg-none"><li><h5 class="font-11">Como funciona o Coaching oferencido pelo Exponencial?</h5></li></ul>
          <ul class="d-block d-md-none text-justify font-11"><li>Como funciona o Coaching oferencido<br/> pelo Exponencial?</li></ul>
        </button>
      </h5>
    </div>

    <div id="collapseDois" class="collapse" aria-labelledby="headingDois" data-parent="#accordion-1">
      <div class="card-body pl-5 pl-md-3 text-justify pt-0 font-10">
      	<p>Você pode obter estas informações de várias maneiras.</p>
<p>Inicialmente, recomendamos dar uma lida rápida em nosso site, na página específica sobre o Coaching Exponencial. E também na Descrição do Serviço de Coaching, onde detalhamos vários aspectos da forma como prestamos nossos serviços. Ainda, esta página de Perguntas Frequentes traz vários pontos comumente questionados por nossos alunos, vale a leitura atenta.</p>
<p>Ainda, veja nosso <a class="t-d-none font-weight-bold" href="https://www.youtube.com/channel/UCr9rg5WOPmXvZgOfBl-HEuw">Canal do Youtube</a>: temos vários vídeos explicando nossos serviços.</p>
<p>Por fim, se ainda sentir necessidade de mais informações, você pode  <a class="t-d-none font-weight-bold" href="/fale-conosco">Agendar uma Entrevista Grátis</a> com um dos nossos consultores para que ele lhe passe mais detalhes do serviço. É um bate papo bem rápido e objetivo, de cerca de 30 minutos, para esclarecer os pontos chave da forma como o serviço de Coaching pode ajudar você!</p>     
      </div>
    </div>
  </div>
   <div class="card">
    <div class="card-header" id="headingTres">
     <h5 class="mb-0">
        <button class="btn btn-link text-dark" data-toggle="collapse" data-target="#collapseTres" aria-expanded="true" aria-controls="collapseTres">
          <ul class="d-none d-lg-block"><li><h4>O Coaching pode realmente me ajudar?</h4></li></ul>
          <ul class="d-none d-md-block d-lg-none"><li><h5 class="font-11">O Coaching pode realmente me ajudar?</h5></li></ul>
          <ul class="d-block d-md-none font-11 text-justify"><li>O Coaching pode realmente me ajudar?</li></ul>
        </button>
      </h5>
    </div>
    <div id="collapseTres" class="collapse" aria-labelledby="headingTres" data-parent="#accordion-1">
      <div class="card-body pl-5 pl-md-3 text-justify pt-0 font-10">
      <p>Esta é uma pergunta bem comum, feita por vários alunos. Na prática, muitos alunos são aprovados em concursos públicos sem precisar de qualquer ajuda de Coaching. A maioria, no entanto, perde um tempo excessivo até encontrar a melhor forma de estudar para o seu objetivo. E esta é apenas uma das grandes vantagens de contratar o serviço de Coaching.</p>
<p>Veja o que você poderá obter contratando o serviço de Coaching:</p>
<ul class="coaching-concurso-3"><li>Metas semanais de estudo</li>
<li>Dicas e técnicas para seu estudo ter maior rendimento</li>
<li>Apoio/Suporte na tomada de decisões ao longo de seu estudo</li>
<li>Indicação de materiais de qualidade para seu estudo</li>
<li>Sistema Online com mais de 300 mil questões GRÁTIS para acompanhar seu aprendizado</li>
<li>Simulados Focados no seu objetivo, todos comentados por professores</li>
<li>Acompanhamento individualizado de seu desempenho</li>
<li>Planejamento e estratégia de estudo, selecionando melhor estratégia para cada situação</li></ul>
<p>Naturalmente, boa parte deste serviço você pode fazer por sua conta. Muitos conseguem com sucesso, mas infelizmente, a maioria se vê perdido ou demora muito para compreender a dinâmica de como fazê-lo de forma adequada. E neste ponto o Coaching para Concursos irá poderá ajudar você.</p>
<p><a class="t-d-none font-weight-bold" href="/src/coachinghing">Leia mais a respeito aqui</a>, para poder tomar uma decisão adequada.</p>
<p>E aproveite para ver <a class="t-d-none font-weight-bold" href="https://www.youtube.com/watch?v=WWOD2aVTg18">este vídeo</a> que explica melhor a ideia do Coaching.</p>
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header" id="headingQuatro">
      <h5 class="mb-0">
        <button class="btn btn-link text-dark" data-toggle="collapse" data-target="#collapseQuatro" aria-expanded="true" aria-controls="collapseQuatro">
          <ul class="d-none d-lg-block"><li><h4>Por que o Exponencial oferece coaching 100% personalizado?</h4></li></ul>
          <ul class="d-none d-md-block d-lg-none"><li><h5 class="font-11">Por que o Exponencial oferece coaching 100% personalizado?</h5></li></ul>
          <ul class="d-block d-md-none text-justify font-11"><li>Por que o Exponencial oferece coaching<br/> 100% personalizado?</li></ul>
        </button>
      </h5>
    </div>

    <div id="collapseQuatro" class="collapse" aria-labelledby="headingQuatro" data-parent="#accordion-1">
      <div class="card-body pl-5 pl-md-3 text-justify pt-0 font-10">
      	<p>O trabalho feito pelo Exponencial visa identificar as forças e fraquezas de cada aluno, tanto do ponto de vista cognitivo quanto do seu histórico de estudo, visando orientá-lo em relação ao tipo de material mais adequado, melhores métodos de estudo, organização e planejamento.</p>
<p>Além disto, cada plano de estudos no Exponencial é TOTALMENTE INDIVIDUALIZADO, buscando atender as demandas de cada pessoa para preparar-se adequadamente para o concurso desejado e adequando-se continuamente conforme o aluno progride no estudo das matérias.</p>
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header" id="headingCinco">
     <h5 class="mb-0">
        <button class="btn btn-link text-dark" data-toggle="collapse" data-target="#collapseCinco" aria-expanded="true" aria-controls="collapseCinco">
          <ul class="d-none d-lg-block"><li><h4>Como faço para participar do Coaching?</h4></li></ul>
          <ul class="d-none d-md-block d-lg-none"><li><h5 class="font-11">Como faço para participar do Coaching?</h5></li></ul>
          <ul class="d-block d-md-none text-justify font-11"><li>Como faço para participar do Coaching?</li></ul>
        </button>
      </h5>
    </div>

    <div id="collapseCinco" class="collapse" aria-labelledby="headingCinco" data-parent="#accordion-1">
      <div class="card-body pl-5 pl-md-3 text-justify pt-0 font-10">
      	<p>Nossas turmas quando abertas são divulgadas aqui no nosso site e na nossa página do <a class="t-d-none font-weight-bold" href="https://www.facebook.com/exponencialconcursos">Facebook</a>. E enviamos informação por e-mail para os alunos cadastrados em nosso site.</p>
<p>Para fazer sua inscrição, basta <a class="t-d-none font-weight-bold" href="/painel-coaching/inscricao">clicar aqui!</a></p>
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header" id="headingSevenn">
      <h5 class="mb-0">
        <button class="btn btn-link text-dark" data-toggle="collapse" data-target="#collapseSevenn" aria-expanded="true" aria-controls="collapseSevenn">
          <ul class="d-none d-lg-block"><li><h4>Qual duração do Coaching?</h4></li></ul>
          <ul class="d-none d-md-block d-lg-none"><li><h5 class="font-11">Qual duração do Coaching?</h5></li></ul>
          <ul class="d-block d-md-none text-justify font-11"><li>Qual duração do Coaching?</li></ul>
        </button>
      </h5>
    </div>

    <div id="collapseSevenn" class="collapse" aria-labelledby="headingSevenn" data-parent="#accordion-1">
      <div class="card-body pl-5 pl-md-3 text-justify pt-0 font-10">
      	<p>O aluno poderá usufruir do serviço durante o período em que estiver em dia com o pagamento do serviço. Alguns planos podem não ter data limite, sendo de uso contínuo. Outros planos poderão ter prazo de duração limitada, sendo definido no anúncio do serviço.</p>
<p>O prazo mínimo de permanência no coaching é de 1 mês. O aluno poderá renovar tanto mensalmente como trimestralmente. Renovações trimestrais têm a vantagem de oferecer descontos significativos para os alunos. Alguns alunos julgam proveitoso passar todo o seu tempo de estudo com o acompanhamento de seu consultor, enquanto outros, após adquirirem a prática de estudo, organização e técnicas, continuam o estudo por conta própria, até sua aprovação.</p>
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header" id="headingEightt">
      <h5 class="mb-0">
        <button class="btn btn-link text-dark" data-toggle="collapse" data-target="#collapseEightt" aria-expanded="true" aria-controls="collapseEightt">
          <ul class="d-none d-lg-block"><li><h4>Qual o custo do Coaching?</h4></li></ul>
          <ul class="d-none d-md-block d-lg-none"><li><h5 class="font-11">Qual o custo do Coaching?</h5></li></ul>
          <ul class="d-block d-md-none text-justify font-11"><li>Qual o custo do Coaching?</li></ul>
        </button>
      </h5>
    </div>

    <div id="collapseEightt" class="collapse" aria-labelledby="headingEightt" data-parent="#accordion-1">
      <div class="card-body pl-5 pl-md-3 text-justify pt-0 font-10">
      	<p>Os preços estão definidos na oferta específica do coaching. O coaching com prazo limitado será anunciado desta forma, sendo o pagamento correspondente ao período a que se refere o produto. Oferecemos serviço de coaching para diversos tipos de concursos, como concursos da Área Fiscal, Área de Controle, Tribunais, Área Policial, Área Legislativa, Bancária, etc e verifique os preços atualizados em nossa página sobre os serviços de Coaching.</p> 
       
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header" id="headingNinee">
      <h5 class="mb-0">
        <button class="btn btn-link text-dark" data-toggle="collapse" data-target="#collapsegNinee" aria-expanded="true" aria-controls="collapsegNinee">
          <ul class="d-none d-lg-block"><li><h4>Como será montado meu ciclo de estudos?</h4></li></ul>
          <ul class="d-none d-md-block d-lg-none"><li><h5 class="font-11">Como será montado meu ciclo de estudos?</h5></li></ul>
          <ul class="d-block d-md-none text-justify font-11"><li>Como será montado meu ciclo de estudos?</li></ul>
        </button>
      </h5>
    </div>

    <div id="collapsegNinee" class="collapse" aria-labelledby="headinggNinee" data-parent="#accordion-1">
      <div class="card-body pl-5 pl-md-3 text-justify pt-0 font-10">
      	<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>      
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header" id="headingtenn">
      <h5 class="mb-0">
        <button class="btn btn-link text-dark" data-toggle="collapse" data-target="#collapsegtenn" aria-expanded="true" aria-controls="collapsegtenn">
          <ul class="d-none d-lg-block"><li><h4>É cobrada alguma taxa de sucesso?</h4></li></ul>
          <ul class="d-none d-md-block d-lg-none"><li><h5 class="font-11">É cobrada alguma taxa de sucesso?</h5></li></ul>
          <ul class="d-block d-md-none text-justify font-11"><li>É cobrada alguma taxa de sucesso?</li></ul>
        </button>
      </h5>
    </div>

    <div id="collapsegtenn" class="collapse" aria-labelledby="headinggtenn" data-parent="#accordion-1">
      <div class="card-body pl-5 pl-md-3 text-justify pt-0 font-10">
      	<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p> 
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header" id="headingonze">
      <h5 class="mb-0">
        <button class="btn btn-link text-dark" data-toggle="collapse" data-target="#collapsegonze" aria-expanded="true" aria-controls="collapsegonze">
          <ul class="d-none d-lg-block"><li><h4>O não pagamento da mensalidade acarreta a exclusão do Coaching?</h4></li></ul>
          <ul class="d-none d-md-block d-lg-none"><li><h5 class="font-11">O não pagamento da mensalidade acarreta a exclusão do Coaching?</h5></li></ul>
          <ul class="d-block d-md-none text-justify font-11"><li>O não pagamento da mensalidade acarreta<br/> a exclusão do Coaching?</li></ul>
        </button>
      </h5>
    </div>

    <div id="collapsegonze" class="collapse" aria-labelledby="headinggonze" data-parent="#accordion-1">
      <div class="card-body pl-5 pl-md-3 text-justify pt-0 font-10">
      	<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p> 
      </div>
    </div>
  </div>

</div> 
</div>
</div> 
</div> 
</div> 
</div> 
</div> 
</div> 
</div> 
</div>  