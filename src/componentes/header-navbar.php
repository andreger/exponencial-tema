<?php
global $woocommerce;

$navbar_index_user = 'navbar-index';
if($usuario_id = get_current_user_id()) {
    if(exibe_assinatura_bar($usuario_id)) {
        $navbar_index_user = 'navbar-index-ass-bar';
    }
}
?>

<nav class="p-0 navbar navbar-expand-xl navbar-dark bg-blue col-12 text-center <?= $navbar_index_user ?> n-b-i">

    <div class="scroll-header-icon d-none">
        <span class="header-botao-sq">
        <a class="mt-1 d-inline-block margin-item-topbar p-2 rounded" href="/questoes/main/resolver_questoes">
          <span class="comum-sq d-inline-block align-bottom"></span>
        </a>
      </span>
    </div>
    <div class="scroll-header-icon d-none">
        <?php if(is_user_logged_in()) : ?>

            <span class="header-botao">       
          <a class="t-d-none font-weight-bold text-green" href="/minha-conta">
            <i class="margin-item-topbar position-relative font-20 fa fa-user" style="top: 3px;"></i>            
          </a>              
        </span>

            <span class="header-botao">       
          <a href="/questoes/login/logout/?empty-cart=1"><span class="ml-2 mr-2">Sair</span></a>
            </span>

            <?php else : ?>

                <span class="header-botao">       
          <a class="margin-item-topbar" href="/cadastro-login/"><span>Entrar</span></a>
                </span>

                <?php endif; ?>
    </div>
    <div class="scroll-header-icon d-none">
        <span class="header-botao">
        <a class="margin-item-topbar" href="/fale-conosco">
          <span class="comum-fale-conosco d-inline-block align-bottom"></span>
        </a>
      </span>
    </div>
    <div class="scroll-header-icon d-none">
        <a class="margin-item-topbar d-block d-md-none t-d-none text-dark cart-button" href="/checkout">
            <span class="comum-carrinho-mobile d-inline-block align-bottom"></span>
            <span id="item-carrinho-scrol" class="text-white item-carrinho-nav font-8">
                <?= $woocommerce->cart->cart_contents_count == 0 ?
                        "0" :
                        sprintf(_n('%d', '%d', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);
                ?>
            </span>
      </a>
    </div>
  
    <div class="scroll-header-icon d-none">
		<span class="header-botao">       
      		<a href="/questoes/login/logout/?empty-cart=1"><span class="ml-2 mr-2">
      			<i class="margin-item-topbar position-relative text-warning font-20 fa fa-star" style="top: 3px;"></i>Assinaturas</span>
      		</a>
        </span>
    </div>

    <?php KLoader::view("header/menu/menu") ?>
</nav>