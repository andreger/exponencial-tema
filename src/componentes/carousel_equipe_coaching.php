<div class="row">
	<div id="owl-demo" class="owl-carousel owl-theme" style="margin:20px 0">
		<?php foreach (get_consultores_array(FALSE) as $consultor) : ?>				
		<div class='item'>
			<div class='' style="text-align:center">
			    <div><a href='<?php echo get_autor_url($consultor['id']) ?>'><?= get_avatar($consultor['id'], 212); ?></a></div>
				<div class="texto-azul coaching2-equipe-titulo"><a href='<?= get_autor_url($consultor['id']) ?>'><?= $consultor['nome_completo'] ?></a></div>
				<div class="coaching2-equipe-subtitulo"><?= get_user_meta($consultor['id'], "cargo_concurso", true) ?></div>
			</div>
		</div>	
		<?php endforeach; ?>
	</div>
</div>