<?php 
$filtro_combo = [
	'0' => "Todos",
	PESQUISA_TIPO_NAO_PACOTE => "Cursos",
    PESQUISA_TIPO_PACOTE => "Pacote"
];

$ordenacao_combo = [
	'0' => "Padrão",
	PESQUISA_MAIS_NOVOS => "Mais novos",
    PESQUISA_ORDEM_A_Z => "A - Z",
    PESQUISA_ORDEM_Z_A => "Z - A",
	PESQUISA_ORDEM_MENOR_PRECO => "Preço: menor para maior",
	PESQUISA_ORDEM_MAIOR_PRECO => "Preço: maior para menor"	
];

get_header();
echo get_pesquisa("Pesquisa", "pesquisa", $filtro_combo, $ordenacao_combo, false);
get_footer();
?>