<?php get_header (); ?>

<div id="sem-forum" class="container-fluid text-center pt-5">
	<div class="container mt-5 pt-5">
		<div class="mt-5 pt-4 col-12 col-lg-8 col-xl-7 ml-auto mr-auto">
		<h1 class="text-white">Você ainda não tem acesso a fóruns de nossos cursos.</h1>
	</div>
	<div class="mt-3">
		<a href="/cursos-online" class="btn btn-lg btn-primary">Adquira seu curso aqui
		</a>
	</div>
</div>

<?php get_footer(); ?>  
