<?php
	get_header();

	global $concurso;

	KLoader::view('concursos/info', ['concurso' => $concurso], false, "expo:concurso:{$concurso->ID}:info");
    KLoader::view('plugins/talkto/talkto');

	get_footer();
?>