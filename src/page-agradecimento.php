<?php get_header(); ?>
<div class="row pt-2 pt-md-5 ml-auto mr-auto">
	<div class="col-12 text-center">	
		<img width="250" src="/wp-content/themes/academy/images/cerebro-focado.png">
	</div>	
	<div class="col-12 text-center mt-4 mb-3">
		<div class="text-blue font-weight-bold font-20 mb-3"><h1>Inscrição efetuada com sucesso.</h1></div>
		<div class="text-blue font-18 mb-3">Temos milhares de materiais gratuitos, <a href="/cursos-gratis">confira aqui</a>!</div>
		<div class="text-blue font-weight-bold font-20">Confira o que temos pra te oferecer... e se não encontrar, FALE CONOSCO!</div>
	</div>
</div>
<div class="row mb-5 ml-auto mr-auto">
	<div class="mt-4 col-md-4 col-lg-2 text-center">
		<div><a href="/cursos-online"><img width="30" src="/wp-content/themes/academy/images/pdf.png"></a></div>
		<div class="text-blue font-11">Cursos Online</div>
		<a class="lnk-black" href="/cursos-online"> Clique Aqui</a>
	</div>	
	<div class="mt-4 col-12 col-md-4 col-lg-2 text-center">
		<div><a href="/src/coaching"><img width="40" src="/wp-content/themes/academy/images/coaching.png"></a></div>
			<div class="text-blue font-11"><span>Coaching</span></div>
			<a class="lnk-black" href="/src/coaching">Clique Aqui</a>
	</div>
	<div class="mt-4 col-12 col-md-4 col-lg-2 text-center">
		<div><a href="/sistema-de-questoes"><img width="40" src="/wp-content/themes/academy/images/SQ.png"></a></div>
			<div class="text-blue font-11"><span>Sistemas de Questões</span></div>
			<a class="lnk-black" href="/sistema-de-questoes">Clique Aqui</a>
	</div>
	<div class="mt-4 col-12 col-md-4 col-lg-2 text-center">
		<div class="text-center"><a href="/simulados"><img width="40" src="/wp-content/themes/academy/images/simulado.png"></a></div>
		<div class="text-blue font-11"><span>Simulado</span></div>
		<a class="lnk-black" href="/simulados">Clique Aqui</a>
	</div>
	<div class="mt-4 col-12 col-md-4 col-lg-2 text-center">
	<div class="text-center"><a href="/blog-noticias"><img width="40" src="/wp-content/themes/academy/images/noticias.png"></a></div>
		<div class="text-blue font-11"><span>Artigos e Notícias</span></div>
		<a class="lnk-black" href="/blog-noticias">Clique Aqui</a>
	</div>
	<div class="mt-4 col-12 col-md-4 col-lg-2 text-center">
	<div><a href="/fale-conosco"><img width="44" src="/wp-content/themes/academy/images/fale-conosco2.png"></a></div>
		<div class="text-blue font-11"><span>Fale Conosco</span></div>
		<a class="lnk-black" href="/fale-conosco">Clique Aqui</a>
	</div>		
</div>
<?php get_footer(); ?>