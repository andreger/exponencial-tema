<?php get_header() ?>

<?php redirecionar_se_nao_for_administrador() ?>

<?php 
if($_POST['submit']) {

	$data_inicio = $_POST['start_date'];
	$data_fim = $_POST['end_date'];
}
else {
	$data_inicio = date('d/m/Y', strtotime('-1 month'));
	$data_fim = date('d/m/Y');
}

$msg = null;
if(!$data_inicio || !data_fim) {
	$msg = "Erro. É necessário informar um período.";
}
else {
	$inicio = converter_para_yyyymmdd_HHiiss($data_inicio . ' 00:00:00');
	$fim = converter_para_yyyymmdd_HHiiss($data_fim . ' 23:59:59');

	$erros = listar_log_erros($inicio, $fim);

	if(count($erros) == 0) {
		$msg = "Não foram encontrados registros no período";
	}
	else {
		$msg = count($erros) . " ocorrência(s) registrada(s) no período";
	}

}
?>

<div class="container-fluid">
<div class="pt-3 mb-5 col-12 text-center text-md-left text-blue">
		<h1>Relatório de erros</h1>
	</div>
</div>	
<div class="container pt-1 pt-md-5 pb-5">
	
<div>
	<form id="relatorio_form" action="/relatorio-de-erros" method="post">
		<div class="row">
			<div class="col-1">Período:</div>
			<div class="col-2">
				<input type="text" class="form-control date-picker" id="start_date" name="start_date" value="<?= $_POST['start_date'] ? $_POST['start_date'] : date('d/m/Y', strtotime('-1 month')) ?> " />
			</div>
			<div class="text-center">até</div>
			<div class="col-2">
				<input type="text" class="form-control date-picker" id="end_date" name="end_date" value="<?= $_POST['end_date'] ? $_POST['end_date'] : date('d/m/Y') ?> " />
			</div>			
			<div class="col">
				<input class="btn u-btn-blue" id="submit_form" type="submit" value="Filtrar" name="submit"/>
			</div>
		</div>
	</form>
</div>

<div class="row">
	<div class="mt-4"><?= $msg ?></div>
	
	<?php if(count($erros) > 0) : ?>
	<table class="table table-bordered">
		<tr>
			<th>Tipo do Erro</th>
			<th>Data da ocorrência</th>
		</tr>
		<?php foreach ($erros as $erro) : ?>
		<tr>
			<td><?= $erro->lea_tipo_erro ?></td>
			<td><?= converter_para_ddmmyyyy_HHiiss($erro->lea_data_hora) ?></td>
		</tr>
		<?php endforeach; ?>
	</table>
	<?php endif; ?>
</div>
</div>
<!-- </div> #content -->
<div class="sectionlargegap"></div>
<?= get_date_picker(".date-picker") ?>
<script src="/wp-content/themes/academy/js/jquery.maskedinput.min.js"></script>
<script>
jQuery().ready(function() {
	// field masks
	 jQuery(".input_date").mask("99/99/9999");
});
</script>
<?php get_footer(); ?>  