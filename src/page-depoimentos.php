<?php
get_header();

KLoader::model('DepoimentoModel');
KLoader::helper("UrlHelper");

//Entevistas em vídeo
$depoimentos = DepoimentoModel::listar_depoimentos([DEPOIMENTO_ENTREVISTA_VIDEO], 0, LIMITE_DEPOIMENTOS_PRINCIPAL_VIDEO);

$data['depoimentos'] = $depoimentos;
$data['titulo'] = "Entrevistas";
$data['apresentacao'] = get_post_field('post_content');

if($depoimentos)
{
    KLoader::view('depoimentos/listagem_em_video', $data);
    KLoader::view('depoimentos/veja_mais', ['url' => UrlHelper::get_entrevistas_videos_url(), 'texto' => 'Veja mais entrevistas em vídeo']);
    $data['titulo'] = null;
    $data['apresentacao'] = null;
}

//Entrevistas em Texto
$depoimentos = DepoimentoModel::listar_depoimentos([DEPOIMENTO_ENTREVISTA_TEXTO], 0, LIMITE_DEPOIMENTOS_PRINCIPAL_TEXTO);

$data['depoimentos'] = $depoimentos;

if($depoimentos)
{
    KLoader::view('depoimentos/listagem_em_texto', $data);
    KLoader::view('depoimentos/veja_mais', ['url' => UrlHelper::get_entrevistas_textos_url(), 'texto' => 'Veja mais entrevistas em texto']);
    $data['titulo'] = null;
    $data['apresentacao'] = null;
}

echo "<div class='bg-gray'>";

//Depoimentos em Vídeo
$depoimentos = DepoimentoModel::listar_depoimentos([DEPOIMENTO_DEPOIMENTO_VIDEO], 0, LIMITE_DEPOIMENTOS_PRINCIPAL_VIDEO);

$data['depoimentos'] = $depoimentos;
$data['titulo'] = "Depoimentos";
$data['bg_color'] = "bg-gray";

if($depoimentos)
{
    KLoader::view('depoimentos/listagem_em_video', $data);
    KLoader::view('depoimentos/veja_mais', ['url' => UrlHelper::get_depoimentos_videos_url(), 'texto' => 'Veja mais depoimentos em vídeo', 'bg_color' => 'bg-gray']);
    $data['titulo'] = null;
    $data['apresentacao'] = null;
}

//Depoimentos em Texto
$depoimentos = DepoimentoModel::listar_depoimentos([DEPOIMENTO_DEPOIMENTO_TEXTO], 0, LIMITE_DEPOIMENTOS_PRINCIPAL_TEXTO);

$data['depoimentos'] = $depoimentos;
$data['bg_color'] = "bg-gray";

if($depoimentos)
{
    KLoader::view('depoimentos/listagem_em_texto', $data);
    KLoader::view('depoimentos/veja_mais', ['url' => UrlHelper::get_depoimentos_textos_url(), 'texto' => 'Veja mais depoimentos em texto', 'bg_color' => 'bg-gray']);
}

echo "</div>";

KLoader::model("");

get_footer();

