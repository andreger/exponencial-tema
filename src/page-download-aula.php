<?php
// redireciona para login se usuário não estiver logado
redirecionar_se_nao_estiver_logado();

KLoader::model("AulaModel");
KLoader::model("PedidoModel");
KLoader::helper("PDFHelper");

$index = $_GET['i'];
$produto_id = $_GET['pr'];
$pedido_id = $_GET['o'] ?: null;
$tipo_aula = $_GET['t'] ?: TIPO_AULA_PDF;

$index_aula_demo = get_post_meta($produto_id, 'aula_demo', true);

if (! $pedido_id) {
    $pedido_id = get_pedido_usuario_comprou_produto(get_current_user_id(), $produto_id);
}

// Impede acesso indevido
if (($index_aula_demo != $index) && (! $pedido_id)) {
    echo "Tentativa de acesso indevido registrada.";
    exit;
}

try {
    // lista os PDFs das aulas
    $arquivos = AulaModel::listar_arquivos_por_aula($produto_id, $index, $tipo_aula);    
    
    //gerar capa
    if(get_post_meta($produto_id, PRODUTO_INCLUIR_CAPA, true) == YES) {
        $capa = PDFHelper::gerar_capa($produto_id);
        array_unshift($arquivos, $capa);
    }
    
    // realiza o merge
    $pdf = PDFHelper::merge($arquivos);
    
    // inicia o download
    $filename = PDFHelper::get_nome_pdf_merge($produto_id, $index, $tipo_aula);
    $pdf->Output($filename,'D');
    
    // realiza a contagem dos downloads
    if($pedido_id) {
        PedidoModel::registrar_download_curso($pedido_id, $produto_id, $index, $tipo_aula);
    }
    exit;
    
} catch (Exception $e) {
    exit; 
}