<?php
date_default_timezone_set('America/Sao_Paulo');
/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/
tem_acesso(array(
	ADMINISTRADOR, COORDENADOR_AREA, PROFESSOR, ATENDENTE, APOIO
), ACESSO_NEGADO);

ini_set('memory_limit', '-1');
set_time_limit(600);

session_start();

KLoader::model("AreaModel");
KLoader::model("ConcursoModel");
KLoader::model("ProdutoModel");
KLoader::model("ProfessorModel");
KLoader::helper("RelatorioCursosHelper");
KLoader::helper("UiMensagemHelper");

if($_POST['alterar_sim_btn']) {

	if($_POST['nova_vendas_ate']) { 

		if($_POST['produtos_ids']) {

			$nova_data = converter_para_yyyymmdd($_POST['nova_vendas_ate']);
			$produtos_ids = explode(",", $_POST['produtos_ids']);

			if($produtos_ids) {
				foreach ($produtos_ids as $produto_id) {
					update_post_meta($produto_id, PRODUTO_VENDAS_ATE_POST_META, converter_para_ddmmyyyy($nova_data));
					update_post_meta($produto_id, PRODUTO_DISPONIVEL_ATE_POST_META, "");

					ProdutoModel::atualizar_metadados($produto_id);
				}
			}

			$mensagem = UiMensagemHelper::get_mensagem_sucesso_html("<strong>Sucesso!</strong> As datas dos produtos foram atualizadas.");
		}
		else {
			$mensagem = UiMensagemHelper::get_mensagem_erro_html("<strong>Erro!</strong> Pelo menos um produto deve ser selecionado.");
		}
	}
	else {
		$mensagem = UiMensagemHelper::get_mensagem_erro_html("<strong>Erro!</strong> A nova data de <i>vendas até</i> deve ser informada.");
	}
}

// carregamento das combos
$professores_combo = ProfessorModel::listar_por_tipo([COLABORADOR_PROFESSOR, COLABORADOR_PROFESSOR_CONSULTOR]);
$disciplinas_combo = listar_todas_disciplinas();
$concursos_combo = ConcursoModel::listar_todos();
$bancas_combo = ConcursoModel::listar_bancas();
$areas_combo = AreaModel::listar();

$status_combo = array(
	'publish' => 'Ativo',
	'pending' => 'Revisão Pendente',
	'draft' => 'Rascunho'
);

// Inicializa a sessão
if(!$_SESSION['form_relatorio_cursos']) {
	$_SESSION['form_relatorio_cursos'] = [];
}

// Alimenta a sessão
if (isset($_POST['filtrar']) || isset($_POST['export-excel'])) {

	$_SESSION['form_relatorio_cursos'] = [
		'expirados' => $_POST['incluir_expirados'] ?: null,
		'disciplinas' => $_POST['disciplinas_selecionadas'] ?: null,
		'professores' => $_POST['professores_selecionados'] ?: null,
		'concursos' => $_POST['concursos_selecionados'] ?: null,
	    'areas' => $_POST['areas_selecionadas'] ?: null,
		'status' => $_POST['status_selecionados'] ?: null,
		'somente_upload_nao_realizado' => $_POST['somente-erro-unr'] ?: null,
		'somente_upload_pendente' => $_POST['somente_pendencias'] ?: null,
		'excluir_cadernos' => $_POST['excluir_cadernos'] ?: null,
		'excluir_simulados' => $_POST['excluir_simulados'] ?: null,
		'excluir_pacotes' => $_POST['excluir_pacotes'] ?: null,
		'excluir_gratuitos' => $_POST['excluir_gratuitos'] ?: null,
		'excluir_mapas' => $_POST['excluir_mapas'] ?: null,
		'disponivel_ate' => $_POST['disponivel_ate'] ?: null,
	    'sem_cronograma' => $_POST['sem_cronograma'] ?: null,
	    'prazos_alerta' => $_POST['prazos_alerta'] ?: null,
		'tipo' => $_POST['tipo'] ?: null,
		'bancas' => $_POST['bancas_selecionadas'] ?: null,
	];
}

// Monta dados para chamadas de modelo
$filtros = [
	"expirados" => $_SESSION['form_relatorio_cursos']['expirados'],
	'disciplinas' => $_SESSION['form_relatorio_cursos']['disciplinas'],
	'professores' => $_SESSION['form_relatorio_cursos']['professores'],
	'concursos' => $_SESSION['form_relatorio_cursos']['concursos'],
    'areas' => $_SESSION['form_relatorio_cursos']['areas'],
	'status' => $_SESSION['form_relatorio_cursos']['status'],
	'somente_upload_nao_realizado' => $_SESSION['form_relatorio_cursos']['somente_upload_nao_realizado'],
	'somente_upload_pendente' => $_SESSION['form_relatorio_cursos']['somente_upload_pendente'],
	'excluir_cadernos' => $_SESSION['form_relatorio_cursos']['excluir_cadernos'],
	'excluir_simulados' => $_SESSION['form_relatorio_cursos']['excluir_simulados'],
	'excluir_pacotes_cat' => $_SESSION['form_relatorio_cursos']['excluir_pacotes'],
	'excluir_gratuitos' => $_SESSION['form_relatorio_cursos']['excluir_gratuitos'],
	'excluir_mapas' => $_SESSION['form_relatorio_cursos']['excluir_mapas'],
	'disponivel_ate' => converter_para_yyyymmdd($_SESSION['form_relatorio_cursos']['disponivel_ate']),
    'sem_cronograma' => $_SESSION['form_relatorio_cursos']['sem_cronograma'],
    'prazos_alerta' => $_SESSION['form_relatorio_cursos']['prazos_alerta'],
	'tipo' => $_SESSION['form_relatorio_cursos']['tipo'],
	'bancas' => $_SESSION['form_relatorio_cursos']['bancas'],
	'coordenador' => null
];

// Restrições de perfil de usuário
$user_id = get_current_user_id();

/**
 * @todo Verificar regras de acesso. Admin está entrando em tem_acesso([PROFESSOR]). Por conta disso é feita uma verificação extra nas permissões.
 */
if(!tem_acesso([ADMINISTRADOR, ATENDENTE, APOIO])) {

	// Coordenadores
	if(tem_acesso([COORDENADOR_AREA])) {
		$filtros['coordenador'] = $user_id;
	}
	// Professores
	else {
		$filtros['professores'] = [$user_id];
	}

}

// Execução da consulta
if($_POST['export-excel']) {
	$limit = null;
	$offset = null;
}
else {
	// Montagem da paginação
	$pg = isset($_GET['pg']) ? $_GET['pg'] : "1";
	$total = ProdutoModel::buscar_contar($filtros, 1);
	$pattern = "/relatorio-cursos?pg=(:num)";
	$limit = RELATORIO_CURSOS_PRODUTOS_POR_PAGINA;
	$paginator = new Paginator($total, $limit, $pg, $pattern);
	$offset = $pg ? ($pg - 1) * $limit : 0;
}

$produtos = ProdutoModel::buscar($filtros, $limit, $offset, 1);

// Monta os dados para serem exibidos no relatório
$max_aulas = 0;
$lines = array(); 
foreach ($produtos as $produto) {
    
	$post_id = $produto->ID;	
	$categorias = get_categorias_str($post_id);
	$professores = get_autores_str(get_post($post_id));
	$data_expiracao = get_data_disponivel_ate($post_id);
	$aulas_arquivo = get_post_meta($post_id, get_nome_campo_aulas_arquivos($filtros['tipo']), true);
	$aulas_data = get_post_meta($post_id, 'aulas_data', true);
	$aulas_nome = get_post_meta($post_id, 'aulas_nome', true);
	$aulas_primeiro_upload = get_post_meta($post_id, get_nome_campo_primeiro_upload($filtros['tipo']), true);

	$line = array(
	    'id' => $post_id,
		'edit_link' => get_edit_post_link($post_id),
		'curso' => $produto->post_title,
		'status' => $produto->post_status,
		'concurso' => $categorias,
	    'areas' => get_area($post_id),
		'professores' => $professores,
		'data_expiracao' => $data_expiracao,
		'data_prova' => ProdutoModel::get_proxima_data_prova($post_id),
		'banca' => ProdutoModel::get_bancas($post_id),
		'preco' => $produto->pro_preco,
		'qtde_esquemas' => get_qtde_esquemas($post_id),
		'qtde_questoes' => get_qtde_questoes($post_id),
		'forum' => get_forum_url_from_product_id($post_id),
		'aulas' => array()
	);
	
	if($aulas_data) {
		foreach ($aulas_data as $i => $data) {
			$aula['nome'] = $aulas_nome[$i];
			$aula['data'] = $data;
			$aula['primeiro_upload'] = $aulas_primeiro_upload[$i];
			$aula['arquivo'] = $aulas_arquivo[$i];
			$aula['erro'] = 0;
			
			// checa se o arquivo da aula foi upado com atraso pelo professor
			if($aulas_primeiro_upload[$i] && (strtotime(str_replace('/','-', $aulas_primeiro_upload[$i] . ' 00:00:00')) > strtotime(str_replace('/','-', $data . ' 00:00:00')))) {
				$aula['erro'] = ERRO_UPLOAD_ATRASADO;
				$line['erro_upload_atrasado'] = 1;
			}

			// checa se o arquivo da aula disponível não foi upado
			if(!$aulas_arquivo[$i] && (date('Y-m-d') > converter_para_yyyymmdd($data))) {
				$aula['erro'] = ERRO_UPLOAD_NAO_REALIZADO;
			}

			// checa se o arquivo da aula (qualquer) não foi upado
			if($aulas_arquivo[$i]) {
				$aula['uploaded'] = TRUE;
			}
			else {
				$aula['uploaded'] = FALSE;
			}

			$line['aulas'] = array_merge($line['aulas'], array($i => $aula));
		}
	}

	if(count($line['aulas']) > $max_aulas) {
		$max_aulas = count($line['aulas']);
	}

	array_push($lines, $line);
}

if(isset($_POST['export-excel'])) {
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="stats.xls"');

	$tabela = RelatorioCursosHelper::get_table($lines, true, $max_aulas); 
	echo RelatorioCursosHelper::converter_table_para_xls($tabela);
	exit;
} else {
	get_header();
}
?>
<link href="/questoes/assets-admin/js/plugins/select2/select2.css" rel="stylesheet">
<link href="/questoes/assets-admin/js/plugins/select2/select2-bootstrap.css" rel="stylesheet">
<script src="/questoes/assets-admin/js/plugins/select2/select2.js"></script>

<?php if($mensagem) : ?>
	<div class="container pt-4"><?= $mensagem ?></div>
<?php endif ?>

<div class="container-fluid pt-1 pt-md-4">
	<div class="mb-5 col-12 text-center text-md-left text-blue">
		<h1>Relatório de Cursos</h1>
	</div>
</div>
<div class="container">	

	<div class="row mt-3" style="display: none;">
		<form action="/relatorio-cursos" method="post">
			<input type="hidden" name="export-excel" value="1">
			<input type="submit" value="Exportar para Excel" name="submit" />
		</form>
	</div>

	<div class="row mt-3">

		<div class="col-7">

			<?php if(tem_acesso(array(ADMINISTRADOR, COORDENADOR_AREA, PROFESSOR, ATENDENTE, APOIO))) : ?>
				<form method="post" action="/relatorio-cursos">
				
					<div class="mb-3">
						<span style="width:90px;display:inline-block"><strong>Tipo:</strong></span>
						<select style="width: 100%" name="tipo" class='filtro'>
							<?php foreach (RelatorioCursosHelper::get_tipo_aula_combo_options() as $key => $value) : ?>
								<?php 
								$selected = "";
								
								$f_tipo = isset($filtros['tipo']) ? $filtros['tipo'] : AULA_UPLOAD_TIPO_PDF;

							    if($key == $f_tipo) {
									$selected = "selected=selected";
								}
								?>
								<option value="<?= $key ?>" <?= $selected ?>><?= $value ?></option>
							<?php endforeach; ?>
						</select>
					</div>
					
					<?php if(tem_acesso(array(ADMINISTRADOR, COORDENADOR_AREA, ATENDENTE, APOIO))) : ?>
					<div class="mb-3"> 
						<span style="width:90px;display:inline-block"><strong>Professores:</strong></span> 
						<select style="width: 100%" name="professores_selecionados[]" class='filtro' multiple="multiple">
							<?php foreach ($professores_combo as $item) : ?>
								<?php 
								$selected = "";
								if($f_professores = $filtros['professores']) {
									foreach($f_professores as $f_professor) {
										if($item->ID == $f_professor) {
											$selected = "selected=selected";
											break;
										}
									}
								}
								?>
								<option value="<?= $item->ID ?>" <?= $selected ?>><?= $item->display_name ?></option>
							<?php endforeach; ?>
							<option value="-1" <?= isset($_POST['professores_selecionados']) && in_array(-1, $_POST['professores_selecionados']) ? "selected=selected" : "" ?> >Sem Professor</option>
						</select>
					</div>
					<?php endif ?>

					<div class="mb-3">
						<span style="width:90px;display:inline-block"><strong>Disciplinas:</strong></span>
						<select style="width: 100%" name="disciplinas_selecionadas[]" class='filtro' multiple="multiple">
							<?php foreach ($disciplinas_combo as $item) : ?>
								<?php 
								$selected = "";
								if($f_disciplinas = $filtros['disciplinas']) {
									foreach ($f_disciplinas as $f_disciplina) {
										if($item->term_id == $f_disciplina) {
											$selected = "selected=selected";
											break;
										}
									}
								}
								?>
								<option value="<?= $item->term_id ?>" <?= $selected ?>><?= $item->name ?></option>
							<?php endforeach; ?>
						</select>
					</div>

					<div class="mb-3">
						<span style="width:90px;display:inline-block"><strong>Concursos:</strong></span>
						<select style="width: 100%" name="concursos_selecionados[]" class='filtro' multiple="multiple">
							<?php foreach ($concursos_combo as $item) : ?>
								<?php 
								$selected = "";
								if($f_concursos = $filtros['concursos']) {
									foreach ($f_concursos as $f_concurso) {
										if($item->ID == $f_concurso) {
											$selected = "selected=selected";
											break;
										}
									}
								}
								?>
								<option value="<?= $item->ID ?>" <?= $selected ?>><?= $item->post_title ?></option>
							<?php endforeach; ?>
						</select>
					</div>

					<div class="mb-3">
						<span style="width: 100%; display: inline-block;"><strong>Bancas:</strong></span>
						<select style="width: 100%" name="bancas_selecionadas[]" class='filtro' multiple="multiple">
							<?php foreach ($bancas_combo as $item) : ?>
								<?php 
								$selected = "";
								if($f_bancas = $filtros['bancas']) {
									foreach ($f_bancas as $f_banca) {
										if($item->con_banca == $f_banca) {
											$selected = "selected=selected";
											break;
										}
									}
								}
								?>
								<option value="<?= $item->con_banca ?>" <?= $selected ?>><?= $item->con_banca ?></option>
							<?php endforeach; ?>
						</select>
					</div>
					
					<div class="mb-3">
						<span style="width:90px;display:inline-block"><strong>Áreas:</strong></span>
						<select style="width: 100%" name="areas_selecionadas[]" class='filtro' multiple="multiple">
							<?php foreach ($areas_combo as $item) : ?>
								<?php 
								$selected = "";
								if($f_areas = $filtros['areas']) {
								    foreach ($f_areas as $f_area) {
								        if($item->term_id == $f_area) {
											$selected = "selected=selected";
											break;
										}
									}
								}
								?>
								<option value="<?= $item->term_id ?>" <?= $selected ?>><?= $item->name ?></option>
							<?php endforeach; ?>
						</select>
					</div>

					<div class="mb-3">
						<span style="width:90px;display:inline-block"><strong>Status:</strong></span>
						<select style="width: 100%" name="status_selecionados[]" class='filtro' multiple="multiple">
							<?php foreach ($status_combo as $key => $value) : ?>
								<?php 
								$selected = "";
								if($f_status = $filtros['status']) {
									foreach ($f_status as $f_status_item) {
										if($key == $f_status_item) {
											$selected = "selected=selected";
											break;
										}
									}
								}
								?>
								<option value="<?= $key ?>" <?= $selected ?>><?= $value ?></option>
							<?php endforeach; ?>
						</select>
					</div>

					<div class="mb-3">
						<span style="display:inline-block"><strong>Data disponível até:</strong></span>
						<input type="text" class="form-control date-picker" id="disponivel_ate" name="disponivel_ate" value="<?= $_SESSION['disponivel_ate'] ?: '' ?>" />
					</div>

					<div class="mb-2">
						<span style="width:90px;display:inline-block"><strong>Excluir:</strong></span>

						<input type="checkbox" id="excluir_cadernos" name="excluir_cadernos" value="1" <?= $filtros['excluir_cadernos'] ? 'checked' : '' ?> > <label class="mr-3" for="excluir_cadernos">Cadernos</label>
						<input type="checkbox" id="excluir_simulados" name="excluir_simulados" value="1" <?= $filtros['excluir_simulados'] ? 'checked' : '' ?> > <label class="mr-3" for="excluir_simulados">Simulados</label>
						<input type="checkbox" id="excluir_pacotes" name="excluir_pacotes" value="1" <?= $filtros['excluir_pacotes_cat'] ? 'checked' : '' ?> > <label class="mr-3" for="excluir_pacotes">Pacotes</label>
						<input type="checkbox" id="excluir_gratuitos" name="excluir_gratuitos" value="1" <?= $filtros['excluir_gratuitos'] ? 'checked' : '' ?> > <label class="mr-3" for="excluir_gratuitos">Produtos Grátis</label>
						<input type="checkbox" id="excluir_mapas" name="excluir_mapas" value="1" <?= $filtros['excluir_mapas'] ? 'checked' : '' ?> > <label class="mr-3" for="excluir_mapas">Mapas Mentais</label>
					</div>

					<div class="mb-3">
						<span style="display:inline-block"><strong>Outras opções:</strong></span>
					</div>

					<div class="mb-2">
						<input type="checkbox" id="somente-erro-unr" name="somente-erro-unr" value="1" <?= $filtros['somente_upload_nao_realizado'] ? 'checked' : '' ?> > <label for="somente-erro-unr">Exibir somente cursos com aulas em atraso</label>
					</div>

					<div class="mb-2">
						<input type="checkbox" id="incluir_expirados" name="incluir_expirados" value="1" <?= $filtros['expirados'] ? 'checked' : '' ?> > <label for="incluir_expirados">Mostrar cursos expirados</label>
					</div>

					<div class="mb-2">
						<input type="checkbox" id="somente_pendencias" name="somente_pendencias" value="1" <?= $filtros['somente_upload_pendente'] ? 'checked' : '' ?> > <label for="somente_pendencias">Exibir somente cursos com aulas futuras pendentes</label>
					</div>
					
					<div class="mb-2">
						<input type="checkbox" id="sem_cronograma" name="sem_cronograma" value="1" <?= $filtros['sem_cronograma'] ? 'checked' : '' ?> > <label for="sem_cronograma">Exibir somente cursos sem cronograma</label>
					</div>
					
					<div class="mb-2">
						<input type="checkbox" id="prazos_alerta" name="prazos_alerta" value="1" <?= $filtros['prazos_alerta'] ? 'checked' : '' ?> > <label for="prazos_alerta">Exibir somente prazos em alerta</label>
					</div>

					<div class="mb-3">
						<input class="btn u-btn-blue" type="submit" name="filtrar" value="Filtrar">
						<input class="btn u-btn-blue" type="submit" name="export-excel" value="Exportar XLS">
						<a class="btn u-btn-blue" href="/forum">Ir para fóruns</a>

						<?php if(tem_acesso([ADMINISTRADOR, COORDENADOR_AREA, APOIO])) : ?>
							<button class="btn u-btn-blue" href="#" id="alterar-data-btn">Alterar Data</button>
						<?php endif ?>
					</div>
				</form>
				<?php endif; ?>

				<?php if(tem_acesso([ADMINISTRADOR, COORDENADOR_AREA, APOIO])) : ?>
				<form method="post" id="nova-vendas-ate-frm">
					<div id="alterar-data-div" style="border:1px solid #ddd; border-radius: 3px; padding: 20px; display:none">
						<div class="mb-3" >
							<h3>Alterar Data</h3>

							<?= UiMensagemHelper::get_mensagem_info_html("<strong>Atenção!</strong> Para efetuar a alteração selecione os produtos na tabela abaixo.") ?>

							<span style="display:inline-block"><strong>Nova data vendas até:</strong></span>
							<input type="text" class="form-control date-picker" id="nova_vendas_ate" name="nova_vendas_ate" value="<?= $_POST['nova_vendas_ate'] ?: '' ?>" />
							<input type="hidden" name="produtos_ids" id="produtos-ids">
						</div>

						<div class="mb-3">
							<button class="btn u-btn-blue" id="alterar-data-confirm-btn" data-toggle="modal" data-target="#alterar-data-modal">Alterar Data</button>

							<div class="modal fade" id="alterar-data-modal" tabindex="-1" role="dialog" aria-hidden="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title">Alterar Data</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<div class="msg-erro msg-erro-alteracao" style="display:none">
												Você irá alterar a data dos produtos selecionados para <strong><span id="nova-data-span"></span></strong>.<br><br>
												Está certo disto?
											</div>
											<div class="msg-erro msg-erro-anterior" style="display:none">
												Você está alterando a data para uma data passada, o que irá impactar na disposição dos produtos e acesso dos mesmos.<br><br>Confirma a operação?
											</div>
										</div>
										<div class="modal-footer">
											<input type="submit" name="alterar_sim_btn" id="alterar_sim_btn" value="Sim" class="btn u-btn-blue">
											<button type="button" class="btn u-btn-red" data-dismiss="modal">Não</button>
										</div>
									</div>
								</div>
							</div>
							<button id="fechar-alterar-data-btn" class="btn btn-white">Fechar</button>
						</div>
					</div>
				</form>
			<?php endif ?>
		</div>
		<?php KLoader::view("relatorios/cursos/legenda") ?>

	</div>
</div>

<div class="mt-4 mb-4">
	<?php if($lines) :?>
		
		<div style="margin: 0 15px">

			<div class="row mt-5 mb-0">
				<div class="col-6">
					<div style="position:relative;top:-6px;">
						Registros encontrados: <strong><?= numero_inteiro($total) ?></strong>
					</div>
				</div>
				<div class="col-6 mt-2">
					<?= $paginator ?>
				</div>
			</div>

			<?= RelatorioCursosHelper::get_table($lines, false, $max_aulas) ?>

			<div class="row mt-5 mb-3" style="text-align: right; width: 100%">
				<div class="col-12">
					<?= $paginator ?>
				</div>
			</div>
		</div>
	<?php else : ?>
		<div class="container">	
			<div class="row text-center">	
				<?php if (isset($_POST['filtrar']) || isset($_POST['export-excel'])) : ?>
					<span>A busca realizada não retornou resultados.</span>
				<?php else : ?>
					<span>Selecione um filtro para realizar a busca.</span>
				<?php endif ?>
			</div>
		</div>
	<?php endif ?>
	</div>
</div>
</div>
</div><!-- #content -->
<div class="sectionlargegap"></div>

<?= get_date_picker(".date-picker") ?>
<script src="/wp-content/themes/academy/js/jquery.validate.min.js"></script>
<script src="/wp-content/themes/academy/js/jquery.validate.ext.js"></script>
<script src="/wp-content/themes/academy/js/jquery.validate.messages.js"></script>
<script>
	jQuery(document).ready(function(){
		jQuery(".filtro").select2();

		jQuery("#alterar-data-btn").click(function(e) {
			e.preventDefault();
			jQuery("#alterar-data-div").toggle();
			jQuery(".checkbox-td").toggle();
		});

		jQuery("#all-chb").change(function() {
			jQuery(".produto-chb").prop('checked',this.checked);
		});

		jQuery("#fechar-alterar-data-btn").click(function(e) {
			e.preventDefault();
			jQuery("#alterar-data-btn").click();
		});

		jQuery("#alterar-data-confirm-btn").click(function(e) {
			e.preventDefault();

			if(!jQuery("#nova-vendas-ate-frm").valid()) {
				return false;
			}
			else {
				jQuery(".msg-erro").hide();

				var hoje = "<?= date('Y-m-d') ?>";
				var novaData = jQuery("#nova_vendas_ate").val().split("/").reverse().join("-");

				if(novaData < hoje) {
					jQuery(".msg-erro-anterior").show();
				}
				else {
					jQuery("#nova-data-span").html(jQuery("#nova_vendas_ate").val());
					jQuery(".msg-erro-alteracao").show();
				}
			}
		});

		jQuery("#nova_vendas_ate").datepicker().on("change", function() {
			jQuery(this).valid();
		});

		jQuery("#nova-vendas-ate-frm").validate({
			rules: {
				nova_vendas_ate: {
					required: true,
					date: true
				}
			}
		});

		jQuery("#alterar_sim_btn").click(function(e) {
			
			var ids = [];
            jQuery.each(jQuery(".produto-chb:checked"), function(){            
                ids.push(jQuery(this).val());
            });
            jQuery("#produtos-ids").val(ids);
            
		});

	});
</script>

<?php get_footer(); ?>