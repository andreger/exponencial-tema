<?php
/**
 * Página de comparativo coaching
 * 
 * http://www.exponencialconcursos.com.br/comparativo-do-coaching
 */

get_header();

?>

<div class="row border border-danger">
	<div class="comparativo-coaching">
		<table>
			<thead>
				<tr>
					<th><img src="/wp-content/themes/academy/images/programa.png" /><br/>O Que o Programa Contempla?</th>
					<th><img src="/wp-content/themes/academy/images/mentoria.png" /><br/>Coaching/Mentoria</th>
					<th><img src="/wp-content/themes/academy/images/turma.png" /><br/>Turma de Coaching Reta Final</th>
					<th><img src="/wp-content/themes/academy/images/ptte.png" /><br/>Coaching em Turma</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Periodicidade</td>
					<td>1, 3 ou 6 meses, renov&aacute;vel pelo per&iacute;odo que desejar</td>
					<td>Foco nos concursos autorizados (editais pr&oacute;ximos)</td>
					<td>Verificar data de validade de cada programa
					</td>
				</tr>
				<tr>
					<td>Possui coach/mentor?</td>
					<td>Sim</td>
					<td>Sim</td>
					<td>Sim</td>
				</tr>
				<tr>
					<td>Whatsapp</td>
					<td>Sim</td>
					<td>Sim</td>
					<td>Sim</td>
				</tr>
				<tr>
					<td>Intera&ccedil;&atilde;o</td>
					<td>Individual</td>
					<td>Em grupo de no m&aacute;ximo 6 pessoas</td>
					<td>Em grupo de no m&aacute;ximo 6 pessoas</td>
				</tr>
				<tr>
					<td>Ciclo de Estudo</td>
					<td>Personalizado</td>
					<td>Sugerido</td>
					<td>Por m&oacute;dulo</td>
				</tr>
				<tr>
					<td>Metas</td>
					<td>
						Por concurso(s) que desejar, com acompanhamento personalizado e direcionamento detalhado

						Leva em conta todos os aspectos da sua rotina (hor&aacute;rios e atividades)
					</td>
					<td>
						Foco exclusivo (100%) no concurso escolhido, com direcionamento no n&iacute;vel &ldquo;assuntos&rdquo; de cada disciplina&nbsp;
					</td>
					<td>
						Foco exclusivo (100%) no concurso/&aacute;rea escolhido, com direcionamento no n&iacute;vel &ldquo;assuntos&rdquo; de cada disciplina
					</td>
				</tr>
				<tr>
					<td>
						Mudan&ccedil;a do concurso de estudo (foco)
					</td>
					<td>
						Sim
					</td>
					<td>
						N&atilde;o
					</td>
					<td>
						N&atilde;o
					</td>
				</tr>
				<tr>
					<td>
						T&eacute;cnicas de Estudo
					</td>
					<td>
						Sim, inclusive com corre&ccedil;&atilde;o do uso pelo aluno e passo-a-passo para aumentar o aprendizado
					</td>
					<td>
						Sim
					</td>
					<td>
						Sim
					</td>
				</tr>
				<tr>
					<td>
						Analise vertical do edital&nbsp;(ponto-a-ponto)
					</td>
					<td>
						Sim, inclusive p&oacute;s edital
					</td>
					<td>
						Sim, inclusive p&oacute;s edital
					</td>
					<td>
						Sim, inclusive p&oacute;s edital
					</td>
				</tr>
				<tr>
					<td>
						Contempla P&oacute;s edital?
					</td>
					<td>
						Sim
					</td>
					<td>
						Sim, apenas do concurso em foco durante a vig&ecirc;ncia do treinamento
					</td>
					<td>
						Sim, apenas do concurso em foco durante a vig&ecirc;ncia do programa
					</td>
				</tr>
				<tr>
					<td>
						Foco
					</td>
					<td>
						M&eacute;dio e longo prazo
					</td>
					<td>
						Curto prazo
					</td>
					<td>
						M&eacute;dio e longo prazo
					</td>
				</tr>
				<tr>
					<td>
						Material de Estudo (cursos, livros)
					</td>
					<td>
						Indica&ccedil;&atilde;o imparcial e personalizada, com base no perfil psicopedag&oacute;gico do aluno, materiais que j&aacute; possuir e foco escolhido. N&atilde;o interfere no Coaching
					</td>
					<td>
						Indica&ccedil;&atilde;o imparcial com base no perfil psicopedag&oacute;gico do aluno, materiais que j&aacute; possuir e foco escolhido. N&atilde;o interfere no Treinamento Intensivo
					</td>
					<td>
						Sim, lista de refer&ecirc;ncia por mat&eacute;ria com base no melhor custo x benef&iacute;cio. Uso de material que n&atilde;o conste na lista n&atilde;o interfere no Programa de Treinamento
					</td>
				</tr>
				<tr>
					<td>
						Descontos no Exponencial
					</td>
					<td>
						Sim
					</td>
					<td>
						Sim
					</td>
					<td>
						Sim
					</td>
				</tr>
				<tr>
					<td>
						Sistema de Quest&otilde;es
					</td>
					<td>
						Sim
					</td>
					<td>
						Sim
					</td>
					<td>
						Sim
					</td>
				</tr>
				<tr>
					<td>
						Cadernos de Quest&atilde;o
					</td>
					<td>
						Sim
					</td>
					<td>
						Sim
					</td>
					<td>
						Sim
					</td>
				</tr>
				<tr class="ultimo">
					<td>
						Simulados
					</td>
					<td>
						Sim, designados pelo coach conforme necessidade de cada aluno
					</td>
					<td>
						Sim, quantidade ser&aacute; informada na descri&ccedil;&atilde;o do servi&ccedil;o
					</td>
					<td>
						Sim, quantidade ser&aacute; informada na descri&ccedil;&atilde;o do servi&ccedil;o
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<div class="row">
	<div class="threecol column">
		<div class="descontos-pre">Se você contratou o serviço de coaching, durante a sua duração, você terá os seguintes descontos:</div>
		<div class="descontos-coluna">
			<div class="desconto-img-sup">
				<div>
					<img src="/wp-content/themes/academy/images/programa.png">
				</div>
				<div class="comparativo-coaching-titulo">
					O que o programa contempla?
				</div>
			</div>
		</div>
		<div class="descontos-coluna-interna">
			<div class="desconto-texto-azul">Periodicidade</div>
			<div class="desconto-texto-azul">Possui coaching/mentor?</div>
			<div class="desconto-texto-azul">Whatsapp</div>
			<div class="desconto-texto-azul">Interação</div>
			<div class="desconto-texto-azul">Ciclo de Estudo</div>
			<div class="desconto-texto-azul">Metas</div>
			<div class="desconto-texto-azul">Mudança do concurso de estudo (foco)</div>
			<div class="desconto-texto-azul">T&eacute;cnicas de Estudo</div>
			<div class="desconto-texto-azul">Analise vertical do edital&nbsp;(ponto-a-ponto)</div>
			<div class="desconto-texto-azul">Contempla P&oacute;s edital?</div>
			<div class="desconto-texto-azul">Foco</div>
			<div class="desconto-texto-azul">Material de Estudo (cursos, livros)</div>
			<div class="desconto-texto-azul">Descontos no Exponencial</div>
			<div class="desconto-texto-azul">Sistema de Quest&otilde;es</div>
			<div class="desconto-texto-azul">Cadernos de Quest&atilde;o</div>
			<div class="desconto-texto-azul">Simulados</div>
		</div>
	</div>

	<div class="threecol column">
		<div class="descontos-pre">Agora, se você adquiriu algum pacote <b>de cursos</b>, então terá direito aos seguintes descontos:</div>
		<div class="descontos-coluna">
			<div class="desconto-img-sup">
				<div>
					<img src="/wp-content/themes/academy/images/mentoria.png">
				</div>
				<div class="comparativo-coaching-titulo">
					Coaching/Mentoria
				</div>
			</div>
		</div>
		<div class="descontos-coluna-interna">
			<div class="">1, 3 ou 6 meses, renov&aacute;vel pelo per&iacute;odo que desejar</div>
			<div class="">Sim</div>
			<div class="">Sim</div>
			<div class="">Individual</div>
			<div class="">Personalizado</div>
			<div class="">Por concurso(s) que desejar, com acompanhamento personalizado e direcionamento detalhado.<br>Leva em conta todos os aspectos da sua rotina (hor&aacute;rios e atividades)</div>
			<div class="">Sim</div>
		</div>
	</div>

	<div class="threecol column">
		<div class="descontos-pre">Agora, se você adquiriu algum pacote <b>de cursos</b>, então terá direito aos seguintes descontos:</div>
		<div class="descontos-coluna">
			<div class="desconto-img-sup">
				<div>
					<img src="/wp-content/themes/academy/images/turma.png">
				</div>
				<div class="comparativo-coaching-titulo">
					Turma de Coaching Reta Final
				</div>
			</div>
		</div>
		<div class="descontos-coluna-interna">
			<div class="">Foco nos concursos autorizados (editais pr&oacute;ximos)</div>
			<div class="">Sim</div>
			<div class="">Sim</div>
			<div class="">Em grupo de, no máximo, 6 pessoas</div>
			<div class="">Foco exclusivo (100%) no concurso escolhido, com direcionamento no n&iacute;vel &ldquo;assuntos&rdquo; de cada disciplina&nbsp;</div>
			<div class="">Não</div>
		</div>
	</div>
	
</div>

<?php get_footer() ?>