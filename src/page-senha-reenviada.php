<?php get_header (); ?>

<?php $usuario_array = get_usuario_array($_GET['uid']) ?>

<div class="container pt-5 pb-5">
	<div class="col-12 text-center mt-2">
		<div>
			<img src="<?php echo get_tema_image_url('nova-senha.png')?>">
		</div>
		<div class="mt-4">
			Uma nova senha foi reenviada para o e-mail: 
			<b><?php echo $usuario_array['email'] ?></b>
		</div>
	</div>
</div>

<?php get_footer(); ?>  
