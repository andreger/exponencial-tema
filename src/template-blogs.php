<?php
/*
Template Name: Articles
*/

get_header();

KLoader::model("BlogModel");
KLoader::helper("BlogHelper");

$url_a = parse_url(get_url_atual());
$path = $url_a['path'];

$blog_tipo_id = BlogHelper::get_blog_tipo_from_url_path($path);

$limit = 4;
$blogs = BlogModel::listar($blog_tipo_id, $limit);

$primeiro = array_shift($blogs);
$url_primeiro = UrlHelper::get_blog_post_url($primeiro);
?>

<?php KLoader::view("blogs/navbar") ?>

<div class="container pt-5">
	<h1 class="font-pop mt-2">
		<?= get_h1($post->ID) ?>
	</h1>
	
	<div class="row" id="txtHint">
	
		<div class="col-12 col-md-7 col-lg-6">
			
			<div class="mt-4">
				<a class="t-d-none text-blue font-18 line-height-1" href="<?= $url_primeiro ?>">
					<?= $primeiro->post_title ?>
				</a>	
            </div>	

            <div id="blog-botoess" class="text-blue font-10 mt-4">
				<?= get_share_buttons($url_primeiro) ?>
				<?php converter_para_ddmmyyyy($primeiro->post_date) ?> por	
				<?php if($colaborador = ColaboradorModel::get_by_id($primeiro->user_id)) : ?>
					<a class="font-10" href="<?= UrlHelper::get_professor_url($colaborador->col_slug) ?>"><strong><?= $primeiro->display_name ?></strong></a>
				<?php else : ?>
					<span class="font-10"><strong><?= $primeiro->display_name ?></strong></span>
				<?php endif ?>
			</div>
			
            <div class="b_col">
            	<a href="<?= $url_primeiro ?>">
					<div class="b_big_image">
						<img src="<?= UrlHelper::get_blog_post_thumb_url($primeiro) ?>">
					</div>
				</a>
				<div class="text-dark font-14">
					<?= wp_trim_words($primeiro->blo_resumo, 25,' [...]'); ?>
				</div>

				<div class="text-right">
					<a href="<?= $url_primeiro ?>">
						<button class="mt-2 btn u-btn-primary font-10">Leia mais</button>
					</a>
				</div>
			</div>
			
		</div>
		
		<div class="d-none d-lg-block col-md-1"></div>
		
		<div class="col-12 col-md-5 mt-2">
			<?php foreach ( $blogs as $blog ) : ?> 
				<?php
            		$url = UrlHelper::get_blog_post_url($blog);
            	?>
				<div class="border-bottom border-dark mt-3">
					<div>
						<a class="t-d-none text-blue font-16 line-height-1" href="<?= $url ?>">
							<?= $blog->post_title ?>
						</a>
					</div>
					<div id="blog-botoes" class="text-blue font-10 mt-4">								
						<?= get_share_buttons($url) ?>							
						<?php converter_para_ddmmyyyy($blog->post_date) ?> por	
        				<?php if($colaborador = ColaboradorModel::get_by_id($blog->user_id)) : ?>
    						<a class="font-10" href="<?= UrlHelper::get_professor_url($colaborador->col_slug) ?>"><strong><?= $blog->display_name ?></strong></a>
    					<?php else : ?>
    						<span class="font-10"><strong><?= $blog->display_name ?></strong></span>
    					<?php endif ?>
					</div>
					<div class="text-dark font-14 mt-3">
						<?= wp_trim_words($blog->blo_resumo, 25,' [...]'); ?>
					</div>
					<div class="text-right mt-2 mb-3">
						<a href="<?= $url ?>">
							<button class="btn u-btn-primary font-10">Leia mais</button>
						</a>								
					</div>
				</div>
			<?php endforeach; ?>
        </div>
    </div>
    
    <div>
        <div class="row">
        	<div class="mt-5 mb-5 ml-auto pr-3">
        		<a href="<?= BlogHelper::get_blog_posts_url($blog_tipo_id) ?>">
        			<button class="btn btn-lg u-btn-primary font-10">Ver todos</button>
        		</a>
        	</div>
        </div>
    </div>
    
</div>

<?php get_footer(); ?>