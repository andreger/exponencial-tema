<?php get_header(); ?>
<div class="acesso-negado">
	<div class="container pt-5">
		<div class="row pt-5">
			<div class="pt-5 col-12 col-lg-5"></div>
			<div class="pt-5 col-12 col-lg-6 text-center text-lg-left"><h1 class="text-white line-height-1">Desculpe-nos,<br>mas você não tem permissão<br>de acesso a esta área do site.</h1>
				<div class="mt-3 mt-md-0 ml-4 col-11 text-center text-lg-right"><a class="t-d-none text-white font-14" href="#" onclick="history.back()"><img src="<?php echo get_tema_image_url('voltar.png')?>"> Voltar para tela anterior.</a></div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>