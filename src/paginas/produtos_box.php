<?php
$i = 0;

foreach ($produtos as $produto) {
	$classes = "";
	if($i % 4 == 3) {
		$classes = "last";
	}
?>

<li class="container border border-danger product <?= $classes ?>">
	<div class="course-preview premium-course">
		<div class="course-image">
			<a href="<?= get_permalink($produto->post->ID) ?>">
				<?php if( has_post_thumbnail($produto->post->ID) ) : ?>
					<?= get_the_post_thumbnail($produto->post->ID, array(285,149)) ?>
				<?php else : ?>
					<img src="<?= get_template_directory_uri() ?>/src/imagesages/default_book.jpg" />
				<?php endif ?>
			</a>

			<div class="course-price product-price">
				<div class="price-text">
					<span class="amount"><?= $produto->get_price_html() ?></span>				
				</div>
				<div class="corner-wrap">
					<div class="corner"></div>
					<div class="corner-background"></div>
				</div>	
			</div>
		</div>
		<div class="course-meta">
			<header class="course-header">
				<h5 class="nomargin">
					<a href="<?= get_permalink($produto->post->ID) ?>">
						<?= get_the_title($produto->post->ID) ?></a>
				</h5>
				<?= wp_trim_words(get_autores_str($produto->post), 6) ?>
			</header>
		</div>
	</div> 
</li>

<?php
	$i++;
}
?>