<?php 
if($_POST['submit']) {
    $nome = trim($_POST['nome']);
    $email = trim($_POST['email']);
    $interesses = $_POST['interesses'];

    $validacao_ok = null;
    $erro_validacao = "";
    
    if(!$nome) {
        $validacao_ok = false;
        $erro_validacao = "O nome deve ser preenchido";
    }
    elseif(strpos($nome, " ") === false) {
        $validacao_ok = false;
        $erro_validacao = "O nome precisa ser completo";   
    }
    elseif(!$email) {
            $validacao_ok = false;
            $erro_validacao = "E-mail precisa ser informado";
        }
    elseif(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $validacao_ok = false;
        $erro_validacao = "E-mail inválido";
    }
    else {
        if($erro_validacao = mailchimp_salvar($email, $nome, $interesses)) {
            $validacao_ok = false;
        }
        else {
            $validacao_ok = true;
        }
    }

    // Google Captcha
    $secret = "6LeN3Q0TAAAAAKZ59noxA3HXU2Ea5hmaMZxZ4JO8";
    $response = null;
    $reCaptcha = new ReCaptcha($secret);
    
    if(is_selenium()) {
        $validacao_ok = true;
    }
    else {
        if ($_POST["g-recaptcha-response"]) {

            $response = $reCaptcha->verifyResponse($_SERVER["REMOTE_ADDR"], $_POST["g-recaptcha-response"]);
            
            if ($response != null && $response->success) {
                $validacao_ok = true;
            }
            else {
                $validacao_ok = false;
                $erro_validacao = "Código de verificação incorreto";
            }
        }
    }       
}
?>
<div>

    <div>
    <?php if(!is_null($validacao_ok)) : ?>          
        <?php if($validacao_ok) :?>
            <div id="faleconosco-sucesso" class="register-success text-center">Cadastro realizado com sucesso</div>
        <?php else : ?>
            <div class="msg-error text-center"><?= $erro_validacao; ?></div>
        <?php endif; ?>
    <?php endif; ?>
    </div>

    <form method="POST">
        <div class="emc-box">
            <div class="emc-campo">
                <label class="emc-label">E-mail <span class="">*</span></label>
                <div class="field-group">
                    <input type="text" autocapitalize="off" autocorrect="off" name="email" value="<?= isset($_POST['email']) ? $_POST['email'] : '' ?>">
                </div>
            </div>
            
            <div>
                <label class="emc-label">Nome Completo</label>
                <div class="field-group">
                    <input type="text" name="nome" value="<?= isset($_POST['nome']) ? $_POST['nome'] : '' ?>">
                </div>
            </div>
            
            <?php $estilo_area = $mostrar_areas ? "" : 'style="display:none;"'; ?>
            <div id="interestTable">
                <div class="mergeRow dojoDndItem mergeRow-interests-checkboxes" <?= $estilo_area ?>>
                    <label>Interesse por Áreas</label>
                    <div class="field-group groups">
                    <?php $i = 1; foreach ($interests as $interest) : ?>
                        <div>
                            <label class="checkbox"">
                                <input type="checkbox" name="interesses[]" class="av-checkbox" <?= (($interest['nome'] == $area) || (isset($_POST['interesses']) && in_array($interest['id'], $_POST['interesses']))) ? "checked=checked" : "" ?> value="<?= $interest['id'] ?>">
                                <span><?= $interest['nome'] ?></span>
                            </label>
                        </div>
                    <?php $i++; endforeach ?>
                    </div>
                </div>
            </div>

            <div>
                <?= carregar_recaptcha() ?>
            </div>
        </div>

        <div class="submit_container clear">
            <input type="submit" class="formEmailButton emc-submit" name="submit" value="Inscreva-se">
        </div>
    </form>
</div>