<?php
KLoader::model("ProdutoModel");
KLoader::helper("UrlHelper");
KLoader::helper("StringHelper");

$q = isset($_GET['q']) ? htmlspecialchars($_GET['q'], ENT_QUOTES, 'UTF-8') : "";
$q2 = isset($_GET['q2']) ? htmlspecialchars($_GET['q2'], ENT_QUOTES, 'UTF-8') : "";
$tipo = isset($_GET['t']) ? htmlspecialchars($_GET['t'], ENT_QUOTES, 'UTF-8') : "";
$ordem = isset($_GET['o']) ? htmlspecialchars($_GET['o'], ENT_QUOTES, 'UTF-8') : "";
$p = isset($_GET['pg']) ? htmlspecialchars($_GET['pg'], ENT_QUOTES, 'UTF-8') : "1";
$offset = $p ? ($p - 1) * PESQUISA_PRODUTOS_POR_PAGINA : 0;

// $produtos = buscar_produtos($q, $offset, PESQUISA_PRODUTOS_POR_PAGINA, $tipo, $filtro, $ordem, $categorias, $somente_gratuitos);
// $total = contar_produtos($q, $tipo, $filtro, $categorias, $somente_gratuitos);

/**
 * Essa página é renderizada após chamada: get_pesquisa($titulo, $slug, $categorias = null, $filtro_combo = null, $ordenacao_combo = null, $somente_gratuitos = true)
 */
$filtros = [];

// inclui somente produtos publicados e visiveis
$filtros["status"] = [STATUS_POST_PUBLICADO];
$filtros["status"] = [STATUS_POST_PUBLICADO];
$filtros['somente_visiveis'] = true;

// define se serão buscados apenas os gratuitos (Material Grátis) ou todos (Pesquisa)
$filtros['somente_gratuitos'] = $somente_gratuitos;

// processa os tipos de conteúdo de material grátis
$filtros['incluir_audiobooks'] = ($slug == "audiobooks-gratis") ? true : false;
$filtros['incluir_cadernos'] = ($slug == "questoes-comentadas-gratis") ? true : false;
$filtros['incluir_mapas_mentais'] = ($slug == "mapas-mentais-gratis") ? true : false;
$filtros['somente_simulados'] = ($slug == "simulados-gratis") ? true : false;


// print_r($slug);

// processa filtros de texto (geral e local)
if($q) {
	$filtros['busca'][] = PesquisaHelper::tratar_input($q);
	$q = StringHelper::tratar_aspas( $q );
}

if($q2) {
	$filtros['busca'][] = PesquisaHelper::tratar_input($q2);
	$q2 = StringHelper::tratar_aspas( $q2 );
}

// processa o tipo
switch ($tipo) {
    case PESQUISA_TIPO_PACOTE: {
        $filtros['incluir_pacotes_cat'] = true; break;
    }
    
    case PESQUISA_TIPO_NAO_PACOTE: {
        $filtros['excluir_pacotes_cat'] = true; break;
    }
    
    case PESQUISA_TIPO_AUDIOBOOKS: {
        $filtros['incluir_audiobooks'] = true; break;
    }
    
    case PESQUISA_TIPO_CADERNOS: {
        $filtros['incluir_cadernos'] = true; break;
    }
    
    case PESQUISA_TIPO_MAPAS_MENTAIS: {
        $filtros['incluir_mapas_mentais'] = true; break;
    }
    
    case PESQUISA_TIPO_PDF: {
        $filtros['incluir_pdfs'] = true; break;
    }
    
    case PESQUISA_TIPO_SIMULADOS: {
        $filtros['somente_simulados'] = true; break;
    }
    
    case PESQUISA_TIPO_TURMA_COACHING: {
        $filtros['incluir_turma_coaching'] = true; break;
    }
    
    case PESQUISA_TIPO_VIDEOS: {
        $filtros['incluir_videos'] = true; break;
    }
}

// processa a ordem
$filtros['order_by'] = "pr.post_id DESC";
switch ($ordem) {
    case 0: {
        $filtros['order_by'] = "pr.post_id DESC"; break;
    }
    
    case PESQUISA_ORDEM_MENOR_PRECO: {
        $filtros['order_by'] = " pr.pro_preco ASC"; break;
    }
    
    case PESQUISA_ORDEM_MAIOR_PRECO: {
        $filtros['order_by'] = " pr.pro_preco DESC"; break;
    }
    
    case PESQUISA_ORDEM_A_Z: {
        $filtros['order_by'] = "po.post_title ASC"; break;
    }
    
    case PESQUISA_ORDEM_Z_A: {
        $filtros['order_by'] = "po.post_title DESC"; break;
    } 
}

$produtos = ProdutoModel::buscar($filtros, PESQUISA_PRODUTOS_POR_PAGINA, $offset);
$total = ProdutoModel::buscar_contar($filtros);

$por_pagina = PESQUISA_PRODUTOS_POR_PAGINA;
$pattern = "/{$slug}?q={$q}&q2={$q2}&t={$tipo}&o={$ordem}&pg=(:num)";

$paginator = new Paginator($total, $por_pagina, $p, $pattern);
?>

<script src="<?php tema_js_url('filtro-cursos.js') ?>"></script>

<div class="container pb-5 p-0">	
    <div class="p-0 row ml-auto mr-auto">	    	
    	<div class="p-0 col-11 ml-auto mr-auto col-md-5 col-xl-4 mt-3">
    
            <?php if(UrlHelper::is_url_pesquisa() || UrlHelper::is_url_todos_cursos()) : ?>
            	<?= get_nav_cursos() ?>				
            <?php else : ?>
            	<?= get_nav_material_gratis() ?>
            <?php endif; ?>
    	</div>
    	
    	
        <div class="p-0 col-12 col-md-7 col-xl-8">
        	<div class="mt-4 mt-md-0 col-12">
    			<h1><?= $titulo ?></h1>
        	</div>
        	<div class="col-12">				
        		<div class="row text-dark">		
        
            		<div class="col-12">
                		<div class="row ml-auto mr-auto mb-3 mt-3">
                			<div class="col p-0 text-right">
								<input class="pesquisa-barra pesquisa-barra-m form-control" type="text" id="busca2" placeholder="Procure por texto" value="<?= StringHelper::tratar_aspas( $q2 ) ?>" />
                			</div>
                			<div class="w-b-l p-0 text-left">	
                				<a href="#" id="texto-btn">		
                					<div class="pesquisa-barra-lupa pesquisa-barra-lupa-m">
                						<i class="fa fa-search mt-2 ml-2 text-white"></i>		
                					</div>
                				</a>
                			</div>
                		</div>    	
                	</div>

					<?php if($filtro_combo) :?>
            		<div class="mt-3 col-12 col-md-4">
            			<div class="font-10">Tipo:</div>
            			<div>
            				<?= get_combobox("tipo", $filtro_combo, $tipo) ?>
            			</div>
            		</div>
            		<?php endif; ?>
    		
            		<div class="mt-3 col-12 col-md-4">
            			<div class="font-10">Ordenação:</div>
            			<div>
            				<?= get_combobox("ordem", $ordenacao_combo, $ordem) ?>
            			</div>
            		</div>
            		
            		<div class="d-none d-md-block col-12 col-md-4 mt-4 pt-1 text-right <?= $filtro_combo ? '' : 'offset-md-4' ?>">
            			<a class="pointer" id="pesquisa-linha"><img width="50" src="<?= get_tema_image_url('view-linha.png') ?>"></a>
            			<a class="pointer" id="pesquisa-grid"><img  width="50" src="<?= get_tema_image_url('view-grid.png') ?>"></a>
            		</div>
    
    			</div>
    		</div>
		<div class="mt-3 col-12 text-justify">O Exponencial Concursos disponibiliza muitos materiais gratuitos, como simulados, aulões ao vivo, mapas mentais, questões online, audiobook e muito mais.
Aproveite para turbinar sua preparação gratuitamente.</div>
   	</div>
</div>

<div class="mt-5" id="carregando" style="display:none">
	<div class='text-center' style="font-size: 18px">
	   <div style="padding: 10px"><img width="40" src='/wp-content/themes/academy/images/carregando.gif'></div>
	   <div style="padding: 10px">Aguarde só mais um momento...</div>
	   <div style="padding: 10px">Estamos buscando no nosso banco de dados!</div>
	</div>
</div>

<div id="carregado">
    <div class="row mt-5">
    	<?php 
    		$total = numero_inteiro($total);
    		
    	    if($total == 1) {
    			$resultado_busca = "1 produto encontrado.";
    		}
    		else if($total > 1) {
    			$resultado_busca = "$total produtos encontrados.";	
    		}
    		else {
    			$resultado_busca = "<div class='alert alert-primary text-center'><strong>Atenção:</strong> Sua pesquisa não retornou resultados.<br>Confira algumas novidades do Exponencial Concursos.</div>";
	
				$filtros = [
				    'busca' => PesquisaHelper::ampliar_texto_busca($filtros['busca'] ?? null),
				    'order_by' => 'RAND()'
				];
				
				$produtos = ProdutoModel::buscar($filtros, PESQUISA_NAO_ENCONTRADA_PRODUTOS_POR_PAGINA, 0 );

				if(count($produtos) < 1)
				{
					$produtos = ProdutoModel::buscar(["incluir_pacotes" => true], PESQUISA_NAO_ENCONTRADA_PRODUTOS_POR_PAGINA, 0);
				}
    		}
    	?>
    
    	<?php if($total == 0) : ?>
        	<div class="col-md-12">
        		<?= $resultado_busca ?>
        	</div>
    	<?php else : ?>	
            <div class="text-dark font-10 mt-3 col-md-3">
            	<?= $resultado_busca ?>
            </div>
            	
            <div class="col-md-9">
            	<?= $paginator ?>
            </div>
        <?php endif; ?>
    </div>
    
    <div class="p-2 p-md-0 row ml-auto mr-auto list">
    	<?php foreach ($produtos as $item) : ?>
			<?php 
				$produto = wc_get_product($item->post_id);
				$carrinho_url = UrlHelper::get_adicionar_produto_ao_carrinho_url($produto->get_id(), FALSE);

                $ocultar_parcelamento = get_post_meta($item->post_id, PRODUTO_OCULTAR_PARCELAMENTO, true) !== ''
                    ? get_post_meta($item->post_id, PRODUTO_OCULTAR_PARCELAMENTO, true)
                    : 'no';
			?>
    		<div class="border rounded col-12 mt-4 pesquisa-box">
    			<div class="mt-3 text-dark pesquisa-cabecalho <?= is_pacote($produto) ? 'pesquisa-cabecalho-pacote' : '' ?>">
    				<div class="nome_prof font-weight-bold font-15 pesquisa-titulo"><?= $produto->get_title(); ?></div>
    				
    				<div class="mt-3 font-10 pesquisa-autores"><?= get_autores_str($produto->post, 'author', false, false, true); ?></div>
    			</div>
    			<div class="h-pesquisa-rodape p-0 mt-4 row pesquisa-rodape">
    				<div class="mt-auto mb-auto col-6 text-blue pesquisa-preco pr-md-0 p-3">
						<?php 
							
							if($produto->is_type("subscription") ){ 

								$preco_recorrente = ProdutoHelper::get_preco_recorrente( $produto ); 
								$preco = $preco_recorrente['preco'];
								$duracao = $preco_recorrente['duracao'];
								$sem_desconto = $preco_recorrente['sem_desconto'];
								$periodicidade_str1 = $preco_recorrente['periodicidade_str1'];
								$periodicidade_str2 = $preco_recorrente['periodicidade_str2'];
								
								if($duracao){
									if($sem_desconto > $preco){
										$preco_html = $duracao . "x de <del>" . moeda($sem_desconto) . "</del> " . moeda($preco) . " " . $periodicidade_str2;
									}else{
										$preco_html = $duracao . "x de " . moeda($sem_desconto) . " " . $periodicidade_str2;
									}
								}else{
									if($sem_desconto > $preco){
										$preco_html = "<del>" . moeda($sem_desconto) . "</del> " . moeda($preco) . " " . $periodicidade_str1;
									}else{
										$preco_html = moeda($sem_desconto) . " " . $periodicidade_str1;
									}
								}
		
								$ocultar_parcelamento = 'yes';
							}else{
								$preco_html = $produto->get_price_html();
							}
						?>

						<?php if($produto->get_price() > 0) : ?>
    						<span class="font-weight-bold font-15"><?= $preco_html ?></span>
						<?php else: ?>
							<span class="font-weight-bold font-15">Grátis</span>
						<?php endif; ?>
                        
    					<?php if($produto->get_price() > 0 &&  $ocultar_parcelamento == 'no') : ?>
    						<span class="d-block d-md-inline pesquisa-parcelado"><small> ou </small>12x <?= moeda($produto->get_price() / 12) ?> <span class="d-block d-md-inline">sem juros</span></span>
    					<?php endif ?>
    				</div>
    				<div class="p-0 pr-2 col-6 text-right mt-auto mb-auto">
						<a class="mt-1 mt-md-0 btn u-btn-blue" href="<?= get_post_permalink($produto->post) ?>">Saiba Mais</a>
						<?php if(is_usuario_logado()): ?>
							<?php if($produto->get_price() > 0) : ?>
								<a class="mt-1 mt-md-0 btn u-btn-primary" href="<?= $carrinho_url ?>">Comprar</a>
							<?php else : ?>
								<a class="mt-1 mt-md-0 btn btn-danger" href="<?= $carrinho_url ?>">Grátis</a>
							<?php endif ?>
						<?php else : ?>
							<?php if($produto->get_price() > 0) : ?>
								<a class="mt-1 mt-md-0 btn u-btn-primary login-modal-link" data-toggle="modal" data-target="#login-modal" href="#" data-redirect="<?= $carrinho_url ?>">Comprar</a>
							<?php else : ?>
								<a class="mt-1 mt-md-0 btn btn-danger login-modal-link" data-toggle="modal" data-target="#login-modal" href="#" data-redirect="<?= $carrinho_url ?>">Grátis</a>
							<?php endif ?>
						<?php endif ?>
    				</div>
    			</div>
    		</div>				
    	<?php endforeach ?>
    </div>
    
    <div class="ml-auto mr-auto row mt-5 mb-3">
    	<?= $paginator ?>
    </div>
</div> 