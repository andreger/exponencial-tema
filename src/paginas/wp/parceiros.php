<?php get_header ();?>

<div class="container pt-1 pt-md-5 pb-5">
<div class="col-12 text-center"><div>[expo_h1]</div>
<div class="col-12 parceiros-header text-blue text-center mt-3">
<h4><strong>Aqui vão algumas vantagens que você pode obter sendo alunos de nossos parceiros:</strong></h4>

</div>

<div class="row mt-3">
<div class="col-12 col-md-6 col-lg-4 mt-3">
<div class="col-12 box-amarelo-light rounded-4 parceiros-height">
	<div class="text-center parceiros-height-1">
		<img class="mt-4 img-fluid" src="/wp-content/themes/academy/images/parceiros-evp.png" />
	</div>
<div class="pt-3 border-top border-width-2 font-10 parceiros-height-2">
<p>Desconto concedido automaticamente no carrinho (*não incluído Coaching)</p>

<p class="text-justify">Alunos VIP1 a VIP10 - 15% de desconto<br>
Alunos VIP11 a VIP20 - 20% de desconto<br>
Alunos VIP21 a VIP30 - 25% de desconto<br>
Alunos VIP31 a VIP45 - 30% de desconto<br>
Alunos VIP46 e superiores - 50% de desconto</p>
</div>

<div class="pt-3 border-top border-width-2 font-10 col-12 text-center parceiros-height-3">
	<a class="btn u-btn-primary" href="/cursos-por-concurso">Veja nossos cursos</a>
</div>
</div>
</div>

<div class="col-12 col-md-6 col-lg-4 mt-3">
<div class="col-12 box-amarelo-light rounded-4 parceiros-height">
	<div class="text-center parceiros-height-1">
		<img class="mt-4 img-fluid" src="/wp-content/themes/academy/images/parceiros-ls.png"/>
	</div>
<div class="pt-3 border-top border-width-2 font-10 parceiros-height-2">
<p>Alunos da LS Ensino adquirem nossos produtos com descontos de 20%.</p>
<p class="text-justify">Não é preciso inserir cupom. Nosso site irá aplicar o desconto automaticamente ao adicionar ao carrinho de compras. Basta que faça o cadastro completo em nosso site e informe seus dados corretamente.</p>
</div>
<div class="pt-3 border-top border-width-2 font-10 col-12 text-center parceiros-height-3">
	<a class="btn u-btn-primary" href="/cursos-por-concurso">Veja nossos cursos</a>
</div>
</div>
</div>

<div class="col-12 col-md-6 col-lg-4 mt-3">
<div class="col-12 box-amarelo-light rounded-4 parceiros-height">
	<div class="ml-auto mr-auto col-9 text-center parceiros-height-1">
		<img class="img-fluid" src="/wp-content/themes/academy/images/parceiros-metodo.png"/>
	</div>

<div class="pt-3 border-top border-width-2 font-10 parceiros-height-2">
<p class="text-justify">Os alunos do Método de Estudo do Alexandre Meireles têm acesso a descontos exclusivos, basta solicitar o CUPOM de desconto ao Método de Estudos e usar em nosso site.</p>
</div>
<div class="pt-3 border-top border-width-2 font-10 col-12 text-center parceiros-height-3">
	<a class="btn u-btn-primary" href="/cursos-por-concurso">Veja nossos cursos</a>
</div>
</div>
</div>

<div class="col-12 col-md-6 col-lg-4 mt-3">
<div class="col-12 box-amarelo-light rounded-4 parceiros-height">
	<div class="ml-auto mr-auto col-8 text-center parceiros-height-1">
		<img class="mt-4 img-fluid" src="/wp-content/uploads/2017/08/metodo-coaching.png"/>
	</div>

<div class="pt-3 border-top border-width-2 font-10 parceiros-height-2">
<p class="text-justify">Os alunos do Método Coaching têm acesso a descontos exclusivos, basta solicitar o CUPOM de desconto ao seu coach e usar em nosso site.</p>
</div>
<div class="pt-3 border-top border-width-2 font-10 col-12 text-center parceiros-height-3">
	<a class="btn u-btn-primary" href="/cursos-por-concurso">Veja nossos cursos</a>
</div>
</div>
</div>
</div>
</div>


<?php get_footer(); ?>