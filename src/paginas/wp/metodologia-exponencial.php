<div>[rev_slider nossa-metodologia]</div>

<div class="col-12 mt-5">
[expo_h1]
</div>

<div class="container pt-3 pb-3">
<div class="row">
<div class="col-12 col-lg-5 col-xl-6">
<div class="row">
<div><img class="pl-3 pl-md-0" title="" src="/wp-content/themes/academy/images/esquematizacao.png" alt="" width="" height="" /></div>
<div class="col font-16 text-darkblue font-calibri font-weight-bold line-height-1-5">Esquematização, mapas mentais e teoria estruturada.</div>
</div>
<div class="row mt-4">
<div><img class="pl-3 pl-md-0" title="" src="/wp-content/themes/academy/images/tabelas.png" alt="" width="" height="" /></div>
<div class="col font-16 text-darkblue font-calibri font-weight-bold line-height-1-5">Tabelas comparativas, gráficos e fluxogramas</div>
</div>
<div class="row mt-4">
<div><img class="pl-3 pl-md-0" title="" src="/wp-content/themes/academy/images/feedback.png" alt="" width="" height="" /></div>
<div class="col font-16 text-darkblue font-calibri font-weight-bold line-height-1-5">Questões comentadas, dúvidas tiradas direto com o professor.</div>
</div>
<div class="row mt-4">
<div><img class="pl-3 pl-md-0" title="" src="/wp-content/themes/academy/images/completo.png" alt="" width="" height="" /></div>
<div class="col font-16 text-darkblue font-calibri font-weight-bold line-height-1-5">Completo, objetivo e focado.</div>
</div>
</div>
<div class="d-none d-md-block ml-auto mr-auto col-10 col-lg-7 col-xl-6 mt-3 mt-lg-0 h-metodologia"><img class="img-fluid" src="/wp-content/themes/academy/images/banner.png" />
<div class="font-14 text-metodologia"><b class="text-darkblue font-18 font-arial line-height-1-2">Economize
seu tempo</b><br>
<span class="text-blue">
Aumente<br>
EXPONENCIALMENTE<br>
suas chances de<br>
passar em um<br>
concurso público.<br>
</span>
<a class="mt-2 font-weight-bold pl-3 pr-3 btn u-btn-blue" href="/cursos-online">Veja nossos cursos</a></div>
</div>
<div class="d-block d-md-none col-12 mt-3 mt-lg-0">
<div class="font-14 bg-metodologia rounded col-8 p-4 ml-auto mr-auto border"><b class="text-darkblue font-18 font-arial line-height-1-2">Economize
seu tempo</b><br>
<span class="text-blue">
Aumente<br>
EXPONENCIALMENTE<br>
suas chances de<br>
passar em um<br>
concurso público.<br>
</span>
<a class="mt-2 font-weight-bold pl-3 pr-3 btn u-btn-blue" href="/cursos-online">Veja nossos cursos</a></div>
</div>
</div>
</div>



<div class="section_content metabg mt-5">
<div class="container pb-5">
<div class="col-12 pt-4">
	<div class="col-12 font-pop text-center pt-1 pb-3">Mais informações</div>
<div class="text-darkblue font-20 font-calibri font-weight-bold">Vantagens</div>
<ul class="mt-2">
    <li class="pl-3">Material focado, objetivo e completo</li>
    <li class="pl-3">Professores de grande destaque, casos vivos de sucesso</li>
    <li class="pl-3">Aprendizagem facilitada</li>
    <li class="pl-3">Memorização simples e estimulada</li>
    <li class="pl-3">Rápida revisão</li>
    <li class="pl-3">Técnicas de leitura dinâmica</li>
    <li class="pl-3">Economia de tempo (você vai ler apenas o que interessa, sem prolixidade)</li>
    <li class="pl-3">Potencialização da memorização visual</li>
    <li class="pl-3">Menos tempo gasto para montar resumos/esquemas e organizando o conteúdo.</li>
</ul>
</div>
<div class="col-12 pt-2">
<div class="text-darkblue font-20 font-calibri font-weight-bold">Sobre a metodologia</div>
<div class="text-justify text-dark line-height-1-2 pt-2">Nossos cursos são elaborados com muitas esquematizações, mapas mentais, tabelas comparativas, gráficos e fluxogramas, além de inúmeras outras técnicas de estudos empregadas para aumentar o seu poder de fixação e estímulo a sua atenção concentrada, empregadas para <b class="text-darkblue font-14 font-calibri">FACILITAR e ACELERAR todas as etapas de seu estudo</b>, que inclui: <b class="text-blue font-14 font-calibri">Aprendizagem, Assimilação, Fixação, Memorização e Revisão.</b> Ao aplicar as esquematizações, reduzimos em até 80% a quantidade de palavras que são lidas, além de ter uma apresentação visual muito mais agradável do que inúmeras linhas de texto puro, o que facilita o seu estudo. A lógica é simples: <b>se você gasta menos tempo lendo o que não importa, sobra mais tempo para o que é realmente importante!</b> Textos enormes aqui no Exponencial são transformados em mapas mentais, gráficos, tabelas comparativas, fluxogramas e outras esquematizações possíveis. Tudo isso pensando em como <b class="text-darkblue font-14 font-calibri">FACILITAR A SUA VIDA!</b></div>
</div>
<div class="col-12 pt-2">
<div class="text-darkblue font-20 font-calibri font-weight-bold pt-2">Público indicado</div>
<div class="text-justify text-dark line-height-1-5">Nossos cursos são indicados para <b>TODOS os níveis</b> que o aluno pode se encontrar (iniciante, intermediário e avançado). Isto porque temos uma didática que permite que você ENTENDA, ASSIMILE, MEMORIZE, APROFUNDE e REVISE o conteúdo. Nossos cursos são ótimos para o aluno que está procurando algo para permitir rápidas revisões, pois além dos destaques e pontos focais de atenção utilizados, ele está estruturado para permitir a leitura dinâmica (acelerada) do conteúdo, conseguindo se ater aos pontos chaves de cada assunto. E para quem está começando, é o caminho mais indicado para que você comece corretamente e torne sua trajetória de estudos de forma mais efetiva com técnicas adequadas de estudo.</div>
</div>
</div>
</div>