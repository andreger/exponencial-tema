<?php get_header(); ?>
<div class="container-fluid pt-1 pt-md-5 pb-5">
<div class="col-12"> 
	<div class="col-12">
		[expo_h1]		
	</div>
</div>	
<div class="col-12 p-compatativo-0"> 
	<div class="col-12 mt-5 text-center">
		<img width="35" src="/wp-content/themes/academy/images/coaching2-icon.png"><div class="font-pop col-12">Comparativo entre os programas de coaching</div>		
	</div>
<div class="container p-compatativo-0">	

	<div id="carouselExampleControls" class="d-block d-md-none mt-4 carousel slide" data-ride="carousel">
  <div class="carousel-inner">    
    <div class="carousel-item active">
      <div class="col-12 col-md-6 col-lg-3 pb-4">
			<div class="bg-gray rounded rounded-5">
				<div class="contarativo-height-1 col-12 text-uppercase text-blue font-13 font-weight-bold text-center"> 
					<img class="mt-4" width="45" src="/wp-content/themes/academy/images/mentoria.png">
					<div class="mt-4">Coaching/mentoria</div>
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary border-width-2 text-center">
					<div class="mt-1 text-blue font-13 font-weight-bold">Periodicidade</div>	
					<div>1, 3 ou 6 meses, renovável pelo período que desejar</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-1 mt-lg-2 mt-xl-4 text-blue font-13 font-weight-bold">Possiu coach/mentor?</div>	
					<div class="mt-2">Sim</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-1 text-blue font-13 font-weight-bold">Whatsapp</div>	
					<div class="mt-2">Sim</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-1 text-blue font-13 font-weight-bold">Interação</div>
					<div class="mt-2">Individual</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center p-0">				<div class="mt-5 text-blue font-13 font-weight-bold">
					Ciclo de Estudo</div>	
					<div class="mt-1 mt-lg-0 mt-xl-3 p-0">
					Personalizado</div>					
				</div>
					<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center p-0">				<div class="mt-1 text-blue font-13 font-weight-bold">
					Metas</div>	
					<div class="mt-1 mt-lg-0 mt-xl-3 p-0">
					Por concurso(s) que desejar, com acompanhamento personalizado e direcionamento detalhado<br>Leva em contatodos os aspectos da sua rotina (horários e atividades</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-1 mt-lg-0 mt-xl-3 text-blue font-13 font-weight-bold">Mudança do concurso de estudo (foco)</div>		
					<div>Sim</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center p-0">				<div class="mt-4 text-blue font-13 font-weight-bold">
					Técnicas de Estudo</div>	
					<div class="mt-1 mt-lg-0 mt-xl-3 p-0">
					Sim, inclusive com correção do uso pelo aluno e passo a passo para aumentar o aprendizado</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center p-0">				<div class="mt-4 text-blue font-13 font-weight-bold">
					Analise vertical do edital (ponto a ponto)</div>	
					<div class="mt-1 mt-lg-0 mt-xl-3 p-0">
					Sim, inclusive pós edital</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-3 text-blue font-13 font-weight-bold">Contempla Pós edital?</div>		
					<div>Sim</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-3 text-blue font-13 font-weight-bold">Foco</div>		
					<div>Sim</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center p-0">				<div class="mt-1 text-blue font-13 font-weight-bold">
					Material de Estudo (cursos, livros)</div>	
					<div class="mt-1 mt-lg-0 mt-xl-3 p-0">
					Indicação imparcial e personalizada, com base no perfil psicopedagógico do aluno, materiais que já possuir e foco escolhido. Não interfere no Coachingo</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-3 mt-lg-0 mt-xl-3 text-blue font-13 font-weight-bold">Descontos no Exponencial</div>		
					<div>Sim</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-3 mt-lg-0 mt-xl-3 text-blue font-13 font-weight-bold">Sistema de Questões</div>		
					<div>Sim</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-3 mt-lg-0 mt-xl-3 text-blue font-13 font-weight-bold">Cadernos de Questão</div>		
					<div>Sim</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center p-0">				<div class="mt-4 text-blue font-13 font-weight-bold">
					Simulados</div>	
					<div class="mt-1 mt-lg-0 mt-xl-3 p-0">
					Sim, designados pelo coach conforme necessidade de cada aluno</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center">						
				</div>
			</div>
		</div>
    </div>
    <div class="carousel-item">
     <div class="col-12 col-md-6 col-lg-3 pb-4">
			<div class="bg-gray rounded rounded-5">
				<div class="contarativo-height-1 col-12 text-uppercase text-blue font-13 font-weight-bold text-center"> 
					<img class="mt-3" width="50" src="/wp-content/themes/academy/images/turma.png">
					<div class="mt-2">Tuma de Coaching e (reta final)</div>
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary border-width-2 text-center">
					<div class="mt-1 text-blue font-13 font-weight-bold">Periodicidade</div>	
					<div>Variável, depende da perspectiva de prova pro concurso</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-1 mt-lg-2 mt-xl-4 text-blue font-13 font-weight-bold">Possiu coach/mentor?</div>	
					<div class="mt-2">Sim</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-1 text-blue font-13 font-weight-bold">Whatsapp</div>	
					<div class="mt-2">Sim</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-1 text-blue font-13 font-weight-bold">Interação</div>
					<div class="mt-2">Em grupo de no máximo 6 pessoas</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center p-0">				<div class="mt-5 text-blue font-13 font-weight-bold">
					Ciclo de Estudo</div>	
					<div class="mt-1 mt-lg-0 mt-xl-3 p-0">
					Sugerido</div>					
				</div>
					<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center p-0">				<div class="mt-1 text-blue font-13 font-weight-bold">
					Metas</div>	
					<div class="mt-1 mt-lg-0 mt-xl-3 p-0">
					Foco exclusivo (100%) no concurso (ou área) escolhido(a), com direcionamento e metas extremamente detalhadas, indo no nível dos assuntos e seus pontos chaves, de cada disciplina</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-1 mt-lg-0 mt-xl-3 text-blue font-13 font-weight-bold">Mudança do concurso de estudo (foco)</div>		
					<div>Não</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center p-0">				<div class="mt-5 text-blue font-13 font-weight-bold">
					Técnicas de Estudo</div>	
					<div class="mt-1 mt-lg-0 mt-xl-3 p-0">
					Sim</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center p-0">				<div class="mt-4 text-blue font-13 font-weight-bold">
					Analise vertical do edital (ponto a ponto)</div>	
					<div class="mt-1 mt-lg-0 mt-xl-3 p-0">
					Sim, inclusive pós edital</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-1 text-blue font-13 font-weight-bold">Contempla Pós edital?</div>		
					<div>Sim, apenas do concurso em foco durante a vigência do treinamento</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-3 text-blue font-13 font-weight-bold">Foco</div>		
					<div>Curto prazo</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center p-0">				<div class="mt-1 text-blue font-13 font-weight-bold">
					Material de Estudo (cursos, livros)</div>	
					<div class="mt-1 mt-lg-0 mt-xl-3 p-0">
					Indicação imparcial com base no perfil psicopedagógico do aluno, materiais que já possuir e foco escolhido. Não interfere no Treinamento Intensivo</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-3 mt-lg-0 mt-xl-3 text-blue font-13 font-weight-bold">Descontos no Exponencial</div>		
					<div>Sim</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-3 mt-lg-0 mt-xl-3 text-blue font-13 font-weight-bold">Sistema de Questões</div>		
					<div>Sim</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-3 mt-lg-0 mt-xl-3 text-blue font-13 font-weight-bold">Cadernos de Questão</div>		
					<div>Sim</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center p-0">				<div class="mt-4 text-blue font-13 font-weight-bold">
					Simulados</div>	
					<div class="mt-1 mt-lg-0 mt-xl-3 p-0">
					Sim, quantidade será informada na descrição do serviço</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center">						
				</div>
			</div>
		</div>	
    </div>
  </div>
  <a id="carousel-compatativo" class="cw-10 carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="cw-10 carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

<div class="row mt-4 d-none d-md-flex d-lg-none">
<div class="col-6 pb-4">
			<div class="bg-gray rounded rounded-5">
				<div class="contarativo-height-1 col-12 text-uppercase text-blue font-13 font-weight-bold text-center"> 
					<img class="mt-4" width="40" src="/wp-content/themes/academy/images/programa.png">
					<div class="mt-4 mt-lg-2">O que o programa contempla?</div>
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary border-width-2 text-center">
					<div class="mt-4 text-blue font-13 font-weight-bold">Periodicidade</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-4 mt-lg-2 mt-xl-3 text-blue font-13 font-weight-bold">Possiu coach/mentor?</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-4 text-blue font-13 font-weight-bold">Whatsapp</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-4 text-blue font-13 font-weight-bold">Interação</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-4 text-blue font-13 font-weight-bold">
					Ciclo de Estudo</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="pt-3"></div>
					<div class="mt-5 text-blue font-13 font-weight-bold">
					Metas</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-3 mt-lg-0 mt-xl-2 text-blue font-13 font-weight-bold">Mudança do concurso de estudo (foco)</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="pt-3"></div>
					<div class="mt-5 text-blue font-13 font-weight-bold">
					Técnicas de Estudo</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="pt-1"></div>
					<div class="mt-5 text-blue font-13 font-weight-bold">
					Analise vertical do edital (ponto a ponto)</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="pt-1"></div>
					<div class="mt-3 text-blue font-13 font-weight-bold">
					Contempla Pós edital?</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="pt-1"></div>
					<div class="mt-4 text-blue font-13 font-weight-bold">
					Foco</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="pt-3"></div>
					<div class="mt-5 text-blue font-13 font-weight-bold">
					Material de Estudo (cursos, livros)</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-3 text-blue font-13 font-weight-bold">
					Descontos no Exponencial</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="pt-1"></div>
					<div class="mt-3 text-blue font-13 font-weight-bold">
					Sistema de Questões</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="pt-1"></div>
					<div class="mt-3 text-blue font-13 font-weight-bold">
					Cadernos de Questão</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="pt-3"></div>
					<div class="mt-5 text-blue font-13 font-weight-bold">
					Simulados</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center">						
				</div>
			</div>
		</div>
<div id="carouselExampleControlss" class="col-6 carousel slide" data-ride="carousel">
  <div class="carousel-inner">    
    <div class="carousel-item active">
      <div class="col-12 pb-4">
			<div class="bg-gray rounded rounded-5">
				<div class="contarativo-height-1 col-12 text-uppercase text-blue font-13 font-weight-bold text-center"> 
					<img class="mt-4" width="45" src="/wp-content/themes/academy/images/mentoria.png">
					<div class="mt-4">Coaching/mentoria</div>
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary border-width-2 text-center">
					<div class="mt-3 mt-lg-1 mt-xl-3">1, 3 ou 6 meses, renovável pelo período que desejar</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-4">Sim</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-4">Sim</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-4">Individual</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-4">Personalizado</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center p-0">					
					<div class="mt-3 mt-lg-1 mt-xl-3 p-0">
					Por concurso(s) que desejar, com acompanhamento personalizado e direcionamento detalhado<br>Leva em contatodos os aspectos da sua rotina (horários e atividades)</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-4">Sim</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center p-0">					
					<div class="mt-5 p-0">
					Sim, inclusive com correção do uso pelo aluno e passo a passo para aumentar o aprendizado</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center p-0">		
				<div class="pt-3"></div>			
					<div class="mt-5 p-0">
					Sim, inclusive pós edital
					</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-4">Sim</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-4">Médio e longo prazo</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center p-0">	<div class="pt-1"></div>				
					<div class="mt-4 p-0">
					Indicação imparcial e personalizada, com base no perfil psicopedagógico do aluno, materiais que já possuir e foco escolhido. Não interfere no Coaching
					</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-4">Sim</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-4">Sim</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-4">Sim</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center p-0">					
					<div class="mt-5 p-0">
					Sim, designados pelo coach conforme necessidade de cada aluno
					</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center">						
				</div>
			</div>
		</div>
    </div>
    <div class="carousel-item">
     <div class="col-12 pb-4">
			<div class="bg-gray rounded rounded-5">
				<div class="contarativo-height-1 col-12 text-uppercase text-blue font-13 font-weight-bold text-center"> 
					<img class="mt-3" width="50" src="/wp-content/themes/academy/images/turma.png">
					<div class="mt-2">Tuma de Coaching e (reta final)</div>
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary border-width-2 text-center">
					<div class="mt-3 mt-lg-1 mt-xl-2">Variável, depende da perspectiva de prova pro concurso</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-4">Sim</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-4">Sim</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-4">Em grupo de no máximo 6 pessoas</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-4">Sugerido
					</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center p-0">				
					<div class="mt-2 p-0">
					Foco exclusivo (100%) no concurso (ou área) escolhido(a), com direcionamento e metas extremamente detalhadas, indo no nível dos assuntos e seus pontos chaves, de cada disciplina</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-4">Não</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="pt-3"></div>
					<div class="mt-5">Sim</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="pt-3"></div>
					<div class="mt-5">Sim, inclusive pós edital</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center p-0">				
					<div class="mt-3 mt-lg-1 mt-xl-3 p-0">
					Sim, apenas do concurso em foco durante a vigência do treinamento</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-4">Curto prazo</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center p-0">	<div class="pt-1"></div>			
					<div class="mt-4 p-0">
					Indicação imparcial com base no perfil psicopedagógico do aluno, materiais que já possuir e foco escolhido. Não interfere no Treinamento Intensivo
					</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-4">Sim</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-4">Sim</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-4">Sim</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center p-0">				
					<div class="mt-5 p-0">
					Sim, quantidade será informada na descrição do serviço
					</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center">						
				</div>
			</div>
		</div>	
    </div>
  </div>
  <a id="carousel-compatativo" class="carousel-control-prev" href="#carouselExampleControlss" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControlss" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
</div>



	<div class="mt-4 row pb-3 d-none d-lg-flex">		
		<div class="col-1 ml-md-0 ml-lg-5 d-none d-lg-block"></div>
		<div class="col-12 col-md-6 col-lg-3 pb-4">
			<div class="bg-gray rounded rounded-5">
				<div class="contarativo-height-1 col-12 text-uppercase text-blue font-13 font-weight-bold text-center"> 
					<img class="mt-4" width="40" src="/wp-content/themes/academy/images/programa.png">
					<div class="mt-4 mt-lg-2">O que o programa contempla?</div>
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary border-width-2 text-center">
					<div class="mt-4 text-blue font-13 font-weight-bold">Periodicidade</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-4 mt-lg-2 mt-xl-3 text-blue font-13 font-weight-bold">Possiu coach/mentor?</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-4 text-blue font-13 font-weight-bold">Whatsapp</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-4 text-blue font-13 font-weight-bold">Interação</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-4 text-blue font-13 font-weight-bold">
					Ciclo de Estudo</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="pt-3"></div>
					<div class="mt-5 text-blue font-13 font-weight-bold">
					Metas</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-3 mt-lg-0 mt-xl-2 text-blue font-13 font-weight-bold">Mudança do concurso de estudo (foco)</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="pt-3"></div>
					<div class="mt-5 text-blue font-13 font-weight-bold">
					Técnicas de Estudo</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="pt-1"></div>
					<div class="mt-5 text-blue font-13 font-weight-bold">
					Analise vertical do edital (ponto a ponto)</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="pt-1"></div>
					<div class="mt-3 text-blue font-13 font-weight-bold">
					Contempla Pós edital?</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="pt-1"></div>
					<div class="mt-4 text-blue font-13 font-weight-bold">
					Foco</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="pt-3"></div>
					<div class="mt-5 text-blue font-13 font-weight-bold">
					Material de Estudo (cursos, livros)</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-3 text-blue font-13 font-weight-bold">
					Descontos no Exponencial</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="pt-1"></div>
					<div class="mt-3 text-blue font-13 font-weight-bold">
					Sistema de Questões</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="pt-1"></div>
					<div class="mt-3 text-blue font-13 font-weight-bold">
					Cadernos de Questão</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="pt-3"></div>
					<div class="mt-5 text-blue font-13 font-weight-bold">
					Simulados</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center">						
				</div>
			</div>
		</div>
		<div class="col-12 col-md-6 col-lg-3 pb-4">
			<div class="bg-gray rounded rounded-5">
				<div class="contarativo-height-1 col-12 text-uppercase text-blue font-13 font-weight-bold text-center"> 
					<img class="mt-4" width="45" src="/wp-content/themes/academy/images/mentoria.png">
					<div class="mt-4">Coaching/mentoria</div>
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary border-width-2 text-center">
					<div class="mt-3 mt-lg-1 mt-xl-3">1, 3 ou 6 meses, renovável pelo período que desejar</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-4">Sim</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-4">Sim</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-4">Individual</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-4">Personalizado</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center p-0">					
					<div class="mt-3 mt-lg-1 mt-xl-3 p-0">
					Por concurso(s) que desejar, com acompanhamento personalizado e direcionamento detalhado<br>Leva em contatodos os aspectos da sua rotina (horários e atividades)</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-4">Sim</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center p-0">					
					<div class="mt-3 mt-lg-5 p-0">
					Sim, inclusive com correção do uso pelo aluno e passo a passo para aumentar o aprendizado</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center p-0">		
				<div class="pt-3"></div>			
					<div class="mt-3 mt-lg-5 p-0">
					Sim, inclusive pós edital
					</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-4">Sim</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-4">Médio e longo prazo</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center p-0">					
					<div class="mt-3 mt-lg-4 mt-xl-3 p-0">
					Indicação imparcial e personalizada, com base no perfil psicopedagógico do aluno, materiais que já possuir e foco escolhido. Não interfere no Coaching
					</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-4">Sim</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-4">Sim</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-4">Sim</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center p-0">					
					<div class="mt-3 mt-lg-5 p-0">
					Sim, designados pelo coach conforme necessidade de cada aluno
					</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center">						
				</div>
			</div>
		</div>
		<div class="col-12 col-md-6 col-lg-3 pb-4">
			<div class="bg-gray rounded rounded-5">
				<div class="contarativo-height-1 col-12 text-uppercase text-blue font-13 font-weight-bold text-center"> 
					<img class="mt-3" width="50" src="/wp-content/themes/academy/images/turma.png">
					<div class="mt-2">Tuma de Coaching e (reta final)</div>
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary border-width-2 text-center">
					<div class="mt-3 mt-lg-1 mt-xl-2">Variável, depende da perspectiva de prova pro concurso</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-4">Sim</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-4">Sim</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-4">Em grupo de no máximo 6 pessoas</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-4">Sugerido
					</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center p-0">				
					<div class="mt-3 mt-lg-1 mt-xl-2 p-0">
					Foco exclusivo (100%) no concurso (ou área) escolhido(a), com direcionamento e metas extremamente detalhadas, indo no nível dos assuntos e seus pontos chaves, de cada disciplina</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-4">Não</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="pt-3"></div>
					<div class="mt-5">Sim</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="pt-3"></div>
					<div class="mt-5">Sim, inclusive pós edital</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center p-0">				
					<div class="mt-3 mt-lg-1 mt-xl-3 p-0">
					Sim, apenas do concurso em foco durante a vigência do treinamento</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-4">Curto prazo</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center p-0">				
					<div class="mt-3 mt-lg-3 p-0">
					Indicação imparcial com base no perfil psicopedagógico do aluno, materiais que já possuir e foco escolhido. Não interfere no Treinamento Intensivo
					</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-4">Sim</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-4">Sim</div>					
				</div>
				<div class="contarativo-height-2 ml-auto mr-auto col-10 border-top border-secondary text-center">
					<div class="mt-4">Sim</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center p-0">				
					<div class="mt-3 mt-lg-5 p-0">
					Sim, quantidade será informada na descrição do serviço
					</div>					
				</div>
				<div class="contarativo-height-3 ml-auto mr-auto col-10 border-top border-secondary text-center">						
				</div>
			</div>
		</div>			
	</div>
</div>	
</div>
</div>
<?php get_footer(); ?>	