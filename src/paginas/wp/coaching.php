<div class="container-fluid p-0">
<div class="pt-1 pt-md-5"></div>
<div class="pb-3 col-12 font-coaching">
[expo_h1]</div>
[rev_slider coaching]
[botao]
<div class="col-12 mt-4 text-center">
<div class="font-pop">O que os alunos falam?</div>
</div>
[rev_slider depoimentos-home]
<div class="col-12 mt-4 text-center"><img src="/wp-content/themes/academy/images/coaching-icon.png" width="45" />
<div class="font-pop">Vagas Abertas</div>
</div>
<div id="accordion" class="container p-2 p-md-0">
<div class="card">
<div id="headingOne" class="card-header">
<h5 class="mb-0"><button class="btn btn-link font-xs-16 font-20 text-blue" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
Área Fiscal
</button></h5>
</div>
<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
<div class="card-body">
<div class="row bg-gray rounded p-3">
<div class="col-12 col-lg-1 text-center"><img src="/wp-content/themes/academy/images/coaching2-area-fiscal.png" width="50" /></div>
<div class="ml-3 ml-md-5 ml-lg-0 p-0 mt-3 mt-lg-0 col-12 col-lg-2 coaching-concurso line-height-2 font-11 text-dark">
<ul class="ml-auto mr-auto ml-lg-0 mr-lg-0 col-8 col-md-4 col-lg-12">
  <li>Receita Federal</li>
  <li>ICMS</li>
  <li>ISS</li>
  <li>Auditor-Fiscal do Trabalho</li>
</ul>
</div>
<div class="p-0 col-12 col-md-4 col-lg-3">
<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
<div class="row">
<div class="col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/c1-box.png" width="45" /></div>
<div class="pl-1 pl-lg-3 col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/coaching-box.png" width="70" />
<p class="text-white mb-2 font-12">COACHING</p>

</div>
</div>
<div class="bg-aqua-box text-white font-22 text-center p-1">MENSAL</div>
<div class="mt-3 text-white font-18 text-center p-0">R$ 700,00
<a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a></div>
</div>
</div>
<div class="p-0 col-12 col-md-4 col-lg-3 mt-2 mt-md-0">
<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
<div class="row">
<div class="col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/c3-box.png" width="58" /></div>
<div class="pl-1 pl-lg-3 col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/coaching-box.png" width="70" />
<p class="text-white mb-2 font-12">COACHING</p>

</div>
</div>
<div class="bg-aqua-box text-white font-22 text-center p-1">TRIMESTRAL</div>
<div class="line-height-1-2 mt-1 text-white font-13 text-center p-0">

<span style="text-decoration: line-through;">R$ 2.100,00</span> R$ 1.680,00 <span class="font-11">economia de</span> R$ 420,00
20% OFF

<a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a>

</div>
</div>
</div>
<div class="p-0 col-12 col-md-4 col-lg-3 mt-2 mt-md-0">
<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
<div class="row">
<div class="col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/c6-box.png" width="58" /></div>
<div class="pl-1 pl-lg-3 col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/coaching-box.png" width="70" />
<p class="text-white mb-2 font-12">COACHING</p>

</div>
</div>
<div class="bg-aqua-box text-white font-22 text-center p-1">SEMESTRAL</div>
<div class="line-height-1-2 mt-1 text-white font-13 text-center p-0">

<span style="text-decoration: line-through;">R$ 4.200,00</span> R$ 2.730,00 <span class="font-11">economia de</span> R$ 1.470,00
35% OFF

<a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a>

</div>
</div>
</div>
<div class="col-12 mt-5 text-right">
<h5 class="mb-0">VER MAIS COACHING</h5>
&nbsp;

</div>
</div>
</div>
</div>
<div class="card">
<div id="headingTwo" class="card-header">
<h5 class="mb-0"><button class="btn btn-link font-xs-16 font-20 text-blue collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
Área de Controle
</button></h5>
</div>
<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
<div class="card-body">
<div class="row bg-gray rounded p-3">
<div class="col-12 col-lg-1 text-center"><img src="/wp-content/themes/academy/images/coaching2-area-fiscal.png" width="50" /></div>
<div class="ml-3 ml-md-5 ml-lg-0 p-0 mt-3 mt-lg-0 col-12 col-lg-2 coaching-concurso line-height-2 font-11 text-dark">
<ul class="ml-auto mr-auto ml-lg-0 mr-lg-0 col-8 col-md-4 col-lg-12">
  <li>TCU</li>
  <li>CGU</li>
  <li>Tribunais de Contas e Controladorias Estaduais
e Municipais</li>
  <li>TCM RJ</li>
</ul>
</div>
<div class="p-0 col-12 col-md-4 col-lg-3">
<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
<div class="row">
<div class="col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/c1-box.png" width="45" /></div>
<div class="pl-1 pl-lg-3 col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/coaching-box.png" width="70" />
<p class="text-white mb-2 font-12">COACHING</p>

</div>
</div>
<div class="bg-aqua-box text-white font-22 text-center p-1">MENSAL</div>
<div class="mt-3 text-white font-18 text-center p-0">R$ 700,00
<a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a></div>
</div>
</div>
<div class="p-0 col-12 col-md-4 col-lg-3 mt-2 mt-md-0">
<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
<div class="row">
<div class="col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/c3-box.png" width="58" /></div>
<div class="pl-1 pl-lg-3 col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/coaching-box.png" width="70" />
<p class="text-white mb-2 font-12">COACHING</p>

</div>
</div>
<div class="bg-aqua-box text-white font-22 text-center p-1">TRIMESTRAL</div>
<div class="line-height-1-2 mt-1 text-white font-13 text-center p-0">

<span style="text-decoration: line-through;">R$ 2.100,00</span> R$ 1.680,00 <span class="font-11">economia de</span> R$ 420,00
20% OFF

<a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a>

</div>
</div>
</div>
<div class="p-0 col-12 col-md-4 col-lg-3 mt-2 mt-md-0">
<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
<div class="row">
<div class="col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/c6-box.png" width="58" /></div>
<div class="pl-1 pl-lg-3 col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/coaching-box.png" width="70" />
<p class="text-white mb-2 font-12">COACHING</p>

</div>
</div>
<div class="bg-aqua-box text-white font-22 text-center p-1">SEMESTRAL</div>
<div class="line-height-1-2 mt-1 text-white font-13 text-center p-0">

<span style="text-decoration: line-through;">R$ 4.200,00</span> R$ 2.730,00 <span class="font-11">economia de</span> R$ 1.470,00
35% OFF

<a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a>

</div>
</div>
</div>
<div class="col-12 mt-5 text-right">
<h5 class="mb-0">VER MAIS COACHING</h5>
&nbsp;

</div>
</div>
</div>
</div>
<div class="card">
<div id="headingThree" class="card-header">
<h5 class="mb-0"><button class="btn btn-link font-xs-16 font-20 text-blue collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
Área Policial
</button></h5>
</div>
<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
<div class="card-body">
<div class="row bg-gray rounded p-3">
<div class="col-12 col-lg-1 text-center"><img src="/wp-content/themes/academy/images/coaching2-area-fiscal.png" width="50" /></div>
<div class="ml-0 ml-md-4 ml-lg-0 p-0 mt-3 mt-lg-0 col-12 col-lg-2 coaching-concurso line-height-2 font-11 text-dark">
<ul class="ml-auto mr-auto ml-lg-0 mr-lg-0 col-8 col-md-4 col-lg-12">
  <li>Polícia Federal</li>
  <li>Polícia Rodoviária Federal</li>
  <li>Polícias Civil e Militar</li>
</ul>
</div>
<div class="p-0 col-12 col-md-4 col-lg-3">
<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
<div class="row">
<div class="col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/c1-box.png" width="45" /></div>
<div class="pl-1 pl-lg-3 col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/coaching-box.png" width="70" />
<p class="text-white mb-2 font-12">COACHING</p>

</div>
</div>
<div class="bg-aqua-box text-white font-22 text-center p-1">MENSAL</div>
<div class="mt-3 text-white font-18 text-center p-0">R$ 560,00
<a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a></div>
</div>
</div>
<div class="p-0 col-12 col-md-4 col-lg-3 mt-2 mt-md-0">
<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
<div class="row">
<div class="col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/c3-box.png" width="58" /></div>
<div class="pl-1 pl-lg-3 col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/coaching-box.png" width="70" />
<p class="text-white mb-2 font-12">COACHING</p>

</div>
</div>
<div class="bg-aqua-box text-white font-22 text-center p-1">TRIMESTRAL</div>
<div class="line-height-1-2 mt-1 text-white font-13 text-center p-0">

<span style="text-decoration: line-through;">R$ 1.680,00</span> R$ 1.344,00 <span class="font-11">economia de</span> R$ 336,00
20% OFF

<a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a>

</div>
</div>
</div>
<div class="p-0 col-12 col-md-4 col-lg-3 mt-2 mt-md-0">
<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
<div class="row">
<div class="col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/c6-box.png" width="58" /></div>
<div class="pl-1 pl-lg-3 col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/coaching-box.png" width="70" />
<p class="text-white mb-2 font-12">COACHING</p>

</div>
</div>
<div class="bg-aqua-box text-white font-22 text-center p-1">SEMESTRAL</div>
<div class="line-height-1-2 mt-1 text-white font-13 text-center p-0">

<span style="text-decoration: line-through;">R$ 3.360,00</span> R$ 2.184,00 <span class="font-11">economia de</span> R$ 1.176,00
35% OFF

<a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a>

</div>
</div>
</div>
<div class="col-12 mt-5 text-right">
<h5 class="mb-0">VER MAIS COACHING</h5>
&nbsp;

</div>
</div>
</div>
</div>
<div class="card">
<div id="headingFour" class="card-header">
<h5 class="mb-0"><button class="btn btn-link font-xs-16 font-20 text-blue collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
Tribunais (Analista e Técnico)
</button></h5>
</div>
<div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
<div class="card-body">
<div class="row bg-gray rounded p-3">
<div class="col-12 col-lg-1 text-center"><img src="/wp-content/themes/academy/images/coaching2-area-fiscal.png" width="50" /></div>
<div class="ml-2 ml-md-4 ml-lg-0 p-0 mt-3 mt-lg-0 col-12 col-lg-2 coaching-concurso line-height-2 font-11 text-dark">
<ul class="ml-auto mr-auto ml-lg-0 mr-lg-0 col-3 col-md-2 col-lg-12">
  <li>TRT</li>
  <li>TRE</li>
  <li>TRF</li>
  <li>TJ</li>
</ul>
</div>
<div class="p-0 col-12 col-md-4 col-lg-3">
<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
<div class="row">
<div class="col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/c1-box.png" width="45" /></div>
<div class="pl-1 pl-lg-3 col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/coaching-box.png" width="70" />
<p class="text-white mb-2 font-12">COACHING</p>

</div>
</div>
<div class="bg-aqua-box text-white font-22 text-center p-1">MENSAL</div>
<div class="mt-3 text-white font-18 text-center p-0">R$ 560,00
<a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a></div>
</div>
</div>
<div class="p-0 col-12 col-md-4 col-lg-3 mt-2 mt-md-0">
<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
<div class="row">
<div class="col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/c3-box.png" width="58" /></div>
<div class="pl-1 pl-lg-3 col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/coaching-box.png" width="70" />
<p class="text-white mb-2 font-12">COACHING</p>

</div>
</div>
<div class="bg-aqua-box text-white font-22 text-center p-1">TRIMESTRAL</div>
<div class="line-height-1-2 mt-1 text-white font-13 text-center p-0">

<span style="text-decoration: line-through;">R$ 1.680,00</span> R$ 1.344,00 <span class="font-11">economia de</span> R$ 336,00
20% OFF

<a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a>

</div>
</div>
</div>
<div class="p-0 col-12 col-md-4 col-lg-3 mt-2 mt-md-0">
<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
<div class="row">
<div class="col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/c6-box.png" width="58" /></div>
<div class="pl-1 pl-lg-3 col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/coaching-box.png" width="70" />
<p class="text-white mb-2 font-12">COACHING</p>

</div>
</div>
<div class="bg-aqua-box text-white font-22 text-center p-1">SEMESTRAL</div>
<div class="line-height-1-2 mt-1 text-white font-13 text-center p-0">

<span style="text-decoration: line-through;">R$ 3.360,00</span> R$ 2.184,00 <span class="font-11">economia de</span> R$ 1.176,00
35% OFF

<a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a>

</div>
</div>
</div>
<div class="col-12 mt-5 text-right">
<h5 class="mb-0">VER MAIS COACHING</h5>
&nbsp;

</div>
</div>
</div>
</div>
<div class="card">
<div id="headingFive" class="card-header">
<h5 class="mb-0"><button class="btn btn-link font-xs-16 font-20 text-blue collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
Área Legislativa
</button></h5>
</div>
<div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
<div class="card-body">
<div class="row bg-gray rounded p-3">
<div class="col-12 col-lg-1 text-center"><img src="/wp-content/themes/academy/images/coaching2-area-fiscal.png" width="50" /></div>
<div class="ml-0 ml-md-2 ml-lg-0 p-0 mt-3 mt-lg-0 col-12 col-lg-2 coaching-concurso line-height-2 font-11 text-dark">
<ul class="ml-auto mr-auto ml-lg-0 mr-lg-0 col-8 col-md-4 col-lg-12">
  <li>Câmara dos Deputados</li>
  <li>Senado Federall</li>
  <li>Assembléias Legislativas</li>
</ul>
</div>
<div class="p-0 col-12 col-md-4 col-lg-3">
<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
<div class="row">
<div class="col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/c1-box.png" width="45" /></div>
<div class="pl-1 pl-lg-3 col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/coaching-box.png" width="70" />
<p class="text-white mb-2 font-12">COACHING</p>

</div>
</div>
<div class="bg-aqua-box text-white font-22 text-center p-1">MENSAL</div>
<div class="mt-3 text-white font-18 text-center p-0">R$ 700,00
<a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a></div>
</div>
</div>
<div class="p-0 col-12 col-md-4 col-lg-3 mt-2 mt-md-0">
<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
<div class="row">
<div class="col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/c3-box.png" width="58" /></div>
<div class="pl-1 pl-lg-3 col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/coaching-box.png" width="70" />
<p class="text-white mb-2 font-12">COACHING</p>

</div>
</div>
<div class="bg-aqua-box text-white font-22 text-center p-1">TRIMESTRAL</div>
<div class="line-height-1-2 mt-1 text-white font-13 text-center p-0">

<span style="text-decoration: line-through;">R$ 2.100,00</span> R$ 1.680,00 <span class="font-11">economia de</span> R$ 420,00
20% OFF

<a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a>

</div>
</div>
</div>
<div class="p-0 col-12 col-md-4 col-lg-3 mt-2 mt-md-0">
<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
<div class="row">
<div class="col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/c6-box.png" width="58" /></div>
<div class="pl-1 pl-lg-3 col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/coaching-box.png" width="70" />
<p class="text-white mb-2 font-12">COACHING</p>

</div>
</div>
<div class="bg-aqua-box text-white font-22 text-center p-1">SEMESTRAL</div>
<div class="line-height-1-2 mt-1 text-white font-13 text-center p-0">

<span style="text-decoration: line-through;">R$ 4.200,00</span> R$ 2.730,00 <span class="font-11">economia de</span>R$ 1.470,00
35% OFF

<a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a>

</div>
</div>
</div>
<div class="col-12 mt-5 text-right">
<h5 class="mb-0">VER MAIS COACHING</h5>
&nbsp;

</div>
</div>
</div>
</div>
<div class="card">
<div id="headingSix" class="card-header">
<h5 class="mb-0"><button class="btn btn-link font-xs-16 font-20 text-blue collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
Área Jurídica
</button></h5>
</div>
<div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
<div class="card-body">
<div class="row bg-gray rounded p-3">
<div class="col-12 col-lg-1 text-center"><img src="/wp-content/themes/academy/images/coaching2-area-fiscal.png" width="50" /></div>
<div class="ml-0 ml-md-2 ml-lg-0 p-0 mt-3 mt-lg-0 col-12 col-lg-2 coaching-concurso line-height-2 font-11 text-dark">
<ul class="ml-auto mr-auto ml-lg-0 mr-lg-0 col-8 col-md-4 col-lg-12">
  <li>Procuradorias (PGE, PGM)</li>
  <li>Defensorias Públicos</li>
  <li>Ministérios Públicos</li>
  <li>Advocacia Pública (AGU, PGF, PFN e PGBACEN)</li>
</ul>
</div>
<div class="p-0 col-12 col-md-4 col-lg-3">
<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
<div class="row">
<div class="col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/c1-box.png" width="45" /></div>
<div class="pl-1 pl-lg-3 col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/coaching-box.png" width="70" />
<p class="text-white mb-2 font-12">COACHING</p>

</div>
</div>
<div class="bg-aqua-box text-white font-22 text-center p-1">MENSAL</div>
<div class="mt-3 text-white font-18 text-center p-0">R$ 900,00
<a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a></div>
</div>
</div>
<div class="p-0 col-12 col-md-4 col-lg-3 mt-2 mt-md-0">
<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
<div class="row">
<div class="col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/c3-box.png" width="58" /></div>
<div class="pl-1 pl-lg-3 col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/coaching-box.png" width="70" />
<p class="text-white mb-2 font-12">COACHING</p>

</div>
</div>
<div class="bg-aqua-box text-white font-22 text-center p-1">TRIMESTRAL</div>
<div class="line-height-1-2 mt-1 text-white font-13 text-center p-0">

<span style="text-decoration: line-through;">R$ 2.700,00</span> R$ 2.160,00 <span class="font-11">economia de</span> R$ 540,00
20% OFF

<a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a>

</div>
</div>
</div>
<div class="p-0 col-12 col-md-4 col-lg-3 mt-2 mt-md-0">
<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
<div class="row">
<div class="col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/c6-box.png" width="58" /></div>
<div class="pl-1 pl-lg-3 col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/coaching-box.png" width="70" />
<p class="text-white mb-2 font-12">COACHING</p>

</div>
</div>
<div class="bg-aqua-box text-white font-22 text-center p-1">SEMESTRAL</div>
<div class="line-height-1-2 mt-1 text-white font-13 text-center p-0">

<span style="text-decoration: line-through;">R$ 5.400,00</span> R$ 2.925,00 <span class="font-11">economia de</span> R$ 3.510,00
35% OFF

<a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a>

</div>
</div>
</div>
<div class="col-12 mt-5 text-right">
<h5 class="mb-0">VER MAIS COACHING</h5>
&nbsp;

</div>
</div>
</div>
</div>
<div class="card">
<div id="headingSeven" class="card-header">
<h5 class="mb-0"><button class="btn btn-link font-xs-16 font-20 text-blue collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
Área Bancária
</button></h5>
</div>
<div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion">
<div class="card-body">
<div class="row bg-gray rounded p-3">
<div class="col-12 col-lg-1 text-center"><img src="/wp-content/themes/academy/images/coaching2-area-fiscal.png" width="50" /></div>
<div class="ml-3 ml-lg-0 p-0 mt-3 mt-lg-0 col-12 col-lg-2 coaching-concurso line-height-2 font-11 text-dark">
<ul class="ml-auto mr-auto ml-lg-0 mr-lg-0 col-6 col-md-3 col-lg-12">
  <li>BACEN</li>
  <li>Banco do Brasil</li>
  <li>Caixa Econômica</li>
  <li>Bancos Regionais</li>
</ul>
</div>
<div class="p-0 col-12 col-md-4 col-lg-3">
<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
<div class="row">
<div class="col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/c1-box.png" width="45" /></div>
<div class="pl-1 pl-lg-3 col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/coaching-box.png" width="70" />
<p class="text-white mb-2 font-12">COACHING</p>

</div>
</div>
<div class="bg-aqua-box text-white font-22 text-center p-1">MENSAL</div>
<div class="mt-3 text-white font-18 text-center p-0">R$ 560,00
<a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a></div>
</div>
</div>
<div class="p-0 col-12 col-md-4 col-lg-3 mt-2 mt-md-0">
<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
<div class="row">
<div class="col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/c3-box.png" width="58" /></div>
<div class="pl-1 pl-lg-3 col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/coaching-box.png" width="70" />
<p class="text-white mb-2 font-12">COACHING</p>

</div>
</div>
<div class="bg-aqua-box text-white font-22 text-center p-1">TRIMESTRAL</div>
<div class="line-height-1-2 mt-1 text-white font-13 text-center p-0">

<span style="text-decoration: line-through;">R$ 1.680,00</span> R$ 1.344,00 <span class="font-11">economia de</span> R$ 336,00
20% OFF

<a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a>

</div>
</div>
</div>
<div class="p-0 col-12 col-md-4 col-lg-3 mt-2 mt-md-0">
<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
<div class="row">
<div class="col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/c6-box.png" width="58" /></div>
<div class="pl-1 pl-lg-3 col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/coaching-box.png" width="70" />
<p class="text-white mb-2 font-12">COACHING</p>

</div>
</div>
<div class="bg-aqua-box text-white font-22 text-center p-1">SEMESTRAL</div>
<div class="line-height-1-2 mt-1 text-white font-13 text-center p-0">

<span style="text-decoration: line-through;">R$ 3.360,00</span> R$ 2.184,00 <span class="font-11">economia de</span> R$ 1.176,00
35% OFF

<a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a>

</div>
</div>
</div>
<div class="col-12 mt-5 text-right">
<h5 class="mb-0">VER MAIS COACHING</h5>
&nbsp;

</div>
</div>
</div>
</div>
<div class="card">
<div id="headingEight" class="card-header">
<h5 class="mb-0"><button class="btn btn-link font-xs-16 font-20 text-blue collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
INSS
</button></h5>
</div>
<div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion">
<div class="card-body">
<div class="row bg-gray rounded p-3">
<div class="col-12 col-lg-1 text-center"><img src="/wp-content/themes/academy/images/coaching2-area-fiscal.png" width="50" /></div>
<div class="ml-0 ml-md-3 ml-lg-0 p-0 mt-3 mt-lg-0 col-12 col-lg-2 coaching-concurso line-height-2 font-11 text-dark">
<ul class="ml-auto mr-auto ml-lg-0 mr-lg-0 col-5 col-md-3 col-lg-12">
  <li>Técnico INSS</li>
  <li>Analista INSS</li>
</ul>
</div>
<div class="p-0 col-12 col-md-4 col-lg-3">
<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
<div class="row">
<div class="col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/c1-box.png" width="45" /></div>
<div class="pl-1 pl-lg-3 col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/coaching-box.png" width="70" />
<p class="text-white mb-2 font-12">COACHING</p>

</div>
</div>
<div class="bg-aqua-box text-white font-22 text-center p-1">MENSAL</div>
<div class="mt-3 text-white font-18 text-center p-0">R$ 560,00
<a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a></div>
</div>
</div>
<div class="p-0 col-12 col-md-4 col-lg-3 mt-2 mt-md-0">
<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
<div class="row">
<div class="col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/c3-box.png" width="58" /></div>
<div class="pl-1 pl-lg-3 col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/coaching-box.png" width="70" />
<p class="text-white mb-2 font-12">COACHING</p>

</div>
</div>
<div class="bg-aqua-box text-white font-22 text-center p-1">TRIMESTRAL</div>
<div class="line-height-1-2 mt-1 text-white font-13 text-center p-0">

<span style="text-decoration: line-through;">R$ 1.680,00</span> R$ 1.344,00 <span class="font-11">economia de</span> R$ 336,00
20% OFF

<a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a>

</div>
</div>
</div>
<div class="p-0 col-12 col-md-4 col-lg-3 mt-2 mt-md-0">
<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
<div class="row">
<div class="col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/c6-box.png" width="58" /></div>
<div class="pl-1 pl-lg-3 col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/coaching-box.png" width="70" />
<p class="text-white mb-2 font-12">COACHING</p>

</div>
</div>
<div class="bg-aqua-box text-white font-22 text-center p-1">SEMESTRAL</div>
<div class="line-height-1-2 mt-1 text-white font-13 text-center p-0">

<span style="text-decoration: line-through;">R$ 3.360,00</span> R$ 2.184,00 <span class="font-11">economia de</span> R$ 1.176,00
35% OFF

<a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a>

</div>
</div>
</div>
<div class="col-12 mt-5 text-right">
<h5 class="mb-0">VER MAIS COACHING</h5>
&nbsp;

</div>
</div>
</div>
</div>
<div class="card">
<div id="headingNine" class="card-header">
<h5 class="mb-0"><button class="btn btn-link font-xs-16 font-20 text-blue collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
Agências e outras áreas
</button></h5>
</div>
<div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion">
<div class="card-body">
<div class="row bg-gray rounded p-3">
<div class="col-12 col-lg-1 text-center"><img src="/wp-content/themes/academy/images/coaching2-area-fiscal.png" width="50" /></div>
<div class="ml-3 ml-lg-0 p-0 mt-3 mt-lg-0 col-12 col-lg-2 coaching-concurso line-height-2 font-11 text-dark">
<ul class="ml-auto mr-auto ml-lg-0 mr-lg-0 col-4 col-md-2 col-lg-12">
  <li>CVM</li>
  <li>SUSEP</li>
  <li>ANS</li>
  <li>ANTT</li>
  <li>ANVISA</li>
</ul>
</div>
<div class="p-0 col-12 col-md-4 col-lg-3">
<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
<div class="row">
<div class="col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/c1-box.png" width="45" /></div>
<div class="pl-1 pl-lg-3 col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/coaching-box.png" width="70" />
<p class="text-white mb-2 font-12">COACHING</p>

</div>
</div>
<div class="bg-aqua-box text-white font-22 text-center p-1">MENSAL</div>
<div class="mt-3 text-white font-18 text-center p-0">R$ 700,00
<a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a></div>
</div>
</div>
<div class="p-0 col-12 col-md-4 col-lg-3 mt-2 mt-md-0">
<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
<div class="row">
<div class="col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/c3-box.png" width="58" /></div>
<div class="pl-1 pl-lg-3 col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/coaching-box.png" width="70" />
<p class="text-white mb-2 font-12">COACHING</p>

</div>
</div>
<div class="bg-aqua-box text-white font-22 text-center p-1">TRIMESTRAL</div>
<div class="line-height-1-2 mt-1 text-white font-13 text-center p-0">

<span style="text-decoration: line-through;">R$ 2.100,00</span> R$ 1.680,00 <span class="font-11">economia de</span> R$ 420,00
20% OFF

<a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a>

</div>
</div>
</div>
<div class="p-0 col-12 col-md-4 col-lg-3 mt-2 mt-md-0">
<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
<div class="row">
<div class="col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/c6-box.png" width="58" /></div>
<div class="pl-1 pl-lg-3 col-6"><img class="img-fluid ml-3" src="/wp-content/themes/academy/images/coaching-box.png" width="70" />
<p class="text-white mb-2 font-12">COACHING</p>

</div>
</div>
<div class="bg-aqua-box text-white font-22 text-center p-1">SEMESTRAL</div>
<div class="line-height-1-2 mt-1 text-white font-13 text-center p-0">

<span style="text-decoration: line-through;">R$ 4.200,00</span> R$ 2.730,00 <span class="font-11">economia de</span> R$ 1.470,00
35% OFF

<a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a>

</div>
</div>
</div>
<div class="col-12 mt-5 text-right">
<h5 class="mb-0">VER MAIS COACHING</h5>
&nbsp;

</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="bg-gray container-fluid pt-3 pb-2">
<div class="col-12 text-center mt-4 mb-3"><img src="/wp-content/themes/academy/images/coaching2-modulos.png" width="55" />
<div class="font-pop">Módulos do programa</div>
</div>
<div class="container">
<div class="row ml-auto mr-auto">
<div class="col-12 col-md-6 pr-0 text-center border-coaching">
<div class="col-12 col-md-10 ml-auto">
<div class="col-12 col-md-6 ml-auto"><img class="mt-1" src="/wp-content/themes/academy/images/coaching2-raio-x.png" width="55" /></div>
<h2 class="text-center text-md-right line-height-1 mt-1">Raio-x do Aluno</h2>
<p class="text-center text-md-right line-height-1-5">Dentro dos ciclos de estudos, serão definidas as metas padrões e extras para serem cumpridas pelos alunos. O resultado (aprovação no concurso desejado) está extremamente ligado ao cumprimento destas metas.</p>

<div class="col-12 col-md-6 ml-auto"><img class="mt-1" src="/wp-content/themes/academy/images/coaching2-material.png" width="55" /></div>
<h2 class="text-center text-md-right line-height-1 mt-1">Material de estudo</h2>
<p class="text-center text-md-right line-height-1-5">Dentro dos ciclos de estudos, serão definidas as metas padrões e extras para serem cumpridas pelos alunos. O resultado (aprovação no concurso desejado) está extremamente ligado ao cumprimento destas metas.</p>

<div class="col-12 col-md-6 ml-auto"><img class="mt-1" src="/wp-content/themes/academy/images/coaching2-planejamento.png" width="55" /></div>
<h2 class="text-center text-md-right line-height-1 mt-1">Planejamento, Ciclos e Metas</h2>
<p class="text-center text-md-right line-height-1-5">Dentro dos ciclos de estudos, serão definidas as metas padrões e extras para serem cumpridas pelos alunos. O resultado (aprovação no concurso desejado) está extremamente ligado ao cumprimento destas metas.</p>

<div class="col-12 col-md-6 ml-auto"><img class="mt-1" src="/wp-content/themes/academy/images/coaching2-questoes.png" width="55" /></div>
<h2 class="text-center text-md-right line-height-1 mt-1">Questões online</h2>
<p class="text-center text-md-right line-height-1-5">Dentro dos ciclos de estudos, serão definidas as metas padrões e extras para serem cumpridas pelos alunos. O resultado (aprovação no concurso desejado) está extremamente ligado ao cumprimento destas metas.</p>

</div>
</div>
<div class="col-12 col-md-6 pl-0 mt-0 mt-md-5">
<div class="col-12 col-md-10 text-center mt-5 pb-5">
<div class="col-12 col-md-6"><img class="mt-1 mt-lg-4" src="/wp-content/themes/academy/images/coaching2-simulados.png" width="55" /></div>
<h2 class="text-center text-md-left line-height-1 mt-1">Simulados online</h2>
<p class="text-center text-md-left line-height-1-5">Dentro dos ciclos de estudos, serão definidas as metas padrões e extras para serem cumpridas pelos alunos. O resultado (aprovação no concurso desejado) está extremamente ligado ao cumprimento destas metas.</p>

<div class="col-12 col-md-6"><img class="mt-1" src="/wp-content/themes/academy/images/coaching2-tecnicas.png" width="55" /></div>
<h2 class="text-center text-md-left line-height-1 mt-1">Técnicas</h2>
<p class="text-center text-md-left line-height-1-5">Dentro dos ciclos de estudos, serão definidas as metas padrões e extras para serem cumpridas pelos alunos. O resultado (aprovação no concurso desejado) está extremamente ligado ao cumprimento destas metas.</p>

<div class="col-12 col-md-6"><img class="mt-1" src="/wp-content/themes/academy/images/coaching2-acompanhamento.png" width="55" /></div>
<h2 class="text-center text-md-left line-height-1 mt-1">Acompanhamento e feedback</h2>
<p class="text-center text-md-left line-height-1-5">Dentro dos ciclos de estudos, serão definidas as metas padrões e extras para serem cumpridas pelos alunos. O resultado (aprovação no concurso desejado) está extremamente ligado ao cumprimento destas metas.</p>

<div class="col-12 col-md-6"><img class="mt-1" src="/wp-content/themes/academy/images/coaching2-posedital.png" width="55" /></div>
<h2 class="text-center text-md-left line-height-1 mt-1">Pós Edital</h2>
<p class="text-center text-md-left line-height-1-5">Dentro dos ciclos de estudos, serão definidas as metas padrões e extras para serem cumpridas pelos alunos. O resultado (aprovação no concurso desejado) está extremamente ligado ao cumprimento destas metas.</p>

</div>
</div>
<div class="col-12 text-center mt-2"><img src="/wp-content/themes/academy/images/coaching2-aprovacao.png" width="55" />
<h2 class="line-height-1 mt-1">Aprovação</h2>
<p class="line-height-1-5 col-12 col-md-5 col-lg-4 ml-auto mr-auto">Dentro dos ciclos de estudos, serão definidas as metas padrões e extras para serem cumpridas pelos alunos.</p>

</div>
<div class="col-12 text-center mt-5">
<h5>Veja o índice Completo do Coaching
<img class="ml-1" src="/wp-content/themes/academy/images/coaching2-pdf.png" width="30" />
<img src="/wp-content/themes/academy/images/coaching2-video.png" width="35" /></h5>
</div>
</div>
</div>
</div>
<div class="col-12 text-center mt-5"><img src="/wp-content/themes/academy/images/coaching2-equipe.png" width="55" />
<div class="font-pop">Equipe</div>
</div>
<div class="container">[carousel_equipe_coaching]</div>
<div class="col-12 text-center mt-5"><img src="/wp-content/themes/academy/images/coaching2-desconto.png" width="55" />
<div class="font-pop">Use cupons de descontos</div>
</div>
<div class="container pl-0 pr-0">
<div class="pl-0 pr-0 pddl-xs-0 col-12 ml-auto mr-auto mt-5">
<div class="pddl-xs-0 row ml-auto mr-auto text-white">
<div class="col-12 col-lg-4 pl-0 pl-md-5 pl-lg-0 pr-0">
<div class="ml-auto mr-auto col-12 col-md-6 col-lg-12 bg-img-desc-1">
<div class="line-height-1-4 font-55 font-arial mt-2 mb-0 font-weight-bold">100%</div>
<p class="ml-1 mt-0 pb-4">Técnicas de Estudos,
Caderno de Questões,
Simulados, Sistema de Questões</p>

</div>
</div>
<div class="col-12 col-lg-4 pl-0 pl-md-5 pl-lg-0 pr-0">
<div class="ml-auto mr-auto col-12 col-md-6 col-lg-12 bg-img-desc-2">
<div class="font-55 font-arial mt-2 mb-0 font-weight-bold">50%</div>
<p class="ml-1 mt-0 pb-4">Pacotes de Cursos,
Mapas Mentais

</p>

</div>
</div>
<div class="col-12 col-lg-4 pl-0 pl-md-5 pl-lg-0 pr-0">
<div class="ml-auto mr-auto col-12 col-md-6 col-lg-12 bg-img-desc-3">
<div class="font-55 font-arial mt-2 mb-0 font-weight-bold">25%</div>
<p class="ml-1 mt-0 pb-5">Cursos Avulsos</p>
&nbsp;

</div>
</div>
</div>
<div class="text-center mt-3">
<h5 class="mb-0">ACESSAR CUPONS</h5>
&nbsp;

</div>
</div>
</div>
<div class="bg-gray pb-3 mt-5 pl-2 pr-2">
<div class="col-12 text-center"><img class="mt-4" src="/wp-content/themes/academy/images/perguntas.png" width="50" />
<h2 class="font-pop">Perguntas frequentes</h2>
</div>
<div class="container">[faq faq_topic="coaching-2" limit=-1]</div>
</div>
<div class="col-12 text-center"><img class="mt-4" src="/wp-content/themes/academy/images/coaching2-turma.png" width="50" />
<div class="font-pop">Turma de coaching</div>
</div>
<div class="col-12 mt-3 font-12">
<div class="ml-auto mr-auto col-10 text-justify p-0">

O Programa de Turma de Coaching nasceu com o intuito de direcionar os alunos com base nos seus objetivos (por áreas) e nivel (básico e avançada) para permitir que cada um consiga implementar as Metas dentro das suas especificidades. Este programa não é para a RETA FINAL, ou seja, para o PÓS-EDITAL, já que este momento é tratado nas turmas específicas e você pode encontrar AQUI!
As principais características do programa são:
<ul class="coaching-concurso-3 text-gray">
  <li><span class="text-blue font-weight-bold">Até 6 pessoas por turma:</span> isto significa que você estará em contato direto com as pessoas com mesmo interesse, objetivo e seriedade que você.</li>
  <li><span class="text-blue font-weight-bold">Interações:</span> Além das intereções por whatsapp (contato diário e ilimitado), o Coach/Mentor passa um pente fino regularmente por Skype.</li>
  <li><span class="text-blue font-weight-bold">Metas:</span> 100% focadas nos concursos da sua área, com direcionamento no nível dos ASSUNTOS das disciplinas (extremamente detalhado), verdadeiro "MAPA DA MINA DE OURO" para você focar no filé mignom e ter maior resultado no menor tempo.</li>
  <li><span class="text-blue font-weight-bold">Material de Estudo:</span> recomendados, com total liberdade de escolha pro aluno.</li>
  <li><span class="text-blue font-weight-bold">Descontos Exclusivos:</span> Cursos do Exponencial com <span class="text-danger">25%</span> (avulsos) e <span class="text-danger">50%</span> (pacote), conforme <span class="text-blue">Políticas de Descontos</span>, com acesso <span class="text-danger font-weight-bold">GRATUITO</span> ao Sistema de Questões Online.</li>
</ul>
</div>
</div>
<div class="mb-5 mt-5 col-10 col-md-7 col-lg-6 col-xl-5 ml-auto mr-auto embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="https://www.youtube.com/embed/LCNNXEmhpCo" width="300" height="150" allowfullscreen="allowfullscreen"></iframe></div>
<div class="col-12 text-center">
<div class="font-16">Procurando pelas turmas de coaching?</div>
</div>
<div class="text-center mt-3 mb-5">
<h5 class="mb-0">SAIBA MAIS</h5>
&nbsp;

</div>
</div>