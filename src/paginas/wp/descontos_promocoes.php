<?php get_header ();?>
<div class="container-fluid">
<div class="container pt-1 pt-md-5">
	<div class="col-12 text-center text-blue">
	[expo_h1]
	</div>
	<div class="mt-5 col-12 text-center font-weight-bold">
		<img src="/wp-content/themes/academy/images/desconto-geral.png"/>
	<div class="font-pop font-18">Aqui vão algumas vantagens que você obtém como aluno do Exponencial Concursos:</div>
	</div>	
	<div class="mt-5 col-12 text-center font-weight-bold">
		<img src="http://exponencial/wp-content/uploads/2017/05/descontos3.png" />
		<div class="font-pop font-18">
	Use o cupom de desconto informado abaixo</div>
	</div>


<div class="row mt-3">
<div class="col-12 col-md-6 col-lg-3 p-4 p-lg-2 p-xl-3">
<div class="font-10 pb-1 pb-lg-4">Se você contratou o serviço de coaching, durante a sua duração, você terá os seguintes descontos:</div>
<div class="mt-5 box-amarelo-light rounded-4 pb-3">
<div class="mb-3 pt-3 text-center">
	<img src="/wp-content/themes/academy/images/aluno-coaching.png"/>
</div>

<div class="border-top border-secondary border-width-2 pt-3 font-12 text-center">Indicação de alunos</div>
<div class="text-blue font-14 font-weight-bold text-center">50% em 1 mensalidade<sup class="font-8">1</sup></div>
<div class="font-12 text-center pt-3">Pacotes</div>
<div class="font-12 row ml-auto mr-auto bg-darkgray rounded-5 p-1">
<div class="col-2 col-lg-3 col-xl-2">
<img id="imagem" src="http://exponencial/wp-content/uploads/2017/05/descontos3.png"/>
</div>
<div class="col-8 col-md-9 col-lg-7 col-xl-8 text-center">coaching50</div>
</div>



<div class="text-blue font-14 text-center font-weight-bold">50%</div>
<div class="font-12 text-center pt-3">Cursos Avulsos</div>

<div class="font-12 row ml-auto mr-auto bg-darkgray rounded-5 p-1">
<div class="col-2">
<img id="imagem" src="http://exponencial/wp-content/uploads/2017/05/descontos3.png"/>
</div>
<div class="col-8 col-md-9 col-lg-9 col-xl-8 text-center">coaching25</div>
</div>

<div class="text-blue font-14 text-center font-weight-bold">25%</div>
<div class="font-12 text-center pt-3">Mapas Mentais</div>

<div class="font-12 row ml-auto mr-auto bg-darkgray rounded-5 p-1">
<div class="col-2">
<img id="imagem" src="http://exponencial/wp-content/uploads/2017/05/descontos3.png"/>
</div>
<div class="col-8 col-md-9 col-lg-9 col-xl-8 text-center">mmcoaching</div>
</div>

<div class="text-blue font-14 text-center font-weight-bold">50%</div>
<div class="font-12 text-center pt-3">Cadernos de Questões</div>
<div class="font-12 row ml-auto mr-auto bg-darkgray rounded-5 p-1">
<div class="col-2 col-md-3 col-lg-1 col-xl-2">
<img id="imagem" src="http://exponencial/wp-content/uploads/2017/05/descontos3.png"/>
</div>
<div class="col-9 col-md-7 col-xl-8 text-center">coaching100caderno</div>
</div>

<div class="text-blue font-14 text-center font-weight-bold">100%</div>
<div class="font-12 text-center pt-3">Sistema de Questões</div>
<div class="text-blue font-14 text-center font-weight-bold">100%</div>
<div class="font-12 text-center pt-3">Simulados</div>
<div class="text-blue font-14 text-center font-weight-bold">100%<sup class="font-8">2</sup></div>
<div class="font-12 text-center pt-3">Técnicas de Estudo</div>

<div class="font-12 row ml-auto mr-auto bg-darkgray rounded-5 p-1">
<div class="col-2 col-md-3 col-xl-2">
<img id="imagem" src="http://exponencial/wp-content/uploads/2017/05/descontos3.png"/>
</div>
<div class="col-8 col-md-7 col-lg-7 col-xl-8 text-center">tecnicas</div>
</div>

<div class="text-blue font-14 text-center font-weight-bold">100%</div>

<div class="text-center pt-3">
	<a href="/painel-coaching/inscricao">
		<img class="img-fluid" src="/wp-content/themes/academy/images/ser-um-aluno-coaching.png" />
	</a>
</div>
</div>
</div>
<!-- 2 -->
<div class="col-12 col-md-6 col-lg-3 p-4 p-lg-2 p-xl-3">
<div class="font-10 pb-1 pb-lg-4">Agora, se você adquiriu algum pacote <b>de cursos</b>, então terá direito aos seguintes descontos:</div>
<div class="mt-5 box-amarelo-light rounded-4 pb-3">
<div class="mb-3 pt-3 text-center">
	<img src="/wp-content/themes/academy/images/aluno-pacote.png"/>
</div>

<div class="border-top border-secondary border-width-2"></div>

<div class="font-12 text-center pt-3">Pacotes</div>
<div class="font-12 row ml-auto mr-auto bg-darkgray rounded-5 p-1">
<div class="col-2 col-md-3 col-xl-2">
<img id="imagem" src="http://exponencial/wp-content/uploads/2017/05/descontos3.png"/>
</div>
<div class="col-9 col-md-7 col-xl-8 text-center">alunopacote15</div>
</div>



<div class="text-blue font-14 text-center font-weight-bold">15%</div>
<div class="font-12 text-center pt-3">Cursos Avulsos</div>

<div class="font-12 row ml-auto mr-auto bg-darkgray rounded-5 p-1">
<div class="col-2 col-md-3 col-xl-2">
<img id="imagem" src="http://exponencial/wp-content/uploads/2017/05/descontos3.png"/>
</div>
<div class="col-9 col-md-7 col-xl-8 text-center">alunopacote10</div>
</div>

<div class="text-blue font-14 text-center font-weight-bold">10%<sup class="font-8">3</sup></div>
<div class="font-12 text-center pt-3">Mapas Mentais</div>

<div class="font-12 row ml-auto mr-auto bg-darkgray rounded-5 p-1">
<div class="col-2 col-md-3 col-lg-2">
<img id="imagem" src="http://exponencial/wp-content/uploads/2017/05/descontos3.png"/>
</div>
<div class="col-9 col-md-7 col-xl-8 text-center">alunopacote25mm</div>
</div>

<div class="text-blue font-14 text-center font-weight-bold">25%</div>
<div class="font-12 text-center pt-3">Cadernos de Questões</div>
<div class="font-12 row ml-auto mr-auto bg-darkgray rounded-5 p-1">
<div class="col-2 col-md-3 col-lg-1 col-xl-2">
<img id="imagem" src="http://exponencial/wp-content/uploads/2017/05/descontos3.png"/>
</div>
<div class="col-9 col-md-7  col-xl-8 text-center">alunopacote50caderno</div>
</div>
<div class="text-blue font-14 text-center font-weight-bold">50%</div>

<div class="font-12 text-center pt-3">Sistema de Questões</div>

<div class="font-12 row ml-auto mr-auto bg-darkgray rounded-5 p-1">
<div class="col-2 col-md-3 col-lg-1 col-xl-2">
<img id="imagem" src="http://exponencial/wp-content/uploads/2017/05/descontos3.png"/>
</div>
<div class="col-9 col-md-7 col-xl-8 text-center">alunopacote6questao</div>
</div>

<div class="text-blue font-14 text-center font-weight-bold">6 meses gratuitos</div>

<div class="font-12 text-center pt-3">Simulados</div>

<div class="font-12 row ml-auto mr-auto bg-darkgray rounded-5 p-1">
<div class="col-2 col-md-3 col-lg-1 col-xl-2">
<img id="imagem" src="http://exponencial/wp-content/uploads/2017/05/descontos3.png"/>
</div>
<div class="font-11 col-9 col-md-7 col-xl-8 text-center">alunopacote50simulado</div>
</div>

<div class="text-blue font-14 text-center font-weight-bold">50%</div>

<div class="text-center pt-3">
	<a href="/cursos-por-concurso">
		<img class="img-fluid" src="/wp-content/themes/academy/images/adquirir-um-pacote.png"/>
	</a>
</div>
</div>
</div>
<!-- 3 -->
<div class="col-12 col-md-6 col-lg-3 p-4 p-lg-2 p-xl-3">
<div class="font-10 pb-1 pb-lg-0 pb-xl-4">Ahhhh, e claro que não esquecemos de dar desconto para você que é nosso aluno de curso avulso ou assinante do sistema de questões. Confira:</div>
<div class="mt-4 box-amarelo-light rounded-4 pb-3">
<div class="mb-3 pt-3 text-center">
	<img src="/wp-content/themes/academy/images/aluno-curso.png"/>
</div>

<div class="border-top border-secondary border-width-2 pt-3"></div>

<div class="font-12 text-center pt-3">Cursos Avulsos</div>
<div class="font-12 row ml-auto mr-auto bg-darkgray rounded-5 p-1">
<div class="col-2 col-md-3 col-xl-2">
<img id="imagem" src="http://exponencial/wp-content/uploads/2017/05/descontos3.png"/>
</div>
<div class="col-8 col-md-7 col-xl-8 text-center">alunoavulso10</div>
</div>



<div class="text-blue font-14 text-center font-weight-bold">10%<sup class="font-8">4</sup></div>
<div class="font-12 text-center pt-3">Cadernos de Questões</div>

<div class="font-12 row ml-auto mr-auto bg-darkgray rounded-5 p-1">
<div class="col-2 col-md-3 col-lg-1 col-xl-2">
<img id="imagem" src="http://exponencial/wp-content/uploads/2017/05/descontos3.png"/>
</div>
<div class="col-8 col-md-7 col-xl-8 text-center">alunoavulso25caderno</div>
</div>

<div class="text-blue font-14 text-center font-weight-bold">25%</div>


<div class="text-center pt-3">
	<a href="/cursos-por-concurso">
		<img class="img-fluid" src="/wp-content/themes/academy/images/escolher-um-curso.png"/>
	</a>
</div>
</div>
</div>
<!-- 4 -->
<div class="col-12 col-md-6 col-lg-3 p-4 p-lg-2 p-xl-3">
<div class="mt-5 pt-1 pt-md-5 pb-md-1 pb-lg-5"></div>
<div class="mt-1 mt-md-0 box-amarelo-light rounded-4 pb-3">
<div class="mb-3 pt-3 text-center">
	<img src="/wp-content/themes/academy/images/aluno-sistema.png"/>
</div>

<div class="border-top border-secondary border-width-2 pt-3"></div>

<div class="font-12 text-center pt-3">Mapas Mentais</div>
<div class="font-12 row ml-auto mr-auto bg-darkgray rounded-5 p-1">
<div class="col-2 col-md-3 col-lg-1 col-xl-2">
<img id="imagem" src="http://exponencial/wp-content/uploads/2017/05/descontos3.png"/>
</div>
<div class="col-8 col-md-7 col-xl-8 text-center">alunoquestao25mm</div>
</div>



<div class="text-blue font-14 text-center font-weight-bold">25%</div>
<div class="font-12 text-center pt-3">Cadernos de Questões</div>

<div class="font-12 row ml-auto mr-auto bg-darkgray rounded-5 p-1">
<div class="col-2 col-md-3 col-lg-1 col-xl-2">
<img id="imagem" src="http://exponencial/wp-content/uploads/2017/05/descontos3.png"/>
</div>
<div class="col-8 col-md-7 font-sm-12 font-md-12 font-11 col-xl-8 text-center">alunoquestao25caderno</div>
</div>

<div class="text-blue font-14 text-center font-weight-bold">25%</div>
<div class="font-12 text-center pt-3">Simulados</div>

<div class="font-12 row ml-auto mr-auto bg-darkgray rounded-5 p-1">
<div class="col-2 col-md-3 col-lg-1 col-xl-2">
<img id="imagem" src="http://exponencial/wp-content/uploads/2017/05/descontos3.png"/>
</div>
<div class="col-8 col-md-7 font-sm-12 font-md-12 font-11 col-xl-8 text-center">alunoquestao50simulado</div>
</div>

<div class="text-blue font-14 text-center font-weight-bold">50%</div>


<div class="text-center pt-3">
	<a href="/sistema-de-questoes/#assinatura">
		<img class="img-fluid" src="/wp-content/themes/academy/images/aderir-sistema-questoes.png"/>
	</a>
</div>
</div>
</div>




<div class="row ml-auto mr-auto mt-5">
<div><a href="/fale-conosco"><img class="img-fluid" src="/wp-content/themes/academy/images/banner-cupom.jpg" /></a></div>

</div>
<div class="mt-5 row ml-auto mr-auto">
<div class="font-pop font-18 font-weight-bold col-12">Legenda</div>
<div class="mt-3 font-11 line-height-1">
<p><span class="text-danger">1 –</span> Válido apenas após o período mínimo definido no programa.</p>
<p><span class="text-danger">2 –</span> Conforme designação feita pelo seu Coach, ao longo do programa de coaching.</p>
<p><span class="text-danger">3 –</span> Se o aluno comprou um pacote de cursos e ele teve, após a compra, a inclusão de novo curso que não existia anteriormente, ele poderá solicitar o cupom do % do Pacote para compra do curso avulso.</p>
<p><span class="text-danger">4 –</span> Para o aluno que já comprou algum curso e deseja comprar o restante de forma a completar o Pacote existente, será dado o mesmo valor de desconto do pacote para os cursos restantes.</p>
<p><span class="text-danger">5 –</span> Não cumulativo com descontos de promoções especiais, como Black Friday, Aniversário, e outras.</p>
</div>

</div>
</div>
</div>


<div class="row bg-gray p-5 mt-3">
<div class="col-12">
<div class="font-pop text-center font-weight-bold">Mais informações</div>
</div>
<div class="col-12 col-lg-6 mt-1 mt-lg-3 font-10">
<div><img src="/wp-content/themes/academy/images/check-tick.png" width="12" height="12" /> <span class="ml-2">Todos os descontos são de uso exclusivo, pessoal, intransferível, não-conversível em moeda ou qualquer outro valor e não-cumulativos entre si, de uso restrito no sitio do Exponencial Concursos, respeitando as condições, regras, regulamentos e disponibilidades das ofertas.</span></div>
<div class="mt-1 mt-lg-0"><img src="/wp-content/themes/academy/images/check-tick.png" width="12" height="12" /> <span class="ml-2">Os descontos para os alunos Coaching e assinantes do Sistema de Questões são válidos apenas enquanto durarem os serviços. Interrupções, postergações, e/ou cancelamentos interrompem a validade dos descontos.</span></div>
</div>
<div class="col-12 col-lg-6 mt-1 mt-lg-3 font-10">
<div><img src="/wp-content/themes/academy/images/check-tick.png" width="12" height="12" /> <span class="ml-2">Os descontos para os alunos de pacote e cursos avulsos são válidos para solicitações feitas até 12 meses após o pagamento dos serviços, não se aplicando aos cursos gratuitos.</span></div>
<div class="mt-1 mt-lg-0"><img src="/wp-content/themes/academy/images/check-tick.png" width="12" height="12" /> <span class="ml-2">Caso você seja aluno em mais de um serviço, você poderá usar apenas 1 dos descontos, não sendo cumulativo. Exemplo: você é aluno do Coaching e do pacote de cursos. Caso você resolva comprar um curso de mapa mental, seu desconto será de 50% (apenas o desconto para o perfil aluno coaching), sem acumular os 25% do perfil aluno pacote.</span></div>
<div class="mt-1 mt-lg-0"><img src="/wp-content/themes/academy/images/check-tick.png" width="12" height="12" /> <span class="ml-2">Os descontos devem ser solicitados através do nosso "fale conosco".</span></div>
</div>
</div>
</div>

<?php get_footer(); ?>