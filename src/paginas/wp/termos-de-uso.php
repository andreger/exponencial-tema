<div class="container pt-1 pt-md-5 pb-5" style="text-align: center;">
[expo_h1]
<div class="col-12 border mt-4">
<table border="0" cellspacing="0">
<tbody>
<tr>
<td>
<h5 style="text-align: center;"><span style="font-size: 16px;"><strong><strong>LEIA TODOS OS TERMOS DESTE TERMO DE SERVIÇO ANTES DE CONTINUAR.</strong></strong></span></h5>
<p style="text-align: justify;"><span style="font-size: 12px;">O presente TERMO DE SERVIÇO DE COACHING, CURSO ON-LINE e SISTEMA DE QUESTÕES ON-LINE é celebrado entre você ("USUÁRIO"), cujos dados estão informados na ficha de cadastro por você preenchida, e a <strong style="font-family: inherit; line-height: 1.5;">EXPONENCIAL CONCURSOS LTDA ME.</strong><span style="font-family: inherit; line-height: 1.5;">, pessoa jurídica de direito privado, inscrita no CNPJ/MF sob o nº 20.094.693/0001-36, com sede na cidade do Rio de Janeiro/RJ, (doravante denominada "EXPONENCIAL"), única e exclusiva proprietária e responsável pelo funcionamento, administração e exploração do portal de Internet denominado Exponencial Concursos, registrado e funcionando no nome de domínio "exponencialconcursos.com.br" (denominado simplesmente "PORTAL").</span></span></p>

<div style="text-align: left;">

&nbsp;

</div>
<ol style="text-align: center; list-style-position: inside;">
 	<li>
<h5 style="text-align: center;"><strong> Do Objeto</strong></h5>
</li>
</ol>
<p style="text-align: justify;"><span style="font-size: 12px;">1.1. A empresa Exponencial Concursos LTDA., doravante denominada CONTRATADA, oferece serviço de coaching, cursos online – em arquivo formato .pdf e videoaulas – e fórum de dúvidas para o aluno (doravante denominados CONTRATANTES), e um sistema de questões e simulados online, resultados de sociedades com o(s) PROFESSOR(ES) ministrante(s) da disciplina.</span></p>

<ol style="text-align: center; list-style-position: inside;" start="2">
 	<li>
<h5 style="text-align: center;"><strong> Das Responsabilidades</strong></h5>
</li>
</ol>
<p style="text-align: left;"><span style="font-size: 12px;">2.1. Cabe à empresa Exponencial Concursos, pelo portal de domínio <a href="http://homol.exponencialconcursos.com.br">www.exponencialconcursos.com.br</a>, fornecer plataforma de ensino em que são disponibilizados os recursos mencionados no item anterior, com a responsabilidade de oferecer os serviços de apoio administrativo inerentes à atividade.</span></p>
<p style="text-align: left;"><span style="font-size: 12px;">2.2. Cabe ao PROFESSOR a elaboração e atualização do conteúdo dos cursos, disponibilizando as aulas conforme previsto no CRONOGRAMA de cada curso online. Ressalta-se que o CRONOGRAMA das aulas é uma expectativa de entrega e o mesmo poderá ser alterado pelo professor, sendo os alunos notificados da alteração.</span></p>
<p style="text-align: left;"><span style="font-size: 12px;">2.3. Cabe ao PROFESSOR o contato com os alunos matriculados por meio do fórum de dúvidas, cujas perguntas serão respondidas de acordo com a disponibilidade de tempo e a pertinência com o assunto da respectiva aula.</span></p>
<p style="text-align: left;"><span style="font-size: 12px;">2.3.1. No fórum de dúvidas, as perguntas realizadas após o período de validade do curso não serão respondidas pelo PROFESSOR. Não há garantia de que todas as dúvidas sejam respondidas.</span></p>
<p style="text-align: left;"><span style="font-size: 12px;">2.3.2. Caso o fórum de dúvida esteja fora do ar ou com problemas técnicos, o aluno poderá entrar em contato com o professor através de seu e-mail.</span></p>
<p style="text-align: left;"><span style="font-size: 12px;">2.4. Cabe ao CONTRATANTE a correta identificação no seu ato de cadastro no site. Em caso de inexatidão ou falsidade das informações prestadas pelo CONTRATANTE em seu cadastro, a CONTRATADA se reserva o direito de bloquear o acesso do usuário ao site.</span></p>
<p style="text-align: left;"><span style="font-size: 12px;">2.4.1. Em caso de inexatidão ou desatualização do endereço do CONTRATANTE em seu cadastro, a CONTRATADA se exime de reenvio de brindes e produtos sorteados cuja entrega tenha sido frustrada por este motivo.</span></p>
<p style="text-align: left;"><span style="font-size: 12px;">2.5. A CONTRATADA se reserva o direito de implementar controle de acesso identificando o local (cidade) de onde o CONTRATANTE efetua o acesso.</span></p>
<p style="text-align: left;"><span style="font-size: 12px;">2.5.1. Sendo identificada situações de acessos em diversas cidades diferentes em um curto espaço de tempo, configura-se “compartilhamento de LOGIN e SENHA de acesso”, violação prevista no <strong>item 3.2</strong> deste termo, aplicando-se o previsto no <strong>item 3.2.1.</strong></span></p>
<p style="text-align: left;"><span style="font-size: 12px;">2.6. Cabe ao CONTRATANTE a observância dos direitos autorais que resguardam as obras intelectuais objeto do presente contrato, nos termos da Lei nº 9.610, de 19 de fevereiro de 1998 (altera, atualiza e consolida a legislação sobre direitos autorais e dá outras providências).</span></p>
<p style="text-align: left;"><span style="font-size: 12px;">2.7. Cabe à CONTRATADA disponibilizar e deixar disponível o sistema de questões on-line, não configurando quebra na prestação de serviço e não sendo devido qualquer ressarcimento por eventuais e temporárias indisponibilidades do sistema.</span></p>

<ol style="text-align: center; list-style-position: inside;" start="3">
 	<li>
<h5 style="text-align: center;"><strong> Da Matrícula e acesso à área do aluno de cursos</strong></h5>
</li>
</ol>
<p style="text-align: left;"><span style="font-size: 12px;">3.1. Para participação do CONTRATANTE no curso online ou serviços de consultoria é exigida prévia matrícula e aceitação de todas as cláusulas do presente TERMO DE SERVIÇO DE COACHING, CURSO ONLINE e SISTEMA DE QUESTÕES ONLINE.</span></p>
<p style="text-align: left;"><span style="font-size: 12px;">3.2. O acesso aos serviços oferecidos é de uso oneroso, personalíssimo e intransferível do CONTRATANTE, sendo ilegal sua distribuição, venda, rateio, compartilhamento ou redistribuição por qualquer modalidade, incluindo-se na proibição tanto a redistribuição física (cópias reprográficas) quanto virtual (disponibilização a terceiros por meio de e-mail, bibliotecas virtuais, pastas compartilhadas, etc.).</span></p>
<p style="text-align: left;"><span style="font-size: 12px;">3.2.1. Em caso de desrespeito a esta cláusula, inclusive no caso de compartilhamento de LOGIN e SENHA de acesso, a CONTRATADA se reserva o direito de bloquear o acesso do CONTRATANTE ao site (sem a devolução do valor pago), além das providências legais e penais cabíveis na defesa de seus direitos.</span></p>
<p style="text-align: left;"><span style="font-size: 12px;">3.3. Os PROFESSORES disponibilizam o material do curso online e os comentários nos sistemas de questões pela plataforma da CONTRATADA, cujo acesso ocorre por meio de LOGIN e SENHA cadastrados pelo CONTRATANTE.</span></p>
<p style="text-align: left;"><span style="font-size: 12px;">3.3.1. É responsabilidade do CONTRATANTE o sigilo do LOGIN e SENHA, para evitar o acesso não autorizado de terceiros à área do aluno.</span></p>
<p style="text-align: left;"><span style="font-size: 12px;">3.4. As videoaulas poderão ou não ser disponibilizadas para download à critério da CONTRATADA, continuando de uso individual e intransferível.</span></p>
<p style="text-align: left;"><span style="font-size: 12px;">3.5. Cursos que forem ofertados no formato PDF + videoaula não necessariamente deverão conter videoaulas para todos os assuntos do curso. Videoaulas são complementares ao material em PDF neste caso e não necessariamente haverá videoaula correspondente a toda a matéria. O professor irá orientar seus alunos conforme o caso.</span></p>

<h2 style="text-align: left;"></h2>
<ol style="text-align: center; list-style-position: inside;" start="4">
 	<li>
<h5 style="text-align: center;"><strong> Do Cancelamento </strong></h5>
</li>
</ol>
<p style="text-align: left;"><span style="font-size: 12px;">4.1. Por falta de quórum mínimo a CONTRATADA pode vir a cancelar o curso ou serviço de consultoria disponibilizado, hipótese em que o CONTRATANTE será informado e terá o valor da matrícula integralmente ressarcido.</span></p>
<p style="text-align: left;"><span style="font-size: 12px;">4.2. O cronograma dos cursos elaborado por parte do PROFESSOR conterão datas de expectativa de disponibilização das aulas, podendo ser alteradas unilateralmente sem prévio aviso ao CONTRATANTE. No caso de curso para concursos, com data de prova definida, o PROFESSOR disponibilizará TODAS as aulas antes da data da prova, sendo que caso não o faça o  CONTRATANTE poderá tomar a iniciativa de cancelar a aquisição, hipótese em que o valor ressarcido obedecerá à proporção do valor das aulas ainda não disponibilizadas pelo PROFESSOR.</span></p>
<p style="text-align: left;"><span style="font-size: 12px;">4.3. Em caso de solicitação de cancelamento da matrícula por culpa exclusiva do CONTRATANTE (desistência, compra por engano), deve-se obedecer às seguintes condições e regras:</span></p>
<p style="text-align: left;"><span style="font-size: 12px;">4.3.1. Cancelamento de Curso Online: o CONTRATANTE terá 7 (sete) dias para solicitar o cancelamento – contados do pagamento –, desde que não tenha sido feito o download ou acessado as aulas disponibilizadas pelo PROFESSOR, sendo configurado prestação de serviço no momento em que as aulas são disponibilizadas ao CONTRATANTE, independentemente de efetivamente tê-las visto.</span></p>
<p style="text-align: left;"><span style="font-size: 12px;">4.3.2. Cancelamento de COACHING: O cancelamento só será aceito se feito em até 7 (sete) dias do pagamento e antes da realização da entrevista com o PROFESSOR.</span></p>
<p style="text-align: left;"><span style="font-size: 12px;">4.3.2.1. O COACHING é um serviço composto de inúmeras atividades, sendo o acompanhamento do aluno uma parte deste serviço, portanto, a não utilização efetiva deste acompanhamento não dá direito à reembolso ao aluno pelo serviço pago, INCLUSIVE nos casos em que seja prevista duração mínima.</span></p>
<p style="text-align: left;"><span style="font-size: 12px;">4.3.2.2. Em caso de estorno autorizado excepcionalmente pela CONTRATADA, será levado em conta para o cálculo do valor a ser reembolsado:</span></p>
<p style="text-align: left;"><span style="font-size: 12px;">4.3.2.2.1. Multa de 20% sobre o valor total do serviço contratado.</span></p>
<p style="text-align: left;"><span style="font-size: 12px;">4.3.2.2.2. O Pedido de cancelamento deve ser feito com no mínimo 15 dias de antecedência, considerando período mensal e desconsiderando qualquer período inferior à esta periodicidade.</span></p>
<p style="text-align: left;"><span style="font-size: 12px;">4.3.2.2.3. Os descontos utilizados de forma direta ou indireta serão cobrados, considerando o valor do serviço mensal (e não valores promocionais para periodicidades maiores), assim como todo e qualquer desconto que utilizar em aquisições de cursos, sistema de questões/simulados e outros.</span></p>
<p style="text-align: left;"><span style="font-size: 12px;">4.3.3. Cancelamento da assinatura do SISTEMA DE QUESTÕES: O prazo para cancelamento do serviço é de 7 (sete) dias – contados do pagamento –, independentemente da duração do plano assinado.</span></p>
<p style="text-align: left;"><span style="font-size: 12px;">4.4. As solicitações de cancelamento deverão ocorrer por meio do <a href="http://homol.exponencialconcursos.com.br/fale-conosco">Fale Conosco</a> no site da Contratada.</span></p>

<ol style="text-align: center; list-style-position: inside;" start="5">
 	<li>
<h5><strong> Política de Privacidade</strong></h5>
</li>
</ol>
<p style="text-align: left;"><span style="font-size: 12px;">Está disponível integralmente aqui: <a href="http://homol.exponencialconcursos.com.br/politica-de-privacidade">www.exponencialconcursos.com.br/politica-de-privacidade</a></span></p>

<ol style="text-align: center; list-style-position: inside;" start="6">
 	<li>
<h5 style="text-align: center;"><strong> Do Sistema de Questões on-line</strong></h5>
</li>
</ol>
<p style="text-align: left;"><span style="font-size: 12px;"><strong>6.1. Das assinaturas</strong></span></p>
<p style="text-align: left;"><span style="font-size: 12px;">6.1.1. Todas as assinaturas disponibilizadas possuem o período de acesso disponibilizado para o CONTRATANTE, sendo de sua livre escolha a contratação.</span></p>
<p style="text-align: left;"><span style="font-size: 12px;">6.1.2. A assinatura começará a correr a partir do pagamento, independentemente de o CONTRATANTE ter acessado ou não o sistema.</span></p>
<p style="text-align: left;"><span style="font-size: 12px;">6.1.3. Conforme previsto no <strong>item 3.2</strong> deste TERMO DE SERVIÇO, as assinaturas permitem o acesso de apenas uma pessoa e os dados de acesso não podem ser compartilhados com outra pessoa em nenhuma circunstância.</span></p>
<p style="text-align: left;"><span style="font-size: 12px;">6.1.4. O CONTRATANTE deverá estar identificado, através de seu LOGIN, sendo obrigatório o preenchimento das informações ali constantes, inclusive o CPF, que será utilizado para emissão de nota fiscal eletrônica. Lei Nº 5098/2009 – http://notacarioca.rio.gov.br/</span></p>
<p style="text-align: left;"><span style="font-size: 12px;">6.1.5. 4. Se o CONTRATANTE efetuar a contratação de nova assinatura e ainda tiver saldo de dias de acesso ao Sistema de Questões, o prazo de vigência de acesso será SOMADO ao prazo anteriormente disponível.</span></p>
<p style="text-align: left;"><span style="font-size: 12px;"><strong>6.2. Do ambiente tecnológico</strong></span></p>
<p style="text-align: left;"><span style="font-size: 12px;">6.2.1. O site está homologado para os sistemas operacionais Windows e para os navegadores Internet Explorer, Firefox e Google Chrome, versões sempre atualizadas.</span></p>
<p style="text-align: left;"><span style="font-size: 12px;">6.2.2. Não nos responsabilizamos por eventuais falhas no acesso ao site ocasionadas por configurações/problemas locais das máquinas dos usuários ou seus respectivos provedores de acesso à internet. Portanto, não oferecemos suporte técnico remoto, mas somente orientações e respostas a consultas para solucionar tais problemas.</span></p>
<p style="text-align: left;"><span style="font-size: 12px;"><strong>6.3. Do bloqueio ao acesso</strong></span></p>
<p style="text-align: left;"><span style="font-size: 12px;">6.3.1. O CONTRATANTE só terá acesso mediante pagamento da assinatura disponibilizada pela CONTRATADA.</span></p>
<p style="text-align: left;"><span style="font-size: 12px;">6.3.2. A CONTRATADA se reserva o direito de implementar controle de acesso identificando o local (cidade) de onde o CONTRATANTE efetua o acesso.</span></p>
<p style="text-align: left;"><span style="font-size: 12px;">6.3.2.1. Sendo identificada situações de acessos em diversas cidades diferentes em um curto espaço de tempo, configura-se “compartilhamento de LOGIN e SENHA de acesso”, violação prevista no <strong>item 3.2</strong> deste termo, aplicando-se o previsto no <strong>item 3.2.1.</strong></span></p>
<p style="text-align: left;"><span style="font-size: 12px;">6.3.3. É vedado o uso pelo CONTRATANTE de qualquer dispositivo automatizado (ou robôs) no acesso ao site, o que será interpretado como infração e violação deste termo de uso, resultando na suspensão e consequente cancelamento do serviço, sem prejuízo das medidas legais (cíveis e criminais) cabíveis e sem direito a qualquer ressarcimento.</span></p>
<p style="text-align: left;"><span style="font-size: 12px;"><strong>6.4. Do funcionamento e particularidades do sistema</strong></span></p>
<p style="text-align: left;"><span style="font-size: 12px;">6.4.1. O CONTRANTE poderá responder ilimitadamente às questões constantes no banco de dados do sistema, além de ter acesso às ferramentas disponibilizadas pela CONTRATADA, tais como filtros, confecção de cadernos, comentário dos PROFESSORES e análise de desempenho.</span></p>
<p style="text-align: left;"><span style="font-size: 12px;">6.4.2. A CONTRATADA efetuará a classificação das questões por disciplina, assunto e demais características das questões, não sendo obrigatório o tratamento de forma igual ao do edital da prova. Por exemplo, se o conteúdo da questão for de Contabilidade, mas ela foi exigida na prova dentro da matéria Auditoria, a classificação poderá ser feita como Contabilidade, conforme avaliação da CONTRATADA.</span></p>
<p style="text-align: left;"><span style="font-size: 12px;">6.4.3. Os filtros de "disciplinas” e “assuntos" são feitos pelos PROFESSORES da matéria, quando da inserção do comentário de cada questão. Como a velocidade disto é menor que a velocidade com que novas questões são cadastradas, existirão questões sem comentário de professores e/ou sem classificação das questões.</span></p>
<p style="text-align: left;"><span style="font-size: 12px;">6.4.4. A atividade de classificação das questões é dinâmica, ficando resguardada à CONTRATADA o direito de mudar sem prévio aviso qualquer classificação de qualquer questão, a qualquer tempo.</span></p>
<p style="text-align: left;"><span style="font-size: 12px;">6.4.5. Caso o CONTRATANTE encontre algum erro no cadastro da questão (enunciado, alternativas, status e/ou gabarito), ele poderá reportar o evidenciado para a CONTRATADA, que providenciará a correção de qualquer erro existente.</span></p>
<p style="text-align: left;"><span style="font-size: 12px;">6.4.6.  Os simulados e cadernos de questões montados pela CONTRATADA não estão incluídos na assinatura do sistema de questões, sendo comercializadas de forma similar aos cursos online para concurso.</span></p>
<p style="text-align: left;"><span style="font-size: 12px;">6.4.7. O sistema de questões inclui comentário de professores, não sendo a CONTRATADA obrigada a fornecer comentário de TODAS questões, nem questões que o CONTRATANTE por ventura venha a solicitar.</span></p>
<p style="text-align: left;"><span style="font-size: 12px;"><strong>6.5. Serviços não incluídos/disponibilizados</strong></span></p>
<p style="text-align: left;"><span style="font-size: 12px;">6.5.1. Não fornecemos tira-dúvidas com os professores, apenas o acesso ao comentário de PROFESSORES.</span></p>
<p style="text-align: left;"><span style="font-size: 12px;">6.5.2. Não é permitida a impressão dos comentários dos professores, assim como não fornecemos ferramenta para fazê-lo. O CONTRATANTE terá acesso a uma ferramenta de impressão dos enunciados, podendo estar limitada a um número específico de questões por questões de viabilidade técnica.</span></p>
<p style="text-align: left;"><span style="font-size: 12px;">6.5.3. Não damos acesso ou cedemos, sob nenhuma hipótese, nosso banco de dados.</span></p>

<ol style="text-align: center; list-style-position: inside;" start="7">
 	<li>
<h5><strong> Das Condições Gerais</strong></h5>
</li>
</ol>
<p style="text-align: left;"><span style="font-size: 12px;">7.1. O acesso do CONTRATANTE ao ambiente do curso online se dá durante o prazo de vigência do mesmo, constante do site. Não haverá disponibilização do material após o final da vigência do curso contratado, assim como no sistema de questões.</span></p>
<p style="text-align: left;"><span style="font-size: 12px;">7.2. Dúvidas e casos omissos devem ser encaminhados à CONTRATADA, através do <a href="http://homol.exponencialconcursos.com.br/fale-conosco">Fale Conosco</a> ou por outros meios de contato disponibilizados no site <a href="http://homol.exponencialconcursos.com.br">www.exponencialconcursos.com.br</a>.</span></p>
<p style="text-align: left;"><span style="font-size: 12px;">7.3. O CONTRATANTE declara expressamente que está ciente e concorda com este Termo de Serviço e a Política de Privacidade.</span></p>
<p style="text-align: left;"><span style="font-size: 12px;">7.4. Fica eleito o foro da comarca da cidade de Rio de Janeiro/RJ, com expressa renúncia a qualquer outro, por mais privilegiado que seja, para dirimir as questões oriundas da interpretação e execução do presente TERMO DE SERVIÇO.</span></p>
<!--<p style="text-align: left;"><span style="font-size: 12px;">7.5. Descontos de datas específicas como Black Friday, Aniversário, entre outros, não serão cumulativos com os descontos dados regularmente definidos na <span style="color: #0000ff;"><a style="color: #0000ff;" href="http://homol.exponencialconcursos.com.br/descontos_promocoes">Política de Descontos</a></span></span></p>-->
</td>
</tr>
</tbody>
</table>
<div></div>
</div>
</div>