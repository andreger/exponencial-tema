<?php get_header(); ?>
<div class="acesso-negado-sq">
	<div class="container pt-5">
		<div class="row pt-5">
			<div class="col-12 col-lg-5"></div>
			<div class="pt-5 col-12 col-lg-7 text-center text-lg-left">
				<h1 class="text-white line-height-1">Desculpe-nos, mas você não tem permissão de acesso a esta área do site.</h1>
			</div>
		</div>
	</div>
</div>

<div class="container mt-4">	
		<div class="mt-4 col-12 text-center font-weight-bold font-22">
			Quero ter acesso!
		</div>		
		<div class="mt-4 col-12 text-center">
			<?php echo get_tema_image_tag('exponencial-simbolo.png') ?>
		</div>		
		<div class="col-12 mt-2 font-10 text-justify">
			O acesso a certas funcionalidades de nosso site é exclusivo para assinantes do Sistema de Questões e alunos do Coaching do Exponencial Concursos. Veja as vantagens de assinar nosso Sistema de Questões:
		</div>
		
	
</div>
	<div class="container-fluid p-0">
		<div class="ml-auto mr-auto col-md-11 col-lg-12">
			<?php include get_pagina_url('vantagens_sq.php'); ?>
		</div>
	</div>

	<div class="container mt-5 pb-5">		
			<div class="col-md-11 col-xl-10 pr-md-3 pr-lg-4 pr-xl-0 text-md-right text-center">
				<div>
				<a class="btn u-btn-primary col-5 col-md-3 col-lg-2 font-12" href="/sistema-de-questoes/#assinatura">Assinar</a>
				<a class="btn u-btn-blue col-5 col-md-3 col-lg-2 font-12" href="/sistema-de-questoes/ ">Saiba mais</a>
				</div>
			</div>		
	</div>

<?php get_footer(); ?>