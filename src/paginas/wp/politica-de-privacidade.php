<div class="container pt-1 pt-md-5 pb-5">

<div class="col-12 text-center">[expo_h1]</div>

<p class="mt-4 font-10 text-justify">
Todas as suas informações pessoais recolhidas serão usadas para ajudar a tornar a sua visita no nosso site a mais produtiva e agradável possível.
A garantia da confidencialidade dos dados pessoais dos usuários do nosso site é importante para o Exponencial Concursos. Todas as informações pessoais relativas a membros, assinantes, clientes ou visitantes que usem o Exponencial Concursos serão tratadas com a máxima diligência e proteção.
A informação pessoal recolhida pode incluir o seu nome, e-mail, número de telefone e/ou celular, endereço, data de nascimento e/ou outros.
<br><br>
O uso do Exponencial Concursos pressupõe a aceitação deste Acordo de Privacidade. A equipe do Exponencial Concursos reserva-se ao direito de alterar este acordo sem aviso prévio. Deste modo, recomendamos que consulte a nossa política de privacidade com regularidade de forma a estar sempre atualizado.
</p>

<h2 class="text-center">Os anúncios</h2>

<p class="mt-4 font-10 text-justify">
Tal como outros websites, coletamos e utilizamos informação contida nos anúncios. A informação contida nos anúncios inclui o seu endereço IP (Internet Protocol), o navegador que utilizou ao visitar o nosso website (como o Internet Explorer, Google Chrome ou o Mozilla Firefox), o tempo da sua visita e que páginas visitou dentro do nosso website.
</p>

<h2 class="text-center">Cookies</h2>

<p class="mt-4 font-10 text-justify">
O Google, como provedor de serviços, utiliza cookies para exibir anúncios no nosso site. Com o uso de cookies, o Google pode exibir anúncios com base nas visitas que o leitor fez a outros sites na Internet.
<br><br>
Utilizamos cookies para armazenar informação, tais como as suas preferências pessoais quando visita o nosso site. Isto poderá incluir um simples pop-up, ou um link para vários serviços que providenciamos, tais como coaching, fóruns e cursos oferecidos.
<br><br>
Também utilizamos publicidade de terceiros no nosso site para suportar os custos de manutenção. Alguns destes anúncios poderão utilizar tecnologias como os cookies e/ou web beacons quando divulgados em nosso site, o que fará com que esses anunciantes (como o Google por meio do Google AdSense) também recebam a sua informação pessoal, como o endereço IP, o seu navegador, etc. Esta função é geralmente utilizada para geotargeting (mostrar publicidade do Rio de Janeiro apenas aos leitores oriundos do Rio de Janeiro por exemplo) ou apresentar publicidade direcionada a um tipo de usuário (como mostrar publicidade de restaurante a um usuário que visita sites de culinária regularmente, por exemplo).
<br><br>
Você pode a qualquer momento desativar os seus cookies, nas opções do seu navegador, ou efetuando alterações nas ferramentas de programas Antivírus, como o Norton Internet Security. No entanto, isto poderá alterar a forma como interage com o nosso ou outros sites. Isso poderá afetar ou não permitir que faça logins em programas, sites ou fóruns da nossa e de outras redes.
</p>

<h2 class="text-center">Links a Sites de Terceiros</h2>

<p class="mt-4 font-10 text-justify">
O Exponencial Concursos possui links para outros sites, os quais, a nosso ver, podem conter informações e/ou ferramentas úteis para os nossos visitantes. A nossa política de privacidade não é aplicada a sites de terceiros. Portanto, ao visitar outro site a partir do nosso, sugerimos que leia a politica de privacidade dele.
<br><br>
Não nos responsabilizamos pela política de privacidade ou conteúdo presente em sites de terceiros.
</p>
</div>