<section>
    <div class="container mt-5 mt-sm-4 mb-sm-3 mb-0 font-14 h1-lg">
    
      <div class="col-12 text-center">
      [expo_h1]
      </div>       
      <div class="mt-3 d-none d-md-block text-center col-12 text-dark">
        Filtrar por:      
      <a class="text-dark t-d-none" id="filtro-todos-concursos" href="#">
      &nbsp;Todos os Concursos</a>    
      <a class="text-dark t-d-none" id="filtro-edital-divulgado" href="#"> &nbsp;Edital Divulgado</a>      
      <a class="text-dark t-d-none" id="filtro-edital-proximo" href="#">   &nbsp;Edital Próximo</a>
    </div>
    <div class="d-block d-md-none col-12 text-center">
       <div>Filtrar por:</div>      
      <div><a class="text-dark t-d-none" id="filtro-todos-concursos" href="#">&nbsp;Todos os Concursos</a></div>      
      <div><a class="text-dark t-d-none" id="filtro-edital-divulgado" href="#">&nbsp;Edital Divulgado</a></div>       
      <div><a class="text-dark t-d-none" id="filtro-edital-proximo" href="#">&nbsp;Edital Próximo</a></div>  
    </div>

    </div>
      
    <div class="container">
    <div class="row woocommerce home_midia mt-3 ml-auto mr-auto">
      <ul id="concursos" class="products ml-auto mr-auto">[home_concursos]</ul>
    </div>
    </div>
    <div style="text-align: center; margin-bottom: 30px;">
      <a id="carregar-mais" class="t-d-none text-dark" href="#">CARREGAR MAIS</a>
    </div>
</section>

<div class="col-12 bg-gray">
<div class="col-12 text-center font-pop mb-3 pt-3">
    Depoimentos
</div>

[rev_slider depoimentos-home]
</div>
<div class="col-12 text-center font-pop mb-3 mt-5">
    Notícias Atualizadas
</div>
    


<div class="container mt-3 mb-3 font-14">
  <div class="d-none d-md-block text-center col-12 text-dark">
        Filtrar por:      
      <a class="text-dark t-d-none" id="filtro-noticias" href="#">
      &nbsp;Notícias</a>    
      <a class="text-dark t-d-none" id="filtro-artigos" href="#"> &nbsp;Artigos</a>      
      <a class="text-dark t-d-none" id="filtro-videos" href="#">   &nbsp;Vídeos</a>
    </div>
    <div class="d-block d-md-none col-12 text-center">
       <div>Filtrar por:</div>      
      <div><a class="text-dark t-d-none" id="filtro-noticias" href="#">&nbsp;Notícias</a></div>      
      <div><a class="text-dark t-d-none" id="filtro-artigos" href="#">&nbsp;Artigos</a></div>       
      <div><a class="text-dark t-d-none" id="filtro-videos" href="#">&nbsp;Vídeos</a></div>  
    </div>
</div>

<div class="container">
<div class="row woocommerce home_midia mt-3 ml-auto mr-auto">
  <ul id="midias" class="products">[home_midia]</ul>
</div>
</div>

<div style="text-align: center; margin-bottom: 30px;">
  <a class="btn u-btn-darkblue font-12 font-arial font-weight-bold" href="/blog-noticias">+ Noticias</a> 
  <a class="btn btn-warning font-12 font-arial font-weight-bold text-white" href="/blog-artigos">+ Artigos</a> 
  <a class="btn btn-danger font-12 font-arial font-weight-bold" href="/blog-videos">+ Videos</a>
</div>

<script> 
jQuery(function() { jQuery('#filtro-noticias').click(function(e) { e.preventDefault(); jQuery('#midias').html('</p><div style="margin-bottom: 30px">Carregando...</div><p>'); jQuery.get('/wp-content/themes/academy/xhr-filtrar-midias.php?midia=news', function(data) { jQuery('#midias').html(data); }); });
jQuery('#filtro-artigos').click(function(e) {
    e.preventDefault();
    jQuery('#midias').html('</p><div style="margin-bottom: 30px">Carregando...</div><p>');     jQuery.get('/wp-content/themes/academy/xhr-filtrar-midias.php?midia=articles', function(data) {
        jQuery('#midias').html(data); 
    });
});

jQuery('#filtro-videos').click(function(e) {
    e.preventDefault();
    jQuery('#midias').html('</p><div style="margin-bottom: 30px">Carregando...</div><p>'); 
    jQuery.get('/wp-content/themes/academy/xhr-filtrar-midias.php?midia=videos', function(data) {
        jQuery('#midias').html(data); 
    });
});

jQuery('#filtro-edital-proximo').click(function(e) { 
    e.preventDefault(); 
    jQuery('#concursos').html('</p><div style="margin-bottom: 30px">Carregando...</div><p>'); 
    jQuery.get('/wp-content/themes/academy/ajax/filtrar_concursos.php?status=2', function(data) { 
        jQuery('#concursos').html(data); 
    });
});

jQuery('#filtro-edital-divulgado').click(function(e) { 
    e.preventDefault(); 
    jQuery('#concursos').html('</p><div style="margin-bottom: 30px">Carregando...</div><p>'); 
    jQuery.get('/wp-content/themes/academy/ajax/filtrar_concursos.php?status=1', function(data) { 
        jQuery('#concursos').html(data); 
    });
});

jQuery('#filtro-todos-concursos').click(function(e) { 
    e.preventDefault(); 
    jQuery('#concursos').html('</p><div style="margin-bottom: 30px">Carregando...</div><p>'); 
    jQuery.get('/wp-content/themes/academy/ajax/filtrar_concursos.php', function(data) { 
        jQuery('#concursos').html(data); 
    });
});

jQuery('#carregar-mais').click(function(e) {
    e.preventDefault();
      jQuery('#concursos li:hidden').slice(0,6).show();
        // exibir_esconder_carregar_mais();
});
}); 
</script>