<div class="row pb-5 <?= $estilo ?>">
	
<?php ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);?>
<?php foreach ( $produtos as $item ) : ?>
	<?php 
		$qtde_aulas = $item->pro_qtde_aulas;
		$qtde_esquemas = $item->pro_qtde_esquemas;
		$qtde_questoes = $item->pro_qtde_questoes;
		$carrinho_url = UrlHelper::get_adicionar_produto_ao_carrinho_url($item->post_id, FALSE);
		$professores = ProdutoModel::listar_professores($item->post_id);
		
		$preco_html = ProdutoHelper::get_preco_produto_wp_html($item->post_id);

		$wc_product = wc_get_product($item->post_id);
		$preco = $wc_product->get_price();

        $ocultar_parcelamento = get_post_meta($item->post_id, PRODUTO_OCULTAR_PARCELAMENTO, true) !== ''
            ? get_post_meta($item->post_id, PRODUTO_OCULTAR_PARCELAMENTO, true)
            : 'no';


	//TODO: esse filtro deveria estar apenas na query e na contagem
	if($exibe_pacote || $item->pro_is_pacote == 0){ ?>
	<div class="cursos-por col-12 col-hide mt-4">
	<div class="ml-auto mr-auto bg-white col-12 row border rounded p-0">
		<div class="concurso-altura col-12 p-0">
		<div class="concurso-curso-tit mt-3 col-12 text-roboto text-uppercase font-weight-bold text-dark font-14">
			<?= $item->post_title; ?>
		</div>		
		<div class="col-12 text-roboto text-dark">
			<?= ProdutoHelper::formatar_professores($professores) ?>
		</div>								
							
		<div class="mt-2 mb-2 col-12 row mr-auto ml-auto p-0">
			<div class="aula-tamanho col-12 col-md-2 text-center pl-3 pl-md-0 font-10" style="<?= $qtde_aulas == 0 ? 'display:none' : '' ?>">
				<div class="bg-agua text-white font-weight-bold">
					Aulas
		    	</div>
				<div class="text-blue">
					<?= numero_inteiro($qtde_aulas)?>
				</div>
			</div>
			
		<div class="aula-tamanho mb-2 col-12 col-md-2 text-center pl-3 pl-md-0 font-10" style="<?= $qtde_esquemas == 0 ? 'display:none' : '' ?>">
			<div class="bg-agua text-white font-weight-bold">
				Esquemas
			</div>
			<div class="text-blue">
				<?= numero_inteiro($qtde_esquemas)?>
			</div>
		</div>
		
		<div class="aula-tamanho mb-2 col-12 col-md-2 text-center pl-3 pl-md-0 font-10" style="<?= $qtde_questoes == 0 ? 'display:none' : '' ?>">
			<div class="bg-agua text-white font-weight-bold">
				Questões
			</div>
			<div class="text-blue">
				<?= numero_inteiro($qtde_questoes) ?>
			</div>
		</div>								
		</div>
		</div>

		<div class="concurso-footer pt-3 pb-3 bg-gray row col-12 ml-auto mr-auto p-0">
			<div class="c-price line-height-1-2 mt-auto mb-auto col-8">
			
			<?php if($preco > 0) : ?>						
			
				<?php
					if( $wc_product->is_type("subscription") ){
						
						$preco_recorrente = ProdutoHelper::get_preco_recorrente( $wc_product ); 
						$preco = $preco_recorrente['preco'];
						$duracao = $preco_recorrente['duracao'];
						$sem_desconto = $preco_recorrente['sem_desconto'];
						$periodicidade_str1 = $preco_recorrente['periodicidade_str1'];
						$periodicidade_str2 = $preco_recorrente['periodicidade_str2'];

						if($duracao){
							if($sem_desconto > $preco){
								$preco_html = $duracao . "x de <del>" . moeda($sem_desconto) . "</del> " . moeda($preco) . " " . $periodicidade_str2;
							}else{
								$preco_html = $duracao . "x de " . moeda($sem_desconto) . " " . $periodicidade_str2;
							}
						}else{
							if($sem_desconto > $preco){
								$preco_html = "<del>" . moeda($sem_desconto) . "</del> " . moeda($preco) . " " . $periodicidade_str1;
							}else{
								$preco_html = moeda($sem_desconto) . " " . $periodicidade_str1;
							}
						}

						$ocultar_parcelamento = 'yes';
					}
				?>

				<span class="font-price-del font-weight-bold font-roboto font-15 text-blue"><?= $preco_html ?></span>

                <?php if($ocultar_parcelamento == 'no') : ?>
                    <div class="c-juros d-block d-md-inline font-roboto font-15 text-blue">
                        <span class="c-juros d-block d-md-inline font-11"> ou 12x <?= moeda($preco / 12) ?> </span>
                        <span class="c-juros d-block d-md-inline font-11">sem juros</span>
				    </div>
                <?php endif ?>

			<?php else : ?>
				<div class="mt-3 mb-4 font-weight-bold font-12">Grátis</div>
			<?php endif; ?>
			</div>						
				<?php //print_r($item); ?>	
			<div class="c-btn text-right mt-auto mb-auto col-4">
				<a class="mt-1 btn u-btn-blue" href="<?= UrlHelper::get_produto_url($item->post_name) ?>">Saiba Mais</a>
				
				<?php if(is_usuario_logado()): ?>
					<?php if($preco > 0) : ?>
						<a class="mt-1 btn u-btn-primary btn-adicionar-carrinho" data-nome="<?= $item->post_name?>" data-id="<?= $item->post_id ?>" data-preco="<?= $preco ?>" href="<?= $carrinho_url ?>">Comprar</a>
					<?php else : ?>
						<a class="mt-1 btn btn-danger text-white btn-adicionar-carrinho" data-nome="<?= $item->post_name?>" data-id="<?= $item->post_id ?>" data-preco="<?= $preco ?>" href="<?= $carrinho_url ?>">Grátis</a>
					<?php endif; ?>
				<?php else: ?>
					<?php if($preco > 0) : ?>
						<a class="mt-1 btn u-btn-primary btn-adicionar-carrinho login-modal-link" data-toggle="modal" data-target="#login-modal" href="#" data-redirect="<?= $carrinho_url ?>" data-nome="<?= $item->post_name?>" data-id="<?= $item->post_id ?>" data-preco="<?= $preco ?>" href="#">Comprar</a>
					<?php else : ?>
						<a class="mt-1 btn btn-danger text-white btn-adicionar-carrinho login-modal-link" data-toggle="modal" data-target="#login-modal" href="#" data-redirect="<?= $carrinho_url ?>" data-nome="<?= $item->post_name?>" data-id="<?= $item->post_id ?>" data-preco="<?= $preco ?>" href="#">Grátis</a>
					<?php endif; ?>
				<?php endif; ?>
			</div>

			</div>	
		</div>
	</div><?php } ?>
		<?php endforeach; ?>
<?php if($ver_todos_url): ?>
	<div class="col-12 d-flex justify-content-center">
		<div class="p-4 text-center col-9">
			<a style="font-size: 25px; white-space: normal;" class="btn btn-primary btn-lg font-weight-bold text-uppercase" href="<?= $ver_todos_url ?>">Clique aqui para ver todos os cursos desta seção</a>
		</div>
	</div>
<?php endif; ?>
</div>

<script>
jQuery(function() {                       
  jQuery("#pesquisa-grid-3").click(function() {  
   jQuery(".cursos-por").addClass("col-4").removeClass("col-12");
   jQuery(".concurso-altura").addClass("concurso-h-top");
   jQuery(".concurso-footer").removeClass("pt-3 pb-3")
   .addClass("concurso-footer-h-footer");
    jQuery(".aula-tamanho").removeClass("col-md-2")
    .addClass("col-md-4");
    jQuery(".c-btn").removeClass("col-4")
    .addClass("col-5");
    jQuery(".c-price").removeClass("col-8")
    .addClass("col-7");
    jQuery(".c-juros").removeClass("d-md-inline");
  });
  jQuery("#pesquisa-linha-3").click(function() {  
    jQuery(".cursos-por").addClass("col-12").removeClass("col-4"); 
     jQuery(".concurso-altura").removeClass("concurso-h-top");
    jQuery(".concurso-footer").removeClass("concurso-footer-h-footer")
    .addClass("pt-3 pb-3");
    jQuery(".aula-tamanho").addClass("col-md-2")
    .removeClass("col-md-4");
    jQuery(".c-btn").addClass("col-4")
    .removeClass("col-5");
    jQuery(".c-price").addClass("col-8")
    .removeClass("col-7");
    jQuery(".c-juros").addClass("d-md-inline");
  });
});

  
</script>