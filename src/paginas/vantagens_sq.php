
<div class="ml-auto mr-auto col-12 col-lg-10 col-xl-8 col-1080 mt-5">
	<div class="row ml-auto mr-auto">
		<div class="col-12 col-12 col-md-7 col-lg-8"></div>
		<div class="font-12 text-center text-md-left col-12 col-md-5 col-lg-4"> 
			<span class="ml-0 ml-md-3">Usuário</span>
			<span class="ml-5">Assinante</span>
		</div>		
	</div>
	
	<div class="row ml-auto mr-auto">
		<div class="col-12 col-12 col-md-7 col-lg-8 sq-vantagem-label fundo-cinza-escuro">Resolução ilimitada de questões (*).</div>
		<div class="text-center text-md-left col-12 col-md-5 col-lg-4"> 
		<div class="sq-vantagem-usuario text-center fundo-vermelho-escuro"><img src="<?php echo get_tema_image_url('sq-nao-contempla.png')?>"></div>
		<div class="sq-vantagem-assinante text-center fundo-verde-escuro"><img src="<?php echo get_tema_image_url('sq-contempla.png')?>"></div>
		</div>
	</div>
	
	<div class="row ml-auto mr-auto">
		<div class="sq-vantagem-label fundo-cinza-claro col-12 col-md-7 col-lg-8">Participe do fórum de cada questão.</div>
		<div class="text-center text-md-left col-12 col-md-5 col-lg-4"> 
		<div class="sq-vantagem-usuario text-center fundo-verde-claro"><img src="<?php echo get_tema_image_url('sq-contempla.png')?>"></div>
		<div class="sq-vantagem-assinante text-center fundo-verde-claro"><img src="<?php echo get_tema_image_url('sq-contempla.png')?>"></div>
		</div>
	</div>
	
	<div class="row ml-auto mr-auto">
		<div class="sq-vantagem-label fundo-cinza-escuro col-12 col-md-7 col-lg-8">Impressão ilimitada de questões.</div>
		<div class="text-center text-md-left col-12 col-md-5 col-lg-4"> 
		<div class="sq-vantagem-usuario text-center fundo-verde-escuro"><img src="<?php echo get_tema_image_url('sq-contempla.png')?>"></div>
		<div class="sq-vantagem-assinante text-center fundo-verde-escuro"><img src="<?php echo get_tema_image_url('sq-contempla.png')?>"></div>
		</div>
	</div>
	
	<div class="row ml-auto mr-auto">
		<div class="sq-vantagem-label fundo-cinza-claro col-12 col-md-7 col-lg-8">Estatísticas de desempenho.</div>
		<div class="text-center text-md-left col-12 col-md-5 col-lg-4"> 
		<div class="sq-vantagem-usuario text-center fundo-vermelho-claro"><img src="<?php echo get_tema_image_url('sq-nao-contempla.png')?>"></div>
		<div class="sq-vantagem-assinante text-center fundo-verde-claro"><img src="<?php echo get_tema_image_url('sq-contempla.png')?>"></div>
		</div>
	</div>
	
	<div class="row ml-auto mr-auto">
		<div class="sq-vantagem-label fundo-cinza-escuro col-12 col-md-7 col-lg-8">Comentar questões.</div>
		<div class="text-center text-md-left col-12 col-md-5 col-lg-4"> 
		<div class="sq-vantagem-usuario text-center fundo-verde-escuro"><img src="<?php echo get_tema_image_url('sq-contempla.png')?>"></div>
		<div class="sq-vantagem-assinante text-center fundo-verde-escuro"><img src="<?php echo get_tema_image_url('sq-contempla.png')?>"></div>
		</div>
	</div>
	
	<div class="row ml-auto mr-auto">
		<div class="sq-vantagem-label fundo-cinza-claro col-12 col-md-7 col-lg-8">Ler comentários de alunos.</div>
		<div class="text-center text-md-left col-12 col-md-5 col-lg-4"> 
		<div class="sq-vantagem-usuario text-center fundo-verde-claro"><img src="<?php echo get_tema_image_url('sq-contempla.png')?>"></div>
		<div class="sq-vantagem-assinante text-center fundo-verde-claro"><img src="<?php echo get_tema_image_url('sq-contempla.png')?>"></div>
		</div>
	</div>
	
	<div class="row ml-auto mr-auto">
		<div class="sq-vantagem-label fundo-cinza-escuro col-12 col-md-7 col-lg-8">Ler comentários dos professores.</div>
		<div class="text-center text-md-left col-12 col-md-5 col-lg-4"> 
		<div class="sq-vantagem-usuario text-center fundo-vermelho-escuro"><img src="<?php echo get_tema_image_url('sq-nao-contempla.png')?>"></div>
		<div class="sq-vantagem-assinante text-center fundo-verde-escuro"><img src="<?php echo get_tema_image_url('sq-contempla.png')?>"></div>
		</div>
	</div>
	
	<div class="row ml-auto mr-auto">
		<div class="sq-vantagem-label fundo-cinza-claro col-12 col-md-7 col-lg-8">Descontos na aquisição de simulados.</div>
		<div class="text-center text-md-left col-12 col-md-5 col-lg-4"> 
		<div class="sq-vantagem-usuario text-center fundo-vermelho-claro"><img src="<?php echo get_tema_image_url('sq-nao-contempla.png')?>"></div>
		<div class="sq-vantagem-assinante text-center fundo-verde-claro"><img src="<?php echo get_tema_image_url('sq-contempla.png')?>"></div>
		</div>
	</div>
	
	<div class="row ml-auto mr-auto">
		<div class="sq-vantagem-label fundo-cinza-escuro col-12 col-md-7 col-lg-8">Cadernos de questões.</div>
		<div class="text-center text-md-left col-12 col-md-5 col-lg-4"> 
		<div class="sq-vantagem-usuario text-center fundo-vermelho-escuro"><img src="<?php echo get_tema_image_url('sq-nao-contempla.png')?>"></div>
		<div class="sq-vantagem-assinante text-center fundo-verde-escuro"><img src="<?php echo get_tema_image_url('sq-contempla.png')?>"></div>
		</div>
	</div>

	<div class="row ml-auto mr-auto">
		<div class="sq-vantagem-label fundo-cinza-claro col-12 col-md-7 col-lg-8">Cadernos compartilhados.</div>
		<div class="text-center text-md-left col-12 col-md-5 col-lg-4"> 
		<div class="sq-vantagem-usuario text-center fundo-vermelho-claro"><img src="<?php echo get_tema_image_url('sq-nao-contempla.png')?>"></div>
		<div class="sq-vantagem-assinante text-center fundo-verde-claro"><img src="<?php echo get_tema_image_url('sq-contempla.png')?>"></div>
		</div>
	</div>

	<div class="row ml-auto mr-auto">
		<div class="sq-vantagem-label fundo-cinza-escuro col-12 col-md-7 col-lg-8">Questões favoritas.</div>
		<div class="text-center text-md-left col-12 col-md-5 col-lg-4"> 
		<div class="sq-vantagem-usuario text-center fundo-vermelho-escuro"><img src="<?php echo get_tema_image_url('sq-nao-contempla.png')?>"></div>
		<div class="sq-vantagem-assinante text-center fundo-verde-escuro"><img src="<?php echo get_tema_image_url('sq-contempla.png')?>"></div>
		</div>
		<p class="mt-3">(*) Para usuário não-assinante: limitado a 100 questões por dia.
	</div>
</div>
	

