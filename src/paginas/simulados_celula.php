<?php
KLoader::model('ProdutoModel');
KLoader::helper('UrlHelper');

$p = $_GET['p'] ?: 0;
$g = $_GET['g'] ?: null;
$t = $_GET['t'] ?: null;

$simulados_por_pagina = 15;
$offset = $p * $simulados_por_pagina;

$filtros = [
	'somente_gratuitos' => $g,
	'somente_simulados' => true,
    'somente_visiveis' => true,
	'titulo' => $t,
    'status' => [STATUS_POST_PUBLICADO]
];

$simulados = ProdutoModel::buscar($filtros, $simulados_por_pagina, $offset);

// $simulados = listar_simulados($offset, $simulados_por_pagina, $g, $t);
$ultima_pagina = (($p + 1) * $simulados_por_pagina) > count($simulados) ? true : false;

foreach ($simulados as $simulado) : 
	$post = get_post($simulado->ID);
	
	$post->post_id = $post->ID;	

	$carrinho_url = UrlHelper::get_adicionar_produto_ao_carrinho_url($post->post_id, FALSE);
	
	$categorias = $post->post_title;
	$produto = new WC_Product($post);	
	$preco = $simulado->price == 0? "gratuito simulado" : "simulado";
?>

<div class="col-12 col-md-6 col-lg-4">	
	<div class="border rounded mt-5 box-w-100">
		<div style="display:none" class="search-categoria"><?= $categorias ?></div> 
		<div style="display:none" class="search-preco"><?= $preco ?></div>	
		<div class="simulado-height">
			<div class="p-2">
				<div class="p-1">
					<span class="text-dark font-weight-bold font-15">
						<?= $post->post_title ?>
					</span>					
				<div class="mt-2 font-10">
					<span class="font-weight-bold">Professores: </span>
					<?= get_autores_str($post); ?>					
				</div>
				</div>
			</div>
		</div>		
			<div class="ml-auto mr-auto row bg-gray simulado-h-footer">
				<div class="text-left col-6 col-lg-7 mt-auto mb-auto pr-3 pr-lg-0 text-blue">
					<div class="font-weight-bold font-15 line-height-1-3"><?= $produto->get_price_html(); ?></div>
					<?php if($produto->get_price() > 0) : ?>
					<div class="line-height-1-3">
						<smal>ou</smal>
						12x
						<?php echo moeda($produto->get_price() / 12) ; ?>
						<span class="d-block d-xl-inline">sem juros</span>
					</div>	
					<?php endif ?>				
				</div>	

				<div class="col-6 col-lg-5 text-right mt-auto mb-auto pr-lg-2 pr-xl-3">
					<a class="mt-1 btn-cursos btn u-btn-blue" href="<?php echo get_permalink($post); ?>">Saiba Mais</a>
					<?php if(is_usuario_logado()): ?>
						<?php if($produto->price > 0) : ?>					
							<a class="mt-1 btn-cursos btn u-btn-primary" href="<?= $carrinho_url ?>">Comprar</a>
						<?php else : ?>
							<a class="mt-1 btn-cursos btn btn-danger" href="<?= $carrinho_url ?>">Grátis</a>
						<?php endif;?>
					<?php else : ?>
						<?php if($produto->price > 0) : ?>					
							<a class="mt-1 btn-cursos btn u-btn-primary" data-toggle="modal" data-target="#login-modal" href="#" data-redirect="<?= $carrinho_url ?>">Comprar</a>
						<?php else : ?>
							<a class="mt-1 btn-cursos btn btn-danger" data-toggle="modal" data-target="#login-modal" href="#" data-redirect="<?= $carrinho_url ?>">Grátis</a>
						<?php endif;?>
					<?php endif;?>
				</div>	
			</div>	
	</div>
</div>

<?php endforeach; ?>


