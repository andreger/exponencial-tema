<?php 
KLoader::model("BlogModel");
KLoader::model("ColaboradorModel");
KLoader::helper("BlogHelper");
KLoader::helper("UrlHelper");

get_header();

global $post;

$tipos = BlogModel::listar_tipos($post->ID);
$blog = BlogModel::get_by_id($post->ID);
?>

<?php while ( have_posts() ) : the_post(); ?>

	<?php KLoader::view("blogs/navbar") ?>

	<div class="container pt-md-5">
		<div class="pt-4">
			<div class="w-100 blog-post-titulo">
				<a class="font-18 t-d-none text-blue" href="<?php the_permalink(); ?>">
					<h1><?= get_h1(); ?></h1>
				</a>

				<div class="font-12 text-blue">
					<?php the_date('d-m-Y'); ?>, por
    				<?php if(ColaboradorModel::get_by_id(get_the_author_meta('ID'))) : ?>
    					<?= get_professor_link(get_the_author_meta('ID'), get_the_author()); ?>
    				<?php else : ?>
    					<?= get_the_author() ?>
    				<?php endif ?>
				</div>
			</div>
			<?php echo get_share_buttons(); ?>
		</div>

		<?php if( has_post_thumbnail() ) : ?>
			<div class="row">
				<div class="pic_outter">
					<?php the_post_thumbnail(array(400,400)); ?>
				</div>
			</div>
		<?php endif ?>

		
		<div class="blog-post">
			<?php echo wpautop(get_the_content()); ?>
		</div>

	</div>
	
	
	<div class="login-options text-center">
		<a class="btn u-btn-primary mb-4" href="<?php echo in_category('articles') ? '/blog-artigos' : '/blog-noticias' ?>">&nbsp;Voltar&nbsp;</a>
	</div>
	

<?php endwhile; ?>

<?php get_footer(); ?>