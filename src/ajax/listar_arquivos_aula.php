<?php 
include_once $_SERVER['DOCUMENT_ROOT'] .'/wp-load.php';

KLoader::model("PedidoModel");
KLoader::helper("ProdutoHelper");
KLoader::helper("PDFHelper");

$produto_id = $_POST['p'];
$index = $_POST['i'];
$tipo = $_POST['t'];
$demo = $_POST['d'];


if(!$demo)
{
	$pedido_id = get_pedido_usuario_comprou_produto(get_current_user_id(), $produto_id);
}

if($demo || $pedido_id) {
	$html = "";

	$arquivos = PedidoModel::listar_arquivos_aulas_disponiveis_agrupados_por_aula($produto_id, $pedido_id, $demo);
	
	$aula = get_cronograma_aula_por_index($produto_id, $index);

	if($tipo == TIPO_AULA_PDF || $tipo == TIPO_AULA_MAPA || $tipo == TIPO_AULA_RESUMO) {

		if($tipo == TIPO_AULA_PDF)
		{
			$imagem_url = get_tema_image_url('icone-pdf-lista.png');
		}
		elseif($tipo == TIPO_AULA_MAPA)
		{
			$imagem_url = get_tema_image_url('icone-mapa-lista-40px.png');
		}
		elseif($tipo == TIPO_AULA_RESUMO)
		{
			$imagem_url = get_tema_image_url('icone-resumo-lista-40px.png');
		}
		
		// Links do PDF da aula do produto COM merge
		$fundir_arquivos_pdf = get_post_meta($produto_id, PRODUTO_FUNDIR_ARQUIVOS, true)?:NO;
		if($fundir_arquivos_pdf == YES) {

		    $pedido_id = $demo ? 0 : $pedido_id;
		    
		    $url = "/download-aula?pr={$produto_id}&i={$index}&o={$pedido_id}&t={$tipo}";
		    $nome = PDFHelper::get_nome_pdf_merge($produto_id, $index, $tipo);
		    
		    $html .= "<div><img src='{$imagem_url}'><a class='btn-baixar' href='{$url}'>" . $nome
		    . "</a></div>";
		    
		}

		// Links dos PDFs da aula do produto SEM merge e/ou MP3
		foreach($arquivos[$aula['nome']] as $item) {
			if($item['tipo'] == $tipo && ( ($fundir_arquivos_pdf && endsWith($item['filename'], 'mp3') ) || ($fundir_arquivos_pdf == NO) ) ) {
				$html .= "<div><img src='{$imagem_url}'><a class='btn-baixar' href='{$item['download_url']}'>{$item['filename']}</a></div>";
			}
		}
	
	}

	if($tipo == TIPO_AULA_VIDEO) {
		$imagem_url = get_tema_image_url('icone-youtube-lista.png');

		foreach($arquivos[$aula['nome']] as $item) {
			if($item['tipo'] == TIPO_AULA_VIDEO) {
				$vimeo = get_vimeo_video_by_url($item['download_url']);
				$html .= "<div><img src='{$imagem_url}'><a href='{$item['download_url']}' data-lity>" . ( $vimeo['nome']?:$item['download_url'] ) . "</a></div>";
			}
		}
	}

	if($tipo == TIPO_AULA_CADERNO) {
		$imagem_url = get_tema_image_url('icone-caderno-lista-40px.png');

		foreach($arquivos[$aula['nome']] as $item) {
			if($item['tipo'] == TIPO_AULA_CADERNO) {
				$html .= "<div><img src='{$imagem_url}'><a href='{$item['url']}' target='_blank'>" . $item['nome'] . "</a></div>";
			}
		}
	}

	if(!$html) {
	    $html .= "Nenhum arquivo está disponível.";
	}
	
	echo "<div class='cronograma-detalhe-interno'>{$html}</div>";
}
else {
	$adicionar_produto_url = adicionar_produto_ao_carrinho_url($produto_id);

	/**
	 * Aulas na Página do curso - se for grátis, botão deve ser o mesmo texto do botão de ação
	 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2609
	 */
	$texto = ProdutoHelper::get_botao_acao_text($produto_id);

	echo "<div class='alert alert-info row'>
			<div class='text-md-left text-center col-12 col-md-9 pt-1 pt-lg-2'>Você não tem acesso aos arquivos, pois ainda não adquiriu este produto.</div>
			<div class='mt-2 mt-md-0 col-12 col-md-3 text-center text-md-right'><a class='btn u-btn-darkblue' href='{$adicionar_produto_url}'>{$texto}
			</a></div>
			
		</div>";
}
