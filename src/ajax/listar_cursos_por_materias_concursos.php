<?php

use Exponencial\Core\Cache\CacheFactory;

include_once $_SERVER['DOCUMENT_ROOT'] .'/wp-load.php';
KLoader::model('ConcursoModel');
KLoader::helper('UrlHelper');

global $post;

$items = ConcursoModel::listar_concursos_com_cursos();

foreach ($items as &$item) {
	$item->url = UrlHelper::get_cursos_por_concurso_especifico_url($item->con_slug);
	$item->qtde_cursos = $item->con_qtde_cursos;
	$item->titulo = $item->post_title;
}

$data['items'] = $items;