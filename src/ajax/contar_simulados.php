<?php
include_once $_SERVER['DOCUMENT_ROOT'] .'/wp-load.php';

KLoader::model('ProdutoModel');

$filtros = [
	'somente_gratuitos' => $_GET['g'] ?: null,
	'somente_simulados' => true,
    'somente_visiveis' => true,
	'titulo' => $_GET['t'] ?: null,
    'status' => [STATUS_POST_PUBLICADO]
];

echo ProdutoModel::buscar_contar($filtros);
