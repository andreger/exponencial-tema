<?php
include_once $_SERVER['DOCUMENT_ROOT'] .'/wp-load.php';

KLoader::model("DownloadModel");

$dados = [
    'dow_id' => null,
    'dow_relatorio_id' => $_GET['relatorio_id'],
    'dow_filtros' => serialize($_POST),
    'dow_data' => date("Y-m-d H:i:s"),
    'dow_status' => DOWNLOAD_STATUS_PENDENTE,
    'dow_arquivo' => null,
    'dow_tentativas' => 0,
    'user_id' => get_current_user_id()
];

DownloadModel::criar($dados);

echo "1";