<?php
include_once $_SERVER['DOCUMENT_ROOT'] .'/wp-load.php';


if($id = $_GET['id']) {
	$lib = vimeo_auth();

	if($_GET['pagina']){
		$pagina = "&page=".$_GET['pagina'];
	}

	$videos = $lib->request('/albums/' . $id .'/videos?per_page=100&fields=uri,link,name,pictures'.$pagina, array(), 'GET');
	//print_r($videos);
	echo vimeo_search();

	echo "<div><a href='#' class='vimeo-voltar'>&lt; Voltar</a></div>";

	echo "<h2>Selecione um vídeo</h2>";

	if($videos) {
		echo "<div style='display: table-row;'>";
		foreach ($videos['body']['data'] as $item) {

			echo 
				"<span class='vimeo-video-box'>
					<a href='#' onclick='adicionar_vimeo(\"{$item['link']}\", event)'>
						<img src='{$item['pictures']['sizes'][1]['link']}'><br>
						{$item['name']}
					</a>
				</span>";
		}	
		echo "</div><div>";		
		$page = $videos['body']['page'];
		if($videos['body']['paging']['previous']){
			echo "<a href='#' onclick='listar_videos_vimeo({$id}, event, ".($page-1).")'>&lt;&nbsp;Anterior&nbsp;</a>&nbsp;&nbsp;";
		}
		if($videos['body']['paging']['next']){
			echo "<a href='#' onclick='listar_videos_vimeo({$id}, event, ".($page+1).");'>&nbsp;Próximo&nbsp;&gt;</a>&nbsp;&nbsp;";
		}
		echo "</div><br/>";
	}
	else {
		echo "<div>Não foram encontrados vídeos nessa categoria</div>";
	}
	

}
?>