<?php
include_once $_SERVER['DOCUMENT_ROOT'] .'/wp-load.php';


$q = $_GET['q'];

$lib = vimeo_auth();

$videos = $lib->request('/me/videos/', array('query' => $q, 'fields' => 'uri,link,name,pictures'), 'GET');

echo vimeo_search();

echo "<div><a href='#' class='vimeo-voltar'>&lt; Voltar</a></div>";

echo "<h2>Selecione um vídeo</h2>";

if($videos) {

	if($videos['body']['total'] == 0) {
		echo "<div>Não foram encontrados vídeos para essa busca</div>";	
	}

	else {

		foreach ($videos['body']['data'] as $item) {

			echo 
				"<span class='vimeo-video-box'>
					<a href='#' onclick='adicionar_vimeo(\"{$item['link']}\", event)'>
						<img src='{$item['pictures']['sizes'][1]['link']}'><br>
						{$item['name']}
					</a>
				</span>";
		}	
	}
}
else {
	echo "<div>Erro ao conectar o servidor do Vimeo</div>";
}
?>