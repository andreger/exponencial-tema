<?php
include_once $_SERVER['DOCUMENT_ROOT'] .'/wp-load.php';

KLoader::model("ProdutoModel");
KLoader::helper("ProdutoHelper");
KLoader::helper("UrlHelper");

$data = array();

$busca = $_POST['busca'];
$concurso_id = $_POST['concurso_id'];
$slug = $_POST['slug'];

$data['title'] = get_h1($concurso_id, true);
	
$data['qtd_premiums'] = ProdutoModel::contar_por_concurso($slug, null, null, null, null, null, true, $busca);
$data['qtd_destaques'] = ProdutoModel::contar_por_concurso($slug, null, null, true, null, null, false, $busca);
$data['qtd_pacotes'] = ProdutoModel::contar_por_concurso($slug, null, true, null, null, null, false, $busca);
$data['qtd_cursos'] = ProdutoModel::contar_por_concurso($slug, false, false, null, null, false, false, $busca);
$data['qtd_simulados'] = ProdutoModel::contar_por_concurso($slug, true, false, null, null, false, false, $busca);
$data['qtd_pacotes_simulados'] = ProdutoModel::contar_por_concurso($slug, true, true, null, null, false, false, $busca);
$data['qtd_cadernos'] = ProdutoModel::contar_por_concurso($slug, false, false, null, null, true, false, $busca);	

$data['premiums'] = ProdutoModel::listar_por_concurso($slug, null, null, null, null, null, true, $busca, 0, LIMITE_POR_CONCURSO_ESPECIFICO_SESSAO);
$data['destaques'] = ProdutoModel::listar_por_concurso($slug, null, null, true, null, null, false, $busca, 0, LIMITE_POR_CONCURSO_ESPECIFICO_SESSAO);
$data['pacotes'] = ProdutoModel::listar_por_concurso($slug, null, true, null, null, null, false, $busca, 0, LIMITE_POR_CONCURSO_ESPECIFICO_SESSAO);
$data['cursos'] = ProdutoModel::listar_por_concurso($slug, false, false, null, null, false, false, $busca, 0, LIMITE_POR_CONCURSO_ESPECIFICO_SESSAO);
$data['simulados'] = ProdutoModel::listar_por_concurso($slug, true, false, null, null, false, false, $busca, 0, LIMITE_POR_CONCURSO_ESPECIFICO_SESSAO);
$data['pacotes_simulados'] = ProdutoModel::listar_por_concurso($slug, true, true, null, null, false, false, $busca, 0, LIMITE_POR_CONCURSO_ESPECIFICO_SESSAO);
$data['cadernos'] = ProdutoModel::listar_por_concurso($slug, false, false, null, null, true, false, $busca, 0, LIMITE_POR_CONCURSO_ESPECIFICO_SESSAO);	

$data['ver_todos_url'] = UrlHelper::get_cursos_por_concurso_especifico_paginado_url($slug);

KLoader::view("cursos/por-concurso", $data);