<?php
include_once $_SERVER['DOCUMENT_ROOT'] .'/wp-load.php';

KLoader::model("MailchimpModel");
KLoader::helper("RecaptchaHelper");

if($_POST['mc-submit']) {
    
    // Google ReCaptcha
    $validacao_ok = false;
    if(!is_selenium() && !is_desenvolvimento()) {
        $response = RecaptchaHelper::get_response($_POST['g-recaptcha-response']);
        
        if ($response != null && $response->success) {
            $validacao_ok = true;
        }
    }
    
    if($validacao_ok) {
    
        $email = $_POST['email'];
        $nome = $_POST['nome'];
        $telefone = $_POST['telefone'];
        
        $interests_novo = [];
        
        // se já existe atualiza as áreas do contato
        if($mc_contato = MailChimpModel::get_contato(MC_LISTA_EXPONENCIAL_CONCURSOS, $email)) {
            
            if($interests = $mc_contato->interests) {
                
                // converte stdclass para array
                $interests = get_object_vars($interests);
                foreach ($interests as $key => $value) {
                    
                    if($value) {
                        $interests_novo[$key] = true;
                    }
                    else {
                        $interests_novo[$key] = false;
                    }
                }
            }
        }
        
        if($area = $_POST['area'])  {
            $interests_novo[$area] = true;
        }
        
        $data = [
            'email_address' => $email,
            'status_if_new' => "subscribed",
            'merge_fields' => [
                'FNAME' => $nome,
                'PHONE' => $telefone
            ],
            'interests' => $interests_novo
        ];
        
        // salva ou atualiza o contato
        MailChimpModel::salvar_contato(MC_LISTA_EXPONENCIAL_CONCURSOS, $data);
        
        echo "1";
        exit;
    }
}

echo "0";
exit;