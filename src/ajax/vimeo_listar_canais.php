<?php
include_once $_SERVER['DOCUMENT_ROOT'] .'/wp-load.php';

$lib = vimeo_auth();

if($_GET['pagina']){
	$pagina = "&page=".$_GET['pagina'];
}

$canais = $lib->request('/me/albums?fields=uri,name&per_page=100&filter=moderated'.$pagina, array(), 'GET');
//print_r($canais);
echo vimeo_search();

echo "<h2>Escolha uma categoria</h2>";

if($canais) {
	foreach ($canais['body']['data'] as $item) {
		$uri_a = explode('/', $item['uri']);
		echo "<div><img src='/wp-content/themes/academy/images/folder.png' style='vertical-align:sub'> <a href='#' onclick='listar_videos_vimeo({$uri_a[4]}, event, null)'>{$item['name']}</div>";
	}
	echo "<br/>";
	$page = $canais['body']['page'];
	if($canais['body']['paging']['previous']){
		echo "<a href='#' onclick='listar_canais_vimeo(this, ".($page-1).")'>&lt;&nbsp;Anterior&nbsp;</a>&nbsp;&nbsp;";
	}
	if($canais['body']['paging']['next']){
		echo "<a href='#' onclick='listar_canais_vimeo(this, ".($page+1).")'>&nbsp;Próximo&nbsp;&gt;</a>&nbsp;&nbsp;";
	}
}
else {
	echo "<div>Não há categorias cadastradas.</div>";
}
?>