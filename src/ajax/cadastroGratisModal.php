<?php
include_once $_SERVER['DOCUMENT_ROOT'] .'/wp-load.php';
?>
<div class="remodal" data-remodal-id="mc-modal">
    <button data-remodal-action="close" class="remodal-close"></button>

    <div class="mc-carregando contato form_outter" style="display: none">
        <div class="mensagem-panel">
            <div style="padding: 10px"><img alt="Carregando" width="80" src='/wp-content/themes/academy/images/carregando.gif'></div>
            <div style="padding: 10px">Aguarde só mais um momento...</div>
            <div style="padding: 10px">Estamos realizando seu cadastro!</div>
        </div>
    </div>

    <div class="mc-sucesso mc-response contato form_outter" style="display: none">
        <div class="mensagem-panel">
            <img alt="Confirmar" src="<?= get_tema_image_url("confirm-grande.png") ?>" width="150">
            <div class="p-5 font-20">
                Seu cadastro foi realizado com sucesso.
            </div>
            <button data-remodal-action="cancel" class="btn btn-primary">Fechar</button>
        </div>
    </div>

    <div class="mc-erro mc-response contato form_outter" style="display: none">
        <div class="mensagem-panel">
            <img alt="Erro" src="<?= get_tema_image_url("erro-grande.png") ?>" width="150">
            <div class="p-5 font-20">
                Você precisa selecionar o campo<br>"Não sou um robô".
            </div>
            <button data-remodal-action="cancel" class="btn btn-danger">Fechar</button>
        </div>
    </div>
</div>

<link rel="stylesheet" type="text/css" href="/wp-content/themes/academy/css/remodal.css">
<link rel="stylesheet" type="text/css" href="/wp-content/themes/academy/css/remodal-default-theme.css">
<script src="/wp-content/themes/academy/js/remodal.min.js"></script>
<script src="/wp-content/themes/academy/js/jquery.validate.min.js"></script>
<script src="/wp-content/themes/academy/js/jquery.mask.js"></script>
<script>
    jQuery.validator.addMethod("onlychar", function(value, element) {
        value = value.trim();
        return this.optional(element) || /^([a-zA-ZãõáéíóúâêôçÃÕÁÉÍÓÚÂÊÔÇ]+\s)*[a-zA-ZãõáéíóúâêôçÃÕÁÉÍÓÚÂÊÔÇ]+$/.test(value);
    }, "Nome inválido");

    jQuery.validator.addMethod("brphone", function(value, element) {
        value = value.replace("_","");
        return this.optional(element) || /^([\(]{1}[1-9]{2}[\)]{1}[ ]{1}[0-9]{4,5}[\-]{1}[0-9]{4})$/.test(value);
    }, "Telefone inválido");


    jQuery(function() {

        jQuery("#frm-mc-footer").submit( function(e) {
            e.preventDefault();
        }).validate({
            rules: {
                email: {
                    required: true,
                    email: true,
                },
                nome: {
                    required: true,
                    onlychar: true
                },
                telefone: {
                    required: true,
                    brphone: true
                },
                area: {
                    required: true,
                },
                concordo: {
                    required: true,
                }
            },
            messages: {
                email: {
                    required: "E-mail é obrigatório",
                    email: "Formato do e-mail inválido",
                },
                nome: {
                    required: "Nome é obrigatório",
                    onlychar: "O nome deve conter apenas letras e espaços"
                },
                telefone: {
                    required: "Telefone é obrigatório",
                    brphone: "Formato do telefone inválido"
                },
                area: {
                    required: "Área de interesse é obrigatória",
                },
                concordo: {
                    required: "Você precisa concordar com a política de privacidade",
                }
            },
            errorPlacement: function(error, element) {
                error.insertAfter(element);
            },
            submitHandler: function( form ){
                jQuery(".mc-response").hide();
                jQuery(".mc-carregando").show();

                var inst = jQuery('[data-remodal-id=mc-modal]').remodal();
                inst.open();

                var dados = jQuery( form ).serialize();

                jQuery.ajax({
                    type: "POST",
                    url: "/wp-content/themes/academy/ajax/enviar_mc_footer.php",
                    data: dados,
                    success: function( data )
                    {
                        jQuery(".mc-carregando").hide();

                        if(data == "1") {
                            jQuery(".mc-sucesso").show();
                        }
                        else {
                            jQuery(".mc-erro").show();
                        }

                        grecaptcha.reset();
                    }
                });

                return false;
            }
        });

        var SPMaskBehavior = function (val) {
                return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
            },
            spOptions = {
                onKeyPress: function(val, e, field, options) {
                    field.mask(SPMaskBehavior.apply({}, arguments), options);
                }
            };

        jQuery('#telefone').mask(SPMaskBehavior, spOptions);



    });
</script>