<?php 
include_once $_SERVER['DOCUMENT_ROOT'] .'/wp-load.php';

$assunto = $_POST['assunto'];
$mensagem = $_POST['mensagem'];
$professor_id = $_POST['professor_id'];

$professor = get_usuario_array($professor_id);
$aluno = get_usuario_array();

$texto = "Contato realizado pelo Site do Professor ({$professor['nome_completo']}):<br><br><br>";
$texto .= "Nome: {$aluno['nome_completo']}<br><br>";
$texto .= "Email: {$aluno['email']}<br><br>";
$texto .= "Assunto: {$assunto}<br><br>";
$texto .= "Mensagem: {$mensagem}";

$secret = "6LeN3Q0TAAAAAKZ59noxA3HXU2Ea5hmaMZxZ4JO8";
$response = null;
$reCaptcha = new ReCaptcha($secret);

if ($_POST["g-recaptcha-response"]) {

	$response = $reCaptcha->verifyResponse($_SERVER["REMOTE_ADDR"],	$_POST["g-recaptcha-response"]);
  				
  	if ($response != null && $response->success) {
		enviar_email($professor['email'], $assunto, $texto);
		echo mensagem_sucesso("E-mail enviado com sucesso para o professor.");
	}
	else {
		echo mensagem_erro("Código de verificação incorreto.");
	}

}