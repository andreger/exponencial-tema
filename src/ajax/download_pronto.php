<?php
include_once $_SERVER['DOCUMENT_ROOT'] .'/wp-load.php';

KLoader::model("DownloadModel");

$relatorio_id = $_POST['relatorio_id'];
$usuario_id = get_current_user_id();
$filtros = [
    'produto_id' => $_POST['produto_id'],
    'pedido_id' => $_POST['pedido_id'],
];

echo DownloadModel::get_download_pronto($usuario_id, $relatorio_id, $filtros) ? 1 : 0;