<?php
include_once $_SERVER['DOCUMENT_ROOT'] .'/wp-load.php';


if($id = $_GET['id']) {

	KLoader::helper("UrlHelper");
	
	$por_pagina = 30;
	$offset = 0;

	$pagina = 0;
	if($_GET['pagina']){
		$pagina = $_GET['pagina']?:0;
		$offset = $pagina * $por_pagina;
	}

	echo "<div><a href='#' class='caderno-voltar'>&lt; Voltar</a></div>";

	echo "<h2>Selecione um caderno</h2>";

	KLoader::model("CadernoModel");
	$cadernos = CadernoModel::listar_cadernos_professor($id, $offset, $por_pagina);

	$total = CadernoModel::contar_cadernos_professor($id);

	if($cadernos) {

		echo "<div style='display: table-row;'>";
		
		foreach ($cadernos as $caderno) {
			$caderno_url = UrlHelper::get_caderno_url( $caderno->cad_id );
			echo "<div><img src='/wp-content/themes/academy/images/ico-cadernos.png' style='vertical-align:sub'> 
				<a href='#' onclick='adicionar_caderno(\"{$caderno->cad_id}\", \"{$caderno_url}\", event)'>{$caderno->cad_nome}</div>";
		}	
		
		echo "</div><div style='margin-top: 20px;'>";

		if($pagina > 0){
			echo "<a href='#' onclick='listar_cadernos_professor({$id}, event, ".($pagina-1).")'>&lt;&nbsp;Anterior&nbsp;</a>&nbsp;&nbsp;";
		}
		if($total > $offset){
			echo "<a href='#' onclick='listar_cadernos_professor({$id}, event, ".($pagina+1).");'>&nbsp;Próximo&nbsp;&gt;</a>&nbsp;&nbsp;";
		}
		echo "</div><br/>";
	}
	else {
		echo "<div>Não foram encontrados cadernos para esse professor</div>";
	}
	

}
?>