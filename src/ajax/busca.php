<?php
include_once $_SERVER['DOCUMENT_ROOT'] .'/wp-load.php';

header('Content-Type: application/json');

$retorno = [];

$categorias = get_categorias_de_curso(CATEGORIA_CONCURSO);
foreach($categorias as $item) {
	array_push($retorno, [
		"name" => $item->name
	]);
}

$categorias = get_categorias_de_curso(CATEGORIA_MATERIA);
foreach($categorias as $item) {
	array_push($retorno, [
		"name" => $item->name
	]);
}

$professores = get_professores(FALSE);
foreach($professores as $item) {
	array_push($retorno, [
		"name" => $item->display_name
	]);
}

echo json_encode($retorno);