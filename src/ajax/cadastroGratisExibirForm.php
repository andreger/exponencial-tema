<?php
include_once $_SERVER['DOCUMENT_ROOT'] .'/wp-load.php';

$post_id = $_GET['post_id'];
$post = get_post($post_id);

switch ($post->post_type) {

    case "post": {
        $exibir = true; break;
    }

    case "page": {
        $inclusoes = [
            'home',
            'sistema-de-questoes',
            'simulados',
            'professores',
            'blog-noticias',
            'blog-posts-noticias',
            'blog-artigos',
            'blog-posts-artigos',
            'blog-videos',
            'blog-posts-videos',
            'descontos-promocoes',
            'metodologia-exponencial',
            'quem-somos',
            'perguntas-frequentes-portal',
            'como-funciona',
            'parceiros',
        ];

        if(in_array($post->post_name, $inclusoes)) {
            $exibir = true;
        }

        break;
    }
}

if($exibir) :
    KLoader::model("MailchimpModel");

    $areas_options = MailChimpModel::listar_interesses(MC_LISTA_EXPONENCIAL_CONCURSOS, MC_INTERESSE_AREA);
    ?>

    <div style="border-top: 1px solid #eee; padding: 30px 0">
        <div class="container">
            <form method="post" action="/" id="frm-mc-footer">
                <div class="row">
                    <div class="col-md-4">
                        <div class="font-weight-bold text-blue pt-md-1 text-center font-30">Cadastro GRÁTIS</div>
                        <div class="font-weight-bold text-blue pt-md-1 font-20 text-center" style="line-height: 30px">Acesso exclusivo a notícias atualizadas e promoções.</div>
                    </div>

                    <div class="col-md-4">

                        <input type="text" class="form-control" placeholder="Nome" name="nome" id="cadastro-gratis-nome">
                        <input type="text" class="form-control mt-3" placeholder="Telefone" name="telefone" id="telefone">
                        <input type="text" class="form-control mt-3" placeholder="E-mail" name="email">

                        <select class="form-control mt-3" name="area">
                            <option value="">Sua área de interesse
                                <?php foreach ($areas_options as $item) : ?>
                            <option value="<?= $item->mca_id ?>" ><?= $item->mca_nome ?>
                                <?php endforeach; ?>
                        </select>

                    </div>
                    <div class="col-md-4">
                        <div style="width: 304px">
                            <div>
                                <?= carregar_recaptcha() ?>
                            </div>
                            <div style="font-size: 12px" class="text-justify">
                                <input type="checkbox" name="concordo" id="concordo" checked="checked">
                                Você concorda com a nossa <a href="/politica-de-privacidade">Política de Privacidade</a> e aceita receber informações adicionais do Exponencial Concursos.
                            </div>
                            <button id="mc-cadastrar" type="submit" value="1" name="mc-submit" class="btn u-btn-darkblue font-14 font-arial font-weight-bold col-12" disabled>Cadastrar</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div id="cadastro-gratis-modal"></div>
<?php endif ?>