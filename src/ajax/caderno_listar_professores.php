<?php
include_once $_SERVER['DOCUMENT_ROOT'] .'/wp-load.php';

if($_GET['pagina']){
	$pagina = "&page=".$_GET['pagina'];
}

echo "<h2>Escolha um professor</h2>";

$professores = get_professores(FALSE);

if($professores) {
	foreach ($professores as $professor) {

		echo "<div><img src='/wp-content/themes/academy/images/folder.png' style='vertical-align:sub'> <a href='#' onclick='listar_cadernos_professor({$professor->ID}, event, null)'>{$professor->display_name}</div>";
	}
	echo "<br/>";
}
else {
	echo "<div>Não há professores cadastrados.</div>";
}
?>