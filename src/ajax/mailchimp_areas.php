<?php
include_once $_SERVER['DOCUMENT_ROOT'] .'/wp-load.php';

KLoader::model("MailchimpModel");

$lista_id = isset($_GET['list_id']) ? $_GET['list_id'] : MC_LISTA_EXPONENCIAL_CONCURSOS;

$interesses_remotos = MailChimpModel::listar_interesses_remotos($lista_id);

foreach ($interesses_remotos->categories as $interesse_remoto) {
    
    if($interesse_remoto->title == MAILCHIMP_INTERESSE_POR_AREAS) {
        $area_id = $interesse_remoto->id;
        break;
    }
}

$areas_options = MailChimpModel::listar_interesses($lista_id, $area_id);

$html = isset($_GET["hide_default"]) ?  "" : "<option value=''>Sua área de interesse";

foreach ($areas_options as $item) {
    $html .= "<option value='{$item->mca_id}'>{$item->mca_nome}";
}

echo $html;