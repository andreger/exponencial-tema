<?php
//Template name:custom home
?>
<?php get_header(); ?>
<?php the_post(); ?>
<div>ASPOKDPOKASOKDPASOP</div>
<section id="online_courses">
<div class="section_head">
<div class="row">
<div class="section_head_img"><img class="alignnone" alt="" src="http://webacers.in/expo/wp-content/uploads/2014/04/bandeira-online.png" /></div>
<div class="header_text">Cursos on-line</div>
</div>
</div>
</section>
<div class="section_content" id="online_courses_content">
<div class="sectiongap"></div>
<div class="row">
<?php echo do_shortcode('[one_third]');?>
<div class="outer_div">
<h3><img alt="" src="http://webacers.in/expo/wp-content/uploads/2014/04/icon-objetivo.png" />
Objetividade</h3>
<div class="text_small">Suspendisse ante mi, iaculis ac eleifend id, venenatis non eros. Sed rhoncus gravida elit, eu sollicitudin sem iaculis. Proin scelerisque, ipsum mollis posuere metus.</div>
</div>
<div class="outer_div">
<h3><img alt="" src="http://webacers.in/expo/wp-content/uploads/2014/04/icon-qualidade.png" />
Qualidade</h3>
<div class="text_small">Suspendisse ante mi, iaculis ac eleifend id, venenatis non eros. Sed rhoncus gravida elit, eu sollicitudin sem iaculis. Proin scelerisque, ipsum mollis posuere metus.</div>
</div>
<?php echo do_shortcode('[/one_third]');?>


<?php echo do_shortcode('[one_third]');?>
<div class="outer_div">
<h3><img alt="" src="http://webacers.in/expo/wp-content/uploads/2014/04/icon-esquematizacao.png" />Esquematização</h3>
<div class="text_small">Suspendisse ante mi, iaculis ac eleifend id, venenatis non eros. Sed rhoncus gravida elit, eu sollicitudin sem iaculis. Proin scelerisque, ipsum mollis posuere metus.</div>
</div>
<div class="outer_div">
<h3><img alt="" src="http://webacers.in/expo/wp-content/uploads/2014/04/icon-metodologia.png" />Metodologia</h3>
<div class="text_small">Suspendisse ante mi, iaculis ac eleifend id, venenatis non eros. Sed rhoncus gravida elit, eu sollicitudin sem iaculis. Proin scelerisque, ipsum mollis posuere metus.</div>
</div>
<?php echo do_shortcode('[/one_third]');?>
<?php echo do_shortcode('[one_third_last]');?>

<h3><img class="alignnone" alt="" src="http://webacers.in/expo/wp-content/uploads/2014/04/pdflist_headicon.png" />Cursos em destaque</h3>
<div class="pdflist"><?php echo do_shortcode('[widgets_on_pages id="Courses List HomePage"]');?></div>
<?php echo do_shortcode('[/one_third_last]');?>

</div>

<div class="section_content">
  <div class="sectionlargegap"></div>
  <div class="row">
	
  </div>
  <div class="sectionlargegap"></div>
</div>

<div class="row">
<div class="c_button"><img alt="" src="http://webacers.in/expo/wp-content/uploads/2014/04/bt-leiamais.png" /></div>
</div>
</div>
<!--  cursos on-line ends  -->

<section id="coaching">
<div class="section_head">
<div class="row">
<div class="section_head_img"><img class="alignnone" alt="" src="http://webacers.in/expo/wp-content/uploads/2014/04/bandeira-coaching.png" /></div>
<div class="header_text">Coaching</div>
</div>
</div>
</section>
<div class="section_content" id="coaching_content">
<div class="sectionlargegap"></div>
<div class="row"><?php echo do_shortcode('[one_third]');?>
<div class="c_img"><img alt="" src="http://webacers.in/expo/wp-content/uploads/2014/04/icon-personalizado.png" /></div>
<div class="c_head">
<h3>100% personalization</h3>
</div>
<div class="c_desc">Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet.</div>
<?php echo do_shortcode('[/one_third]');?>
<?php echo do_shortcode('[one_third]');?>
<div class="c_img"><img alt="" src="http://webacers.in/expo/wp-content/uploads/2014/04/icon-equipe.png" /></div>
<div class="c_head">
<h3>Trabalho em equipe</h3>
</div>
<div class="c_desc">Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet.</div>
<?php echo do_shortcode('[/one_third]');?>
<?php echo do_shortcode('[one_third_last]');?>

<div class="c_img"><img alt="" src="http://webacers.in/expo/wp-content/uploads/2014/04/icon-relogio.png" /></div>
<div class="c_head">
<h3>Nosso objectivo</h3>
</div>
<div class="c_desc">Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet.</div>
<?php echo do_shortcode('[/one_third_last]');?>

</div>
<div class="row">
<div class="c_button"><a href="http://expo.com/coaching/"><img alt="" src="http://webacers.in/expo/wp-content/uploads/2014/04/bt-saibamais.png" /></a></div>
</div>
</div>
<!--  cursos on-line ends  -->

<section id="qualified_team">
<div class="section_head">
<div class="row">
<div class="section_head_img"><img class="alignnone" alt="" src="http://webacers.in/expo/wp-content/uploads/2014/04/bandeira-time.png" /></div>
<div class="header_text">Nosso time e altamente qualificado</div>
</div>
</div>
</section>
<div class="section_content Equipe_bg" id="qualified_team_content">
<div class="sectiongap"></div>
<div class="row">
<div class="col-6 fleft">
<div class="circlearea fleft pos_rel">
<div class="globe_logo pos_abs"></div>
<div class=" area_1 fleft pos_rel"><!--| 7 |-->
<div class="tab7 pos_abs single_tab">
<div class="person_pic align7pic"><img alt="" src="http://expo.com/wp-content/themes/academy/images/sample_person.png" /></div>
<span class="align7text text">Nome 7</span>

</div>
<!--| 8 |-->
<div class="tab8 pos_abs single_tab">
<div class="person_pic align8pic"><img alt="" src="http://expo.com/wp-content/themes/academy/images/sample_person.png" /></div>
<span class="align8text text">Nome 8</span>

</div>
</div>
<div class="area_2 fright pos_rel"><!--| 1 |-->
<div class="tab1 pos_abs single_tab">
<div class="person_pic align1pic"><img alt="" src="http://expo.com/wp-content/themes/academy/images/sample_person.png" /></div>
<span class="align1text text">Nome 1</span>

</div>
<!--| 2 |-->
<div class="tab2 pos_abs single_tab">
<div class="person_pic align2pic"><img alt="" src="http://expo.com/wp-content/themes/academy/images/sample_person.png" /></div>
<span class="align2text text">Nome 2</span>

</div>
</div>
<div class="area_3 fleft pos_rel"><!--| 5 |-->
<div class="tab5 pos_abs single_tab">
<div class="person_pic align5pic"><img alt="" src="http://expo.com/wp-content/themes/academy/images/sample_person.png" /></div>
<span class="align5text text">Nome 5</span>

</div>
<!--| 6 |-->
<div class="tab6 pos_abs single_tab">
<div class="person_pic align6pic"><img alt="" src="http://expo.com/wp-content/themes/academy/images/sample_person.png" /></div>
<span class="align6text text">Nome 6</span>

</div>
</div>
<div class="area_4 fright pos_rel"><!--| 3 |-->
<div class="tab3 pos_abs single_tab">
<div class="person_pic align3pic"><img alt="" src="http://expo.com/wp-content/themes/academy/images/sample_person.png" /></div>
<span class="align3text text">Nome 3</span>

</div>
<!--| 4 |-->
<div class="tab4 pos_abs single_tab">
<div class="person_pic align4pic"><img alt="" src="http://expo.com/wp-content/themes/academy/images/sample_person.png" /></div>
<span class="align4text text">Nome 4</span>

</div>
</div>
</div>
</div>
<div class="col-6 fright">
<div class="discription_outter m_auto">
<div class="col-12 m_auto"><img class="fleft" alt="" src="http://expo.com/wp-content/themes/academy/images/person_img.png" />
<span class="fleft person_name_text">Nome do Professor </span>
<div class="clear"></div>
</div>
<div class="col-12 m_auto"><span class="person_detail">
Lorem ipsum dolor sit amet, consectetuer adipiscing elit
sed diam nonummy nibh euismod tincidunt ut laoreet
dolore magna aliquam erat volutpat Lorem ipsum dolor sit
amet, consectetuer adipiscing elit, sed diam nonummy
</span></div>
<div class="col-12 m_auto"></div>
<div class="col-12"></div>
</div>
</div>
<div class="clear"></div>
</div>
<div class="sectiongap"></div>
</div>
<section id="learn_more_contests">
<div class="section_head">
<div class="row">
<div class="section_head_img"><img class="alignnone" alt="" src="http://webacers.in/expo/wp-content/uploads/2014/04/badeira-brasil.png" /></div>
<div class="header_text">Saiba mais o que esta rolando no mundo dos concursos.</div>
</div>
</div>
</section>
<div class="section_content" id="learn_more_contests_content">
<div class="sectionlargegap"></div>
<div class="row"><?php echo do_shortcode('[one_third]]');?>
<h3><img alt="" src="http://webacers.in/expo/wp-content/uploads/2014/04/icon-artigos.png" />Artigos</h3>
<div class="c_img"><img alt="" src="http://webacers.in/expo/wp-content/uploads/2014/04/pic1.png" /></div>
<div class="c_desc">[widgets_on_pages id="Articles HomePage"]</div>
<?php echo do_shortcode('[/one_third]]');?>
<?php echo do_shortcode('[one_third]]');?>
<h3><img alt="" src="http://webacers.in/expo/wp-content/uploads/2014/04/icon-noticias.png" />Noticias</h3>
<div class="c_img"><img alt="" src="http://webacers.in/expo/wp-content/uploads/2014/04/pic2.png" /></div>
<div class="c_desc">[widgets_on_pages id="News HomePage"]</div>
<?php echo do_shortcode('[/one_third]]');?>
<?php echo do_shortcode('[one_third_last]');?><h3><img alt="" src="http://webacers.in/expo/wp-content/uploads/2014/04/icon-videos.png" />Videos</h3>
<div class="c_img"><img alt="" src="http://webacers.in/expo/wp-content/uploads/2014/04/pic3.png" /></div>
<div class="c_desc"><?php echo do_shortcode('[widgets_on_pages id="Videos HomePage"]');?></div>
<?php echo do_shortcode('[/one_third_last]');?>

</div>
</div>
<!--  learn more about contests ends  -->



<?php get_footer(); ?>
