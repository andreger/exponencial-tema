<?php get_header(); ?>

<div class="section_content">
 	<div class="row secao-barra-branca" style="margin-bottom: 20px">
  		Coaching
    </div>
</div>


<div class="row">
    <div class="coaching2-titulo-azul">
        <div class="coaching2-titulo-texto">O que os alunos falam?</div>
    </div>
</div>

<div class="row">
    <div class="coaching2-titulo">
    	<div><img class="coaching2-titulo-img" src="/wp-content/themes/academy/images/coaching2-icon.png"></div>
        <div class="coaching2-titulo-texto">Coaching - Vagas Abertas</div>
    </div>
</div>


<div class="row coaching2-row">
	<div>
		<a href="#" class="coaching2-area-link">Área Fiscal</a>
	</div>

	<div class="coaching2-area-cinza">
		<div class="fourcol column ">
			<img class="coaching2-area-img" src='/wp-content/themes/academy/images/coaching2-area-fiscal.png'>
			<div class="coaching2-area-concursos">
				<ul class="tick">
					<div class="coaching2-concurso"><li><span>Receita Federal</span></li></div>
					<div class="coaching2-concurso"><li><span>ICMS</span></li></div>
					<div class="coaching2-concurso"><li><span>ISS</span></li></div>
					<div class="coaching2-concurso"><li><span>Auditor-Fiscal do Trabalho</span></li></div>
				</ul>
			</div>
		</div>

		<div class="eightcol column last">
			<div class="row">
				<div class="fourcol column text-align-center">
					<a href="#">
						<div class="coaching2-box">
							<img src="http://expoconcursos/wp-content/themes/academy/images/coaching2-box1.png">
							<div class="coaching2-box-periodo">Mensal</div>
							<div class="coaching2-box-preco">R$ 9,99</div>
							<div class="coaching2-box-inscrevase">Inscreva-se Já!</div>
						</div>
					</a>
				</div>
				<div class="fourcol column text-align-center">
					<a href="#">
						<div class="coaching2-box">
							<img src="http://expoconcursos/wp-content/themes/academy/images/coaching2-box3.png">
							<div class="coaching2-box-periodo">Trimestral</div>
							<div class="coaching2-box-preco">R$ 99,99</div>
							<div class="coaching2-box-inscrevase">Inscreva-se Já!</div>
						</div>
					</a>
				</div>
				<div class="fourcol column text-align-center">
					<a href="#">
						<div class="coaching2-box">
							<img src="http://expoconcursos/wp-content/themes/academy/images/coaching2-box6.png">
							<div class="coaching2-box-periodo">Semestral</div>
							<div class="coaching2-box-preco">R$ 999,99</div>
							<div class="coaching2-box-inscrevase">Inscreva-se Já!</div>
						</div>
					</a>
				</div>
			</div>
		</div>
		<div style="clear:both;"></div>
	</div>
</div>

<div class="row coaching2-row">
	<div>
		<a href="#" class="coaching2-area-link">Área de Controle</a>
	</div>

	<div class="coaching2-area-cinza" style="display:none">
		<div class="fourcol column ">
			<img class="coaching2-area-img" src='/wp-content/themes/academy/images/coaching2-area-controle.png'>
			<div class="coaching2-area-concursos">
				<ul class="tick">
					<div class="coaching2-concurso"><li><span>TCU</span></li></div>
					<div class="coaching2-concurso"><li><span>CGU</span></li></div>
					<div class="coaching2-concurso"><li><span>Tribunais de Contas e Controladorias Estaduais e Municipais</span></li></div>
					<div class="coaching2-concurso"><li><span>TCM RJ</span></li></div>
				</ul>
			</div>
		</div>

		<div class="eightcol column last">
			<div class="row">
				<div class="fourcol column text-align-center">
					<a href="#">
						<div class="coaching2-box">
							<img src="http://expoconcursos/wp-content/themes/academy/images/coaching2-box1.png">
							<div class="coaching2-box-periodo">Mensal</div>
							<div class="coaching2-box-preco">R$ 9,99</div>
							<div class="coaching2-box-inscrevase">Inscreva-se Já!</div>
						</div>
					</a>
				</div>
				<div class="fourcol column text-align-center">
					<a href="#">
						<div class="coaching2-box">
							<img src="http://expoconcursos/wp-content/themes/academy/images/coaching2-box3.png">
							<div class="coaching2-box-periodo">Trimestral</div>
							<div class="coaching2-box-preco">R$ 99,99</div>
							<div class="coaching2-box-inscrevase">Inscreva-se Já!</div>
						</div>
					</a>
				</div>
				<div class="fourcol column text-align-center">
					<a href="#">
						<div class="coaching2-box">
							<img src="http://expoconcursos/wp-content/themes/academy/images/coaching2-box6.png">
							<div class="coaching2-box-periodo">Semestral</div>
							<div class="coaching2-box-preco">R$ 999,99</div>
							<div class="coaching2-box-inscrevase">Inscreva-se Já!</div>
						</div>
					</a>
				</div>
			</div>
		</div>
		<div style="clear:both;"></div>
	</div>
</div>

<div class="row coaching2-row">
	<div>
		<a href="#" class="coaching2-area-link">Área Policial</a>
	</div>

	<div class="coaching2-area-cinza" style="display:none">
		<div class="fourcol column ">
			<img class="coaching2-area-img" src='/wp-content/themes/academy/images/coaching2-area-policial.png'>
			<div class="coaching2-area-concursos">
				<ul class="tick">
					<div class="coaching2-concurso"><li><span>Polícia Federal</span></li></div>
					<div class="coaching2-concurso"><li><span>Polícia Rodoviária Federal</span></li></div>
					<div class="coaching2-concurso"><li><span>Polícias Civil e Militar</span></li></div>
				</ul>
			</div>
		</div>

		<div class="eightcol column last">
			<div class="row">
				<div class="fourcol column text-align-center">
					<a href="#">
						<div class="coaching2-box">
							<img src="http://expoconcursos/wp-content/themes/academy/images/coaching2-box1.png">
							<div class="coaching2-box-periodo">Mensal</div>
							<div class="coaching2-box-preco">R$ 9,99</div>
							<div class="coaching2-box-inscrevase">Inscreva-se Já!</div>
						</div>
					</a>
				</div>
				<div class="fourcol column text-align-center">
					<a href="#">
						<div class="coaching2-box">
							<img src="http://expoconcursos/wp-content/themes/academy/images/coaching2-box3.png">
							<div class="coaching2-box-periodo">Trimestral</div>
							<div class="coaching2-box-preco">R$ 99,99</div>
							<div class="coaching2-box-inscrevase">Inscreva-se Já!</div>
						</div>
					</a>
				</div>
				<div class="fourcol column text-align-center">
					<a href="#">
						<div class="coaching2-box">
							<img src="http://expoconcursos/wp-content/themes/academy/images/coaching2-box6.png">
							<div class="coaching2-box-periodo">Semestral</div>
							<div class="coaching2-box-preco">R$ 999,99</div>
							<div class="coaching2-box-inscrevase">Inscreva-se Já!</div>
						</div>
					</a>
				</div>
			</div>
		</div>
		<div style="clear:both;"></div>
	</div>
</div>

<div class="row coaching2-row">
	<div>
		<a href="#" class="coaching2-area-link">Tribunais (Analista e Técnico)</a>
	</div>

	<div class="coaching2-area-cinza" style="display:none">
		<div class="fourcol column ">
			<img class="coaching2-area-img" src='/wp-content/themes/academy/images/coaching2-tribunais.png'>
			<div class="coaching2-area-concursos">
				<ul class="tick">
					<div class="coaching2-concurso"><li><span>TRT</span></li></div>
					<div class="coaching2-concurso"><li><span>TRE</span></li></div>
					<div class="coaching2-concurso"><li><span>TRF</span></li></div>
					<div class="coaching2-concurso"><li><span>TJ</span></li></div>
				</ul>
			</div>
		</div>

		<div class="eightcol column last">
			<div class="row">
				<div class="fourcol column text-align-center">
					<a href="#">
						<div class="coaching2-box">
							<img src="http://expoconcursos/wp-content/themes/academy/images/coaching2-box1.png">
							<div class="coaching2-box-periodo">Mensal</div>
							<div class="coaching2-box-preco">R$ 9,99</div>
							<div class="coaching2-box-inscrevase">Inscreva-se Já!</div>
						</div>
					</a>
				</div>
				<div class="fourcol column text-align-center">
					<a href="#">
						<div class="coaching2-box">
							<img src="http://expoconcursos/wp-content/themes/academy/images/coaching2-box3.png">
							<div class="coaching2-box-periodo">Trimestral</div>
							<div class="coaching2-box-preco">R$ 99,99</div>
							<div class="coaching2-box-inscrevase">Inscreva-se Já!</div>
						</div>
					</a>
				</div>
				<div class="fourcol column text-align-center">
					<a href="#">
						<div class="coaching2-box">
							<img src="http://expoconcursos/wp-content/themes/academy/images/coaching2-box6.png">
							<div class="coaching2-box-periodo">Semestral</div>
							<div class="coaching2-box-preco">R$ 999,99</div>
							<div class="coaching2-box-inscrevase">Inscreva-se Já!</div>
						</div>
					</a>
				</div>
			</div>
		</div>
		<div style="clear:both;"></div>
	</div>
</div>

<div class="row coaching2-row">
	<div>
		<a href="#" class="coaching2-area-link">Área Legislativa</a>
	</div>

	<div class="coaching2-area-cinza" style="display:none">
		<div class="fourcol column ">
			<img class="coaching2-area-img" src='/wp-content/themes/academy/images/coaching2-area-legislativa.png'>
			<div class="coaching2-area-concursos">
				<ul class="tick">
					<div class="coaching2-concurso"><li><span>Câmara dos Deputados</span></li></div>
					<div class="coaching2-concurso"><li><span>Senado Federal</span></li></div>
					<div class="coaching2-concurso"><li><span>Assembléias Legislativas</span></li></div>
				</ul>
			</div>
		</div>

		<div class="eightcol column last">
			<div class="row">
				<div class="fourcol column text-align-center">
					<a href="#">
						<div class="coaching2-box">
							<img src="http://expoconcursos/wp-content/themes/academy/images/coaching2-box1.png">
							<div class="coaching2-box-periodo">Mensal</div>
							<div class="coaching2-box-preco">R$ 9,99</div>
							<div class="coaching2-box-inscrevase">Inscreva-se Já!</div>
						</div>
					</a>
				</div>
				<div class="fourcol column text-align-center">
					<a href="#">
						<div class="coaching2-box">
							<img src="http://expoconcursos/wp-content/themes/academy/images/coaching2-box3.png">
							<div class="coaching2-box-periodo">Trimestral</div>
							<div class="coaching2-box-preco">R$ 99,99</div>
							<div class="coaching2-box-inscrevase">Inscreva-se Já!</div>
						</div>
					</a>
				</div>
				<div class="fourcol column text-align-center">
					<a href="#">
						<div class="coaching2-box">
							<img src="http://expoconcursos/wp-content/themes/academy/images/coaching2-box6.png">
							<div class="coaching2-box-periodo">Semestral</div>
							<div class="coaching2-box-preco">R$ 999,99</div>
							<div class="coaching2-box-inscrevase">Inscreva-se Já!</div>
						</div>
					</a>
				</div>
			</div>
		</div>
		<div style="clear:both;"></div>
	</div>
</div>

<div class="row coaching2-row">
	<div>
		<a href="#" class="coaching2-area-link">Área Jurídica</a>
	</div>

	<div class="coaching2-area-cinza" style="display:none">
		<div class="fourcol column ">
			<img class="coaching2-area-img" src='/wp-content/themes/academy/images/coaching2-area-juridica.png'>
			<div class="coaching2-area-concursos">
				<ul class="tick">
					<div class="coaching2-concurso"><li><span>Procuradorias (PGE, PGM)</span></li></div>
					<div class="coaching2-concurso"><li><span>Defensores Públicos</span></li></div>
					<div class="coaching2-concurso"><li><span>Ministérios Públicos</span></li></div>
					<div class="coaching2-concurso"><li><span>Advocacia Pública (AGU, PGF, PFN e PGBACEN)</span></li></div>
				</ul>
			</div>
		</div>

		<div class="eightcol column last">
			<div class="row">
				<div class="fourcol column text-align-center">
					<a href="#">
						<div class="coaching2-box">
							<img src="http://expoconcursos/wp-content/themes/academy/images/coaching2-box1.png">
							<div class="coaching2-box-periodo">Mensal</div>
							<div class="coaching2-box-preco">R$ 9,99</div>
							<div class="coaching2-box-inscrevase">Inscreva-se Já!</div>
						</div>
					</a>
				</div>
				<div class="fourcol column text-align-center">
					<a href="#">
						<div class="coaching2-box">
							<img src="http://expoconcursos/wp-content/themes/academy/images/coaching2-box3.png">
							<div class="coaching2-box-periodo">Trimestral</div>
							<div class="coaching2-box-preco">R$ 99,99</div>
							<div class="coaching2-box-inscrevase">Inscreva-se Já!</div>
						</div>
					</a>
				</div>
				<div class="fourcol column text-align-center">
					<a href="#">
						<div class="coaching2-box">
							<img src="http://expoconcursos/wp-content/themes/academy/images/coaching2-box6.png">
							<div class="coaching2-box-periodo">Semestral</div>
							<div class="coaching2-box-preco">R$ 999,99</div>
							<div class="coaching2-box-inscrevase">Inscreva-se Já!</div>
						</div>
					</a>
				</div>
			</div>
		</div>
		<div style="clear:both;"></div>
	</div>
</div>

<div class="row coaching2-row">
	<div>
		<a href="#" class="coaching2-area-link">Área Bancária</a>
	</div>

	<div class="coaching2-area-cinza" style="display:none">
		<div class="fourcol column ">
			<img class="coaching2-area-img" src='/wp-content/themes/academy/images/coaching2-area-bancaria.png'>
			<div class="coaching2-area-concursos">
				<ul class="tick">
					<div class="coaching2-concurso"><li><span>BACEN</span></li></div>
					<div class="coaching2-concurso"><li><span>Banco do Brasil</span></li></div>
					<div class="coaching2-concurso"><li><span>Caixa Econômica</span></li></div>
					<div class="coaching2-concurso"><li><span>Bancos Regionais</span></li></div>
				</ul>
			</div>
		</div>

		<div class="eightcol column last">
			<div class="row">
				<div class="fourcol column text-align-center">
					<a href="#">
						<div class="coaching2-box">
							<img src="http://expoconcursos/wp-content/themes/academy/images/coaching2-box1.png">
							<div class="coaching2-box-periodo">Mensal</div>
							<div class="coaching2-box-preco">R$ 9,99</div>
							<div class="coaching2-box-inscrevase">Inscreva-se Já!</div>
						</div>
					</a>
				</div>
				<div class="fourcol column text-align-center">
					<a href="#">
						<div class="coaching2-box">
							<img src="http://expoconcursos/wp-content/themes/academy/images/coaching2-box3.png">
							<div class="coaching2-box-periodo">Trimestral</div>
							<div class="coaching2-box-preco">R$ 99,99</div>
							<div class="coaching2-box-inscrevase">Inscreva-se Já!</div>
						</div>
					</a>
				</div>
				<div class="fourcol column text-align-center">
					<a href="#">
						<div class="coaching2-box">
							<img src="http://expoconcursos/wp-content/themes/academy/images/coaching2-box6.png">
							<div class="coaching2-box-periodo">Semestral</div>
							<div class="coaching2-box-preco">R$ 999,99</div>
							<div class="coaching2-box-inscrevase">Inscreva-se Já!</div>
						</div>
					</a>
				</div>
			</div>
		</div>
		<div style="clear:both;"></div>
	</div>
</div>

<div class="row coaching2-row">
	<div>
		<a href="#" class="coaching2-area-link">INSS</a>
	</div>

	<div class="coaching2-area-cinza" style="display:none">
		<div class="fourcol column ">
			<img class="coaching2-area-img" src='/wp-content/themes/academy/images/coaching2-inss.png'>
			<div class="coaching2-area-concursos">
				<ul class="tick">
					<div class="coaching2-concurso"><li><span>Técnico INSS</span></li></div>
					<div class="coaching2-concurso"><li><span>Analista INSS</span></li></div>
				</ul>
			</div>
		</div>

		<div class="eightcol column last">
			<div class="row">
				<div class="fourcol column text-align-center">
					<a href="#">
						<div class="coaching2-box">
							<img src="http://expoconcursos/wp-content/themes/academy/images/coaching2-box1.png">
							<div class="coaching2-box-periodo">Mensal</div>
							<div class="coaching2-box-preco">R$ 9,99</div>
							<div class="coaching2-box-inscrevase">Inscreva-se Já!</div>
						</div>
					</a>
				</div>
				<div class="fourcol column text-align-center">
					<a href="#">
						<div class="coaching2-box">
							<img src="http://expoconcursos/wp-content/themes/academy/images/coaching2-box3.png">
							<div class="coaching2-box-periodo">Trimestral</div>
							<div class="coaching2-box-preco">R$ 99,99</div>
							<div class="coaching2-box-inscrevase">Inscreva-se Já!</div>
						</div>
					</a>
				</div>
				<div class="fourcol column text-align-center">
					<a href="#">
						<div class="coaching2-box">
							<img src="http://expoconcursos/wp-content/themes/academy/images/coaching2-box6.png">
							<div class="coaching2-box-periodo">Semestral</div>
							<div class="coaching2-box-preco">R$ 999,99</div>
							<div class="coaching2-box-inscrevase">Inscreva-se Já!</div>
						</div>
					</a>
				</div>
			</div>
		</div>
		<div style="clear:both;"></div>
	</div>
</div>

<div class="row coaching2-row">
	<div>
		<a href="#" class="coaching2-area-link">Agências e outras áreas</a>
	</div>

	<div class="coaching2-area-cinza" style="display:none">
		<div class="fourcol column ">
			<img class="coaching2-area-img" src='/wp-content/themes/academy/images/coaching2-agencias.png'>
			<div class="coaching2-area-concursos">
				<ul class="tick">
					<div class="coaching2-concurso"><li><span>CVM</span></li></div>
					<div class="coaching2-concurso"><li><span>SUSEP</span></li></div>
					<div class="coaching2-concurso"><li><span>ANS</span></li></div>
					<div class="coaching2-concurso"><li><span>ANTT</span></li></div>
					<div class="coaching2-concurso"><li><span>ANVISA</span></li></div>
				</ul>
			</div>
		</div>

		<div class="eightcol column last">
			<div class="row">
				<div class="fourcol column text-align-center">
					<a href="#">
						<div class="coaching2-box">
							<img src="http://expoconcursos/wp-content/themes/academy/images/coaching2-box1.png">
							<div class="coaching2-box-periodo">Mensal</div>
							<div class="coaching2-box-preco">R$ 9,99</div>
							<div class="coaching2-box-inscrevase">Inscreva-se Já!</div>
						</div>
					</a>
				</div>
				<div class="fourcol column text-align-center">
					<a href="#">
						<div class="coaching2-box">
							<img src="http://expoconcursos/wp-content/themes/academy/images/coaching2-box3.png">
							<div class="coaching2-box-periodo">Trimestral</div>
							<div class="coaching2-box-preco">R$ 99,99</div>
							<div class="coaching2-box-inscrevase">Inscreva-se Já!</div>
						</div>
					</a>
				</div>
				<div class="fourcol column text-align-center">
					<a href="#">
						<div class="coaching2-box">
							<img src="http://expoconcursos/wp-content/themes/academy/images/coaching2-box6.png">
							<div class="coaching2-box-periodo">Semestral</div>
							<div class="coaching2-box-preco">R$ 999,99</div>
							<div class="coaching2-box-inscrevase">Inscreva-se Já!</div>
						</div>
					</a>
				</div>
			</div>
		</div>
		<div style="clear:both;"></div>
	</div>
</div>				

<div style="margin-bottom: 40px"></div>

<div class="row">
    <div class="coaching2-titulo">
    	<div><img class="coaching2-titulo-img" src="/wp-content/themes/academy/images/coaching2-modulos.png"></div>
        <div class="coaching2-titulo-texto">Módulos do Programa</div>
    </div>
</div>

<div class="row coaching2-area-cinza">
    <div class="col-6 fleft text-align-right coaching2-modulo-esquerda wid_full">
    	<div>
    		<img class="coaching2-modulo-img" src="/wp-content/themes/academy/images/coaching2-raio-x.png">
    		<span class="coaching2-modulo-separador"></span>
    	</div>
    	<div class="coaching2-modulo-textos">
	        <div class="coaching2-modulo-titulo">Raio-x do Aluno</div>
	        <div class="coaching2-modulo-texto">
	        	Texto texto texto texto texto texto texto texto texto texto  
	        	Texto texto texto texto texto texto texto texto texto texto
	        	Texto texto texto texto texto texto texto texto texto texto
	        	Texto texto texto texto texto texto texto texto texto texto
	        </div>
	    </div>
    </div>
    <div class="col-6m fright text-align-left coaching2-modulo-direta coaching2-modulo-primeiro wid_full">
    	<div>
    		<span class="coaching2-modulo-separador"></span>
    		<img class="coaching2-modulo-img" src="/wp-content/themes/academy/images/coaching2-simulados.png">
    	</div>
    	<div class="coaching2-modulo-textos">
	        <div class="coaching2-modulo-titulo">Simulados online</div>
	        <div class="coaching2-modulo-texto">
	        	Texto texto texto texto texto texto texto texto texto texto  
	        	Texto texto texto texto texto texto texto texto texto texto
	        	Texto texto texto texto texto texto texto texto texto texto
	        	Texto texto texto texto texto texto texto texto texto texto
	        </div>
	    </div>
    </div>
    <div class="col-6 fleft text-align-right coaching2-modulo-esquerda wid_full">
    	<div>
    		<img class="coaching2-modulo-img" src="/wp-content/themes/academy/images/coaching2-material.png">
    		<span class="coaching2-modulo-separador"></span>
    	</div>
    	<div class="coaching2-modulo-textos">
	        <div class="coaching2-modulo-titulo">Material de estudo</div>
	        <div class="coaching2-modulo-texto">
	        	Texto texto texto texto texto texto texto texto texto texto  
	        	Texto texto texto texto texto texto texto texto texto texto
	        	Texto texto texto texto texto texto texto texto texto texto
	        	Texto texto texto texto texto texto texto texto texto texto
	        </div>
	    </div>
    </div>
    <div class="col-6m fright text-align-left coaching2-modulo-direta wid_full">
    	<div>
    		<span class="coaching2-modulo-separador"></span>
    		<img class="coaching2-modulo-img" src="/wp-content/themes/academy/images/coaching2-tecnicas.png">
    	</div>
    	<div class="coaching2-modulo-textos">
	        <div class="coaching2-modulo-titulo">Técnicas</div>
	        <div class="coaching2-modulo-texto">
	        	Texto texto texto texto texto texto texto texto texto texto  
	        	Texto texto texto texto texto texto texto texto texto texto
	        	Texto texto texto texto texto texto texto texto texto texto
	        	Texto texto texto texto texto texto texto texto texto texto
	        </div>
	    </div>
    </div>
    <div class="col-6 fleft text-align-right coaching2-modulo-esquerda wid_full">
    	<div>
    		<img class="coaching2-modulo-img" src="/wp-content/themes/academy/images/coaching2-planejamento.png">
    		<span class="coaching2-modulo-separador"></span>
    	</div>
    	<div class="coaching2-modulo-textos">
	        <div class="coaching2-modulo-titulo">Planejamento, clicos e metas</div>
	        <div class="coaching2-modulo-texto">
	        	Texto texto texto texto texto texto texto texto texto texto  
	        	Texto texto texto texto texto texto texto texto texto texto
	        	Texto texto texto texto texto texto texto texto texto texto
	        	Texto texto texto texto texto texto texto texto texto texto
	        </div>
	    </div>
    </div>
    <div class="col-6m fright text-align-left coaching2-modulo-direta wid_full">
    	<div>
    		<span class="coaching2-modulo-separador"></span>
    		<img class="coaching2-modulo-img" src="/wp-content/themes/academy/images/coaching2-acompanhamento.png">
    	</div>
    	<div class="coaching2-modulo-textos">
	        <div class="coaching2-modulo-titulo">Acompanhamento e feedback</div>
	        <div class="coaching2-modulo-texto">
	        	Texto texto texto texto texto texto texto texto texto texto  
	        	Texto texto texto texto texto texto texto texto texto texto
	        	Texto texto texto texto texto texto texto texto texto texto
	        	Texto texto texto texto texto texto texto texto texto texto
	        </div>
	    </div>
    </div>
    <div class="col-6 fleft text-align-right coaching2-modulo-esquerda wid_full">
    	<div>
    		<img class="coaching2-modulo-img" src="/wp-content/themes/academy/images/coaching2-questoes.png">
    		<span class="coaching2-modulo-separador"></span>
    	</div>
    	<div class="coaching2-modulo-textos">
	        <div class="coaching2-modulo-titulo">Questões online</div>
	        <div class="coaching2-modulo-texto">
	        	Texto texto texto texto texto texto texto texto texto texto  
	        	Texto texto texto texto texto texto texto texto texto texto
	        	Texto texto texto texto texto texto texto texto texto texto
	        	Texto texto texto texto texto texto texto texto texto texto
	        </div>
	    </div>
    </div>
    <div class="col-6m fright text-align-left coaching2-modulo-direta wid_full">
    	<div>
    		<span class="coaching2-modulo-separador"></span>
    		<img class="coaching2-modulo-img" src="/wp-content/themes/academy/images/coaching2-posedital.png">
    	</div>
    	<div class="coaching2-modulo-textos">
	        <div class="coaching2-modulo-titulo">Pós Edital</div>
	        <div class="coaching2-modulo-texto">
	        	Texto texto texto texto texto texto texto texto texto texto  
	        	Texto texto texto texto texto texto texto texto texto texto
	        	Texto texto texto texto texto texto texto texto texto texto
	        	Texto texto texto texto texto texto texto texto texto texto
	        </div>
	    </div>
    </div>
    <div class="col-6 fright text-align-left coaching2-modulo-esquerda coaching2-modulo-ultimo">
    </div>

    <div style="clear:both"></div>

	<div class="col-4 fleft">&nbsp;</div>
    <div class="col-4 text-align-center fleft">
    	<div><img class="coaching2-modulo-img" src="/wp-content/themes/academy/images/coaching2-aprovacao.png"></div>
    	<div class="coaching2-modulo-titulo">Aprovação</div>
    	<div class="coaching2-modulo-texto">
        	Texto texto texto texto texto texto texto texto texto texto  
        	Texto texto texto texto texto texto texto texto texto texto
        	Texto texto texto texto texto texto texto texto texto texto
        	Texto texto texto texto texto texto texto texto texto texto
        </div>
        <div class="coaching2-modulo-aprovacao">
        	<span class="coaching2-modulo-aprovacao-texto">
        		Veja o índice completo do coaching 
        	</span>
        	<a href="#"><img class="coaching2-modulo-aprovacao-pdf" src="/wp-content/themes/academy/images/coaching2-pdf.png"></a> <a href="https://vimeo.com/260868922" data-lity><img src="/wp-content/themes/academy/images/coaching2-video.png"></a>
        </div>
    </div>

	</div>
</div>

<div class="row">
    <div class="coaching2-titulo-sem-borda">
    	<div><img class="coaching2-titulo-img" src="/wp-content/themes/academy/images/coaching2-equipe.png"></div>
        <div class="coaching2-titulo-texto">Equipe</div>
    </div>
</div>

<?php get_carousel_equipe_coaching() ?>

<div class="row">
    <div class="coaching2-titulo">
    	<div><img class="coaching2-titulo-img" src="/wp-content/themes/academy/images/coaching2-desconto.png"></div>
        <div class="coaching2-titulo-texto">Use Cupons de Descontos</div>
    </div>
</div>

<div class="row">
	<div class="col-4 fleft wid_full">
		<div class="coaching2-desconto">
			<img class="coaching2-desconto-img" src='/wp-content/themes/academy/images/coaching2-desconto-box100.png'>
			<div class="coaching2-desconto-textos">
				<div class="coaching2-desconto-titulo">100%</div>
				<div class="coaching2-desconto-descricao">
					Técnicas de Estudo,<br>
					Caderno de Questões,<br>
					Simulados, Sistemas de Questões
				</div>
			</div>
		</div>
	</div>

	<div class="col-4 fleft wid_full">
		<div class="coaching2-desconto">
			<img class="coaching2-desconto-img" src='/wp-content/themes/academy/images/coaching2-desconto-box50.png'>
			<div class="coaching2-desconto-textos">
				<div class="coaching2-desconto-titulo">50%</div>
				<div class="coaching2-desconto-descricao">
					Pacotes de cursos,<br>
					Mapas Mentais
				</div>
			</div>
		</div>
	</div>

	<div class="col-4 fleft wid_full">
		<div class="coaching2-desconto">
			<img class="coaching2-desconto-img" src='/wp-content/themes/academy/images/coaching2-desconto-box25.png'>
			<div class="coaching2-desconto-textos">
				<div class="coaching2-desconto-titulo">25%</div>
				<div class="coaching2-desconto-descricao">
					Cursos Avulsos
				</div>
			</div>
		</div>
	</div>
</div>

<div style="margin-bottom:80px"></div>

<!--<div class="row" style="text-align: center">-->
<!--	<a href="/descontos_promocoes">-->
<!--    	<span class="coaching2-botao-azul">Acessar Cupons</span>-->
<!--    </a>-->
<!--</div>-->

<div style="margin-bottom:30px"></div>

<div class="row coaching2-faq">
    <div class="coaching2-titulo-sem-borda">
    	<div><img class="coaching2-titulo-img" src="/wp-content/themes/academy/images/coaching2-desconto.png"></div>
        <div class="coaching2-titulo-texto">Perguntas Frequentes</div>
    </div>

	<div style="margin-bottom:30px"></div>

	<div id="faq-block" class="border border-danger">
		<div class="faq-list" data-speed="200">
			<div class="single-faq expand-faq">
				<h2 id="o-coaching-pode-realmente-me-ajudar" class="faq-question expand-title">O Coaching pode realmente me ajudar?</h2>
				<div class="faq-answer" rel="o-coaching-pode-realmente-me-ajudar" style="display: block;">
					Esta é uma pergunta bem comum, feita por vários alunos. Na prática, muitos alunos são aprovados em concursos públicos sem precisar de qualquer ajuda de Coaching. A maioria, no entanto, perde um tempo excessivo até encontrar a melhor forma de estudar para o seu objetivo. E esta é apenas uma das grandes vantagens de contratar o serviço de Coaching.
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
    <div class="coaching2-titulo-sem-borda">
    	<div><img class="coaching2-titulo-img" src="/wp-content/themes/academy/images/coaching2-turma.png"></div>
        <div class="coaching2-titulo-texto">Turma de Coaching</div>
    </div>
</div>

<div class="row">
    texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto 
</div>

<div class="row coaching2-cursos">
	<?php get_concursos_coaching() ?>
</div>

<?php get_vimeo_player_assets() ?>

<?php get_footer(); ?>