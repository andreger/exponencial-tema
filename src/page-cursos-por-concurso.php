<?php
get_header();

KLoader::model('ConcursoModel');
KLoader::helper('UrlHelper');

global $post;

$items = ConcursoModel::listar_concursos_com_cursos();

foreach ($items as &$item) {
	$item->url = UrlHelper::get_cursos_por_concurso_especifico_url($item->con_slug);
	$item->qtde_cursos = $item->con_qtde_cursos;
	$item->titulo = $item->post_title;
}

$data['items'] = $items;
$data['placeholder'] = "Procure pelo nome do concurso";

KLoader::view('cursos/por-materia-concurso', $data);