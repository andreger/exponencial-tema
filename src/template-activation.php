<?php
/*
 Template Name: Activation
*/

get_header();
?>

<?php

		$user_id = filter_input( INPUT_GET, 'user', FILTER_VALIDATE_INT, array( 'options' => array( 'min_range' => 1 ) ) );
		if ( $user_id ) {
			
			$code = get_user_meta( $user_id, 'has_to_be_activated', true );
				if ( $code == filter_input( INPUT_GET, 'key' ) ) {
					
					
					delete_user_meta( $user_id, 'has_to_be_activated' );
					?>
					<center>
					<div class="register-success">Parab&eacute;ns! Sua conta foi ativada! Clique no link abaixo para fazer login</div>
					
					<?php 
				}
				else {
					?>
					<center>
					<div class="msg-error">C&oacute;digo de ativa&ccedil;&atilde;o inv&aacute;lido ou expirado!</div>
					
					<?php 
					
				}
			}
			else {
				
				?>
					<center>
					<div class="msg-error">Usu&aacute;rio n&atilde;o encontrado ou expirado!</div>
				
					<?php 

			}


	?>
	<br/>
	</center>
	<p class="return-to-shop">
	<a class="btn u-btn-darkblue" href="/cadastro-login">retornar a tela de login</a></p>
	</br>
	<?php
?>

<?php get_footer(); ?>