<?php

	get_header();
    
    KLoader::model("ProdutoModel");
    KLoader::helper("UrlHelper");
    KLoader::helper("ProfessorHelper");

    $orderby = $_GET['o'];

	$slug = get_query_var('nome');
    
    if(isset($slug) && $slug) {
        global $professor;
    
        if($professor->col_oculto){
            echo "<script>window.location.href='/cursos-por-professor'</script>";
        }else{
    
            $order_param = "";
            if($orderby) {
                
                $order_param = "o=".$orderby."&";
    
                switch ($orderby) {
                    case 'menor-preco': {
                        $order_by = 'pro_preco ASC';
                        break;
                    }
                    case 'maior-preco': {
                        $order_by = 'pro_preco DESC';
                        break;
                    }
                    case 'mais-novos' : {
                        $order_by = 'pro_publicado_em DESC';
                        break;
                    }
                    case 'alfabetica' : {
                        $order_by = 'post_title ASC';
                        break;
                    }
                }
            }
            else {
                $order_by = 'post_title ASC';
            }
    
            $url_base = UrlHelper::get_cursos_por_professor_especifico_url($slug);
            
            $filtro = isset($_GET['curso']) ? $_GET['curso'] : "";
            $filtro_param = "";
            if($filtro){
                $filtro_param = "curso=".$filtro."&";
            }
    
            // Montagem da paginação	
            $pg = isset($_GET['pg']) ? $_GET['pg'] : "1";
            $total = ProdutoModel::contar_por_professor($professor->user_id, $filtro);
            $pattern = $url_base."?{$order_param}{$filtro_param}pg=(:num)";
            $limit = LIMITE_POR_PROFESSOR;
            $paginator = new Paginator($total, $limit, $pg, $pattern);
            $offset = $pg ? ($pg - 1) * $limit : 0;
    
    
            $cursos = ProdutoModel::listar_por_professor($professor->user_id, $filtro, $order_by, $offset, $limit);
            


?>

<div class="container pt-1 pt-md-3 pb-5">
	<div class="row col-12 ml-auto mr-auto">
		<div class="col-12 col-md-6 text-center text-md-left">
			<?= ProfessorHelper::get_avatar_box($professor->user_id) ?>
		</div>
		<div class="col-12 col-md-6 mt-3 mt-md-0">
			<form class="woocommerce-ordering" method="get">
				<div>
					<select id="orderby-autor" name="o" class="ml-auto form-control col-md-10 col-lg-8 orderby">
						<option value="alfabetica" <?= (!isset($orderby) || $orderby == 'alfabetica') ? "selected='selected'" : "" ?>>Ordem alfabética</option>
						<option value="mais-novos" <?= $orderby == 'mais-novos' ? "selected='selected'" : "" ?>>Ordenar por mais novos</option>
						<option value="menor-preco" <?= $orderby == 'menor-preco' ? "selected='selected'" : "" ?> >Ordenar por preço: menor para maior</option>
						<option value="maior-preco" <?= $orderby == 'maior-preco' ? "selected='selected'" : "" ?>>Ordenar por preço: maior para menor</option>	
					</select>
				</div>
				<?php if($filtro): ?>
					<input type="hidden" name="curso" value="<?= $filtro ?>" />
				<?php endif; ?>
			</form>
		</div>
	</div>
	    
		
	<div class="container">	
		<div class="pt-3 p-l-r col-md-6 offset-md-3">
			<div class="row mb-3 mt-3">
				<div class="col p-0 text-right">
				<input class="pesquisa-barra form-control input-pesquisa" type="text" placeholder="Procure pelo nome do curso" value="<?= $filtro ?>" /></div>
				<div class="w-b-l p-0 text-left">
				<a class="link-pesquisa" href="#">
					<div class="pesquisa-barra-lupa">
						<i class="fa fa-search mt-2 ml-2 text-white"></i>		
					</div>
				</a>
				</div>
			</div>    	
    	</div>
    	<div class="pt-4 d-none d-md-block col-12 text-right pr-0">
			<a class="pointer" id="pesquisa-linha-3"><img width="50" src="<?= get_tema_image_url('view-linha.png') ?>"></a>
			<a class="pointer" id="pesquisa-grid-3"><img  width="50" src="<?= get_tema_image_url('view-grid.png') ?>"></a>
		</div>
    
	</div>
	
	<?php if($cursos) : ?>

	<div class="row pt-5">
		<?= $paginator ?>
	</div>

    <div class="container">
		<?= get_concursos_produtos_celulas($cursos, 'nao-pacote'); ?>
	</div>

	<div class="row pb-5">
		<?= $paginator ?>
	</div>
	<?php else: ?>
	<div class="container">Não foram encontrados cursos</div>
	<?php endif; ?>
</div>

<script>
	jQuery(".link-pesquisa").click(function(e){
		var valor = jQuery('.input-pesquisa').val();		
		window.location.replace("<?= $url_base . ($orderby?"?o={$orderby}&":"?") ?>curso="+valor);
	});
	jQuery('.input-pesquisa').keyup(function(e){
		if(e.keyCode == 13)
		{	
			window.location.replace("<?= $url_base . ($orderby?"?o={$orderby}&":"?") ?>curso="+this.value);
		}
	});
</script>

<?php
  }
}else{
    echo "<script>window.location.href='/cursos-por-professor'</script>";
}
?>

<?php get_footer();?>