<?php 
include_once $_SERVER['DOCUMENT_ROOT'] .'/wp-load.php';

KLoader::model("MailchimpModel");

// recupera id da lista
$lista = $_GET['list_id'];

// cria a representação do contato
$membro = [
	'email' => $_POST['EMAIL'],
	'nome' => $_POST['NOME'],
	'telefone' => $_POST['TELEFONE'],
];

// cria um array para armazenar segmentos
$segmentos_a = [];

// cria um array para armazenar áreas de interesses
$interests_a = [];

// se já existe atualiza o seguimento do contato
if($mc_contato = MailChimpModel::get_contato($lista, $membro['email'])) {

	// alimenta array de segmentos com os já existentes
	if($segs = $mc_contato->merge_fields->SEG) {
		$segmentos_a = explode(",", $segs);
	};
	
	if($interests = $mc_contato->interests) {
	    
	    // converte stdclass para array
	    $interests = get_object_vars($interests);
	    foreach ($interests as $key => $value) {
	        
	        if($value) {
	            $interests_a[$key] = true;
	        }
	        else {
	            $interests_a[$key] = false;
	        }
	    }
	}
}

// se novos segmentos forem passados adiciona-os no array
if($_POST['SEG']) {

	if($post_segs = explode(",", $_POST['SEG'])) {
		foreach ($post_segs as $item) {
			array_push($segmentos_a, trim($item));
		}
	}
}

// se novas áreas forem passadas adiciona-as no array
if($area = $_POST['area'])  {
    foreach ($area as $key => $value) {
        $interests_a[$value] = true;
    }
}

// transforma array em string para gravação no MC
$segmentos_a = array_unique($segmentos_a);
$membro['segmento'] = implode(",", $segmentos_a);

$data = [
    'email_address' => $membro['email'],
    'status_if_new' => "subscribed",
    'status' => "subscribed",
    'merge_fields' => [
        'FNAME' => $membro['nome'],
        'SEG' => $membro['segmento'],
        'PHONE' => $membro['telefone']
    ],
];

if($interests_a) {
    $data['interests'] = $interests_a;
}

// salva ou atualiza o contato
MailChimpModel::salvar_contato($lista, $data);

// redireciona para página de agradecimento
header("Location: /agradecimento");