<?php
KLoader::model("LandingPageModel");
KLoader::helper("UrlHelper");

$slug = get_query_var('nome');

$posts = get_posts(array('name' => $slug, 'post_type' => 'landing_pages'));

if($posts) {
	echo $posts[0]->post_content;
}
else 
{
    $historico = LandingPageModel::get_by_slug_historico($slug);

    if($historico) {
        wp_redirect(UrlHelper::get_landing_page_url($historico->post_name), 301);
        exit;
    }
    else {
        redirecionar_erro_404();
        exit;
    }
}