<?php
/*
Template Name: Template METODOLOGIA
*/
get_header();
?><head>
<link href="style.css" rel="stylesheet"/>
</head>
<div class="section_content meta_head">
	<div class="row">
        <div class="sectionlargegap"></div>
        <div class="sectionlargegap"></div>
        <div class="sectiongap"></div>
   	</div>
</div>

<section id="online_courses">
    
<div class="section_head">
        <div class="row border border-danger">
            <div class="section_head_img">
                <img title="" alt="" src=" <?php echo get_template_directory_uri(); ?>/images/icon-nossametodologia.png" width="" height="" />
            </div>
            <div class="header_text ml-5">
                Nossa Metodologia - Cursos on-line
            </div>
        </div>
    </div>
</section>

<div class="section_content">
    <div class="row">
        <div class="sectiongap"></div>
            <div class="sixcol fleft">
            	<div class="elevencol m_auto m_bot_20">
                    <div class="meta_icon fleft">
                        <img title="" alt="" src=" <?php echo get_template_directory_uri(); ?>/images/esquematizacao.png" width="" height="" />
                    </div>
                    <div class="meta_icon_diss fright">
                    Esquematização, mapas mentais e teoria estruturada.
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="elevencol m_auto m_bot_20">
                    <div class="meta_icon fleft">
                        <img title="" alt="" src=" <?php echo get_template_directory_uri(); ?>/images/tabelas.png" width="" height="" />
                    </div>
                    <div class="meta_icon_diss fright">
                     	Tabelas comparativas, gráficos e fluxogramas 
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="elevencol m_auto m_bot_20">
                    <div class="meta_icon fleft">
                        <img title="" alt="" src=" <?php echo get_template_directory_uri(); ?>/images/feedback.png" width="" height="" />
                    </div>
                    <div class="meta_icon_diss fright">
                    	Questões comentadas, dúvidas tiradas direto com o professor.
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="elevencol m_auto m_bot_20">
                    <div class="meta_icon fleft">
                        <img title="" alt="" src=" <?php echo get_template_directory_uri(); ?>/images/completo.png" width="" height="" />
                    </div>
                    <div class="meta_icon_diss fright">
                    	Completo, objetivo e focado.
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="sixcol fright">
            	<div class="banner_area">
                	<span class="metabanner_text fleft">
                        <b>Economize<br/>
                        seu tempo</b>
                        Aumente<br/>
                        EXPONENCIALMENTE<br/>
                        suas chances de <br/>
                        passar em um <br/>
                        concurso público. 
                    </span>
                	<a href="<?php echo SITE_URL; ?>cadastro-login" class="fleft meta_bt"><img title="" alt="" src=" <?php echo get_template_directory_uri(); ?>/images/metodologia_bt.png" width="" height="" /></a>
                </div>
            
            </div>
        <div class="clear"></div>
        <div class="sectiongap"></div>
    </div>
</div>



<section id="online_courses">
    <div class="section_head">
        <div class="row">
            <div class="section_head_img">
                <img title="" alt="" src=" <?php echo get_template_directory_uri(); ?>/images/icon-mais informaçoes.png" width="" height="" />
            </div>
            <div class="header_text">
                Mais informações
            </div>
        </div>
    </div>
</section>


<div class="section_content metabg">
    <div class="row">
    	<div class="sectionlargegap"></div>
	    	<div class="elevencol fleft metabox">
            	<h1>Vantagens </h1>
                <span>
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna 
                    aliquam erat volutpat. Sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. 
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna 
                    aliquam erat volutpat. Sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. 
                </span>
            </div>
            <div class="elevencol fleft metabox">
            	<h1>Sobre a metodologia </h1>
                <span>
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna 
                    aliquam erat volutpat. Sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. 
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna 
                    aliquam erat volutpat. Sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. 
                    
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna 
                    aliquam erat volutpat. Sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. 
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna 
                    aliquam erat volutpat. Sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                </span>
            </div>
            <div class="elevencol fleft metabox">
            	<h1>Público indicado </h1>
                <span>
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna 
                    aliquam erat volutpat. Sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. 
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna 
                    aliquam erat volutpat. Sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. 
                </span>
            </div>
            
    	<div class="sectionglargeap"></div>
    	<div class="clear"></div>
    </div>
</div>

<?php get_footer(); ?>