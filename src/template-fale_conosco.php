<?php
/*
Template Name: Fale conosco
*/

session_start();

get_header(); 

KLoader::helper("FaleConoscoHelper");

$erro_validacao = "";

if(isset($_POST['submit_primario'])) {
    $erro_validacao = FaleConoscoHelper::erro_validacao_primaria();
	
    if(!$erro_validacao) {
       $_SESSION['faleconosco_post'] = $_POST;
       $_SESSION['faleconosco_imagens'] = FaleConoscoHelper::upload_embed_images();

        KLoader::view("fale_conosco/formulario_secundario");
    }
    else {
        KLoader::view("fale_conosco/formulario_primario", ['erro_validacao' => $erro_validacao]);
    }
}
elseif(isset($_POST['submit_secundario'])) {
    
    $erro_validacao_sec = FaleConoscoHelper::erro_validacao_secundaria();
    
    if(!$erro_validacao_sec) {
       FaleConoscoHelper::enviar_email();
    }
    
    KLoader::view("fale_conosco/formulario_secundario", ['erro_validacao_sec' => $erro_validacao_sec]);
    
}
else {
    KLoader::view("fale_conosco/formulario_primario");
}

KLoader::view("fale_conosco/footer");

get_footer();