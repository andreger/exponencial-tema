<?php
date_default_timezone_set('America/Sao_Paulo');

KLoader::helper("AcessoGrupoHelper");
KLoader::helper("RelatorioVendasHelper");
KLoader::model("ProfessorModel");

AcessoGrupoHelper::relatorio_de_vendas(ACESSO_NEGADO);

session_start();

require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-content/Spout/Autoloader/autoload.php';

ini_set('memory_limit', '-1');
set_time_limit(600);

carregar_remodal();

global $wpdb;

$msg = 'Exibindo resultados entre ' . date('d/m/Y', strtotime(date('Y-m-1'))) . ' e ' . date('d/m/Y');

//$query = "SELECT ID,display_name,user_nicename FROM wp_users u LEFT JOIN wp_usermeta um ON um.user_id = u.ID " . "WHERE um.meta_key LIKE 'oneTarek_author_type' AND (um.meta_value = '1' OR um.meta_value = '3') ORDER BY display_name";
//$result = $wpdb->get_results ( $query );

$professores = ProfessorModel::listar_professores_com_cursos();

$options = array ();
foreach ( $professores as $professor ) {
	$options [$professor->ID] = $professor->display_name;
}

if(isset($_POST['submit'])) { 
	
	if(trim($_POST['start_date'])) {

		if(has_error_dates( $_POST['start_date'],  $_POST['end_date'])) {
			$msg = null;
		} else {
			$start_date = trim($_POST['start_date']);

			if(trim($_POST['end_date'])) {
				$end_date = trim($_POST['end_date']);
				$msg = 'Exibindo resultados entre ' . $start_date . ' e ' . $end_date;

			} else {
				$msg = 'Exibindo resultados entre ' . $start_date . ' e ' . date('d/m/Y');
			}
		
		}
	}
}

// Inicializa a sessão
if(!$_SESSION['form_relatorio_vendas']) {
    $_SESSION['form_relatorio_vendas'] = [
        'start_date' => date('01/m/Y'),
        'end_date' => date('d/m/Y'),
        'professor_id' => null,
        'meus_produtos' => null,
        'pedido_id' => null,
        'nome_aluno' => null,
        'email_aluno' => null,
        'exibir_gratis' => null,
        'exibir_apenas_descontos' => null,
        'exibir_apenas_estornos' => null
    ];
}

// Alimenta a sessão
if (isset($_POST['submit']) || isset($_POST['export-excel'])) {
    
    $_SESSION['form_relatorio_vendas'] = [
        'start_date' => isset($_POST['start_date']) ? $_POST['start_date'] : null,
        'end_date' => isset($_POST['end_date']) ? $_POST['end_date'] : null,
        'professor_id' => isset($_POST['professor_id']) ? $_POST['professor_id'] : null,
        'meus_produtos' => $_POST['meus_produtos'] ?: null,
        'pedido_id' => $_POST['pedido_id'] ?: null,
        'email_aluno' => $_POST['email_aluno'] ?: null,
        'nome_aluno' => $_POST['nome_aluno'] ?: null,
        'exibir_gratis' => $_POST['exibir_gratis'] ?: null,
        'exibir_apenas_descontos' => $_POST['exibir_apenas_descontos'] ?: null,
        'exibir_apenas_estornos' => $_POST['exibir_apenas_estornos'] ?: null,
    ];
}

// Monta dados para chamadas de modelo
$p = isset($_GET['pg']) ? $_GET['pg'] : "1";
$offset = ($p - 1) * RELATORIO_VENDAS_POR_PAGINA;

$filtros = [
    'start_date' => converter_para_yyyymmdd($_SESSION['form_relatorio_vendas']['start_date']),
    'end_date' => converter_para_yyyymmdd($_SESSION['form_relatorio_vendas']['end_date']),
    'professor_id' => $_SESSION['form_relatorio_vendas']['professor_id'],
    'meus_produtos' => $_SESSION['form_relatorio_vendas']['meus_produtos'],
    'pedido_id' => $_SESSION['form_relatorio_vendas']['pedido_id'],
    'nome_aluno' => $_SESSION['form_relatorio_vendas']['nome_aluno'],
    'email_aluno' => $_SESSION['form_relatorio_vendas']['email_aluno'],
    'exibir_gratis' => $_SESSION['form_relatorio_vendas']['exibir_gratis'],
    'exibir_apenas_descontos' => $_SESSION['form_relatorio_vendas']['exibir_apenas_descontos'],
    'exibir_apenas_estornos' => $_SESSION['form_relatorio_vendas']['exibir_apenas_estornos'],
    'limit' => RELATORIO_VENDAS_POR_PAGINA,
    'offset' => $offset,
    'user_id' => get_current_user_id()
];

$tabela = RelatorioVendasHelper::get_table($filtros);
$total = RelatorioVendasModel::get_dados($filtros, true);

$pattern = "/relatorio-de-vendas?&pg=(:num)";
$paginator = new Paginator($total, RELATORIO_VENDAS_POR_PAGINA, $p, $pattern);

if(isset($_POST['export-excel'])) {
    $filtros['limit'] = null;
    $lines = RelatorioVendasModel::get_dados($filtros);
	RelatorioVendasHelper::exportar_xls($lines);
}
else {
	get_header();
}

?>

<div class="container-fluid pt-1 pt-md-5">
	<div class="mb-5 col-12 text-center text-md-left text-blue">
		<h1>Relatório de vendas</h1>
	</div>
    <div class="container">	
        <form id="relatorio_form" action="/relatorio-de-vendas" method="post">
        	<div class="row">
        		<div class="col-4">
        			<strong>Data Início:</strong>
        			<input type="text" class="form-control date-picker" id="start_date" name="start_date" value="<?= $_SESSION['form_relatorio_vendas']['start_date'] ? $_SESSION['form_relatorio_vendas']['start_date'] : '' ?>" />
        		</div>

        		<div class="col-4">
        			<strong>Data Fim:</strong>
        			<input type="text" class="form-control date-picker" id="end_date" name="end_date" value="<?= $_SESSION['form_relatorio_vendas']['end_date'] ? $_SESSION['form_relatorio_vendas']['end_date'] : '' ?>" />
        		</div>		
        		<div class="col-4">
        		<?php if(tem_acesso([ADMINISTRADOR])) : ?>
        			<strong>Professor:</strong>
        			<select class="form-control" name="professor_id">
        				<option value="">Selecione o professor</option>				
        			<?php foreach ($options as $key => $value) : ?>
        				<?php $selecionado = (isset($_SESSION['form_relatorio_vendas']['professor_id']) && ($_SESSION['form_relatorio_vendas']['professor_id'] == $key)) ? 'selected=selected' : '' ?>
        				<option value="<?php echo $key;?>" <?php echo $selecionado ?>><?php echo $value; ?></option>
        			<?php endforeach; ?>
        			</select>
        		<?php endif ?>
        		</div>
        		
        	</div>
        	
        	<div class="row mt-4">
        		<div class="col-4">
        			<strong>Id do Pedido:</strong>
        			<input type="text" class="form-control" id="pedido_id" name="pedido_id" value="<?= $_SESSION['form_relatorio_vendas']['pedido_id'] ? $_SESSION['form_relatorio_vendas']['pedido_id'] : '' ?>" />
        		</div>
        		
        		<div class="col-4">
        			<strong>Nome do Aluno:</strong>
        			<input type="text" class="form-control" id="" name="nome_aluno" value="<?= $_SESSION['form_relatorio_vendas']['nome_aluno'] ? $_SESSION['form_relatorio_vendas']['nome_aluno'] : '' ?>" />
        		</div>
        		
        		<div class="col-4">
        			<strong>E-mail do Aluno:</strong>
        			<input type="text" class="form-control" id="" name="email_aluno" value="<?= $_SESSION['form_relatorio_vendas']['email_aluno'] ? $_SESSION['form_relatorio_vendas']['email_aluno'] : '' ?>" />
        		</div>
        	</div>
        
        	<div class="row mt-4">	
            	<div class="col-12">
            		<input type="checkbox" name="exibir_gratis" id="exibir_gratis" value="1" <?= $_SESSION['form_relatorio_vendas']['exibir_gratis'] ? "checked=checked" : "" ?>> 
            		<label for="exibir_gratis"> &nbsp;Exibir compras gratuitas</label>
            	</div>	
            	<div class="col-12">
            		<input type="checkbox" name="exibir_apenas_descontos" id="exibir_apenas_descontos" value="1" <?= $_SESSION['form_relatorio_vendas']['exibir_apenas_descontos'] ? "checked=checked" : "" ?>> 
            		<label for="exibir_apenas_descontos"> &nbsp;Exibir apenas pedidos com desconto</label>
            	</div>
            	<div class="col-12">
            		<input type="checkbox" name="exibir_apenas_estornos" id="exibir_apenas_estornos" value="1" <?= $_SESSION['form_relatorio_vendas']['exibir_apenas_estornos'] ? "checked=checked" : "" ?>>
            		<label for="exibir_apenas_estornos"> &nbsp;Exibir apenas pedidos com estorno</label>
            	</div>
            	
            	<?php if(tem_acesso([COORDENADOR])) : ?>
            	<div class="col-12">
            		<input type="checkbox" id="meus_produtos" name="meus_produtos" value="1" <?= $_SESSION['form_relatorio_vendas']['meus_produtos'] ? 'checked' : '' ?>>
            		<label for="meus_produtos"> &nbsp;Meus Produtos</label>
            	</div>
            	<?php endif; ?>
        	</div>
        	
        	<div class="row mt-4">	
        		<div class="col-12">
        			<input class="btn u-btn-blue" id="submit_form" type="submit" value="Filtrar Consulta" name="submit"/> 
        			<input class="btn u-btn-blue" type="button" value="Exportar para Excel" id="exportar-xls" />
        		</div>
        	</div>	
        </form>

        <div class="row mt-3">
        	<a id="mes_anterior" href="#"><span class="btn-relatorio-mes">Mês Anterior</span></a> |
        	<a id="mes_passado" href="#"><span class="btn-relatorio-mes">Mês Passado</span></a> |
        	<a id="mes_atual" href="#"><span class="btn-relatorio-mes">Mês Atual</span></a>
        </div>

        <div class="row mt-3">
        	<?php echo is_null($msg) ? "Período inválido" : $msg; ?>
        </div>
	</div>
    <div class="mt-3">
    	<?php if(!is_null($msg)) echo $tabela; ?>
    </div>
     <div class="ml-auto mr-auto row mt-5 mb-3">
        <?= $paginator ?>
    </div>
</div><!-- #content -->

<div class="sectionlargegap"></div>

<div class="remodal" data-remodal-id="rv-modal">
    <button data-remodal-action="close" class="remodal-close"></button>    
    
    <div class="rv-carregando contato form_outter" style="display: none">
        <div class="mensagem-panel">
    	   <div style="padding: 10px"><img width="80" src='<?= CARREGANDO_IMG ?>'></div>
    	   <div style="padding: 10px">Aguarde só mais um momento...</div>
    	   <div style="padding: 10px">Estamos agendando seu download!</div>
        </div>
    </div>

    <div class="rv-sucesso contato form_outter" style="display: none">
        <div class="mensagem-panel">
            <img src="<?= get_tema_image_url("confirm-grande.png") ?>" width="150">
            <div class="p-5 font-20">
            	Seu download foi agendado com sucesso.
            </div>
            <button data-remodal-action="cancel" class="btn btn-primary">Fechar</button>
        </div>
    </div>
</div>

<?= get_date_picker(".date-picker") ?>

<script src="/wp-content/themes/academy/js/jquery.maskedinput.min.js"></script>
<script>
function mes_atual() {
	jQuery('#start_date').css('color','#fff');
	jQuery('#end_date').css('color','#fff');
	jQuery('#start_date').val('<?php echo date("d/m/Y", strtotime("first day of this month")) ?>');
	jQuery('#end_date').val('<?php echo date("d/m/Y", strtotime("last day of this month")) ?>');
	jQuery('#submit_form').click();
}

function mes_passado() {
	jQuery('#start_date').css('color','#fff');
	jQuery('#end_date').css('color','#fff');
	jQuery('#start_date').val('<?php echo date("01/m/Y", strtotime("first day of previous month")) ?>');
	jQuery('#end_date').val('<?php echo date("t/m/Y", strtotime("last day of previous month")) ?>');
	jQuery('#submit_form').click();
}

function mes_anterior() {
	jQuery('#start_date').css('color','#fff');
	jQuery('#end_date').css('color','#fff');
	jQuery('#start_date').val('<?php echo date("01/m/Y", strtotime("-2 months")) ?>');
	jQuery('#end_date').val('<?php echo date("t/m/Y", strtotime("-2 months")) ?>');
	jQuery('#submit_form').click();
}


jQuery().ready(function() {
	// field masks
	 jQuery(".input_date").mask("99/99/9999");

	 jQuery("#mes_anterior").click(function() {
		mes_anterior();
	 });
		
	 jQuery("#mes_passado").click(function() {
		mes_passado();
	 });

	 jQuery("#mes_atual").click(function() {
		mes_atual();
	 });

	 jQuery("#meus_produtos").click(function() {
	 	jQuery('#submit_form').click();
	 });

	 jQuery("#exportar-xls").click(function() {
		jQuery(".rv-sucesso").hide();
 	 	jQuery(".rv-carregando").show();

 	 	var inst = jQuery('[data-remodal-id=rv-modal]').remodal();
	 	inst.open();

		var filtros = jQuery("#relatorio_form").serialize();

		jQuery.post("/wp-content/themes/academy/ajax/adicionar_download.php?relatorio_id=1", jQuery("#relatorio_form").serialize(), function (data) {
			jQuery(".rv-carregando").hide();
			jQuery(".rv-sucesso").show();
		});
	 });

});
</script>
<?php get_footer(); ?>  
