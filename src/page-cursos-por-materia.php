<?php
get_header();

KLoader::model('MateriaModel');
KLoader::helper('UrlHelper');

global $post;

$items = MateriaModel::listar_materias_com_cursos();
foreach ($items as &$item) {
	$item->url = UrlHelper::get_cursos_por_materia_especifica_url($item->mat_slug);
	$item->qtde_cursos = $item->mat_qtde_cursos;
	$item->titulo = $item->name;
}

$data['items'] = $items;
$data['placeholder'] = "Procure pelo nome da matéria";

KLoader::view('cursos/por-materia-concurso', $data);