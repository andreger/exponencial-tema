<?php
date_default_timezone_set('America/Sao_Paulo');

tem_acesso(array(
	ADMINISTRADOR, COORDENADOR
), ACESSO_NEGADO);

ini_set('memory_limit', '-1');
set_time_limit(600);

function get_table($suplines, $excel = false) {
	$table = "<table style='border-top:0; width:100%;' border='0' cellspacing='0' cellpadding='0' class='reporttab concursotab'>".
				"<tr>".
					"<th>Área</th>".
					"<th>Coordenador</th>".
					"<th>Qtde Alunos</th>";
	
	$table .= "<th>Total Vendido</th>";
	$table .= "<th>Total Descontos</th>";
	$table .= "<th class='relatorio-destaque'>Total Pago</th>";
	$table .= "<th>% do Total</th>";
	$table .= "<th>Total PagSeguro</th>";
	$table .= "<th>Total Imposto</th>";
	$table .= "<th>Total Líquido</th>";
	$table .= "</tr>";
	
	$soma_alunos = 0;
	$soma_valor_de_venda = 0;
	$soma_desconto = 0;
	$soma_valor_pago = 0;
	$soma_valor_imposto = 0;
	$soma_pag_seguro = 0;
	$soma_liquido_venda = 0;
	
	$total_pago = get_total_pago($suplines);

	$i = 1;
	$j = 1;
	
	foreach ($suplines as $key => $supline) {
		$valor_pago = $supline['total'] - $supline['discount'];
		$valor_imposto = $supline['valor_imposto'];
		$pagseguro = $supline['pagseguro'];
		$liquido = $supline['liquido'];
		$percentual = $total_pago ? $valor_pago / $total_pago * 100 : 0;
						
		$link = $excel ? $key :
					"<a href='#' onclick='event.preventDefault();togglei($i)'>$key</a>";
		
		$table .= "<tr>";
		$table .= "<td>" . $link . "</td>";
		$table .= "<td>" . $supline['coordenador'] . "</td>";
		$table .= "<td>" . $supline['qtde_alunos'] . "</td>";
		$table .= "<td>" . format_moeda($supline['total']) . "</td>";
		$table .= "<td>" . format_moeda($supline['discount']) . "</td>";
		$table .= "<td class='relatorio-destaque'>" . format_moeda($valor_pago) . "</td>";
		$table .= "<td>" . porcentagem($percentual) . "</td>";
		$table .= "<td>" . format_moeda($pagseguro) . "</td>";
		$table .= "<td>" . format_moeda($valor_imposto) . "</td>";
		$table .= "<td>" . format_moeda($liquido) . "</td>";
		$table .= "</tr>";
		
		$soma_valor_de_venda += $supline['total'];
		$soma_desconto += $supline['discount'];
		$soma_valor_pago += $valor_pago;
		$soma_valor_imposto += $valor_imposto;
		$soma_pag_seguro += $pagseguro;
		$soma_liquido_venda += $liquido;

		$total_pago_2 = get_total_pago($supline['concursos']);

		foreach ($supline['concursos'] as $nome => $line) {

			$valor_pago = $line['total'] - $line['discount'];
			$valor_imposto = $line['valor_imposto'];
			$pagseguro = $line['pagseguro'];
			$liquido = $line['liquido'];
			$percentual = $total_pago_2 ? $valor_pago / $total_pago_2 * 100 : 0;

			$link = $excel ? $nome :
					"<a href='#' onclick='event.preventDefault();togglej($j)'>$nome</a>";

			$table .= "<tr class='tri-$i' style='display:none'>";
			$table .= "<td><span style='margin-left: 15px'>" . $link . "</span></td>";
			$table .= "<td>" . $supline['coordenador'] . "</td>";
			$table .= "<td>" . $line['qtde_alunos'] . "</td>";
			$table .= "<td>" . format_moeda($line['total']) . "</td>";
			$table .= "<td>" . format_moeda($line['discount']) . "</td>";
			$table .= "<td>" . format_moeda($valor_pago) . "</td>";
			$table .= "<td>" . porcentagem($percentual) ."</td>";
			$table .= "<td>" . format_moeda($pagseguro) . "</td>";
			$table .= "<td>" . format_moeda($valor_imposto) . "</td>";
			$table .= "<td>" . format_moeda($liquido) . "</td>";
			$table .= "</tr>";
			
			$soma_alunos += $line['qtde_alunos'];

			$total_pago_3 = get_total_pago($line['cursos']);
			
			foreach ($line['cursos'] as $nome => $subline) {

				$valor_pago = $subline['total'] - $subline['discount'];
				$valor_imposto = $subline['valor_imposto'];
				$pagseguro = $subline['pagseguro'];
				$liquido = $subline['liquido'];
				$percentual = $total_pago_3 ? $valor_pago / $total_pago_3 * 100 : 0;
								
				$table .= "<tr class='trj-$j trip-$i' style='display:none'>";
				$table .= "<td><span style='margin-left: 30px'>" . $nome . "</span></td>";
				$table .= "<td>" . $subline['coordenador'] . "</td>";
				$table .= "<td>" . $subline['qtde_alunos'] . "</td>";
				$table .= "<td>" . format_moeda($subline['total']) . "</td>";
				$table .= "<td>" . format_moeda($subline['discount']) . "</td>";
				$table .= "<td>" . format_moeda($valor_pago) . "</td>";
				$table .= "<td>" . porcentagem($percentual) . "</td>";
				$table .= "<td>" . format_moeda($pagseguro) . "</td>";
				$table .= "<td>" . format_moeda($valor_imposto) . "</td>";
				$table .= "<td>" . format_moeda($liquido) . "</td>";
				$table .= "</tr>";
			}

			$j++;

		}
		
		$i++;
	}
	
	// Adiciona linha somatório total
	$table .= "<tfoot><tr class=''>";
	$table .= "<th></th>";
	$table .= "<th></th>";
	$table .= "<th>" . $soma_alunos . "</th>";
	$table .= "<th>" . format_moeda($soma_valor_de_venda) . "</th>";
	$table .= "<th>" . format_moeda($soma_desconto) . "</th>";
	$table .= "<th class='relatorio-destaque'>" . format_moeda($soma_valor_pago) . "</th>";
	$table .= "<th></th>";
	$table .= "<th>" . format_moeda($soma_pag_seguro) . "</th>";
	$table .= "<th>" . format_moeda($soma_valor_imposto) . "</th>";
	$table .= "<th>" . format_moeda($soma_liquido_venda) . "</th>";
	$table .= "</tr></tfoot>";
	$table .=  "</table>";

	return $table;
}

global $wpdb;
global $current_user;
get_currentuserinfo();
$user_id = get_current_user_id();

$msg = 'Exibindo resultados entre ' . date('d/m/Y', strtotime(date('Y-m-1'))) . ' e ' . date('d/m/Y');

$end_date = date('d/m/Y');
$start_date = date('1/m/Y');
$query_professor = '';

if(isset($_POST)) { 
	
	if(!empty($_POST['start_date'])) {
		
		if(has_error_dates( $_POST['start_date'],  $_POST['end_date'])) {
			$msg = null;
		} else {
			$start_date = $_POST['start_date'];
			
			if(!empty($_POST['end_date'])) {
				$end_date = $_POST['end_date'];
				$msg = 'Exibindo resultados entre ' . $_POST['start_date'] . ' e ' . $_POST['end_date'];
			} else {
				$msg = 'Exibindo resultados entre ' . $_POST['start_date'] . ' e ' . date('d/m/Y');
			}
			
		}
	}	
}

$ordem = isset($_POST['ordem']) ? $_POST['ordem'] : 1;

$query_start_date = date('Y-m-d', strtotime(str_replace('/','-',$start_date)));
$query_end_date = date('Y-m-d', strtotime(str_replace('/','-',$end_date)));
$data_i = date('d/m/Y', strtotime(str_replace('/','-',$start_date)));
$data_f = date('d/m/Y', strtotime(str_replace('/','-',$end_date)));

$coordenador_ids = $_POST['coordenador_ids'] ?: null;
$area_ids = $_POST['area_ids'] ?: null;

$lines = listar_vendas_agrupadas_por_area($query_start_date, $query_end_date, $ordem, 'name', $coordenador_ids, $area_ids);

if(isset($_POST['export-excel'])) { 
	
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="stats.xls"');
	
	$table_result = get_table($lines, true); 
	$table_result = str_replace("<table", "<table style='border-collapse:collapse; border: 1px solid black;'", $table_result);
	$table_result = str_replace("<tr", "<tr style='border: 1px solid black;'", $table_result);
	$table_result = str_replace("<td", "<td style='border: 1px solid black;'", $table_result);
	
	$list = get_html_translation_table(HTML_ENTITIES);
	unset($list['"']);
	unset($list['<']);
	unset($list['>']);
	unset($list['&']);
	
	$search = array_keys($list);
	$values = array_values($list);
	$search = array_map('utf8_encode', $search);
	
	$out =  str_replace($search, $values, $table_result);
	
	echo "<html xmlns:x='urn:schemas-microsoft-com:office:excel'>
		<head>
			<meta http-equiv='Content-Type' content='text/html;charset=UTF-8' />
		    <!--[if gte mso 9]>
		    <xml>
		        <x:ExcelWorkbook>
		            <x:ExcelWorksheets>
		                <x:ExcelWorksheet>
		                    <x:Name>Sheet 1</x:Name>
		                    <x:WorksheetOptions>
		                        <x:Print>
		                            <x:ValidPrinterInfo/>
		                        </x:Print>
		                    </x:WorksheetOptions>
		                </x:ExcelWorksheet>
		            </x:ExcelWorksheets>
		        </x:ExcelWorkbook>
		    </xml>
		    <![endif]-->
		</head>
				
		<body>
		   " . $out . "
		</body></html>";
	
	exit;
} else {
	get_header();
}
?>

<br />
<br />
<script>
jQuery(document).ready(function(){
	jQuery(".reporttab").slideDown("slow");
	
  jQuery("tr:nth-child(odd)").css("background-color","#efefef");
  jQuery("tr:nth-child(even)").css("background-color","#fff");  
});
</script>


<?php  
	$coordenadores = get_coordenadores_combo_options("(Sem coordenador)", -1);
	$areas = get_areas_combo_options();
?>
<form id="relatorio_form" action="/relatorio-de-vendas-por-area" method="post">
	<div class="row">
		<div class="twocol fleft margin-top-10">Período:</div>
		<div class="twocol fleft">
			<input type="text" class="input_field datepicker" id="start_date" name="start_date" value="<?= isset($_POST['start_date']) ? $_POST['start_date'] : '' ?>" />
		</div>
		<div class="onecol fleft text-center margin-top-10">até</div>
		<div class="twocol fleft">
			<input type="text" class="input_field datepicker" id="end_date" name="end_date" value="<?= isset($_POST['end_date']) ? $_POST['end_date'] : '' ?>" />
		</div>
		<div class="twocol fleft">
			<?= get_relatorio_ordenacao_combo($ordem) ?>
		</div>
		<div class="onecol fleft"></div>
		<!--<div class="twocol fleft" style="text-align: right">
			<input id="submit_form" type="submit" value="Filtrar Consulta" name="submit"/>
		</div>-->
	</div>
	<div class="row">
		<div class="twocol fleft margin-top-10">Coordenador:</div>
		<div class="tencol fleft" style="margin-bottom: 10px">
			<select name="coordenador_ids[]" class="select2 input_field" multiple="multiple">
			<?php foreach ($coordenadores as $key => $value): ?>
				<?= $selected = in_array($key, $_POST['coordenador_ids']) ? "selected=selected" : ""; ?>
				<option value="<?= $key ?>" <?= $selected ?>><?= $value ?></option>
			<?php endforeach ?>
			</select>
		</div>
	</div>
	<div class="row">
		<div class="twocol fleft margin-top-10">Área:</div>
		<div class="tencol fleft" style="margin-bottom: 10px">
			<select name="area_ids[]" class="select2 input_field" multiple="multiple">
			<?php foreach ($areas as $key => $value): ?>
				<?= $selected = in_array($key, $_POST['area_ids']) ? "selected=selected" : ""; ?>
				<option value="<?= $key ?>" <?= $selected ?>><?= $value ?></option>
			<?php endforeach ?>
			</select>
		</div>
	</div>
</form>

<div class="row" style="margin-top:10px">

	<form action="/relatorio-de-vendas-por-area" method="post">
		<input type="hidden" name="export-excel" value="1">
		
		<?php if(isset($_POST['start_date'])) : ?>
		<input type="hidden" name="start_date" value="<?= $query_start_date ?>">
		<?php endif; ?>
		
		<?php if(isset($_POST['end_date'])) : ?>
		<input type="hidden" name="end_date" value="<?= $query_end_date ?>">
		<?php endif; ?>

		<?php if(isset($_POST['coordenador_ids'])) : ?>
			<?php foreach ($_POST['coordenador_ids'] as $value) : ?>
				<input type="hidden" name="coordenador_ids[]" value="<?= $value ?>">
			<?php endforeach ?>
		<?php endif; ?>

		<?php if(isset($_POST['area_ids'])) : ?>
			<?php foreach ($_POST['area_ids'] as $value) : ?>
				<input type="hidden" name="area_ids[]" value="<?= $value ?> ">
			<?php endforeach ?>
		<?php endif; ?>

		<input id="submit_form" type="button" value="Filtrar Consulta" name="submit"/>
		<input type="submit" value="Exportar para Excel" name="submit" />
	</form>
</div>

<div class="row" style="margin-top:10px">
	<a id="mes_anterior" href="#">Mês Anterior</a> |
	<a id="mes_passado" href="#">Mês Passado</a> |
	<a id="mes_atual" href="#">Mês Atual</a>
</div>

<div style="display:none">
	<form action="/relatorio-de-vendas" method="post" target="_blank">
		<input type="hidden" id="professor_id" name="professor_id" value="">
		<input type="hidden" id="start_date" name="start_date" value="<?= $data_i ?>">
		<input type="hidden" id="end_date" name="end_date" value="<?= $data_f ?>">		
		<input type="submit" value="submit" name="submit" id="submit-detalhe" />
	</form>
</div>

<div class="row"  style="margin-top:10px">
<?php echo is_null($msg) ? "Período inválido" : $msg; ?>
</div>

<div class="row">
	<div style="margin-top:20px">
		<?php if(!is_null($msg)) echo get_table($lines, false) ?>
	</div>
</div>
</div><!-- #content -->
<div class="sectionlargegap"></div>

<link href="/wp-content/themes/academy/css/select2.min.css" rel="stylesheet" />
<script src="/wp-content/themes/academy/js/select2.min.js"></script>

<script>
function mes_atual() {
	jQuery('#start_date').css('color','#fff');
	jQuery('#end_date').css('color','#fff');
	jQuery('#start_date').val('<?php echo date("d/m/Y", strtotime("first day of this month")) ?>');
	jQuery('#end_date').val('<?php echo date("d/m/Y", strtotime("last day of this month")) ?>');
	jQuery('#submit_form').click();
}

function mes_passado() {
	jQuery('#start_date').css('color','#fff');
	jQuery('#end_date').css('color','#fff');
	jQuery('#start_date').val('<?php echo date("01/m/Y", strtotime("first day of previous month")) ?>');
	jQuery('#end_date').val('<?php echo date("t/m/Y", strtotime("last day of previous month")) ?>');
	jQuery('#submit_form').click();
}

function mes_anterior() {
	jQuery('#start_date').css('color','#fff');
	jQuery('#end_date').css('color','#fff');
	jQuery('#start_date').val('<?php echo date("01/m/Y", strtotime("-2 months")) ?>');
	jQuery('#end_date').val('<?php echo date("t/m/Y", strtotime("-2 months")) ?>');
	jQuery('#submit_form').click();
}

function detalhar_professor($professor_id) {
	jQuery("#professor_id").val($professor_id);
	jQuery("#submit-detalhe").click();
	
}

function togglei($i) {
	jQuery(".tri-"+$i).toggle();

	if(jQuery(".tri-"+$i+":hidden").length > 0) {
		jQuery(".trip-"+$i).hide();		
	}
}

function togglej($i) {
	jQuery(".trj-"+$i).toggle();
}

jQuery().ready(function() {

	jQuery('#submit_form').click(function() {
		jQuery("#relatorio_form").submit();
	})

	jQuery('.datepicker').datepicker({
		dateFormat: "dd/mm/yy",
		language: "pt-BR"
	});

	jQuery("#mes_anterior").click(function() {
		mes_anterior();
	});
		
	jQuery("#mes_passado").click(function() {
		mes_passado();
	});

	jQuery("#mes_atual").click(function() {
		mes_atual();
	});

	jQuery(".select2").select2();

});
</script>
<?php get_footer(); ?>  
