<?php
date_default_timezone_set('America/Sao_Paulo');

tem_acesso(array(
	ADMINISTRADOR, PROFESSOR
), ACESSO_NEGADO);

ini_set('memory_limit', '-1');
set_time_limit(600);

function get_table($lines, $data_i = null, $data_f = null)
{
	$mva = get_minimo_valor_acumulado_por_periodo(converter_para_yyyymmdd($data_i), converter_para_yyyymmdd($data_f));

	$table_head =
		"<table style='overflow-x: scroll' border='0' cellspacing='0' cellpadding='0' class='table table-striped table-bordered'>
			<tr class='font-10 bg-blue text-white'>
				<td>Professor</td>
				<td>Qtde Alunos</td>
				<td>Total Vendido</td>
				<td>Total Descontos</td>
				<td>Total Reembolsado</td>
				<td>Total Pago</td>
				<td>Total PagSeguro</td>
				<td>Total Afiliados</td>
				<td>Total Imposto</td>
				<td>Total Líquido</td>
				<td class='relatorio-destaque'>Total Professor</td>";

	if(!is_null($mva)) {
		$table_head .=
				"<td>Acumulado Não Pago</td>
				<td>Valor Acumulado próximo mês</td>
				<td>Valor a Receber</td>";
	}

	if(tem_acesso([ADMINISTRADOR])) {
		$table_head .= "<td>Total Exponencial</td>";
	}

	$table_head .= "</tr>";

	$soma_alunos = 0;
	$soma_valor_de_venda = 0;
	$soma_desconto = 0;
	$soma_reembolso = 0;
	$soma_valor_pago = 0;
	$soma_valor_imposto = 0;
	$soma_pag_seguro = 0;
	$soma_folha_dirigida = 0;
	$soma_liquido_venda = 0;
	$soma_valor_liquido_professor = 0;

	if(!is_null($mva)) {
		$soma_acumulado_anterior = 0;
		$soma_acumulado = 0;
		$soma_a_receber = 0;
	}

	$soma_exponencial = 0;
	$data_anterior = date('Y-m-01', strtotime(converter_para_yyyymmdd($data_i) . ' -15 days'));

	$table_body = '';

	foreach ($lines as $key => $line) {
// 		$valor_pago = $line['total'] - $line['discount'] - $line['reembolso'];
		$valor_pago = $line['valor_pago'];
		$valor_imposto = $line['valor_imposto'];
		$pagseguro = $line['pagseguro'];
		$folha_dirigida = $line['folha_dirigida'];
		$liquido = $line['liquido'];
		$lucro_professor = $line['lucro_professor'];
		$exponencial = $liquido - $lucro_professor;
		$professor_id = $line['professor_id'];

		if(!is_null($mva)) {
			$acumulado_anterior = get_valor_pagamento_acumulado($professor_id, $data_anterior);
			$lucro_acumulado = $acumulado_anterior + $lucro_professor;
			$acumulado = $lucro_acumulado - $mva < 0 ? $lucro_acumulado : 0;
			$a_receber = $lucro_acumulado - $mva >= 0 ? $lucro_acumulado : 0;
		}


		if(is_null($data_i) || $key == "(Sem professor)") {
			$nome = $key;
		} else {
			$nome = "<a href='#' onclick='detalhar_professor($professor_id)'>" . $key . "</a>";
		}

		$table_body .=
			"<tr>
				<td>" . $nome . "</td>
				<td>" . $line['qtde_alunos'] . "</td>
				<td>" . format_moeda($line['total']) . "</td>
				<td>" . format_moeda($line['discount']) . "</td>
				<td>" . format_moeda($line['reembolso']) . "</td>
				<td>" . format_moeda($valor_pago) . "</td>
				<td>" . format_moeda($pagseguro) . "</td>
				<td>" . format_moeda($folha_dirigida) . "</td>
				<td>" . format_moeda($valor_imposto) . "</td>
				<td>" . format_moeda($liquido) . "</td>
				<td class='relatorio-destaque'>" . format_moeda($lucro_professor) . "</td>";

		if(!is_null($mva)) {
			$table_body .=
				"<td>" . format_moeda($acumulado_anterior) . "</td>
				<td>" . format_moeda($acumulado) . "</td>
				<td>" . format_moeda($a_receber) . "</td>";
		}

		if(tem_acesso([ADMINISTRADOR])) {
			$table_body .= "<td>" . format_moeda($exponencial) . "</td>";
		}

		$table_body .= "</tr>";

		$soma_alunos += $line['qtde_alunos'];
		$soma_valor_de_venda += $line['total'];
		$soma_desconto += $line['discount'];
		$soma_reembolso += $line['reembolso'];
		$soma_valor_pago += $valor_pago;
		$soma_valor_imposto += $valor_imposto;
		$soma_pag_seguro += $pagseguro;
		$soma_folha_dirigida += $folha_dirigida;
		$soma_liquido_venda += $liquido;
		$soma_valor_liquido_professor += $lucro_professor;

		if(!is_null($mva)) {
			$soma_acumulado_anterior += $acumulado_anterior;
			$soma_acumulado += $acumulado;
			$soma_a_receber += $a_receber;
		}

		$soma_exponencial += $exponencial;
	}

	$table_summary = '';

	if(tem_acesso([ADMINISTRADOR])) {
		$table_summary =
			"<tr class='bg-secondary text-white font-11'>
				<td>Resumo</td>
				<td>" . $soma_alunos . "</td>
				<td>" . format_moeda($soma_valor_de_venda) . "</td>
				<td>" . format_moeda($soma_desconto) . "</td>
				<td>" . format_moeda($soma_reembolso) . "</td>
				<td>" . format_moeda($soma_valor_pago) . "</td>
				<td>" . format_moeda($soma_pag_seguro) . "</td>
				<td>" . format_moeda($soma_folha_dirigida) . "</td>
				<td>" . format_moeda($soma_valor_imposto) . "</td>
				<td>" . format_moeda($soma_liquido_venda) . "</td>
				<td class='relatorio-destaque'>" . format_moeda($soma_valor_liquido_professor) . "</td>";

		if(!is_null($mva)) {
			$table_summary .=
				"<td>" . format_moeda($soma_acumulado_anterior) . "</td>
				<td>" . format_moeda($soma_acumulado) . "</td>
				<td>" . format_moeda($soma_a_receber) . "</td>";
		}

		$table_summary .=
				"<td>" . format_moeda($soma_exponencial) . "</td>
			</tr>";
	}

	$table =  $table_head . $table_summary . $table_body . "</table>";

	return $table;
}

global $wpdb;
global $current_user;
get_currentuserinfo();
$user_id = get_current_user_id();

$msg = 'Exibindo resultados entre ' . date('d/m/Y', strtotime(date('Y-m-1'))) . ' e ' . date('d/m/Y');

$end_date = date('d/m/Y');
$start_date = date('1/m/Y');
$query_professor = '';

if(isset($_POST['submit'])) {

	if(!empty($_POST['start_date'])) {

		if(has_error_dates( $_POST['start_date'],  $_POST['end_date'])) {
			$msg = null;
		} else {
			$start_date = $_POST['start_date'];

			if(!empty($_POST['end_date'])) {
				$end_date = $_POST['end_date'];
				$msg = 'Exibindo resultados entre ' . $_POST['start_date'] . ' e ' . $_POST['end_date'];
			} else {
				$mdata_fsg = 'Exibindo resultados entre ' . $_POST['start_date'] . ' e ' . date('d/m/Y');
			}

		}
	}
}

$ordem = isset($_POST['ordem']) ? $_POST['ordem'] : 3;

$query_start_date = date('Y-m-d', strtotime(str_replace('/','-',$start_date)));
$query_end_date = date('Y-m-d', strtotime(str_replace('/','-',$end_date)));
$data_i = date('d/m/Y', strtotime(str_replace('/','-',$start_date)));
$data_f = date('d/m/Y', strtotime(str_replace('/','-',$end_date)));

$visao_professor_id = null;
if(!tem_acesso([ADMINISTRADOR])) {
	$visao_professor_id = get_current_user_id();
}

$lines = listar_vendas_agrupadas_por_professor($query_start_date, $query_end_date, $ordem, $visao_professor_id);

if(isset($_POST['export-excel'])) {
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="stats.xls"');

	$table_result = get_table($lines, $data_i, $data_f);
	$table_result = str_replace("<table", "<table style='border-collapse:collapse; border: 1px solid black;'", $table_result);
	$table_result = str_replace("<tr", "<tr style='border: 1px solid black;'", $table_result);
	$table_result = str_replace("<td", "<td style='border: 1px solid black;'", $table_result);

	$list = get_html_translation_table(HTML_ENTITIES);
	unset($list['"']);
	unset($list['<']);
	unset($list['>']);
	unset($list['&']);

	$search = array_keys($list);
	$values = array_values($list);
	$search = array_map('utf8_encode', $search);

	$out =  str_replace($search, $values, $table_result);

	echo "<html xmlns:x='urn:schemas-microsoft-com:office:excel'>
		<head>
			<meta http-equiv='Content-Type' content='text/html;charset=UTF-8' />
		    <!--[if gte mso 9]>
		    <xml>
		        <x:ExcelWorkbook>
		            <x:ExcelWorksheets>
		                <x:ExcelWorksheet>
		                    <x:Name>Sheet 1</x:Name>
		                    <x:WorksheetOptions>
		                        <x:Print>
		                            <x:ValidPrinterInfo/>
		                        </x:Print>
		                    </x:WorksheetOptions>
		                </x:ExcelWorksheet>
		            </x:ExcelWorksheets>
		        </x:ExcelWorkbook>
		    </xml>
		    <![endif]-->
		</head>

		<body>
		   " . $out . "
		</body></html>";

	exit;
} else {
	get_header();
}
?>

<div class="container-fluid pt-1 pt-md-4">
	<div class="mb-5 col-12 text-center text-md-left text-blue">
		<h1>Relatório de vendas por professor</h1>
	</div>
<form id="relatorio_form" action="/relatorio-de-vendas-por-professor" method="post">
	<div class="container">
	<div class="row">
		<div class="col-1">Período:</div>
		<div class="col">
			<input type="text" class="form-control date-picker" id="start_date" name="start_date" value="<?= isset($_POST['start_date']) ? $_POST['start_date'] : '' ?>" />
		</div>
		<div class="text-center">até</div>
		<div class="col">
			<input type="text" class="form-control date-picker" id="end_date" name="end_date" value="<?= isset($_POST['end_date']) ? $_POST['end_date'] : '' ?>" />
		</div>
		<div class="col">
			<?= get_relatorio_ordenacao_combo($ordem) ?>
		</div>
		<div class="col"></div>
		<div class="col">
			<input class="btn u-btn-blue" id="submit_form" type="submit" value="Filtrar Consulta" name="submit"/>
		</div>
	</div>
</div>
</form>
<div class="container">
<div class="row mt-4">
	<a id="mes_anterior" href="#">Mês Anterior</a> |
	<a id="mes_passado" href="#">Mês Passado</a> |
	<a id="mes_atual" href="#">Mês Atual</a>
</div>

<div class="row mt-3">
	<form action="/relatorio-de-vendas-por-professor" method="post">
		<input class="btn u-btn-blue" type="hidden" name="export-excel" value="1">

		<?php if(isset($_POST['start_date'])) : ?>
		<input class="btn u-btn-blue" type="hidden" name="start_date" value="<?php echo $_POST['start_date'] ?> ">
		<?php endif; ?>

		<?php if(isset($_POST['end_date'])) : ?>
		<input class="btn u-btn-blue" type="hidden" name="end_date" value="<?php echo $_POST['end_date'] ?> ">
		<?php endif; ?>

		<input class="btn u-btn-blue" type="submit" value="Exportar para Excel" name="submit" />
	</form>
</div>

<div style="display:none">
	<form action="/relatorio-de-vendas" method="post" target="_blank">
		<input type="hidden" id="professor_id" name="professor_id" value="">
		<input type="hidden" id="start_date" name="start_date" value="<?= $data_i ?>">
		<input type="hidden" id="end_date" name="end_date" value="<?= $data_f ?>">
		<input type="submit" value="submit" name="submit" id="submit-detalhe" />
	</form>
</div>

<div class="row mt-3">
<?php echo is_null($msg) ? "Período inválido" : $msg; ?>
</div>
</div>
<div class="col-12">
	<div class="mt-3">
		<?php if(!is_null($msg)) echo get_table($lines, $data_i, $data_f) ?>
	</div>
</div>
</div>
</div>
<div class="sectionlargegap"></div>
<?= get_date_picker(".date-picker") ?>
<script src="<?php bloginfo('template_url');?>/src/js/bootstrap.min.js"></script>
<script src="<?php bloginfo('template_url');?>/src/js/bootstrap-datepicker.min.js"></script>

<script>
function mes_atual() {
	jQuery('#start_date').css('color','#fff');
	jQuery('#end_date').css('color','#fff');
	jQuery('#start_date').val('<?php echo date("d/m/Y", strtotime("first day of this month")) ?>');
	jQuery('#end_date').val('<?php echo date("d/m/Y", strtotime("last day of this month")) ?>');
	jQuery('#submit_form').click();
}

function mes_passado() {
	jQuery('#start_date').css('color','#fff');
	jQuery('#end_date').css('color','#fff');
	jQuery('#start_date').val('<?php echo date("01/m/Y", strtotime("first day of previous month")) ?>');
	jQuery('#end_date').val('<?php echo date("t/m/Y", strtotime("last day of previous month")) ?>');
	jQuery('#submit_form').click();
}

function mes_anterior() {
	jQuery('#start_date').css('color','#fff');
	jQuery('#end_date').css('color','#fff');
	jQuery('#start_date').val('<?php echo date("01/m/Y", strtotime("-2 months")) ?>');
	jQuery('#end_date').val('<?php echo date("t/m/Y", strtotime("-2 months")) ?>');
	jQuery('#submit_form').click();
}

function detalhar_professor($professor_id) {
	jQuery("#professor_id").val($professor_id);
	jQuery("#submit-detalhe").click();

}

jQuery().ready(function() {

	jQuery('.datepicker').datepicker({
		dateFormat: "dd/mm/yy"
	});

	 jQuery("#mes_anterior").click(function() {
		mes_anterior();
	 });

	 jQuery("#mes_passado").click(function() {
		mes_passado();
	 });

	 jQuery("#mes_atual").click(function() {
		mes_atual();
	 });

});
</script>
<?php get_footer(); ?>
