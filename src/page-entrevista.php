<?php

KLoader::model('DepoimentoModel');

$slug = get_query_var('nome');

$depoimento = DepoimentoModel::get_by_slug($slug);

if($depoimento){

    $data['depoimento'] = $depoimento;
    $data['titulo'] = "Entrevista";

    KLoader::view('depoimentos/em_texto', $data);
}
else
{
  redirecionar_erro_404();
}
