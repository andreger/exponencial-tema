<?php
KLoader::model("AulaModel");
KLoader::model("PedidoModel");
KLoader::helper("PedidoHelper");
KLoader::helper("PDFHelper");
KLoader::helper("StringHelper");
KLoader::helper("UrlHelper");
KLoader::helper("ZipHelper");

set_time_limit(360);

$produto_id = $_GET['pr'];
$pedido_id = $_GET['o'];

$is_merge = get_post_meta($produto_id, PRODUTO_FUNDIR_ARQUIVOS, true) == YES ? true : false;
$arquivos_zip = [];

foreach([TIPO_AULA_PDF, TIPO_AULA_MAPA, TIPO_AULA_RESUMO] as $tipo_aula)
{

    $aulas = AulaModel::listar_aulas($produto_id, $pedido_id, $tipo_aula, TRUE);

    if($aulas) {

        
        foreach ($aulas as $aula) {
            
            // tratamento para produtos com o merge ativado
            if($is_merge) {
                $filename = $_SERVER["DOCUMENT_ROOT"] . '/wp-content/temp/' . PDFHelper::get_nome_pdf_merge($produto_id, $aula['index'], $tipo_aula);
                
                if($pdf = PDFHelper::merge($aula["arquivos"]))
                {
                    $pdf->Output($filename,'F');
                    $arquivos_zip[] = $filename;
                    log_zip("INFO", "DownloaCurso COM MERGE prd={$produto_id} ped={$pedido_id}: " . implode(", ", $aula['arquivos']));
                }
                else
                {
                    log_zip("INFO", "DownloaCurso COM MERGE prd={$produto_id} ped={$pedido_id}: VAZIO");
                    continue;
                }
            }
            
            // tratamento para produtos sem o merge
            else {
                foreach ($aula["arquivos"] as $arquivo) {
                    if($arquivo)
                    {
                        $arquivos_zip[] = $arquivo;
                        log_zip("INFO", "DownloaCurso SEM MERGE prd={$produto_id} ped={$pedido_id}: {$arquivo}");
                    }
                    else
                    {
                        log_zip("INFO", "DownloaCurso SEM MERGE prd={$produto_id} ped={$pedido_id}: VAZIO");
                    }
                }
            }
        }
        
        PedidoModel::registrar_download_curso($pedido_id, $produto_id, null, $tipo_aula);

    }
}

if(!empty($arquivos_zip))
{
    $post = get_post($produto_id);
    $nome = StringHelper::slugify($post->post_title, 30);
    $zip = $_SERVER["DOCUMENT_ROOT"] . ZipHelper::download_all_pdf_produto_zip($arquivos_zip, $nome);
    
    
    if (file_exists($zip)) {
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename='.basename($zip));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . filesize($zip));
        ob_clean();
        flush();
        readfile($zip);
        exit;
    }
    else {
        echo "Erro ao criar o zip";
    }
}
else {
	echo "Arquivos não disponíveis";
}

