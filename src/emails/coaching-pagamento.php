<html>
	<head>
		<meta http-equiv='Content-Type' content='text/html;UTF-8' />
	</head>
	<body style='margin: 0px; background-color: #F4F3F4; font-family: Helvetica, Arial, sans-serif; font-size:12px;' text='#444444' bgcolor='#F4F3F4' link='#21759B' alink='#21759B' vlink='#21759B' marginheight='0' topmargin='0' marginwidth='0' leftmargin='0'>
		<table width='100%' border='0' cellspacing='0' cellpadding='0' bgcolor='#F4F3F4'>
			<tbody>
				<tr>
					<td style='padding: 15px;'><center>
						<table width='550' cellspacing='0' cellpadding='0' align='center' bgcolor='#ffffff'>
							<tbody>
								<tr>
									<td align='left'>
										<div>
											<img alt='Exponencial Concursos' src='http://www.exponencialconcursos.com.br/wp-content/themes/academy/images/email-novo-post-header.jpg' border='0' />
										</div>
									</td>
								</tr>	
								<tr>
									<td colspan='2' style='padding:20px 0 0 20px;'>
										<font style='font-size:14px;line-height:20px;' face='Arial' color='#373737'>
											<p>Olá, <?php echo $nome ?>!</p>
											<p>Gostaríamos de convidar você a estudar conosco. Esperamos contribuir para seu sucesso nesta dura jornada para a aprovação em concursos.</p>
											<p>Por favor, leia com atenção a página do detalhamento do serviço disponível no link: <a href='https://www.exponencialconcursos.com.br/detalhamento-do-coaching/'>https://www.exponencialconcursos.com.br/detalhamento-do-coaching/</a>
											<p>Aceitando estes termos, você poderá confirmar sua inscrição por meio dos links abaixo, clicando em 'Comprar'.</p>
											<p>Mensal: <a href='<?= $link_mensal ?>'><?= $link_mensal ?></a></p>
											<p>Trimestral com desconto: <a href='<?= $link_trimestral ?>'><?= $link_trimestral ?></a></p>
											<p>Semestral com (ainda mais) desconto: <a href='<?= $link_semestral ?>'><?= $link_semestral ?></a></p>
											<p>Efetue a compra do produto e pague eletronicamente, via transferência bancária, boleto ou cartão de crédito (no cartão de crédito, o valor poderá ser parcelado em até 12x sem juros!).</p>
											<p>Este link é oculto no site, disponível apenas para alunos selecionados para nossas turmas. Por favor, não o divulgue.</p>
											<p>A data de início será considerada o dia <?php echo $data_inicio ?>, consequentemente, os pagamentos dos demais meses devem ser efetuados até o dia <?php echo $dia_inicio ?> do mês.</p>
											<p>Após a confirmação do pagamento, você deverá terminar seu cadastro respondendo algumas perguntas adicionais que lhe enviaremos por e-mail. Em seguida, iremos lhe enviar informações para preparação de sua entrevista online, se possível, a ser realizada o quanto antes.</p>
											<p>Abraços e até breve,</p>
											<p>Equipe Exponencial Concursos</p>
										</font>
									</td>
								</tr>
								<tr>
									<td align='left'>
										<div>
											<img class='alignnone size-full wp-image-4056' alt='cabecalho3' src='http://www.exponencialconcursos.com.br/wp-content/themes/academy/images/email-novo-post-footer.png' border='0' />
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</center></td>
				</tr>
			</tbody>
		</table>
	</body>
</html>
