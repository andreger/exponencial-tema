<html>
	<head>
			
	</head>
	<body bgcolor='#ffffff' text="ffffff" marginheight='0' topmargin='0' marginwidth='0' leftmargin='0'>
		<table width='600' border='0' cellspacing='0' cellpadding='0' align="center">
			<tbody background="http://www.exponencialconcursos.com.br/wp-content/themes/academy/images/emails/foto.jpg">
			<tr>
				<td colspan="2" style="padding-top: 20px;" align="center">
				<a href="https://www.exponencialconcursos.com.br/"> 
					<img alt="Exponencial Concursos" src="http://www.exponencialconcursos.com.br/wp-content/themes/academy/images/emails/logo-expo.png">
				</a>	
				</td>				
			</tr>
			<tr>				
				<td colspan="2" style="padding-top: 20px;" align="center"> 
					Bem vindo ao Exponencial Concursos
				</td>
			</tr>
			<tr>				
				<td colspan="2" style="padding-top: 20px;font-weight: bold;font-size: 20px;" align="center"> 
					QUER ESTUDAR SEM GASTAR NADA?
				</td>
			</tr>
			<tr>				
				<td colspan="2" align="center"> 
					Confira as opções GRATUITAS que o
				</td>
			</tr>
			<tr>				
				<td colspan="2" align="center" style="padding-bottom: 30px;"> 
					Exponencial oferece para você!
				</td>
			</tr>
			<tr >				
				<td width="50%" colspan="1" style="border-right: 1px solid;padding-top: 20px;" align="center"> 
					<img width="60%" alt="Exponencial Concursos" src="http://www.exponencialconcursos.com.br/wp-content/themes/academy/images/emails/sq.png">
					<p style="font-size: 24px;font-weight: bold;">15 dias GRÁTIS</p>
				</td>
				<td width="50%" colspan="1"  align="center" style="padding-top: 20px;"> 
					<img width="35%" alt="Exponencial Concursos" src="http://www.exponencialconcursos.com.br/wp-content/themes/academy/images/emails/simulado.png">
				</td>
			</tr>
			<tr>				
				<td width="50%" colspan="1" align="center" style="border-right: 1px solid;"> 
					<p style="font-size: 13px;padding-top: 10px;">Acesse o Sistema de Questões<br> online agora mesmo!</p>
				</td>
				<td width="50%" colspan="1"  align="center"> 
					<p style="font-size: 13px;padding-top: 10px;">Quer treinar para prova? Faça online<br> ou imprima e treine para o seu grande dia!</p>
				</td>
			</tr>
			<tr>				
				<td width="50%" colspan="1" align="center" style="border-right: 1px solid;"> 
					<a href="https://www.exponencialconcursos.com.br/questoes/main/resolver_questoes">
						<img style="padding-top: 10px;padding-bottom: 10px;" alt="Exponencial Concursos" src="http://www.exponencialconcursos.com.br/wp-content/themes/academy/images/emails/botao-quero.png">
					</a>
				</td>
				<td width="50%" colspan="1"  align="center"> 
					<a href="https://www.exponencialconcursos.com.br/simulados/">
						<img style="padding-top: 10px;padding-bottom: 10px;" alt="Exponencial Concursos" src="http://www.exponencialconcursos.com.br/wp-content/themes/academy/images/emails/botao-quero.png">
					</a>
				</td>
			</tr>
				<tr>				
				<td colspan="2" align="center" style="border-right: 1px solid;padding-top: 20px;padding-bottom: 20px;"> 
					<a href="https://www.exponencialconcursos.com.br/cursos-gratis/">
						<img alt="Exponencial Concursos" src="http://www.exponencialconcursos.com.br/wp-content/themes/academy/images/emails/mais-cursos.png">
					</a>
				</td>			
			</tr>
			</tbody>
			<tbody bgcolor="ffffff">
				<tr>
					<td colspan="2" align="center">
						<p style="text-align: center; color: #005fad; font-weight: bold; font-size: 26px;padding-top: 10px;">SAIBA TUDO SOBRE OS CONCURSOS</p>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center">
						<p style="color: #252525;text-align: center;font-size: 16px;margin-top: 1px;">Confira as notícias astualizadas da sua área de interesse</p>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center" style="padding-top: 30px;">
						<img  alt="Exponencial Concursos" src="http://www.exponencialconcursos.com.br/wp-content/themes/academy/images/emails/fiscal.png">
						<span style="color: #005fad; font-weight: bold; font-size: 22px; position: relative; top: -7px; margin-left: 9px;">FISCAL</span>
						<span style="color: #005fad; position: relative; top: -9px;margin-left: 65px;">RFB, ICMS, ISS, AFT e demais</span>
						<a href="https://www.exponencialconcursos.com.br/concursos-previstos-area-fiscal-aft-rfb-icms-iss/"><img style="margin-left: 20px;" alt="Exponencial Concursos" src="http://www.exponencialconcursos.com.br/wp-content/themes/academy/images/emails/botao-saiba.png"></a>
					</td>
				</tr>
				<tr>	
					<td colspan="2" align="center" style="padding-top: 5px;">						
						<img style="padding-top: 7px" alt="Exponencial Concursos" src="http://www.exponencialconcursos.com.br/wp-content/themes/academy/images/emails/controle.png">
						<span style="color: #005fad; font-weight: bold; font-size: 22px; position: relative; top: -7px; margin-left: 6px;">CONTROLE</span> 
						<span style="color: #005fad; position: relative; top: -9px;margin-left: 15px;" class="">TCU, CGU, TCE, TCM e demais</span>
						<a href="https://www.exponencialconcursos.com.br/concursos-noticias-edital-area-de-controle-tcu-tce-tcm-controladoria"><img style="margin-left: 20px;" alt="Exponencial Concursos" src="http://www.exponencialconcursos.com.br/wp-content/themes/academy/images/emails/botao-saiba.png"></a>
					</td>
				</tr>
				<tr>		
					<td colspan="2" align="center" style="padding-top: 10px;">
						<img style="padding-top: 7px" alt="Exponencial Concursos" src="http://www.exponencialconcursos.com.br/wp-content/themes/academy/images/emails/tribunais.png">
						<span style="color: #005fad; font-weight: bold; font-size: 22px; position: relative; top: -7px; margin-left: 10px;">TRIBUNAIS</span> 
						<span style="color: #005fad; position: relative; top: -9px;margin-left: 75px;" class="">TRT, TRE, TRF e TJ</span>
						<a href="https://www.exponencialconcursos.com.br/concursos-previstos-tribunais-tre-trt-trf-e-tj/"><img style="margin-left: 45px;" alt="Exponencial Concursos" src="http://www.exponencialconcursos.com.br/wp-content/themes/academy/images/emails/botao-saiba.png"></a>
					</td>
				</tr>
				<tr>		
					<td colspan="2" align="center" style="padding-top: 10px;">
						<img alt="Exponencial Concursos" src="http://www.exponencialconcursos.com.br/wp-content/themes/academy/images/emails/policial.png">
						<span style="color: #005fad; font-weight: bold; font-size: 22px; position: relative; top: -7px; margin-left: 9px;">POLICIAL</span>
						<span style="color: #005fad; position: relative; top: -9px;margin-left: 30px;">Policia Federal, PRF, Policia Militar</span>
						<a href="https://www.exponencialconcursos.com.br/carreira-policial-noticias-edital-policia-federal-prf-pm-pc/"><img style="margin-left: 20px;" alt="Exponencial Concursos" src="http://www.exponencialconcursos.com.br/wp-content/themes/academy/images/emails/botao-saiba.png"></a>
					</td>										
				</tr>
				<tr>
					<td colspan="2" style="height: 40px;">
						<span style="color: #005fad; position: relative; top: -20px;margin-left: 42px;padding-left: 163px;padding-bottom: 20px;">Civil, Bombeiro, AGEPEN e demais</span>
					</td>
				</tr>
			</tbody>
			<tbody bgcolor="e6e7e9">
				<tr>
					<td colspan="2" align="center">
						<p style="padding-top: 15px; text-align: center; color: #005fad; font-weight: bold; font-size: 22px;margin-bottom: 1px" class="">NÃO SABE O QUE FAZER, PARA O<br>QUE ESTUDAR OU POR ONDE COMEÇAR?</p>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center">
						<p style="color: #252525;padding-top: 20px;">Te ajudaremos GRATUITAMENTE, fale com um dos nossos coaches no whatsapp: 21 99202-7050.</p>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center" style="padding-top: 20px;">
						<a href="http://www.exponencialconcursos.com.br/fale-conosco/">
							<img style="padding-top: 10px; padding-bottom: 30px;" alt="Exponencial Concursos" src="http://www.exponencialconcursos.com.br/wp-content/themes/academy/images/emails/falar-coach.png" class="">
						</a>
					</td>
				</tr>
			</tbody>
			<tbody>
				<tr>
					<td colspan="2" align="center" style="padding-top: 20px;padding-bottom: 20px;">
					<a href="http://www.exponencialconcursos.com.br/" style="text-decoration: none;">
						<img alt="Exponencial Concursos" src="http://www.exponencialconcursos.com.br/wp-content/themes/academy/images/emails/logo-expo-rodape.png">
					</a>
					<span style="color: #252525;padding-left: 70px;position: relative;top: -6px;">Acompanhe nossas redes sociais: </span>
					<span style="padding-left: 15px;">
					<a href="https://www.facebook.com/exponencialconcursos/" style="text-decoration: none;">
						<img alt="Exponencial Concursos" src="http://www.exponencialconcursos.com.br/wp-content/themes/academy/images/emails/face.png">
					</a>
					<a href="https://www.instagram.com/exponencial_concursos/" style="text-decoration: none;">
						<img alt="Exponencial Concursos" src="http://www.exponencialconcursos.com.br/wp-content/themes/academy/images/emails/insta.png">
					</a>
					<a href="https://www.youtube.com/channel/UCr9rg5WOPmXvZgOfBl-HEuw" style="text-decoration: none;">
						<img alt="Exponencial Concursos" src="http://www.exponencialconcursos.com.br/wp-content/themes/academy/images/emails/yt.png">
					</a>
					</span>
					</td>
				</tr>
			</tbody>
		</table>
	</body>
</html>

