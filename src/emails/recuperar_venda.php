<html>
	<head>
		<meta http-equiv='Content-Type' content='text/html;UTF-8' />
	</head>
	<body style='margin: 0px; background-color: #F4F3F4; font-family: Helvetica, Arial, sans-serif; font-size:12px;' text='#444444' bgcolor='#F4F3F4' link='#21759B' alink='#21759B' vlink='#21759B' marginheight='0' topmargin='0' marginwidth='0' leftmargin='0'>
		<table width='100%' border='0' cellspacing='0' cellpadding='0' bgcolor='#F4F3F4'>
			<tbody>
				<tr>
					<td style='padding: 15px;'><center>
						<table width='550' cellspacing='0' cellpadding='0' align='center' bgcolor='#ffffff'>
							<tbody>
								<tr>
									<td align='left'>
										<div>
											<img alt='Exponencial Concursos' src='http://www.exponencialconcursos.com.br/wp-content/themes/academy/images/email-novo-post-header.jpg' border='0' />
										</div>
									</td>
								</tr>	
								<tr>
									<td colspan='2' style='padding:20px 0 0 20px;'>
										<font style='font-size:14px;line-height:20px;' face='Arial' color='#373737'>
											<p>Olá <?= $nome ?></p>
											<p>Notamos que você fez um pedido em nosso site <?= $cursos ?>. Gostaríamos de lhe perguntar se teve alguma dificuldade para efetuar a compra e se precisa de nossa ajuda.</p>
											<p>Se ainda está na dúvida, veja em nosso site as aulas demonstrativas gratuitas: <a href="http://www.exponencialconcursos.com.br/cursos-online/">http://www.exponencialconcursos.com.br/cursos-online/</a>. Basta clicar no curso desejado e, em seguida, clicar no link 'Aula demonstrativa'. </p>
											<p>Se busca questões online, experimente nosso Sistema de Questões Online e resolva 100 questões por dia sem qualquer custo: <a href="https://www.exponencialconcursos.com.br/questoes/main/resolver_questoes">https://www.exponencialconcursos.com.br/questoes/main/resolver_questoes.</a></p>
											<p>E ainda, se deseja informações sobre Coaching, veja aqui: <a href="https://www.exponencialconcursos.com.br/coaching/">https://www.exponencialconcursos.com.br/coaching/.</a> E fique à vontade para entrar em contato conosco para uma Entrevista Grátis para sanar todas as suas dúvidas.</p>
											<p>Por fim, <b>não deixe de acessar nossos materiais grátis</b>. Veja aqui: <a href="http://www.exponencialconcursos.com.br/cursos-gratis">http://www.exponencialconcursos.com.br/cursos-gratis</a></p>
											<p>Abraços e bons estudos!</p>
										</font>
									</td>
								</tr>
								<tr>
									<td align='left'>
										<div>
											<img class='alignnone size-full wp-image-4056' alt='cabecalho3' src='http://www.exponencialconcursos.com.br/wp-content/themes/academy/images/email-novo-post-footer.png' border='0' />
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</center></td>
				</tr>
			</tbody>
		</table>
	</body>
</html>