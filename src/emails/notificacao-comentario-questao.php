<html>
	<head>
		<meta http-equiv='Content-Type' content='text/html;UTF-8' />
	</head>
	<body style='margin: 0px; background-color: #F4F3F4; font-family: Helvetica, Arial, sans-serif; font-size:12px;' text='#444444' bgcolor='#F4F3F4' link='#21759B' alink='#21759B' vlink='#21759B' marginheight='0' topmargin='0' marginwidth='0' leftmargin='0'>
		<table width='100%' border='0' cellspacing='0' cellpadding='0' bgcolor='#F4F3F4'>
			<tbody>
				<tr>
					<td style='padding: 15px;'><center>
						<table width='550' cellspacing='0' cellpadding='0' align='center' bgcolor='#ffffff'>
							<tbody>
								<tr>
									<td align='left'>
										<div>
											<img alt='Exponencial Concursos' src='http://www.exponencialconcursos.com.br/wp-content/themes/academy/images/email-novo-post-header.jpg' border='0' />
										</div>
									</td>
								</tr>
								<tr>
									<td colspan='2' style='padding:0 0 0 20px;'>
										<font style='font-size:14px;line-height:20px;' face='Arial' color='#373737'>
											<p>Olá, <?= $nome ?></p>
											<p>A questão <b><?= $questao_id . " - " . $disciplina ?></b> que você está acompanhando recebeu novo comentário do usuário 
											<?php if($is_professor): ?>
											<a href="<?= $comentario_usuario_url ?>"><?= $comentario_usuario ?></a>.
											<?php else: ?>
											<b><?php echo $comentario_usuario ?></b>.
											<?php endif; ?> Segue o comentário:</p>
											<blockquote><?= $comentario ?></blockquote>
											<p><a href="<?= $questao_link ?>">Clique aqui</a> para visualizar a questão.</p>
										</font>
									</td>
								</tr>
								<tr>
									<td align='left'>
										<div>
											<img class='alignnone size-full wp-image-4056' alt='cabecalho3' src='http://www.exponencialconcursos.com.br/wp-content/themes/academy/images/email-novo-post-footer.png' border='0' />
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</center></td>
				</tr>
			</tbody>
		</table>
	</body>
</html>