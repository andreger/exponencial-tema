<html>
	<head>
		<meta http-equiv='Content-Type' content='text/html;UTF-8' />
	</head>
	<body style='margin: 0px; background-color: #F4F3F4; font-family: Helvetica, Arial, sans-serif; font-size:12px;' text='#444444' bgcolor='#F4F3F4' link='#21759B' alink='#21759B' vlink='#21759B' marginheight='0' topmargin='0' marginwidth='0' leftmargin='0'>
		<table width='100%' border='0' cellspacing='0' cellpadding='0' bgcolor='#F4F3F4'>
			<tbody>
				<tr>
					<td style='padding: 15px;'><center>
						<table width='550' cellspacing='0' cellpadding='0' align='center' bgcolor='#ffffff'>
							<tbody>
								<tr>
									<td align='left'>
										<div>
											<img alt='Exponencial Concursos' src='https://www.exponencialconcursos.com.br/wp-content/themes/academy/images/email-novo-post-header.jpg' border='0' />
										</div>
									</td>
								</tr>	
								<tr>
									<td colspan='2' style='padding:0 0 0 20px;'>
										<font style='font-size:14px;line-height:20px;' face='Arial' color='#373737'>
											<p>Olá, <?php echo $nome ?>!</p>
											<p>Sua inscrição foi confirmada. É uma grande alegria ter você estudando conosco.</p>
											<p>Por favor, dê continuidade à sua inscrição preenchendo informações detalhadas sobre seu estudo, no link: <a href="https://www.exponencialconcursos.com.br/painel-coaching/inscricao/planilhas_detalhadas">https://www.exponencialconcursos.com.br/painel-coaching/inscricao/planilhas_detalhadas</a></p>
											<p>Neste link, você terá acesso a duas planilhas. E lá você deverá fazer o upload delas para nossa análise (após preenchê-las!). Em seguida, iremos entrar em contato com você para agendar sua entrevista inicial! Provável que demoremos entre 2 e 3 dias para analisarmos suas planilhas e lhe enviar opções para a entrevista, ok?</p>
											<p>Note que uma destas planilhas possui diversas abas: selecione aquela correspondente ao concurso em que foca (sua prioridade).</p>
											<p>Caso tenha qualquer dúvida, fique à vontade para nos contatar.</p>
											<p>Abraços e até breve,</p>
											<p>Equipe Exponencial Concursos</p>
										</font>
									</td>
								</tr>
								<tr>
									<td align='left'>
										<div>
											<img class='alignnone size-full wp-image-4056' alt='cabecalho3' src='https://www.exponencialconcursos.com.br/wp-content/themes/academy/images/email-novo-post-footer.png' border='0' />
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</center></td>
				</tr>
			</tbody>
		</table>
	</body>
</html>
