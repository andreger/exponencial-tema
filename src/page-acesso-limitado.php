<?php get_header(); ?>
<div class="section_content meta_head_produto_expirado">
	<div class="container pt-5">
		<div class="col-12 pt-5">
			<h1 class="text-white">Pedimos desculpas, mas esse produto só pode ser adquirido uma única vez. Veja <a class="t-d-none text-green" href="/cursos-online/">aqui</a> os produtos que temos disponíveis.</h1>
		</div>
	</div>
</div>
<?php get_footer();?>