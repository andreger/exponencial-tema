<?php
KLoader::model("ProdutoModel");

$user_ID = get_current_user_id ();

function converter_pacote_antigo($items)
{
	$nova_lista = array();
	
	foreach ($items as $item) {
		if($item ['product_id'] == 7195) {
			$ids = array(
					6412,
					3545,
					6105,
					3936,
					3542,
					3529,
					3548,
					3537,
					4013,
					3533,
					6756,
					6065,
					4247,
					3791
			);
			foreach ($ids as $id) {
				array_push($nova_lista, array('product_id' => $id));
			}
		}
		else {
			array_push($nova_lista, $item);
		}
	}
	
	return $nova_lista;
}


if(is_area_desativada(array(PAINEL_FORUM))){
	redirecionar_erro_500();
}

redirecionar_se_nao_estiver_logado();
get_header(); 
?>


<div class="container pt-1 pt-md-5">
	<h1>Fórum</h1>
</div>

<br /><br />
<script>
		jQuery(document).ready(function(){
			jQuery(".productab").slideDown("slow");
			
			jQuery("tr:nth-child(odd)").css("background-color","#efefef");
			jQuery("tr:nth-child(even)").css("background-color","#fff");  
		});
		</script>
<div class="container">		
<div>
	<?php


	if (is_professor ( $user_ID )) {

		$hoje = strtotime(hoje_yyyymmdd(). ' 00:00:00');

		$GLOBALS['proxima'] = 1;

		$produtos = ProdutoModel::listar_por_professor($user_ID);
		// $produtos = get_produtos_por_autor ( $user_ID );

		$foruns_ativos = array();
		$foruns_indisponiveis = array();
		foreach($produtos as $produto){
			$disponivel_ate = strtotime(converter_para_yyyymmdd(get_data_disponivel_ate($produto->post_id)). ' 00:00:00');
			if($disponivel_ate < $hoje){
				array_push($foruns_indisponiveis, $produto);
			}else{
				array_push($foruns_ativos, $produto);
			}						
		}

		//Ativos
		if (count ( $foruns_ativos ) > 0) {
			
			?>
			<table class="table-margin table table-striped table-bordered">
		<tr class="bg-blue text-white" align="center">
			<td class="font-weight-bold new_address text-white" align="center" colspan="4">F&oacute;runs que
				voc&ecirc; modera - Cursos Ativos</td>
		</tr>
		<tr>
			<td class="font-weight-bold">F&oacute;rum</td>
			<td class="font-weight-bold text-center">Alunos</td>
			<td class="font-weight-bold text-center">Tópicos Pendentes</td>
			<td class="font-weight-bold text-center">Respostas Pendentes</td>
		</tr>
			<?php
			foreach ( $foruns_ativos as $post ) {
				// setup_postdata ( $post );
				$forum_id = get_post_meta ( $post->post_id, 'forum_id', true );
				if (empty ( $forum_id ))
					continue;
				
				$forum = bbp_get_forum ( $forum_id );
				if (! isset ( $forum ))
					continue;
				
				if (is_forum_expirado ( $forum_id ))
					continue;
				
				if ($forum->post_status == 'hidden')
					continue;
				
				$total_respostas = bbp_get_forum_post_count ( $forum_id );
				?>
		<tr>
			<td class="">
				<?php do_action( 'bbp_theme_before_forum_title' ); ?>
				<a class="bbp-forum-title"
				href="<?php bbp_forum_permalink($forum_id); ?>"><?php bbp_forum_title($forum_id); ?></a>
				<?php do_action( 'bbp_theme_after_forum_title' ); ?>
			</td>

			<td class="text-center">
				<?php echo count(bbp_get_forum_subscribers($forum_id)) ?>
			</td>
			
			<td class="text-center">
				<?php
				$total = contar_topicos_pendentes ( $forum_id );
				echo $total;
				
				if ($total > 0) {
					$url = get_moderar_topico_url ( $forum_id );
					echo " <img style='vertical-align: bottom;' src='". get_tema_image_url('warning.png') ."'>";
				}
				?>	
			</td>
			<td class="text-center">
							<?php
				$total = contar_respostas_pendentes ( $forum_id );
				echo $total;
				
				if ($total > 0) {
					$url = get_moderar_resposta_url ( $forum_id );
					echo " <img style='vertical-align: bottom;' src='". get_tema_image_url('warning.png') ."'>";
				}
				?>	
			</td>
		</tr>
		<?php
			}
			wp_reset_query ();
			?>
	</table>
	<br /> <br />
		<?php
		}
		
		//Indisponíveis
		if (count ( $foruns_indisponiveis ) > 0) {
	
			?>
			<table class="table-margin table table-striped table-bordered">
		<tr class="bg-blue text-white" align="center">
			<td class="font-weight-bold new_address text-white" align="center" colspan="4">F&oacute;runs que
				voc&ecirc; modera - Cursos Indisponíveis</td>
		</tr>
		<tr>
			<td class="font-weight-bold">F&oacute;rum</td>
			<td class="font-weight-bold text-center">Alunos</td>
			<td class="font-weight-bold text-center">Tópicos Pendentes</td>
			<td class="font-weight-bold text-center">Respostas Pendentes</td>
		</tr>
			<?php
			foreach ( $foruns_indisponiveis as $post ) {
				// setup_postdata ( $post );
				$forum_id = get_post_meta ( $post->post_id, 'forum_id', true );
				if (empty ( $forum_id ))
					continue;
				
				$forum = bbp_get_forum ( $forum_id );
				if (! isset ( $forum ))
					continue;
				
				//if (is_forum_expirado ( $forum_id ))
				//	continue;
				
				if ($forum->post_status == 'hidden')
					continue;
				
				$total_respostas = bbp_get_forum_post_count ( $forum_id );
				?>
		<tr>
			<td class="">
				<?php do_action( 'bbp_theme_before_forum_title' ); ?>
				<a class="bbp-forum-title"
				href="<?php bbp_forum_permalink($forum_id); ?>"><?php bbp_forum_title($forum_id); ?></a>
				<?php do_action( 'bbp_theme_after_forum_title' ); ?>
			</td>

			<td class="text-center">
				<?php echo count(bbp_get_forum_subscribers($forum_id)) ?>
			</td>
			
			<td class="text-center">
				<?php
				$total = contar_topicos_pendentes ( $forum_id );
				echo $total;
				
				if ($total > 0) {
					$url = get_moderar_topico_url ( $forum_id );
					echo " <img style='vertical-align: bottom;' src='". get_tema_image_url('warning.png') ."'>";
				}
				?>	
			</td>
			<td class="text-center">
							<?php
				$total = contar_respostas_pendentes ( $forum_id );
				echo $total;
				
				if ($total > 0) {
					$url = get_moderar_resposta_url ( $forum_id );
					echo " <img style='vertical-align: bottom;' src='". get_tema_image_url('warning.png') ."'>";
				}
				?>	
			</td>
		</tr>
		<?php
			}
			wp_reset_query ();
			?>
	</table>
	<br /> <br />
		<?php
		}
	}

	$result = get_pedidos ( $user_ID );
	
	$forum_ids = array();
	
	if ($result) {
		foreach ( $result as $row ) {
			$post_id = $row->post_id;

			try {
                $order = new WC_Order ($post_id);

                $items = $order->get_items();
                $items = converter_pacote_antigo($items);
                foreach ($items as $item) {
                    $forum_id = get_post_meta($item ['product_id'], 'forum_id', true);
                    if (empty ($forum_id))
                        continue;

                    $forum = bbp_get_forum($forum_id);
                    if (!isset ($forum))
                        continue;

                    if (is_forum_expirado($forum_id))
                        continue;

                    if ($forum->post_status == 'hidden')
                        continue;

                    if ($order->status == 'completed') {
                        array_push($forum_ids, $forum_id);
                    }
                }
            }
            catch (Exception $e) {
			    continue;
            }
		}
	}
	?>
	
	<?php if ($forum_ids) : ?>
	<table class="table table-striped table-bordered">
		<tr class="bg-blue text-white" align="center">
			<td class="font-weight-bold" align="center" colspan="3">F&oacute;runs de
				que voc&ecirc; participa</td>
		</tr>
		<tr class="font-weight-bold">
			<td>F&oacute;rum</td>
			<td class="text-center">T&oacute;picos</td>
			<td class="text-center">T&oacute;pico mais recente</td>
		</tr>
		<?php foreach ( $forum_ids as $forum_id ) : ?> 
		<tr>
			<td>
				<?php do_action( 'bbp_theme_before_forum_title' ); ?>
				<a class="bbp-forum-title"
				href="<?php bbp_forum_permalink($forum_id); ?>"><?php bbp_forum_title($forum_id); ?></a>
				<?php do_action( 'bbp_theme_after_forum_title' ); ?>
			</td>
			<td class="text-center">
				<?php bbp_forum_topic_count($forum_id); ?>
			</td>
			<td class="text-center">
				<?php do_action( 'bbp_theme_before_forum_freshness_link' ); ?>
				<?php bbp_forum_freshness_link($forum_id); ?>
				<?php do_action( 'bbp_theme_after_forum_freshness_link' ); ?>
			</td>
		</tr>
		<?php endforeach; ?>
	</table>
	<?php else : ?>
		<?php if(!is_professor(get_current_user_id()) && !current_user_can('administrator')) : ?>
			<script>window.location.replace('/sem-forum');</script>
		<?php endif; ?>
	<?php endif;?>
	
	<?php if (current_user_can ( 'update_core' )) {	?>
		<div class="text-center col-12 mt-3 mb-5">
		<a class="btn u-btn-darkblue" href="/forums">Ver todos os f&oacute;rums</a>
	</div>
<?php } ?>
</div>
</div>
<!-- #content -->
<div class="sectionlargegap"></div>
<?php get_footer(); ?>