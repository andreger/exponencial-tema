<?php get_header(); ?>
<div class="container-fluid p-0">
<div class="pt-1 pt-md-5"></div>
<div class="col-12 text-blue text-center">
<img width="45" src="/wp-content/themes/academy/images/coaching-icon.png">	
<h2 class="text-uppercase">Coaching - Vagas Abertas</h2>
</div>


<div id="accordion" class="container p-2 p-md-0">
  <div class="card">
    <div class="card-header" id="headingOne">
      <h5 class="mb-0">
        <button class="btn btn-link font-20 text-blue" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Área Fiscal
        </button>
      </h5>
    </div>

    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
      <div class="card-body">
        <div class="row bg-gray rounded p-3">
			<div class="col-12 col-lg-1 text-center">	
				<img width="50" src="/wp-content/themes/academy/images/coaching2-area-fiscal.png">
			</div>	
		<div class="ml-3 ml-md-5 ml-lg-0 p-0 mt-3 mt-lg-0 col-12 col-lg-2 coaching-concurso line-height-2 font-11 text-dark">
			<ul class="ml-auto mr-auto ml-lg-0 mr-lg-0 col-8 col-md-4 col-lg-12">
				<li>Receita Federal</li>
				<li>ICMS</li>
				<li>ISS</li>
				<li>Auditor-Fiscal do Trabalho</li>
			</ul>
		</div>	
		

	<div class="p-0 col-12 col-md-4 col-lg-3">
    	<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
    		<div class="row">
    			<div class="col-6">
   					 <img class="img-fluid ml-3" width="45" src="/wp-content/themes/academy/images/c1-box.png">
   				 </div>
    			<div class="pl-1 pl-lg-3 col-6">
    				<img class="img-fluid ml-3" width="70" src="/wp-content/themes/academy/images/coaching-box.png">
        			<p class="text-white mb-2 font-12">COACHING</p>
       			</div>       
     		</div>
       	<div class="bg-aqua-box text-white font-22 text-center p-1">MENSAL
       	</div>
      		<div class="mt-3 text-white font-18 text-center p-0">R$ 750,00<br/>
       			<a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a>
    		</div>
    	</div>
    </div>
	<div class="p-0 col-12 col-md-4 col-lg-3 mt-2 mt-md-0">
    	<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
    		<div class="row">
    			<div class="col-6">
   					 <img class="img-fluid ml-3" width="58" src="/wp-content/themes/academy/images/c3-box.png">
   				 </div>
    			<div class="pl-1 pl-lg-3 col-6">
    				<img class="img-fluid ml-3" width="70" src="/wp-content/themes/academy/images/coaching-box.png">
        			<p class="text-white mb-2 font-12">COACHING</p>
       			</div>       
     		</div>
       	<div class="bg-aqua-box text-white font-22 text-center p-1">TRIMESTRAL
       	</div>
      		<div class="line-height-1-2 mt-1 text-white font-13 text-center p-0"><strike>R$ 2.250,00</strike> R$ 1.687,50 <span class="font-11">economia de</span> R$ 562,50<br/> 25% OFF
       			<p><a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a></p>
    		</div>
    	</div>
    </div>
	<div class="p-0 col-12 col-md-4 col-lg-3 mt-2 mt-md-0">
    	<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
    		<div class="row">
    			<div class="col-6">
   					 <img class="img-fluid ml-3" width="58" src="/wp-content/themes/academy/images/c6-box.png">
   				 </div>
    			<div class="pl-1 pl-lg-3 col-6">
    				<img class="img-fluid ml-3" width="70" src="/wp-content/themes/academy/images/coaching-box.png">
        			<p class="text-white mb-2 font-12">COACHING</p>
       			</div>       
     		</div>
       	<div class="bg-aqua-box text-white font-22 text-center p-1">SEMESTRAL
       	</div>
      		<div class="line-height-1-2 mt-1 text-white font-13 text-center p-0"><strike>R$ 4.500,00</strike> R$ 2.250,00 <span class="font-11">economia de</span> R$2.250,00<br/> 50% OFF
       			<p><a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a></p>
    		</div>
    	</div>
    </div>
		<div class="col-12 mt-5 text-right">
			<button class="btn u-btn-darkblue mr-2"><h5 class="mb-0">VER MAIS COACHING</h5></button>	
		</div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo">
      <h5 class="mb-0">
        <button class="btn btn-link font-20 text-blue collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Área de Controle
        </button>
      </h5>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
      <div class="card-body">
        <div class="row bg-gray rounded p-3">
			<div class="col-12 col-lg-1 text-center">	
				<img width="50" src="/wp-content/themes/academy/images/coaching2-area-fiscal.png">
			</div>	
		<div class="ml-3 ml-md-5 ml-lg-0 p-0 mt-3 mt-lg-0 col-12 col-lg-2 coaching-concurso line-height-2 font-11 text-dark">
			<ul class="ml-auto mr-auto ml-lg-0 mr-lg-0 col-8 col-md-4 col-lg-12">
				<li>TCU</li>
				<li>CGU</li>
				<li>Tribunais de Contas e Controladorias Estaduais<br/> e Municipais</li>
				<li>TCM RJ</li>
			</ul>
		</div>	
		

	<div class="p-0 col-12 col-md-4 col-lg-3">
    	<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
    		<div class="row">
    			<div class="col-6">
   					 <img class="img-fluid ml-3" width="45" src="/wp-content/themes/academy/images/c1-box.png">
   				 </div>
    			<div class="pl-1 pl-lg-3 col-6">
    				<img class="img-fluid ml-3" width="70" src="/wp-content/themes/academy/images/coaching-box.png">
        			<p class="text-white mb-2 font-12">COACHING</p>
       			</div>       
     		</div>
       	<div class="bg-aqua-box text-white font-22 text-center p-1">MENSAL
       	</div>
      		<div class="mt-3 text-white font-18 text-center p-0">R$ 750,00<br/>
       			<a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a>
    		</div>
    	</div>
    </div>
	<div class="p-0 col-12 col-md-4 col-lg-3 mt-2 mt-md-0">
    	<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
    		<div class="row">
    			<div class="col-6">
   					 <img class="img-fluid ml-3" width="58" src="/wp-content/themes/academy/images/c3-box.png">
   				 </div>
    			<div class="pl-1 pl-lg-3 col-6">
    				<img class="img-fluid ml-3" width="70" src="/wp-content/themes/academy/images/coaching-box.png">
        			<p class="text-white mb-2 font-12">COACHING</p>
       			</div>       
     		</div>
       	<div class="bg-aqua-box text-white font-22 text-center p-1">TRIMESTRAL
       	</div>
      		<div class="line-height-1-2 mt-1 text-white font-13 text-center p-0"><strike>R$1.800,00</strike> R$ 1.350,00 <span class="font-11">economia de</span> R$ 450,00<br/> 25% OFF
       			<p><a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a></p>
    		</div>
    	</div>
    </div>
	<div class="p-0 col-12 col-md-4 col-lg-3 mt-2 mt-md-0">
    	<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
    		<div class="row">
    			<div class="col-6">
   					 <img class="img-fluid ml-3" width="58" src="/wp-content/themes/academy/images/c6-box.png">
   				 </div>
    			<div class="pl-1 pl-lg-3 col-6">
    				<img class="img-fluid ml-3" width="70" src="/wp-content/themes/academy/images/coaching-box.png">
        			<p class="text-white mb-2 font-12">COACHING</p>
       			</div>       
     		</div>
       	<div class="bg-aqua-box text-white font-22 text-center p-1">SEMESTRAL
       	</div>
      		<div class="line-height-1-2 mt-1 text-white font-13 text-center p-0"><strike>R$ 3.600,00</strike> R$ 1.800,00 <span class="font-11">economia de</span> R$1.800,00<br/> 50% OFF
       			<p><a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a></p>
    		</div>
    	</div>
    </div>
		<div class="col-12 mt-5 text-right">
			<button class="btn u-btn-darkblue mr-2"><h5 class="mb-0">VER MAIS COACHING</h5></button>	
		</div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingThree">
      <h5 class="mb-0">
        <button class="btn btn-link font-20 text-blue collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Área Policial
        </button>
      </h5>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
      <div class="card-body">
        <div class="row bg-gray rounded p-3">
			<div class="col-12 col-lg-1 text-center">	
				<img width="50" src="/wp-content/themes/academy/images/coaching2-area-fiscal.png">
			</div>	
		<div class="ml-0 ml-md-4 ml-lg-0 p-0 mt-3 mt-lg-0 col-12 col-lg-2 coaching-concurso line-height-2 font-11 text-dark">
			<ul class="ml-auto mr-auto ml-lg-0 mr-lg-0 col-8 col-md-4 col-lg-12">
				<li>Polícia Federal</li>
				<li>Polícia Rodoviária Federal</li>
				<li>Polícias Civil e Militar</li>
			</ul>
		</div>	
		

	<div class="p-0 col-12 col-md-4 col-lg-3">
    	<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
    		<div class="row">
    			<div class="col-6">
   					 <img class="img-fluid ml-3" width="45" src="/wp-content/themes/academy/images/c1-box.png">
   				 </div>
    			<div class="pl-1 pl-lg-3 col-6">
    				<img class="img-fluid ml-3" width="70" src="/wp-content/themes/academy/images/coaching-box.png">
        			<p class="text-white mb-2 font-12">COACHING</p>
       			</div>       
     		</div>
       	<div class="bg-aqua-box text-white font-22 text-center p-1">MENSAL
       	</div>
      		<div class="mt-3 text-white font-18 text-center p-0">R$ 750,00<br/>
       			<a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a>
    		</div>
    	</div>
    </div>
	<div class="p-0 col-12 col-md-4 col-lg-3 mt-2 mt-md-0">
    	<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
    		<div class="row">
    			<div class="col-6">
   					 <img class="img-fluid ml-3" width="58" src="/wp-content/themes/academy/images/c3-box.png">
   				 </div>
    			<div class="pl-1 pl-lg-3 col-6">
    				<img class="img-fluid ml-3" width="70" src="/wp-content/themes/academy/images/coaching-box.png">
        			<p class="text-white mb-2 font-12">COACHING</p>
       			</div>       
     		</div>
       	<div class="bg-aqua-box text-white font-22 text-center p-1">TRIMESTRAL
       	</div>
      		<div class="line-height-1-2 mt-1 text-white font-13 text-center p-0"><strike>R$ 2.250,00</strike> R$ 1.687,50 <span class="font-11">economia de</span> R$ 562,50<br/> 25% OFF
       			<p><a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a></p>
    		</div>
    	</div>
    </div>
	<div class="p-0 col-12 col-md-4 col-lg-3 mt-2 mt-md-0">
    	<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
    		<div class="row">
    			<div class="col-6">
   					 <img class="img-fluid ml-3" width="58" src="/wp-content/themes/academy/images/c6-box.png">
   				 </div>
    			<div class="pl-1 pl-lg-3 col-6">
    				<img class="img-fluid ml-3" width="70" src="/wp-content/themes/academy/images/coaching-box.png">
        			<p class="text-white mb-2 font-12">COACHING</p>
       			</div>       
     		</div>
       	<div class="bg-aqua-box text-white font-22 text-center p-1">SEMESTRAL
       	</div>
      		<div class="line-height-1-2 mt-1 text-white font-13 text-center p-0"><strike>R$ 4.500,00</strike> R$ 2.250,00 <span class="font-11">economia de</span> R$2.250,00<br/> 50% OFF
       			<p><a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a></p>
    		</div>
    	</div>
    </div>
		<div class="col-12 mt-5 text-right">
			<button class="btn u-btn-darkblue mr-2"><h5 class="mb-0">VER MAIS COACHING</h5></button>	
		</div>
      </div>
    </div>
  </div>
   <div class="card">
    <div class="card-header" id="headingFour">
      <h5 class="mb-0">
        <button class="btn btn-link font-20 text-blue collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
          Tribunais (Analista e Técnico)
        </button>
      </h5>
    </div>
    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
      <div class="card-body">
        <div class="row bg-gray rounded p-3">
			<div class="col-12 col-lg-1 text-center">	
				<img width="50" src="/wp-content/themes/academy/images/coaching2-area-fiscal.png">
			</div>	
		<div class="ml-2 ml-md-4 ml-lg-0 p-0 mt-3 mt-lg-0 col-12 col-lg-2 coaching-concurso line-height-2 font-11 text-dark">
			<ul class="ml-auto mr-auto ml-lg-0 mr-lg-0 col-3 col-md-2 col-lg-12">
				<li>TRT</li>
				<li>TRE</li>
				<li>TRF</li>
				<li>TJ</li>
			</ul>
		</div>	
		

	<div class="p-0 col-12 col-md-4 col-lg-3">
    	<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
    		<div class="row">
    			<div class="col-6">
   					 <img class="img-fluid ml-3" width="45" src="/wp-content/themes/academy/images/c1-box.png">
   				 </div>
    			<div class="pl-1 pl-lg-3 col-6">
    				<img class="img-fluid ml-3" width="70" src="/wp-content/themes/academy/images/coaching-box.png">
        			<p class="text-white mb-2 font-12">COACHING</p>
       			</div>       
     		</div>
       	<div class="bg-aqua-box text-white font-22 text-center p-1">MENSAL
       	</div>
      		<div class="mt-3 text-white font-18 text-center p-0">R$ 600,00<br/>
       			<a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a>
    		</div>
    	</div>
    </div>
	<div class="p-0 col-12 col-md-4 col-lg-3 mt-2 mt-md-0">
    	<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
    		<div class="row">
    			<div class="col-6">
   					 <img class="img-fluid ml-3" width="58" src="/wp-content/themes/academy/images/c3-box.png">
   				 </div>
    			<div class="pl-1 pl-lg-3 col-6">
    				<img class="img-fluid ml-3" width="70" src="/wp-content/themes/academy/images/coaching-box.png">
        			<p class="text-white mb-2 font-12">COACHING</p>
       			</div>       
     		</div>
       	<div class="bg-aqua-box text-white font-22 text-center p-1">TRIMESTRAL
       	</div>
      		<div class="line-height-1-2 mt-1 text-white font-13 text-center p-0"><strike>R$ 1.800,00</strike> R$ 1.350,00 <span class="font-11">economia de</span> R$ 450,00<br/> 25% OFF
       			<p><a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a></p>
    		</div>
    	</div>
    </div>
	<div class="p-0 col-12 col-md-4 col-lg-3 mt-2 mt-md-0">
    	<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
    		<div class="row">
    			<div class="col-6">
   					 <img class="img-fluid ml-3" width="58" src="/wp-content/themes/academy/images/c6-box.png">
   				 </div>
    			<div class="pl-1 pl-lg-3 col-6">
    				<img class="img-fluid ml-3" width="70" src="/wp-content/themes/academy/images/coaching-box.png">
        			<p class="text-white mb-2 font-12">COACHING</p>
       			</div>       
     		</div>
       	<div class="bg-aqua-box text-white font-22 text-center p-1">SEMESTRAL
       	</div>
      		<div class="line-height-1-2 mt-1 text-white font-13 text-center p-0"><strike>R$ 3.600,00</strike> R$ 1.800,00 <span class="font-11">economia de</span> R$1.800,00<br/> 50% OFF
       			<p><a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a></p>
    		</div>
    	</div>
    </div>
		<div class="col-12 mt-5 text-right">
			<button class="btn u-btn-darkblue mr-2"><h5 class="mb-0">VER MAIS COACHING</h5></button>	
		</div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingFive">
      <h5 class="mb-0">
        <button class="btn btn-link font-20 text-blue collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
          Área Legislativa
        </button>
      </h5>
    </div>
    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
      <div class="card-body">
        <div class="row bg-gray rounded p-3">
			<div class="col-12 col-lg-1 text-center">	
				<img width="50" src="/wp-content/themes/academy/images/coaching2-area-fiscal.png">
			</div>	
		<div class="ml-0 ml-md-2 ml-lg-0 p-0 mt-3 mt-lg-0 col-12 col-lg-2 coaching-concurso line-height-2 font-11 text-dark">
			<ul class="ml-auto mr-auto ml-lg-0 mr-lg-0 col-8 col-md-4 col-lg-12">
				<li>Câmara dos Deputados</li>
				<li>Senado Federall</li>
				<li>Assembléias Legislativas</li>
			</ul>
		</div>	
		

	<div class="p-0 col-12 col-md-4 col-lg-3">
    	<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
    		<div class="row">
    			<div class="col-6">
   					 <img class="img-fluid ml-3" width="45" src="/wp-content/themes/academy/images/c1-box.png">
   				 </div>
    			<div class="pl-1 pl-lg-3 col-6">
    				<img class="img-fluid ml-3" width="70" src="/wp-content/themes/academy/images/coaching-box.png">
        			<p class="text-white mb-2 font-12">COACHING</p>
       			</div>       
     		</div>
       	<div class="bg-aqua-box text-white font-22 text-center p-1">MENSAL
       	</div>
      		<div class="mt-3 text-white font-18 text-center p-0">R$ 750,00<br/>
       			<a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a>
    		</div>
    	</div>
    </div>
	<div class="p-0 col-12 col-md-4 col-lg-3 mt-2 mt-md-0">
    	<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
    		<div class="row">
    			<div class="col-6">
   					 <img class="img-fluid ml-3" width="58" src="/wp-content/themes/academy/images/c3-box.png">
   				 </div>
    			<div class="pl-1 pl-lg-3 col-6">
    				<img class="img-fluid ml-3" width="70" src="/wp-content/themes/academy/images/coaching-box.png">
        			<p class="text-white mb-2 font-12">COACHING</p>
       			</div>       
     		</div>
       	<div class="bg-aqua-box text-white font-22 text-center p-1">TRIMESTRAL
       	</div>
      		<div class="line-height-1-2 mt-1 text-white font-13 text-center p-0"><strike>R$ 2.250,00</strike> R$ 1.687,50 <span class="font-11">economia de</span> R$ 562,50<br/> 25% OFF
       			<p><a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a></p>
    		</div>
    	</div>
    </div>
	<div class="p-0 col-12 col-md-4 col-lg-3 mt-2 mt-md-0">
    	<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
    		<div class="row">
    			<div class="col-6">
   					 <img class="img-fluid ml-3" width="58" src="/wp-content/themes/academy/images/c6-box.png">
   				 </div>
    			<div class="pl-1 pl-lg-3 col-6">
    				<img class="img-fluid ml-3" width="70" src="/wp-content/themes/academy/images/coaching-box.png">
        			<p class="text-white mb-2 font-12">COACHING</p>
       			</div>       
     		</div>
       	<div class="bg-aqua-box text-white font-22 text-center p-1">SEMESTRAL
       	</div>
      		<div class="line-height-1-2 mt-1 text-white font-13 text-center p-0"><strike>R$ 4.500,00</strike> R$ 2.250,00 <span class="font-11">economia de</span> R$2.250,00<br/> 50% OFF
       			<p><a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a></p>
    		</div>
    	</div>
    </div>
		<div class="col-12 mt-5 text-right">
			<button class="btn u-btn-darkblue mr-2"><h5 class="mb-0">VER MAIS COACHING</h5></button>	
		</div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingSix">
      <h5 class="mb-0">
        <button class="btn btn-link font-20 text-blue collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
          Área Jurídica
        </button>
      </h5>
    </div>
    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
      <div class="card-body">
        <div class="row bg-gray rounded p-3">
			<div class="col-12 col-lg-1 text-center">	
				<img width="50" src="/wp-content/themes/academy/images/coaching2-area-fiscal.png">
			</div>	
		<div class="ml-0 ml-md-2 ml-lg-0 p-0 mt-3 mt-lg-0 col-12 col-lg-2 coaching-concurso line-height-2 font-11 text-dark">
			<ul class="ml-auto mr-auto ml-lg-0 mr-lg-0 col-8 col-md-4 col-lg-12">
				<li>Procuradorias (PGE, PGM)</li>
				<li>Defensorias Públicos</li>
				<li>Ministérios Públicos</li>
				<li>Advocacia Pública (AGU, PGF, PFN e PGBACEN)</li>
			</ul>
		</div>	
		

	<div class="p-0 col-12 col-md-4 col-lg-3">
    	<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
    		<div class="row">
    			<div class="col-6">
   					 <img class="img-fluid ml-3" width="45" src="/wp-content/themes/academy/images/c1-box.png">
   				 </div>
    			<div class="pl-1 pl-lg-3 col-6">
    				<img class="img-fluid ml-3" width="70" src="/wp-content/themes/academy/images/coaching-box.png">
        			<p class="text-white mb-2 font-12">COACHING</p>
       			</div>       
     		</div>
       	<div class="bg-aqua-box text-white font-22 text-center p-1">MENSAL
       	</div>
      		<div class="mt-3 text-white font-18 text-center p-0">R$ 975,00<br/>
       			<a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a>
    		</div>
    	</div>
    </div>
	<div class="p-0 col-12 col-md-4 col-lg-3 mt-2 mt-md-0">
    	<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
    		<div class="row">
    			<div class="col-6">
   					 <img class="img-fluid ml-3" width="58" src="/wp-content/themes/academy/images/c3-box.png">
   				 </div>
    			<div class="pl-1 pl-lg-3 col-6">
    				<img class="img-fluid ml-3" width="70" src="/wp-content/themes/academy/images/coaching-box.png">
        			<p class="text-white mb-2 font-12">COACHING</p>
       			</div>       
     		</div>
       	<div class="bg-aqua-box text-white font-22 text-center p-1">TRIMESTRAL
       	</div>
      		<div class="line-height-1-2 mt-1 text-white font-13 text-center p-0"><strike>R$ 2.925,00</strike> R$ 1.193,75 <span class="font-11">economia de</span> R$ 731,25<br/> 25% OFF
       			<p><a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a></p>
    		</div>
    	</div>
    </div>
	<div class="p-0 col-12 col-md-4 col-lg-3 mt-2 mt-md-0">
    	<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
    		<div class="row">
    			<div class="col-6">
   					 <img class="img-fluid ml-3" width="58" src="/wp-content/themes/academy/images/c6-box.png">
   				 </div>
    			<div class="pl-1 pl-lg-3 col-6">
    				<img class="img-fluid ml-3" width="70" src="/wp-content/themes/academy/images/coaching-box.png">
        			<p class="text-white mb-2 font-12">COACHING</p>
       			</div>       
     		</div>
       	<div class="bg-aqua-box text-white font-22 text-center p-1">SEMESTRAL
       	</div>
      		<div class="line-height-1-2 mt-1 text-white font-13 text-center p-0"><strike>R$ 5.850,00</strike> R$ 2.925,00 <span class="font-11">economia de</span> R$2.925,00<br/> 50% OFF
       			<p><a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a></p>
    		</div>
    	</div>
    </div>
		<div class="col-12 mt-5 text-right">
			<button class="btn u-btn-darkblue mr-2"><h5 class="mb-0">VER MAIS COACHING</h5></button>	
		</div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingSeven">
      <h5 class="mb-0">
        <button class="btn btn-link font-20 text-blue collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
          Área Bancária
        </button>
      </h5>
    </div>
    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion">
      <div class="card-body">
        <div class="row bg-gray rounded p-3">
			<div class="col-12 col-lg-1 text-center">	
				<img width="50" src="/wp-content/themes/academy/images/coaching2-area-fiscal.png">
			</div>	
		<div class="ml-3 ml-lg-0 p-0 mt-3 mt-lg-0 col-12 col-lg-2 coaching-concurso line-height-2 font-11 text-dark">
			<ul class="ml-auto mr-auto ml-lg-0 mr-lg-0 col-6 col-md-3 col-lg-12">
				<li>BACEN</li>
				<li>Banco do Brasil</li>
				<li>Caixa Econômica</li>
				<li>Bancos Regionais</li>
			</ul>
		</div>	
		

	<div class="p-0 col-12 col-md-4 col-lg-3">
    	<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
    		<div class="row">
    			<div class="col-6">
   					 <img class="img-fluid ml-3" width="45" src="/wp-content/themes/academy/images/c1-box.png">
   				 </div>
    			<div class="pl-1 pl-lg-3 col-6">
    				<img class="img-fluid ml-3" width="70" src="/wp-content/themes/academy/images/coaching-box.png">
        			<p class="text-white mb-2 font-12">COACHING</p>
       			</div>       
     		</div>
       	<div class="bg-aqua-box text-white font-22 text-center p-1">MENSAL
       	</div>
      		<div class="mt-3 text-white font-18 text-center p-0">R$ 750,00<br/>
       			<a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a>
    		</div>
    	</div>
    </div>
	<div class="p-0 col-12 col-md-4 col-lg-3 mt-2 mt-md-0">
    	<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
    		<div class="row">
    			<div class="col-6">
   					 <img class="img-fluid ml-3" width="58" src="/wp-content/themes/academy/images/c3-box.png">
   				 </div>
    			<div class="pl-1 pl-lg-3 col-6">
    				<img class="img-fluid ml-3" width="70" src="/wp-content/themes/academy/images/coaching-box.png">
        			<p class="text-white mb-2 font-12">COACHING</p>
       			</div>       
     		</div>
       	<div class="bg-aqua-box text-white font-22 text-center p-1">TRIMESTRAL
       	</div>
      		<div class="line-height-1-2 mt-1 text-white font-13 text-center p-0"><strike>R$ 2.250,00</strike> R$ 1.687,50 <span class="font-11">economia de</span> R$ 562,50<br/> 25% OFF
       			<p><a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a></p>
    		</div>
    	</div>
    </div>
	<div class="p-0 col-12 col-md-4 col-lg-3 mt-2 mt-md-0">
    	<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
    		<div class="row">
    			<div class="col-6">
   					 <img class="img-fluid ml-3" width="58" src="/wp-content/themes/academy/images/c6-box.png">
   				 </div>
    			<div class="pl-1 pl-lg-3 col-6">
    				<img class="img-fluid ml-3" width="70" src="/wp-content/themes/academy/images/coaching-box.png">
        			<p class="text-white mb-2 font-12">COACHING</p>
       			</div>       
     		</div>
       	<div class="bg-aqua-box text-white font-22 text-center p-1">SEMESTRAL
       	</div>
      		<div class="line-height-1-2 mt-1 text-white font-13 text-center p-0"><strike>R$ 4.500,00</strike> R$ 2.250,00 <span class="font-11">economia de</span> R$2.250,00<br/> 50% OFF
       			<p><a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a></p>
    		</div>
    	</div>
    </div>
		<div class="col-12 mt-5 text-right">
			<button class="btn u-btn-darkblue mr-2"><h5 class="mb-0">VER MAIS COACHING</h5></button>	
		</div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingEight">
      <h5 class="mb-0">
        <button class="btn btn-link font-20 text-blue collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
          INSS
        </button>
      </h5>
    </div>
    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion">
      <div class="card-body">
        <div class="row bg-gray rounded p-3">
			<div class="col-12 col-lg-1 text-center">	
				<img width="50" src="/wp-content/themes/academy/images/coaching2-area-fiscal.png">
			</div>	
		<div class="ml-0 ml-md-3 ml-lg-0 p-0 mt-3 mt-lg-0 col-12 col-lg-2 coaching-concurso line-height-2 font-11 text-dark">
			<ul class="ml-auto mr-auto ml-lg-0 mr-lg-0 col-5 col-md-3 col-lg-12">
				<li>Técnico INSS</li>
				<li>Analista INSS</li>
			</ul>
		</div>	
		

	<div class="p-0 col-12 col-md-4 col-lg-3">
    	<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
    		<div class="row">
    			<div class="col-6">
   					 <img class="img-fluid ml-3" width="45" src="/wp-content/themes/academy/images/c1-box.png">
   				 </div>
    			<div class="pl-1 pl-lg-3 col-6">
    				<img class="img-fluid ml-3" width="70" src="/wp-content/themes/academy/images/coaching-box.png">
        			<p class="text-white mb-2 font-12">COACHING</p>
       			</div>       
     		</div>
       	<div class="bg-aqua-box text-white font-22 text-center p-1">MENSAL
       	</div>
      		<div class="mt-3 text-white font-18 text-center p-0">R$ 600,00<br/>
       			<a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a>
    		</div>
    	</div>
    </div>
	<div class="p-0 col-12 col-md-4 col-lg-3 mt-2 mt-md-0">
    	<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
    		<div class="row">
    			<div class="col-6">
   					 <img class="img-fluid ml-3" width="58" src="/wp-content/themes/academy/images/c3-box.png">
   				 </div>
    			<div class="pl-1 pl-lg-3 col-6">
    				<img class="img-fluid ml-3" width="70" src="/wp-content/themes/academy/images/coaching-box.png">
        			<p class="text-white mb-2 font-12">COACHING</p>
       			</div>       
     		</div>
       	<div class="bg-aqua-box text-white font-22 text-center p-1">TRIMESTRAL
       	</div>
      		<div class="line-height-1-2 mt-1 text-white font-13 text-center p-0"><strike>R$ 1.800,00</strike> R$ 1.350,00 <span class="font-11">economia de</span> R$ 450,00<br/> 25% OFF
       			<p><a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a></p>
    		</div>
    	</div>
    </div>
	<div class="p-0 col-12 col-md-4 col-lg-3 mt-2 mt-md-0">
    	<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
    		<div class="row">
    			<div class="col-6">
   					 <img class="img-fluid ml-3" width="58" src="/wp-content/themes/academy/images/c6-box.png">
   				 </div>
    			<div class="pl-1 pl-lg-3 col-6">
    				<img class="img-fluid ml-3" width="70" src="/wp-content/themes/academy/images/coaching-box.png">
        			<p class="text-white mb-2 font-12">COACHING</p>
       			</div>       
     		</div>
       	<div class="bg-aqua-box text-white font-22 text-center p-1">SEMESTRAL
       	</div>
      		<div class="line-height-1-2 mt-1 text-white font-13 text-center p-0"><strike>R$ 3.600,00</strike> R$ 1.800,00 <span class="font-11">economia de</span> R$1.800,00<br/> 50% OFF
       			<p><a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a></p>
    		</div>
    	</div>
    </div>
		<div class="col-12 mt-5 text-right">
			<button class="btn u-btn-darkblue mr-2"><h5 class="mb-0">VER MAIS COACHING</h5></button>	
		</div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingNine">
      <h5 class="mb-0">
        <button class="btn btn-link font-20 text-blue collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
          Agências e outras áreas
        </button>
      </h5>
    </div>
    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion">
      <div class="card-body">
        <div class="row bg-gray rounded p-3">
			<div class="col-12 col-lg-1 text-center">	
				<img width="50" src="/wp-content/themes/academy/images/coaching2-area-fiscal.png">
			</div>	
		<div class="ml-3 ml-lg-0 p-0 mt-3 mt-lg-0 col-12 col-lg-2 coaching-concurso line-height-2 font-11 text-dark">
			<ul class="ml-auto mr-auto ml-lg-0 mr-lg-0 col-4 col-md-2 col-lg-12">
				<li>CVM</li>
				<li>SUSEP</li>
				<li>ANS</li>
				<li>ANTT</li>
				<li>ANVISA</li>
			</ul>
		</div>	
		

	<div class="p-0 col-12 col-md-4 col-lg-3">
    	<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
    		<div class="row">
    			<div class="col-6">
   					 <img class="img-fluid ml-3" width="45" src="/wp-content/themes/academy/images/c1-box.png">
   				 </div>
    			<div class="pl-1 pl-lg-3 col-6">
    				<img class="img-fluid ml-3" width="70" src="/wp-content/themes/academy/images/coaching-box.png">
        			<p class="text-white mb-2 font-12">COACHING</p>
       			</div>       
     		</div>
       	<div class="bg-aqua-box text-white font-22 text-center p-1">MENSAL
       	</div>
      		<div class="mt-3 text-white font-18 text-center p-0">R$ 750,00<br/>
       			<a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a>
    		</div>
    	</div>
    </div>
	<div class="p-0 col-12 col-md-4 col-lg-3 mt-2 mt-md-0">
    	<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
    		<div class="row">
    			<div class="col-6">
   					 <img class="img-fluid ml-3" width="58" src="/wp-content/themes/academy/images/c3-box.png">
   				 </div>
    			<div class="pl-1 pl-lg-3 col-6">
    				<img class="img-fluid ml-3" width="70" src="/wp-content/themes/academy/images/coaching-box.png">
        			<p class="text-white mb-2 font-12">COACHING</p>
       			</div>       
     		</div>
       	<div class="bg-aqua-box text-white font-22 text-center p-1">TRIMESTRAL
       	</div>
      		<div class="line-height-1-2 mt-1 text-white font-13 text-center p-0"><strike>R$ 2.250,00</strike> R$ 1.687,50 <span class="font-11">economia de</span> R$ 562,50<br/> 25% OFF
       			<p><a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a></p>
    		</div>
    	</div>
    </div>
	<div class="p-0 col-12 col-md-4 col-lg-3 mt-2 mt-md-0">
    	<div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
    		<div class="row">
    			<div class="col-6">
   					 <img class="img-fluid ml-3" width="58" src="/wp-content/themes/academy/images/c6-box.png">
   				 </div>
    			<div class="pl-1 pl-lg-3 col-6">
    				<img class="img-fluid ml-3" width="70" src="/wp-content/themes/academy/images/coaching-box.png">
        			<p class="text-white mb-2 font-12">COACHING</p>
       			</div>       
     		</div>
       	<div class="bg-aqua-box text-white font-22 text-center p-1">SEMESTRAL
       	</div>
      		<div class="line-height-1-2 mt-1 text-white font-13 text-center p-0"><strike>R$ 4.500,00</strike> R$ 2.250,00 <span class="font-11">economia de</span> R$2.250,00<br/> 50% OFF
       			<p><a class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="/painel-coaching/inscricao">Inscreva-se Já</a></p>
    		</div>
    	</div>
    </div>
		<div class="col-12 mt-5 text-right">
			<button class="btn u-btn-darkblue mr-2"><h5 class="mb-0">VER MAIS COACHING</h5></button>	
		</div>
      </div>
    </div>
  </div> 
 </div> 
</div>
</div> 
</div> 
</div> 
</div> 
</div> 
</div> 
</div> 
</div>  

<div class="col-12 text-blue text-center mt-3">
<img width="55" src="/wp-content/themes/academy/images/coaching2-modulos.png">
<h2 class="text-uppercase">MÓDULOS DO PROGRAMA</h2>
</div>

<div class="bg-gray row mr-auto ml-auto col-12 pt-3 pb-2">
	<div class="col-12 col-md-6 pr-0 text-center border-coaching">
		<div class="col-12 col-md-10 ml-auto">
<div class="col-12 col-md-6 ml-auto">	
<img class="mt-1" width="55" src="/wp-content/themes/academy/images/coaching2-raio-x.png"></div>
<h2 class="text-center text-md-right line-height-1 mt-1">Raio-x do Aluno</h2>
<p class="text-center text-md-right line-height-1-5">Dentro dos ciclos de estudos, serão definidas as metas padrões e extras para serem cumpridas pelos alunos. O resultado (aprovação no concurso desejado) está extremamente ligado ao cumprimento destas metas.</p> 
<div class="col-12 col-md-6 ml-auto">	
<img class="mt-1" width="55" src="/wp-content/themes/academy/images/coaching2-material.png"></div>
<h2 class="text-center text-md-right line-height-1 mt-1">Material de estudo</h2>
<p class="text-center text-md-right line-height-1-5">Dentro dos ciclos de estudos, serão definidas as metas padrões e extras para serem cumpridas pelos alunos. O resultado (aprovação no concurso desejado) está extremamente ligado ao cumprimento destas metas. </p>
<div class="col-12 col-md-6 ml-auto">	 
<img class="mt-1" width="55" src="/wp-content/themes/academy/images/coaching2-planejamento.png"></div>
<h2 class="text-center text-md-right line-height-1 mt-1">Planejamento, Ciclos e Metas</h2>
<p class="text-center text-md-right line-height-1-5">Dentro dos ciclos de estudos, serão definidas as metas padrões e extras para serem cumpridas pelos alunos. O resultado (aprovação no concurso desejado) está extremamente ligado ao cumprimento destas metas.</p>  
<div class="col-12 col-md-6 ml-auto">	
<img class="mt-1" width="55" src="/wp-content/themes/academy/images/coaching2-questoes.png"></div>
<h2 class="text-center text-md-right line-height-1 mt-1">Questões online</h2>
<p class="text-center text-md-right line-height-1-5">Dentro dos ciclos de estudos, serão definidas as metas padrões e extras para serem cumpridas pelos alunos. O resultado (aprovação no concurso desejado) está extremamente ligado ao cumprimento destas metas. </p> 
</div>
</div>
<div class="col-12 col-md-6 pl-0 mt-0 mt-md-5">
	<div class="col-12 col-md-10 text-center mt-5 pb-5">
		<div class="col-12 col-md-6">		
			<img class="mt-1 mt-lg-4" width="55" src="/wp-content/themes/academy/images/coaching2-simulados.png">
		</div>
	<h2 class="text-center text-md-left line-height-1 mt-1">Simulados online</h2>
	<p class="text-center text-md-left line-height-1-5">Dentro dos ciclos de estudos, serão definidas as metas padrões e extras para serem cumpridas pelos alunos. O resultado (aprovação no concurso desejado) está extremamente ligado ao cumprimento destas metas.</p>
<div class="col-12 col-md-6">	 
<img class="mt-1" width="55" src="/wp-content/themes/academy/images/coaching2-tecnicas.png"></div>
<h2 class="text-center text-md-left line-height-1 mt-1">Técnicas</h2>
<p class="text-center text-md-left line-height-1-5">Dentro dos ciclos de estudos, serão definidas as metas padrões e extras para serem cumpridas pelos alunos. O resultado (aprovação no concurso desejado) está extremamente ligado ao cumprimento destas metas. </p> 
<div class="col-12 col-md-6">	
<img class="mt-1" width="55" src="/wp-content/themes/academy/images/coaching2-acompanhamento.png"></div>
<h2 class="text-center text-md-left line-height-1 mt-1">Acompanhamento e feedback</h2>
<p class="text-center text-md-left line-height-1-5">Dentro dos ciclos de estudos, serão definidas as metas padrões e extras para serem cumpridas pelos alunos. O resultado (aprovação no concurso desejado) está extremamente ligado ao cumprimento destas metas. </p> 
<div class="col-12 col-md-6">	
<img class="mt-1" width="55" src="/wp-content/themes/academy/images/coaching2-posedital.png"></div>
<h2 class="text-center text-md-left line-height-1 mt-1">Pós Edital</h2>
<p class="text-center text-md-left line-height-1-5">Dentro dos ciclos de estudos, serão definidas as metas padrões e extras para serem cumpridas pelos alunos. O resultado (aprovação no concurso desejado) está extremamente ligado ao cumprimento destas metas.</p> 
</div>
</div>
<div class="col-12 text-center mt-2">	
<img width="55" src="/wp-content/themes/academy/images/coaching2-aprovacao.png">
<h2 class="line-height-1 mt-1">Aprovação</h2>
<p class="line-height-1-5 col-12 col-md-5 col-lg-4 ml-auto mr-auto">Dentro dos ciclos de estudos, serão definidas as metas padrões e extras para serem cumpridas pelos alunos.</p> 
</div>
<div class="col-12 text-center mt-5">
	<h5>Veja o índice Completo do Coaching
	<img class="ml-1" width="30" src="/wp-content/themes/academy/images/coaching2-pdf.png">
	<img width="35" src="/wp-content/themes/academy/images/coaching2-video.png"></h5>
	
</div>
</div>

<div class="col-12 text-blue text-center mt-5">
<img width="55" src="/wp-content/themes/academy/images/coaching2-equipe.png">

<h2 class="text-uppercase">EQUIPE</h2>
</div>
<div class="container">
  <div class="row">
    <div id="owl-demo" class="owl-carousel owl-theme" style="margin:20px 0">
      <?php foreach (get_consultores_array() as $consultor) : ?>
      <div class='item'>
        <div class='' style="text-align:center">
            <div><a href='<?php echo get_autor_url($consultor['id']) ?>'><?= get_avatar($consultor['id'], 212); ?></a></div>
          <div class="texto-azul detalhamento-coaching-titulo"><a href='<?php echo get_autor_url($consultor['id']) ?>'><?php echo str_replace(" ", "<br>", $consultor['nome_completo']) ?></a></div>
        </div>
      </div>
      <?php endforeach; ?>
    </div>
  </div>
 </div> 


<div class="col-12 text-blue text-center mt-5">
<img width="55" src="/wp-content/themes/academy/images/coaching2-desconto.png">
<h2 class="text-uppercase">USE CUPONS DE DESCONTOS</h2>
</div>

<div class="col-12 ml-auto mr-auto mt-5">
	<div class="row col-12 col-xl-10 ml-auto mr-auto text-white">
		<div class="col-12 col-lg-4 p-0 ml-0 ml-md-5 ml-lg-0">
			<div class="ml-auto mr-auto col-12 col-md-6 col-lg-12 bg-img-desc-1">
				<h1 class="font-55 font-arial mt-2 mb-0 font-weight-bold">100%</h1>
				<p class="ml-1 mt-0 pb-4">Técnicas de Estudos,<br/> Caderno de Questões,<br/> Simulados, Sistema de Questões</p>			
			</div>
		</div>
		<div class="col-12 col-lg-4 p-0 ml-0 ml-md-5 ml-lg-0">
			<div class="ml-auto mr-auto col-12 col-md-6 col-lg-12 bg-img-desc-2">
				<h1 class="font-55 font-arial mt-2 mb-0 font-weight-bold">50%</h1>
				<p class="ml-1 mt-0 pb-4">Pacotes de Cursos,<br/> 
				Mapas Mentais<br/><br/>
				</p>
			</div>
		</div>
		<div class="col-12 col-lg-4 p-0 ml-0 ml-md-5 ml-lg-0">
			<div class="ml-auto mr-auto col-12 col-md-6 col-lg-12 bg-img-desc-3">
				<h1 class="font-55 font-arial mt-2 mb-0 font-weight-bold">25%</h1>
				<p class="ml-1 mt-0 pb-5">Cursos Avulsos<br/>
				</p><p><br/></p>
			</div>
		</div>
	</div>	
	<div class="text-center mt-3">
		<button class="btn u-btn-darkblue mr-2 pl-4 pr-4"><h5 class="mb-0">ACESSAR CUPONS</h5></button>	
	</div>
</div>

<div class="bg-gray pb-3 mt-5 pl-2 pr-2">
<div class="col-12 text-blue text-center">
<img class="mt-4" width="50" src="/wp-content/themes/academy/images/perguntas.png">
<h2 class="text-uppercase">PERGUNTAS FREQUENTES</h2>
</div>
[faq faq_topic="Portal"] 
</div> 


<div class="col-12 text-blue text-center">
<img class="mt-4" width="50" src="/wp-content/themes/academy/images/coaching2-turma.png">
<h2 class="text-uppercase">TURMA DE COACHING</h2>
</div>


<div class="col-12 mt-3 font-10">
	<div class="ml-auto mr-auto col-10 text-justify p-0">
		<p>O Programa de Turma de Coaching nasceu com o intuito de direcionar os alunos com base nos seus objetivos (por áreas) e nivel (básico e avançada) para permitir que cada um consiga implementar as Metas dentro das suas especificidades. Este programa não é para a RETA FINAL, ou seja, para o PÓS-EDITAL, já que este momento é tratado nas turmas específicas e você pode encontrar AQUI!<br/>As principais características do programa são:</p>
		<ul class="coaching-concurso-3 text-gray">
			<li><span class="text-blue font-weight-bold">Até 6 pessoas por turma:</span> isto significa que você estará em contato direto com as pessoas com mesmo interesse, objetivo e seriedade que você.</li>
			<li><span class="text-blue font-weight-bold">Interações:</span> Além das intereções por whatsapp (contato diário e ilimitado), o Coach/Mentor passa um pente fino regularmente por Skype.</li>
			<li><span class="text-blue font-weight-bold">Metas:</span> 100% focadas nos concursos da sua área, com direcionamento no nível dos ASSUNTOS das disciplinas (extremamente detalhado), verdadeiro "MAPA DA MINA DE OURO" para você focar no filé mignom e ter maior resultado no menor tempo.</li>
			<li><span class="text-blue font-weight-bold">Material de Estudo:</span> recomendados, com total liberdade de escolha pro aluno.</li>
			<li><span class="text-blue font-weight-bold">Descontos Exclusivos:</span> Cursos do Exponencial com <span class="text-danger">25%</span> (avulsos) e <span class="text-danger">50%</span> (pacote), conforme <span class="text-blue">Políticas de Descontos</span>, com acesso <span class="text-danger font-weight-bold">GRATUITO</span> ao Sistema de Questões Online.</li>
		</ul>
	</div>	
</div>


<div class="mb-5 mt-5 col-10 col-md-7 col-lg-6 col-xl-5 ml-auto mr-auto embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/LCNNXEmhpCo" allowfullscreen></iframe>
</div>


<?php get_footer(); ?>