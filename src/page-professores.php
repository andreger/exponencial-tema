<?php
get_header();

KLoader::model('ProfessorModel');
KLoader::helper('UrlHelper');

/**
 * B2965-2 - Miniatura professores || Página professores
 * Listagem original era listar_professores_nao_ocultos(). Alterado temporariamente para exibir professores com curso.
 * Esta listagem não exibe os professores ativados por invasão.
 */
$professores = ProfessorModel::listar_professores_com_cursos();

global $post;
?>
<script src="<?php tema_js_url('filtro-lista-professores.js') ?>"></script>

<div class="container-fluid">

<div class="container pt-1 pt-md-5">
    <h1><?= get_h1($post->ID) ?></h1>

   <div class="col-md-6 offset-md-3">
    <div class="row mb-3 mt-3">
      <div class="col p-0 text-right">
      <input class="pesquisa-barra form-control" type="text" id="search" placeholder="Procure pelo nome do professor" /></div>
      <div class="w-b-l p-0 text-left">
    
        <div class="pesquisa-barra-lupa">
          <i class="fa fa-search mt-2 ml-2 text-white"></i>   
        </div>
      
      </div>
    </div>      
      </div>
<div class="col-12 lista-professores">

<?php foreach ($professores as $professor) : ?>
  <?php $url = UrlHelper::get_professor_url($professor) ?>
 <div class="hide-prof"> 
	<div class="row d-block mt-5 pt-5">
    <div class="ml-10 col-lg-9 d-none d-lg-block">
    <div class="pr_1text ml-2 ml-xl-5 font-weight-bold font-calibri font-20 text-darkblue"><?= $professor->display_name; ?></div>
    <div class="ml-2 ml-xl-5 font-weight-bold font-calibri text-darkblue mr-5"><?= $professor->col_cargo ?></div>
     </div> 

    <div class="col-12 d-block d-lg-none">
    <div class="font-weight-bold font-calibri font-20 text-darkblue text-center"><?= $professor->display_name; ?></div>
    <div class="font-weight-bold font-calibri text-darkblue text-center font-12"><?= $professor->col_cargo; ?></div>
     </div> 


        <div class="row-fluid mb-5 p-2">          
          <div class="row d-none d-lg-flex">
            <div class="ml-2 row col-10 bg-agua-light border-bottom border-secondary">

            <div class="col-3" id="im">              
						    <div class="float-img-professor">                  
                  <?php if($professor->col_foto_url_200 !== get_site_url().'/wp-content/themes/academy/images/avatar.png')  : ?>
                  <img style="width: 200px!important; height: 200px!important;" src="<?= $professor->col_foto_url_200 ?>">
                   <?php endif; ?>
                </div>
            </div>

            <div class="col-9 font-10 text-justify pr_txt_con">
              <?= nl2br($professor->col_mini_cv); ?>
            </div> 

          </div>

             <div class="ml-2 col-2">
                <a href="<?= $url ?>"><img src="<?= THEME_URI; ?>images/bt-siteprofessor.png"> </a>
              </div>       
          </div> 

              
          <div class="row bg-agua-light border-bottom border-secondary d-block d-lg-none
          ">
            <div class="col-12 pt-2">

            <div class="ml-auto mr-auto border-professor text-center" id="im">
                <img style="width: 200px!important; height: 200px!important;" src="<?= $professor->col_foto_url_200; ?>">
            </div>

            <div class="font-10 text-center pr_txt_con mt-3">
              <?= nl2br($professor->col_mini_cv); ?>
            </div> 

          </div>

             <div class="text-center mt-4 mb-1">
                <a href="<?= $url ?>"><img src="<?= THEME_URI; ?>images/bt-siteprofessor.png"> </a>
              </div>       
          </div> 
   

      </div> 
    </div>
  </div>
  <?php endforeach ?>
  </div>

</div>         
  
<?php get_footer(); ?>