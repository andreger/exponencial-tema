<?php get_header(); ?>
<?php 
$url =  $_SERVER['REQUEST_URI'];
if (false !== strpos($url,'cursos-online')) 
{ ?>	
	<div class="woocommerce container" id="container">
		
		<div class="row ml-auto mr-auto">    	
			<div id="nav-todos-cursos" class="ml-auto mr-auto col-8 col-md-4 col-lg-3 mt-3">	
				<?= get_nav_cursos() ?>
			</div>
		<div class="col-12 col-md-8 col-lg-9">
			<h1 class="mt-md-2 mt-4 col-12 text-center">Todos os Cursos</h1>
			<div class="col-12">
				<div class="row ml-auto mr-auto">
					<div class="col p-0 text-right">
					<input class="pesquisa-barra pesquisa-barra-m form-control" type="text" id="texto" value="<?= $filtro ?>" placeholder="Filtre pelo texto" /></div>
					<div class="w-b-l p-0 text-left">
					
						<a href="#" id="texto-btn">
							<div class="pesquisa-barra-lupa pesquisa-barra-lupa-m">
								<i class="fa fa-search mt-2 ml-2 text-white"></i>		
							</div>
						</a>
					
					</div>
				</div> 
	    </div>
			<div class="mt-3 mt-md-2 col-12 text-justify">O Exponencial Concursos possui os melhores cursos online para concursos. Nossa metodologia exclusiva é composta por muitas esquematizações e mapas mentais para que você consiga APRENDER de forma mais fácil, MEMORIZAR de forma mais efetiva, REVISAR de forma mais rápida.
			Confira todos os cursos disponíveis.</div>

   	</div>
		</div>
		
		</div>

		<div id="content" class="container pb-5">		
			<div class="row ml-auto mr-auto col-12">
			<?php woocommerce_content(); ?>
			</div>
		</div>
	
</div>	
<?php }
else{ ?>	
	<div class="woocommerce container pt-1 pt-md-4" id="container">
	<div id="content">
		<?php woocommerce_content(); ?>
	</div>
	</div>
<script>

</script>
<?php } ?>
<!-- woocommerce -->
<?php get_footer(); ?>