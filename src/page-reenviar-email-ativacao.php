<?php 
get_header(); 

$user = get_user_by('id', $_GET['u']);
//*****  Criação do código de ativação

$code = get_user_meta ( $user->ID, 'has_to_be_activated', true );
$activation_link = add_query_arg ( array (
		'key' => $code,
		'user' => $user->ID
), site_url( '/activation/' ) );

add_filter ( 'wp_mail_content_type', 'set_html_content_type' );
$to_user = $user->user_email;
$from = "contato@exponencialconcursos.com.br";
$headers_user = "";
$message_user = "<html>
						<head>
							<meta http-equiv='Content-Type' content='text/html;UTF-8' />
						</head>
						<body style='margin: 0px; background-color: #F4F3F4; font-family: Helvetica, Arial, sans-serif; font-size:12px;' text='#444444' bgcolor='#F4F3F4' link='#21759B' alink='#21759B' vlink='#21759B' marginheight='0' topmargin='0' marginwidth='0' leftmargin='0'>
							<table width='100%' border='0' cellspacing='0' cellpadding='0' bgcolor='#F4F3F4'>
								<tbody>
									<tr>
										<td style='padding: 15px;'><center>
											<table width='550' cellspacing='0' cellpadding='0' align='center' bgcolor='#ffffff'>
												<tbody>
													<tr>
														<td align='left'>
															<div style='border: solid 1px #d9d9d9;'>
																<img style='margin-bottom: 30px;' alt='Exponencial Concursos' src='http://www.exponencialconcursos.com.br/wp-content/themes/academy/images/logo.png' />
															</div>
														</td>
													</tr>
													<tr>
														<td colspan='2' style='padding:25px 0 0 0;'>
															<font style='font-size:14px;line-height:20px;' face='Arial' color='#373737'>
																Bem vindo ao Exponencial Concursos! Para ativar a sua conta, clique no link abaixo:
															</font>
															<p style='padding:5px 0 5px 10px;border-left:2px solid #cccccc;'>
																<font style='font-size:14px;line-height:20px;' face='Arial' color='#373737'><a href=" . $activation_link . " target='_blank'>" . $activation_link .

																"</a></font>
															</p>
														</td>
													</tr>
													<tr>
														<td colspan='2' style='padding:15px 0 0;'>
															<font style='font-size:14px;line-height:20px;' face='Arial' color='#373737'>
																Se não conseguir clicar no link, copie e cole o mesmo no seu navegador.
															</font>
														</td>
													</tr>
													<tr>
														<td align='left'>
															<div style='border: solid 1px #d9d9d9;'>
																<img class='alignnone size-full wp-image-4056' alt='cabecalho3' src='http://www.exponencialconcursos.com.br/wp-content/themes/academy/images/cabecalho3.jpg' width='1110' height='412' />
															</div>
														</td>
													</tr>
												</tbody>
											</table>
										</center></td>
									</tr>
								</tbody>
							</table>
						</body>
					</html>";

$subject_user = "Exponencial Concursos - Ative sua Conta!";
wp_mail ( $to_user, $subject_user, $message_user, "From: " . $subject_user . " <" . $from . ">\r\n" . "Reply-To: " . $email . "\r\n" . "X-Mailer: PHP/" . phpversion () );

function set_html_content_type() {
	return 'text/html';
}
?>

<center>
	<div class="register-success">Código de ativação enviado com sucesso. Para ativar a sua conta, acesse seu e-mail.</div><br/>
</center>

<p class="return-to-shop">
	<a class="btn u-btn-darkblue" href="/cadastro-login">Retornar à tela de login</a>
</p>

</br>
<?php get_footer(); ?>