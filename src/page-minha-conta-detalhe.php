<?php
KLoader::model("ProdutoModel");

redirecionar_se_nao_estiver_logado();

if(isset($_GET['p_id'])) {
    
    if(ProdutoModel::is_produto_premium($_GET['p_id'])) {
        KLoader::view("minha_conta/midep/midep");
    }
    else {
        KLoader::view("minha_conta/midet/midet");
    }
}
else {
    redirecionar_para_acesso_negado();
}