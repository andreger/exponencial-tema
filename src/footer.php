			</div>
			<?php
            global $post, $js_footer;

            use Exponencial\Core\Helpers\Template;

//            if(!UrlHelper::is_url_cadastro_login()) {
//                KLoader::view("login/modal");
//            }
				
				KLoader::view("login/modal_expiracao");
			?>

            <div id="login-modal-div"></div>
            <div id="cadastro-gratis" data-post-id="<?= $post->ID ?>"></div>
            <div id="rodape"></div>

			<?php wp_footer(); ?>

            <?php if( SHOW_ACTIVE_PLUGINS && is_administrador() ): ?>
                <div class="text-center p-3 text-info font-weight-bold">
                    <?php
                    FiltroPluginsConstantes::plugins_ativos();
                    ?>
                </div>
            <?php endif; ?>

            <?php
            $template = new Template();
            echo $template->render($js_footer ?? 'comum/footer/scripts.html.twig');
            ?>

			<?php KLoader::view("plugins/plugins") ?>
		</body>
		</html>