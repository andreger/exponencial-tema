<?php get_header();
$secao_barra_clara_user = 'secao-barra-clara';
    if($usuario_id = get_current_user_id()) {
      if(exibe_assinatura_bar($usuario_id)) {
        $secao_barra_clara_user = 'secao-barra-clara-ass-bar';
      }
   }
?>
<div class="d-block d-md-none bg-blue p-3">
			<div class="row ml-auto mr-auto">
			<div class="col-12 text-center">	
				<div class="col-12">		
				<img src="<?php echo get_tema_image_url('ss-mini.png')?>">
				</div>	
				<div class="col-12 mt-1">		
				<a class="t-d-none font-weight-bold text-white text-uppercase" href="#simulados" onclick="jQuery('#btn-todos').click()">Por Concurso</a>	
				</div>
				<div class="col-12">		
				<a class="t-d-none font-weight-bold text-white text-uppercase" id="barra-gratuito" href="#simulados" onclick="jQuery('#btn-gratuito').click()">Gratuitos</a>
				</div>
				<div class="col-12">
				<a class="t-d-none font-weight-bold text-white text-uppercase" href="#desempenho">Desempenho e Ranking</a>
				</div>	
				<div class="col-12">		
				<a class="t-d-none font-weight-bold text-white text-uppercase" href="#saiba-mais">Saiba Mais</a>
				</div>	
				<div class="col-12">		
				<a class="t-d-none font-weight-bold text-white text-uppercase" href="#depoimentos">Depoimentos</a>
				</div>			
			</div>
			</div>
		</div>
<div class="pt-3 pt-md-5 sistema-de-simulados text-center text-md-left">
	<div class="pt-1 pt-md-5 mt-0 mt-md-4 mb-4 text-blue container">
		<h1>Simulados</h1>
	</div>
	<div class="row-fluid">
		<div class="s-b-c d-none d-md-block border-top border-white bg-blue pt-3 pb-3 pr-2 pl-2 p-lg-3 <?= $secao_barra_clara_user ?> col-12">
			<div class="container row ml-auto mr-auto">
			<div class="col-12 p-0">			
				<img class="ml-0 mr-1 ml-lg-2 mr-lg-2" src="<?php echo get_tema_image_url('ss-mini.png')?>">			
				<a class="ml-lg-3 ml-md-0 mr-lg-3 mr-md-0 ml-1 mr-0 font-weight-bold text-white text-uppercase" href="#simulados" onclick="jQuery('#btn-todos').click()">Por Concurso</a>			
				<a class="ml-lg-3 ml-md-0 mr-lg-3 mr-md-0 ml-1 mr-0 font-weight-bold text-white text-uppercase" id="barra-gratuito" href="#simulados" onclick="jQuery('#btn-gratuito').click()">Gratuitos</a>
				<a class="ml-lg-3 ml-md-0 mr-lg-3 mr-md-0 ml-1 mr-0 font-weight-bold text-white text-uppercase" href="#desempenho">Desempenho e Ranking</a>			
				<a class="ml-lg-3 ml-md-0 mr-lg-3 mr-md-0 ml-1 mr-0 font-weight-bold text-white text-uppercase" href="#saiba-mais">Saiba Mais</a>			
				<a class="ml-lg-3 ml-md-0 mr-lg-0 mr-md-0 ml-1 mr-0 font-weight-bold text-white text-uppercase" href="#depoimentos">Depoimentos</a>			
			</div>
			</div>
		</div>					
	</div>

	<!-- <div class="secao-capa">
		<img src="<?php echo get_tema_image_url('ss-capa.jpg')?>">
	</div> -->
	
	<div class="secao-capa">
		<div class=""><?php putRevSlider('capa-ss') ?></div>
		<!-- <img src="<?php echo get_tema_image_url('sq-capa.jpg')?>"> -->
	</div>

	<a class="anchor" id="simulados"></a>
	<a class="anchor" id="gratuitos"></a>
	<div class="container">	
	<div class="col-12 text-center font-pop mb-3 mt-4">
    Simulados por Concursos
	</div>	
			<input type="hidden" id="p" value="0">
			<input type="hidden" id="g" value="0">			
		<div class="p-l-r col-md-6 offset-md-3">
		<div class="row mb-3 mt-3">
			<div class="col p-0 text-right">
			<input class="pesquisa-barra form-control" type="text" id="search" placeholder="Procure pelo nome do concurso"></div>
			<div class="w-b-l p-0 text-left">
			
				<div class="pesquisa-barra-lupa">
					<i class="fa fa-search mt-2 ml-2 text-white"></i>		
				</div>
			
			</div>
		</div>    	
    	</div>


			<div class="row ml-auto mr-auto mt-4 pt-2">			
				<div class="col-12 col-md-6 text-left text-darkblue font-12 font-weight-bold" id="num_simulados">			
				</div>
				<div class="col-12 col-md-6 text-right">	
				<a class="btn u-btn-blue font-13 mb-2 mb-sm-0" href="#" id="btn-todos" onclick="event.preventDefault(); jQuery('#g').val(0); filtrar();">			<span class="font-weight-bold">Todos</span> os simulados
				</a>				
				<a class="btn u-btn-primary font-13 mb-2 mb-sm-0" href="#" id="btn-gratuito" onclick="event.preventDefault(); jQuery('#g').val(1); filtrar();">			Simulados <span class="font-weight-bold">gratuitos</span>
				</a>	
			</div>
			</div>

		<div class="row ml-auto mr-auto products mt-4"></div>		
			<?php if(!$ultima_pagina) : ?>
			<div class="text-center mb-5 mt-5" id="ver-mais"> 
				<a class="ver-mais-simulados" href="#">CARREGAR MAIS</a>
			</div>
			<?php endif ?>		
	</div>

	<a class="anchor" id="desempenho"></a>
<div class="container-fluid bg-gray pb-5">	
	<div class="col-12 text-center font-pop mb-3 pt-4">
    Desempenho e Ranking
	</div>
	<div class="container">
	<div class="row ml-auto mr-auto mt-5">		
			<div class="col-12 col-md-4 text-center">
				<img class="img-fluid" width="45%" src="<?php echo get_tema_image_url('ss-g1.png')?>">
				<div class="mt-3 text-uppercase font-10 font-weight-bold">Desempenho por concurso</div>
			</div>		
			<div class="col-12 col-md-4 text-center">
				<img class="img-fluid" width="45%" src="<?php echo get_tema_image_url('ss-g2.png')?>">
				<div class="mt-3 text-uppercase font-10 font-weight-bold">Aproveitamento global</div>
			</div>
			<div class="col-12 col-md-4 text-center">
				<img class="img-fluid" width="85.5%" src="<?php echo get_tema_image_url('ss-g3.png')?>">
				<div class="mt-3 text-uppercase font-10 font-weight-bold">Ranking de simulados</div>
			</div>
	</div>
</div>
</div>
	<a class="anchor" id="saiba-mais"></a>
	<div class="col-12 text-center font-pop mb-3 mt-4">
    Saiba Mais
	</div>	
	<div class="pb-5"><?php putRevSlider('saiba-mais-ss') ?></div>
	<a class="anchor" id="depoimentos"></a>

<div class="container-fluid pb-5 pl-0 pr-0">		
	<div class="col-12 text-center font-pop mb-3 pt-4">
    Depoimentos
	</div>	
	<div><?php putRevSlider('depoimentos-sqs') ?></div>
</div>	
</div>

<script>
jQuery().ready(function() {
	jQuery("#search").keyup( function () {
		filtrar();
	}).keydown (function(e) {
		if(e.keyCode == 13) {
			return false;
		}
	});
	
	jQuery("body").on("click", ".ver-mais-simulados", function (e) {
		e.preventDefault();
		jQuery(".ver-mais-simulados").text("Carregando...");
		jQuery("#p").val(parseInt(jQuery("#p").val(), 10) + 1);
		filtrar(true);
	});

	if(window.location.hash == "#gratuitos") {
		jQuery('#g').val(1); 
	}

	filtrar();
	contar_simulados();
});

var delayTimer;
function filtrar(mais) {

	if(mais == undefined) {
		jQuery("#p").val(0);
		jQuery("#ver-mais").hide();
		jQuery("#num_simulados").text("Carregando...");
		jQuery(".products").html("<div style='margin: 20px 0; text-align: center'>Carregando...</div>");
	}

    clearTimeout(delayTimer);
    delayTimer = setTimeout(function() {
        
        var p = jQuery("#p").val();
		var g = jQuery("#g").val();
		var t = jQuery("#search").val();

		var url = "/wp-content/themes/academy/ajax/listar_simulados.php?p="+p+"&g="+g+"&t="+t;
		jQuery.get(url, function(data) {

			if(mais) {
				jQuery(".products").append(data);
			}
			else {
				jQuery(".products").html(data);	
			}

			if(jQuery.trim(data) != "") {
				jQuery(".ver-mais-simulados").text("CARREGAR MAIS");
			}
			else {
				jQuery(".ver-mais-simulados").text("");
			}

			contar_simulados();
		});
    }, 1000);

}

function contar_simulados()
{
	var g = jQuery("#g").val();
	var t = jQuery("#search").val();

	var url = "/wp-content/themes/academy/ajax/contar_simulados.php?g="+g+"&t="+t;
	jQuery.get(url, function(data) {

		var num = parseInt(data, 10);

		if(num == 0) {
			var texto = "Nenhum simulado encontrado";
		}
		else if(num == 1) {
			var texto = "Mostrando 1 simulado";	
		}
		else {
			var texto = "Mostrando " + num + " simulados";
		}

		jQuery("#num_simulados").html(texto);
		jQuery("#num_simulados").show();

		console.log(texto);

		if(jQuery('.products li').length == num) {
			jQuery("#ver-mais").hide();
		}
		else {
			jQuery("#ver-mais").show();	
		}
	});
		
}
</script>
<?php get_footer() ?>