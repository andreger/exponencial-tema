<?php
define('ADMINISTRADOR', 1);
define('PROFESSOR', 2);
define('CONSULTOR', 3);
define('ALUNO', 4);
define('ALUNO_COACH', 5);
define('ASSINANTE_SQ', 6);
define('VISITANTE', 7);
define('REVISOR', 8);
define('USUARIO_LOGADO', 9);
define('COORDENADOR', 10);
define('COORDENADOR_SQ', 11);
define('COORDENADOR_AREA', 12);
define('COORDENADOR_COACHING', 13);
define('APOIO', 14);
define('ATENDENTE', 15);
define('PARCEIRO_COACHING', 16);

define('ACESSO_NEGADO', '/acesso-negado');
define('ACESSO_NEGADO_SQ', '/acesso-negado-ao-sistema-de-questoes');

function tem_acesso($permitidos, $redirecionar = null)
{
	$user_id = get_current_user_id();

	foreach ($permitidos as $permitido) {

		switch ($permitido) {

			case ADMINISTRADOR: {
				if(is_administrador()) {
					return true;
				}
				break;
			}

			case COORDENADOR: {
				if(is_coordenador()) {
					return true;
				}
				break;
			}

			case COORDENADOR_SQ: {
				if(is_coordenador_sq()) {
					return true;
				}
				break;
			}

			case COORDENADOR_AREA: {
				if(is_coordenador_area()) {
					return true;
				}
				break;
			}

			case COORDENADOR_COACHING: {
				if(is_coordenador_coaching()) {
					return true;
				}
				break;
			}

			case PROFESSOR: {
				if(is_professor()) {
					return true;
				}
				break;
			}

			case CONSULTOR: {
				if(is_consultor()) {
					return true;
				}
				break;
			}

			case ALUNO: {
				if (is_aluno()) {
					return true;
				}
				break;
			}

			case ALUNO_COACH: {
				if(is_aluno_ativo_podio($user_id)) {
					return true;
				}
				break;
			}

			case ASSINANTE_SQ : {
				if (is_assinante_sq()) {
					return true;
				}
				break;
			}

			case REVISOR : {
				if (is_revisor_sq()) {
					return true;
				}
				break;
			}

			case APOIO : {
				if (is_apoio()) {
					return true;
				}
				break;
			}

			case ATENDENTE : {
				if (is_atendente()) {
					return true;
				}
				break;
			}

			case VISITANTE : {
				if (is_visitante()) {
					return true;
				}
				break;
			}

			case USUARIO_LOGADO : {
				if (is_usuario_logado()) {
					return true;
				}
				break;
			}
			
			case PARCEIRO_COACHING : {
				if (is_parceiro_coaching()) {
					return true;
				}
				break;
			}
		}

	}

	if($redirecionar) {

		if(!is_user_logged_in()) {
			$redirecionar = '/acesso-restrito/?uri=' . $_SERVER['REQUEST_URI'];
		}

		header("Location: $redirecionar");
		exit;
	}

	return false;
}

function is_usuario_logado($user_id = null) {
	if(!get_current_user_id()) {
		return false;
	}

	if(is_null($user_id)) {
		$user_id = get_current_user_id();
	}

	return $user_id == get_current_user_id();
}

function is_assinante_sq($user_id = null)
{
	if(is_null($user_id)) {
		$user_id = get_current_user_id();
	}

	if(date('Y-m-d') <= get_user_meta($user_id, 'assinante-sq-validade', true) || (is_aluno_ativo_podio($user_id))) {
		return true;
	}

	return false;
}

function is_professor($user_id = null, $visivel = null)//TODO: esse método faz um cache sem expiração que pode não fazer sentido quando a visibilidade foi alterada mas o cache não
{
	if(is_null($user_id)) {
		$user_id = get_current_user_id();
	}

	$mcache_id = MCACHE_IS_PROFESSOR . $user_id ;
	$mcache = wp_cache_get($mcache_id);

	if($mcache === false) {

    	$tipo = get_user_meta($user_id, 'oneTarek_author_type', true);

    	if($tipo == 1 || $tipo == 3) {

    	    if(!is_null($visivel)) {
    	        $mcache = get_user_meta($user_id, 'ocultar', true) == 1 ? 0 : 1;
    	    }

    	    $mcache = 1;
    	}
    	else {
    	   $mcache = 0;
    	}

    	wp_cache_set($mcache_id, $mcache);
	}

	return $mcache;
}

function is_coordenador($user_id = null)
{
	if(is_null($user_id)) {
		$user_id = get_current_user_id();
	}

	if(is_coordenador_area($user_id) || is_coordenador_sq($user_id) || is_coordenador_coaching($user_id)) {

		return TRUE;
	}



	return FALSE;
}

function is_coordenador_sq($usuario_id = null)
{
	global $wpdb;

	if(is_null($usuario_id)) {
		$usuario_id = get_current_user_id();
	}

	$mcache_id = MCACHE_IS_COORDENADOR_SQ . $usuario_id ;
	$mcache = wp_cache_get($mcache_id);

	if($mcache === false) {
	    $sql = "SELECT * FROM coordenadores_sq WHERE user_id = $usuario_id AND data_fim IS NULL";

	    $resultado = $wpdb->get_results($sql);

	    $mcache = $resultado ? 1 : 0;

	    wp_cache_set($mcache_id, $mcache);
	}

	return $mcache;
}

function is_coordenador_area($usuario_id = null)
{
	global $wpdb;

	if(is_null($usuario_id)) {
		$usuario_id = get_current_user_id();
	}

	$mcache_id = MCACHE_IS_COORDENADOR_AREA . $usuario_id ;
	$mcache = wp_cache_get($mcache_id);

	if($mcache === false) {
    	$sql = "SELECT * FROM coordenadores_areas WHERE user_id = $usuario_id AND data_fim IS NULL";

    	$resultado = $wpdb->get_results($sql);

    	$mcache = $resultado ? 1 : 0;

    	wp_cache_set($mcache_id, $mcache);
	}

	return $mcache;
}

function is_coordenador_coaching($usuario_id = null)
{
	global $wpdb;

	if(is_null($usuario_id)) {
		$usuario_id = get_current_user_id();
	}

	$mcache_id = MCACHE_IS_COORDENADOR_COACHING . $usuario_id ;
	$mcache = wp_cache_get($mcache_id);

	if($mcache === false) {
    	$sql = "SELECT * FROM coordenadores_coaching WHERE user_id = $usuario_id AND data_fim IS NULL";

    	$resultado = $wpdb->get_results($sql);

    	$mcache = $resultado ? 1 : 0;

    	wp_cache_set($mcache_id, $mcache);
	}

	return $mcache;
}


function is_consultor($user_id = null)
{
	if(is_null($user_id)) {
		$user_id = get_current_user_id();
	}

	$tipo = get_user_meta($user_id, 'oneTarek_author_type', true);

	if($tipo == 2 || $tipo == 3) {
		return true;
	}

	return false;
}

function is_administrador($user_id = null)
{
	if(is_null($user_id)) {
		$user_id = get_current_user_id();
	}

	$mcache_id = MCACHE_IS_ADMINISTRADOR . $user_id ;
	$mcache = wp_cache_get($mcache_id);

	if($mcache === false) {
	    $mcache = user_can($user_id, 'administrator') ? 1 : 0;
	}

	return $mcache;
}

/**
 * Role para Revisor de SQ
 * Acesso no WP igual ao Aluno, porém no SQ tem acesso igual ao Admin
 *
 * Verificação de role feita conforme sugerido aqui:
 * https://core.trac.wordpress.org/ticket/22624
 *
 * @since J1
 *
 * @param int $user_id
 *
 * @return bool
 */

function is_revisor_sq($user_id = null) {

	$user = ( $user_id ) ? get_userdata( $user_id ) : wp_get_current_user();

	if ( in_array( 'revisor-sq', (array) $user->roles ) ) {
		return true;
	}
	return false;
}

/**
 * Role para Apoio
 *
 * Verificação de role feita conforme sugerido aqui:
 * https://core.trac.wordpress.org/ticket/22624
 *
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2412
 *
 * @since K2
 *
 * @param int $user_id
 *
 * @return bool
 */

function is_apoio($user_id = null) {

	$user = ( $user_id ) ? get_userdata( $user_id ) : wp_get_current_user();

	if ( in_array( 'apoio', (array) $user->roles ) ) {
		return true;
	}
	return false;
}

/**
 * Role para Atendente
 *
 * Verificação de role feita conforme sugerido aqui:
 * https://core.trac.wordpress.org/ticket/22624
 *
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2412
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2228
 *
 * @since K3
 *
 * @param int $user_id
 *
 * @return bool
 */

function is_atendente($user_id = null) {

	$user = ( $user_id ) ? get_userdata( $user_id ) : wp_get_current_user();

	if ( in_array( 'atendente', (array) $user->roles ) ) {
		return true;
	}
	return false;
}



function is_aluno($user_id = null)
{
	if(is_null($user_id)) {
		$user_id = get_current_user_id();
	}

	if(user_can($user_id, 'customer') || user_can($user_id, 'aluno')) {
		return true;
	}

	return false;
}

function is_visitante()
{
	if(!is_user_logged_in()) {
		return true;
	}
	return false;
}

/**
 * Role para Parceiro Coaching
 *
 * Verificação de role feita conforme sugerido aqui:
 * https://core.trac.wordpress.org/ticket/22624
 *
 * @since L1
 *
 * @param int $user_id
 *
 * @return bool
 */

function is_parceiro_coaching($user_id = null) {

	$user = ( $user_id ) ? get_userdata( $user_id ) : wp_get_current_user();

	if ( in_array( 'parceiro_coaching', (array) $user->roles ) ) {
		return true;
	}
	return false;
}

function verifica_login_disponivel($username){

	if(is_area_desativada(array(PAINEL_LOGIN))){

		if (username_exists( $username)) {
			$userinfo = get_user_by( 'login', $username );
			if(is_administrador($userinfo->ID)){
				return;
			}
		}

		if (email_exists( $username)) {
			$userinfo = get_user_by( 'email', $username );
			if(is_administrador($userinfo->ID)){
				return;
			}
		}

		redirecionar_erro_500();
	}

	/*if(!is_administrador($user->ID)
		&& is_area_desativada(array(PAINEL_LOGIN))){
		wp_logout();
		exit();
	}*/
}
add_action('wp_authenticate', 'verifica_login_disponivel');

/*add_action ( 'wp_logout', 'go_500');
function go_500() {
	if(is_area_desativada(array(PAINEL_LOGIN))){
		redirecionar_erro_500();
		exit();
	}
}*/