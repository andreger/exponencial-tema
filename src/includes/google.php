<?php
function google_pixel($valor, $order_id) {

    $pedido = new WC_Order($order_id);
    $total = $pedido->get_total();
    
	$html = 
		"<script>
			ga('require', 'ecommerce', 'ecommerce.js');
			ga('ecommerce:addTransaction', {
				id: '{$order_id}',
				affiliation: 'Exponencial Concursos',
				revenue: '{$total}',
				shipping: '0',
				tax: '0' });";
	
	

	foreach ($pedido->get_items() as $item) {
		if(is_item_de_pacote($item)) {
			continue;
		}

		$produto = new WC_Product($item['product_id']);
		$id = $produto->get_id();
		$nome = $produto->get_name();
		$preco = $item['line_subtotal'];
		$sku = $produto->get_sku();
		$qtde = $item['quantity'];

		$html .= 
			"ga('ecommerce:addItem', {
				id: '{$id}',
				sku: '{$sku}',
				price: '{$preco}',
				name: '{$nome}'
				quantity: '{$qtde}'});";
	}

	$html .= "ga('ecommerce:send');
		</script>";

	return $html;
}

function google_analytics()
{
	return 
		"<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
			
			ga('create', 'UA-57123111-1', 'auto');
			ga('send', 'pageview');
			
		</script>";
}

function gtm_geral($nome_pagina, $produtos)
{
	$html_p = [];
	foreach ($produtos as $produto) {
		$produto['valor'] = number_format($produto['valor'], 2, ".", "");

		array_push($html_p, "{
			'ecomm_prodid': '{$produto['id']}',
			'ecomm_totalvalue': '{$produto['valor']}'
			}");
	}

	return "<script>
		dataLayer = [{
		'ecomm_pagetype': '$nome_pagina',
		'products': [" . implode(",", $html_p) . "]
		}];
		</script>";
}

function gtm_agradecimento()
{
	global $woocommerce, $post;
	
	$pedido_id = get_order_id_by_url();

	$pedido = new WC_Order($pedido_id);
	$total = $pedido->get_total();

	$html_p = [];
	foreach ($pedido->get_items() as $item) {
		$p = get_product($item['product_id']);
		$sku = $p->get_id();
		$name = $p->get_name();
		$price = number_format($p->get_price(), 2, ".", "");

		array_push($html_p, "{
			'sku': '{$sku}',
			'name': '{$name}',
			'price': {$price},
			'quantity': 1
			}");
	}

	return "<script>
			dataLayer = [{
			'ecomm_pagetype': 'purchase',
			'transactionId': '{$pedido_id}',
			'transactionAffiliation': 'Exponencial Concursos',
			'transactionTotal': {$total},
			'transactionProducts': [" . implode(",", $html_p) . "]
		}];
		</script>";
}

function gtm_pagina()
{
	KLoader::helper("UrlHelper");

	if(is_pagina_cursos_online()) {
		return gtm_pagina_cursos_online();
	}

	if(UrlHelper::is_url_cursos_por_concurso_especifico()) {
		return gtm_pagina_concurso();
	}

	if(UrlHelper::is_url_produto()) {
		return gtm_pagina_produto();
	}

	if(is_pagina_carrinho()) {
		return gtm_pagina_carrinho();
	}

	if(is_pagina_agradecimento()) {
		return gtm_agradecimento();
	}

	if(is_pagina_checkout()) {
		return gtm_pagina_checkout();
	}

	if(is_pagina_sq_meus_simulados()) {
		return gtm_pagina_sq_meus_simulados();
	}
	
}

function gtm_pagina_cursos_online() 
{
	KLoader::model("ProdutoModel");

	$p = [];
	$produtos = ProdutoModel::listar_para_gtm();
	foreach ($produtos as $produto) {
		array_push($p, [
			'id' => $produto->post_id,
			'valor' => $produto->pro_preco
		]);
	}

	return gtm_geral('other', $p);
}

function gtm_pagina_concurso() 
{
    global $concurso;

    $cacheKey = "expo:concurso:{$concurso->ID}:gtm";

    $cache = get_memcached_entry($cacheKey);

    if(!$cache) {
        $nome_concurso = get_query_var('nome');

        $destaques = get_destaques_por_categoria($nome_concurso);
        $pacotes = get_pacotes_por_categoria($nome_concurso, true);
        $cursos = get_cursos_por_categoria($nome_concurso, false, false, true);
        $pacotes_simulados = get_simulados_por_categoria($nome_concurso, false, TIPO_SIMULADO_SOMENTE_PACOTES);
        $simulados = get_simulados_por_categoria($nome_concurso, false, TIPO_SIMULADO_SOMENTE_CURSOS);

        $p = [];
        foreach ($destaques as $item) {
            array_push($p, [
                'id' => $item->get_id(),
                'valor' => $item->get_price()
            ]);
        }

        foreach ($pacotes as $item) {
            array_push($p, [
                'id' => $item->get_id(),
                'valor' => $item->get_price()
            ]);
        }

        foreach ($cursos as $item) {
            array_push($p, [
                'id' => $item->get_id(),
                'valor' => $item->get_price()
            ]);
        }

        foreach ($pacotes_simulados as $item) {
            array_push($p, [
                'id' => $item->get_id(),
                'valor' => $item->get_price()
            ]);
        }

        foreach ($simulados as $item) {
            array_push($p, [
                'id' => $item->get_id(),
                'valor' => $item->get_price()
            ]);
        }

        $cache = gtm_geral('category', $p);
        set_memcached_entry($cacheKey, $cache);
    }

    return $cache;
}

function gtm_pagina_produto() 
{
	global $product;
	
	//Quando o slug da página não existe a variável 'product' é uma string com o slug informado
	//mas esperasse que seja um objeto WC_Product
	if($product && gettype($product) == "object") {
		$p = [];
		array_push($p, [
			'id' => $product->get_id(),
			'valor' => $product->get_price()
		]);

		return gtm_geral('product', $p);
	}
}

function gtm_carrinho_checkout_produtos() 
{
	$p = [];

	foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {

		$product = get_product($cart_item['product_id']);

		array_push($p, [
			'id' => $product->get_id(),
			'valor' => $product->get_price()
		]);
	}

	return $p;
}

function gtm_pagina_carrinho() 
{
	return gtm_geral('cart', gtm_carrinho_checkout_produtos());
}

function gtm_pagina_checkout() 
{
	return gtm_geral('cart', gtm_carrinho_checkout_produtos());
}

function gtm_pagina_sq_meus_simulados() 
{
    KLoader::model("SimuladoModel");
    
    $simulados = Simulado_model::listar_todos_produtos_simulados();

	if($simulados) {
		$p = [];
		foreach ($simulados as $item) {
			try {
				$produto = get_produto_by_id($item->prd_id); 

				array_push($p, [
					'id' => $item->prd_id,
					'valor' => $produto->get_price()
				]);
			}
			catch(Exception $e) {
				continue;
			}

			
		}

		return gtm_geral('product', $p);
	}
}
