<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/corporativo/imposto.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/corporativo/item_coaching.php';

function salvar_valor_imposto($valor, $data_inicio) {
	$imposto = array(
			'imp_valor' => $valor,
			'imp_data_inicio' => $data_inicio
	);
	Imposto::salvar($imposto);
}

function get_valor_atual() {
	$imposto = Imposto::get_imposto_atual();
	return $imposto['imp_valor'];
}

function get_imposto_por_data($data) {
	$imposto = Imposto::get_imposto_por_data($data);
	return $imposto['imp_valor'];
}

function listar_impostos()
{
	return Imposto::listar_impostos();
}

// TODO: Melhorar o nome desse arquivo

function salvar_item_coaching($slug, $tipo) {
	$item = array('ite_slug' => $slug, 'ite_tipo' => $tipo);
	ItemCoaching::salvar($item);
}

function listar_items_coaching()
{
	return ItemCoaching::listar_todos();
}

function listar_items_cupons_coaching()
{
	return ItemCoaching::listar_cupons();
}

function excluir_item_coaching($slug) {
	ItemCoaching::excluir($slug);
	
	$post = get_cupom_by_post_name($slug);
	update_post_meta($post->ID, 'customer_email', "cupom@exponencialconcursos.com.br");
}

?>