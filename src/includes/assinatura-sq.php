<?php
function listar_assinaturas_sq()
{
	global $wpdb;
	
	$sql = "SELECT * FROM wp_postmeta WHERE meta_key = '". ASSINANTE_SQ_META . "'";
	return $wpdb->get_results($sql);
}

function listar_assinaturas_sq_validades()
{
	global $wpdb;

	$sql = "SELECT * FROM wp_postmeta WHERE meta_key = '". ASSINANTE_SQ_VALIDADE_META . "'";
	return $wpdb->get_results($sql);
}

function get_produto_assinatura_mensal()
{
	$produtos = get_cursos_por_categoria('assinatura-sq-mensal');
	
	if($produtos) {
		return $produtos[0];
	}
	
	return null;
}

function get_produto_assinatura_trimestral()
{
	$produtos = get_cursos_por_categoria('assinatura-sq-trimestral');

	if($produtos) {
		return $produtos[0];
	}

	return null;
}

function get_produto_assinatura_semestral()
{
	$produtos = get_cursos_por_categoria('assinatura-sq-semestral');

	if($produtos) {
		return $produtos[0];
	}

	return null;
}

function get_produto_assinatura_anual()
{
	$produtos = get_cursos_por_categoria('assinatura-sq-anual');

	if($produtos) {
		return $produtos[0];
	}

	return null;
}

function get_assinatura_validade($usuario_id)
{
	return get_user_meta($usuario_id, 'assinante-sq-validade', TRUE);
}
