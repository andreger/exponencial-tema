<?php

/**
 * Conta posts que tenha determinado conteúdo na coluna post_content
 * 
 * @since K1
 * 
 * @param $conteudo string
 * 
 * @return int
 */

function contar_posts_com_conteudo($conteudo, $tipo = null, $exclude = null)
{
	global $wpdb;

	$where = $tipo ? " AND post_type = '{$tipo}'" : "";
	
	if($exclude)
	{
	   //Acredito que o DB seja inteligente só faça o replace após o primeiro filtro, senão haverá problema de performance
	   $where .= " AND replace(post_content, '{$exclude}', '') LIKE '%{$conteudo}%'";    
	}

	return $wpdb->get_var("SELECT COUNT(*) FROM wp_posts WHERE post_content LIKE '%{$conteudo}%' {$where}");
}

/**
 * Lista posts que tenha determinado conteúdo na coluna post_content
 * 
 * @since K1
 * 
 * @param $conteudo string
 * 
 * @return array
 */

function listar_posts_com_conteudo($conteudo, $tipo = null, $exclude = null)
{
	global $wpdb;
	
	$where = $tipo ? " AND post_type = '{$tipo}'" : "";
	
	if($exclude)
	{
	    //Acredito que o DB seja inteligente só faça o replace após o primeiro filtro, senão haverá problema de performance
	    $where .= " AND replace(post_content, '{$exclude}', '') LIKE '%{$conteudo}%'";
	}
	
	return $wpdb->get_results("SELECT * FROM wp_posts WHERE post_content LIKE '%{$conteudo}%'" .$where );
}

/**
 * Substitui conteúdo antigo pelo novo em todos os posts, coluna post_content
 * 
 * @since K1
 * 
 * @param $antigo string
 * @param $novo string
 */

function replace_conteudo_de_posts($antigo, $novo)
{
	global $wpdb;

	$wpdb->query("UPDATE wp_posts SET post_content = REPLACE(post_content, '{$antigo}', '{$novo}') WHERE post_content LIKE '%{$antigo}%'");
}

/**
 * Lista posts de um determinado tipo
 * 
 * @since K2
 * 
 * @param $tipo string
 * 
 * @return array
 */

function listar_posts_por_tipo($tipo, $limit = null, $offset = null)
{
	global $wpdb;

	$tipo_str = implode("','", $tipo);
	$tipo_str = "'". $tipo_str . "'";
	$limit_offset = $limit ? " LIMIT {$offset}, {$limit} " : "";

	return $wpdb->get_results("SELECT ID FROM wp_posts WHERE post_type IN ($tipo_str) and post_status= 'publish' {$limit_offset}");
}

function contar_posts_por_tipo($tipo)
{
    global $wpdb;
    
    $tipo_str = implode("','", $tipo);
    $tipo_str = "'". $tipo_str . "'";
    
    return $wpdb->get_var("SELECT COUNT(*) AS qtde FROM wp_posts WHERE post_type IN ($tipo_str) and post_status= 'publish' ");
}

function get_post_id_by_slug($slug) {
    global $wpdb;

	$sql = "SELECT ID FROM wp_posts WHERE post_name = '{$slug}'";

    return $wpdb->get_var($sql);
}

function listar_todos_blog_posts()
{
	global $wpdb;

	return $wpdb->get_results("SELECT ID FROM wp_posts WHERE post_type = 'post' AND post_status = 'publish' ");
}