<?php
/*
add_shortcode('home_midia', 'home_midia_func');
function home_midia_func()
{
	return get_midias_componente();
}

function get_midias_componente($tipos = "news,articles,videos")
{
	$data['midias'] = get_posts("category_name={$tipos}&showposts=6&orderby=DESC");

	extract($data);

	ob_start();
	include get_stylesheet_directory() . '/paginas/home_midia.php';
	return ob_get_clean();
}
*/
function get_pagina($filename, $data = null)
{
	if($data) {
		extract($data);
	}

	ob_start();
	include get_stylesheet_directory() . '/paginas/' . $filename;
	return ob_get_clean();
}

function get_midias($tipos = "news,articles,videos", $limit = -1)
{
	return get_posts("category_name={$tipos}&showposts={$limit}&orderby=DESC");
}

function get_midia_categorias($post_id)
{
	$categorias = get_the_category($post_id);

	$resultado = array();
	foreach ($categorias as $categoria) {
		if($categoria->slug == NOTICIAS) {
			$titulo = "Notícia";
			$estilo = "midia-noticia-blue";
		}

		if($categoria->slug == ARTIGOS) {
			$titulo = "Artigo";
			$estilo = "midia-artigo-yellow";
		}

		if($categoria->slug == VIDEOS) {
			$titulo = "Vídeo";
			$estilo = "midia-video-red";
		}

		if(isset($titulo) && $titulo) {
			$resultado[$estilo] = $titulo;
		}
	}

	return $resultado;
}

function get_midia_categorias_ids($post_id)
{
	$categorias = get_the_category($post_id);

	$resultado = [];
	foreach ($categorias as $categoria) {
		if(in_array($categoria->term_id, [1,2,14])) {
			$resultado[] = $categoria->term_id;
		}
	}

	return $resultado;
}

function get_midia_categoria($post_id)
{
	$categorias = get_the_category($post_id);

	foreach ($categorias as $categoria) {
		if($categoria->slug == NOTICIAS) return "Notícia";
		if($categoria->slug == ARTIGOS) return "Artigo";
		if($categoria->slug == VIDEOS) return "Vídeo";
	}

	return "";
}

function get_midia_class($post_id)
{
	$categorias = get_the_category($post_id);

	foreach ($categorias as $categoria) {
		if($categoria->slug == NOTICIAS) return "midia-noticia";
		if($categoria->slug == ARTIGOS) return "midia-artigo";
		if($categoria->slug == VIDEOS) return "midia-video";
	}

	return "";
}

function is_midia_noticia($post_id)
{
	$categorias = get_the_category($post_id);

	foreach ($categorias as $categoria) {
		if($categoria->slug == NOTICIAS) return true;
	}

	return false;
}

function is_midia_artigo($post_id)
{
	$categorias = get_the_category($post_id);

	foreach ($categorias as $categoria) {
		if($categoria->slug == ARTIGOS) return true;
	}

	return false;
}

function is_midia_video($post_id)
{
	$categorias = get_the_category($post_id);

	foreach ($categorias as $categoria) {
		if($categoria->slug == VIDEOS) return true;
	}

	return false;
}

function get_woocommerce_sucesso_box($mensagem)
{
	return "<div class='woocommerce-message w-100'>{$mensagem}</div>";
}

function get_woocommerce_erro_box($mensagem)
{
	return "<div class='woocommerce-error w-100'>{$mensagem}</div>";
}

function include_componente($componente)
{
	include $_SERVER['DOCUMENT_ROOT'] . "/wp-content/themes/academy/componentes/{$componente}.php";
}

function get_relatorio_ordenacao_combo($selected = 3, $prf = true)
{
	$options = array(
			1 => "A - Z",
			2 => "Z - A",
			3 => "Maior Valor",
			4 => "Menor Valor",
			5 => $prf ? "Maior Total Professor" : "Maior Total Pago",
			6 => $prf ? "Menor Total Professor" : "Menor Total Pago",
	);

	$combo = "<select name='ordem' class='custom-select input_field combo-ordem'>";

	foreach ($options as $key => $value) {
		$is_selected = $selected == $key ? 'selected=selected' : '';
		$combo .= "<option value='{$key}' {$is_selected}>{$value}</option>";
	}

	$combo .=  "</select>";

	return $combo;
}

/**
 * Carrega o css e js do Remodal
 *
 * @see https://github.com/vodkabears/Remodal
 *
 * @since 10.1.0
 */

function carregar_remodal()
{
	wp_enqueue_style('remodal.min', get_template_directory_uri() . '/css/remodal.css');
	wp_enqueue_style('remodal-default-theme', get_template_directory_uri() . '/css/remodal-default-theme.css');
	wp_enqueue_script('remodal.min', get_template_directory_uri() . '/js/remodal.min.js', array('jquery'));
}

/**
 * Caixa padrão do curso
 *
 * @since 10.1.0
 *
 * @param WC_Product $produto Produto do WP
 *
 * @return string HTML do componente
 */

function curso_box($produto)
{
	KLoader::helper("UrlHelper");
	// $post = $produto->post;
	$titulo = wp_trim_words($produto->post_title, 14);
	$url = UrlHelper::get_produto_url($produto->post_name);
	$imagem_src = get_the_post_thumbnail($produto->post_id, array(285,149));
	$preco = moeda($produto->pro_preco);
	$autores = "";wp_trim_words(get_autores_str($post), 5);

	if($produto == null){
		 return null;
	}

	else{
		return
			"<div>
				<div class='border rounded p-2 box-cursos-professor'>
					<div class='bg-img-size-curso bg-light border'>
					<a href='{$url}'>
					{$imagem_src}
					</a>
					<div class='flt-left pt-1 pb-1 pr-3 pl-3 rounded'>{$preco}</div>
					</div>
					<div class='p-1'>
						<a class='titulo-professores' href='$url'>{$titulo}</a>
						<p class='font-10'>{$autores}</p>
					</div>
				</div>
			</div>";
		}
	}

/**
 * Caixa padrão de mídias
 *
 * @since 10.1.0
 *
 * @param WP_Post $post Post do WP
 * @param string $tipo Tipo da mídia [Artigo, Notícia, Vídeo]
 *
 * @return string HTML do componente
 */

function midia_box($blog)
{
	KLoader::helper("UrlHelper");
	KLoader::helper("BlogHelper");
	KLoader::helper('UiLabelHelper');
	KLoader::model("BlogModel");

	$titulo = $blog->post_title;
	$url = UrlHelper::get_blog_post_url($blog);
	$imagem_src = $blog->blo_thumb;
	$resumo = $blog->blo_resumo;
	$leia_mais = get_tema_image_url('botao-leia-mais.png');
	$categorias = BlogModel::listar_tipos($blog->blo_id);

	$autores = null;
	if(BlogHelper::is_blog_tipo(TIPO_ARTIGO, $categorias)) {
		$autor_url = UrlHelper::get_professor_url($blog->col_slug);
		$autores = "<a href='{$autor_url}'>{$blog->display_name}</a>";
	}

	$tipos .= UiLabelHelper::get_blog_label_html($blog->blo_id);

	if($blog == null){
		 return null;
	}

	else{
	return

		"<div class='border rounded p-2 box-artigos'>
			<div class='bg-light border bg-img-size'>
				<a href='{$url}'>
					<img src='{$imagem_src}'>
				</a>
				<div>{$tipos}</div>
			</div>
			<div class='p-2'>
				<div class='p-1'>
					<a class='titulo-professores' href='{$url}'>{$titulo}</a>
					<div class='font-10 t-d-none text-blue'>{$autores}</div>
					<div class='font-10'>{$resumo}</div>
				</div>
			</div>
			<div class='col-11 text-center ml-auto mr-auto leia-mais-absolute mb-2'>
				<a class='ml-3 btn u-btn-primary' href='{$url}'>Leia mais</a>
			</div>
		</div>";

	}

}


/**
 * Botões de redes sociais de professores
 *
 * @since 10.1.0
 *
 * @param int $usuario_id Id do usuário
 *
 * @return string HTML do componente
 */

function social_box($professor)
{
	$html = "";

	if($url = $professor->col_linkedin) {
		$img_src = get_tema_image_url('ico-linkedin.png');
		$html .= "<span class='mr-2'><a href='$url'><img src='{$img_src}'></a></span>";
	}

	if($url = $professor->col_facebook) {
		$img_src = get_tema_image_url('ico-facebook.png');
		$html .= "<span class='mr-2'><a href='$url'><img src='{$img_src}'></a></span>";
	}

	if($url = $professor->col_twitter) {
		$img_src = get_tema_image_url('ico-twitter.png');
		$html .= "<span class='mr-2'><a href='http://twitter.com/$url'><img src='{$img_src}'></a></span>";
	}

	if($url = $professor->col_googleplus) {

		$img_src = get_tema_image_url('ico-googleplus.png');
		$html .= "<span class='mr-2'><a href='{$url}'><img src='{$img_src}'></a></span>";
	}

	if($url = $professor->col_instagram) {
		$img_src = get_tema_image_url('ico-instagram.png');
		$html .= "<span class='mr-2'><a href='$url'><img src='{$img_src}'></a></span>";
	}

	if($url = $professor->col_youtube) {
		$img_src = get_tema_image_url('ico-youtube.png');
		$html .= "<span class='mr-2'><a href='$url'><img src='{$img_src}'></a></span>";
	}

	$img_src = get_tema_image_url('ico-email.png');
	$html .= "<span class='mr-2'><a id='professor-social-email-link' href='#modal'><img src='{$img_src}'></a></span>";

	return $html;
}

function carregar_recaptcha()
{
	KLoader::helper("RecaptchaHelper");
	return
		"<div class='g-recaptcha' data-sitekey='6LeN3Q0TAAAAAPOxoY7FvmksGSqPi0aB4U5TGNm2'" . RecaptchaHelper::get_size() . "></div>
         <script src='https://www.google.com/recaptcha/api.js?hl=pt-BR' async defer></script>";
}

//add_shortcode('home_concursos', 'get_concursos_componente');

/*function get_concursos_componente($status = NULL, $offset = 0)
{
	KLoader::model('ConcursoModel');
	KLoader::helper('UrlHelper');
	KLoader::helper('ConcursoHelper');

	$concursos = ConcursoModel::listar_mais_vendidos(6, $offset, $status);

	$data['concursos'] = $concursos;
	extract($data);

	ob_start();
	include get_stylesheet_directory() . '/paginas/home_concursos.php';
	$pagina = ob_get_clean();

	return $pagina;
}*/

function get_date_picker($selector)
{
	return
		"<script>
			jQuery(function() {
			  jQuery.datepicker.setDefaults({
			    dateFormat: 'dd/mm/yy',
			      dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
			      dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
			      dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
			      monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
			      monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
			      nextText: 'Próximo',
			      prevText: 'Anterior'
			  });
			  jQuery('{$selector}').datepicker();
			});
		</script>";
}


function get_concursos_produtos_celulas($produtos, $estilo, $exibe_pacote = FALSE, $ver_todos_url = null)
{
	return get_pagina('concurso_celula.php', ['produtos' => $produtos, 'estilo' => $estilo, 'exibe_pacote' => $exibe_pacote, 'ver_todos_url' => $ver_todos_url]);
}


function slicknav()
{
	$menu_a = get_menu_array('Main Menu');
	$menu = get_menu($menu_a);

	return "<div class='menu-mobile'>$menu</div>";
}

function exibe_assinatura_bar($usuario_id)
{
	$esconder_assinatura_bar = get_user_meta($usuario_id, 'esconder_assinatura_bar', TRUE);
	return (!$esconder_assinatura_bar || ($esconder_assinatura_bar < date('Y-m-d')));
}

add_shortcode('expo_mailchimp', 'expo_mailchimp');
function expo_mailchimp($atts) {
	$area = $atts['area'];
	$mostrar_areas = $atts['mostrar_areas'];

    if(!get_transient(TRANSIENT_MAILCHIMP)) {
	    atualizar_lista_mailchimp();
	}

    $interests = unserialize(get_transient(TRANSIENT_MAILCHIMP));

    ob_start();
	include get_stylesheet_directory() . '/paginas/expo_mailchimp.php';
	return ob_get_clean();
}

/**
 * Atualiza a lista do MailChimp
 *
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2299
 *
 * @since Joule 5
 */

function atualizar_lista_mailchimp()
{
	$campos = mailchimp_listar_interesses_areas();
	set_transient(TRANSIENT_MAILCHIMP, serialize($campos));
}

/**
 * Carrega o carousel da equipe coaching
 *
 * @since Joule 5
 */

add_shortcode('carousel_equipe_coaching', 'get_carousel_equipe_coaching');

function get_carousel_equipe_coaching()
{
	//carregar_owl_carousel();

	ob_start();
	include get_stylesheet_directory() . '/componentes/carousel_equipe_coaching.php';
	return ob_get_clean();
}

/**
 * Carrega as células de turmas de coaching
 *
 * @since Joule 5
 */

add_shortcode('concursos_coaching', 'get_concursos_coaching');

function get_concursos_coaching()
{
	$nome_concurso = "turma-de-coaching";

	$destaques = get_destaques_por_categoria($nome_concurso);
	$pacotes = get_pacotes_por_categoria($nome_concurso, true);
	$cursos = get_cursos_por_categoria($nome_concurso, false, false, true);
	$pacotes_simulados = get_simulados_por_categoria($nome_concurso, false, TIPO_SIMULADO_SOMENTE_PACOTES);
	$simulados = get_simulados_por_categoria($nome_concurso, false, TIPO_SIMULADO_SOMENTE_CURSOS);

	$html = "";
	if($destaques) {
		$html .= get_concursos_produtos_celulas($destaques, 'destaque');
	}

	if($pacotes) {
		$html .= get_concursos_produtos_celulas($pacotes, 'pacote');
	}

	if($cursos) {
		$html .= get_concursos_produtos_celulas($cursos, 'nao-pacote');
	}

	if($pacotes_simulados) {
		$html .= get_concursos_produtos_celulas($pacotes_simulados, 'pacote');
	}

	if($simulados) {
		$html .= get_concursos_produtos_celulas($simulados, 'nao-pacote');
	}

	return $html;
}

add_shortcode('add_vimeo_player_assets', 'get_vimeo_player_assets');

function get_vimeo_player_assets()
{
	return vimeo_player_assets();
}

function get_pesquisa($titulo, $slug, $filtro_combo = null, $ordenacao_combo = null, $somente_gratuitos = true)
{
	ob_start();
	include get_stylesheet_directory() . '/paginas/pesquisa.php';
	return ob_get_clean();
}

function get_produtos_box($produtos)
{
	ob_start();
	include get_stylesheet_directory() . '/paginas/produtos_box.php';
	return ob_get_clean();
}

function get_combobox($nome, $opcoes, $selecionado)
{
	ob_start();
	include get_stylesheet_directory() . '/componentes/combobox.php';
	return ob_get_clean();
}

function get_pesquisa_filtro_produtos_gratis_opcoes()
{
    return [
        '0' => "Todos",
        PESQUISA_TIPO_PACOTE => "Pacote",
        PESQUISA_TIPO_AUDIOBOOKS => "Audiobooks",
        PESQUISA_TIPO_CADERNOS => "Cadernos de Questões",
        PESQUISA_TIPO_MAPAS_MENTAIS => "Mapas Mentais",
        PESQUISA_TIPO_PDF => "PDF",
        PESQUISA_TIPO_SIMULADOS => "Simulados",
        PESQUISA_TIPO_TURMA_COACHING => "Turma de Coaching",
        PESQUISA_TIPO_VIDEOS => "Vídeos"
    ];
}

function get_pesquisa_ordem_produtos_gratis_opcoes()
{
	return[
		'0' => "Mais novos",
		PESQUISA_ORDEM_A_Z => "A - Z",
		PESQUISA_ORDEM_Z_A => "Z - A"
	];
}

function get_pesquisa_form_url()
{

}

add_shortcode('expo_h1', 'expo_h1');
function expo_h1()
{
	global $post;

	$h1 = get_h1($post->ID);
	return "<h1>$h1</h1>";
}