<?php
function get_parceiros()
{
	return array(
		0 => '',
		'ls' => 'LS',
		'evp' => 'EVP'
	);
}

function get_parceiro_by_id($id)
{
	if(is_null($id)) return NULL;

	$parceiros = get_parceiros();
	return $parceiros[$id];
}

function listar_parceiros_cupons()
{
	global $wpdb;
	
	$sql = "SELECT * FROM parceiros_cupons ORDER BY par_id";

	return $wpdb->get_results($sql);
}

function salvar_parceiro_cupom($parceiro_id, $cupom_id, $nivel)
{

	if(!existe_parceiro_cupom($parceiro_id, $cupom_id, $nivel)) {

		global $wpdb;
		
		$wpdb->insert(
			'parceiros_cupons',
			array(
				'par_id' => $parceiro_id,
				'cup_id' => $cupom_id,
				'par_nivel' => $nivel
			)
		);

	}
}

function excluir_parceiro_cupom($parceiro_id, $cupom_id)
{
	global $wpdb;
	
	$sql = "DELETE FROM parceiros_cupons WHERE par_id = '{$parceiro_id}' AND cup_id = '{$cupom_id}'";

	$wpdb->query($sql);
}

function get_cupons_by_parceiro($parceiro_id, $nivel = NULL)
{
	global $wpdb;

	$nivel_frag = $nivel ? " AND par_nivel = '{$nivel}' " : "";
	
	$sql = "SELECT * FROM parceiros_cupons WHERE par_id = '{$parceiro_id}' {$nivel_frag}";

	return $wpdb->get_results($sql);
}

function get_parceiro_by_cupom($cupom_id)
{
	global $wpdb;
	
	$sql = "SELECT * FROM parceiros_cupons WHERE cup_id = '{$cupom_id}'";

	return $wpdb->get_row($sql);
}

function get_parceiro_cupom($parceiro_id, $cupom_id, $nivel)
{
	global $wpdb;
	
	$sql = "SELECT * FROM parceiros_cupons WHERE par_id = '{$parceiro_id}' AND cup_id = '{$cupom_id}' AND par_nivel = {$nivel}";

	return $wpdb->get_row($sql);
}

function existe_parceiro_cupom($parceiro_id, $cupom_id, $nivel)
{
	return get_parceiro_cupom($parceiro_id, $cupom_id, $nivel) ? TRUE : FALSE;
}

function identifica_parceria_do_pedido($order)
{
	$cupons = $order->get_used_coupons();

	foreach ($cupons as $cupom) {
		$pc = get_parceiro_by_cupom($cupom);

		if($pc) {
			return $pc->par_id;
		}
	}

	return NULL;
}

/**********************************************************************
 * Parceiros
 **********************************************************************/

function recupera_cupons_parceiros()
{
	$parceiros = get_parceiros();

	foreach ($parceiros as $key => $value) {

		if($key) {
		
			$parceiro_func = 'parceiro_' . $key;

			if(function_exists($parceiro_func)) {
			
				if($nivel = call_user_func($parceiro_func)) {
				
					$nivel = is_bool($nivel) ? NULL : $nivel;

					/**
					 * @todo COMENTAR esse código para funcionar com WP Dynamic Pricing and Discount
					 */
					if($pcs = get_cupons_by_parceiro($key, $nivel)) {

					 	return $pcs[0]->cup_id;
					}
					/**
					 * @todo DESCOMENTAR esse código para funcionar com WP Dynamic Pricing and Discount
					 */  

					// if($nivel) {
					// 	return $key . "-" . $nivel;
					// }
					// else {
					// 	return $key;
					// }

				}
			}

		}
	}

	return FALSE;
}

function parceiro_ls()
{
	$usuario = get_usuario_array();

	if($usuario && $usuario['cpf']) {

		if($cpf = desformatar_cpf($usuario['cpf'])) {;

			$url = "http://ws.lsensino.com.br/api/Parceiro/ConsultarAluno?cpf=$cpf";

			if($conteudo = @file_get_contents($url)) {
				$ls = json_decode($conteudo);
				return $ls->Mensagem == "ALUNO ATIVO";
			}
			return false;
		}

		return false;
	}

	return false;
}

function parceiro_evp()
{   
	return FALSE;
	
	$usuario = get_usuario_array();

	if($usuario && $usuario['cpf']) {

		if($cpf = desformatar_cpf($usuario['cpf'])) {;

			$url = "http://webservice.sinapix.com.br/evp/service/usuario_info.php?cpf=$cpf";

			if($conteudo = @file_get_contents($url)) {
				$json = json_decode($conteudo);
				if($json->cpf) {
					return $json->nivel;
				}
				else {
					return FALSE;
				}
			}

			return FALSE;
		}

		return FALSE;
	}

	return FALSE;
}