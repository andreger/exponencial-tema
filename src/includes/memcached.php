<?php

function get_memcached_server() {
    $memcached = new \Memcached();
    $memcached->addServer("127.0.0.1", 11211);

    return $memcached;
}

function set_memcached_entry($cache_name, $cache, $cache_time = HOUR_IN_SECONDS)
{
    $memcached = get_memcached_server();
    $memcached->set($cache_name, $cache, $cache_time);
    log_memcached("CACHE SAVE: $cache_name");
}

function get_memcached_entry($cache_name)
{
    $memcached = get_memcached_server();

    if($cache = $memcached->get($cache_name)) {
        log_memcached("CACHE HIT: $cache_name");
    }
    else {
        log_memcached("CACHE MISS: $cache_name");
    }

    return $cache;
}

function delete_cache_entries($inicia_com, $contem) {

    if(!is_array($contem)) {
        $contem = [$contem];
    }

    $memcached = get_memcached_server();

    if($all = $memcached->getAllKeys()) {
        foreach ($all as $item) {
            if(strpos($item, $inicia_com) === 0) {
                foreach ($contem as $c) {
                    if(strpos($item, (String)$c) !== false) {
                        $memcached->delete($item);
                        log_memcached("CACHE REMOVED: $item");
                    }
                }
            }
        }
    }


}

function remover_cache_concurso($concurso_id)
{
    delete_cache_entries("expo:concurso:", [$concurso_id]);
}

function remover_cache()
{
    //$memcached = get_memcached_server();
    //$memcached->flush();
}