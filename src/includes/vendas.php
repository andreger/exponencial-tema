<?php
function is_aluno_real($row) {
	if($row['ven_item_tipo'] == 0) return true;

	if($row['ven_item_tipo'] == 1) return false;

	if($row['ven_item_tipo'] == 2) {
		return $row['vep_nao_somar'] == 0 ? true : false;
	}
}

function format_data_venda($data) {
	if($data == "0000-00-00")
		return "";

	return substr($data, 8, 2) . '/' . substr($data, 5, 2) . '/' . substr($data, 0, 4);
}

function get_data_filtro($data, $before = false) {
	$hour = $before ? 23 : 0;
	$minute = $before ? 59 : 0;
	$second = $before ? 59 : 0;

	$timestamp = strtotime(str_replace('/', '-', $data));
	$data_array = getdate($timestamp);
	return  array('year' => $data_array['year'], 'month' => $data_array['mon'], 'day' => $data_array['mday'], 'hour' => $hour, 'minute' => $minute, 'second' => $second);
}

function format_moeda($valor) {
	return number_format ( $valor, 2, "," , "." );
}

function has_error_dates($start_date, $end_date) {

	$timestamp_start = strtotime(str_replace('/', '-', $start_date));
	if($timestamp_start == 0) return true;

	if(empty($end_date)) return false;

	$timestamp_end = strtotime(str_replace('/', '-', $end_date));
	if($timestamp_end == 0) return true;

	if($timestamp_end < $timestamp_start) return true;

	return false;
}

function get_total_pago($lines)
{
	$total = 0;

	foreach ($lines as $line) {
		$total += $line['total'] - $line['discount'];
	}

	return $total;
}

function listar_vendas_por_periodo($data_inicio, $data_fim = null)
{
	global $wpdb;

	if(is_null($data_fim)) {
		$data_fim = $data_inicio;
	}

	$inicio = converter_para_yyyymmdd($data_inicio);
	$fim = converter_para_yyyymmdd($data_fim);

	$sql = "SELECT * FROM vendas WHERE ven_data >= '{$inicio} 00:00:00' AND ven_data <= '{$fim} 23:59:59'";

	return $wpdb->get_results($sql);
}

function excluir_vendas_por_periodo($data_inicio, $data_fim = null)
{
	global $wpdb;

	if(!$data_fim) {
		$data_fim = $data_inicio;
	}

	$inicio = converter_para_yyyymmdd($data_inicio);
	$fim = converter_para_yyyymmdd($data_fim);

	$sql = "DELETE FROM wp_vendas WHERE ven_data >= '{$inicio} 00:00:00' AND ven_data <= '{$fim} 23:59:59'";

	$wpdb->query($sql);
}

function excluir_vendas_por_pedido($pedido_id)
{
	global $wpdb;

	$sql = "DELETE FROM wp_vendas WHERE ven_order_id = {$pedido_id}";

	$wpdb->query($sql);
}

function contar_pedidos_nao_processados()
{
	global $wpdb;

	$skip = [29422, 346020];
	$skip_list = "(" . implode(",", $skip) . ")";

	$query = "select count(*) as num from wp_posts 
                where post_type = 'shop_order' 
                  and post_status IN ('wc-completed', 'wc-refunded') 
                  and ID NOT IN (SELECT ven_order_id FROM wp_vendas)
                  AND ID NOT IN {$skip_list}";
	$result = $wpdb->get_row($query);

	return $result->num;
}

function processar_pedidos_nao_processados($force_pagseguro = false, $limit = 50)
{
	global $wpdb;

	$skip = [29422, 346020];
	$skip_list = "(" . implode(",", $skip) . ")";

	$query = "SELECT ID FROM wp_posts
              WHERE post_type = 'shop_order'
                 AND post_status IN ('wc-completed', 'wc-refunded') 
                 AND ID NOT IN (SELECT ven_order_id FROM wp_vendas)
                 AND ID NOT IN {$skip_list}
              ORDER BY ID ASC LIMIT {$limit}";

	$resultado = $wpdb->get_results($query);

	// Guarda os pedidos que estão sendo processados. Finalidade: log.
	$pedidos_processados = [];

	foreach ($resultado as $item) {
		set_time_limit(60);
		processar_pedido($item->ID, $force_pagseguro);
		array_push($pedidos_processados, new WC_Order($item->ID));
	}

	// Retorna os pedidos processados nessa execução
	return $pedidos_processados;
}

function processar_pedido($pedido_id, $force_pagseguro = false, $debug = false)
{
	global $wpdb;

	KLoader::model("RelatorioVendasPorProfessorModel");
	KLoader::model("PedidoModel");
	KLoader::helper("PagSeguroGatewayHelper");
	KLoader::helper("PagarMeGatewayHelper");

	$order = new WC_Order($pedido_id);
	$data_pedido = $order->get_date_created()->date('Y-m-d H:i:s');

	log_pedido("info", "Processando pedido {$pedido_id}");

	$parcelas = get_post_meta($order->get_id(), 'Installments', TRUE);
	$parcelas = empty($parcelas) ? get_post_meta($order->get_id(), 'Parcelas', TRUE) : $parcelas;

	$metodo = get_post_meta($order->get_id(), 'Payment method', TRUE);
	$metodo = empty($metodo) ? get_post_meta($order->get_id(), 'Método de pagamento', TRUE) : $metodo;

	$total_desconto = PedidoModel::get_pedido_desconto($order);
	log_pedido("info", "Total desconto: {$total_desconto}");
	log_pedido("info", "Order->get_total(): " . $order->get_total() );

	$total = $order->get_total() + $total_desconto;
	log_pedido("info", "Total (order + desconto): " . $total );

	$is_reembolso_total = PedidoModel::is_pedido_reembolsado_totalmente($order, $total);
	log_pedido("info", "Is Reembolso total? " . ($is_reembolso_total?'Sim':'Nao') );

	$taxas_pagseguro = 0;
	if($total > 0) {
	    // Taxas caso pedido tenha sido no Pagar.me
	    if(PagarMeGatewayHelper::is_gateway_pedido_pagarme($order->get_id())) {
	        $taxas_pagseguro = PagarMeGatewayHelper::get_taxas($order);
	    }
	    // Taxas caso pedido tenha sido no PagSeguro
	    else {
	        $taxas_pagseguro = PagSeguroGatewayHelper::get_taxas($order, $force_pagseguro);
	    }
	}

	$last_bundle_id = null;
	$last_bundle_valor_venda = null;
	$last_bundle_desconto = null;
	$last_bundle_pagseguro = null;
	$last_bundle_subtotal = null;
    $last_bundle_aliquota = null;
    $last_bundle_afiliados = null;

	$parceiro = identifica_parceria_do_pedido($order);

	$valores_produtos = get_post_meta($order->get_id(), 'valores-produtos', true);

	if($items = $order->get_items()) {

		foreach ($items as $item) {

			$product = new WC_Product($item['product_id']);

			log_pedido("info", "Processando item do pedido. Produto: {$item['product_id']}");

			// recupera valor estornado
			$reembolso = 0;

			if(is_produto_estornado($order->get_id(), $item->get_product_id())) {
				$reembolso = $order->get_total_refunded_for_item($item->get_id());
				log_pedido("info", "Item teve produto {$item->get_product_id()} estornado: {$reembolso}");
			}else{
				log_pedido("info", "Item NAO teve produto {$item->get_product_id()} estornado");
			}
			
			if(count($items) == 1) {
			    $percentual_item = 1;
			}
			else {
    			if(is_item_de_pacote($item)) {//O subtotal de um item de pacote é ZERO
    
    				if($total > 0){
    					//Valor percentual do item do pacote no pacote
    					$percentual_item = ( ($valores_produtos[$item->get_id()] - $reembolso) / ($last_bundle_valor_venda - $last_bundle_desconto) );
    					//Valor do item do pacote no pedido sem desconto
    					$item_pacote_subtotal = $percentual_item * $last_bundle_subtotal;
    					//Valor percentual do item do pacote no pedido sem desconto
    					$percentual_item = $item_pacote_subtotal / $total;
    
    				}else{
    					$percentual_item = 0;
    				}
    
    			}else{
    				$percentual_item = ($total > 0) ? ( ($item->get_subtotal() - $reembolso) / $total ) : 0;
    			}
			}

			$desconto = $total_desconto * $percentual_item;

			$aluno = get_usuario_array( $order->get_user_id() );
			$aliquota = get_imposto_por_data($data_pedido);

			$afiliados_percentual = get_post_meta($order->get_id(), AFILIADOS_PERCENTUAL_META, true);
			$folha_dirigida = $afiliados_percentual ? $afiliados_percentual / 100 : 0;

			// Hack para reembolso total. Se reembolso total então valor_venda - desconto = reembolso.
			// Necessário, pois reembolsos diretos no PS não adicionam informações referentes aos produtos
			if($is_reembolso_total) {
				$reembolso = $item['line_subtotal'] - $desconto;
				log_pedido("info", "Eh reembolso total entao reembolso = " . $item['line_subtotal'] . " menos " .$desconto);
			}
			
			$valor_pago = $item['line_subtotal'] - $desconto - $reembolso;
			log_pedido("info", "Valor pago (line_subtotal - desconto - reembolso) = " . $valor_pago);
			
			$venda = array();
			$venda['ven_order_id'] = $order->get_id();
			$venda['ven_aluno_id'] = $order->get_user_id();
			$venda['ven_aluno_nome'] = $aluno['nome_completo'];
			$venda['ven_aluno_email'] = $aluno['email'];
			$venda['ven_aluno_cpf'] = $aluno['cpf'] ? substr($aluno['cpf'], 0, 11) : "";
			$venda['ven_aluno_cidade'] = $aluno['cidade_nome'];
			$venda['ven_aluno_uf'] = $aluno['uf'];
			$venda['ven_aluno_telefone'] = $aluno['telefone'];
			$venda['ven_data'] = $data_pedido;
			$venda['ven_curso_id'] = $item['product_id'];
			$venda['ven_curso_areas'] = get_area($item['product_id']);
			$venda['ven_valor_venda'] = $item['line_subtotal'];
			$venda['ven_desconto'] = $desconto;
			$venda['ven_reembolso'] = $reembolso;
			$venda['ven_pagseguro'] = $valor_pago == 0 ? 0 : $taxas_pagseguro * $percentual_item;
			$venda['ven_folha_dirigida'] = $valor_pago == 0 ? 0 : $folha_dirigida;
			$venda['ven_forma_pgto'] = $metodo;
			$venda['ven_parcelamento'] = $parcelas;
			$venda['ven_aliquota'] = $valor_pago == 0 ? 0 : $aliquota;
			$venda['ven_parceiro'] = $parceiro;

			// Item Raiz de Pacote
			if(is_item_raiz_de_pacote($item)) {
			    $venda['ven_item_tipo'] = ITEM_RAIZ_PACOTE;
				$venda['ven_item_perc'] = 100;
				$last_bundle_id = $item['product_id'];
				$last_bundle_valor_venda = $venda['ven_valor_venda'];
				$last_bundle_desconto = $venda['ven_desconto'];
				$last_bundle_pagseguro = $venda['ven_pagseguro'];
				$last_bundle_subtotal = $item->get_subtotal();
                $last_bundle_aliquota =  $venda['ven_aliquota'];
                $last_bundle_afiliados =  $venda['ven_folha_dirigida'];
			}
			// Item Não-Raiz de Pacote
			elseif(is_item_de_pacote($item)) {
                $last_bundle_com_desconto = $last_bundle_valor_venda - $last_bundle_desconto;

			    $venda['ven_item_tipo'] = ITEM_NAO_RAIZ_PACOTE;
				//$venda['ven_item_perc'] = get_valor_percentual_item_pacote($last_bundle_id, $item['product_id']);

                $percentual_item_pacote = $last_bundle_com_desconto ? $valores_produtos[$item->get_id()] / $last_bundle_com_desconto : 0;

				$venda['ven_item_perc'] =  $percentual_item_pacote * 100;
// 				$venda['ven_valor_venda'] = $valores_produtos[$item->get_id()];
				$venda['ven_valor_venda'] = $last_bundle_valor_venda * $percentual_item_pacote;
				$venda['ven_desconto'] = $last_bundle_desconto * $percentual_item_pacote;
				$venda['ven_pagseguro'] = $last_bundle_pagseguro * $percentual_item_pacote;
				$venda['ven_aliquota'] = $last_bundle_aliquota;
				$venda['ven_folha_dirigida'] = $last_bundle_afiliados;
			}
			// item Não-Pacote
			else {
			    $venda['ven_item_tipo'] = ITEM_NAO_PACOTE;
				$venda['ven_item_perc'] = 100;
			}

			// Salva em wp_vendas
            log_pedido("info", "Adicionando valor na tabela wp_vendas " . serialize($venda));
			$wpdb->insert('wp_vendas', $venda);

			$venda_id = $wpdb->insert_id;

			$wpdb->delete('wp_vendas_professores', [
			    'ven_id' => $venda_id
            ]);

            $wpdb->delete('wp_vendas_coordenadores', [
                'ven_id' => $venda_id
            ]);

			// não grava os autores de um pacote, pois isso é tratado individualmente em cada item do pacote
			if(is_item_raiz_de_pacote($item)) continue;

			$product_id = $item['product_id'];

			$post = get_post($product->get_id());
			$autores = get_autores($post);

            log_pedido("info", "Produto Id: " . $product->get_id());
            log_pedido("info", "Autores: " . serialize($autores));

			$soma_participacoes = 0;

            foreach ($autores as $autor) {

			    if($autor) {
    				$participacao = str_replace(',', '.', (get_participacao_autor($product_id, $autor->ID)));
    				$data_ymd = converter_para_yyyymmdd($venda['ven_data']);

    				$lucro_por_data = get_lucro_por_data($autor->ID, $data_ymd) ?: PARTICIPACAO_PROFESSOR_DEFAULT;
    				$percentual_professor = $lucro_por_data * ($participacao / 100);
    				$soma_participacoes += $percentual_professor;

    				$pago = get_usuario_array($autor->ID);

    				$item = array(
    						'ven_id' 				=> $venda_id,
    						'vep_professor_id' 		=> $autor->ID,
    						'vep_professor_nome'	=> $pago['nome_completo'],
    						'vep_professor_perc' 	=> $percentual_professor,
    						'vep_nao_somar'			=> has_multiplos_autores($product_id),
    						'vep_professor_lucro'	=> $lucro_por_data,
    						'vep_participacao'		=> $participacao
    				);

    				// Salva em wp_vendas
                    log_pedido("info", "Adicionando valor na tabela wp_vendas_professores " . serialize($item));
                    $wpdb->insert('wp_vendas_professores', $item);
			    }
			}

			if(has_multiplos_autores($product_id)) {
				$item = array(
						'ven_id' 				=> $venda_id,
						'vep_professor_id' 		=> -1,
						'vep_professor_perc' 	=> $soma_participacoes
				);
				// Salva em wp_vendas
                log_pedido("info", "Adicionando valor na tabela wp_vendas_professores " . serialize($item));
                $wpdb->insert('wp_vendas_professores', $item);
			}

			// Participação de coordenadores
			log_pedido("info", "Processando participacao dos coordenadores");

			if(is_produto_assinatura($product_id) || is_produto_simulado($product_id) || is_produto_caderno($product_id)) {

				log_pedido("info", "Produto eh assinatura");

				$data_ymd = converter_para_yyyymmdd($venda['ven_data']);
				$coordenadores = listar_coordenadores_sq_por_data($data_ymd);

				log_pedido("info", "Tem " . count($coordenadores) . " coordenador(es)");

				if($coordenadores) {
					$soma = 0;
					foreach ($coordenadores as $coordenador) {
						$data_ymd = converter_para_yyyymmdd($venda['ven_data']);
						$participacao = get_valor_percentual_por_data_coordenador_sq($data_ymd, $coordenador->user_id);
						$soma += $participacao;

						$item = array(
							'ven_id' => $venda_id,
							'vec_participacao' => $participacao,
							'vec_coordenador_id' => $coordenador->user_id
						);

						log_pedido("info", "Adicionando valor na tabela wp_vendas_coordenadores " . serialize($item));
						$wpdb->insert('wp_vendas_coordenadores', $item);
					}

					$item = array(
						'ven_id' => $venda_id,
						'vec_participacao' => $soma,
						'vec_coordenador_id' => -1
					);

					log_pedido("info", "Adicionando valor na tabela wp_vendas_coordenadores " . serialize($item));
					$wpdb->insert('wp_vendas_coordenadores', $item);
				}
			}

			else if (is_produto_coaching($product_id)) {

				log_pedido("info", "Produto eh coaching");

				$data_ymd = converter_para_yyyymmdd($venda['ven_data']);
				$coordenadores = listar_coordenadores_coaching_por_data($data_ymd);

				log_pedido("info", "Tem " . count($coordenadores) . " coordenador(es)");

				if($coordenadores) {
					$soma = 0;
					foreach ($coordenadores as $coordenador) {
						$data_ymd = converter_para_yyyymmdd($venda['ven_data']);
						$participacao = get_valor_percentual_por_data_coordenador_coaching($data_ymd, $coordenador->user_id);
						$soma += $participacao;

						$item = array(
							'ven_id' => $venda_id,
							'vec_participacao' => $participacao,
							'vec_coordenador_id' => $coordenador->user_id
						);

						log_pedido("info", "Adicionando valor na tabela wp_vendas_coordenadores " . serialize($item));
						$wpdb->insert('wp_vendas_coordenadores', $item);
					}

					$item = array(
						'ven_id' => $venda_id,
						'vec_participacao' => $soma,
						'vec_coordenador_id' => -1
					);

					log_pedido("info", "Adicionando valor na tabela wp_vendas_coordenadores " . serialize($item));
					$wpdb->insert('wp_vendas_coordenadores', $item);
				}
			}

			else {

				log_pedido("info", "Produto nao eh assinatura, nem coaching");

				$area_id = get_produto_area_coordenacao($product_id);

				if($area_id) {
					$data_ymd = converter_para_yyyymmdd($venda['ven_data']);
					$coordenadores = listar_coordenadores_areas_por_data($data_ymd, $area_id);

					log_pedido("info", "Area ID: $area_id");
					log_pedido("info", "Tem " . count($coordenadores) . " coordenador(es)");

					if($coordenadores) {
						$soma = 0;

						foreach ($coordenadores as $coordenador) {
							$data_ymd = converter_para_yyyymmdd($venda['ven_data']);
	 						$participacao = get_valor_percentual_por_data_coordenador_area($data_ymd, $area_id, $coordenador->user_id);
	 						$soma += $participacao;

							$item = array(
								'ven_id' => $venda_id,
								'vec_participacao' => $participacao,
								'vec_coordenador_id' => $coordenador->user_id
							);

							log_pedido("info", "Adicionando valor na tabela wp_vendas_coordenadores " . serialize($item));
							$wpdb->insert('wp_vendas_coordenadores', $item);
						}

						$item = array(
							'ven_id' => $venda_id,
							'vec_participacao' => $soma,
							'vec_coordenador_id' => -1
						);

						log_pedido("info", "Adicionando valor na tabela wp_vendas_coordenadores " . serialize($item));
						$wpdb->insert('wp_vendas_coordenadores', $item);
					}
				}
			}
		}

		processar_assinaturas($order->get_id());
	}

	RelatorioVendasPorProfessorModel::calcular_acumulados($data_pedido);
	PedidoModel::alterar_pedido_status_usuario($order->get_id());
}

/**
 * Processa a assinatura SQ
 *
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/upgrades/items/310
 *
 * @param int $order_id Id do pedido no WP
 */

function processar_assinaturas($order_id)
{
	global $wpdb;

	$order = new WC_Order($order_id);

	$valores_produtos = get_post_meta($order->get_id(), 'valores-produtos', true);

	if($items = $order->get_items()) {

		foreach ($items as $item) {

			$product_id = $item['product_id'];

			if($tempo = get_produto_assinatura_tipo($product_id)) {

				$data_inicio = $order->get_date_created()->date('Y-m-d');
				$data_fim = date('Y-m-d', strtotime($data_inicio . ' +' . $tempo . ' months'));

				$wpdb->insert('assinaturas', array(
					'user_id' => $order->get_user_id(),
					'order_id' => $order_id,
					'tempo' => $tempo,
					'data_inicio' => $data_inicio,
					'data_fim' => $data_fim,
					'preco' => $valores_produtos[$item->get_id()]
				));

			}
		}
	}
}

/*
function get_ultima_assinatura_processada()
{
	$query = "SELECT * from wp_posts where post_type = 'shop_order' and post_status = 'wc-completed' and ID not in (SELECT ven_order_id FROM wp_vendas) order by ID limit 100";
}
*/

function processar_pedidos_pendentes()
{
	global $wpdb;

	// Recupera pedidos com status "wc-pending" feitos nos últimos 2 dias
	$ontem_ts = strtotime("-2 days");

	$args = array(
			'post_type' => 'shop_order',
			'post_status' => 'wc-pending',
			'posts_per_page' => 5,
			'orderby' => 'post_date',
			'order' => 'DESC',
			'date_query' => array(
					array(
							'after'		=> array(
									'year'  => date('Y', $ontem_ts),
									'month' => date('m', $ontem_ts),
									'day'   => date('d', $ontem_ts),
							),
							'inclusive' => true,
					),
			),
	);

	$my_query = new WP_Query($args);
	$customer_orders = $my_query->posts;

	foreach ($customer_orders as $customer_order) {
		set_time_limit(60);

		$order = new WC_Order();
		$order->populate($customer_order);

		if($order->status != 'pending') continue;

		// recupera o id da transação no pagseguro
		$pagseguro_id = get_pagseguro_transaction_id($order->get_id());

		if($pagseguro_id) {
			$base = "https://ws.pagseguro.uol.com.br/v3/transactions/". $pagseguro_id ;
			$data = "email=leonardo.coelho@exponencialconcursos.com.br&token=67E47689AEB74EF685A9B47B2FCB1E4A";

			$url = $base . '?' . $data;

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, 30);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_URL, $url );
			$return = curl_exec($ch);
			curl_close($ch);

			$xml= str_get_html($return);

			$installmentFeeAmout = $xml->find('installmentFeeAmount', 0)->plaintext;
			$intermediationRateAmount = $xml->find('intermediationRateAmount', 0)->plaintext;
			$intermediationFeeAmount = 	$xml->find('intermediationFeeAmount', 0)->plaintext;
			$taxas_pagseguro = 	$installmentFeeAmout + $intermediationFeeAmount + $intermediationRateAmount;

			update_post_meta($order->get_id(), 'PagSeguro Total Taxes', $taxas_pagseguro);
			update_post_meta($order->get_id(), 'PagSeguro Installment Fee Amount', $installmentFeeAmout);
			update_post_meta($order->get_id(), 'PagSeguro Intermediation Rate Amount', $intermediationRateAmount);
			update_post_meta($order->get_id(), 'PagSeguro Intermediation Fee Amount', $intermediationFeeAmount);

			/**********************************************************************
			 * Código	Significado
			 *   1	 Aguardando pagamento: o comprador iniciou a transação, mas até o momento o PagSeguro não recebeu nenhuma informação sobre o pagamento.
			 *   2	 Em análise: o comprador optou por pagar com um cartão de crédito e o PagSeguro está analisando o risco da transação.
			 *   3	 Paga: a transação foi paga pelo comprador e o PagSeguro já recebeu uma confirmação da instituição financeira responsável pelo processamento.
			 *   4	 Disponível: a transação foi paga e chegou ao final de seu prazo de liberação sem ter sido retornada e sem que haja nenhuma disputa aberta.
			 *   5	 Em disputa: o comprador, dentro do prazo de liberação da transação, abriu uma disputa.
			 *   6	 Devolvida: o valor da transação foi devolvido para o comprador.
			 *   7	 Cancelada: a transação foi cancelada sem ter sido finalizada.
			 *
			 * Fonte: https://pagseguro.uol.com.br/v2/guia-de-integracao/api-de-notificacoes.html#v2-item-api-de-notificacoes-status-da-transacao
			 **********************************************************************/

			$status = $xml->find('status', 0)->plaintext;

			if($status == 3) {
				$order->update_status('completed', 'Pagseguro: Pagamento aprovado.');
			}
		}

	}
}

function salvar_minimo_valor_acumulado($data, $valor, $tipo = VMA_VENDAS)
{
	global $wpdb;

	if($vma = existe_minimo_valor_acumulado_definido($data, $tipo)) {

		$wpdb->update('vendas_minimos_acumulados', array(
			'vma_valor' => $valor,
			'vma_tipo' => $tipo
		), array(
			'vma_data' => $data
		));

	}
	else {

		$wpdb->insert('vendas_minimos_acumulados', array(
			'vma_data' => $data,
			'vma_valor' => $valor,
			'vma_tipo' => $tipo
		));

	}
}

function existe_minimo_valor_acumulado_definido($data, $tipo = VMA_VENDAS)
{
	global $wpdb;

	$sql = "SELECT * FROM vendas_minimos_acumulados WHERE vma_data = '{$data}' AND vma_tipo = $tipo ORDER BY vma_data desc LIMIT 1";

	return $wpdb->get_row($sql) ? TRUE : FALSE;
}

function get_minimo_valor_acumulado($data, $tipo = VMA_VENDAS)
{
	global $wpdb;

	$sql = "SELECT * FROM vendas_minimos_acumulados WHERE vma_data <= '{$data}' AND vma_tipo = $tipo ORDER BY vma_data desc LIMIT 1";

	return $wpdb->get_row($sql);
}

function get_minimo_valor_acumulado_por_data($data, $tipo = VMA_VENDAS)
{
	if($mva = get_minimo_valor_acumulado($data, $tipo)) {
		return $mva->vma_valor;
	}

	return 0;
}


function get_minimo_valor_acumulado_por_periodo($data_inicio, $data_fim, $tipo = VMA_VENDAS)
{
	$i_a = explode("-", $data_inicio);
	$f_a = explode("-", $data_fim);

	// retorna NULL se mês ou ano forem diferentes
	if($i_a[1] != $f_a[1] || $i_a[0] != $f_a[0]) {
		return NULL;
	}

	$data = "{$i_a[0]}-{$i_a[1]}-01";

	/**************************************************************************************
	 * https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/1975
	 * Workaround para mês anterior à primeira data de mínimo acumulado
	 **************************************************************************************/
	if($data == "2017-03-01" && $tipo == VMA_VENDAS) {
		$data = "2017-04-01";
	}

	return get_minimo_valor_acumulado_por_data($data, $tipo);
}


function listar_minimos_valores_acumulados($tipo = VMA_VENDAS)
{
	global $wpdb;

	$sql = "SELECT * FROM vendas_minimos_acumulados WHERE vma_tipo = $tipo ORDER BY vma_data DESC";

	return $wpdb->get_results($sql);
}

function get_minimo_valor_acumulado_atual($tipo = VMA_VENDAS)
{
	global $wpdb;

	$sql = "SELECT * FROM vendas_minimos_acumulados WHERE vma_tipo = $tipo ORDER BY vma_data DESC LIMIT 1";

	if($resultado = $wpdb->get_row($sql))
	{
		return $resultado->vma_valor;
	}

	return 0;
}

function excluir_pagamentos_acumulados_por_data($data)
{
	global $wpdb;

	$sql = "DELETE FROM vendas_pagamentos_acumulados WHERE vpa_data = '{$data}'";

	$wpdb->query($sql);
}

function salvar_pagamento_acumulado($user_id, $data, $valor)
{
	global $wpdb;

	$wpdb->insert('vendas_pagamentos_acumulados', array(
		'user_id' => $user_id,
		'vpa_data' => $data,
		'vpa_acumulado' => $valor
	));
}

function get_valor_pagamento_acumulado($user_id, $data)
{
	global $wpdb;

	$sql = "SELECT * FROM vendas_pagamentos_acumulados WHERE user_id = {$user_id} AND vpa_data = '{$data}' ORDER BY vpa_data DESC LIMIT 1";

	if($resultado = $wpdb->get_row($sql)) {
		return $resultado->vpa_acumulado;
	}

	return 0;
}

function listar_vendas_agrupadas_por_area($data_inicio, $data_fim, $ordem = 1, $categoria_campo = 'name', $coordenador_ids = null, $area_ids = null)
{
	global $wpdb;

	$ordem = isset($_POST['ordem']) ? $_POST['ordem'] : 1;

	$query = "SELECT DISTINCT v.*, vp.*, vc.*, u.display_name as nome_professor, p.post_title as nome_curso
				FROM wp_vendas v
				LEFT JOIN wp_vendas_professores vp ON v.ven_id = vp.ven_id
				LEFT JOIN wp_vendas_coordenadores vc ON v.ven_id = vc.ven_id
				LEFT JOIN wp_posts p ON v.ven_curso_id = p.ID
				LEFT JOIN wp_users u ON vp.vep_professor_id = u.ID
				WHERE v.ven_data BETWEEN '{$data_inicio} 00:00:00' AND '{$data_fim} 23:59:59'

				ORDER BY ven_data DESC, ven_order_id DESC, ven_curso_id DESC, ven_item_tipo ASC
				";
// echo $query;
	$result = $wpdb->get_results($query, ARRAY_A);

	// print_r($result);exit;

	$sublines = array();

	foreach ($result as $row) {
		// if($row['vec_coordenador_id'] == -1) continue;

		if(empty($row['vep_participacao'])) $row['vep_participacao'] = 100;

		$index_curso = $row['nome_curso'] . "-" . $row['vec_coordenador_id'];

		if(empty($index_curso)) continue;

		if(($row['ven_item_tipo'] == 0 ||  $row['ven_item_tipo'] == 2) && $row['vep_nao_somar'] == 1) continue; // trata produtos com mais de um professor

		$total = $row['ven_valor_venda'] * ($row['vep_participacao'] /100);
		$desconto = $row['ven_desconto'] * ($row['vep_participacao'] /100);
		$valor_pago = $total - $desconto;
		$aliquota =  $row['ven_aliquota'];
		$valor_imposto = $aliquota / 100 * $valor_pago;
		$pag_seguro = $row['ven_pagseguro']  * ($row['vep_participacao'] /100);
		$folha_dirigida = $row['ven_folha_dirigida'] * $valor_pago;
		$liquido = $valor_pago - $valor_imposto - $pag_seguro - $folha_dirigida;
		$u = get_usuario_array($row['vec_coordenador_id']);

		if(empty($sublines[$index_curso])) {

			$sublines[$index_curso] = array(
				'nome'				=> $row['nome_curso'],
				'coordenador_id'	=> $row['vec_coordenador_id'],
				'coordenador'		=> $u['nome_completo'],
				'qtde_alunos' 		=> 1,
				'total' 			=> $total,
				'discount'			=> $desconto,
				'valor_pago'		=> $valor_pago,
				'pagseguro'			=> $pag_seguro,
				'folha_dirigida'	=> $folha_dirigida,
				'valor_imposto' 	=> $valor_imposto,
				'liquido' 			=> $liquido,
				'curso_id'	 		=> $row['ven_curso_id'],
				'item_tipo'			=> $row['ven_item_tipo'],
				'qtde_alunos_reais'	=> is_aluno_real($row) ? 1 : 0,
			);
		}
		else {

			$sublines[$index_curso] = array(
				'nome'				=> $row['nome_curso'],
				'coordenador_id'	=> $row['vec_coordenador_id'],
				'coordenador'		=> $u['nome_completo'],
				'qtde_alunos' 		=> $sublines[$index_curso]['qtde_alunos'] + 1,
				'total' 			=> $sublines[$index_curso]['total'] + $total,
				'discount'			=> $sublines[$index_curso]['discount'] + $desconto,
				'valor_pago'		=> $sublines[$index_curso]['valor_pago'] + $valor_pago,
				'pagseguro'			=> $sublines[$index_curso]['pagseguro'] + $pag_seguro,
				'folha_dirigida'	=> $sublines[$index_curso]['folha_dirigida'] + $folha_dirigida,
				'valor_imposto' 	=> $sublines[$index_curso]['valor_imposto'] + $valor_imposto,
				'liquido'			=> $sublines[$index_curso]['liquido'] + $liquido,
				'curso_id'	 		=> $row['ven_curso_id'],
				'item_tipo'			=> $row['ven_item_tipo'],
				'qtde_alunos_reais'	=> is_aluno_real($row) ? $sublines[$index_curso]['qtde_alunos_reais'] + 1 : $sublines[$index_curso]['qtde_alunos_reais'],
			);
		}
	}
	// ksort($sublines);

	if($sublines) {

		$nome = [];
		$valor = [];

		foreach ($sublines as $key => $row) {
		    $nome[$key]  = $row['nome'];
		    $valor[$key] = $row['liquido'];
		}

		switch ($ordem) {
			case 1:
				array_multisort($nome, SORT_ASC, $valor, SORT_ASC, $sublines);
				break;
			case 2:
				array_multisort($nome, SORT_DESC, $valor, SORT_DESC, $sublines);
				break;
			case 3:
				array_multisort($valor, SORT_DESC, $nome, SORT_DESC, $sublines);
				break;
			case 4:
				array_multisort($valor, SORT_ASC, $nome, SORT_ASC, $sublines);
				break;
		}

	}

	if($categoria_campo == 'name') {
		$assinaturas_array = ["Assinatura SQ Mensal", "Assinatura SQ Trimestral", "Assinatura SQ Semestral", "Assinatura SQ Anual"];
		$assinatura_index = "Assinaturas SQ";
		$caderno_index = "Caderno de Questões On-line";
	}
	else {
		$assinaturas_array = ["assinatura-sq-mensal", "assinatura-sq-trimestral", "assinatura-sq-semestral", "assinatura-sq-anual"];
		$assinatura_index = "assinatura-sq";
		$caderno_index = "caderno-questoes-online";
	}

	$lines = array();
	foreach ($sublines as $key => $subline) {
		log_relatorio('debug', "Curso: {$subline['curso_id']}");
		$categorias_str = trim(get_categorias_str($subline['curso_id'], true, $categoria_campo));

		if($categorias_str) {
			$categorias = explode(",", $categorias_str);

			foreach ($categorias as $categoria) {
				$index_concurso = trim($categoria);

				if(in_array($index_concurso, $assinaturas_array)) {
					$index_concurso = $assinatura_index;
				}

				log_relatorio('debug', "Categoria (index_concurso): {$index_concurso}");

				/*******************************************************************************
				 * https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/1931
				 *
				 * LUCAS SALVETTI: O que pedi de ajuste é que o VALOR dos cadernos saiam do
				 * somatório dos "concursos", pra não misturar no pagamento dos coordenadores e
				 * ser possível validar os totais entre os relatórios (pra detectar erro)
				 ******************************************************************************/
				// echo "<pre>";
				// print_r($subline);
				$nao_somar = ($subline['item_tipo'] == 1) ||
					($index_concurso != $caderno_index && is_produto_caderno_questoes_online($subline['curso_id'])) ||
					$subline['coordenador_id'] != -1 ||
					is_produto_simulado($subline['curso_id'])
					? TRUE : FALSE;

				if(empty($lines[$index_concurso])) {

					log_relatorio('debug', "Primeira linha do index: {$index_concurso} , {$key}");

					if($nao_somar) {

						log_relatorio('debug', "Não Somar");

						$lines[$index_concurso] = array(
							'nome'				=> $index_concurso,
							'qtde_alunos' 		=> 0,
							'total' 			=> 0,
							'discount'			=> 0,
							'valor_pago'		=> 0,
							'pagseguro'			=> 0,
							'folha_dirigida'	=> 0,
							'valor_imposto' 	=> 0,
							'liquido' 			=> 0,
							'cursos'			=> array($key => $subline)
						);

					}
					else {
						log_relatorio('debug', "Pode Somar");

						$lines[$index_concurso] = array(
							'nome'				=> $index_concurso,
							'qtde_alunos' 		=> $subline['qtde_alunos_reais'],
							'total' 			=> $subline['total'],
							'discount'			=> $subline['discount'],
							'valor_pago'		=> $subline['valor_pago'],
							'pagseguro'			=> $subline['pagseguro'],
							'folha_dirigida'	=> $subline['folha_dirigida'],
							'valor_imposto' 	=> $subline['valor_imposto'],
							'liquido' 			=> $subline['liquido'],
							'cursos'			=> array($key => $subline)
						);

					}
				}
				else {
					log_relatorio('debug', "Já existe linha do index: {$index_concurso} , {$key}");

					if($nao_somar) {
						log_relatorio('debug', "Não Somar");

						$lines[$index_concurso]['cursos'] = array_merge($lines[$index_concurso]['cursos'], array($key => $subline));
					}
					else {
						log_relatorio('debug', "Pode Somar");

						$lines[$index_concurso] = array(
							'nome'				=> $index_concurso,
							'qtde_alunos' 		=> $lines[$index_concurso]['qtde_alunos'] + $subline['qtde_alunos_reais'],
							'total' 			=> $lines[$index_concurso]['total'] + $subline['total'],
							'discount'			=> $lines[$index_concurso]['discount'] + $subline['discount'],
							'valor_pago'		=> $lines[$index_concurso]['valor_pago'] + $subline['valor_pago'],
							'pagseguro'			=> $lines[$index_concurso]['pagseguro'] + $subline['pagseguro'],
							'folha_dirigida'	=> $lines[$index_concurso]['folha_dirigida'] + $subline['folha_dirigida'],
							'valor_imposto' 	=> $lines[$index_concurso]['valor_imposto'] + $subline['valor_imposto'],
							'liquido' 			=> $lines[$index_concurso]['liquido'] + $subline['liquido'],
							'cursos' 			=> array_merge($lines[$index_concurso]['cursos'], array($key => $subline))
						);
					}
				}

				if(is_produto_simulado($subline['curso_id'])) {

					$nao_somar_pacote = ($subline['item_tipo'] == 1) ? TRUE : FALSE;

					if(empty($line_simulados)) {

						if($nao_somar_pacote) {

							$line_simulados = array(
								'nome'				=> $index_concurso,
								'qtde_alunos' 		=> 0,
								'total' 			=> 0,
								'discount'			=> 0,
								'valor_pago'		=> 0,
								'pagseguro'			=> 0,
								'folha_dirigida'	=> 0,
								'valor_imposto' 	=> 0,
								'liquido' 			=> 0,
								'cursos'			=> array($key => $subline)
							);

						}
						else {
							$line_simulados = array(
								'nome'				=> 'Simulados',
								'qtde_alunos' 		=> $subline['qtde_alunos_reais'],
								'total' 			=> $subline['total'],
								'discount'			=> $subline['discount'],
								'valor_pago'		=> $subline['valor_pago'],
								'pagseguro'			=> $subline['pagseguro'],
								'folha_dirigida'	=> $subline['folha_dirigida'],
								'valor_imposto' 	=> $subline['valor_imposto'],
								'liquido' 			=> $subline['liquido'],
								'cursos'			=> array($key => $subline)
							);
						}
					}
					else {
						if($nao_somar_pacote) {
							$line_simulados['cursos'] = array_merge($line_simulados['cursos'], array($key => $subline));
						}
						else {
							$line_simulados = array(
								'nome'				=> 'Simulados',
								'qtde_alunos' 		=> $line_simulados['qtde_alunos'] + $subline['qtde_alunos_reais'],
								'total' 			=> $line_simulados['total'] + $subline['total'],
								'discount'			=> $line_simulados['discount'] + $subline['discount'],
								'valor_pago'		=> $line_simulados['valor_pago'] + $subline['valor_pago'],
								'pagseguro'			=> $line_simulados['pagseguro'] + $subline['pagseguro'],
								'folha_dirigida'	=> $line_simulados['folha_dirigida'] + $subline['folha_dirigida'],
								'valor_imposto' 	=> $line_simulados['valor_imposto'] + $subline['valor_imposto'],
								'liquido' 			=> $line_simulados['liquido'] + $subline['liquido'],
								'cursos' 			=> array_merge($line_simulados['cursos'], array($key => $subline))
							);
						}
					}

				}
			}
		}
	}

	// ksort($lines);

	if(tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ))) {
		$lines['Simulados'] = $line_simulados;
	}


	if($lines) {

		$nome = [];
		$valor = [];
		foreach ($lines as $key => $row) {
		    $nome[$key]  = $row['nome'];
		    $valor[$key] = $row['liquido'];
		}

		switch ($ordem) {
			case 1:
				array_multisort($nome, SORT_ASC, $valor, SORT_ASC, $lines);
				break;
			case 2:
				array_multisort($nome, SORT_DESC, $valor, SORT_DESC, $lines);
				break;
			case 3:
				array_multisort($valor, SORT_DESC, $nome, SORT_DESC, $lines);
				break;
			case 4:
				array_multisort($valor, SORT_ASC, $nome, SORT_ASC, $lines);
				break;
		}

	}


	$suplines = [];

	foreach ($lines as $key => $line) {

		$area = null;

		if(isset($line['cursos']) && $line['cursos']) {

			foreach ($line['cursos'] as $subkey => $subline) {
				$area = get_primeira_area($subline['curso_id']);
				break;
			}

		}

		if(!$area) {
			continue;
		}

		if($area == "SQ") {
			$index_area = "Área Sistema de Questões";
			$index_area_id = "-1";

			// $coordenador = listar_coordenadores_sq_por_data($data_inicio);

			// if($coordenador) {
			// 	$u = get_usuario_array($coordenador[0]->user_id);
			// 	$coordenador = $u['nome_completo'];
			// 	$index_coordenador_id = $u['id'];
			// }
			// else {
			// 	$coordenador = "(Sem coordenador)";
			// 	$index_coordenador_id = -1;
			// }
		}
		else {
			$index_area = $area->name;
			$index_area_id = $area->term_id;

			// $coordenador = listar_coordenadores_areas_por_data($data_inicio, $area->term_id);
			// if($coordenador) {
			// 	$u = get_usuario_array($coordenador[0]->user_id);
			// 	$coordenador = $u['nome_completo'];
			// 	$index_coordenador_id = $u['id'];
			// }
			// else {
			// 	$coordenador = "(Sem coordenador)";
			// 	$index_coordenador_id = -1;
			// }

		}
		// echo "<pre>";
		// print_r($line);

		// $index_coordenador_id = $line['coordenador_id'];
		// echo $index_coordenador_id;
		// $u = get_usuario_array($index_coordenador_id);

		if($u_coordenador) {
			$coordenador = $u['nome_completo'];
		}
		else {
			$coordenador = "(Sem coordenador)";
		}

		// filtro por área e por professor
		if($area_ids && !in_array($index_area_id, $area_ids)) {
			continue;
		}

		if($coordenador_ids && !in_array($index_coordenador_id, $coordenador_ids)) {
			continue;
		}

		if(empty($suplines[$index_area])) {

			if($nao_somar) {

				$suplines[$index_area] = array(
					'nome'				=> $index_area,
					'coordenador'		=> $coordenador,
					'qtde_alunos' 		=> 0,
					'total' 			=> 0,
					'discount'			=> 0,
					'valor_pago'		=> 0,
					'pagseguro'			=> 0,
					'folha_dirigida'	=> 0,
					'valor_imposto' 	=> 0,
					'liquido' 			=> 0,
					'concursos'			=> array($key => $line)
				);

			}
			else {
				$suplines[$index_area] = array(
					'nome'				=> $index_area,
					'coordenador'		=> $coordenador,
					'qtde_alunos' 		=> $line['qtde_alunos'],
					'total' 			=> $line['total'],
					'discount'			=> $line['discount'],
					'valor_pago'		=> $line['valor_pago'],
					'pagseguro'			=> $line['pagseguro'],
					'folha_dirigida'	=> $line['folha_dirigida'],
					'valor_imposto' 	=> $line['valor_imposto'],
					'liquido' 			=> $line['liquido'],
					'concursos'			=> array($key => $line)
				);

			}
		}
		else {
			if($nao_somar) {
				$suplines[$index_area]['concursos'] = array_merge($suplines[$index_area]['concursos'], array($key => $line));
			}
			else {

				$suplines[$index_area] = array(
					'nome'				=> $index_area,
					'coordenador'		=> $coordenador,
					'qtde_alunos' 		=> $suplines[$index_area]['qtde_alunos'] + $line['qtde_alunos'],
					'total' 			=> $suplines[$index_area]['total'] + $line['total'],
					'discount'			=> $suplines[$index_area]['discount'] + $line['discount'],
					'valor_pago'		=> $suplines[$index_area]['valor_pago'] + $line['valor_pago'],
					'pagseguro'			=> $suplines[$index_area]['pagseguro'] + $line['pagseguro'],
					'folha_dirigida'	=> $suplines[$index_area]['folha_dirigida'] + $line['folha_dirigida'],
					'valor_imposto' 	=> $suplines[$index_area]['valor_imposto'] + $line['valor_imposto'],
					'liquido' 			=> $suplines[$index_area]['liquido'] + $line['liquido'],
					'concursos' 		=> array_merge($suplines[$index_area]['concursos'], array($key => $line))
				);
			}
		}

	}

	// exit;

	if($suplines) {

		$nome = [];
		$valor = [];
		foreach ($suplines as $key => $row) {
		    $nome[$key]  = $row['nome'];
		    $valor[$key] = $row['liquido'];
		}

		switch ($ordem) {
			case 1:
				array_multisort($nome, SORT_ASC, $valor, SORT_ASC, $suplines);
				break;
			case 2:
				array_multisort($nome, SORT_DESC, $valor, SORT_DESC, $suplines);
				break;
			case 3:
				array_multisort($valor, SORT_DESC, $nome, SORT_DESC, $suplines);
				break;
			case 4:
				array_multisort($valor, SORT_ASC, $nome, SORT_ASC, $suplines);
				break;
		}

	}

	// echo "<pre>";
	// print_r($suplines);
	// exit;

	return $suplines;
}

function listar_vendas_agrupadas_por_professor($data_inicio, $data_fim, $ordem = 3, $pago_id = null)
{
    KLoader::helper("StringHelper");
    
	global $wpdb;

	$where_professor = $pago_id ? " AND vp.vep_professor_id = $pago_id " : "";

	$query = "SELECT DISTINCT v.*, vp.*, u.display_name as nome_aluno, u.user_email as email_aluno,
			u2.display_name as nome_professor, um.meta_value as cpf_aluno,
			p.post_title as nome_curso
			FROM wp_vendas v
			LEFT JOIN wp_vendas_professores vp ON v.ven_id = vp.ven_id
			LEFT JOIN wp_users u ON v.ven_aluno_id = u.ID
			LEFT JOIN wp_usermeta um ON v.ven_aluno_id = um.user_id
			LEFT JOIN wp_posts p ON v.ven_curso_id = p.ID
			LEFT JOIN wp_users u2 ON vp.vep_professor_id = u2.ID
			WHERE v.ven_order_id NOT IN (SELECT pedido_oculto_id FROM premium_log) AND um.meta_key = 'oneTarek_CPF'
			{$where_professor}
			AND v.ven_data BETWEEN '{$data_inicio} 00:00:00'
			AND '{$data_fim} 23:59:59'";

	$result = $wpdb->get_results($query, ARRAY_A);

	$lines = array();
	$ids = array();
	$nome = [];
	$exponencial = [];
	$pago = [];

	foreach ($result as $row) {

		if(empty($row['vep_participacao'])) $row['vep_participacao'] = 100;

		$index_professor = $row['nome_professor'];

		if(empty($index_professor)) {
			if($row['ven_item_tipo'] == 1) continue;
			if(($row['ven_item_tipo'] == 0 ||  $row['ven_item_tipo'] == 2) && $row['vep_professor_id'] == -1) continue; // trata produtos com mais de um professor

			$index_professor = "(Sem professor)";
		}

		$total = $row['ven_valor_venda'] * ($row['vep_participacao'] /100);
		$desconto = $row['ven_desconto'] * ($row['vep_participacao'] /100);
		$reembolso = $row['ven_reembolso'] * ($row['vep_participacao'] /100);
		$valor_pago = $total - $desconto - $reembolso;
		$aliquota =  $row['ven_aliquota'];
		$valor_imposto = $aliquota / 100 * $valor_pago;
		$pag_seguro = $row['ven_pagseguro']  * ($row['vep_participacao'] /100);
		$folha_dirigida = $row['ven_folha_dirigida']  * $valor_pago;
		$liquido = $valor_pago - $valor_imposto - $pag_seguro - $folha_dirigida;
		$percentual_professor = $row['ven_item_tipo'] == 1 ? 0 : $row['vep_professor_lucro'];
		$lucro_professor = $row['ven_item_tipo'] == 1 ? 0 : ($percentual_professor / 100) * $liquido;
		$pago_id = $row['vep_professor_id'];


		if(empty($lines[$index_professor])) {

			$lines[$index_professor] = array(
				'nome_professor'	=> $row['nome_professor'],
				'slug_professor'	=> StringHelper::remover_acentos($row['nome_professor']),
				'qtde_alunos' 		=> 1,
				'total' 			=> $total,
				'discount'			=> $desconto,
				'reembolso'			=> $reembolso,
				'valor_pago'		=> $valor_pago,
				'lucro_professor'	=> $lucro_professor,
				'pagseguro'			=> $pag_seguro,
				'folha_dirigida'	=> $folha_dirigida,
				'valor_imposto' 	=> $valor_imposto,
				'liquido' 			=> $liquido,
				'professor_id'		=> $pago_id,
				'exponencial'		=> $liquido - $lucro_professor
			);
		}
		else {
			$lines[$index_professor] = array(
				'nome_professor'	=> $row['nome_professor'],
				'slug_professor'	=> StringHelper::remover_acentos($row['nome_professor']),
				'qtde_alunos' 		=> $lines[$index_professor]['qtde_alunos'] + 1,
				'total' 			=> $lines[$index_professor]['total'] + $total,
				'discount'			=> $lines[$index_professor]['discount'] + $desconto,
				'reembolso'			=> $lines[$index_professor]['reembolso'] + $reembolso,
				'valor_pago'		=> $lines[$index_professor]['valor_pago'] + $valor_pago,
				'lucro_professor'	=> $lines[$index_professor]['lucro_professor'] + $lucro_professor,
				'pagseguro'			=> $lines[$index_professor]['pagseguro'] + $pag_seguro,
				'folha_dirigida'	=> $lines[$index_professor]['folha_dirigida'] + $folha_dirigida,
				'valor_imposto' 	=> $lines[$index_professor]['valor_imposto'] + $valor_imposto,
				'liquido'			=> $lines[$index_professor]['liquido'] + $liquido,
				'professor_id'		=> $pago_id,
				'exponencial'		=> $lines[$index_professor]['exponencial'] + ($liquido - $lucro_professor)
			);
		}

		array_push($ids, $pago_id);

		$nome[$index_professor]  = $lines[$index_professor]['slug_professor'];
		$exponencial[$index_professor] = $lines[$index_professor]['exponencial'];
		$pago[$index_professor] = $lines[$index_professor]['lucro_professor'];
	}

	if(!$pago_id) {

		$outros = listar_professores_exceto($ids);
		foreach ($outros as $outro) {
			$lines[$outro->display_name] = array(
					'nome_professor'	=> $outro->display_name,
					'slug_professor'	=> StringHelper::remover_acentos($outro->display_name),
					'qtde_alunos' 		=> 0,
					'total' 			=> 0,
					'discount'			=> 0,
					'reembolso'			=> 0,
					'valor_pago'		=> 0,
					'lucro_professor'	=> 0,
					'pagseguro'			=> 0,
					'folha_dirigida'	=> 0,
					'valor_imposto' 	=> 0,
					'liquido'			=> 0,
					'professor_id'		=> $outro->ID
			);

			$nome[$outro->display_name]  = $lines[$outro->display_name]['slug_professor'];
			$exponencial[$outro->display_name] = $lines[$outro->display_name]['exponencial'];
			$pago[$outro->display_name] = $lines[$outro->display_name]['lucro_professor'];
		}

	}

	if($lines) {

		switch ($ordem) {
			case 1:
				array_multisort($nome, SORT_ASC, $exponencial, SORT_ASC, $lines);
				break;
			case 2:
				array_multisort($nome, SORT_DESC, $exponencial, SORT_DESC, $lines);
				break;
			case 3:
				array_multisort($exponencial, SORT_DESC, $nome, SORT_DESC, $lines);
				break;
			case 4:
				array_multisort($exponencial, SORT_ASC, $nome, SORT_ASC, $lines);
				break;

			case 5:
				array_multisort($pago, SORT_DESC, $nome, SORT_DESC, $lines);
				break;
			case 6:
				array_multisort($pago, SORT_ASC, $nome, SORT_ASC, $lines);
				break;
		}

	}

	return $lines;
}

function listar_vendas_agrupadas_por_concurso($start_date, $end_date, $ordem = 3, $campo = 'name')
{
	global $wpdb;

	$query_start_date = date('Y-m-d', strtotime(str_replace('/','-',$start_date)));
	$query_end_date = date('Y-m-d', strtotime(str_replace('/','-',$end_date)));

	$ordem = isset($_POST['ordem']) ? $_POST['ordem'] : 3;

	$query_professor = '';

	if(is_coordenador()) {
		$query_professor .= ' AND ( 1=0 ';

		$user_id = get_current_user_id();

		if(is_coordenador_area($user_id)) {
			$query_professor .=  " OR v.ven_curso_id IN ( SELECT object_id FROM wp_term_relationships tr JOIN wp_term_taxonomy tt ON tt.term_taxonomy_id = tr.term_taxonomy_id WHERE term_id IN (
				SELECT area_id FROM coordenadores_areas WHERE user_id = {$user_id}
			) ) ";
		}

		if(is_coordenador_sq($user_id)) {
			// Os numeros se referem aos ids das categorias de:
			// assinaturas -> 211, 212, 213, 214
			// cadernos -> 418, 303
			// simulados -> 124

			$query_professor .=  " OR v.ven_curso_id IN ( SELECT object_id FROM wp_term_relationships tr JOIN wp_term_taxonomy tt ON tt.term_taxonomy_id = tr.term_taxonomy_id WHERE term_id IN (418, 303, 124, 211, 212, 213, 214
			) ) ";
		}

		if(is_coordenador_coaching($user_id) && !$_POST['meus_produtos']) {
			$query_professor .= " OR v.ven_curso_id IN ( SELECT object_id FROM wp_term_relationships tr JOIN wp_term_taxonomy tt ON tt.term_taxonomy_id = tr.term_taxonomy_id WHERE term_id = 413
			) ";
		}

		$query_professor .= ')';
	}

	$query = "SELECT DISTINCT v.ven_id, v.ven_curso_id, v.ven_valor_venda, v.ven_desconto, v.ven_reembolso, v.ven_aliquota, v.ven_pagseguro,
                v.ven_folha_dirigida, v.ven_item_tipo, vp.vep_nao_somar, vp.vep_participacao, vp.vep_professor_perc, p.post_title as nome_curso
				FROM wp_vendas v
				LEFT JOIN wp_vendas_professores vp ON v.ven_id = vp.ven_id
				LEFT JOIN wp_posts p ON v.ven_curso_id = p.ID
				WHERE v.ven_order_id NOT IN (SELECT pedido_oculto_id FROM premium_log) AND v.ven_data BETWEEN '{$query_start_date} 00:00:00'
				AND '{$query_end_date} 23:59:59' {$query_professor}
				";
//echo $query;exit;
	$result = $wpdb->get_results($query, ARRAY_A);

	$sublines = array();
	$nome = [];
	$valor = [];
	$pago = [];


	foreach ($result as $row) {
		if(empty($row['vep_participacao'])) $row['vep_participacao'] = 100;

		$index_curso = $row['nome_curso'];

		if(empty($index_curso)) continue;

		if(($row['ven_item_tipo'] == 0 ||  $row['ven_item_tipo'] == 2) && $row['vep_nao_somar'] == 1) continue; // trata produtos com mais de um professor

		// echo "<pre>";
		$total = $row['ven_valor_venda'] * ($row['vep_participacao'] /100);
		$desconto = $row['ven_desconto'] * ($row['vep_participacao'] /100);
		$reembolso = $row['ven_reembolso'] * ($row['vep_participacao'] /100);
		$valor_pago = $total - $desconto - $reembolso;
		$aliquota =  $row['ven_aliquota'];
		$valor_imposto = $aliquota / 100 * $valor_pago;
		$pag_seguro = $row['ven_pagseguro']  * ($row['vep_participacao'] /100);
		$folha_dirigida = $row['ven_folha_dirigida'] * $valor_pago;
		$liquido = $valor_pago - $valor_imposto - $pag_seguro - $folha_dirigida;

		// $percentual_professor = $row['vep_prof_percentual'];
		$lucro_professor = $row['vep_professor_perc'] / 100 * $liquido;
		$liquido_pos_professor = $liquido - $lucro_professor;

		if(empty($sublines[$index_curso])) {

			$sublines[$index_curso] = array(
				'nome'				=> $index_curso,
				'qtde_alunos' 		=> 1,
				'total' 			=> $total,
				'discount'			=> $desconto,
				'reembolso'			=> $reembolso,
				'valor_pago'		=> $valor_pago,
				'pagseguro'			=> $pag_seguro,
				'folha_dirigida'	=> $folha_dirigida,
				'valor_imposto' 	=> $valor_imposto,
				'liquido' 			=> $liquido,
				'lucro_professor'	=> $lucro_professor ?? 0,
				'liquido_pos_professor' => $liquido_pos_professor,
				'curso_id'	 		=> $row['ven_curso_id'],
				'item_tipo'			=> $row['ven_item_tipo'],
				'qtde_alunos_reais'	=> is_aluno_real($row) ? 1 : 0,
			);
		}
		else {

			$sublines[$index_curso] = array(
				'nome'				=> $index_curso,
				'qtde_alunos' 		=> $sublines[$index_curso]['qtde_alunos'] + 1,
				'total' 			=> $sublines[$index_curso]['total'] + $total,
				'discount'			=> $sublines[$index_curso]['discount'] + $desconto,
				'reembolso'			=> $sublines[$index_curso]['reembolso'] + $reembolso,
				'valor_pago'		=> $sublines[$index_curso]['valor_pago'] + $valor_pago,
				'pagseguro'			=> $sublines[$index_curso]['pagseguro'] + $pag_seguro,
				'folha_dirigida'	=> $sublines[$index_curso]['folha_dirigida'] + $folha_dirigida,
				'valor_imposto' 	=> $sublines[$index_curso]['valor_imposto'] + $valor_imposto,
				'liquido'			=> $sublines[$index_curso]['liquido'] + $liquido,
				'lucro_professor'	=> ($sublines[$index_curso]['lucro_professor'] ?? 0) + $lucro_professor,
				'liquido_pos_professor' => $sublines[$index_curso]['liquido_pos_professor'] + $liquido_pos_professor,
				'curso_id'	 		=> $row['ven_curso_id'],
				'item_tipo'			=> $row['ven_item_tipo'],
				'qtde_alunos_reais'	=> is_aluno_real($row) ? $sublines[$index_curso]['qtde_alunos_reais'] + 1 : $sublines[$index_curso]['qtde_alunos_reais'],
			);
		}

		$nome[$index_curso]  = strtolower($sublines[$index_curso]['nome']);
		$valor[$index_curso] = $sublines[$index_curso]['liquido'];
		$pago[$index_curso] = $sublines[$index_curso]['valor_pago'];
	}

	if($sublines) {

		switch ($ordem) {
			case 1:
				array_multisort($nome, SORT_ASC, $valor, SORT_ASC, $sublines);
				break;
			case 2:
				array_multisort($nome, SORT_DESC, $valor, SORT_DESC, $sublines);
				break;
			case 3:
				array_multisort($valor, SORT_DESC, $nome, SORT_DESC, $sublines);
				break;
			case 4:
				array_multisort($valor, SORT_ASC, $nome, SORT_ASC, $sublines);
				break;

			case 5:
				array_multisort($pago, SORT_DESC, $nome, SORT_DESC, $sublines);
				break;
			case 6:
				array_multisort($pago, SORT_ASC, $nome, SORT_ASC, $sublines);
				break;
		}

	}

	$lines = array();
	$nome = [];
	$valor = [];
	$pago = [];

	foreach ($sublines as $key => $subline) {

		$categorias = listar_categorias_relatorio($subline['curso_id']);

		$categorias_ids = [];
		foreach ($categorias as $categoria) {
		    $categorias_ids[] = $categoria->term_id;
		}

//		if($subline['curso_id'] != 646654	) continue;
//        print_r($categorias);exit;

		if($categorias) {

            $max_hierarquia = get_curso_max_hierarquia($categorias_ids);

			foreach ($categorias as $categoria) {
				$index_concurso = $categoria->slug;
				$index_concurso_nome = $categoria->$campo;

				if(in_array($index_concurso, [CATEGORIA_ASSINATURA_SQ_QUINZENAL, CATEGORIA_ASSINATURA_SQ_MENSAL, CATEGORIA_ASSINATURA_SQ_TRIMESTRAL, CATEGORIA_ASSINATURA_SQ_SEMESTRAL, CATEGORIA_ASSINATURA_SQ_ANUAL])) {
					$index_concurso = CATEGORIA_ASSINATURA_SQ;
				}

				/**
				 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/1931
				 *
				 * LUCAS SALVETTI: O que pedi de ajuste é que o VALOR dos cadernos saiam do
				 * somatório dos "concursos", pra não misturar no pagamento dos coordenadores e
				 * ser possível validar os totais entre os relatórios (pra detectar erro)
				 */
				$nao_somar = calcular_nao_somar($subline['item_tipo'], $index_concurso, $categorias_ids, $max_hierarquia);

				$subline['nao_somar'] = $nao_somar;

				if(empty($lines[$index_concurso_nome])) {


					if($nao_somar) {

						$lines[$index_concurso_nome] = array(
							'nome'				=> $index_concurso_nome,
							'qtde_alunos' 		=> 0,
							'total' 			=> 0,
							'discount'			=> 0,
							'reembolso'			=> 0,
							'valor_pago'		=> 0,
							'pagseguro'			=> 0,
							'folha_dirigida'	=> 0,
							'valor_imposto' 	=> 0,
							'liquido' 			=> 0,
							'lucro_professor'	=> 0,
							'liquido_pos_professor'	=> 0,
							'cursos'			=> array($key => $subline)
						);

					}
					else {

						$lines[$index_concurso_nome] = array(
							'nome'				=> $index_concurso_nome,
							'qtde_alunos' 		=> $subline['qtde_alunos_reais'],
							'total' 			=> $subline['total'],
							'discount'			=> $subline['discount'],
							'reembolso'			=> $subline['reembolso'],
							'valor_pago'		=> $subline['valor_pago'],
							'pagseguro'			=> $subline['pagseguro'],
							'folha_dirigida'	=> $subline['folha_dirigida'],
							'valor_imposto' 	=> $subline['valor_imposto'],
							'liquido' 			=> $subline['liquido'],
							'lucro_professor'	=> $subline['lucro_professor'],
							'liquido_pos_professor'	=> $subline['liquido_pos_professor'],
							'cursos'			=> array($key => $subline)
						);

					}
				}
				else {

					if($nao_somar) {

						$lines[$index_concurso_nome]['cursos'] = array_merge($lines[$index_concurso_nome]['cursos'], array($key => $subline));
					}
					else {

						$lines[$index_concurso_nome] = array(
							'nome'				=> $index_concurso_nome,
							'qtde_alunos' 		=> $lines[$index_concurso_nome]['qtde_alunos'] + $subline['qtde_alunos_reais'],
							'total' 			=> $lines[$index_concurso_nome]['total'] + $subline['total'],
							'discount'			=> $lines[$index_concurso_nome]['discount'] + $subline['discount'],
							'reembolso'			=> $lines[$index_concurso_nome]['reembolso'] + $subline['reembolso'],
							'valor_pago'		=> $lines[$index_concurso_nome]['valor_pago'] + $subline['valor_pago'],
							'pagseguro'			=> $lines[$index_concurso_nome]['pagseguro'] + $subline['pagseguro'],
							'folha_dirigida'	=> $lines[$index_concurso_nome]['folha_dirigida'] + $subline['folha_dirigida'],
							'valor_imposto' 	=> $lines[$index_concurso_nome]['valor_imposto'] + $subline['valor_imposto'],
							'liquido' 			=> $lines[$index_concurso_nome]['liquido'] + $subline['liquido'],
							'lucro_professor' 			=> $lines[$index_concurso_nome]['lucro_professor'] + $subline['lucro_professor'],
							'liquido_pos_professor' 			=> $lines[$index_concurso_nome]['liquido_pos_professor'] + $subline['liquido_pos_professor'],
							'cursos' 			=> array_merge($lines[$index_concurso_nome]['cursos'], array($key => $subline))
						);
					}
				}

				$nome[$index_concurso_nome]  = strtolower($lines[$index_concurso_nome]['nome']);
				$valor[$index_concurso_nome] = $lines[$index_concurso_nome]['liquido'];
				$pago[$index_concurso_nome] = $lines[$index_concurso_nome]['valor_pago'];

			}
		}
	}
//exit;
	// ksort($lines);

	if($lines) {

		switch ($ordem) {
			case 1:
				array_multisort($nome, SORT_ASC, $valor, SORT_ASC, $lines);
				break;
			case 2:
				array_multisort($nome, SORT_DESC, $valor, SORT_DESC, $lines);
				break;
			case 3:
				array_multisort($valor, SORT_DESC, $nome, SORT_DESC, $lines);
				break;
			case 4:
				array_multisort($valor, SORT_ASC, $nome, SORT_ASC, $lines);
				break;
			case 5:
				array_multisort($pago, SORT_DESC, $nome, SORT_DESC, $lines);
				break;
			case 6:
				array_multisort($pago, SORT_ASC, $nome, SORT_ASC, $lines);
				break;
		}

	}

	return $lines;
}

function calcular_nao_somar($item, $index_concurso, $categorias_ids, $max_hierarquia)
{
    return calcular_nao_somar_item_pacote($item)
        || calcular_nao_somar_fora_de_sua_categoria($index_concurso, $categorias_ids, $max_hierarquia)
        || calcular_nao_somar_na_hierarquia($index_concurso, $categorias_ids);
}

function calcular_nao_somar_item_pacote($item_tipo)
{
    if($item_tipo == 1) {
        return true;
    }

    return false;
}

function calcular_nao_somar_fora_de_sua_categoria($index_concurso, $categorias_ids, $max_hierarquia)
{
    $categorias_agrupadas = get_lista_categorias_agrupadas();

    foreach ($categorias_agrupadas as $id => $slug) {

         if($index_concurso != $slug && in_array($id, $categorias_ids)) {

             if($index_concurso != $max_hierarquia) {
                 return true;
             }
         }
     }

    return false;
}

function calcular_nao_somar_na_hierarquia($index_concurso, $categorias_ids)
{
    $hierarquia = false;

    $categorias_agrupadas = get_lista_categorias_agrupadas();

    foreach ($categorias_agrupadas as $id => $slug) {

        $is_tipo = in_array($id, $categorias_ids);

        if($index_concurso == $slug && $is_tipo && $hierarquia) {
            return true;
        }

        if($is_tipo) {
            $hierarquia = true;
        }
    }

    return false;
}