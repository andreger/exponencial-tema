<?php
function get_produtos_ids_de_pacote_antigo($product_id)
{
	switch ($product_id) {
	
	// 4456 - Pacote de Exatas – ICMS RJ – 3 Cursos
	case 4456: {
		return array(4247, 3533, 3791);
	}
	
	// 4507 - Pacote de Cursos – ICMS RJ – 11 Cursos
	case 4507: {
		return array(3791, 4247, 3533, 3545, 3946, 4013, 3537, 3542, 3548, 3529, 4540);
	}
	
	// 6084 - Pacote de Exatas – Auditor SEFAZ PI 2014 – 2 Cursos
	case 6084: {
		return array(4247, 6024);
	}
	
	// 6087 - Pacote de Exatas – Analista SEFAZ PI 2014 – 3 Cursos
	case 6087: {
		return array(6035, 5942, 6026);
	}
	
	// 6088- Pacote de Cursos – ICMS RJ – 13 Cursos
	case 6088: {
		return array(6105, 6065, 3791, 4247, 3545, 3533, 3946, 4013, 3537, 3542, 3548, 3529, 4540);
	}
	
	// 6114 - Pacote G1 – ICMS RJ – 3 Cursos
	case 6114: {
		return array(3545, 6105, 3946);
	}
	
	// 6117 - Pacote – Agente PF – 5 Cursos
	case 6117: {
		return array(5279, 5733, 5272, 5276, 5267);	
	}
	
	// 7195 - Pacote de Cursos – ICMS RJ – 14 Cursos
	case 7195: {
		return array(6412, 3545, 3946, 6105, 3529, 3542, 3548, 3537, 4013, 3533, 6756, 6065, 4247, 3791);
	}
	
	// 7855 - Pacote de Cursos – Técnico INSS
	case 7855: {
		return array(7064, 7055, 6397, 6617, 6407, 7149, 7854);
	}
	
	
	}
}
?>