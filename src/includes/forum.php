<?php
function criar_forum($produto_id)
{
	$produto = get_produto_by_id($produto_id);

	$forum_data = array (
		'post_content' => $produto->post->post_content,
		'tags_input' => array (),
		'post_status' => 'private',
		'post_title' => $produto->post->post_title
	);

	$forum_id = bbp_insert_forum ( $forum_data );

	update_post_meta ( $produto_id, 'forum_id', $forum_id );
	update_post_meta ( $forum_id, 'product_id', $produto_id );
}

function increver_aluno_em_forum($forum_id, $user_id)
{
	if( ! empty( $forum_id ) ) {
		bbp_add_user_forum_subscription( $user_id, $forum_id);
	}
}

function increver_aluno_em_forum_de_produto($produto_id, $user_id)
{
	$forum_id = get_post_meta($produto_id, 'forum_id', true );
	increver_aluno_em_forum($forum_id, $user_id);
}

function is_topico_acessivel($topic_id) {
	if(current_user_can( 'administrator' ))
		return true;
	
	if(is_user_forum_moderador(get_current_user_id(),
			bbp_get_topic_forum_id($topic_id)))
		return true;
			
	if(is_autor_topico($topic_id))
		return true;
	
	if(!is_bbp_post_pendente($topic_id))
		return true;
	
	return false;
}

function is_reply_acessivel($reply_id) {
	if (current_user_can ( 'administrator' ))
		return true;
	
	if (is_user_forum_moderador ( get_current_user_id (), 
			bbp_get_reply_forum_id ( $reply_id ) ))
		return true;
	
	if (is_autor_reply ( $reply_id ))
		return true;
	
	if (! is_bbp_post_pendente ( $reply_id ))
		return true;
	
	return false;
}

function get_forum_moderadores($forum_id) 
{
	$forum = bbp_get_forum($forum_id);
	$produto = get_produto_by_forum_id($forum_id);
	
	return get_autores($produto->post);
}

function is_autor_topico($topic_id) {
	$autor_id = bbp_get_topic_author_id($topic_id);
	if($autor_id == get_current_user_id())
		return true;
	
	return false;
}

function is_autor_reply($reply_id) {
	$autor_id = bbp_get_reply_author_id($reply_id);
	if($autor_id == get_current_user_id())
		return true;

	return false;
}

function is_autor_and_reply_pendente($reply_id) {
	$is_autor = is_autor_reply($reply_id);
	$is_pendente = is_bbp_post_pendente($reply_id);
	if($is_autor && $is_pendente)
		return true;
	
	return false;
}

function is_autor_and_topic_pendente($topic_id) {
	$is_autor = is_autor_topico($topic_id);
	$is_pendente = is_bbp_post_pendente($topic_id);
	if($is_autor && $is_pendente)
		return true;

	return false;
}

function is_user_forum_moderador($user_id, $forum_id) 
{
	if(current_user_can( 'administrator' ))
		return true;

	foreach ( get_forum_moderadores ( $forum_id ) as $moderador ) {
		
		if ($moderador->ID == $user_id)
			return true;
	}
	
	return false;
}

function get_forum_link_from_product_id($product_id)
{
	$forum_id = get_post_meta($product_id, 'forum_id', true );
	$forum = bbp_get_forum($forum_id);

	if(isset($forum)) {
		if($forum->post_status == 'hidden') {
			return '';
		}
		else {
			if(is_forum_expirado($forum_id)) {
				return "F&oacute;rum Expirado!";
			}
			$forum_title = bbp_get_forum_title($forum_id);
			$link = bbp_get_forum_permalink($forum_id);
			return "<a class='bbp-forum-title' href='$link'>$forum_title</a>";
		}
	} else {
		return '';
	}
}

function get_forum_url_from_product_id($product_id)
{
	$forum_id = get_post_meta($product_id, 'forum_id', true );
	$forum = bbp_get_forum($forum_id);

	if(isset($forum)) {
		if($forum->post_status == 'hidden') {
			return false;
		}
		else {
			if(is_forum_expirado($forum_id)) {
				return false;
			}
			$forum_title = bbp_get_forum_title($forum_id);
			$link = bbp_get_forum_permalink($forum_id);
			return $link;
		}
	} else {
		return false;
	}
}

function redirecionar_se_forum_expirado($forum_id) {
	if (is_forum_expirado ($forum_id) && !is_autor_forum($forum_id, get_current_user_id ())) {
		wp_redirect('/forum-expirado');
		exit;
	}
}

function get_string_expiracao_forum($forum_id) {
	$product = get_produto_by_forum_id($forum_id);
	$data_produto = get_post_meta($product->id, FORUM_EXPIRADO_POST_META, true);
	
	if(empty($data_produto))
		return false;

	return $data_produto;
}

function contar_topicos_e_respostas_pendentes($forum_id) 
{
	global $wpdb;
	
	$sql = "SELECT * FROM wp_postmeta pm, wp_posts p WHERE pm.post_id=p.ID AND pm.meta_key = '_bbp_forum_id' AND pm.meta_value = $forum_id AND post_status = 'pending'";
	
	$result = $wpdb->get_results ( $sql );
	
	return count ( $result );
}

function contar_topicos_pendentes($forum_id) 
{
	global $wpdb;
	
	$sql = "SELECT * FROM wp_postmeta pm, wp_posts p WHERE p.post_type = 'topic' AND pm.post_id=p.ID AND pm.meta_key = '_bbp_forum_id' AND pm.meta_value = $forum_id AND post_status = 'pending'";
	
	$result = $wpdb->get_results ( $sql );
	
	return count ( $result );
}

function contar_respostas_pendentes($forum_id)
{
	global $wpdb;

	$sql = "SELECT * FROM wp_postmeta pm, wp_posts p WHERE p.post_type = 'reply' AND pm.post_id=p.ID AND pm.meta_key = '_bbp_forum_id' AND pm.meta_value = $forum_id AND post_status = 'pending'";

	$result = $wpdb->get_results ( $sql );

	return count ( $result );
}

function contar_respostas_pendentes_topico($topic_id) {
	global $wpdb;
	
	$sql = "SELECT * FROM wp_postmeta pm, wp_posts p WHERE p.post_type = 'reply' AND pm.post_id=p.ID AND pm.meta_key = '_bbp_topic_id' AND pm.meta_value = $topic_id AND post_status = 'pending'";
	
	$result = $wpdb->get_results ( $sql );
	
	return count ( $result );
}

function is_bbp_post_pendente($post_id)
{

	$post = get_post_status($post_id);
	
	if($post == 'pending')
		return true;

	return false;
}

function is_autor_forum($forum_id, $user_id){
	$produto_id = get_post_meta($forum_id, 'product_id', true );
	return is_autor_produto($produto_id, $user_id);
}

function is_forum_expirado($forum_id) {

	$produto_id = get_post_meta($forum_id, 'product_id', true );

	return is_produto_indisponivel($produto_id);
/*
	$post = get_post($forum_id);
	$product = get_produto_by_post_name($post->post_name);
	
	$data_string = get_post_meta($product->ID, PRODUTO_VENDAS_ATE_POST_META, true);
	if(empty($data_string))
		return false;
	
	$data_string = converter_para_yyyymmdd($data_string);
	$data_produto = strtotime($data_string. ' 00:00:00');
	$agora = strtotime("now");

	if (($data_produto - $agora) > 0)
		return false;
	
	return true;
*/
}

function aprovar_post($post_id) {

	$my_post = array(
		'ID'           => $post_id,
		'post_status'   => 'publish',
	);

	wp_update_post( $my_post );
	$post = get_post($post_id);
	$bbpressmoderation = new bbPressModeration();
	$bbpressmoderation::pending_to_publish($post);
}

function atualizar_reply($post) {
	$my_post = array(
			'ID'           => $post['ID'],
			'post_content'   => $post['post_content'],
	);
	
	wp_update_post( $my_post );
}

function atualizar_topic($post) {
	$my_post = array(
			'ID'           => $post['ID'],
			'post_title'   => $post['post_title'],
			'post_content'   => $post['post_content'],
	);

	wp_update_post( $my_post );
}

function has_resposta_pendente($topic_id) {
	
	$total = contar_respostas_pendentes_topico($topic_id);

	if($total > 0)
		return true;
	
	return false;
}

function has_topicos_pendente($forum_id) {
	$respostas = contar_topicos_pendentes($forum_id);
	if($respostas > 0)
		return true;

	return false;
}

function contar_topicos_e_respostas($forum_id) 
{
	global $wpdb;
	
	$sql = "SELECT * FROM wp_postmeta pm, wp_posts p WHERE pm.post_id=p.ID AND pm.meta_key = '_bbp_forum_id' AND pm.meta_value = $forum_id";
	
	$result = $wpdb->get_results ( $sql );
	
	return count ( $result );
}

function get_moderar_topico_url($forum_id) 
{
	return "/wp-admin/edit.php?s&post_status=pending&post_type=topic&bbp_forum_id=$forum_id";
}

function get_moderar_resposta_url($forum_id) 
{
	return "/wp-admin/edit.php?s&post_status=pending&post_type=reply&bbp_forum_id=$forum_id";
}

function get_titulo_forum($forum_id) 
{
	return str_replace ( 'Private: ', '', bbp_get_forum_title ( $forum_id ) );
}

function get_forum_participantes($forum_id) 
{
	$forum = bbp_get_forum ( $forum_id );
	
	$produto = get_produto_by_post_name ( $forum->post_name );
	
	return get_usuarios_que_compraram ( $produto->ID );
}

/**
 * ***********************************************************************
 *
 * HOOKS
 *
 * ***********************************************************************
 */

add_filter ( 'parse_query', 'exponencial_moderacao_forum' );
function exponencial_moderacao_forum($query) 
{
    if (is_admin () && ($query->query_vars ['post_type'] == 'topic' || $query->query_vars ['post_type'] == 'reply')) {
        
        KLoader::model("ProdutoModel");
        
    	if (! current_user_can ( 'administrator' )) {
			echo "<style>#wpbody-content {display:none}</style>";
			echo "<style>.row-actions .stick {display:none}</style>";

			$produtos = ProdutoModel::listar_por_professor(get_current_user_id());
			
			$foruns_ids = array ();
			
			foreach ( $produtos as $produto ) {
				
				$forum_id = ProdutoModel::get_forum_id($produto->post_id);
				
				if (! is_null ( $forum_id ))
					
					array_push ( $foruns_ids, $forum_id );
			}
			
			$qv = &$query->query_vars;
			
			$qv ['meta_query'] = array (
					
					array (
							
							'key' => '_bbp_forum_id',
							
							'value' => $foruns_ids,
							
							'compare' => 'IN' 
					)
					 
			);
			
			echo "<style>.subsubsub a .count, .subsubsub a.current .count, .pending-count { display: none !important } </style>";
			wp_enqueue_script('filtro-forum', get_template_directory_uri() . '/js/filtro-forum.js', array('jquery'));
		}
	}
}

add_filter ( 'bbp_topic_admin_links', 'exponencial_change_topic_admin_links' );
function exponencial_change_topic_admin_links($r) {
	$topic = bbp_get_topic ( bbp_get_topic_id ( ( int ) $r ['id'] ) );
	if (is_bbp_post_pendente ( $topic->ID ) && is_user_forum_moderador ( get_current_user_id (), 
			bbp_get_topic_forum_id ( $topic->ID ) )) {
		$r ['links'] = apply_filters ( 'rw_reply_admin_links', array (
				'Aprovar' => '<a href="/wp-content/themes/academy/bbpress/helper/forum_helper.php?act=aprovar_topic&id=' . 
					$topic->ID . '">Aprovar</a>',
				'edit' => '<a href="/wp-content/themes/academy/edit-topic.php?id='. $topic->ID.'">Editar</a>',
				'close' => bbp_get_topic_close_link ( $r ),
				'trash' => bbp_get_topic_trash_link ( $r ),
				'spam' => bbp_get_topic_spam_link ( $r ),
				'reply' => bbp_get_topic_reply_link ( $r ) 
		), $r ['id'] );
		
		return $r ['links'];
	}
	
	if (is_autor_and_topic_pendente($topic->ID)) {
		$r ['links'] = apply_filters ( 'rw_reply_admin_links', array (
				
				'edit' => '<a href="/wp-content/themes/academy/edit-topic.php?id='. $topic->ID.'">Editar</a>',
				'close' => bbp_get_topic_close_link ( $r ),
				'trash' => bbp_get_topic_trash_link ( $r ),
				'spam' => bbp_get_topic_spam_link ( $r ),
				'reply' => bbp_get_topic_reply_link ( $r )
		), $r ['id'] );
		
		return $r ['links'];
	}
	
	$r ['links'] = apply_filters ( 'rw_topic_admin_links', array (
			
			'edit' => bbp_get_topic_edit_link ( $r ),
			'close' => bbp_get_topic_close_link ( $r ),
			'trash' => bbp_get_topic_trash_link ( $r ),
			'spam' => bbp_get_topic_spam_link ( $r ),
			'reply' => bbp_get_topic_reply_link ( $r ) 
	)
	, $r ['id'] );
	
	return $r ['links'];
}

add_filter ( 'bbp_reply_admin_links', 'exponencial_change_reply_admin_links' );
function exponencial_change_reply_admin_links($r) {
	$reply = bbp_get_reply( bbp_get_reply_id( (int) $r['id'] ) );
	if(is_bbp_post_pendente($reply->ID) && is_user_forum_moderador(get_current_user_id(), 
			bbp_get_reply_forum_id($reply->ID))) {
		$r ['links'] = apply_filters ( 'rw_reply_admin_links', array (
			'Aprovar' => '<a href="/wp-content/themes/academy/bbpress/helper/forum_helper.php?act=aprovar_reply&id='.
				$reply->ID.'">Aprovar</a>',
			'edit' => '<a href="/wp-content/themes/academy/edit-reply.php?id='. $reply->ID.'">Editar</a>',
			'trash' => bbp_get_reply_trash_link ( $r ),
			'spam' => bbp_get_reply_spam_link ( $r ),
			'reply' => bbp_get_topic_reply_link ( $r ) 
		)
		, $r ['id'] );
		
		return $r ['links'];
	}
	
	if(is_autor_and_reply_pendente($reply->ID)) {
		$r ['links'] = apply_filters ( 'rw_reply_admin_links', array (
			
				'edit' => '<a href="/wp-content/themes/academy/edit-reply.php?id='. $reply->ID.'">Editar</a>',
				'trash' => bbp_get_reply_trash_link ( $r ),
				'spam' => bbp_get_reply_spam_link ( $r ),
				'reply' => bbp_get_topic_reply_link ( $r )
		)
				, $r ['id'] );
		
		return $r ['links'];
	}

	$r ['links'] = apply_filters ( 'rw_reply_admin_links', array (
			'edit' => bbp_get_reply_edit_link ( $r ),
			'trash' => bbp_get_reply_trash_link ( $r ),
			'spam' => bbp_get_reply_spam_link ( $r ),
			'reply' => bbp_get_topic_reply_link ( $r ) 
	)
	, $r ['id'] );
	
	return $r ['links'];
}

/**
 * ***************************************************************
 *
 * Ativa WYSIWYG na edição de tópicos e respostas
 *
 * ***************************************************************
 */

add_filter ( 'bbp_after_get_the_content_parse_args', 'bbp_enable_visual_editor' );
function bbp_enable_visual_editor($args = array()) {
	$args ['tinymce'] = true;
	
	$args ['quicktags'] = false;
	
	$args ['teeny'] = false;
	
	return $args;
}

/**
 * ***************************************************************
 *
 * Adiciona campos extras na criação de tópicos
 *
 * ***************************************************************
 */

add_action ( 'bbp_theme_after_topic_form_content', 'bbp_extra_fields' );
add_action ( 'bbp_theme_after_reply_form_content', 'bbp_extra_fields' );
function bbp_extra_fields() {
	if (is_user_forum_moderador ( get_current_user_id (), bbp_get_forum_id () )) {
		echo "<input id='bbp_notify_all' type='checkbox' name='bbp_notify_all' value='1'> ";
		echo "<label for='bbp_notify_all'> Notificar via e-mail todos os alunos sobre esse tópico.</label>";
	}
}

/**
 * ***************************************************************
 *
 * Envia e-mails após criação de novo tópico:
 *
 * 1) E-mail de notificação de tópico importante
 *
 * 2) E-mail de notificação de novo tópico enviado por aluno
 *
 * ***************************************************************
 */

//add_action ( 'bbp_new_topic', 'bbp_save_extra_fields', 10, 1 );
//add_action ( 'bbp_new_reply', 'bbp_save_extra_fields', 10, 1 );
function bbp_save_extra_fields() {
	$assunto = "";
	if(isset($_POST ['bbp_topic_title'])) $assunto = $_POST ['bbp_topic_title'];
	
	/**
	 * ***********************************************************
	 *
	 * Envia e-mail de notificação de tópico importante
	 *
	 * ***********************************************************
	 */
	$url = $_POST['_wp_http_referer'];
	$url = get_site_url() . $_POST['_wp_http_referer'];
	if (isset ( $_POST ) && $_POST ['bbp_notify_all'] != '') {
		
		$titulo = 'Exponencial Concursos - Nova mensagem no Fórum do curso ' . bbp_get_forum_title();
		
		$conteudo = $_POST['bbp_topic_content'] ?: $_POST['bbp_reply_content'];
		
		$url = $_POST['_wp_http_referer'];
		$url = get_site_url() . $_POST['_wp_http_referer'];
		$autor_link = get_autor_link(wp_get_current_user());
		$mensagem = get_template_email ( 'forum-notificacao-topico-importante.php', array ('conteudo' => $conteudo, 'url' => $url, 'autor_link' => $autor_link) );
		
		$usuarios = get_forum_participantes ( bbp_get_forum_id () );
		
		foreach ( $usuarios as $usuario ) {
			//enviar_email ( $usuario->user_email, $titulo, $mensagem, null, null );
		}

		/**
		 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/1257
		 */
		//enviar_email (EMAIL_LEONARDO, $titulo, $mensagem, null, null );
	}
}

//add_action( 'bbp_new_topic', 'expo_bbp_new_topic', 10, 4 );
function expo_bbp_new_topic($topic_id = 0, $forum_id = 0, $anonymous_data = false, $topic_author = 0) {
	/**
	 * ***********************************************************
	 *
	 * Envia e-mail de notificação de novo tópico enviado por aluno
	 *
	 * ***********************************************************
	 */
	if (! is_user_forum_moderador ( get_current_user_id (), bbp_get_forum_id () )) {
		$titulo = 'Exponencial Concursos - Novo tópico de aluno';
		$mensagem = get_template_email ( 'forum-notificacao-novo-topico.php', array (
				'url' => bbp_get_forum_permalink(), 
				'nome' => bbp_get_forum_title(),
				'parent' => 'fórum'
		) );
		
		$moderadores = get_forum_moderadores ( bbp_get_forum_id () );
		
		log_forum('debug', "Enviando e-mail de notificação de novo tópico enviado por aluno");
		log_forum('debug', "Fórum id: " . bbp_get_forum_id());
		log_forum('debug', count($moderadores) . " moderador(es) encontrados");
		
		$enviado_para_admin = false;
		foreach ( $moderadores as $moderador ) {
			$bcc = null;
			if(!$enviado_para_admin) {
				$bcc = EMAIL_LEONARDO;
				$enviado_para_admin = true;
			}

			//enviar_email ( $moderador->user_email, $titulo, $mensagem, null, $bcc );
			log_forum('debug', "Enviando para moderador: " . $moderador->user_email);
		}
	}
	return true;
}

//add_action( 'bbp_new_reply', 'expo_bbp_new_reply', 10, 4 );
function expo_bbp_new_reply($reply_id = 0, $topic_id = 0, $forum_id = 0, $anonymous_data = false, $topic_author = 0) {
	/**
	 * ***********************************************************
	 *
	 * Envia e-mail de notificação de nova resposta enviado por aluno
	 *
	 * ***********************************************************
	 */
	$usuario = get_usuario_array(get_current_user_id());
	
	if (! is_user_forum_moderador ( get_current_user_id (), bbp_get_forum_id () )) {
		$reply = bbp_get_reply($reply_id);
		
		$titulo = 'Exponencial Concursos - Nova resposta de aluno';
		$mensagem = get_template_email ( 'forum-notificacao-nova-resposta.php', array (
				'link' => bbp_get_reply_url($reply_id),
				'topico' => bbp_get_topic_title($topic_id),
				'nome_autor' => $usuario['nome_completo']
		) );

		$moderadores = get_forum_moderadores ( bbp_get_forum_id () );

		$enviado_para_admin = false;
		foreach ( $moderadores as $moderador ) {
			$bcc = null;
			if(!$enviado_para_admin) {
				$bcc = EMAIL_LEONARDO;
				$enviado_para_admin = true;
			}

			//enviar_email ( $moderador->user_email, $titulo, $mensagem, null, $bcc );
		}
	}
	return true;
}


/**************************************************************
 * Corrige as restrições do bbpress para exibição de tags html
 **************************************************************/
function myprefix_kses_allowed_tags($input){
	return array_merge( $input, array(
			// paragraphs
			'p' => array(
					'style'     => array()
			),
			'span' => array(
					'style'     => array()
			),
			'div' => array(
					'style'     => array()
			),
			// Links
			'a' => array(
					'href'     => array(),
					'title'    => array(),
					'rel'      => array()
			),
			// Quotes
			'blockquote'   => array(
					'cite'     => array()
			),
			// Code
			'code'         => array(),
			'pre'          => array(),
			// Formatting
			'em'           => array(),
			'strong'       => array(),
			'del'          => array( 'datetime' => true, ),
			// Lists
			'ul'           => array(),
			'ol'           => array( 'start'    => true, ),
			'li'           => array(),
			// Images
			'img'          => array(
					'src'      => true,
					'border'   => true,
					'alt'      => true,
					'height'   => true,
					'width'    => true,
			)
	));
}
add_filter( 'bbp_kses_allowed_tags', 'myprefix_kses_allowed_tags', 999, 1 );

add_filter( 'bbp_after_get_the_content_parse_args', 'bavotasan_bbpress_upload_media' );
/**
 * Allow upload media in bbPress
 *
 * This function is attached to the 'bbp_after_get_the_content_parse_args' filter hook.
*/
function bavotasan_bbpress_upload_media( $args ) {
	$args['media_buttons'] = true;

	return $args;
}

function bbp_increase_forum_per_page( $args = array() ) {
	$path = explode ("/", get_path_atual());

	$args['posts_per_page'] = FORUMS_PER_PAGE;
	$args['paged'] = get_query_var("page", 1);

	return $args;
}

add_filter( 'bbp_before_has_forums_parse_args', 'bbp_increase_forum_per_page' );
