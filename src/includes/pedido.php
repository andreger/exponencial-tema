<?php
function is_item_de_pacote($item)
{
	return isset($item['bundled_by']); 
}

function get_item_raiz_de_pacote($item_de_pacote)
{
	return is_item_de_pacote($item_de_pacote) ? $item_de_pacote['bundled_by'] : NULL;
}

function is_item_raiz_de_pacote($item)
{
	return isset($item['bundled_items']) || isset($item['per_product_pricing']);	
}

function get_pedidos($user_id) {

    if($user_id) {
        global $wpdb;

        $sql = "SELECT pm.* 
                    FROM wp_postmeta pm 
                    INNER JOIN wp_posts p 
                        ON p.ID = pm.post_id
                WHERE pm.meta_key = '_customer_user' 
                    AND pm.meta_value='" . $user_id . "'
                    AND p.post_type = 'shop_order'";//Necessário porque a assinatura não é pedido e gera entrada do tipo "_customer_user"

        return $wpdb->get_results($sql);
    }

    return [];
}

function listar_pedidos_recuperacao_vendas($minimo_dias = 7)
{
	global $wpdb;

	$dia = date('Y-m-d 23:59:59', strtotime("-{$minimo_dias} days"));

	$sql = "SELECT ID FROM wp_posts p
				LEFT JOIN wp_postmeta pm ON p.ID = pm.post_id AND pm.meta_key = 'recuperacao_enviada'
			WHERE post_status = 'wc-pending' AND post_type = 'shop_order' AND meta_value IS NULL 
				AND post_date <= '{$dia}' AND post_date >= '2017-04-01 00:00:00' ORDER BY ID DESC LIMIT 100";

	$order_ids = $wpdb->get_col($sql);

	$orders = array();
	foreach ($order_ids as $id) {
		array_push($orders, new WC_Order($id));
	}
	
	return $orders;
}

function listar_pedidos_por_data_por_usuario($data, $usuario_id, $exclusoes = NULL, $somente_pagos = TRUE)
{
	global $wpdb;

	$exclusoes_frag = $exclusoes ? " AND ID NOT IN ("  . implode(',', $exclusoes) . ") " : "";

	$dia = date("Y-m-d", strtotime($data));
	// $agora = date('Y-m-d H:i:s');

	$sql = "SELECT ID FROM wp_posts WHERE post_status = 'wc-completed' AND post_type = 'shop_order' AND post_date >= '{$dia} 00:00:00' AND post_date <= '{$dia} 23:59:59' {$exclusoes_frag}";

	$order_ids = $wpdb->get_col($sql);

	$orders = array();
	foreach ($order_ids as $id) {
		$order = new WC_Order($id);

		if($order->get_user_id() != $usuario_id) {
 			continue;
		}

		if($somente_pagos && $order->get_total() == 0) {
			continue;
		}

		array_push($orders, $order);
	}
	
	return $orders;
}

function listar_pedidos_por_produto($produto_id)
{
	global $wpdb;

	$consulta = "SELECT order_id FROM wp_woocommerce_order_itemmeta woim
					LEFT JOIN wp_woocommerce_order_items oi
						ON woim.order_item_id = oi.order_item_id
				WHERE meta_key = '_product_id' AND meta_value = %d
				GROUP BY order_id;";

	$order_ids = $wpdb->get_col( $wpdb->prepare( $consulta, $produto_id ) );

	$orders = array();
	foreach ($order_ids as $id) {
		array_push($orders, new WC_Order($id));
	}
	
	return $orders;
}

function get_root_pacote($order_id, $order_item)
{
	global $wpdb;

	if($order_item['bundled_by']) {
		$sql = "select m.order_item_id from wp_woocommerce_order_itemmeta m
				join wp_woocommerce_order_items i on m.order_item_id = i.order_item_id 
				where m.meta_value = '{$order_item['bundled_by']}'
				and m.meta_key = '_bundle_cart_key' and i.order_id = {$order_id}";

		$resultado = $wpdb->get_row($sql, ARRAY_A);
		
		if($resultado) {
			return new WC_Product(get_metadata( 'order_item', $resultado['order_item_id'], '_product_id', true ));
		}
		return null;
	}

	return null;
}

function get_produtos_ids_dos_pedidos($user_id, $somente_pagos = false) {
	$pedidos = get_pedidos($user_id);
	
	$produtos_ids = array();
	foreach ( $pedidos as $row ) {
		$post_id = $row->post_id;
		try{
			$order = new WC_Order ( $post_id );
		}catch(Exception $e){
			log_pedido("ERRO", "Pedido {$post_id} do usuário {$user_id} está com erro e não pode ser utilizado");
		}
	
		if($order->status == 'completed') {

			foreach ( $order->get_items () as $item ) {

				if ($item ['bundled_items'])
					continue;
				
				if (! is_produto_indisponivel ( $item ['product_id'] )) {

					if($somente_pagos) {
						// busca o pacote, caso seja pacote retorna null
						$pacote_root = get_root_pacote($order->id, $item);

						// se é pacote, verica se o preço é maior do que 0
						if($pacote_root) {
							if($pacote_root->price > 0) {
								array_push($produtos_ids, $item['product_id']);
							}
						}

						// se for produto normal, verifica se o preço é maior do que 0
						else {
							$produto = new WC_Product($item ['product_id']);

							if($produto->price > 0) {
								array_push($produtos_ids, $item['product_id']);
							}
						}
					} 
					else {
						array_push($produtos_ids, $item['product_id']);
					}
				}
			}
		}
	}
	return $produtos_ids;
}

function is_pedido_concluido($order_id)
{
	$order = new WC_Order($order_id);
	return $order->status == 'completed' ? TRUE : FALSE;
}

function get_pedido_data_criacao($order_id){
	try{
		$order = new WC_Order($order_id);
		if($order){
			return $order->date_created;
		}
		return null;
	}catch(Exception $e) {
		return null;
	}
}

/**
 * Recupera o preco de um produto em uma determinada compra. Utilizado principalmente para calcular
 * o preço do produto em compras com desconto.
 * 
 * [JOULE][J1] UPGRADE - Cadastro automático de pagamentos do coaching no Podio
 *  
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/upgrades/items/193
 * 
 * @since 10.0.0
 * 
 * @param int $produto_id Id do produto no WP
 * @param WC_Order $order Pedido no WP (para extrair usuário, valor da compra, descontos, etc.)
 *
 * @return float Preço do produto
 */

function get_preco_produto_com_descontos($produto_id, $order)
{
	$produto = new WC_Product($produto_id);
	
	// se a compra tiver desconto leva isso em consideração
	if($order->get_total_discount()) {

		// recupera percentual de desconto do pedido
		$order_total = $order->get_total() + $order->get_total_discount();
		$percentual = $order->get_total_discount() / ($order->get_total() + $order->get_total_discount());

		// retorna valor do produto com percentual de desconto
		return $produto->price * $percentual;
	}

	// se não tiver desconto retorna o preço normal do produto
	else {
		return $produto->price;
	}
}

/************************************************************************************
 * Hook executado após a finalização de uma compra
 ************************************************************************************/
add_action( 'woocommerce_order_status_completed', 'expoconcursos_woocommerce_payment_complete' );

function expoconcursos_woocommerce_payment_complete( $order_id ) {
	global $wpdb;
	
	KLoader::model("ProdutoModel");
	KLoader::model("PremiumModel");
	KLoader::model("SimuladoModel");

	$order = new WC_Order($order_id);
	$usuario = get_userdata($order->user_id);
	$usuario_array = get_usuario_array($order->user_id); 
	
	$discriminacao = 'Curso(s) online: ';
	
	log_wp("debug", "Compra finalizada: {$order_id} pelo usuario: {$order->user_id}");

	$adicionar_validade = "";
	$valores_produtos = array();

	//O Pagar.me gera um desconto em forma de taxa negativa que não é atrelado ao item
	$total_fee = 0;
	foreach($order->get_items('fee') as $item_fee)
	{
		$total_fee += $item_fee->get_total();
	}

	//O Pagar.me gera um total de pedido sem informação de desconto então precisamos calcular o total sem desconto
	$total_pedido = 0;
	foreach ($order->get_items() as $item)
	{
		$total_pedido += $item->get_total();
	}
	
	foreach ($order->get_items() as $item) {

		$item_id = $item['product_id'];
		$product_name = $item['name'];
		$post = get_post($item_id);
	
		SimuladoModel::concede_acesso_a_simulado(get_current_user_id(), $item_id);
	
		//---------------------------------------------------------------
		// Inscreve aluno no fórum do curso (se houver)
		//---------------------------------------------------------------

		log_wp("debug", "Verificando se produto tem forum");
		$forum_id = get_post_meta($item_id, 'forum_id', true );
	
		// check if the custom field has a value
		if(!empty( $forum_id )) {
			log_wp("debug", "Concedendo acesso ao forum: " . $forum_id);

			$user_id = $order->customer_user;
			bbp_add_user_forum_subscription($user_id, $forum_id);
		}
		else {
			log_wp("debug", "Produto nao tem forum");
		}
	
		//---------------------------------------------------------------
		// Envia e-mail de notificação de novo aluno para cada professor
		//---------------------------------------------------------------
		
//		if(get_post_meta($item_id, 'notificar_vendas', true) == 'yes' && !PremiumModel::is_pedido_oculto($order->id)) {
//			$autores = get_autores($post);
//
//			$discriminacao .= $product_name.', ';
//			log_wp("debug", "Curso vendido: " . $product_name);
//			log_wp("debug", "Preparando para notificar " . count($autores) . " professor(es)");
//
//			foreach ($autores as $author)
//			{
//				$author_email = $author->user_email;
//
//				$customer_email = $order->billing_email;
//
//				$message_user = get_template_email('aviso-novo-aluno.php', array('product_name' => $product_name, 'customer_name' => $usuario_array['nome_completo']));
//
//				enviar_email($author_email, 'Exponencial Concursos - Novo Aluno', $message_user);
//
//				log_wp("debug", "E-mail de notificação de novo aluno enviado para professor " . $author_email);
//			}
//
//		}
//		else {
//			log_wp("debug", "Pulando envio de e-mail de aviso de novo aluno (opção Notificar Vendas desmarcada'): " . $product_name);
//		}
		
		//---------------------------------------------------------------
		// Verifica se o produto é um caderno.
		//---------------------------------------------------------------
		
		criar_caderno($item_id, $order_id, TRUE, TRUE);

		//---------------------------------------------------------------
		// Verifica se o produto é pagamento de coaching.
		//---------------------------------------------------------------
		log_wp('debug', "verificando se produto é coaching");
		pagamento_coaching($item_id, $order);
		
		//---------------------------------------------------------------
		// Verifica se o produto fornece cupons bônus
		//---------------------------------------------------------------
	
		if(!is_produto_acesso_limitado($item_id)) {
			verifica_cupom_produto($post, $order);
		}
		
		//---------------------------------------------------------------
		// Salvar expiração
		//---------------------------------------------------------------
// 		if($tempo_acesso = get_post_meta($item_id, PRODUTO_TEMPO_DE_ACESSO, true)) {
// 		    $data_expiracao = date("Y-m-d", strtotime("+{$tempo_acesso} days"));
		    
// 		    ProdutoModel::salvar_produto_expiracao($item_id, $order_id, $data_expiracao);
// 		}
		
		//---------------------------------------------------------------
		// Verifica se é assinatura SQ
		//---------------------------------------------------------------
		$categorias = listar_categorias($item_id);
		
		foreach ($categorias as $categoria) {
			log_wp('debug', "verificando se produto é assinatura: {$item_id}, categorias: {$categoria->slug}");
			
			if(is_categoria_assinatura_sq_semanal($categoria)) {
				log_wp('debug', 'É assinatura semanal');
				$adicionar_validade .= " +7 days";
			}
			elseif(is_categoria_assinatura_sq_quinzenal($categoria)) {
				log_wp('debug', 'É assinatura quinzenal');
				$adicionar_validade .= " +15 days";
			}
			elseif(is_categoria_assinatura_sq_mensal($categoria)) {
				log_wp('debug', 'É assinatura mensal');
				$adicionar_validade .= " +1 months";
			}
			elseif(is_categoria_assinatura_sq_trimestral($categoria)) {
				log_wp('debug', 'É assinatura trimestral');
				$adicionar_validade .= " +3 months";
			}
			elseif(is_categoria_assinatura_sq_semestral($categoria)) {
				log_wp('debug', 'É assinatura semestral');
				$adicionar_validade .= " +6 months";
			}
			elseif(is_categoria_assinatura_sq_anual($categoria)) {
				log_wp('debug', 'É assinatura anual');
				$adicionar_validade .= " +12 months";
			}
			
		}
		
		//---------------------------------------------------------------
		// Verifica se é assinatura SQ de produto premium
		//---------------------------------------------------------------
		if(get_post_meta($item_id, PRODUTO_PREMIUM_CONCEDER_ACESSO_SQ, true) == 'yes') {
		    
		    if($tempo_acesso = get_post_meta($item_id, PRODUTO_TEMPO_DE_ACESSO, true)) {
		        $adicionar_validade .= " +{$tempo_acesso} days";
		    }
		}

		/*
		 * Captura o valor do produto na data da compra (incluindo o valor proporcional dos itens do pacote, 
		 * logo a soma de todos esses valores pode ser maior que o valor do pedido)
		*/
		if(is_item_raiz_de_pacote($item)) {
			$last_bundle_id = $item['product_id'];
			$last_bundle_valor = $item->get_total();
			$item_raiz_pacote = $item;
		}

		if(is_item_de_pacote($item)) {
			//Verifica se tem alguma taxa no pedido e diminui proporcionalmente no valor do item
			$percentual_pacote_pedido = $last_bundle_valor / $total_pedido;
			$valor_pacote_fee = $total_fee * $percentual_pacote_pedido;
			
			$percentual = get_valor_percentual_item_pacote_no_pedido($order->get_id(), $item['product_id'], $item_raiz_pacote);
			$valor_item = ($last_bundle_valor + $valor_pacote_fee) * $percentual;		    		
		}
		else {
			//Verifica se tem alguma taxa no pedido e diminui proporcionalmente no valor do item
			$percentual_pedido = $item->get_total() / $total_pedido;
			$valor_item_fee = $total_fee * $percentual_pedido;

			$valor_item = $item->get_total() + $valor_item_fee;
		}

		$valores_produtos[$item->get_id()] = $valor_item;
		
	}

	update_post_meta($order->id, 'valores-produtos', $valores_produtos);

	log_wp("debug", "Adicionar validade: " . $adicionar_validade);
	if($adicionar_validade) {
		update_user_meta($order->user_id, 'assinante-sq', 'yes');
		
		/**
		 * AJUSTE - Produto SQ 7 dias (semanal)
		 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/1737
		 * 
		 * Se assinatura estiver expirada seta data base como now.
		 */

		if(is_assinatura_sq_expirada($order->user_id)) {
			$data_validade = 'now';
		}
		else {
			$data_validade = get_user_meta($order->user_id, 'assinante-sq-validade', true) ? : 'now';
		}

		log_wp('info', "Data validade: {$data_validade}");
		log_wp('info', "Adicionar validade: {$adicionar_validade}");
		
		$nova_validade = date('Y-m-d', strtotime($data_validade . $adicionar_validade));

		log_wp('info', "A nova validade: {$nova_validade}");

		update_user_meta($order->user_id, 'assinante-sq-validade', $nova_validade);
	}
	else {
		log_wp('info',"Nova validade nula ou igual a zero");
	}
	

	//---------------------------------------------------------------	
	// Salva informações para emissão de nota fiscal
	//---------------------------------------------------------------
	if($order->get_total() > 0) {




		$item = array(
				'order_id' 				=> $order->id,
				'nf_data_pedido' 		=> $order->order_date,
				//'nf_data_emissao' 		=> $order->order_date,
				//'nf_valor' 				=> $order->get_total(),
				'nf_nome'				=> $usuario->display_name,
				'nf_cpf'				=> desformatar_cpf(get_user_meta($order->user_id, 'oneTarek_CPF', true)),
				'nf_endereco'			=> get_user_meta($order->user_id, 'billing_address_1', true),
				'nf_bairro'				=> get_user_meta($order->user_id, 'oneTarek_nhb', true),
				'nf_uf'					=> get_user_meta($order->user_id, 'billing_state', true),
				'nf_cep'				=> get_user_meta($order->user_id, 'oneTarek_CEP', true),
				'nf_email'				=> $usuario->user_email,
				'nf_discriminacao'		=> rtrim($discriminacao, ', ')
		);

        // verifica flag de ativar cadastro de NF
        $nf_ativar_cadastro = get_option(NOTA_FISCAL_ATIVAR_CADASTRO);
        $nf_ativar_skip = get_option(NOTA_FISCAL_ATIVAR_SKIP);
        $nf_skip_rand = rand(1,3);

        if(!$nf_ativar_cadastro || ($nf_ativar_skip && $nf_skip_rand != 1)) {
            $item['nf_status'] = 6;
        }

		// Salva em notas_fiscais
		$wpdb->insert('notas_fiscais', $item);
	}
	
	//---------------------------------------------------------------	
	// verifica se está usando cupons
	//---------------------------------------------------------------
// 	foreach($order->get_used_coupons() as $cupom_slug)
// 	{	
// 		// verifica se o cupom é de bonus
// 		if(get_cupom_bonus_by_slug($cupom_slug)) {
// 			// limita o cupom a uma utilização por usuário
// 			adiciona_limite_de_uso_por_usuario($cupom_slug, 1);			
// 		}
// 	}

	/** Envia e-mail de pedido completo (somente se não for pedido oculto premium */
//    if(!PremiumModel::is_pedido_oculto($order->id)) {
//    	$message_order_completed = get_template_email('pedido-completo.php', array('order' => $order));
//    	enviar_email($usuario->user_email, 'Seu pedido em Exponencial Concursos realizado em '. converter_para_ddmmyyyy($order->order_date) .' está concluído', $message_order_completed);
//    }
}

function listar_pedidos_por_periodo($data_inicio, $data_fim)
{
	/*$timestamp_dtinicio = strtotime($data_inicio);
	$data_array_ini = getdate($timestamp_dtinicio);
	
	$timestamp_dtfim = strtotime($data_fim);
	$data_array_fim = getdate($timestamp_dtfim);
	
	$after = array('year' => $data_array_ini['year'], 'month' => $data_array_ini['mon'], 'day' => $data_array_ini['mday'], 'hour' => 0, 'minute' => 0, 'second' => 0);
	$before = array('year' => $data_array_fim['year'], 'month' => $data_array_fim['mon'], 'day' => $data_array_fim['mday'], 'hour' => 23, 'minute' => 59, 'second' => 59);
	
	$args = array(
		'post_type' => 'shop_order',
		'post_status' => 'publish',
		'meta_key' => '_customer_user',
		'posts_per_page' => '-1',
		'date_query'     => array(
								array(
									'column' => 'order_date',
									'after' => $after,
									'before' => $before,
									'inclusive' => true
								),	
							)
	);
	
	$query_pedidos = new WP_Query($args);
	
	print_r($query_pedidos->request);

	$pedidos = array();
	foreach ($query_pedidos->posts as $pedido){
		$order = new WC_Order();
		$order->populate($pedido);
		$pedidos[] = $order;		
	}
	
	return $pedidos;
	*/

	global $wpdb;

	$sql = "SELECT ID FROM wp_posts WHERE post_status = 'wc-completed' AND post_type = 'shop_order' AND post_date <= '{$data_fim} 23:59:59' AND post_date >= '{$data_inicio} 00:00:00' {$exclusoes_frag}";

	$order_ids = $wpdb->get_col($sql);

	$orders = array();
	foreach ($order_ids as $id) {
		array_push($orders, new WC_Order($id));
	}
	
	return $orders;
}

// add_filter( 'woocommerce_billing_fields', 'expoconcursos_required_fields', 10, 1 );

// function expoconcursos_required_fields( $address_fields ) {
// 	foreach ($address_fields as &$field) {
// 		$field['required'] = false;
// 	}
	
// 	return $address_fields;
// }

/************************************************************************************
 * Hook executado antes de aplicar descontos
 ************************************************************************************/

add_action('woocommerce_before_checkout_form' , 'adicionar_desconto_parceiro');
function adicionar_desconto_parceiro(){
	// $cupom = 'aluno-ls';

	$cupom = recupera_cupons_parceiros();
	
	if($cupom) {
		if(!tem_cupom_aplicado()) {
			if(!is_cupom_aplicado($cupom)) {
				// if(is_usuario_ls()) {
					global $woocommerce;

					adiciona_email_a_restricoes_de_cupom($cupom, get_current_user_id());
			    	$woocommerce->cart->add_discount($cupom);
				// }	
			}
		}
	}
}

add_action('woocommerce_before_checkout_form' , 'remover_cupons');
function remover_cupons(){

	global $woocommerce;

	
	$cupons = $woocommerce->cart->get_applied_coupons();

	// durante a adicao de um novo cupom remove todos os outros
	if($nome = $_GET['c'] ?? null) {
		if($cupons) {
			foreach ($cupons as $cupom) {
				if($cupom != $nome) {
					$woocommerce->cart->remove_coupon($cupom);
				}
			}
		}
	}

	// se houver mais de um cupom remove todos
	else  {
		if(count($cupons) > 1) {
			$woocommerce->cart->remover_cupons();
		}
	}
}


function is_cupom_aplicado($nome)
{
	global $woocommerce;

	if($cupons = $woocommerce->cart->get_applied_coupons()) {
		foreach ($cupons as $cupom) {
			if($cupom == $nome) {
				return true;
			}
		}
	}

	return false;
}

function tem_cupom_aplicado()
{
	global $woocommerce;

	if($cupons = $woocommerce->cart->get_applied_coupons()) {
		return true;
	}

	return false;
}

/************************************************************************************
 * Hook executado para redirecionar diretamente para o checkout ao invés do carrinho
 ************************************************************************************/

add_filter('add_to_cart_redirect', 'expo_add_to_cart_redirect');
function expo_add_to_cart_redirect() 
{
	global $woocommerce;
 	$checkout_url = $woocommerce->cart->get_checkout_url();
 	return $checkout_url;
}

add_action('woocommerce_add_to_cart', 'expo_acesso_limitado');
function expo_acesso_limitado()
{
    $produto_id = $_GET['add-to-cart'];
	$usuario_id = get_current_user_id();

 	if(is_produto_acesso_limitado($produto_id) && comprou_produto($usuario_id, $produto_id)) {

 		wp_redirect('/acesso-limitado');
 		exit;
 	}

}

/**
 * Retorna o ID da transação de um pedido no PagSeguro.
 * Esse ID é armazenado como metadado de um post que representa o pedido. 
 * O plugin que faz esse intercâmbio trocou o nome do parâmetro algumas 
 * vezes. Por conta disso é necessário procurar por vários nomes.
 * 
 * @since Joule 1
 * 
 * @param int ID do pedido do WP.
 * 
 * @return string Transaction ID do Pagseguro
 */

function get_pagseguro_transaction_id($order_id)
{
	$pagseguro_id = get_post_meta($order_id, 'PagSeguro Transaction ID', TRUE);
	$pagseguro_id = empty($pagseguro_id) ? get_post_meta($order_id, 'PagSeguro: ID da transação', TRUE) : $pagseguro_id;

	// [Joule 1] Woocommerce 3.1.1, WooCommerce PagSeguro 2.12.5
	$pagseguro_id = empty($pagseguro_id) ? get_post_meta($order_id, '_transaction_id', TRUE) : $pagseguro_id;

	return $pagseguro_id;
}

function listar_pedidos_cancelados_pagseguro($dias = 1)
{
	global $wpdb;

	$data_inicio = date('Y-m-d H:i:s', strtotime("-{$dias} days"));
	$data_fim = date('Y-m-d H:i:s');

	$sql = "SELECT ID FROM wp_posts WHERE post_status = 'wc-cancelled' AND post_type = 'shop_order' AND post_date <= '{$data_fim}' AND post_date >= '{$data_inicio}'";

	$order_ids = $wpdb->get_col($sql);

	$orders = array();
	foreach ($order_ids as $id) {
		array_push($orders, new WC_Order($id));
	}
	
	return $orders;
}

/**
 * Estorna um valor de um pedido
 * 
 * @since J5
 * 
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2043
 * 
 * @param int $pedido_id
 * @param float $valor
 * @param string $motivo
 */ 

function estornar_pedido_por_valor($pedido_id, $valor, $motivo = "", $itens_ids = null, $revoga_acesso = TRUE)
{
	 
  	$pedido = wc_get_order($pedido_id);

	$status = estornar_pedido_pagseguro($pedido_id, $valor);
  	
	if(!$status) {  
		$line_items = [];

		$items = $pedido->get_items();

		if($items) {

			$last_bundle_id = null;
			$valores_produtos = get_post_meta($pedido->get_id(), 'valores-produtos', true);
			$valor_itens = 0;
			$valor_pacote_parcial = array();
			foreach( $items as $item_id => $item ) {
				
				if(is_item_raiz_de_pacote($item)) {
					$last_bundle_id = $item->get_id();
					$itens_pacote_pedido = get_itens_pacote_no_pedido($pedido_id, $item);
				}

				//Se é parte do pacote e está marcado então removo 
				//o item de pacote da lista de produtos para não ocorrer duplicação de valor
				if(in_array($last_bundle_id, $itens_ids) 
					&& is_item_de_pacote($item) 
					&& in_array($item->get_id(), $itens_pacote_pedido)){
						continue;
				}

				if(in_array($item->get_id(), $itens_ids)){					
					$valor_itens += $valores_produtos[$item->get_id()];						
				}

			}
			
			$produtos_ids = array();
			
			$last_bundle_id = null;

			foreach( $items as $item_id => $item ) {
				
				if(is_item_raiz_de_pacote($item)) {
					$last_bundle_id = $item->get_id();
				}

	    		if(in_array($item->get_id(), $itens_ids)) {
					
					array_push($produtos_ids, $item['product_id']);

					if($revoga_acesso){
						revogar_acesso_item_pedido($item);
					}

					$percentual = $valores_produtos[$item->get_id()] / $valor_itens;
					
				    $line_items[ $item_id ] = [ 
					    'qty' => $item->get_quantity(), 
					    'refund_total' => round($valor * $percentual, 2), 
						'refund_tax' =>  0,
						'line_total' => round($valor * $percentual, 2)
					];

					//Grava o valor de estorno parcial do pacote
					if(is_item_de_pacote($item)
						&& !in_array($last_bundle_id, $itens_ids) ){
						$valor_pacote_parcial[$last_bundle_id] = ($valor_pacote_parcial[$last_bundle_id]?:0) + round($valor * $percentual, 2);
					}
				}
			}
			
			if(!empty($valor_pacote_parcial)){
				foreach($valor_pacote_parcial as $pacote_id => $pacote_valor){
					$line_items[ $pacote_id ] = [ 
						'qty' => 1, 
						'refund_total' => $pacote_valor, 
						'refund_tax' =>  0,
						'line_total' => $pacote_valor
					];
				}
			}

		}
	  
		$refund = wc_create_refund([
			'amount'         => wc_format_decimal( $valor, wc_get_price_decimals() ),
			'reason'         => $motivo,
			'order_id'       => $pedido_id,
			'line_items'     => $line_items
		]);
	}
	
	salvar_estorno(ESTORNO_POR_VALOR, $pedido, $status, $valor, $itens_ids, $produtos_ids, $motivo, $revoga_acesso);
}

/**
 * Estorna produtos de um pedido
 * 
 * @since J5
 * 
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2043
 * 
 * @param int $pedido_id
 * @param array $produtos_ids
 * @param string $motivo
 */ 

function estornar_pedido_por_produtos($pedido_id, $itens_ids, $motivo = "")
{
	$valor = 0;

  	$pedido = wc_get_order($pedido_id);
  
    $items = $pedido->get_items();

	$line_items = [];

	if($items) {

		$last_bundle_id = null;
		$valores_produtos = get_post_meta($pedido->get_id(), 'valores-produtos', true);
		$valor_pacote_parcial = array();
    	foreach( $items as $item_id => $item ) {

    		if(is_item_raiz_de_pacote($item)) {
				$last_bundle_id = $item->get_id();
				$itens_pacote_pedido = get_itens_pacote_no_pedido($pedido_id, $item);
    		}

    		if(in_array($item->get_id(), $itens_ids)) {

				//Se é parte do pacote e está marcado então removo 
				//o item de pacote da lista de produtos para não ocorrer duplicação de valor
				if(in_array($last_bundle_id, $itens_ids) 
					&& is_item_de_pacote($item) 
					&& in_array($item->get_id(), $itens_pacote_pedido)){
						if (($key = array_search($item->get_id(), $itens_ids)) !== false) {
							unset($itens_ids[$key]);
						}
						$line_items[ $item_id ] = [ 
							'qty' => $item->get_quantity(), 
							'refund_total' => $valores_produtos[$item->get_id()], 
							'refund_tax' =>  0
						];
						continue;
				}
				
				//Caso o pacote já tenha estorno parcial (por causa dos itens)
				if(is_item_raiz_de_pacote($item)) {
					$valor_produto = $valores_produtos[$item->get_id()] - $pedido->get_total_refunded_for_item($item->get_id());
				}else{	    		
					$valor_produto = $valores_produtos[$item->get_id()];
				}

	    		$valor += $valor_produto;

			    $line_items[ $item_id ] = [ 
				    'qty' => $item->get_quantity(), 
				    'refund_total' => $valor_produto, 
				    'refund_tax' =>  0
				];

				//Grava o valor de estorno parcial do ṕacote
				if(is_item_de_pacote($item)
					&& !in_array($last_bundle_id, $itens_ids)){
					$valor_pacote_parcial[$last_bundle_id] = ($valor_pacote_parcial[$last_bundle_id]?:0) + $valores_produtos[$item->get_id()];
				}
				
			}
		}
		
		if(!empty($valor_pacote_parcial)){
			foreach($valor_pacote_parcial as $pacote_id => $pacote_valor){
				$line_items[ $pacote_id ] = [ 
					'qty' => 1, 
					'refund_total' => $pacote_valor, 
					'refund_tax' =>  0,
					'line_total' => $pacote_valor
				];
			}
		}
	}

	$status = estornar_pedido_pagseguro($pedido_id, $valor);
	log_estorno("PagSeguro: " . print_r($status, true));

	/**
	 * Se um pacote estiver sendo estornado então adiciona seus itens 
	 * para também serem estornados e salvos no estorno, pois eles foram removidos mais acima
	 */
	$produtos_ids = array();
	if($items){
		foreach( $items as $item ) {
			if(in_array($item->get_id(), $itens_ids)){
				if(is_item_raiz_de_pacote($item)) {
					//Item_id
					$itens_pacote = get_itens_pacote_no_pedido($pedido_id, $item);
					$itens_ids = array_merge($itens_ids, $itens_pacote);
					//Product_id
					$produtos_pacote = get_produtos_pacote_no_pedido($pedido_id, $item);
					$produtos_ids = array_merge($produtos_ids, $produtos_pacote);					
				}
				array_push($produtos_ids, $item['product_id']);
			}
		}
	}

	if(!$status) {

		foreach( $items as $item_id => $item ) {

	    	if(in_array($item->get_id(), $itens_ids)) {
				revogar_acesso_item_pedido($item);	         
			}

		}
		
		$refund = [
			'amount'         => wc_format_decimal( $valor, wc_get_price_decimals() ),
			'reason'         => $motivo,
			'order_id'       => $pedido_id,
			'line_items'     => $line_items
		];

		//log_estorno("Reembolso Antes: " . print_r($refund, true));

		$refund = wc_create_refund($refund);

		//log_estorno("Reembolso Depois: " . print_r($refund, true));
	}
	
	salvar_estorno(ESTORNO_POR_PRODUTO, $pedido, $status, $valor, $itens_ids, $produtos_ids, $motivo);
}

/**
 * Revoga acesso de um item de pedido.
 * A revogação é feita setando os downloads restantes para zero
 * 
 * @see https://stackoverflow.com/questions/44724410/woocommerce-how-to-remove-downloadable-products-permission-for-order
 * 
 * @since J5 
 * 
 * @param WC_Order_Item $item
 */ 

function revogar_acesso_item_pedido($item)
{
	global $wpdb;

	$product = $item->get_product(); 

    if ( $product && $product->exists() && $product->is_downloadable() ) { 
        $downloads = $product->get_downloads(); 

		$pedido = $item->get_order();
        $usuario_id = $pedido->get_user_id();

        foreach ( array_keys( $downloads ) as $download_id ) { 

            $wpdb->update( 
                'wp_woocommerce_downloadable_product_permissions', 
                array( 
                    'downloads_remaining' => '0'
                ), 
                array(
                	'download_id' => $download_id,
                	'user_id' => $usuario_id,
                	'product_id' => $product->get_id(),
                	'order_id' => $pedido->get_id()
                )
            );                    
        } 
	}
	
	//---------------------------------------------------------------
	// Verifica se é assinatura SQ
	//---------------------------------------------------------------
	$item_id = $item['product_id'];
	$categorias = listar_categorias($item_id);
	
	foreach ($categorias as $categoria) {
		log_wp('debug', "ESTORNO: verificando se produto é assinatura: {$item_id}, categorias: {$categoria->slug}");
		
		if(is_categoria_assinatura_sq_semanal($categoria)) {
			log_wp('debug', 'É assinatura semanal');
			$remover_validade .= " -7 days";
		}
		elseif(is_categoria_assinatura_sq_quinzenal($categoria)) {
			log_wp('debug', 'É assinatura quinzenal');
			$remover_validade .= " -15 days";
		}
		elseif(is_categoria_assinatura_sq_mensal($categoria)) {
			log_wp('debug', 'É assinatura mensal');
			$remover_validade .= " -1 months";
		}
		elseif(is_categoria_assinatura_sq_trimestral($categoria)) {
			log_wp('debug', 'É assinatura trimestral');
			$remover_validade .= " -3 months";
		}
		elseif(is_categoria_assinatura_sq_semestral($categoria)) {
			log_wp('debug', 'É assinatura semestral');
			$remover_validade .= " -6 months";
		}
		elseif(is_categoria_assinatura_sq_anual($categoria)) {
			log_wp('debug', 'É assinatura anual');
			$remover_validade .= " -12 months";
		}
		
	}

	log_wp("debug", "ESTORNO: Remover validade: " . $remover_validade);
	if($remover_validade) {

		$data_validade = get_user_meta($usuario_id, 'assinante-sq-validade', true);

		log_wp('info', "Data validade: {$data_validade}");
		log_wp('info', "Remover validade: {$remover_validade}");
		
		$nova_validade = date('Y-m-d', strtotime($data_validade . $remover_validade));

		$nova_validade = date('Y-m-d', strtotime($nova_validade . ' -1 days'));

		log_wp('info', "A nova validade: {$nova_validade}");

		update_user_meta($usuario_id, 'assinante-sq-validade', $nova_validade);

		if($nova_validade < date('Y-m-d')){
			update_user_meta($usuario_id, 'assinante-sq', 'no');
		}else{
			update_user_meta($usuario_id, 'assinante-sq', 'yes');
		}
	}
	else {
		log_wp('info',"Nova validade nula ou igual a zero");
	}

}

/**
 * Salva o estorno
 * 
 * @since J5 
 * 
 * @param WC_Order $pedido
 * @param string $status Possível erro de comunicação com o PagSeguro. String vazia se não houver erro
 * @param float $valor
 * @param array|null $produtos_ids
 * 
 */ 

function salvar_estorno($tipo, $pedido, $status, $valor, $itens_ids = null, $produtos_ids = null, $motivo = "", $revoga_acesso = TRUE)
{
	global $wpdb;

	$wpdb->insert('estornos', array(
    	'usu_id' => $pedido->get_user_id(),
    	'pedido_id' => $pedido->get_id(),
		'est_data_hora' => date("Y-m-d H:i:s"),
		'est_itens_ids' => $itens_ids ? implode(", ", $itens_ids) : "",
    	'est_produtos_ids' => $produtos_ids ? implode(", ", $produtos_ids) : "",
    	'est_valor' => $valor,
    	'est_status' => $status,
		'est_motivo' => $motivo,
		'est_tipo' => $tipo,
		'est_revoga_acesso' => $revoga_acesso
	));

}


/**
 * Lista estornos
 * 
 * @since J5 
 * 
 * @param int $offset
 * @param int $limit
 * 
 * @return array
 */

function listar_estornos($offset = 0, $limit = 20)
{
	global $wpdb;

	$sql = "SELECT * FROM estornos ORDER BY est_data_hora DESC LIMIT $offset, $limit ";

	return $wpdb->get_results($sql, ARRAY_A);
}

/**
 * Conta total de estornos
 * 
 * @since J5 
 * 
 * @return int
 */

function contar_estornos($pedido_id = null, $tipo = null)
{
	global $wpdb;

	$where = " WHERE 1=1 ";

	if($pedido_id){
		$where .= " AND pedido_id = ".$pedido_id;
	}

	if($tipo){
		$where .= " AND est_tipo = '".$tipo."'";
	}

	return $wpdb->get_var("SELECT COUNT(`est_id`) FROM estornos ".$where );
}

/**
 * Recupera estorno
 * 
 * @since K1
 * 
 * @param int $pedido_id
 * 
 * @return array
 */

function get_estorno($pedido_id)
{
	global $wpdb;

	$sql = "SELECT * FROM estornos WHERE pedido_id = {$pedido_id}";

	return $wpdb->get_row($sql, ARRAY_A);
}

/**
 * Recupera todos os estornos de um pedido
 * 
 * 
 * @param int $pedido_id
 * 
 * @return array
 */

function get_estornos_por_pedido($pedido_id)
{
    $mcache_id = MCACHE_ESTORNOS_POR_PEDIDO . $pedido_id;
    $mcache = wp_cache_get($mcache_id);
    
    if($mcache === false) {
    	global $wpdb;
    
    	$sql = "SELECT * FROM estornos WHERE pedido_id = {$pedido_id}";

    	$mcache = $wpdb->get_results($sql, ARRAY_A);
	    
	    wp_cache_set($mcache_id, $mcache);
    }
    
    return $mcache;
}

/**
 * Recuper valor total do estorno
 * 
 * @since K1
 * 
 * @param int $pedido_id
 * 
 * @return float
 */

function get_total_estorno($pedido_id)
{
	$estorno = get_estorno($pedido_id);

	if($estorno) {
		return $estorno['est_valor'];
	}

	return 0;
}

function get_order_id_by_url()
{
	$url = explode("/", $_SERVER['REQUEST_URI']);
	
	if(strpos($url[3], "?") !== false) {
	    $suf = explode("?", $url[3]);
	    return $suf[0];
	}
	else {
	   return $url[3];
	}
}

function is_pedido_produto_disponivel($pedido_id, $product_id){

	$order = new WC_Order($pedido_id);
	if($order->status == 'completed'){//Só vira "refunded" quando o valor estornado é total
		return !is_produto_estornado($pedido_id, $product_id, TRUE) && !is_produto_indisponivel($product_id, $pedido_id);
	}else if($order->status == 'refunded'){
		return !is_produto_estornado($pedido_id, $product_id, TRUE) && !is_produto_indisponivel($product_id, $pedido_id);
	}else{
		return FALSE;
	}
}

function get_estorno_tipo($pedido_id)
{
	$estorno = get_estorno($pedido_id);

	if($estorno) {
		return $estorno['est_tipo'];
	}

	return NULL;
}

function is_item_pedido_estornado($pedido_id, $item_id, $acesso_revogado = null){

	$estornos = get_estornos_por_pedido(trim($pedido_id));

	if($estornos) {

		$revogou = FALSE;
		$estornou = FALSE;
		foreach($estornos as $estorno){

			//Quando o estorno possui o ID dos itens
			if($estorno['est_itens_ids']){
				
				$itens_ids = explode(', ', $estorno['est_itens_ids']);

				if(in_array($item_id, $itens_ids)) {
					$revogou = $revogou || $estorno['est_revoga_acesso'];				
					$estornou = TRUE;
				}

			}
		}

		if(!is_null($acesso_revogado)){
			return ($acesso_revogado && $revogou) || (!$acesso_revogado && $estornou && !$revogou);
		}else{
			return $estornou;
		}

	}

	return FALSE;

}

function is_produto_estornado($pedido_id, $produto_id, $acesso_revogado = null)
{
	$estornos = get_estornos_por_pedido(trim($pedido_id));

	if($estornos) {

		$revogou = FALSE;
		$estornou = FALSE;
		foreach($estornos as $estorno){

			//Quando o estorno possui o ID dos itens
			if($estorno['est_itens_ids']){
				
				$pedido = wc_get_order($pedido_id);  
				$items = $pedido->get_items();
				$itens_ids = explode(', ', $estorno['est_itens_ids']);

				foreach($items as $item){
					
					if($item['product_id'] == $produto_id && in_array($item->get_id(), $itens_ids)){
						$revogou = $revogou || $estorno['est_revoga_acesso'];
						$estornou = TRUE;
					}
				}

			}else{//Quando o estorno NÃO possui o ID dos itens continua fazendo com base no produto
				$produtos_ids = explode(', ', $estorno['est_produtos_ids']);
				if(in_array(trim($produto_id), $produtos_ids)) {
					$revogou = $revogou || $estorno['est_revoga_acesso'];				
					$estornou = TRUE;
				}
			}

		}

		if(!is_null($acesso_revogado)){
			return ($acesso_revogado && $revogou) || (!$acesso_revogado && $estornou && !$revogou);
		}else{
			return $estornou;
		}

	}

	return FALSE;
}

/**
 * Retorna um array com o dado solicitado de cada item do pacote
 * 
 * @since K5
 * 
 * @param int $pedido_id Id do pedido do item de pacote
 * @param WC_Order_Item $item_raiz_pacote Item do pedido que é raiz do pacote
 * @param string $info Identificação do dado do item que será incluído no resultado: 'item_id' ou 'product_id'
 * 
 * @return array
 */
function get_itens_pacote_info($pedido_id, $item_raiz_pacote, $info){

	$pedido = wc_get_order($pedido_id);
  
	$items = $pedido->get_items();
	
	$itens_info_pacote_pedido = array();
	//Verifica quais itens do pedido são parte do pacote
	foreach($items as $item_aux){
		if(in_array($item_aux['bundle_cart_key'], $item_raiz_pacote['bundled_items'])){
			if($info == 'item_id'){
				array_push($itens_info_pacote_pedido, $item_aux->get_id());
			}elseif($info == 'product_id'){
				array_push($itens_info_pacote_pedido, $item_aux['product_id']);
			}
		}
	}
	return empty($itens_info_pacote_pedido)?null:$itens_info_pacote_pedido;
}

/**
 * Retorna os item_id dos itens de um pacote
 */
function get_itens_pacote_no_pedido($pedido_id, $item_raiz_pacote){
	return get_itens_pacote_info($pedido_id, $item_raiz_pacote, 'item_id');
}

/**
 * Retorna os product_id dos itens de um pacote
 */
function get_produtos_pacote_no_pedido($pedido_id, $item_raiz_pacote){
	return get_itens_pacote_info($pedido_id, $item_raiz_pacote, 'product_id');
}

/**
 * Recupera valor percentual de um item de um pacote a partir de um pedido
 * 
 * @since K5
 *
 * @param int $pedido_id Id do pedido do item de pacote
 * @param int $item_id Id do produto do item do pedido
 * @param array $item_raiz_pacote Item do pedido que é raiz do pacote
 * 
 * @return float
 */
function get_valor_percentual_item_pacote_no_pedido($pedido_id, $item_id, $item_raiz_pacote)
{
	$produtos_pacote_pedido = get_produtos_pacote_no_pedido($pedido_id, $item_raiz_pacote);
	
	$item = new WC_Product($item_id);
	
	$total = 0;
	
	foreach ($produtos_pacote_pedido as $product_id) {
		$product = new WC_Product($product_id);
		$total += $product->price;
		//echo "Produto: ".$product_id."/".$product->price."<br/>";
	}
	
	//echo "=============Item_price: ".$item->price."/".$total."<br/>";
	
	return $item->price / $total;
}

function expoconcursos_woocommerce_get_order_item_totals( $total_rows, $order, $tax_display ) { 
	if ( $order->get_total() > 0 && $order->get_payment_method_title() ) {
		$total_rows['payment_method'] = array(
			'label' => 'Método de pagamento:',
			'value' => $order->get_payment_method_title(),
		);
	}
	return $total_rows;
}
add_filter( 'woocommerce_get_order_item_totals', 'expoconcursos_woocommerce_get_order_item_totals', 10, 3 );


function expoconcursos_woocommerce_order_status_cancelled($pedido_id){
	$pedido = wc_get_order($pedido_id);
	foreach($pedido->get_items() as $item){
		revogar_acesso_item_pedido($item);
	}
}
add_action('woocommerce_order_status_cancelled', 'expoconcursos_woocommerce_order_status_cancelled', 10, 1);

/**
 * Add a discount to an Orders programmatically
 * (Using the FEE API - A negative fee)
 *
 * @since  3.2.0
 * @param  int     $order_id  The order ID. Required.
 * @param  string  $title  The label name for the discount. Required.
 * @param  mixed   $amount  Fixed amount (float) or percentage based on the subtotal. Required.
 * @param  string  $tax_class  The tax Class. '' by default. Optional.
 */
function wc_order_add_discount( $order_id, $title, $amount, $tax_class = '' ) {
    $order    = wc_get_order($order_id);
    $subtotal = $order->get_subtotal();
    $item     = new WC_Order_Item_Fee();
    
    if ( strpos($amount, '%') !== false ) {
        $percentage = (float) str_replace( array('%', ' '), array('', ''), $amount );
        $percentage = $percentage > 100 ? -100 : -$percentage;
        $discount   = $percentage * $subtotal / 100;
    } else {
        $discount = (float) str_replace( ' ', '', $amount );
        $discount = $discount > $subtotal ? -$subtotal : -$discount;
    }
    
    $item->set_tax_class( $tax_class );
    $item->set_name( $title );
    $item->set_amount( $discount );
    $item->set_total( $discount );
    
    if ( '0' !== $item->get_tax_class() && 'taxable' === $item->get_tax_status() && wc_tax_enabled() ) {
        $tax_for   = array(
            'country'   => $order->get_shipping_country(),
            'state'     => $order->get_shipping_state(),
            'postcode'  => $order->get_shipping_postcode(),
            'city'      => $order->get_shipping_city(),
            'tax_class' => $item->get_tax_class(),
        );
        $tax_rates = WC_Tax::find_rates( $tax_for );
        $taxes     = WC_Tax::calc_tax( $item->get_total(), $tax_rates, false );
        print_pr($taxes);
        
        if ( method_exists( $item, 'get_subtotal' ) ) {
            $subtotal_taxes = WC_Tax::calc_tax( $item->get_subtotal(), $tax_rates, false );
            $item->set_taxes( array( 'total' => $taxes, 'subtotal' => $subtotal_taxes ) );
            $item->set_total_tax( array_sum($taxes) );
        } else {
            $item->set_taxes( array( 'total' => $taxes ) );
            $item->set_total_tax( array_sum($taxes) );
        }
        $has_taxes = true;
    } else {
        $item->set_taxes( false );
        $has_taxes = false;
    }
    $item->save();
    
    $order->add_item( $item );
    $order->calculate_totals( $has_taxes );
    $order->save();
}


function get_pedido_recorrente_id($pedido_id)
{
    $recorrente_proxy = get_post_meta($pedido_id, '_subscription_renewal', true);

    if($recorrente_proxy) {
        return $recorrente_proxy;
    }

    return null;
}

function get_subscription_from_pedido($pedido_id)
{
	global $wpdb;
	$subscription_id = $wpdb->get_var("select ID from wp_posts where post_parent = {$pedido_id} and post_type = 'shop_subscription'");
	return $subscription_id;
}

function is_pedido_recorrente_suspenso($pedido_id)
{
    global $wpdb;

    return $wpdb->get_var("select ID from wp_posts where post_parent = {$pedido_id} and post_type = 'shop_subscription' and post_status = 'wc-on-hold'") ? true : false;
}

function is_pedido_recorrente_cancelado($pedido_id)
{
    global $wpdb;

    return $wpdb->get_var("select ID from wp_posts where post_parent = {$pedido_id} and post_type = 'shop_subscription' and post_status in ( 'wc-pending-cancel', 'wc-cancelled' )") ? true : false;
}

function is_pedido_recorrente_expirado($pedido_id)
{
    global $wpdb;

    return $wpdb->get_var("select ID from wp_posts where post_parent = {$pedido_id} and post_type = 'shop_subscription' and post_status = 'wc-expired'") ? true : false;
}

function is_pedido_recorrente_pendente($pedido_id)
{
    global $wpdb;

    return $wpdb->get_var("select ID from wp_posts where post_parent = {$pedido_id} and post_type = 'shop_subscription' and post_status in ( 'wc-pending', 'wc-failed', 'wc-processing' ) ") ? true : false;
}
function hack_is_pedido_recorrente_cancelado($pedido_id)
{
    global $wpdb;

    $row = $wpdb->get_row("select ID from wp_posts where post_parent = {$pedido_id} and post_type = 'shop_subscription' ");

    if($row) {
        if($row->post_status != 'wc-active') {
            return true;
        }

        return false;
    }
    else {
        return false;
    }

}
