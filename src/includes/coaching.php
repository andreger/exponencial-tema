<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/painel-coaching/app-expocoaching/libraries/podio-php-4.0.1/PodioAPI.php';

switch ($_SERVER['HTTP_HOST']) {
	case 'exponencial':
	case 'homol.exponencialconcursos.com.br': {
		/**************************************************
		 * Local e Homologação
		 *************************************************/
		// APP COACHEE
	    defined('PODIO_CLIENT_COACHEE')                OR define('PODIO_CLIENT_COACHEE', 'coachee');
	    defined('PODIO_CLIENT_COACHEE_SECRET')         OR define('PODIO_CLIENT_COACHEE_SECRET', 'ffWq5nYmixEUp49EJJKl2H47awvDaOSpeJuUKylOUAy3hu9rNlWDgnIyea2GGcaJ');
	    defined('PODIO_APP_COACHEE')                   OR define('PODIO_APP_COACHEE', 18494445);
	    defined('PODIO_APP_COACHEE_TOKEN')             OR define('PODIO_APP_COACHEE_TOKEN', 'eade45b56fe143f395f6adb37818db8d');
		
		// APP COACHEE ESTUDO
	    defined('PODIO_APP_COACHEE_ESTUDO')            OR define('PODIO_APP_COACHEE_ESTUDO', 18494448);
	    defined('PODIO_APP_COACHEE_ESTUDO_TOKEN')      OR define('PODIO_APP_COACHEE_ESTUDO_TOKEN', 'c569bdc853684245bc3b3ae4db66b065');

		// APP_PAGAMENTOS
	    defined('PODIO_APP_PAGAMENTOS')                OR define('PODIO_APP_PAGAMENTOS', 18908174);
	    defined('PODIO_APP_PAGAMENTOS_TOKEN')          OR define('PODIO_APP_PAGAMENTOS_TOKEN', '91a68450fe9349d8bee4dbcf44bb01c3');
	}
	case 'exponencialconcursos.com.br':
	case 'www.exponencialconcursos.com.br': {
		/**************************************************
		 * Produção
		 *************************************************/
		// APP COACHEE
	    defined('PODIO_CLIENT_COACHEE')                OR define('PODIO_CLIENT_COACHEE', 'coachee-pd4ph9');
	    defined('PODIO_CLIENT_COACHEE_SECRET')         OR define('PODIO_CLIENT_COACHEE_SECRET', 'BBOuZ33xpEX0THWpcqhIA0pEtlDGkZUihasfsyEiMK5HudYcQFPx5vXVuxcomn6y');

	    defined('PODIO_APP_COACHEE')                   OR define('PODIO_APP_COACHEE', 17602823);
	    defined('PODIO_APP_COACHEE_TOKEN')             OR define('PODIO_APP_COACHEE_TOKEN', '7a6b84cb9ed94bd4b7bf23109e299f2c');

	    defined('PODIO_APP_COACHEE_ESTUDO')            OR define('PODIO_APP_COACHEE_ESTUDO', 17602834);
		defined('PODIO_APP_COACHEE_ESTUDO_TOKEN')      OR define('PODIO_APP_COACHEE_ESTUDO_TOKEN', '1abc2a23624c4cabb594c4f724253f74');
		
		// APP_PAGAMENTOS
		defined('PODIO_APP_PAGAMENTOS')                OR define('PODIO_APP_PAGAMENTOS', 17602836);
		defined('PODIO_APP_PAGAMENTOS_TOKEN')          OR define('PODIO_APP_PAGAMENTOS_TOKEN', '10b696cdde3d4248be8d9047544e23a7');

		break;
	}
}

define('PODIO_COACHEE_CAMPO_STATUS', 'status');
define('PODIO_COACHEE_CAMPO_COACH', 'coach');
define('PODIO_COACHEE_CAMPO_AREA', 'escolha-sua-turma-de-coaching');
define('PODIO_COACHEE_STATUS_FORMULARIO_BASICO', 6);
define('PODIO_COACHEE_STATUS_FORMULARIO_DETALHADO', 9);
define('PODIO_COACHEE_STATUS_PERFIL_PSICOPEDAGOGICO', 11);
define('PODIO_COACHEE_STATUS_PLANILHAS_DETALHADAS', 12);
define('PODIO_COACHEE_STATUS_INSCRICAO_PENDENTE', 7);
define('PODIO_COACHEE_STATUS_PAGAMENTO_LIBERADO', 13);
define('PODIO_COACHEE_STATUS_ATIVO', 8);
define('PODIO_COACHEE_STATUS_EX_ALUNO', 2);
define('PODIO_COACHEE_STATUS_SUSPENSO', 10);
define('PODIO_COACHEE_STATUS_NAO_CONVERTIDO', 5);
define('PODIO_COACHEE_STATUS_ALUNO_CHAVE', 14);

define('PODIO_COACHEE_TURMA_1', 1);
define('PODIO_COACHEE_TURMA_2', 2);
define('PODIO_COACHEE_TURMA_3', 3);
define('PODIO_COACHEE_TURMA_4', 4);
define('PODIO_COACHEE_TURMA_INSS', 5);

define('PODIO_COMBO_ESCOLHA_SUA_TURMA', 1);

define('EMAIL_COBRANCA_COACHING_DIAS_ANTES', 2);

define('PODIO_COACHE_ITENS_POR_PAGINA', 50);

function get_alunos_ativos_podio($nome_consultor = null, $forceUpdate = FALSE)
{
	if($is_admin = is_administrador()){
		$transient = get_transient(TRANSIENT_USUARIOS_ATIVOS_PODIO);
	}else{
		$transient = get_transient(TRANSIENT_USUARIOS_ATIVOS_PODIO.sanitize_title($nome_consultor));
	}

	$usuarios = get_transient($transient);

	if(!$usuarios || ($is_admin && $forceUpdate)) { 

		try {
			Podio::setup(PODIO_CLIENT_COACHEE, PODIO_CLIENT_COACHEE_SECRET);
			Podio::authenticate_with_app(PODIO_APP_COACHEE, PODIO_APP_COACHEE_TOKEN);
	
			$filters[PODIO_COACHEE_CAMPO_STATUS] = array(PODIO_COACHEE_STATUS_ATIVO);
			
			if(!$is_admin && !is_revisor_sq() && $nome_consultor) {
				
				$campo = PodioAppField::get(PODIO_APP_COACHEE, PODIO_COACHEE_CAMPO_COACH);
				
				$id_consultor = false;
				foreach ($campo->config['settings']['options'] as $option) {
					if($option['text'] == $nome_consultor) { 
						$id_consultor = $option['id'];
						break;
					}
				}
				
				if($id_consultor) {
					$filters[PODIO_COACHEE_CAMPO_COACH] = array($id_consultor);
				}
			}
						
			$items = PodioItem::filter(PODIO_APP_COACHEE, 
					array(
							'limit' => 200, 
							'sort_by' => 'nome', 
							'sort_desc' => false, 
							'filters' => $filters
					));
		} catch (PodioRateLimitError $pe) {
			set_mensagem_flash('erro', 'O limite de utilização do Podio foi atingido. Tente novamente mais tarde.');
			echo get_mensagem_flash() ;
		} catch (PodioError $pe) {
			print_r($pe->__toString());
		}
	
		$usuarios = array();
		
		if($items) {
			foreach ($items as $item) {
				$user = get_usuario_by_email($item->fields['e-mail']->values);
				
				if($user->ID) {
					$usuario = get_usuario_array($user->ID);
				
					array_push($usuarios, $usuario);
				}
			}
		}
		
		set_transient($transient, $usuarios, 60 * MINUTE_IN_SECONDS);
	}
	
	return $usuarios;
}

function is_aluno_ativo_podio($usuario_id = null)
{
	if(is_null($usuario_id)) {
		$usuario_id = get_current_user_id();
	}
	
	$alunos = get_alunos_ativos_podio();
	
	foreach ($alunos as $aluno) {
		if($aluno['id'] == $usuario_id) {
			return true;
		}
	}
	
	return false;
}

function is_aluno_consultor($nome_consultor, $aluno_id)
{
	$alunos = get_alunos_ativos_podio($nome_consultor);

	foreach ($alunos as $aluno) {
		if($aluno['id'] == $aluno_id) {
			return true;
		}
	}

	return false;
}

/**
 * Retorna um objeto Coachee do Podio a partir de um e-mail informado
 * 
 * [JOULE][J1] UPGRADE - Cadastro automático de pagamentos do coaching no Podio
 *  
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/upgrades/items/193
 * 
 * @since 10.0.0
 * 
 * @param string $email E-mail do usuário
 * 
 * @return PodioItem|null Podio Item ou null se não encontrar
 */

function get_coachee_por_email($email)
{
	try {
		Podio::setup(PODIO_CLIENT_COACHEE, PODIO_CLIENT_COACHEE_SECRET);
		Podio::authenticate_with_app(PODIO_APP_COACHEE, PODIO_APP_COACHEE_TOKEN);

		$items = PodioItem::filter(PODIO_APP_COACHEE, array (
			'filters' => array (
				'e-mail' => $email
			),
			'limit' => 1
		));

		if($items->filtered > 0) {
			return $items[0];
		}
		else {
			
		}
	}
	catch (PodioError $pe) {
		log_podio('error', 'Podio: Erro ao recuperar aluno. ' . $pe->__toString());
	}
}

/**
 * Retorna uma turma de coaching
 * 
 * [JOULE][J1] UPGRADE - Cadastro automático de pagamentos do coaching no Podio
 *  
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/upgrades/items/193
 * 
 * @since 10.0.0
 * 
 * @param $id int Id da área de coaching do Podio
 * 
 * @return mixed Turma/Área de coaching. Tabela exponenc_corp.coaching_combos
 */

function get_turma_coaching($id) 
{
	$wpdb_corp = new wpdb(DB_USER_CORP, DB_PASSWORD_CORP, DB_NAME_CORP, DB_HOST_CORP);
	
	$sql = sprintf("SELECT * FROM coaching_combos WHERE cco_key = '%s' AND cco_valor = $id", PODIO_COACHEE_CAMPO_AREA);
	
	return $wpdb_corp->get_row($sql);
}

/**
 * Retorna todos as turmas de coaching cadastradas no Podio
 * 
 * [JOULE][J1] UPGRADE - Cadastro automático de pagamentos do coaching no Podio
 *  
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/upgrades/items/193
 * 
 * @since 10.0.0
 * 
 * @return array Lista de [id => area]
 */

function get_turmas_coaching_combo() 
{
	$wpdb_corp = new wpdb(DB_USER_CORP, DB_PASSWORD_CORP, DB_NAME_CORP, DB_HOST_CORP);
	
	$sql = sprintf("SELECT * FROM coaching_combos WHERE cco_key = '%s'", PODIO_COACHEE_CAMPO_AREA);
	
	$resultado = $wpdb_corp->get_results($sql);

	$combo = [];
	foreach ($resultado as $item) {
		$combo[$item->cco_valor] = $item->cco_texto;
	}

	return $combo;
}

/**
 * Salva a relação produto WP x área Podio
 * 
 * [JOULE][J1] UPGRADE - Cadastro automático de pagamentos do coaching no Podio
 *  
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/upgrades/items/193
 * 
 * @since 10.0.0
 * 
 * @param int $produto_id Id do produto no WP
 * @param int $area_id Id da área no Podio (aplicativo Coachee, campo 'escolha-sua-turma-de-coaching')
 */

function salvar_produto_area($produto_id, $area_id)
{
	global $wpdb;
	
	$wpdb->insert('coaching_produtos_areas', array(
		'produto_id' => $produto_id,
		'area_id' => $area_id
	));
}

/**
 * Lista a relação produto WP x área Podio
 * 
 * [JOULE][J1] UPGRADE - Cadastro automático de pagamentos do coaching no Podio
 *  
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/upgrades/items/193
 * 
 * @since 10.0.0
 * 
 * @return array Lista de produtos_areas 
 */

function listar_produtos_areas()
{
	global $wpdb;
	
	$sql = "SELECT * FROM coaching_produtos_areas cpa JOIN wp_posts p ON cpa.produto_id = p.ID";
	$resultado = $wpdb->get_results($sql);

	$produtos_coaching = [];
	foreach ($resultado as $item) {
		// recupera dados da área. Essa informação está em outro banco de dados.
		$area = get_turma_coaching($item->area_id);

		$produto_coaching = [
			'produto_id' => $item->produto_id,
			'produto_nome' => $item->post_title,
			'area_id' => $item->area_id,
			'area_nome' => $area->cco_texto
		];

		array_push($produtos_coaching, $produto_coaching);
	}

	return $produtos_coaching;
}

/**
 * Verifica a existência de um produto_area
 * 
 * [JOULE][J1] UPGRADE - Cadastro automático de pagamentos do coaching no Podio
 *  
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/upgrades/items/193
 * 
 * @since 10.0.0
 * 
 * @param int $produto_id Id do produto no WP
 * @param int $area_id Id da área no Podio (aplicativo Coachee, campo 'escolha-sua-turma-de-coaching')
 * 
 * @return bool
 */

function existe_coaching_produto($produto_id)
{
	return get_coaching_area_by_produto($produto_id) ? true : false;
}

/**
 * Remove a relação produto WP x área Podio
 * 
 * [JOULE][J1] UPGRADE - Cadastro automático de pagamentos do coaching no Podio
 *  
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/upgrades/items/193
 * 
 * @since 10.0.0
 * 
 * @param int $produto_id Id do produto no WP
 * @param int $area_id Id da área no Podio (aplicativo Coachee, campo 'escolha-sua-turma-de-coaching')
 */

function remover_produto_area($produto_id, $area_id)
{
	global $wpdb;
	
	$wpdb->delete('coaching_produtos_areas', array(
		'produto_id' => $produto_id,
		'area_id' => $area_id
	));
}

/**
 * Retorna um produto_area através de um id de um produto
 * 
 * [JOULE][J1] UPGRADE - Cadastro automático de pagamentos do coaching no Podio
 *  
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/upgrades/items/193
 * 
 * @since 10.0.0
 * 
 * @param int $produto_id Id do produto no WP
 * 
 * @return mixed Produtos_areas
 */

function get_coaching_area_by_produto($produto_id)
{
	global $wpdb;
	
	$sql = "SELECT * FROM coaching_produtos_areas WHERE produto_id = {$produto_id}";

	return $wpdb->get_row($sql);
}

/**
 * Gera um array para representar o Podio Item Pagamento
 * 
 * [JOULE][J1] UPGRADE - Cadastro automático de pagamentos do coaching no Podio
 *  
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/upgrades/items/193
 * 
 * @since 10.0.0
 * 
 * @param mixed $podio_item Podio Item Pagamento
 * 
 * @return array
 */

function get_pagamento_array($podio_item)
{
	$pagamento = array();
	$pagamento['item_id'] = $podio_item->item_id;

	foreach ($podio_item->fields as $campo) {

		switch ($campo->external_id) {
			case 'aluno': $pagamento['aluno'] = $campo->values[0]->item_id; break;
			case 'mes-de-referencia': $pagamento['mes_de_referencia'] = $campo->start_date; break;
			case 'coach': $pagamento['coach'] = $campo->values[0]['text']; break;
			case 'valor': $pagamento['valor'] = $campo->values['value']; break;
			case 'status': $pagamento['status'] = $campo->values[0]['text']; break;
			case 'produto': $pagamento['produto'] = $campo->values[0]['text']; break;
		}
	}

	return $pagamento;
}

/**
 * Retorna último pagamento de um aluno
 * 
 * [JOULE][J1] UPGRADE - Cadastro automático de pagamentos do coaching no Podio
 *  
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/upgrades/items/193
 * 
 * @since 10.0.0
 * 
 * @param string $email E-mail do usuário
 * 
 * @return string Data do último pagamento no formato YYYY-MM-DD
 */

function get_ultimo_pagamento_de_aluno($email)
{
	try {
		$coachee = get_coachee_por_email($email);

		Podio::setup(PODIO_CLIENT_COACHEE, PODIO_CLIENT_COACHEE_SECRET);
		Podio::authenticate_with_app(PODIO_APP_COACHEE, PODIO_APP_COACHEE_TOKEN);
			
		$items = PodioItem::filter(PODIO_APP_PAGAMENTOS,
			array(
				'filters' => array(
					'aluno' => $coachee->item_id
				),
				'sort_by' => 'mes-de-referencia',
				'sort_desc' => true,
				'limit' => 1
			)
		);

		if($items->filtered > 0) {
			$item = get_pagamento_array($items[0]);
			
			return $item['mes_de_referencia']->format('Y-m-d');
		}

		return null;
	}
	catch(PodioError $pe) {
		log_podio('error', 'Podio: Erro ao listar último pagamento de aluno. ' . $pe->__toString());
	}
}

/**
 * Salva um novo pagamento no aplicativo Pagamentos do Podio
 * 
 * [JOULE][J1] UPGRADE - Cadastro automático de pagamentos do coaching no Podio
 *  
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/upgrades/items/193
 * 
 * @since 10.0.0
 * 
 * @param int $usuario_podio_id Id do usuário no Podio
 * @param string $data_pagamento Data a ser cadastrada no pagamento
 * @param float $valor Valor a ser cadastrado no pagamento
 */

function salvar_pagamento_coaching($usuario_podio_id, $data_pagamento, $valor)
{
	$fields = [
		'aluno' => $usuario_podio_id,
		'mes-de-referencia' => $data_pagamento . ' 11:00:00',
		'valor' => [
			'value' => $valor,
			'currency' => "BRL"
		]
	];

	try {
		Podio::setup(PODIO_CLIENT_COACHEE, PODIO_CLIENT_COACHEE_SECRET);
		Podio::authenticate_with_app(PODIO_APP_COACHEE, PODIO_APP_COACHEE_TOKEN);
			
		PodioItem::create(PODIO_APP_PAGAMENTOS, ['fields' => $fields]);
	}
	catch(PodioError $pe) {
		log_podio('error', 'Podio: Erro ao listar último pagamento de aluno. ' . $pe->__toString());

		print_r($pe->__toString());
	}
}

/**
 * Realiza o processamento do pagamento do coaching. Regras descritas no Podio
 * 
 * [JOULE][J1] UPGRADE - Cadastro automático de pagamentos do coaching no Podio
 *  
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/upgrades/items/193
 * 
 * @since 10.0.0
 * 
 * @param int $produto_id Id do produto no WP
 * @param WC_Order $order Pedido no WP (para extrair usuário, valor da compra, descontos, etc.)
 */

function pagamento_coaching($produto_id, $order)
{
	// recupera o tipo do coaching
	if($tipo = get_produto_coaching_tipo($produto_id)) {

		$usuario = get_usuario_array($order->user_id);
		$coachee = get_coachee_por_email($usuario['email']);


		if(!$coachee) {
			Podio::setup(PODIO_CLIENT_COACHEE, PODIO_CLIENT_COACHEE_SECRET);
			Podio::authenticate_with_app(PODIO_APP_COACHEE, PODIO_APP_COACHEE_TOKEN);

			PodioItem::create(PODIO_APP_COACHEE, array('fields' => 
				array(
					"nome" => $usuario['nome_completo'],
					"e-mail" => $usuario['email'],
					"status" => [PODIO_COACHEE_STATUS_FORMULARIO_BASICO, PODIO_COACHEE_STATUS_ATIVO],
					"data-formulario-basico" => get_data_hora_agora()
				)
			));

			PodioItem::create(PODIO_APP_COACHEE_ESTUDO, array('fields' =>
					array(
							"e-mail" => $usuario['email'],
					)
			));

			$coachee = get_coachee_por_email($usuario['email']);
		}

		// recupera área do produto comprado pelo aluno
		$area = get_coaching_area_by_produto($produto_id);
		$area_id = $area ? $area->area_id : null;

		// verifica se aluno está inscrito na área correspondente à área do produto adquirido
		// if($area_id == $coachee->fields[PODIO_COACHEE_CAMPO_AREA]->values[0]['id']) {
			$ultimo_pagamento = get_ultimo_pagamento_de_aluno($usuario['email']);

			// define a data do próximo pagamento. Se existir último pagamento significa que é aluno antigo. Caso contrário é aluno novo.
			if($ultimo_pagamento) {
				$proximo_pagamento = get_data_proximo_pagamento($ultimo_pagamento);
			} 
			else {
				$proximo_pagamento = get_data_primeiro_pagamento();
				// salva data de referência

				$podio['referencia-pgto'] = $proximo_pagamento . ' 11:00:00';
				atualizar_item_podio($coachee->item_id, $podio);
			}

			// calcula o valor do produto levando em consideração os descontos
			$valor = get_preco_produto_com_descontos($produto_id, $order);

			// cadastra o primeiro pagamento
			salvar_pagamento_coaching($coachee->item_id, $proximo_pagamento, $valor);

			// verifica quantos pagamentos subsequentes serão feitos
			switch ($tipo) {
				case COACHING_TRIMESTRAL: $outros_meses = 2; break;
				case COACHING_SEMESTRAL: $outros_meses = 5; break;
				default: $outros_meses = 0; break;
			}

			// cadastra os pagamentos subsequentes
			for($i = 0; $i < $outros_meses; $i++) {
				$proximo_pagamento = get_data_proximo_pagamento($proximo_pagamento);
				salvar_pagamento_coaching($coachee->item_id, $proximo_pagamento, 0);
			}

		// }

		// notifica ao administrador que uma compra foi realizada indevidamente
		// else {
		// 	$produto = new WC_Product($produto_id);

		// 	$titulo = "Compra indevida feita pelo aluno {$usuario['nome_completo']}";
		// 	$mensagem = "O aluno <b>{$usuario['nome_completo']}</b> realizou a compra do produto <b>{$produto->post->post_title}</b>, mas não está inscrito na turma de coaching correspondente.";

		// 	enviar_email(EMAIL_COACHING, $titulo, $mensagem, null, null);
		// }

	}

	// se não tiver tipo significa que não é pagamento de coaching
	else {
		log_wp("debug", "o produto nao é pagamento de coaching");
		return;
	}
	
}

/**
 * Retorna o link do produto WP referente ao pagamento mensal de turma de coaching
 * 
 * @since Joule 1
 * 
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/1973
 * 
 * @param int $area_id Id da área cadastrada no Podio
 * 
 * @return string URL do produto WP
 */

function get_pagamento_coaching_link_mensal($area_id)
{
	switch ($area_id) 
	{
		case COACHING_AREA_FISCAL: 
			return "https://www.exponencialconcursos.com.br/produto/coaching-exponencial-concursos-area-fiscal/";
			break;
		
		case COACHING_AREA_CONTROLE: 
			return "https://www.exponencialconcursos.com.br/produto/coaching-exponencial-concursos-area-de-controle/";
			break;

		case COACHING_AREA_POLICIAL: 
			return "https://www.exponencialconcursos.com.br/produto/coaching-exponencial-concursos-area-policial-2/";
			break;

		case COACHING_AREA_TRIBUNAIS: 
			return "https://www.exponencialconcursos.com.br/produto/coaching-tribunais-tre-trt-trf-e-tj/";
			break;

		case COACHING_AREA_LEGISLATIVA: 
			return "https://www.exponencialconcursos.com.br/produto/coaching-exponencial-concursos-area-legislativa-2/";
			break;

		case COACHING_AREA_JURIDICA: 
			return "https://www.exponencialconcursos.com.br/produto/coaching-exponencial-concursos-area-juridica-2/";
			break;

		case COACHING_AREA_BANCARIA: 
			return "https://www.exponencialconcursos.com.br/produto/coaching-exponencial-concursos-area-bancaria-2/";
			break;

		case COACHING_AREA_INSS: 
			return "https://www.exponencialconcursos.com.br/produto/coaching-exponencial-concursos-inss/";
			break;

		case COACHING_AREA_AGENCIAS: 
			return "https://www.exponencialconcursos.com.br/produto/coaching-exponencial-concursos-agencias-reguladoras-e-similares-2/";
			break;
	}

}

/**
 * Retorna o link do produto WP referente ao pagamento trimestral de turma de coaching
 * 
 * @since Joule 1
 * 
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/1973
 * 
 * @param int $area_id Id da área cadastrada no Podio
 * 
 * @return string URL do produto WP
 */

function get_pagamento_coaching_link_trimestral($area_id)
{
	switch ($area_id) 
	{
		case COACHING_AREA_FISCAL: 
			return "https://www.exponencialconcursos.com.br/produto/coaching-exponencial-concursos-area-fiscal-2/";
			break;
		
		case COACHING_AREA_CONTROLE: 
			return "https://www.exponencialconcursos.com.br/produto/coaching-exponencial-concursos-area-de-controle-2/";
			break;

		case COACHING_AREA_POLICIAL: 
			return "https://www.exponencialconcursos.com.br/produto/coaching-exponencial-concursos-area-policial/";
			break;

		case COACHING_AREA_TRIBUNAIS: 
			return "https://www.exponencialconcursos.com.br/produto/coaching-tribunais-tre-trt-trf-e-tj-2/";
			break;

		case COACHING_AREA_LEGISLATIVA: 
			return "https://www.exponencialconcursos.com.br/produto/coaching-exponencial-concursos-area-legislativa/";
			break;

		case COACHING_AREA_JURIDICA: 
			return "https://www.exponencialconcursos.com.br/produto/coaching-exponencial-concursos-area-juridica/";
			break;

		case COACHING_AREA_BANCARIA: 
			return "https://www.exponencialconcursos.com.br/produto/coaching-exponencial-concursos-area-bancaria/  ";
			break;

		case COACHING_AREA_INSS: 
			return "https://www.exponencialconcursos.com.br/produto/coaching-exponencial-concursos-inss-2/";
			break;

		case COACHING_AREA_AGENCIAS: 
			return "https://www.exponencialconcursos.com.br/produto/coaching-exponencial-concursos-agencias-reguladoras-e-similares/";
			break;
	}

}

/**
 * Retorna o link do produto WP referente ao pagamento semestral de turma de coaching
 * 
 * @since Joule 1
 * 
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/1973
 * 
 * @param int $area_id Id da área cadastrada no Podio
 * 
 * @return string URL do produto WP
 */

function get_pagamento_coaching_link_semestral($area_id)
{
	switch ($area_id) 
	{
		case COACHING_AREA_FISCAL: 
			return "https://www.exponencialconcursos.com.br/produto/coaching-exponencial-concursos-area-fiscal-semestral/";
			break;
		
		case COACHING_AREA_CONTROLE: 
			return "https://www.exponencialconcursos.com.br/produto/coaching-exponencial-concursos-area-de-controle-semestral/";
			break;

		case COACHING_AREA_POLICIAL: 
			return "https://www.exponencialconcursos.com.br/produto/coaching-exponencial-concursos-area-policial-semestral/";
			break;

		case COACHING_AREA_TRIBUNAIS: 
			return "https://www.exponencialconcursos.com.br/produto/coaching-tribunais-tre-trt-trf-e-tj-semestral/";
			break;

		case COACHING_AREA_LEGISLATIVA: 
			return "https://www.exponencialconcursos.com.br/produto/coaching-exponencial-concursos-area-legislativa-semestral/";
			break;

		case COACHING_AREA_JURIDICA: 
			return "https://www.exponencialconcursos.com.br/produto/coaching-exponencial-concursos-area-juridica-semestral/";
			break;

		case COACHING_AREA_BANCARIA: 
			return "https://www.exponencialconcursos.com.br/produto/coaching-exponencial-concursos-area-bancaria-semestral/";
			break;

		case COACHING_AREA_INSS: 
			return "https://www.exponencialconcursos.com.br/produto/coaching-exponencial-concursos-inss-semestral/";
			break;

		case COACHING_AREA_AGENCIAS: 
			return "https://www.exponencialconcursos.com.br/produto/coaching-exponencial-concursos-agencias-reguladoras-e-similares-semestral/";
			break;
	}

}

function atualizar_item_podio($item_id, $data)
{
	try {
		Podio::setup(PODIO_CLIENT_COACHEE, PODIO_CLIENT_COACHEE_SECRET);
		Podio::authenticate_with_app(PODIO_APP_COACHEE, PODIO_APP_COACHEE_TOKEN);

		log_podio('debug', 'Podio: Atualizando item id.');
		PodioItem::update($item_id, array('fields' => $data));
	} catch (PodioError $pe) {
		log_podio('error', 'Podio: Erro ao atualizar item id. ' . $pe->__toString());
		redirecionar_erro_500();
	}

}

function get_data_proximo_pagamento($pagamento_atual)
{
	$data = explode("-", $pagamento_atual);
	$ano = $data[0];
	$mes = $data[1];
	$dia = $data[2];

	$mes++;

	if($mes > 12) {
		$mes = "01";
		$ano++;
	}

	if($mes < 10) {
		$mes = "0" . (int)$mes;
	}

	return "$ano-$mes-$dia";

}


function get_data_primeiro_pagamento()
{
	$pagamento_atual = date("Y-m-d", strtotime("+ 7 days"));
	
	$data = explode("-", $pagamento_atual);
	$ano = $data[0];
	$mes = $data[1];
	$dia = $data[2];

	if($dia > 28) {
		$pagamento_atual = date("Y-m-d", strtotime("first day of next month"));
	}

	return $pagamento_atual;
}