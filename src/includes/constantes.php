<?php
define('VERSAO', '12.0.4');

/**
 * Geral
 */
defined('SIM')                           OR define('SIM', 1);
defined('NAO')                           OR define('NAO', 2);
defined('YES')                           OR define('YES', 'yes');
defined('NO')                            OR define('NO', 'no');

/**
 * PATHS
 */
define('TEMA_AJAX_PATH', '/wp-content/themes/academy/ajax/');
define('CARREGANDO_IMG', '/wp-content/themes/academy/images/carregando.gif');


/**
 * URLs
 */
defined('MEUS_CADERNOS_URL')                            		  OR define('MEUS_CADERNOS_URL', '/questoes/cadernos/meus_cadernos');
defined('EDITAR_QUESTAO_URL')                            		  OR define('EDITAR_QUESTAO_URL', '/questoes/admin/editar_questao/');

/**
 * Posts
 */
define('STATUS_POST_PUBLICADO', 'publish');
define('STATUS_POST_REVISAO_PENDENTE', 'pending');
define('STATUS_POST_RASCUNHO', 'draft');

define('TIPO_POST_PAGINA', 'page');
define('TIPO_POST_CONCURSO', 'concurso');
define('TIPO_POST_DEPOIMENTO', 'depoimento');
define('TIPO_POST_POST', 'post');
define('TIPO_POST_PRODUTO', 'product');

/**
 * Páginas
 */
defined('META_H1')													OR define('META_H1', 'page_h1');

/**
 * Pedidos
 */
defined('AFILIADOS_PERCENTUAL_META')								OR define('AFILIADOS_PERCENTUAL_META', '_afiliados_percentual');

define('PEDIDO_ATIVO', 1);
define('PEDIDO_CANCELADO', 2);
define('PEDIDO_PENDENTE', 3);
define('PEDIDO_EXPIRADO', 4);

define('MEUS_PEDIDOS_ATIVOS', 0);
define('MEUS_PEDIDOS_EXPIRADOS', 1);
define('MEUS_PEDIDOS_CANCELADOS', 2);

define('MINHA_CONTA_PEDIDOS_POR_PAGINA', 30);

/**
 * Produtos
 */
defined('NUM_PRODUTOS_RELACIONADOS')								OR define('NUM_PRODUTOS_RELACIONADOS',	4);
defined('PRODUTO_EXPIRADO_POST_META')                               OR define('PRODUTO_EXPIRADO_POST_META', 'expiracao_curso');
defined('PRODUTO_ID_CHAVE')                                         OR define('PRODUTO_ID_CHAVE', 'product_id');

defined('PRODUTO_VENDAS_ATE_POST_META')                             OR define('PRODUTO_VENDAS_ATE_POST_META', 'vendas_ate');
defined('PRODUTO_DISPONIVEL_ATE_POST_META')                         OR define('PRODUTO_DISPONIVEL_ATE_POST_META', 'disponivel_ate');
defined('PRODUTO_TEMPO_DE_ACESSO')                                  OR define('PRODUTO_TEMPO_DE_ACESSO', 'tempo_de_acesso');
defined('PRODUTO_MOSTRAR_GARANTIA')                                 OR define('PRODUTO_MOSTRAR_GARANTIA', 'mostrar_garantia');
defined('PRODUTO_PREMIUM')                                          OR define('PRODUTO_PREMIUM', 'produto_premium');
defined('PRODUTO_PREMIUM_EXIBIR_PAGINA_CONCURSO')                   OR define('PRODUTO_PREMIUM_EXIBIR_PAGINA_CONCURSO', 'produto_premium_exibir_pagina_concurso');
defined('PRODUTO_PREMIUM_EXIBIR_PAGINA_CURSO')                      OR define('PRODUTO_PREMIUM_EXIBIR_PAGINA_CURSO', 'produto_premium_exibir_pagina_curso');
defined('PRODUTO_PREMIUM_CONCEDER_ACESSO_SQ')                       OR define('PRODUTO_PREMIUM_CONCEDER_ACESSO_SQ', 'produto_premium_conceder_acesso_sq');
defined('PRODUTO_PREMIUM_REGRAS')                                   OR define('PRODUTO_PREMIUM_REGRAS', 'produto_premium_regras');
defined('PRODUTO_PREMIUM_TIPO_SEMPRE')                              OR define('PRODUTO_PREMIUM_TIPO_SEMPRE', 1);
defined('PRODUTO_PREMIUM_TIPO_PRODUTO')                             OR define('PRODUTO_PREMIUM_TIPO_PRODUTO', 2);
defined('PRODUTO_PREMIUM_TIPO_CATEGORIA')                           OR define('PRODUTO_PREMIUM_TIPO_CATEGORIA', 3);
defined('PRODUTO_PREMIUM_OPERADOR_E')                               OR define('PRODUTO_PREMIUM_OPERADOR_E', 1);
defined('PRODUTO_PREMIUM_OPERADOR_NAO_E')                           OR define('PRODUTO_PREMIUM_OPERADOR_NAO_E', 2);
defined('PRODUTO_PREMIUM_ITENS_POR_PAGINA')                         OR define('PRODUTO_PREMIUM_ITENS_POR_PAGINA', 96);
defined('PRODUTO_CAPA_TEXTO_CONCURSO')                              OR define('PRODUTO_CAPA_TEXTO_CONCURSO', 'produto_capa_texto_concurso');
defined('PRODUTO_CAPA_TEXTO_DISCIPLINA')                            OR define('PRODUTO_CAPA_TEXTO_DISCIPLINA', 'produto_capa_texto_disciplina');

define('TERM_TAXONOMY_CURSO', 16);
define('TERM_TAXONOMY_PACOTE_CURSO', 87);
define('TERM_TAXONOMY_CADERNOS', 454);
define('TERM_TAXONOMY_MAPAS_MENTAIS', 104);
define('TERM_TAXONOMY_SIMULADOS', 125);
define('TERM_TAXONOMY_QUESTOES_COMENTADAS', 401);
define('TERM_TAXONOMY_AUDIOBOOKS', 177);
define('TERM_TAXONOMY_TIPO_AUDIOBOOKS', 420);
define('TERM_TAXONOMY_TIPO_CADERNOS', 418);
define('TERM_TAXONOMY_TIPO_PDF', 416);
define('TERM_TAXONOMY_TIPO_MAPAS_MENTAIS', 104);
define('TERM_TAXONOMY_TIPO_SIMULADOS', 419);
define('TERM_TAXONOMY_TIPO_TURMA_COACHING', 773);
define('TERM_TAXONOMY_TIPO_TURMA_COACHING_RF', 530);
define('TERM_TAXONOMY_TIPO_VIDEOS', 417);

define('TERM_GATEWAY_PER_PRODUCT', 3037);
define('TERM_AREA', 381);
define('TERM_PACOTE_CURSO', 90);
define('TERM_PACOTE_SQ', 2897);
define('TERM_TIPO_CURSO', 415);
define('TERM_CONCURSO', 33);
define('TERM_MATERIA', 34);
define('TERM_SIMULADO', 124);
define('TERM_AUDIOBOOK', 420);
define('TERM_MAPAS_MENTAIS', 103);
define('TERM_CADERNO_QUESTOES_ONLINE', 454);
define('TERM_RESUMO_QUESTOES_COMENTADAS', 400);
define('TERM_TECNICAS_ESTUDO', 531);
define('TERM_DISCURSIVA_REDACAO', 2643);
define('TERM_RECURSOS_POS_PROVA', 2644);
define('TERM_TIPO_PDF', 416);
define('TERM_TIPO_VIDEO', 417);
define('TERM_COACHING', 413);
define('TERM_TURMA_COACHING', 774);
define('TERM_ASSINATURA_SEMANAL', 455);
define('TERM_ASSINATURA_QUINZENAL', 523);
define('TERM_ASSINATURA_MENSAL', 211);
define('TERM_ASSINATURA_TRIMESTRAL', 212);
define('TERM_ASSINATURA_SEMESTRAL', 213);
define('TERM_ASSINATURA_ANUAL', 214);
define('TERM_TI_TECNOLOGIA_INFORMACAO', 2794);
define('TERM_CURSOS_REGULARES_AREA_FISCAL', 95);
define('TERM_BASICO_PARA_CONCURSOS', 3008);
define('TERM_SENADO_FEDERAL', 209);
define('TERM_CURSOS_REGULARES_AREA_CONTROLE', 3172);
define('TERM_CURSOS_REGULARES_POLICIA_MILITAR', 1523);
define('TERM_CURSOS_REGULARES_POLICIA_CIVIL', 1524);
define('TERM_CURSOS_REGULARES_BOMBEIROS', 1525);
define('TERM_CURSOS_REGULARES_AREA_POLICIAL', 2960);
define('TERM_CURSOS_REGULARES_GERAL', 3164);
define('TERM_CURSOS_REGULARES_AREA_AGEPEN', 1526);
define('TERM_CURSOS_REGULARES_AREA_DEPEN', 1527);
define('TERM_CURSOS_REGULARES_TRF', 188);
define('TERM_CURSOS_REGULARES_TJ', 187);
define('TERM_CURSOS_REGULARES_TRE', 186);
define('TERM_CURSOS_REGULARES_TRT', 185);
define('TERM_COM_CORRECAO', 3059);
defined('TERM_ASSINATURA_CURSO')                                OR define('TERM_ASSINATURA_CURSO', 3111);

defined('CHECKBOX_EDICAO_MANUAL_CATEGORIAS')                    OR define('CHECKBOX_EDICAO_MANUAL_CATEGORIAS', 'edicao_manual_categorias');
defined('PRODUTO_OCULTAR_TIPO_PRODUTO')                         OR define('PRODUTO_OCULTAR_TIPO_PRODUTO', 'produto_ocultar_tipo_produto');
defined('PRODUTO_OCULTAR_PROFESSORES')                          OR define('PRODUTO_OCULTAR_PROFESSORES', 'produto_ocultar_professores');
defined('PRODUTO_OCULTAR_MATERIAS')                             OR define('PRODUTO_OCULTAR_MATERIAS', 'produto_ocultar_materias');
defined('PRODUTO_OCULTAR_CONCURSOS')                            OR define('PRODUTO_OCULTAR_CONCURSOS', 'produto_ocultar_concursos');
defined('PRODUTO_OCULTAR_QUANTIDADE')                           OR define('PRODUTO_OCULTAR_QUANTIDADE', 'produto_ocultar_quantidade');
defined('PRODUTO_OCULTAR_CARGA_HORARIA')                        OR define('PRODUTO_OCULTAR_CARGA_HORARIA', 'produto_ocultar_carga_horaria');
defined('PRODUTO_OCULTAR_PARCELAMENTO')                         OR define('PRODUTO_OCULTAR_PARCELAMENTO', 'produto_ocultar_parcelamento');
defined('PRODUTO_INCLUIR_CAPA')                                 OR define('PRODUTO_INCLUIR_CAPA', 'produto_incluir_capa');
defined('PRODUTO_FUNDIR_ARQUIVOS')                              OR define('PRODUTO_FUNDIR_ARQUIVOS', 'produto_fundir_arquivos');

/**
 * Fóruns
 */
define('FORUM_EXPIRADO_POST_META', 'expiracao_forum');
define('FORUM_ID_CHAVE', 'forum_id');
define('FORUMS_PER_PAGE', 100);

/**
 * Relatórios
 */
define('PARTICIPACAO_PROFESSOR_DEFAULT', 45);

/**
 * E-mails
 */
defined('EMAIL_BCC_KEYDEA')                                     OR define('EMAIL_BCC_KEYDEA', 'clientes@keydea.com.br');
defined('EMAIL_BCC_DESENVOLVEDOR')                              OR define('EMAIL_BCC_DESENVOLVEDOR', '');
defined('EMAIL_WP_FROM')                                        OR define('EMAIL_WP_FROM', 'exponencial@exponencialconcursos.com.br');
defined('EMAIL_WP_CONTATO')                                     OR define('EMAIL_WP_CONTATO', 'contato@exponencialconcursos.com.br');
defined('EMAIL_COACHING')                                       OR define('EMAIL_COACHING', 'coaching@exponencialconcursos.com.br');

defined('EMAIL_FROM')                                           OR define('EMAIL_FROM', 'exponencial@exponencialconcursos.com.br');
defined('EMAIL_CONTATO')                                        OR define('EMAIL_CONTATO', 'contato@exponencialconcursos.com.br');
defined('EMAIL_QUESTOES')                                       OR define('EMAIL_QUESTOES', 'questoes@exponencialconcursos.com.br');
defined('EMAIL_LEONARDO')                                       OR define('EMAIL_LEONARDO', 'leonardo.coelho@exponencialconcursos.com.br');

/**
 * Cupons Bônus
 */
define('ITEM_BONUS_PRODUTO', 1);
define('ITEM_BONUS_CATEGORIA', 2);

/**
 * Relatório Cursos
 */
defined('ERRO_UPLOAD_NAO_REALIZADO')								OR define('ERRO_UPLOAD_NAO_REALIZADO', 1);
defined('ERRO_UPLOAD_ATRASADO')										OR define('ERRO_UPLOAD_ATRASADO', 2);

/**
 * Relatório de Vendas
 */
defined('ITEM_RAIZ_PACOTE')								            OR define('ITEM_RAIZ_PACOTE', 1);
defined('ITEM_NAO_RAIZ_PACOTE')										OR define('ITEM_NAO_RAIZ_PACOTE', 2);
defined('ITEM_NAO_PACOTE')										    OR define('ITEM_NAO_PACOTE', 0);

/**
 * Cache Transients
 */
define('TRANSIENT_USUARIOS_ATIVOS_PODIO', 'transient_usuarios_ativos_podio');
define('TRANSIENT_USUARIOS_ATIVOS_RF', 'transient_usuarios_ativos_rf');
define('TRANSIENT_USUARIOS_ATIVOS_COACHING', 'transient_usuarios_ativos_coaching');
define('TRANSIENT_CONCURSOS', 'transient_concursos');
define('TRANSIENT_NUM_QUESTOES', 'transient_num_questoes');
define('TRANSIENT_VIMEO_VIDEO', 'transient_vimeo_video');
define('TRANSIENT_EXPORTAR_FOLHA_DIRIGIDA', 'transient_exportar_folha_dirigida');
define('TRANSIENT_MAILCHIMP', 'transient_mail_chimp');
define('TRANSIENT_PAINEL_ALUNO_PRODUTOS', 'painel_aluno_produtos');
define('TRANSIENT_PROFESSORES', 'transient_professores');

defined('CACHE_1_HORA') 	                                        OR define('CACHE_1_HORA', 1 * HOUR_IN_SECONDS);
defined('CACHE_HOME_BLOGS') 	                                    OR define('CACHE_HOME_BLOGS', 'home.blogs');
defined('CACHE_PRODUTO_AULAS_DEMO_DATA')		    				OR define('CACHE_PRODUTO_AULAS_DEMO_DATA', 'cache_produto_aulas_demo_data_');
defined('CACHE_PRODUTO_AULAS_DEMO_HTML')		    				OR define('CACHE_PRODUTO_AULAS_DEMO_HTML', 'cache_produto_aulas_demo_html_');
defined('CACHE_VENDAS_AGRUPADAS_POR_PROFESSOR_DATA')		    	OR define('CACHE_VENDAS_AGRUPADAS_POR_PROFESSOR_DATA', 'cache_vendas_agrupadas_por_professor_data_');

defined('MCACHE_TEM_CATEGORIA')		    				            OR define('MCACHE_TEM_CATEGORIA', 'mcache_tem_categoria_');
defined('MCACHE_TEM_UMA_DAS_CATEGORIA')		    				    OR define('MCACHE_TEM_UMA_DAS_CATEGORIA', 'mcache_tem_uma_das_categorias_');
defined('MCACHE_ESTORNOS_POR_PEDIDO')		    				    OR define('MCACHE_ESTORNOS_POR_PEDIDO', 'mcache_estornos_por_pedido_');
defined('MCACHE_IS_COORDENADOR_SQ')		    				        OR define('MCACHE_IS_COORDENADOR_SQ', 'mcache_is_coordenador_sq_');
defined('MCACHE_IS_COORDENADOR_AREA')		    				    OR define('MCACHE_IS_COORDENADOR_AREA', 'mcache_is_coordenador_area_');
defined('MCACHE_IS_COORDENADOR_COACHING')		    				OR define('MCACHE_IS_COORDENADOR_COACHING', 'mcache_is_coordenador_coaching_');
defined('MCACHE_IS_PROFESSOR')		    				            OR define('MCACHE_IS_PROFESSOR', 'mcache_is_professor_');
defined('MCACHE_IS_ADMINISTRADOR')		    				        OR define('MCACHE_IS_ADMINISTRADOR', 'mcache_is_administrador_');

/**
 * Selenium
 */
define('SELENIUM_KEY', '1b9616ebf9ad2e0f3dbdecfc3e558c40');

/**
 * Mídias
 */
define('ARTIGOS', 'articles');
define('NOTICIAS', 'news');
define('VIDEOS', 'videos');

define('TIPO_ARTIGO', 1);
define('TIPO_NOTICIA', 2);
define('TIPO_VIDEO', 14);

/**
 * Categorias
 */
define('CATEGORIAS_AGRUPADAS', 'categorias-agrupadas');

define('COACHING', 'coaching');
define('COACHING_MENSAL', 'coaching-mensal');
define('COACHING_TRIMESTRAL', 'coaching-trimestral');
define('COACHING_SEMESTRAL', 'coaching-semestral');

define('CATEGORIA_ASSINATURA_CURSO', 'assinatura-curso');
define('CATEGORIA_COACHING', 'coaching');
define('CATEGORIA_ASSINATURA_SQ', 'assinatura-sq');
define('CATEGORIA_ASSINATURA_SQ_SEMANAL', 'assinatura-sq-semanal');
define('CATEGORIA_ASSINATURA_SQ_QUINZENAL', 'assinatura-sq-quinzenal');
define('CATEGORIA_ASSINATURA_SQ_MENSAL', 'assinatura-sq-mensal');
define('CATEGORIA_ASSINATURA_SQ_TRIMESTRAL', 'assinatura-sq-trimestral');
define('CATEGORIA_ASSINATURA_SQ_SEMESTRAL', 'assinatura-sq-semestral');
define('CATEGORIA_ASSINATURA_SQ_ANUAL', 'assinatura-sq-anual');
define('CATEGORIA_CADERNO_QUESTOES_ONLINE', 'caderno-questoes-online');
define('CATEGORIA_RESUMO_QUESTOES_COMENTADAS', 'resumo-questoes-comentadas');
define('CATEGORIA_SIMULADO', 'simulados');

define('CATEGORIA_CURSO_REGULAR_FISCAL_CONTROLE', 'cursos-regulares-area-fiscal-controle');
define('CATEGORIA_CURSO_REGULAR_FISCAL', 'cursos-regulares-area-fiscal');
define('CATEGORIA_CURSO_REGULAR_CONTROLE', 'cursos-regulares-area-controle');
define('CATEGORIA_CURSO_REGULAR_POLICIA_MILITAR', 'policia-militar-pm-cursos-regulares');
define('CATEGORIA_CURSO_REGULAR_POLICIA_CIVIL', 'policia-civil-pc-cursos-regulares');
define('CATEGORIA_CURSO_REGULAR_POLICIAL', 'cursos-regulares-area-policial');
define('CATEGORIA_CURSO_REGULAR_BOMBEIROS', 'bombeiros-cursos-regulares');
define('CATEGORIA_CURSO_REGULAR_GERAL', 'cursos-regulares-geral');
define('CATEGORIA_CURSO_REGULAR_AGEPEN', 'agepen-cursos-regulares');
define('CATEGORIA_CURSO_REGULAR_DEPEN', 'depen-cursos-regulares');
define('CATEGORIA_CURSO_REGULAR', 'cursos-regulares-area-fiscal-controle');

define('CATEGORIA_BASICO_PARA_CONCURSOS', 'basico-para-concursos');
define('CATEGORIA_MAPA_MENTAL', 'mapas-mentais');
define('CATEGORIA_TECNICAS_ESTUDO', 'treinamento-tecnicas-estudo-ptte');
define('CATEGORIA_DISCURSIVA', 'discursiva-redacao');
define('CATEGORIA_RECURSO', 'recursos-pos-prova');
define('CATEGORIA_SENADO_FEDERAL', 'senado-federal');

define('CATEGORIA_TRF_REGULAR', 'trf-regular');
define('CATEGORIA_TJ_REGULAR', 'tj-regular');
define('CATEGORIA_TRE_REGULAR', 'tre-regular');
define('CATEGORIA_TRT_REGULAR', 'trt-regular');
define('CATEGORIA_TI_TECNOLOGIA_INFORMACAO', 'ti-tecnologia-informacao');

// PHP 5.6 não permite define de array
// define('CATEGORIA_ID_COACHING', array(773, 774));
// define('CATEGORIA_ID_RETA_FINAL', array(530, 534));

/**
 * Assinaturas SQ
 */
define('ASSINATURA_SEMANAL', 0.25);
define('ASSINATURA_QUINZENAL', 0.5);
define('ASSINATURA_MENSAL', 1);
define('ASSINATURA_TRIMESTRAL', 3);
define('ASSINATURA_SEMESTRAL', 6);
define('ASSINATURA_ANUAL', 12);

define('LIMITE_RELATORIO_ASSINATURAS', 50);

/**
 * Redes Sociais
 */
define('LINKEDIN', 'linkedin');
define('TWITTER', 'twitter');
define('FACEBOOK', 'facebook');
define('GOOGLEPLUS', 'googleplus');
define('INSTAGRAM', 'instagram');
define('YOUTUBE', 'youtube');

/**
 * Tipo de Autores (Role WP)
 */
define('COLABORADOR_PROFESSOR', 1);
define('COLABORADOR_CONSULTOR', 2);
define('COLABORADOR_PROFESSOR_CONSULTOR', 3);

 /**
  *  Áreas coaching
  */
define('COACHING_AREA_AGENCIAS', '28');
define('COACHING_AREA_BANCARIA', '24');
define('COACHING_AREA_CONTROLE', '13');
define('COACHING_AREA_FISCAL', '20');
define('COACHING_AREA_JURIDICA', '6');
define('COACHING_AREA_LEGISLATIVA', '25');
define('COACHING_AREA_POLICIAL', '34');
define('COACHING_AREA_INSS', '4');
define('COACHING_AREA_TRIBUNAIS', '33');

/**
 * Concursos
 */
define('EDITAL_DIVULGADO', 1);
define('EDITAL_PROXIMO', 2);


/**
 * Depoimentos
 */
define('DEPOIMENTO_ENTREVISTA_VIDEO', 1);
define('DEPOIMENTO_ENTREVISTA_TEXTO', 2);
define('DEPOIMENTO_DEPOIMENTO_VIDEO', 3);
define('DEPOIMENTO_DEPOIMENTO_TEXTO', 4);

/**
 * Yoast
 */
define('META_YOAST_CONCURSO_TITULO', 'title-concurso');
define('META_YOAST_CONCURSO_DESCRICAO', 'metadesc-concurso');
define('META_YOAST_MATERIA_TITULO', 'title-product_cat');
define('META_YOAST_MATERIA_DESCRICAO', 'metadesc-tax-product_cat');
define('META_YOAST_PROFESSOR_TITULO', 'title-author-wpseo');
define('META_YOAST_PROFESSOR_DESCRICAO', 'metadesc-author-wpseo');

/**
 * Regex
 */
define('REGEX_TELEFONE', '#\([1-9]{2}\) [2-9]{1}[0-9]{3,4}-[0-9]{4}#');
define('ER_MOEDA', '/^([1-9][0-9]*|0)(,[0-9]{2})?$/');
define('ER_MOEDA2', '/^([0-9]{1,3}[\.][0-9]{3}|[1-9][0-9]*|0)(,[0-9]{2}|,[0-9])?$/');

/**
 * Relatório assinaturas
 */
define('FILTRO_ASSINATURAS_TODOS', 1);
define('FILTRO_ASSINATURAS_SOMENTE_ATIVOS', 2);
define('FILTRO_ASSINATURAS_A_EXPIRAR_EM_30_DIAS', 3);
define('FILTRO_ASSINATURAS_SOMENTE_EXPIRADOS', 4);

/**
 * Tipo de Simulados
 */
define('TIPO_SIMULADO_SOMENTE_CURSOS', 1);
define('TIPO_SIMULADO_SOMENTE_PACOTES', 2);

/**
 * Tipo de aulas
 */
define('TIPO_AULA_PDF', 1);
define('TIPO_AULA_VIDEO', 2);
define('TIPO_AULA_CADERNO', 3);
define('TIPO_AULA_RESUMO', 4);
define('TIPO_AULA_MAPA', 5);

/**
 * Fale Conosco
 */
define("ASSUNTO_PROBLEMA_ACESSO", 1);
define("ASSUNTO_NAO_CONSIGO_FINALIZAR_COMPRA", 2);
define("ASSUNTO_FAZER_ELOGIO", 3);
define("ASSUNTO_ESQUECI_SENHA", 4);
define("ASSUNTO_CANCELAMENTO", 5);
define("ASSUNTO_DESCONTOS", 6);
define("ASSUNTO_DUVIDA_ALUNO", 7);
define("ASSUNTO_DUVIDA_NAO_ALUNO", 8);

define("ASSUNTO_SQ_SS", 9);
define("ASSUNTO_COACHING", 10);
define("ASSUNTO_PDF_VIDEO", 11);

/**
 * Pesquisa
 */
define("PESQUISA_ORDEM_MENOR_PRECO", 1);
define("PESQUISA_ORDEM_MAIOR_PRECO", 2);
define("PESQUISA_ORDEM_A_Z", 3);
define("PESQUISA_ORDEM_Z_A", 4);
define("PESQUISA_MAIS_NOVOS", 5);
define("PESQUISA_PRODUTOS_POR_PAGINA", 24);
define("PESQUISA_NAO_ENCONTRADA_PRODUTOS_POR_PAGINA", 12);
define("PESQUISA_TIPO_PACOTE", 1);
define("PESQUISA_TIPO_NAO_PACOTE", 2);
define("PESQUISA_TIPO_AUDIOBOOKS", 3);
define("PESQUISA_TIPO_CADERNOS", 4);
define("PESQUISA_TIPO_PDF", 5);
define("PESQUISA_TIPO_SIMULADOS", 6);
define("PESQUISA_TIPO_TURMA_COACHING", 7);
define("PESQUISA_TIPO_VIDEOS", 8);
define("PESQUISA_TIPO_MAPAS_MENTAIS", 9);

define('CURSOS_ONLINE_POR_PAGINA', 32);

define('VMA_VENDAS', 1);
define('VMA_SQ', 2);

define('CATEGORIA_AREA', 381);
define('CATEGORIA_PACOTE', 90);
define('CATEGORIA_TIPO_CURSO', 415);
define('CATEGORIA_CONCURSO', 33);
define('CATEGORIA_MATERIA', 34);

define('LIMITE_POR_CONCURSO', 24);
define('LIMITE_POR_CONCURSO_ESPECIFICO_SESSAO', 100);
define('LIMITE_POR_MATERIA', 24);
define('LIMITE_POR_PROFESSOR', 24);
define('LIMITE_DEPOIMENTOS', 12);
define('LIMITE_DEPOIMENTOS_PRINCIPAL_VIDEO', 6);
define('LIMITE_DEPOIMENTOS_PRINCIPAL_TEXTO', 3);
define('LIMITE_SEO_HTTPS_EXTERNO', 30);

define('EXIBIR_EM_CURSOS_ONLINE', '_exibir_em_cursos_online');
define('NOTIFICACAO_CANCELAMENTO_PAGSEGURO', 'notificacao_cancelamento_pagseguro');

define('MAILCHIMP_API_KEY', '9a8bb3e1c8eba8bb248fffcc29a842d1-us1');
define('MC_LISTA_EXPONENCIAL_CONCURSOS', "7746555c27");
define('MC_INTERESSE_AREA', "cd3440d75a");

define('SEO_IMAGENS_OK', 1);
define('SEO_IMAGENS_TIPO_INVALIDO', 2);
define('SEO_IMAGENS_ARQUIVO_INEXISTENTE', 3);

define('META_PRODUTO_BUSCA', '_produto_busca');

define('GERAL_WHATSAPP', 'geral_whatsapp');

define('NOTA_FISCAL_ATIVAR_CADASTRO', 'nf_ativar_cadastro');
define('NOTA_FISCAL_ATIVAR_SKIP', 'nf_ativar_skip');

define('PESQUISA_PADRAO', 1);
define('PESQUISA_PRODUTOS_GRATIS', 2);

//Excluir questoes simulado
define('EXCLUIR_QUESTOES_INATIVAS', 1);
define('EXCLUIR_QUESTOES_SEM_COMENTARIO_PROFESSOR', 2);
define('EXCLUIR_QUESTOES_DE_OUTROS_SIMULADOS', 3);

/**
 * Meus cadernos
*/
define('FILTRO_LEGENDA_CRIADOS', 1);
define('FILTRO_LEGENDA_COMPARTILHADOS', 2);
define('FILTRO_LEGENDA_DESIGNADOS', 3);
define('FILTRO_LEGENDA_COMPRADOS', 4);

/**
 * Painel de Controle
 */
define('PAINEL_SQ_GERAL', 1);
define('PAINEL_SQ_FILTRO_QUESTOES', 2);
define('PAINEL_SQ_SIMULADOS', 3);
define('PAINEL_SQ_CADERNOS', 4);
define('PAINEL_SQ_MEU_DESEMPENHO', 5);
define('PAINEL_SQ_ADMINISTRACAO', 6);
define('PAINEL_INSCRICAO_COACHING', 7);
define('PAINEL_FORUM', 8);
define('PAINEL_SCRIPTS', 9);
define('PAINEL_LOGIN', 10);

define('CRONJOB_CRIADO', 1);
define('CRONJOB_EDITADO', 2);

/**
 * Tipo Estorno
 */
define('ESTORNO_POR_PRODUTO', 'P');
define('ESTORNO_POR_VALOR', 'V');

/**
 * Questões
 */
//Questão de professor aprovacao
define('QUESTAO_APROVADA', 1);
define('QUESTAO_EM_APROVACAO', 2);
define('QUESTAO_REPROVADA', 3);

//Procedência da questão
define('QUESTAO_INEDITA', 1);
define('QUESTAO_ADAPTADA', 2);
define('QUESTAO_CONCURSO', 3);
define('QUESTAO_FIXACAO', 4);

/**
 * Erros
 */
define('ERRO_PERCENTUAL_DIFERENTE_DE_100', 1);


/**
 * Relatórios
 */
define('RELATORIO_CURSOS_PRODUTOS_POR_PAGINA', 100);
define('RELATORIO_VENDAS_POR_PAGINA', 100);
define('RELATORIO_PRODUTOS_PREMIUM_POR_PAGINA', 100);
define('RELATORIO_PEDIDOS_PREMIUM_POR_PAGINA', 50);

// Status processamento produto premium
defined('PREMIUM_STATUS_PENDENTE')  	   or  define('PREMIUM_STATUS_PENDENTE', 1);
defined('PREMIUM_STATUS_CONCLUIDO')  	   or  define('PREMIUM_STATUS_CONCLUIDO', 2);

/**
 * Aulas
 */

defined('AULA_UPLOAD_TIPO_PDF') 	        or  define('AULA_UPLOAD_TIPO_PDF', 1);
defined('AULA_UPLOAD_TIPO_VIDEO') 	        or  define('AULA_UPLOAD_TIPO_VIDEO', 2);
defined('AULA_UPLOAD_TIPO_CADERNO') 	    or  define('AULA_UPLOAD_TIPO_CADERNO', 3);
defined('AULA_UPLOAD_TIPO_RESUMO')        or define('AULA_UPLOAD_TIPO_RESUMO', 4);
defined('AULA_UPLOAD_TIPO_CADERNO_SQ')        or define('AULA_UPLOAD_TIPO_CADERNO_SQ', 6);
defined('AULA_UPLOAD_TIPO_MAPA')        or define('AULA_UPLOAD_TIPO_MAPA', 5);

defined('AULA_TAMANHO_MAXIMO_MERGE_STR') 	or  define('AULA_TAMANHO_MAXIMO_MERGE_STR', 30);



/**
 * Configurações
 */

defined('CFG_SQ_QTDE_QUESTOES_DIARIAS') 	or define('CFG_SQ_QTDE_QUESTOES_DIARIAS','cfg_sq_qtde_questoes_diarias');
defined('CFG_FIXAR_PREMIUM_REQUERIDOS_KEY') or define('CFG_FIXAR_PREMIUM_REQUERIDOS_KEY','cfg_fixar_premium_requeridos_');
define('EXPIRACAO_COOKIE_AUTENTICACAO', HOUR_IN_SECONDS * 6);

/**
 * Professores
 */

defined('PROFESSOR_MAX_ARTIGOS') 	          or define('PROFESSOR_MAX_ARTIGOS',10);
defined('PROFESSOR_MAX_CURSOS') 	          or define('PROFESSOR_MAX_CURSOS',10);

/**
 * Downloadable Files
 */
defined('PDF_ZERO_ID') 	                      or define('PDF_ZERO_ID', 416756);
defined('PDF_ZERO_NAME') 	                  or define('PDF_ZERO_NAME', '_zero_');
defined('PDF_CURSO_CAPA') 	                  or define('PDF_CURSO_CAPA', "/wp-content/themes/academy/assets/pdf/capa-pdf.pdf");

/**
 * Downloads
 */
defined('DOWNLOAD_TEMPO_EXPIRACAO')           or define('DOWNLOAD_TEMPO_EXPIRACAO', "-1 day");
defined('DOWNLOAD_MAX_TENTATIVAS')            or define('DOWNLOAD_MAX_TENTATIVAS', 10);
defined('DOWNLOAD_NOTIFICACAO_PREFIXO')       or define('DOWNLOAD_NOTIFICACAO_PREFIXO', "expo-dn-");

defined('DOWNLOAD_STATUS_PENDENTE')           or define('DOWNLOAD_STATUS_PENDENTE', 1);
defined('DOWNLOAD_STATUS_CONCLUIDO')          or define('DOWNLOAD_STATUS_CONCLUIDO', 2);
defined('DOWNLOAD_STATUS_ERRO')               or define('DOWNLOAD_STATUS_ERRO', 3);

defined('DOWNLOAD_RELATORIO_VENDAS')          or define('DOWNLOAD_RELATORIO_VENDAS', 1);
defined('DOWNLOAD_BAIXAR_TODAS')              or define('DOWNLOAD_BAIXAR_TODAS', 2);

/**
 * Produto Recorrente
 */
defined('RECORRENTE_DIARIO') 	              or define('RECORRENTE_DIARIO', 'day');
defined('RECORRENTE_SEMANAL') 	              or define('RECORRENTE_SEMANAL', 'week');
defined('RECORRENTE_MENSAL') 	              or define('RECORRENTE_MENSAL', 'month');
defined('RECORRENTE_ANUAL') 	              or define('RECORRENTE_ANUAL', 'year');

/**
 * Coaching
 */



/**
 * Minha conta
 */
define('MICON_ABA_ATIVOS', 'ativos');
define('MICON_ABA_CANCELADOS', 'cancelados');
define('MICON_ABA_EXPIRADOS', 'expirados');


/**
 * Pagar.me
 */
defined('PAGARME_API_KEY_PRD') 	              or define('PAGARME_API_KEY_PRD', 'ak_live_hZZ4fjkJdSoPL7DtGjBIf8VWWB0Gtl');

/**
 * Mailchimp
 */
defined('MAILCHIMP_INTERESSE_POR_AREAS')      or define('MAILCHIMP_INTERESSE_POR_AREAS', 'Interesse por Áreas');

/**
 * Professores
 */
defined('PROFESSOR_EQUIPE_EXPONENCIAL')      or define('PROFESSOR_EQUIPE_EXPONENCIAL', 150138);
defined('PROFESSOR_EQUIPE_ASSINATURAS')      or define('PROFESSOR_EQUIPE_ASSINATURAS', 190784);

