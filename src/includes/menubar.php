<?php 

function get_menu_array($menu_name)
{
	$menu = array();

	$wp_menu = wp_get_nav_menu_items($menu_name);

	if($wp_menu) {
		foreach ($wp_menu as $chave => $item) {

			if($pai = $item->menu_item_parent) {

				$menu[$pai]['filhos'][$item->ID] = array(
					'nome' => $item->title,
					'url' => $item->url,
					'pai' => $pai
				);

			}
			else {
				$menu[$item->ID] = array(
					'nome' => $item->title,
					'url' => $item->url,
					'filhos' => array(),
				);
			}
		}
	}

	return $menu;
}

function get_menu($input, $hide = false, $id = null)
{
	$estilo = $hide ? "style='display:none'" : "";
	$id_ul = $id ? "id='pai-{$id}'" : "id='pai-menu'";
	$menu = "<ul $estilo $id_ul>";
	foreach($input as $chave => $item)  {
		$class_link = isset($item['filhos']) && $item['filhos'] ? "item-pai" : "";
		$url = $item['url'] ? $item['url'] : '#';

		$menu .= 
			"<li>
				<a data-id='{$chave}' class='{$class_link}' href='{$url}'>{$item['nome']}</a>";

		if(isset($item['filhos']) && $item['filhos']) {
			$menu .= get_menu($item['filhos'], true, $chave);
		}
		
		$menu .= "</li>";

	}

	$menu .= "</ul>";

	return $menu;
}

?>