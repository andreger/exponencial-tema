<?php

function woocommerce_wp_select_multiple($field) {
	global $thepostid, $post, $woocommerce;
	
	$thepostid = empty ( $thepostid ) ? $post->ID : $thepostid;
	$field ['class'] = isset ( $field ['class'] ) ? $field ['class'] : 'select short';
	$field ['wrapper_class'] = isset ( $field ['wrapper_class'] ) ? $field ['wrapper_class'] : '';
	$field ['name'] = isset ( $field ['name'] ) ? $field ['name'] : $field ['id'];
	$field ['value'] = isset ( $field ['value'] ) ? $field ['value'] : (get_post_meta ( $thepostid, $field ['id'], true ) ? get_post_meta ( $thepostid, $field ['id'], true ) : array ());
	
	echo '<p class="form-field ' . esc_attr ( $field ['id'] ) . '_field ' . esc_attr ( $field ['wrapper_class'] ) . '"><label for="' . esc_attr ( $field ['id'] ) . '">' . wp_kses_post ( $field ['label'] ) . '</label><select size="10" id="' . esc_attr ( $field ['id'] ) . '" name="' . esc_attr ( $field ['name'] ) . '" class="' . esc_attr ( $field ['class'] ) . '" multiple="multiple">';
	
	foreach ( $field ['options'] as $key => $value ) {
		
		echo '<option value="' . esc_attr ( $key ) . '" ' . (in_array ( $key, $field ['value'] ) ? 'selected="selected"' : '') . '>' . esc_html ( $value ) . '</option>';
	}
	
	echo '</select> ';
	
	if (! empty ( $field ['description'] )) {
		
		if (isset ( $field ['desc_tip'] ) && false !== $field ['desc_tip']) {
			echo '<img class="help_tip" data-tip="' . esc_attr ( $field ['description'] ) . '" src="' . esc_url ( WC ()->plugin_url () ) . '/assets/images/help.png" height="16" width="16" />';
		} else {
			echo '<span class="description">' . wp_kses_post ( $field ['description'] ) . '</span>';
		}
	}
	echo '</p>';
}

add_action ( 'woocommerce_product_options_general_product_data', 'woo_add_custom_general_fields' );
function woo_add_custom_general_fields() {
	global $woocommerce, $post, $wpdb, $wp_scripts;

	$produto = wc_get_product($post->ID);

	wp_enqueue_script('wc-enhanced-select');
	wp_enqueue_style('woocommerce_admin_styles', WC()->plugin_url() . '/assets/css/admin.css');
	
	if(tem_acesso([ADMINISTRADOR, COORDENADOR, REVISOR, ATENDENTE, APOIO]) && !tem_cronograma($post->ID) && !is_pacote($produto) && !hack_is_arquivo_zero($post->ID)) {
		echo "<style>.show_if_downloadable {display:block !important;}</style>";
	}
	else {
		echo "<style>.show_if_downloadable {display:none !important;}</style>";
	}

	if(tem_acesso([ADMINISTRADOR, COORDENADOR, REVISOR, ATENDENTE, APOIO])){
		echo "<style>#general_product_data > div.pricing, p._subscription_price_fields, #alg-per_product, #rocket_post_exclude {display: block !important;}</style>";
	}else{
		echo "<style>#general_product_data > div.pricing, p._subscription_price_fields, #alg-per_product, #rocket_post_exclude {display: none !important;}</style>";
	}
	
	echo "<script src='/wp-content/themes/academy/js/produtos.js?v=" . VERSAO . "'></script>";
	
	// Soma dos produtos
	if(is_pacote($produto)) {
	    $soma_pacote = 0;
	    foreach ( $produto->get_bundled_items() as $item ) {
	        $item_pacote = $item->get_product();
	        
	        $soma_pacote += $item_pacote->get_regular_price();
	    }
	    
	    woocommerce_wp_text_input ( array (
	        'id' => 'soma_produtos',
	        'label' => 'Soma dos produtos',
	        'value' => number_format($soma_pacote, 2, ',', ''),
	        'custom_attributes' => [
	            'readonly' => 'readonly'
	        ]
	    ) );
	}

	KLoader::model("CadernoModel");
	KLoader::helper("UrlHelper");

	KLoader::view("wp-admin/produto/editar_produto_forum", ['post' => $post]);
	KLoader::view("wp-admin/produto/editar_produto_vendas_disponibilidade");
	KLoader::view("wp-admin/produto/editar_produto_informacoes", ['post' => $post, 'produto' => $produto]);
	KLoader::view("wp-admin/produto/editar_produto_premium", ['post' => $post]);
	KLoader::view("wp-admin/produto/editar_produto_visibilidade_campos", ['post' => $post]);
	KLoader::view("wp-admin/produto/editar_produto_gestao_conteudo", ['post' => $post]);
	KLoader::view("wp-admin/produto/editar_produto_autores", ['post' => $post]);
	
	// Cronograma de Aulas
	$aulas_nome = get_post_meta ( $post->ID, 'aulas_nome', true );
	$aulas_data = get_post_meta ( $post->ID, 'aulas_data', true );
	$aulas_liberacao = get_post_meta ( $post->ID, 'aulas_liberacao', true );
	$aulas_descricao = get_post_meta ( $post->ID, 'aulas_descricao', true );
	$aulas_arquivo = get_post_meta ( $post->ID, 'aulas_arquivo', true );
	$aulas_vimeo = get_post_meta ( $post->ID, 'aulas_vimeo', true );
	$aulas_caderno = get_post_meta ( $post->ID, 'aulas_caderno', true );
    $aulas_caderno_sq = get_post_meta ( $post->ID, 'aulas_caderno_sq', true );
	$aulas_resumo = get_post_meta ( $post->ID, 'aulas_resumo', true );
	$aulas_mapa = get_post_meta ( $post->ID, 'aulas_mapa', true );
	$aulas_primeiro_upload = get_post_meta ( $post->ID, 'aulas_primeiro_upload', true );
	
	$aula_demo = get_post_meta ( $post->ID, 'aula_demo', true );




	/*echo "<pre>Aulas nome: ";
	print_r($aulas_nome);
	echo "</pre>";

	echo "<pre>Aulas data: ";
	print_r($aulas_data);
	echo "</pre>";

	echo "<pre>Aulas descrição: ";
	print_r($aulas_descricao);
	echo "</pre>";

	echo "<pre>Aulas arquivo: ";
	print_r($aulas_arquivo);
	echo "</pre>"; */

	echo "<div id='cronograma-produto'>";
	echo '<h3 style="margin: 10px" class="collapse-sessao in">Cronograma de Aulas<span class="expand">-</span></h3>';

	
	KLoader::view("wp-admin/produto/editar_produto_importar_cronograma", ['post' => $post]);
	// if(get_current_user_id() == 1) {
	// 	print_r($aulas_arquivo);
	// }
	echo '<div id="adm-cronograma-aula-sort" class="options_group collapse-sessao-item">';
	echo "<span class='collapse-todos exibir'>+ Exibir todas</span>&nbsp;&nbsp;";
	echo "<span class='collapse-todos recolher'>- Recolher todas</span>";
	echo "<input type='hidden' id='aulas_arquivo' name='aulas_arquivo'>";
	echo "<input type='hidden' id='aulas_vimeo' name='aulas_vimeo'>";
	echo "<input type='hidden' id='aulas_caderno' name='aulas_caderno'>";
    echo "<input type='hidden' id='aulas_caderno_sq' name='aulas_caderno_sq'>";
	echo "<input type='hidden' id='aulas_resumo' name='aulas_resumo'>";
	echo "<input type='hidden' id='aulas_mapa' name='aulas_mapa'>";

	for($i = 0; $i <= count ( $aulas_nome ); $i ++) { 		
		echo '<div class="adm-cronograma-aula options_group" style="margin: 40px 20px">';
		echo '<div style="margin-left:15px; font-weight: bold;" class="collapse-sessao in">== Aula ==<span class="expand">- Recolher arquivos</span></div>';
		$checked = $aula_demo === (string)$i ? 'checked' : '';
		echo "<p><input type='radio' name='aula-demo' value='{$i}' {$checked}> Aula Demonstrativa </p>";
		
		woocommerce_wp_text_input ( array (
				'id' => "aulas_nome_{$i}",
				'name' => 'aulas_nome[]',
				'label' => 'Nome da Aula',
				'value' => $aulas_nome [$i],
				'class' => 'aulas_nome'
		) );

		woocommerce_wp_text_input ( array (
			'id' => "aulas_descricao_{$i}",
			'name' => 'aulas_descricao[]',
			'label' => 'Descrição',
			'value' =>  $aulas_descricao [$i] ? $aulas_descricao [$i] : '' 
		) );
		
		echo '<div class="collapse-sessao-item">';

		woocommerce_wp_text_input ( array (
				'id' => "aulas_data_{$i}",
				'name' => 'aulas_data[]',
				'label' => 'Data da Aula',
				'value' => $aulas_data [$i] ? $aulas_data [$i] : '',
				'class' => 'aulas_data campo_data'
		) );
		woocommerce_wp_text_input ( array (
			'id' => "aulas_liberacao_{$i}",
			'name' => 'aulas_liberacao[]',
			'label' => 'Dias para Liberação Após Compra',
			'value' => $aulas_liberacao [$i] ? $aulas_liberacao [$i] : '',
			'class' => 'aulas_liberacao'
	) );

		woocommerce_wp_text_input ( array (
				'id' => "aulas_primeiro_upload_{$i}",
				'name' => 'aulas_primeiro_upload[]',
				'label' => 'Data do Upload',
				'value' =>  $aulas_primeiro_upload [$i] ? $aulas_primeiro_upload[$i] : '',
				'class' => 'aulas_arquivo'
		) );

		//Arquivos
		echo '<div class="form-field downloadable_files">
				<label>Arquivos</label>
				<table class="widefat">
					<thead>
						<tr>
							<th class="sort">&nbsp;</th>
							<th style="display:none">Nome <span class="woocommerce-help-tip"></span></th>
							<th colspan="2">File URL <span class="woocommerce-help-tip"></span></th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody class="ui-sortable">';


		if($aulas_arquivo[$i]) {
			if(!is_array($aulas_arquivo[$i])) {
				$aulas_arquivo[$i] = array($aulas_arquivo[$i]);
			}

			foreach($aulas_arquivo[$i] as $key => $item) {

				echo 
					"<tr>
						<td class='sort'></td>
						<td class='file_name' style='display:none'><input type='text' class='input_text' placeholder='File Name' name='_wc_file_names[]' value='{$key}'></td>
						<td class='file_url'><input type='text' class='nome_arquivo input_text' placeholder='http://' name='_wc_file_urls_expo[]' value='{$item}'></td>
						<td class='file_url_choose' width='1%'><a href='#' class='button upload_file_button' data-choose='Choose file' data-update='Insert file URL'>Escolher arquivo</a></td>
						<td width='1%'><a href='#' class='delete'>Delete</a></td>
					</tr>";
			}
		}

		echo 
			'</tbody>
			<tfoot>
				<tr>
					<th colspan="5">
							<a href="#" class="button insert" data-row="<tr>
								<td class=&quot;sort&quot;></td>
								<td class=&quot;file_name&quot; style=&quot;display:none&quot;><input type=&quot;text&quot; class=&quot;input_text&quot; placeholder=&quot;File Name&quot; name=&quot;_wc_file_names[]&quot; value=&quot;&quot; /></td>
								<td class=&quot;file_url&quot;><input type=&quot;text&quot; class=&quot;nome_arquivo input_text&quot; placeholder=&quot;http://&quot; name=&quot;_wc_file_urls_expo[]&quot; value=&quot;&quot; /></td>
								<td class=&quot;file_url_choose&quot; width=&quot;1%&quot;><a href=&quot;#&quot; class=&quot;button upload_file_button&quot; data-choose=&quot;Escolher arquivo&quot; data-update=&quot;Insert file URL&quot;>Escolher&nbsp;arquivo</a></td>
								<td width=&quot;1%&quot;><a href=&quot;#&quot; class=&quot;delete&quot;>Delete</a></td>
							</tr>">Adicionar Arquivo</a>
						</th>
					</tr>
				</tfoot>
			</table>
		</div>';
		
		// print_r($aulas_nome);
		// print_r($aulas_arquivo[$i]);
		
		//Vimeo
		echo '<div class="form-field downloadable_files">
				<label>Vimeo</label>
				<table class="widefat">
					<thead>
						<tr>
							<th class="sort">&nbsp;</th>
							<th style="display:none">Nome <span class="woocommerce-help-tip"></span></th>
							<th colspan="2">Vimeo URL <span class="woocommerce-help-tip"></span></th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody class="ui-sortable">';

		if($aulas_vimeo[$i]) {

			foreach($aulas_vimeo[$i] as $key => $item) {

				echo 
					"<tr>
						<td class='sort'></td>
						<td class='file_name' style='display:none'><input type='text' class='input_text' placeholder='File Name' name='vimeo_names[]' value='{$key}'></td>
						<td class='file_url'><input type='text' class='input_text' placeholder='http://' name='vimeo_urls[]' value='{$item}'></td>
						<td class='file_url_choose' width='1%'><a href='#TB_inline?width=600&height=550&inlineId=vimeo-modal' class='thickbox button' onclick='listar_canais_vimeo(this)' data-choose='Choose file' data-update='Insert file URL'>Escolher Vimeo</a></td>
						<td width='1%'><a href='#' class='delete'>Delete</a></td>
					</tr>";
			}
		}

		echo 
			'</tbody>
			<tfoot>
				<tr>
					<th colspan="5">
							<a href="#" class="button insert" data-row="<tr>
								<td class=&quot;sort&quot;></td>
								<td class=&quot;file_name&quot; style=&quot;display:none&quot;><input type=&quot;text&quot; class=&quot;input_text&quot; placeholder=&quot;File Name&quot; name=&quot;vimeo_names[]&quot; value=&quot;&quot; /></td>
								<td class=&quot;file_url&quot;><input type=&quot;text&quot; class=&quot;input_text&quot; placeholder=&quot;http://&quot; name=&quot;vimeo_urls[]&quot; value=&quot;&quot; /></td>
								<td class=&quot;file_url_choose&quot; width=&quot;1%&quot;><a href=&quot;#TB_inline?width=600&height=550&inlineId=vimeo-modal&quot; class=&quot;thickbox button upload_vimeo_button&quot; data-choose=&quot;Escolher Vimeo&quot; data-update=&quot;Insert file URL&quot; onclick=&quot;listar_canais_vimeo(this)&quot;>Escolher&nbsp;Vimeo</a></td>
								<td width=&quot;1%&quot;><a href=&quot;#&quot; class=&quot;delete&quot;>Delete</a></td>
							</tr>">Adicionar Vimeo</a>
						</th>
					</tr>
				</tfoot>
			</table>
		</div>';

		//Cadernos
		echo '<div class="form-field downloadable_files">
				<label>Cadernos</label>
				<table class="widefat">
					<thead>
						<tr>
							<th class="sort">&nbsp;</th>
							<th style="display:none">Index <span class="woocommerce-help-tip"></span></th>
							<th style="display:none">ID <span class="woocommerce-help-tip"></span></th>
							<th colspan="2">Caderno <span class="woocommerce-help-tip"></span></th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody class="ui-sortable">';

		if($aulas_caderno[$i]) {

			foreach($aulas_caderno[$i] as $key => $item) {
				
				//$caderno = CadernoModel::get_caderno($item);
				$caderno = UrlHelper::get_caderno_url($item);
				echo 
					"<tr>
						<td class='sort'></td>
						<td class='file_name' style='display:none'><input type='text' class='input_text' placeholder='Index' name='vimeo_names[]' value='{$key}'></td>
						<td class='caderno_id' style='display:none'><input type='text' class='input_text' placeholder='Id Caderno' name='caderno_ids[]' value='{$item}'></td>
						<td class='caderno_nome'><input type='text' class='input_text caderno_nome_text' name='caderno_nomes[]' value='{$caderno}'></td>
						<td class='file_url_choose' width='1%'><a href='#TB_inline?width=auto&height=550&inlineId=caderno-modal' class='thickbox button' onclick='listar_cadernos_professores(this)' data-choose='Selecionar caderno' data-update='Insert file URL'>Escolher caderno</a></td>
						<td width='1%'><a href='#' class='delete'>Delete</a></td>
					</tr>";
			}
		}

		echo 
			'</tbody>
			<tfoot>
				<tr>
					<th colspan="5">
							<a href="#" class="button insert" data-row="<tr>
								<td class=&quot;sort&quot;></td>
								<td class=&quot;&quot; style=&quot;display:none&quot;><input type=&quot;text&quot; class=&quot;input_text&quot; placeholder=&quot;Index do Caderno&quot; name=&quot;caderno_ids[]&quot; value=&quot;&quot; /></td>
								<td class=&quot;caderno_id&quot; style=&quot;display:none&quot;><input type=&quot;text&quot; class=&quot;input_text&quot; placeholder=&quot;ID do Caderno&quot; name=&quot;caderno_ids[]&quot; value=&quot;&quot; /></td>
								<td class=&quot;caderno_nome&quot;><input type=&quot;text&quot; class=&quot;input_text caderno_nome_text&quot; name=&quot;caderno_nomes[]&quot; value=&quot;&quot; /></td>
								<td class=&quot;file_url_choose&quot; width=&quot;1%&quot;><a href=&quot;#TB_inline?width=auto&height=550&inlineId=caderno-modal&quot; class=&quot;thickbox button upload_caderno_button&quot; data-choose=&quot;Escolher caderno&quot; data-update=&quot;Selecionar caderno&quot; onclick=&quot;listar_cadernos_professores(this)&quot;>Escolher&nbsp;caderno</a></td>
								<td width=&quot;1%&quot;><a href=&quot;#&quot; class=&quot;delete&quot;>Delete</a></td>
							</tr>">Adicionar Caderno</a>
						</th>
					</tr>
				</tfoot>
			</table>
		</div>';

        //Caderno SQ
        echo '<div class="form-field downloadable_files">
				<label>Caderno SQ</label>
				<table class="widefat">
					<thead>
						<tr>
							<th class="sort">&nbsp;</th>
							<th style="display:none">Nome <span class="woocommerce-help-tip"></span></th>
							<th colspan="2">Caderno SQ URL <span class="woocommerce-help-tip"></span></th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody class="ui-sortable">';


        if($aulas_caderno_sq[$i]) {
            if(!is_array($aulas_caderno_sq[$i])) {
                $aulas_caderno_sq[$i] = array($aulas_caderno_sq[$i]);
            }

            foreach($aulas_caderno_sq[$i] as $key => $item) {

                echo
                "<tr>
						<td class='sort'></td>
						<td class='file_name' style='display:none'><input type='text' class='input_text' placeholder='File Name' name='caderno_sq_names[]' value='{$key}'></td>
						<td class='file_url'><input type='text' class='nome_arquivo input_text' placeholder='http://' name='caderno_sq_urls[]' value='{$item}'></td>
						<td class='file_url_choose' width='1%'><a href='#' class='button upload_file_button' data-choose='Escolher caderno SQ' data-update='Insert file URL'>Escolher caderno SQ</a></td>
						<td width='1%'><a href='#' class='delete'>Delete</a></td>
					</tr>";
            }
        }

        echo
        '</tbody>
			<tfoot>
				<tr>
					<th colspan="5">
							<a href="#" class="button insert" data-row="<tr>
								<td class=&quot;sort&quot;></td>
								<td class=&quot;file_name&quot; style=&quot;display:none&quot;><input type=&quot;text&quot; class=&quot;input_text&quot; placeholder=&quot;File Name&quot; name=&quot;caderno_sq_names[]&quot; value=&quot;&quot; /></td>
								<td class=&quot;file_url&quot;><input type=&quot;text&quot; class=&quot;nome_arquivo input_text&quot; placeholder=&quot;http://&quot; name=&quot;caderno_sq_urls[]&quot; value=&quot;&quot; /></td>
								<td class=&quot;file_url_choose&quot; width=&quot;1%&quot;><a href=&quot;#&quot; class=&quot;button upload_file_button&quot; data-choose=&quot;Escolher caderno SQ&quot; data-update=&quot;Insert file URL&quot;>Escolher&nbsp;caderno&nbsp;SQ</a></td>
								<td width=&quot;1%&quot;><a href=&quot;#&quot; class=&quot;delete&quot;>Delete</a></td>
							</tr>">Adicionar Caderno SQ</a>
						</th>
					</tr>
				</tfoot>
			</table>
		</div>';


        //Resumo
		echo '<div class="form-field downloadable_files">
				<label>Resumo</label>
				<table class="widefat">
					<thead>
						<tr>
							<th class="sort">&nbsp;</th>
							<th style="display:none">Nome <span class="woocommerce-help-tip"></span></th>
							<th colspan="2">Resumo URL <span class="woocommerce-help-tip"></span></th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody class="ui-sortable">';


		if($aulas_resumo[$i]) {
			if(!is_array($aulas_resumo[$i])) {
				$aulas_resumo[$i] = array($aulas_resumo[$i]);
			}

			foreach($aulas_resumo[$i] as $key => $item) {

				echo 
					"<tr>
						<td class='sort'></td>
						<td class='file_name' style='display:none'><input type='text' class='input_text' placeholder='File Name' name='resumo_names[]' value='{$key}'></td>
						<td class='file_url'><input type='text' class='nome_arquivo input_text' placeholder='http://' name='resumo_urls[]' value='{$item}'></td>
						<td class='file_url_choose' width='1%'><a href='#' class='button upload_file_button' data-choose='Escolher resumo' data-update='Insert file URL'>Escolher resumo</a></td>
						<td width='1%'><a href='#' class='delete'>Delete</a></td>
					</tr>";
			}
		}

		echo 
			'</tbody>
			<tfoot>
				<tr>
					<th colspan="5">
							<a href="#" class="button insert" data-row="<tr>
								<td class=&quot;sort&quot;></td>
								<td class=&quot;file_name&quot; style=&quot;display:none&quot;><input type=&quot;text&quot; class=&quot;input_text&quot; placeholder=&quot;File Name&quot; name=&quot;resumo_names[]&quot; value=&quot;&quot; /></td>
								<td class=&quot;file_url&quot;><input type=&quot;text&quot; class=&quot;nome_arquivo input_text&quot; placeholder=&quot;http://&quot; name=&quot;resumo_urls[]&quot; value=&quot;&quot; /></td>
								<td class=&quot;file_url_choose&quot; width=&quot;1%&quot;><a href=&quot;#&quot; class=&quot;button upload_file_button&quot; data-choose=&quot;Escolher resumo&quot; data-update=&quot;Insert file URL&quot;>Escolher&nbsp;resumo</a></td>
								<td width=&quot;1%&quot;><a href=&quot;#&quot; class=&quot;delete&quot;>Delete</a></td>
							</tr>">Adicionar Resumo</a>
						</th>
					</tr>
				</tfoot>
			</table>
		</div>';

		//Mapa Mental
		echo '<div class="form-field downloadable_files">
				<label>Mapas mentais</label>
				<table class="widefat">
					<thead>
						<tr>
							<th class="sort">&nbsp;</th>
							<th style="display:none">Nome <span class="woocommerce-help-tip"></span></th>
							<th colspan="2">Mapa mental URL <span class="woocommerce-help-tip"></span></th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody class="ui-sortable">';


		if($aulas_mapa[$i]) {
			if(!is_array($aulas_mapa[$i])) {
				$aulas_mapa[$i] = array($aulas_mapa[$i]);
			}

			foreach($aulas_mapa[$i] as $key => $item) {

				echo 
					"<tr>
						<td class='sort'></td>
						<td class='file_name' style='display:none'><input type='text' class='input_text' placeholder='File Name' name='mapa_names[]' value='{$key}'></td>
						<td class='file_url'><input type='text' class='nome_arquivo input_text' placeholder='http://' name='mapa_urls[]' value='{$item}'></td>
						<td class='file_url_choose' width='1%'><a href='#' class='button upload_file_button' data-choose='Choose file' data-update='Insert file URL'>Escolher mapa mental</a></td>
						<td width='1%'><a href='#' class='delete'>Delete</a></td>
					</tr>";
			}
		}

		echo 
			'</tbody>
			<tfoot>
				<tr>
					<th colspan="5">
							<a href="#" class="button insert" data-row="<tr>
								<td class=&quot;sort&quot;></td>
								<td class=&quot;file_name&quot; style=&quot;display:none&quot;><input type=&quot;text&quot; class=&quot;input_text&quot; placeholder=&quot;File Name&quot; name=&quot;mapa_names[]&quot; value=&quot;&quot; /></td>
								<td class=&quot;file_url&quot;><input type=&quot;text&quot; class=&quot;nome_arquivo input_text&quot; placeholder=&quot;http://&quot; name=&quot;mapa_urls[]&quot; value=&quot;&quot; /></td>
								<td class=&quot;file_url_choose&quot; width=&quot;1%&quot;><a href=&quot;#&quot; class=&quot;button upload_file_button&quot; data-choose=&quot;Escolher&nbsp;mapa&nbsp;mental&quot; data-update=&quot;Insert file URL&quot;>Escolher&nbsp;mapa&nbsp;mental</a></td>
								<td width=&quot;1%&quot;><a href=&quot;#&quot; class=&quot;delete&quot;>Delete</a></td>
							</tr>">Adicionar Mapa</a>
						</th>
					</tr>
				</tfoot>
			</table>
		</div>';

		echo "<div style='margin:10px;text-align:right'><a class='button adicionar-aula' style='margin:10px;' href='#'>Adicionar Aula</a>";
		echo "<a href='#' style='margin:10px;' class='button remover-aula'>Remover Aula</a></div>";
		echo '</div>';
		echo '</div>';
	}
	
	add_thickbox();

	echo "</div></div>";

	echo "<div style='display:none' id='caderno-modal'>
		<p id='caderno-modal-content'></p>
		</div>";

	echo "<div style='display:none' id='vimeo-modal'>
		<p id='vimeo-modal-content'></p>
		</div>";

	echo "<style>
		.vimeo-video-box {
		    float: left;
		    min-height: 220px;
		    margin-right: 10px;
		    max-height: 220px;
		    overflow: hidden;
		    width: 200px;
		 }
		</style>";
}


// Salva aulas
add_action ( 'woocommerce_process_product_meta', 'woo_process_hidden_fields' );
function woo_process_hidden_fields($post_id) 
{
	// Início - Importação
	if(isset($_POST['importar-cronograma-id']) && tem_acesso([ADMINISTRADOR, COORDENADOR, ATENDENTE, APOIO])) {
	    
	    $import_id = trim($_POST['importar-cronograma-id']);
	    
	    if($import_id) {
	        
	        try {
	            $import_product = new WC_Product($import_id);
	            
            	$import_downloadable_files = get_post_meta($import_id, '_downloadable_files', true);
	
            	if($import_downloadable_files) {
            	    foreach ($import_downloadable_files as $import_downloadable_file) {
            	        $_POST['_wc_file_urls_expo'][] = $import_downloadable_file['file'];
            	    }
            	    
            	}
	        } catch (Exception $e) {
	            
	        }
	    }
	}
	
	$_POST['_wc_file_names'] = [];
	$_POST['_wc_file_urls'] = array_merge($_POST['_wc_file_urls_expo']?:[], $_POST['resumo_urls']?:[], $_POST['mapa_urls']?:[]);
	
	foreach ($_POST['_wc_file_urls'] as $item) {
		array_push($_POST['_wc_file_names'], "");	
	}
}

// Save Fields
add_action ( 'woocommerce_update_product', 'woo_add_custom_general_fields_save' );
function woo_add_custom_general_fields_save($post_id) 
{
	set_time_limit(360);

    /**
     * Após atualização para Woocommerce 5.3.0 e Wordpress 5.7.1 a gravação do preços e status do produto ficou intermitente
     * Solução dada: forçar a gravação dos campos
     */

    $price = $_POST['_regular_price'];
    if(trim($_POST['_sale_price'])) {
        $price = $_POST['_sale_price'];
    }

    update_post_meta( $post_id, '_regular_price', $_POST['_regular_price'] );
    update_post_meta( $post_id, '_sale_price', $_POST['_sale_price'] );
    update_post_meta( $post_id, '_price', $price );

    wp_update_post([
        'ID' => $post_id,
        'post_status' => $_POST['post_status']
    ]);

	adiciona_categoria_gateway_per_product($post_id);
	
	// diferenciação para não atualizar input de form quando for ação em massa
	if($_POST['action'] == 'editpost') {
		$produto = get_product ( $post_id );
		
		update_post_meta($post_id, '_sku', $post_id);
		
		if(isset($_POST['forum'])) {
			$forum_data = array (
				'post_content' => $_POST['post_content'],
				'post_status' => 'private',
				'post_title' => $_POST['post_title']
			);

			$forum_id = bbp_insert_forum ( $forum_data );
			add_post_meta ( $post_id, 'forum_id', $forum_id, true );
			add_post_meta ( $forum_id, 'product_id', $post_id, true );
		}

		if(tem_acesso([ADMINISTRADOR, COORDENADOR, REVISOR, ATENDENTE, APOIO])) {
			$liberar_gradualmente = $_POST['liberar_gradualmente'];
		
			if(!$liberar_gradualmente) {
				$liberar_gradualmente = "no";	
			}
			
			update_post_meta($post_id, 'liberar_gradualmente', $liberar_gradualmente);
		}
    
    	$mostrar_garantia = $_POST['mostrar_garantia'];
    
    	if(!$mostrar_garantia) {
    		$mostrar_garantia = "no";	
    	}
    
    	update_post_meta($post_id, 'mostrar_garantia', $mostrar_garantia);
    	
    	if(tem_acesso([ADMINISTRADOR, COORDENADOR, PROFESSOR, REVISOR, ATENDENTE, APOIO])) {		

    		
    		$qtde_aulas = trim ( $_POST ['qtde_aulas'] );
    		$qtde_esquemas = trim ( $_POST ['qtde_esquemas'] );
    		$qtde_questoes = trim ( $_POST ['qtde_questoes'] );
    		$carga_horaria = trim ( $_POST ['carga_horaria'] );
			$aula_demo = $_POST['aula-demo'];
    
    		log_wp('debug', 'Salvando dados gerais : ' . $post_id);
    
    		update_post_meta($post_id, 'qtde_aulas', $qtde_aulas );
    		update_post_meta($post_id, 'qtde_esquemas', $qtde_esquemas );
    		update_post_meta($post_id, 'qtde_questoes', $qtde_questoes );
    		update_post_meta($post_id, 'carga_horaria', $carga_horaria );
			
			
			//Professor não pode efetuar essas alterações: B2745
			if(tem_acesso([ADMINISTRADOR, COORDENADOR, REVISOR, ATENDENTE, APOIO])) {
				$botao_comprar = trim ( $_POST ['botao_comprar'] );
				$chamada = $_POST['chamada'];
				update_post_meta($post_id, 'botao_comprar', $botao_comprar );
				update_post_meta($post_id, 'chamada', $chamada );
			}
    
    		log_wp('debug', 'Salvando datas de venda : ' . $post_id);
    		if(tem_acesso([ADMINISTRADOR, COORDENADOR, REVISOR, ATENDENTE, APOIO])) {
    			$notificar_vendas = $_POST['notificar_vendas'];
    			
    			if(!$notificar_vendas) {
    				$notificar_vendas = "no";	
    			}
    
    			update_post_meta($post_id, 'notificar_vendas', $notificar_vendas);

    			// Vendas e Disponibilidade de Produto
    			$vendas_ate = trim($_POST[PRODUTO_VENDAS_ATE_POST_META]);
    			$disponivel_ate = trim($_POST[PRODUTO_DISPONIVEL_ATE_POST_META]);
    			$tempo_de_acesso = trim($_POST[PRODUTO_TEMPO_DE_ACESSO]);
    			
    			update_post_meta($post_id, PRODUTO_VENDAS_ATE_POST_META, $vendas_ate );
    			update_post_meta($post_id, PRODUTO_DISPONIVEL_ATE_POST_META, $disponivel_ate );
    			update_post_meta($post_id, PRODUTO_TEMPO_DE_ACESSO, $tempo_de_acesso );
    			
    			// Capa do produto
    			$capa_concurso = trim($_POST[PRODUTO_CAPA_TEXTO_CONCURSO]);
    			$capa_disciplina = trim($_POST[PRODUTO_CAPA_TEXTO_DISCIPLINA]);
    			update_post_meta($post_id, PRODUTO_CAPA_TEXTO_CONCURSO, $capa_concurso);
    			update_post_meta($post_id, PRODUTO_CAPA_TEXTO_DISCIPLINA, $capa_disciplina);
    			
    			// Produto Premium e Visibilidade de campos
    			$checkboxes = [PRODUTO_PREMIUM, PRODUTO_PREMIUM_EXIBIR_PAGINA_CONCURSO, PRODUTO_PREMIUM_EXIBIR_PAGINA_CURSO,
    			    PRODUTO_PREMIUM_CONCEDER_ACESSO_SQ, PRODUTO_OCULTAR_TIPO_PRODUTO, PRODUTO_OCULTAR_PROFESSORES, PRODUTO_OCULTAR_MATERIAS,
    			    PRODUTO_OCULTAR_CONCURSOS, PRODUTO_OCULTAR_QUANTIDADE, PRODUTO_OCULTAR_CARGA_HORARIA, PRODUTO_OCULTAR_PARCELAMENTO, PRODUTO_FUNDIR_ARQUIVOS, PRODUTO_INCLUIR_CAPA
    			];
    			
    			foreach($checkboxes as $meta_key) {
    			    $meta_value = $_POST[$meta_key] ?: NO;
    			    update_post_meta($post_id, $meta_key, $meta_value);
    			}
    			
    			$premium_regras = $_POST[PRODUTO_PREMIUM_REGRAS];
    			update_post_meta($post_id, PRODUTO_PREMIUM_REGRAS, $premium_regras);    						
    		}
    		
    		log_wp('debug', 'Salvando percentuais : ' . $post_id);
    		
    
    		if(is_pacote($produto)) {
    
    			$autores = array();
    			$percentuais = array();	
    
    			$categorias = array();
    			$categorias_produto = wp_get_object_terms($produto->get_id(), 'product_cat');
    			
    			/**
    			 * Sincronismo de categorias de pacotes deve se restringir a Exams e Disciplines
    			 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2404
    			 */
    			foreach ($categorias_produto as $categoria_produto) {
    			    if(!in_array($categoria_produto->parent, [CATEGORIA_MATERIA, CATEGORIA_CONCURSO])) {
    			        array_push($categorias, $categoria_produto->term_id);
    			    }
    			}
    			
    			$total = get_soma_produtos_pacote($produto);
    
    			if($itens = $produto->get_bundled_items()) {
    			    
    				foreach ($itens as $item) {
    					$bundle_product = $item->get_product();
    				
    					$autores_produto_ids = get_post_meta($bundle_product->get_id(), '_authors', true);
    					$autores_percentuais = get_post_meta($bundle_product->get_id(), '_percentuais', true);
    					
    					$autores_produto_ids = array_values(array_diff($autores_produto_ids, array(-1)));
    					
    					for($i = 0; $i < count($autores_produto_ids); $i++) {
    						$autor_produto_id = $autores_produto_ids[$i];
    						$autor_percentual = str_replace(',','.',$autores_percentuais[$i]);
    						
    						if(empty($autor_percentual)) {
    							$autor_percentual = 100 / count($autores_produto_ids);
    						}
    						
    						$percentual_item = round($bundle_product->get_price() / $total * $autor_percentual, 2);
    						
    						$key = array_search($autor_produto_id, $autores);
    						
    						if($key !== false) {
    							$percentuais[$key] += $percentual_item;
    						} 
    						else {
    							array_push($autores, $autor_produto_id);
    							array_push($percentuais, $percentual_item);
    						}
    					}
    					
    					$categorias_produto = wp_get_object_terms($bundle_product->get_id(), 'product_cat');
    					
    					foreach ($categorias_produto as $categoria_produto) {
    						array_push($categorias, $categoria_produto->term_id);
    					}
    				}
    				
    				$percentuais = corrige_percentuais_pacote($percentuais);
    			}
    
    			$edicao_manual_categorias = $_POST[CHECKBOX_EDICAO_MANUAL_CATEGORIAS];
    
    			if(!$edicao_manual_categorias) {
    				$edicao_manual_categorias = NULL;
    				salvar_categorias_em_produto($post_id, $categorias);
    			}
    
    			update_post_meta($post_id, CHECKBOX_EDICAO_MANUAL_CATEGORIAS, $edicao_manual_categorias);
    			salvar_autores_em_produto($post_id, $autores, $percentuais);
    		}
    		else {
    			
    			// executa gravação apenas dos perfis que conseguem enxergar os campos. Caso contrário valor do campo virá vazio no $_POST e apagará informação atual
    
    			if(tem_acesso([ADMINISTRADOR, COORDENADOR, REVISOR, ATENDENTE, APOIO])) {
    				// remove dados inválidos do array de autores/percentuais
    				$woocommerce_authors = array_diff($_POST['authors'], array ("", -1));
    				$woocommerce_percentuais = array_diff($_POST['percentuais'], array ("", -1));
    				
    				// acerta dados para casos onde não há autores
    				if(!$woocommerce_authors) {
    					$woocommerce_authors = [];
    					$woocommerce_percentuais = [];
    				}
    
    				// salva autores/percentuais
    				update_post_meta($post_id, '_authors', $woocommerce_authors);
    				update_post_meta($post_id, '_percentuais', $woocommerce_percentuais);
    			
    				if(($_POST['tax_input']['product_cat']) && (!is_pacote($produto))) {
    					wp_set_post_terms( $post_id, $_POST['tax_input']['product_cat'], 'product_cat');
    				}
    			}
    		}
    		
    		// Início - Importação
    		if(isset($_POST['importar-cronograma-id']) && tem_acesso([ADMINISTRADOR, COORDENADOR, ATENDENTE, APOIO])) {
    		    
    		    $import_id = trim($_POST['importar-cronograma-id']);
    		    
    		    if($import_id) {
    		        
    		        try {
    		            $import_product = new WC_Product($import_id);
    		            
    		            $import_aulas_nome = get_post_meta($import_id, 'aulas_nome', true);
    		            $import_aulas_liberacao = get_post_meta($import_id, 'aulas_liberacao', true);
    		            $import_aulas_descricao = get_post_meta($import_id, 'aulas_descricao', true);
    		            $import_aulas_data = get_post_meta($import_id, 'aulas_data', true);
    		            $import_aulas_arquivo = get_post_meta($import_id, 'aulas_arquivo', true);
    		            $import_aulas_vimeo = get_post_meta($import_id, 'aulas_vimeo', true);
						$import_aulas_caderno = get_post_meta($import_id, 'aulas_caderno', true);
                        $import_aulas_caderno_sq = get_post_meta($import_id, 'aulas_caderno_sq', true);
						$import_aulas_resumo = get_post_meta($import_id, 'aulas_resumo', true);
						$import_aulas_mapa = get_post_meta($import_id, 'aulas_mapa', true);
    		            $import_aulas_primeiro_upload = get_post_meta($import_id, 'aulas_primeiro_upload', true);
    		            $import_aulas_primeiro_upload_video = get_post_meta($import_id, 'aulas_primeiro_upload_video', true);
    		            $import_aulas_primeiro_upload_caderno = get_post_meta($import_id, 'aulas_primeiro_upload_caderno', true);
    		            $import_aula_demo = get_post_meta($import_id, 'aula_demo', true);
    		            $import_downloadable_files = get_post_meta($import_id, '_downloadable_files', true);

    		            $_POST["aulas_nome"] = $import_aulas_nome;
    		            $_POST["aulas_data"] = $import_aulas_data;
    		            $_POST["aulas_liberacao"] = $import_aulas_liberacao;
    		            $_POST["aulas_descricao"] = $import_aulas_descricao;
    		            $_POST["aulas_arquivo"] = json_encode($import_aulas_arquivo, JSON_UNESCAPED_UNICODE+JSON_UNESCAPED_SLASHES);
    		            $_POST["aulas_vimeo"] = json_encode($import_aulas_vimeo, JSON_UNESCAPED_UNICODE+JSON_UNESCAPED_SLASHES);
						$_POST["aulas_caderno"] = json_encode($import_aulas_caderno, JSON_UNESCAPED_UNICODE+JSON_UNESCAPED_SLASHES);
						$_POST["aulas_caderno_sq"] = json_encode($import_aulas_caderno_sq, JSON_UNESCAPED_UNICODE+JSON_UNESCAPED_SLASHES);
						$_POST["aulas_resumo"] = json_encode($import_aulas_resumo, JSON_UNESCAPED_UNICODE+JSON_UNESCAPED_SLASHES);
						$_POST["aulas_mapa"] = json_encode($import_aulas_mapa, JSON_UNESCAPED_UNICODE+JSON_UNESCAPED_SLASHES);
    		            $_POST["aulas_primeiro_upload"] = $import_aulas_primeiro_upload;
    		            $_POST["aulas_primeiro_upload_video"] = $import_aulas_primeiro_upload_video;
    		            $_POST["aulas_primeiro_upload_caderno"] = $import_aulas_primeiro_upload_caderno;
    		            
    		            if($import_downloadable_files) {
    		                foreach ($import_downloadable_files as $import_downloadable_file) {
    		                    $_POST['_wc_file_urls_expo'][] = $import_downloadable_file['file'];
    		                }
    		                
    		            }
//     		            $_POST["_wc_file_urls_expo"] = $import_downloadable_files;
    		            
    		            $aula_demo = $import_aula_demo;
    		            
    		        }
    		        catch (Exception $e) {
    		            
    		            
    		        }
    		    }
    		}
    		// Fim - importação
//     		echo "<pre>";
//     		print_r($_POST);
//     		exit;
			//$aulas_nome = array_map('trim', array_diff($_POST['aulas_nome'], array("")));
			$aulas_nome = array_map('trim', $_POST['aulas_nome']);
			$aulas_nome = remover_ultimos_valores_vazio($aulas_nome);//É o campo usado como referência para existencia de aula, por isso corto os outros arrays mais abaixo

    		if (! empty ( $aulas_nome )) {
    
    			$_POST['_wc_file_names'] = [];
    			$_POST['_wc_file_urls'] = $_POST['_wc_file_urls_expo'];
    			
    			foreach ($_POST['_wc_file_urls'] as $item) {
    				array_push($_POST['_wc_file_names'], "");	
				}

                $_POST['caderno_sq_names'] = [];
                //$_POST['caderno_sq_urls'] = $_POST['_wc_file_urls_expo'];


                foreach ($_POST['caderno_sq_urls'] as $item) {
                    array_push($_POST['caderno_sq_names'], "");
                }
				
				$_POST['resumo_names'] = [];
    			//$_POST['resumo_urls'] = $_POST['_wc_file_urls_expo'];
    			
    			foreach ($_POST['resumo_urls'] as $item) {
    				array_push($_POST['resumo_names'], "");	
				}
				
				$_POST['mapa_names'] = [];
    			//$_POST['mapa_urls'] = $_POST['_wc_file_urls_expo'];
    			
    			foreach ($_POST['mapa_urls'] as $item) {
    				array_push($_POST['mapa_names'], "");	
    			}
				
				//$aulas_data = array_map('trim', array_diff($_POST['aulas_data'], array("")));
				$aulas_data = array_map('trim', $_POST['aulas_data']);
				$aulas_data = array_slice($aulas_data, 0, count($aulas_nome));

				//$aulas_liberacao = array_map('trim', array_diff($_POST['aulas_liberacao'], array("")));
				$aulas_liberacao = array_map('trim', $_POST['aulas_liberacao']);
				$aulas_liberacao = array_slice($aulas_liberacao, 0, count($aulas_nome));
				
				//$aulas_descricao = array_map('trim', array_diff($_POST['aulas_descricao'], array("")));
				$aulas_descricao = array_map('trim', $_POST['aulas_descricao']);
				$aulas_descricao = array_slice($aulas_descricao, 0, count($aulas_nome));
    
    			$aulas_arquivo = array();
				$aulas_vimeo = array();
				$aulas_caderno = array();
				$aulas_caderno_sq = array();
				$aulas_resumo = array();
				$aulas_mapa = array();
    
    			if($aulas_arquivo_post = json_decode(stripslashes($_POST['aulas_arquivo']))) {
    
    				// print_r($aulas_arquivo_post);exit;
    
    				foreach ($aulas_arquivo_post as $aula_item) {
    					$aula_item_array = array();
    
    					if($aula_item) {
    						foreach ($aula_item as $key => $arquivo_item) {
    							if($arquivo_item) {
    								$aula_item_array[$key] = $arquivo_item;
    							}
    						}
    					}
    
    					// if($aula_item_array) {
    						array_push($aulas_arquivo, $aula_item_array);
    					// }
    				}	
    			}
    
    			if($aulas_vimeo_post = json_decode(stripslashes($_POST['aulas_vimeo']))) {
    
    				foreach ($aulas_vimeo_post as $aula_item) {
    					$aula_item_array = array();
    
    					if($aula_item) {
    						foreach ($aula_item as $key => $arquivo_item) {
    							if($arquivo_item) {
    								$aula_item_array[$key] = $arquivo_item;
    							}
    						}
    					}
    
    				// if($aula_item_array) {
    					array_push($aulas_vimeo, $aula_item_array);
    				// }
        			}	
				}
				
				if($aulas_caderno_post = json_decode(stripslashes($_POST['aulas_caderno']))) {
    
    				foreach ($aulas_caderno_post as $aula_item) {
    					$aula_item_array = array();
    
    					if($aula_item) {
    						foreach ($aula_item as $key => $arquivo_item) {
    							if($arquivo_item) {
    								$aula_item_array[$key] = $arquivo_item;
    							}
    						}
    					}
    
    				// if($aula_item_array) {
    					array_push($aulas_caderno, $aula_item_array);
    				// }
        			}	
				}

                if($aulas_caderno_sq_post = json_decode(stripslashes($_POST['aulas_caderno_sq']))) {

                    // print_r($aulas_caderno_sq_post);exit;

                    foreach ($aulas_caderno_sq_post as $aula_item) {
                        $aula_item_array = array();

                        if($aula_item) {
                            foreach ($aula_item as $key => $arquivo_item) {
                                if($arquivo_item) {
                                    $aula_item_array[$key] = $arquivo_item;
                                }
                            }
                        }

                        // if($aula_item_array) {
                        array_push($aulas_caderno_sq, $aula_item_array);
                        // }
                    }
                }
//                echo "<pre>";
//                print_r($_POST);
				
				if($aulas_resumo_post = json_decode(stripslashes($_POST['aulas_resumo']))) {
    
    				// print_r($aulas_resumo_post);exit;
    
    				foreach ($aulas_resumo_post as $aula_item) {
    					$aula_item_array = array();
    
    					if($aula_item) {
    						foreach ($aula_item as $key => $arquivo_item) {
    							if($arquivo_item) {
    								$aula_item_array[$key] = $arquivo_item;
    							}
    						}
    					}
    
    					// if($aula_item_array) {
    						array_push($aulas_resumo, $aula_item_array);
    					// }
    				}	
				}
				
				if($aulas_mapa_post = json_decode(stripslashes($_POST['aulas_mapa']))) {
    
    				// print_r($aulas_mapa_post);exit;
    
    				foreach ($aulas_mapa_post as $aula_item) {
    					$aula_item_array = array();
    
    					if($aula_item) {
    						foreach ($aula_item as $key => $arquivo_item) {
    							if($arquivo_item) {
    								$aula_item_array[$key] = $arquivo_item;
    							}
    						}
    					}
    
    					// if($aula_item_array) {
    						array_push($aulas_mapa, $aula_item_array);
    					// }
    				}	
    			}


			
				//Removido: EC110, Podio: 2925
        		/*$aulas_nome_antes = get_post_meta($post_id, 'aulas_nome', true);
        		$aulas_descricao_antes = get_post_meta($post_id, 'aulas_descricao', true);
        		$aulas_data_antes = get_post_meta($post_id, 'aulas_data', true);
        		$aulas_liberacao_antes = get_post_meta($post_id, 'aulas_liberacao', true);
        		$aulas_arquivo_antes = get_post_meta($post_id, 'aulas_arquivo', true);*/
        		
        		log_wp('debug', 'Salvando dados de aulas : ' . $post_id);
        		update_post_meta($post_id, 'aula_demo', $aula_demo );
        		update_post_meta($post_id, 'aulas_nome', $aulas_nome );
        		update_post_meta($post_id, 'aulas_data', $aulas_data );
        		update_post_meta($post_id, 'aulas_liberacao', $aulas_liberacao );
        		update_post_meta($post_id, 'aulas_descricao', $aulas_descricao );
        		update_post_meta($post_id, 'aulas_arquivo', $aulas_arquivo );
				update_post_meta($post_id, 'aulas_vimeo', $aulas_vimeo );
				update_post_meta($post_id, 'aulas_caderno', $aulas_caderno );
				update_post_meta($post_id, 'aulas_caderno_sq', $aulas_caderno_sq );
				update_post_meta($post_id, 'aulas_resumo', $aulas_resumo );
				update_post_meta($post_id, 'aulas_mapa', $aulas_mapa );
				
				//Removido: EC110, Podio: 2925
        		/*log_wp('debug', 'Notificando mudança de nome aula : ' . $post_id);
        		notificar_mudanca_aula_nome($post_id, $aulas_nome, $aulas_nome_antes);
        
        		log_wp('debug', 'Notificando mudança de data aula : ' . $post_id);
        		notificar_mudanca_aula_data($post_id, $aulas_data, $aulas_data_antes);
        
        		log_wp('debug', 'Notificando mudança de data liberação : ' . $post_id);
        		notificar_mudanca_aula_data($post_id, $aulas_liberacao, $aulas_liberacao_antes);
        
        		log_wp('debug', 'Notificando mudança de descrição aula : ' . $post_id);
        		notificar_mudanca_aula_descricao($post_id, $aulas_descricao, $aulas_descricao_antes);
        
        		log_wp('debug', 'Notificando mudança de arquivo aula : ' . $post_id);
        		notificar_mudanca_aula_arquivo($post_id, $aulas_arquivo, $aulas_arquivo_antes);*/
        		
        		gravar_primeiro_upload($post_id, $aulas_arquivo, AULA_UPLOAD_TIPO_PDF);
        		gravar_primeiro_upload($post_id, $aulas_vimeo, AULA_UPLOAD_TIPO_VIDEO);
				gravar_primeiro_upload($post_id, $aulas_caderno, AULA_UPLOAD_TIPO_CADERNO);
				gravar_primeiro_upload($post_id, $aulas_caderno_sq, AULA_UPLOAD_TIPO_CADERNO_SQ);
				gravar_primeiro_upload($post_id, $aulas_resumo, AULA_UPLOAD_TIPO_RESUMO);
				gravar_primeiro_upload($post_id, $aulas_mapa, AULA_UPLOAD_TIPO_MAPA);
        		
        	} else {
        		delete_post_meta($post_id, 'aulas_nome');
        		delete_post_meta($post_id, 'aulas_data');
        		delete_post_meta($post_id, 'aulas_liberacao');
        		delete_post_meta($post_id, 'aulas_descricao');
        		delete_post_meta($post_id, 'aulas_arquivo');
				delete_post_meta($post_id, 'aulas_vimeo');
				delete_post_meta($post_id, 'aulas_caderno');
				delete_post_meta($post_id, 'aulas_caderno_sq');
				delete_post_meta($post_id, 'aulas_resumo');
				delete_post_meta($post_id, 'aulas_mapa');
        	}
    
    		log_wp('debug', 'Atualizando status exibição listagem : ' . $post_id);
    		definir_produto_listagem_status($post_id);
    
    		log_wp('debug', 'Fim salvar produto : ' . $post_id);
    	}

	}
	
// 	echo "<pre>";
// 	    		print_r($_POST);
// 	    		exit;

	KLoader::model("ProdutoModel");
	ProdutoModel::atualizar_metadados($post_id);
	hack_adicionar_arquivo_zero($post_id);
}

function adiciona_categoria_gateway_per_product($post_id)
{
    $categorias_produto = wp_get_object_terms($post_id, 'product_cat');
    $categorias = [TERM_GATEWAY_PER_PRODUCT];
    
    foreach ($categorias_produto as $categoria_produto) {    
        if(!in_array($categoria_produto->term_id, $categorias)) {
            array_push($categorias, $categoria_produto->term_id);
        }
    }

    wp_set_post_terms($post_id, $categorias, 'product_cat');
}

function  hack_adicionar_arquivo_zero($post_id)
{
    $product = wc_get_product($post_id);
    
    // recupera os arquivos
    $downloads = (array) $product->get_downloads();
    
    // executa somente se não houver arquivos no produto
    if( sizeof($downloads) == 0 ){
        
        // Prepare download data
        $file_name = PDF_ZERO_NAME;
        $file_url   = wp_get_attachment_url(PDF_ZERO_ID);
        $file_md5   = md5($file_url);
        
        // Inserting new file in the exiting array of downloadable files
        $files[0][$file_md5] = array(
            'name'   =>  $file_name,
            'file'   =>  $file_url
        );
        
        // Updating database with the new array
        update_post_meta($post_id, '_downloadable_files', $files[0]);
    }
}

function  hack_is_arquivo_zero($post_id)
{
    $downloads = get_post_meta($post_id, "_downloadable_files", true);
    
    if(count($downloads) == 1) {
       $download = reset($downloads);
       
       if($download['name'] == PDF_ZERO_NAME) {
            return true;        
       }
       
    }
    
    return false;
}

function gravar_primeiro_upload($post_id, $aulas_arquivos, $tipo)
{
    $campo = get_nome_campo_primeiro_upload($tipo);
    
	$aulas_primeiro_upload = get_post_meta($post_id, $campo, true);
		
	if(!isset($aulas_primeiro_upload) || !$aulas_primeiro_upload) {
		$aulas_primeiro_upload = array();
	}

	foreach ($aulas_arquivos as $i => $valor) {
		
		if(!$valor) {
			$aulas_primeiro_upload[$i] = '';
		}
		else {
			if(!$aulas_primeiro_upload[$i]) {
				$aulas_primeiro_upload[$i] = date('d/m/Y');
			}
		}		
	}

	$aulas_primeiro_upload = array_slice($aulas_primeiro_upload, 0, count($aulas_arquivos));

	update_post_meta($post_id, $campo, $aulas_primeiro_upload );
}

function get_nome_campo_primeiro_upload($tipo) 
{
    switch($tipo) {
        case AULA_UPLOAD_TIPO_PDF: $nome_campo = 'aulas_primeiro_upload'; break;
        case AULA_UPLOAD_TIPO_VIDEO: $nome_campo = 'aulas_primeiro_upload_video'; break;
		case AULA_UPLOAD_TIPO_CADERNO: $nome_campo = 'aulas_primeiro_upload_caderno'; break;
		case AULA_UPLOAD_TIPO_CADERNO_SQ: $nome_campo = 'aulas_primeiro_upload_caderno_sq'; break;
		case AULA_UPLOAD_TIPO_RESUMO: $nome_campo = 'aulas_primeiro_upload_resumo'; break;
		case AULA_UPLOAD_TIPO_MAPA: $nome_campo = 'aulas_primeiro_upload_mapa'; break;
    }
    
    return $nome_campo;
}

function get_nome_campo_aulas_arquivos($tipo)
{
    switch($tipo) {
        case AULA_UPLOAD_TIPO_PDF: $nome_campo = 'aulas_arquivo'; break;
        case AULA_UPLOAD_TIPO_VIDEO: $nome_campo = 'aulas_vimeo'; break;
		case AULA_UPLOAD_TIPO_CADERNO: $nome_campo = 'aulas_caderno'; break;
		case AULA_UPLOAD_TIPO_CADERNO_SQ: $nome_campo = 'aulas_caderno_sq'; break;
		case AULA_UPLOAD_TIPO_RESUMO: $nome_campo = 'aulas_resumo'; break;
		case AULA_UPLOAD_TIPO_MAPA: $nome_campo = 'aulas_mapa'; break;
    }
    
    return $nome_campo;
}

function notificar_mudanca_aula_nome($post_id, $novos_valores, $antigos_valores)
{
	$produto = get_produto_by_id($post_id);
	$editor = get_usuario_array(get_current_user_id());
	
	for($i = 0; $i < count($antigos_valores); $i++) {
		set_time_limit(60);
		
		if(trim($antigos_valores[$i]) == "") {
			continue;
		}
		
		log_wp('debug', 'Valor antigo: ' . $antigos_valores[$i]);
		log_wp('debug', 'Valor novo: ' . $novos_valores[$i]);
		
		if(trim($antigos_valores[$i]) != trim($novos_valores[$i])) {
			
			$mensagem = get_template_email('notificacao-alteracao-aula-nome.php', array(
				'curso_nome' => $produto->post->post_title,
				'editor' => $editor['nome_completo'],
				'antigo_valor' => $antigos_valores[$i],
				'novo_valor' => $novos_valores[$i],
				'link' => get_editar_produto_url($produto->id)
			));
			
			//enviar_email('contato@exponencialconcursos.com.br', "Alteração de Nome de Aula - {$produto->post->post_title}", $mensagem, null, null);
		}
	}
}

function notificar_mudanca_aula_data($post_id, $novos_valores, $antigos_valores)
{
	$aulas_nome = get_post_meta($post_id, 'aulas_nome', true);
	
	$produto = get_produto_by_id($post_id);
	$editor = get_usuario_array(get_current_user_id());

	for($i = 0; $i < count($antigos_valores); $i++) {
		set_time_limit(60);
		
		if(trim($antigos_valores[$i]) == "") {
			continue;
		}

		if(trim($antigos_valores[$i]) != trim($novos_valores[$i])) {
				
			$mensagem = get_template_email('notificacao-alteracao-aula-data.php', array(
					'curso_nome' => $produto->post->post_title,
					'editor' => $editor['nome_completo'],
					'antigo_valor' => $antigos_valores[$i],
					'novo_valor' => $novos_valores[$i],
					'aula' => $aulas_nome[$i],
					'link' => get_editar_produto_url($produto->id)
			));
			try {
				enviar_email('contato@exponencialconcursos.com.br', "Alteração de Data de Aula - {$produto->post->post_title} - {$aulas_nome[$i]}", $mensagem, null, null);
			}catch(Exception $e) {
				continue;	
			}
		}
	}
}

function notificar_mudanca_aula_arquivo($post_id, $novos_valores, $antigos_valores)
{
	$aulas_nome = get_post_meta($post_id, 'aulas_nome', true);
	$aulas_liberacao = get_post_meta($post_id, 'aulas_liberacao', true);

	$produto = get_produto_by_id($post_id);
	$editor = get_usuario_array(get_current_user_id());

	for($i = 0; $i < count($antigos_valores); $i++) {
		set_time_limit(60);

		if(trim($antigos_valores[$i]) == "") {
			continue;
		}

		if(trim($antigos_valores[$i]) != trim($novos_valores[$i])) {

			/******************************************************************
			 * Se aula estiver disponivel para aluno avisa todos que compraram
			 * ****************************************************************/
			$liberar_gradualmente = get_post_meta($produto->id, 'liberar_gradualmente', true) == 'yes';
			if($liberar_gradualmente){
				$usuarios = get_usuarios_que_compraram_por_dias_apos_compra($produto->id, $aulas_liberacao[$i]);
			}elseif(is_aula_disponivel_para_aluno($produto->id, $i)) {
				$usuarios = get_usuarios_que_compraram($produto->id);
			}
			
			if(!empty($alunos)){

				foreach ($usuarios as $usuario) {
					salvar_atualizacao_aula($produto->id, $aulas_nome[$i], $usuario->ID);
					
					$u = get_usuario_array($usuario->ID);

					$mensagem = get_template_email('notificacao-alteracao-aula-arquivo.php', array(
							'nome' => $u['nome_completo'],
							'curso_nome' => $produto->post->post_title,
							'editor' => $editor['nome_completo'],
	// 							'antigo_valor' => $antigos_valores[$i],
	// 							'novo_valor' => $novos_valores[$i],
							'aula' => $aulas_nome[$i],
	// 							'link' => get_editar_produto_url($produto->id)
					));
					
					try {
	// 						enviar_email($u['email'], "Alteração de Arquivo de Aula - {$produto->post->post_title} - {$aulas_nome[$i]}", $mensagem, EMAIL_WP_FROM, null);
					}
					catch(Exception $e) {
						continue;
					}
				}
				try {
					//enviar_email(EMAIL_WP_CONTATO, "Alteração de Arquivo de Aula - {$produto->post->post_title} - {$aulas_nome[$i]}", $mensagem, EMAIL_WP_FROM, null);
				}
				catch(Exception $e) {
					continue;
				}

			}
			/******************************************************************
			 * Senao avisa somente contato@exponencialconcursos.com.br
			 * ****************************************************************/
			else { 
				$mensagem = get_template_email('notificacao-alteracao-aula-descricao-contato.php', array(
						'curso_nome' => $produto->post->post_title,
						'editor' => $editor['nome_completo'],
						'antigo_valor' => $antigos_valores[$i],
						'novo_valor' => $novos_valores[$i],
						'aula' => $aulas_nome[$i],
						'link' => get_editar_produto_url($produto->id)
				));
				
				try {
					//enviar_email(EMAIL_WP_CONTATO, "Alteração de Arquivo de Aula - {$produto->post->post_title} - {$aulas_nome[$i]}", $mensagem, null, null);
				} catch(Exception $e) {
					continue;
				}
			}
		}
	}
}

function notificar_mudanca_aula_descricao($post_id, $novos_valores, $antigos_valores)
{
	$aulas_nome = get_post_meta($post_id, 'aulas_nome', true);

	$produto = get_produto_by_id($post_id);
	$editor = get_usuario_array(get_current_user_id());

	for($i = 0; $i < count($antigos_valores); $i++) {
		set_time_limit(60);
		
		if(trim($antigos_valores[$i]) == "") {
			continue;
		}

		if(trim($antigos_valores[$i]) != trim($novos_valores[$i])) {

			/******************************************************************
			 * Se aula estiver disponivel para aluno avisa todos que compraram
			 * ****************************************************************/
			if(is_aula_disponivel_para_aluno($produto->id, $i)) {
				$usuarios = get_usuarios_que_compraram($produto->id);

				foreach ($usuarios as $usuario) {
					$u = get_usuario_array($usuario->ID);
						
					$mensagem = get_template_email('notificacao-alteracao-aula-descricao.php', array(
							'nome' => $u['nome_completo'],
							'curso_nome' => $produto->post->post_title,
							'editor' => $editor['nome_completo'],
							// 							'antigo_valor' => $antigos_valores[$i],
					// 							'novo_valor' => $novos_valores[$i],
							'aula' => $aulas_nome[$i],
							// 							'link' => get_editar_produto_url($produto->id)
					));
						
					try {
// 						enviar_email($u['email'], "Alteração de Descrição de Aula - {$produto->post->post_title} - {$aulas_nome[$i]}", $mensagem, EMAIL_WP_FROM, null);
					}
					catch(Exception $e) {
						continue;
					}
				}
				try {
					//enviar_email(EMAIL_WP_CONTATO, "Alteração de Descrição de Aula - {$produto->post->post_title} - {$aulas_nome[$i]}", $mensagem, EMAIL_WP_FROM, null);
				}
				catch (Exception $e) {
					continue;
				}
			}
			/******************************************************************
			 * Senao avisa somente contato@exponencialconcursos.com.br
			 * ****************************************************************/
			else {
				$mensagem = get_template_email('notificacao-alteracao-aula-descricao-contato.php', array(
						'aula' => $file['name'],
						'curso_nome' => $produto->post->post_title,
						'editor' => $editor['nome_completo'],
						'antigo_valor' => $antigos_valores[$i],
						'novo_valor' => $novos_valores[$i],
						'aula' => $aulas_nome[$i],
						'link' => get_editar_produto_url($produto->id)
				));

				try {
					//enviar_email(EMAIL_WP_CONTATO, "Alteração de Descrição de Aula - {$produto->post->post_title} - {$aulas_nome[$i]}", $mensagem, null, null);
				}
				catch(Exception $e) {
					continue;
				}
			}
		}
	}
}

function corrige_percentuais_pacote($percentuais)
{
    $soma = 0;
    
    foreach($percentuais as $item) {
        $soma += $item;
    }
    
    $diferenca = 100 - $soma;
    
    if($diferenca != 0) {
        
        // index do último item
        $index = count($percentuais) - 1;
        
        while($index >= 0) {
            $novo_valor = $percentuais[$index] + $diferenca;
            
            if($novo_valor >= 0) {
                $percentuais[$index] = $novo_valor;
                return $percentuais;
            }
            else {
                $index--;
            } 
        }
    }
    
    return $percentuais;
}