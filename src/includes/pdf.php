<?php
//============================================================+
// File name   : example_010.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 010 for TCPDF class
//               Text on multiple columns
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Text on multiple columns
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-content/tcpdf/tcpdf.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-content/fpdi/fpdi.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-content/fpdi/FPDI_Protection.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-content/fpdi/rfpdi.php';


/**
 * Extend TCPDF to work with multiple columns
 */
class MC_TCPDF extends TCPDF {

    /**
     * Print chapter
     * @param $num (int) chapter number
     * @param $title (string) chapter title
     * @param $file (string) name of the file containing the chapter body
     * @param $mode (boolean) if true the chapter body is in HTML, otherwise in simple text.
     * @public
     */
    public function PrintChapter($num, $title= false, $file, $mode=false) {
        // add a new page
        $this->AddPage();
        // disable existing columns
        $this->resetColumns();
        // print chapter title
        if($title)
        	$this->ChapterTitle($num, $title);
        // set columns
        $this->setEqualColumns(2, 85);
        // print chapter body
        $this->ChapterBody($file, $mode);
    }
    
    public function PrintBeggining($num, $title= false, $file, $mode=false) {
    	// add a new page
    	$this->AddPage();
    	// disable existing columns
    	$this->resetColumns();
    	// print chapter title
    	if($title)
    		$this->ChapterTitle($num, $title);
    	// set columns
    	$this->setEqualColumns(1, 50);
    	// print chapter body
    	$this->ChapterBody($file, $mode);
    }

    /**
     * Set chapter title
     * @param $num (int) chapter number
     * @param $title (string) chapter title
     * @public
     */
    public function ChapterTitle($num, $title) {
        $this->SetFont('helvetica', '', 14);
        $this->SetFillColor(200, 220, 255);
        $this->Cell(180, 6, 'Chapter '.$num.' : '.$title, 0, 1, '', 1);
        $this->Ln(4);
    }

    /**
     * Print chapter body
     * @param $file (string) name of the file containing the chapter body
     * @param $mode (boolean) if true the chapter body is in HTML, otherwise in simple text.
     * @public
     */
    public function ChapterBody($file, $mode=false) {
        $this->selectColumn();
        // get esternal file content
        if(file_exists($file))
        	$content = file_get_contents($file, false);
        else 
        	$content = $file;
        // set font
        $this->SetFont('helvetica', '', 10);
        $this->SetTextColor(50, 50, 50);
        // print content
        if ($mode) {
            // ------ HTML MODE ------
            $this->writeHTML($content, true, false, true, false, '');
        } else {
            // ------ TEXT MODE ------
            $this->Write(0, $content, '', 0, 'J', true, 0, false, true, 0);
        }
        $this->Ln();
    }
    
    function assinar_pdf($file_path, $output = 'D')
    {
    	/* HACK EXPONENCIAL BEGIN */
    	$url_array = parse_url($file_path);
    
    	$dir =  'wp-content/temp/';
    	$nome = $dir . md5(time()) . '.pdf';
    	$name = $_SERVER['DOCUMENT_ROOT'] . '/' . $nome;
    	$temp_folder = $_SERVER['DOCUMENT_ROOT'] . '/' . $dir;
    
    	if (!file_exists($temp_folder)) {
    		mkdir($temp_folder, 0777, true);
    	}
    	copy($_SERVER['DOCUMENT_ROOT'] . $url_array['path'], $name);
    
    	$url_array = explode('/', $url_array['path']);
    	$filename = remove_accents(array_pop($url_array));
    
    	header( "X-Robots-Tag: noindex, nofollow", true );
    	header( "Content-Type:  application/force-download");
    	header( "Content-Description: File Transfer" );
    	header( "Content-Transfer-Encoding: binary" );
    	header( "Content-Disposition: attachment; filename=" . $filename);
    
    	try {
    		$file = new FPDI();
    		$file->setSourceFile($name);
    
    		$pdf = new RFPDI();
    		$pdf->SetProtection(array('print','annot-forms'));
    		$pdf->SetFont('Helvetica');
    		$pdf->SetFontSize('9');
    		// 		$pdf->SetTextColor(47, 63, 96); //dark blue
    		$pdf->SetTextColor(160, 160, 160); //gray
    
    		$current_user = wp_get_current_user();
    		$text1 = 'Cópia registrada para '. $current_user->display_name . ' (CPF: ' . get_user_meta($current_user->ID, 'oneTarek_CPF', true) .')';
    		$text1 =  stripslashes($text1);
    		$text1 = iconv('UTF-8', 'windows-1252', $text1);
    		$stringWidth1 = $pdf->GetStringWidth($text1);
    
    		$text2 = 'Direitos autorais reservados (Lei 9610/98). Proibida a reprodução, venda ou compartilhamento deste arquivo. Uso individual.';
    		$text2 =  stripslashes($text2);
    		$text2 = iconv('UTF-8', 'windows-1252', $text2);
    		$stringWidth2 = $pdf->GetStringWidth($text2);
    
    		$pagesCount = $pdf->setSourceFile($name);
    
    		for ($i = 1; $i <= $pagesCount; $i++) {
    			$tplIdxFile = $file->importPage($i);
    			$size = $file->getTemplateSize($tplIdxFile);
    
    			$pdf->AddPage($size['h'] > $size['w'] ? 'P' : 'L',  array($size['w'], $size['h']));
    
    			$tplIdx = $pdf->importPage($i);
    			$pdf->useTemplate($tplIdx);
    
    			$position1 = $size['h'] - (($size['h'] - $stringWidth1) / 2);
    			$position2 = $size['h'] - (($size['h'] - $stringWidth2) / 2);
    
    			$pdf->TextWithRotation(10, $position1, $text1, 90);
    			$pdf->TextWithRotation(5, $position2, $text2, 90);
    		}
    		$pdf->Output($filename,$output);
    		// print_r($pdf);
    		exit;
    	} catch (Exception $e) {
    		print_r($e);
    	}
    	// 		echo $file_path; exit;
    	/* HACK EXPONENCIAL END */
    }
} // end of extended class
//============================================================+
// END OF FILE
//============================================================+