<?php

/*************************************************************************
 * Hooks para personalizacao da página de login do WP
 *************************************************************************/

function login_personalizado() {
	echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('stylesheet_directory') . '/css/login.css" />';
}
add_action('login_head', 'login_personalizado');

function my_login_logo_url() {
	return get_bloginfo( 'url' );
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

function my_login_logo_url_title() {
	return 'Exponencial Concursos';
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );

function my_login_head() {
	remove_action('login_head', 'wp_shake_js', 12);
}
add_action('login_head', 'my_login_head');

/*************************************************************************
 * Hook para criacao de pasta temporaria 
 *************************************************************************/

function criar_pasta_temporaria()
{
	$temp_folder = $_SERVER['DOCUMENT_ROOT'] . '/wp-content/temp/';
	
	if (!file_exists($temp_folder)) {
		mkdir($temp_folder, 0777, true);
	}
}

add_action( 'wp_login_failed', 'expo_login_fail' );  // hook failed login
function expo_login_fail( $username ) {
	$redirect_to = "";
	if(isset($_GET['redirect_to']) && $_GET['redirect_to']){//Mantém o redirect_to quando o login falhar
		$redirect_to = "&ref=".$_GET['redirect_to'];
	}
	wp_redirect(home_url() . '/cadastro-login/?login=failed'.$redirect_to );
}

/*************************************************************************
 * Hook para permitir outros tipos de arquivo
 *************************************************************************/
add_filter('upload_mimes', 'my_myme_types', 1, 1);
function my_myme_types($mime_types){
	$mime_types['xmind'] = 'application/vnd.xmind.workbook';

	return $mime_types;
}


/*************************************************************************
 * Hook para remover cupons ao logar
 *************************************************************************/
function limpar_cupons() {
    global $woocommerce;
    $woocommerce->cart->remove_coupons();
}
add_action('wp_login', 'limpar_cupons');
//add_action('wp_login', 'limpar_carrinho');