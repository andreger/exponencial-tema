<?php
add_filter( 'jetpack_implode_frontend_css', '__return_false' );


function get_tema_image_tag($imagem, $width = '', $height = '', $alt = '', $class = '')
{
	if($width)
	{
		$imagem_array = explode(".", $imagem);
		$nome_imagem = $imagem_array[0] . '-' . $width . '.' . $imagem_array[1];
		$caminho_nova_imagem = $_SERVER['DOCUMENT_ROOT'] . '/wp-content/themes/academy/images/' . $nome_imagem; 

		if(!file_exists($caminho_nova_imagem)) {
			$caminho_antiga_imagem = $_SERVER['DOCUMENT_ROOT'] . '/wp-content/themes/academy/images/' . $imagem;
			
			$verot = new VerotUpload($caminho_antiga_imagem);
			$verot->image_resize          = true;
			$verot->image_x               = $width;
			$verot->image_y               = $height;
			$verot->file_new_name_body = $imagem_array[0] . '-' . $width;
			$verot->file_overwrite = true;
			$verot->process($_SERVER['DOCUMENT_ROOT'] . '/wp-content/themes/academy/images/');
		}
		$imagem = $nome_imagem;
	}
	
	$image_src = get_tema_image_url($imagem);

	//Evitar atributo vazios
	if($width){
		$att_width = "width='{$width}'";
	}else{
		$att_width = "";
	}

	if($height){
		$att_height = "height='{$height}'";
	}else{
		$att_height = "";
	}

	if($alt){
		$att_alt = "alt='{$alt}'";
	}else{
		$att_alt = "";
	}

	if($class){
		$att_class = "class='{$class}'";
	}else{
		$att_class = "";
	}

	return "<img src='{$image_src}' {$att_width} {$att_height} {$att_alt} {$att_class}>";
	
}

function get_resized_image_tag($imagem_url, $width = '', $height = '', $alt = '')
{
	if($width)
	{
		$arquivo_antigo = converter_para_arquivo($imagem_url);
		
		$parts = explode(".", $arquivo_antigo);
		$last = array_pop($parts);
		$imagem_array = array(implode('.', $parts), $last);		
		
		$caminho_nova_imagem = $imagem_array[0] . '-' . $width . '.' . $imagem_array[1];

		$parts = explode('/', $arquivo_antigo);
		$last = array_pop($parts);
			
		$last = explode('.', $last);
		$extensao = array_pop($last);
		$nome_arquivo = array(implode('.', $last), $extensao);	
		
		$path = implode('/', $parts);
		
		if(!file_exists($caminho_nova_imagem)) {
			$verot = new VerotUpload($arquivo_antigo);
			$verot->image_resize          = true;
			$verot->image_x               = $width;
			$verot->image_y               = $height;
			$verot->file_new_name_body = $nome_arquivo[0] . '-' . $width;
			$verot->file_overwrite = true;
			$verot->process($path);
		}
		
		$imagem_url = converter_para_url($caminho_nova_imagem);
	}

	
	return "<img src='{$imagem_url}' width='{$width}' height='{$height}' alt='{$alt}'>";

}

function compress_image($source, $destination, $quality) {

	

	$source = converter_para_arquivo($source);
	$destination = converter_para_arquivo($destination);

	$tmp = $_SERVER['DOCUMENT_ROOT'] . '/wp-content/temp/compress.tmp';

	if(!file_exists($source)) { 

		clearstatcache();
		return [
			'size_src' => 0,
			'size_dst' => 0,
			'status' => SEO_IMAGENS_ARQUIVO_INEXISTENTE,
		];
	}
	else {
	    // backup
	    copy($source, $source . ".seo.backup");
	    
		copy($source, $tmp);
		$data['size_src'] = filesize($tmp);

	    $info = getimagesize($source);

		$data['status'] = SEO_IMAGENS_OK;
	    if ($info['mime'] == 'image/jpeg') {
	        $image = imagecreatefromjpeg($source);
	    }

	    elseif ($info['mime'] == 'image/gif') {
	        $image = imagecreatefromgif($source);
	    }

	    elseif ($info['mime'] == 'image/png') {
	        $image = imagecreatefrompng($source);
	    }

	    else {
	    	$data['status'] = SEO_IMAGENS_TIPO_INVALIDO;
	    }

	    if($data['status'] == SEO_IMAGENS_OK) {
	    	imagejpeg($image, $destination, $quality);

	    	if(filesize($destination) > filesize($tmp)) {
				copy($tmp, $destination);
	    	}

	    }

	    $data['size_dst'] = filesize($destination);

	    return $data;
	}
}


function get_pagina_url($pagina)
{
	return $_SERVER['DOCUMENT_ROOT'] . '/wp-content/themes/academy/paginas/' . $pagina; 
}

function get_tema_image_url($imagem)
{
	return get_bloginfo('stylesheet_directory') . '/images/' . $imagem; 
}

function get_tema_js_url($js)
{
	return get_bloginfo('stylesheet_directory') . '/js/' . $js;
}

function tema_js_url($js)
{
	echo get_tema_js_url($js);
}

function get_cabecalho_secao($imagem, $texto, $subtexto = '', $h1_class = '')
{
	$imagem_url = get_tema_image_url($imagem);

	if($h1_class)
	{
		$h1_class = " class={$h1_class} ";
	}

	$html = 	"<div class='col-12 pt-3'>
				<h1 {$h1_class}>$texto</h1>
				<div>$subtexto</div>
				</div>";
	return $html;
}

function get_barra_titulo($texto, $icone_url = false)
{
	$icone = $icone_url ? "<img class='alignnone' alt='' src='{$icone_url}'>" : "";
	
	$html = "<div class='col-12 pt-3'>
				<h1>$texto</h1>
				<div>$subtexto</div>
				</div>";
	
	return $html;
}	

function get_voltar_link($texto = '&lt;&lt; Voltar', $url = false, $usuario_logado = false)
{
	if($texto == '&lt;&lt; Minha Conta') {
		return "<a href='$url'>$texto</a>";
	}
	
	if($usuario_logado) {
		return "<a href='#' onclick='window.history.back()'>$texto</a>";
	}	
	if(!$url) {
		return "<a href='#' onclick='window.history.back()'>$texto</a>";
	}
	else	
		return "<a href='$url'>$texto</a>";
}

function loading_img()
{
	return get_tema_image_tag('loader.gif', null, null, 'Carregando...');
}

function mensagem_sucesso($msg)
{
	return "<div class='register-success text-center'>$msg</div>";
}

function mensagem_erro($msg)
{
	return "<div class='register-error text-center'>$msg</div>";
}

function get_tabela_thead($colunas)
{
	$html = "";
	foreach ($colunas as $coluna) {
		$html .= "<th>" . $coluna . "</th>";
	}

	return "<thead><tr>$html</tr></thead>";
}

function get_tabela_tr($colunas)
{
	$html = "";
	foreach ($colunas as $coluna) {
		$html .= "<td>" . $coluna . "</td>";
	}

	return "<tr>$html</tr>";
}


/**
 * Retorna o H1 de uma página. H1 é um campo customizado de objetos do tipo Página
 * 
 * @since k2
 * 
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2252
 * 
 * @param int $post_id Id do Post
 * 
 * @return string
 */
 
function get_h1($post_id = NULL, $is_concurso = FALSE)
{
	global $post;

	if(is_null($post_id)) {
		$post_id = $post->ID;
	}

	$h1 = get_post_meta($post_id, META_H1, TRUE);

	// se não tem h1 definido, busca o title
	if(!$h1) {
		if($is_concurso) {
			$h1 = "Concurso " . get_the_title($post_id);
		}
		else {
			$h1 = get_the_title($post_id);
		}
	}

	return html_entity_decode($h1) ?: "";
}

add_filter( 'woocommerce_show_page_title' , 'woo_hide_page_title' );
/**
 * woo_hide_page_title
 *
 * Removes the "shop" title on the main shop page
 *
 * @access      public
 * @since       1.0 
 * @return      void
*/
function woo_hide_page_title() {
	return false;
}

function get_nav_cursos()
{  	
   	$ativo1 = "";
   	$ativo2 = "";
   	$ativo3 = "";
   	$ativo4 = "";
   	
	if(UrlHelper::is_url_cursos_por_concurso()) {
		$ativo1 = "is-active";
	}
	elseif(UrlHelper::is_url_cursos_por_materia()) {
		$ativo2 = "is-active";
	}
	elseif(UrlHelper::is_url_cursos_por_professor()) {
		$ativo3 = "is-active";
	}
	elseif(UrlHelper::is_url_todos_cursos()) {
		$ativo4 = "is-active";
	}	

	return $html = 
		"<nav id=\"nav-concursos\" class=\"nav nav-pills flex-column\">
		<a href=\"/cursos-gratis\" class=\" font-roboto font-weight-bold text-blue-2 text-uppercase font-14 nav-link\">
            <i class=\"fa fa-angle-right text-green\"></i>
            Cursos Grátis
          </a>         
          <a href=\"/cursos-por-concurso\" class=\"$ativo1 font-roboto font-weight-bold text-blue-2 text-uppercase font-14 nav-link\">
            <i class=\"fa fa-angle-right text-green\"></i>
            Por Concurso
          </a>
          <a href=\"/cursos-por-materia\" class=\"$ativo2 font-roboto font-weight-bold text-blue-2 text-uppercase font-14 nav-link\">
           <i class=\"fa fa-angle-right text-green\"></i>
            Por Matéria
          </a>
          <a href=\"/cursos-por-professor\" class=\"$ativo3 font-roboto font-weight-bold text-uppercase text-blue-2 font-14 nav-link\">
            <i class=\"fa fa-angle-right text-green\"></i>
            Por Professor
          </a>
         <a href=\"/todos-cursos\" class=\"$ativo4 font-roboto font-weight-bold text-blue-2 text-uppercase font-14 nav-link\">
            <i class=\"fa fa-angle-right text-green\"></i>
            Todos Cursos
          </a>          
        </nav>";
	
}

function get_nav_material_gratis()
{  	
   	$active1 = "";
   	$active2 = "";
   	$active3 = "";
   	$active4 = "";
   	$active5 = "";
   	
   if(is_pagina_cursos_gratis()) {
   	  $active1 = "is-active";
   } elseif(is_pagina_simulados_gratis()) {
   	$active2 = "is-active";
   } elseif(is_pagina_mapas_mentais_gratis()) {
   	  $active3 = "is-active";
   } elseif(is_pagina_questoes_comentadas_gratis()) {
   	  $active4 = "is-active";
   }
   elseif(is_pagina_audiobooks_gratis()) {
   	  $active5 = "is-active";
   }


	return $html = 
		"<nav id=\"nav-material-gratis\" class=\"nav nav-pills flex-column\">         
          <a href=\"/cursos-gratis\" class=\"$active1 font-roboto font-weight-bold text-blue-2 text-uppercase font-14 nav-link\">
            <i class=\"fa fa-angle-right text-green\"></i>
            Cursos Grátis
          </a>
          <a href=\"/simulados-gratis\" class=\"$active2 font-roboto font-weight-bold text-blue-2 text-uppercase font-14 nav-link\">
           <i class=\"fa fa-angle-right text-green\"></i>
            Simulados Grátis
          </a>
          <a href=\"/mapas-mentais-gratis\" class=\"$active3 font-roboto font-weight-bold text-uppercase text-blue-2 font-14 nav-link\">
            <i class=\"fa fa-angle-right text-green\"></i>
            Mapas Mentais Grátis
          </a>
         <a href=\"/questoes-comentadas-gratis\" class=\"$active4 font-roboto font-weight-bold text-blue-2 text-uppercase font-14 nav-link\">
            <i class=\"fa fa-angle-right text-green\"></i>
            Questoes Comentadas Grátis
          </a>
          <a href=\"/audiobooks-gratis\" class=\"$active5 font-roboto font-weight-bold text-blue-2 text-uppercase font-14 nav-link\">
            <i class=\"fa fa-angle-right text-green\"></i>
            Audiobooks Grátis
          </a>            
        </nav>";
	
}