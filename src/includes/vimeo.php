<?php 
require_once $_SERVER['DOCUMENT_ROOT'] . "/wp-content/vimeo/autoload.php";

function vimeo_auth()
{
	$client_id = "4047998f8e00bec0ff00ee2b1af82ba1d0aadf2f";
	$client_secret = "SLWbGRQ4eoT/RE2YdYcHX9+qN76iFWDKDqIRIUxVW2mEVcMTtzlSkvDJvot6U7fli7eAeaYw/3lEik13YMZJV2LwDmjl66eugj8P5W6lLR2YQjgtoVWAcNKhWnARdlCg";
	$access_token = "0f044475fab3af7136ce5797366e3539";

	$lib = new \Vimeo\Vimeo($client_id, $client_secret, $access_token);

	return $lib;

	// $response = $lib->request('/me/videos', array('per_page' => 2), 'GET');
}

function vimeo_search()
{
	return 	"<div style='text-align:right'><input type='text' id='txt-vimeo' placeholder='Buscar por...'><a href='#' onclick='buscar_videos_vimeo(jQuery(\"#txt-vimeo\").val(), event)' class='button'>Buscar</a></div>";
}

function vimeo_player_assets()
{
	return '<script src="/wp-content/themes/academy/js/jquery-lity.js"></script>
			<link href="/wp-content/themes/academy/css/lity.min.css" rel="stylesheet">
			<script src="/wp-content/themes/academy/js/lity.min.js"></script>

			<style>
			.lity, .lity-wrap {
				z-index: 99999 !important;
			}
			</style>';
}

/**
 * Recupera o id de um vídeo do vimeo através da url
 * 
 * @see https://stackoverflow.com/questions/10488943/easy-way-to-get-vimeo-id-from-a-vimeo-url
 * 
 * @since Joule 3
 * 
 * @param string $url
 *
 * @return int
 */

function get_vimeo_video_id($url) 
{

	if(preg_match("/(https?:\/\/)?(www\.)?(player\.)?vimeo\.com\/([a-z]*\/)*([0-9]{6,11})[?]?.*/", $url, $output_array)) {
    	return $output_array[5];
	}

}

/**
 * Retorna um objeto com os dados de um vídeo do Vimeo a partir do seu id
 * 
 * @see https://developer.vimeo.com/api/endpoints/videos#GET/videos/{video_id}
 * 
 * @since Joule 3
 * 
 * @param int $id
 *
 * @return array
 */

function get_vimeo_video_by_id($id)
{
	/**
	 * Cache necessário por conta das limitações do Vimeo
	 * @see https://developer.vimeo.com/guidelines/rate-limiting
	 */ 

	$obj = get_transient(TRANSIENT_VIMEO_VIDEO . "_" . $id);
	if(!$obj) {
		$api = vimeo_auth();
		$obj = $api->request("/videos/$id");
		set_transient(TRANSIENT_VIMEO_VIDEO . "_" . $id, $obj, DAY_IN_SECONDS);
	};

	return [
		'nome' => $obj['body']['name']
	];
}

/**
 * Retorna um objeto com os dados de um vídeo do Vimeo a partir de uma url
 * 
 * @see https://developer.vimeo.com/api/endpoints/videos#GET/videos/{video_id}
 * 
 * @since Joule 3
 * 
 * @param string $url
 *
 * @return array
 */

function get_vimeo_video_by_url($url)
{
	$id = get_vimeo_video_id($url);
	return get_vimeo_video_by_id($id);
}