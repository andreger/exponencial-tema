<?php

/**
 * Insere uma imagem execução
 * 
 * @since K1 
 * 
 * @param array $dados
 * 
 * @return int 
 * 
 */

function salvar_seo_imagem_execucao($dados)
{
	global $wpdb;

	$wpdb->insert('seo_imagens_execucoes', $dados);

	return $wpdb->insert_id;
}

/**
 * Insere uma imagem execução detalhe
 * 
 * @since K1 
 * 
 * @param array $dados
 * 
 * @return int 
 * 
 */

function salvar_seo_imagem_detalhe($dados)
{
	global $wpdb;

	$wpdb->insert('seo_imagens_detalhes', $dados);

	return $wpdb->insert_id;
}

/**
 * Atualizar uma imagem execução
 * 
 * @since K1 
 * 
 * @param int $id ID da imagem execução
 * @param array $dados
 * 
 */

function atualizar_seo_imagem_execucao($id, $dados)
{
	global $wpdb;

	$wpdb->update('seo_imagens_execucoes', $dados, ['sie_id' => $id]);
}

/**
 * Lista imagens execuções
 * 
 * @since K1 
 * 
 * @param int $limit
 * @param int $offset
 * 
 * @return array
 */

function listar_seo_imagens_execucoes($limit = 20, $offset = 0)
{
	global $wpdb;

	return $wpdb->get_results("SELECT * FROM seo_imagens_execucoes ORDER BY sie_data_hora DESC LIMIT $offset, $limit ", ARRAY_A);
}

/**
 * Conta total de imagens execuções
 * 
 * @since K1 
 * 
 * @return int
 */

function contar_seo_imagens_execucoes()
{
	global $wpdb;

	return $wpdb->get_var("SELECT COUNT(`sie_id`) FROM seo_imagens_execucoes");
}

/**
 * Lista imagens detalhes
 * 
 * @since K1 
 * 
 * @param int $limit
 * @param int $offset
 * 
 * @return array
 */

function listar_seo_imagens_detalhes($id, $limit = 20, $offset = 0)
{
	global $wpdb;

	return $wpdb->get_results("SELECT * FROM seo_imagens_detalhes WHERE sie_id = {$id} ORDER BY sid_arquivo ASC LIMIT $offset, $limit ", ARRAY_A);
}

/**
 * Conta total de imagens detalhes
 * 
 * @since K1 
 * 
 * @return int
 */

function contar_seo_imagens_detalhes($id)
{
	global $wpdb;

	return $wpdb->get_var("SELECT COUNT(`sie_id`) FROM seo_imagens_detalhes WHERE sie_id = {$id} ");
}

function seo_imagens_status_str($status)
{
	switch($status) {
		case SEO_IMAGENS_OK: return "Ok";
		case SEO_IMAGENS_TIPO_INVALIDO: return "Tipo inválido";
		case SEO_IMAGENS_ARQUIVO_INEXISTENTE: return "Arquivo inexistente";
	}

	return "";
}

/**
 * Insere uma H1 info
 * 
 * @since K2
 * 
 * @param array $dados
 * 
 * @return int 
 * 
 */

function salvar_seo_h1($dados)
{
	global $wpdb;

	if(get_seo_h1_by_id($dados['sh1_id'])) {

		$wpdb->update(
			'seo_h1',
			$dados,
			array(
				'sh1_id' => $dados['sh1_id']
			)
		);
	}
	else {
		$wpdb->insert('seo_h1', $dados);
	}
}

function iniciar_seo_h1s($posts)
{
	global $wpdb;

	$values = array();
	$place_holders = array();
	
	$query = "INSERT INTO seo_h1 (sh1_id, sh1_url, sh1_titulo, sh1_h1, sh1_num, sh1_edit_url) VALUES ";

	foreach($posts as $post)
	{
	     array_push($values, $post->ID, "", "", "", -1, "");
	     $place_holders[] = "('%d', '%s', '%s', '%s', '%d', '%s')";
	}

	$query .= implode(', ', $place_holders);
	$wpdb->query( $wpdb->prepare("$query ", $values));
}


function listar_seo_h1_nao_processados()
{
	global $wpdb;

	$sql = "SELECT * from seo_h1 WHERE sh1_num = -1 LIMIT 10";

	$resultado = $wpdb->get_results($sql, ARRAY_A);

	return $resultado ?: NULL;
}

function contar_seo_h1_processados()
{
	global $wpdb;

	return $wpdb->get_var("SELECT COUNT(*) from seo_h1 WHERE sh1_num != -1");
}

function contar_todos_seo_h1()
{
	global $wpdb;

	return $wpdb->get_var("SELECT COUNT(*) from seo_h1");
}


/**
 * Recuper informações de H1 específico
 * 
 * @since K3
 * 
 * @param string $link
 * 
 * @return array 
 */

function get_seo_h1_by_id($id)
{
	global $wpdb;

	$sql = "SELECT * from seo_h1 WHERE sh1_id = {$id}";

	$resultado = $wpdb->get_row($sql, ARRAY_A);

	return $resultado ?: null;
}

/**
 * Listar informações de H1s
 * 
 * @since K2
 * 
 * @param int $limit
 * @param int $offset
 * @param string $texto Filtro textual
 * @param int $num_tipo Filtra por tipo de dado (1 = sem H1, 2 = mais de 1 H1)
 * 
 * @return array 
 * 
 */

function listar_seo_h1($limit = 20, $offset = 0, $texto = "", $num_tipo = NULL)
{
	global $wpdb;

	$where_texto = $texto ? " AND (sh1_url LIKE '%{$texto}%' OR sh1_titulo LIKE '%{$texto}%' OR sh1_h1 LIKE '%{$texto}%') " : "";
		
	$where_num_tipo = "";
	if($num_tipo == 1) {
		$where_num_tipo = " AND sh1_num = 0 ";
	}
	elseif($num_tipo == 2) {
		$where_num_tipo = " AND sh1_num > 1 ";	
	}

	$sql = "SELECT * FROM seo_h1 where 1=1 {$where_texto} {$where_num_tipo} ORDER BY sh1_id ASC LIMIT $offset, $limit";

	return $wpdb->get_results($sql, ARRAY_A);
}

/**
 * Conta informações de H1s
 * 
 * @since K2
 * 
 * @param string $texto Filtro textual
 * @param int $num_tipo Filtra por tipo de dado (1 = sem H1, 2 = mais de 1 H1)
 * 
 * @return int 
 * 
 */

function contar_seo_h1($texto = "", $num_tipo = NULL)
{
	global $wpdb;

	$where_texto = $texto ? " AND (sh1_url LIKE '%texto%' OR sh1_titulo LIKE '%texto%' OR sh1_h1 LIKE '%texto%') " : "";
		
	$where_num_tipo = "";
	if($num_tipo == 1) {
		$where_num_tipo = " AND sh1_num = 0 ";
	}
	elseif($num_tipo == 2) {
		$where_num_tipo = " AND sh1_num > 1 ";	
	}

	return $wpdb->get_var("SELECT COUNT(`sh1_id`) FROM seo_h1 where 1=1 {$where_texto} {$where_num_tipo}");
}


function excluir_seo_h1()
{
	global $wpdb;
	$wpdb->query("TRUNCATE TABLE seo_h1");
}
