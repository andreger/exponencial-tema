<?php
function get_ajax_url($arquivo)
{
	return '/wp-content/themes/academy/ajax/' . $arquivo;
}

function get_excerpt_by_id($post_id, $palavras = 12)
{
	$the_post = get_post($post_id); //Gets post ID
	$the_excerpt = $the_post->post_content; //Gets post_content to be used as a basis for the excerpt
	$excerpt_length = $palavras; //Sets excerpt length by word count
	$the_excerpt = strip_tags(strip_shortcodes($the_excerpt)); //Strips tags and images
	$words = explode(' ', $the_excerpt, $excerpt_length + 1);

	if(count($words) > $excerpt_length) :
	array_pop($words);
	array_push($words, '[…]');
	$the_excerpt = implode(' ', $words);
	endif;

	return $the_excerpt;
}

function get_extensao($arquivo) 
{
	$arquivo_a = explode('.', $arquivo);
	return strtolower(array_pop($arquivo_a));
}

function is_pdf($arquivo)
{
	return get_extensao($arquivo) == 'pdf' ? true : false;
}

function is_imagem($arquivo)
{
	return in_array(get_extensao($arquivo), ['png', 'jpg', 'jpeg', 'gif']) ? true : false;
}

function is_video($arquivo)
{
	$extensoes = get_video_extensoes();
	$extensao_arquivo = get_extensao($arquivo);

	foreach ($extensoes as $extensao) {
		if($extensao_arquivo == $extensao) {
			return true;
		}
	}
	return false;
}

function get_video_extensoes()
{
	return array('mp4', 'wmv', 'avi');
}

function get_nome_arquivo($arquivo, $incluir_extensao = TRUE)
{
	$arquivo_a = explode('/', parse_url($arquivo, PHP_URL_PATH));
	
	$nome = end($arquivo_a);
	if($incluir_extensao) {
		return $nome;
	}
	else {
		$nome_a = explode('.', $nome);
		array_pop($nome_a);

		return implode('.', $nome_a);
	}
}

/**
 * Retorna a URL de login
 * 
 * @param string $ref URL a ser redirecionada após o login
 * 
 * @return string URL de login
 */
 
function login_url($ref = null) 
{
	if($ref) {
		return '/cadastro-login?ref=' . $ref;
	}
	elseif($_SESSION[ULTIMA_URL]) {
		return '/cadastro-login?ref=' . $_SESSION[ULTIMA_URL];
	}

	return '/cadastro-login';
}

function truncar($string, $tamanho = 30)
{
	return strlen($string) > $tamanho ? mb_substr($string, 0, $tamanho) . "..." : $string;
}

function pluralizar($num, $singular, $plural)
{
	if($num == 1) 
		return "1 {$singular}";
	return "{$num} {$plural}";
}

function starts_with($haystack, $needle)
{
	$length = strlen($needle);
	return (substr($haystack, 0, $length) === $needle);
}

function ends_with($haystack, $needle)
{
	$length = strlen($needle);
	if ($length == 0) {
		return true;
	}

	return (substr($haystack, -$length) === $needle);
}

function str_replace_first($search, $replace, $haystack)
{
	$pos = strpos($haystack, $search);
	
	if ($pos !== false) {
    	$newstring = substr_replace($haystack, $replace, $pos, strlen($search));
	}

	return $newstring;
}

function str_replace_last( $search , $replace , $str ) {
	if( ( $pos = strrpos( $str , $search ) ) !== false ) {
		$search_length  = strlen( $search );
		$str    = substr_replace( $str , $replace , $pos , $search_length );
	}
	return $str;
}

function desformatar_cpf($cpf)
{
	$cpf = str_replace('-','', $cpf);
	$cpf = str_replace('.','', $cpf);
	
	return $cpf;
}

function validaCPF($cpf = null) {

	// Verifica se um número foi informado
	if(empty($cpf)) {
		return false;
	}

	// Elimina possivel mascara
	$cpf = preg_replace('/[^0-9]/', '', $cpf);
	$cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);
	 
	// Verifica se o numero de digitos informados é igual a 11
	if (strlen($cpf) != 11) {
		return false;
	}
	// Verifica se nenhuma das sequências invalidas abaixo
	// foi digitada. Caso afirmativo, retorna falso
	else if ($cpf == '00000000000' ||
			$cpf == '11111111111' ||
			$cpf == '22222222222' ||
			$cpf == '33333333333' ||
			$cpf == '44444444444' ||
			$cpf == '55555555555' ||
			$cpf == '66666666666' ||
			$cpf == '77777777777' ||
			$cpf == '88888888888' ||
			$cpf == '99999999999') {
				return false;
				// Calcula os digitos verificadores para verificar se o
				// CPF é válido
			} else {
				 
				for ($t = 9; $t < 11; $t++) {
					 
					for ($d = 0, $c = 0; $c < $t; $c++) {
						$d += $cpf{$c} * (($t + 1) - $c);
					}
					$d = ((10 * $d) % 11) % 10;
					if ($cpf{$c} != $d) {
						return false;
					}
				}
	
				return true;
			}
}

function numero_americano($valor)
{
	$valor = str_replace('.', '', $valor);
	return str_replace(',', '.', $valor);
}

function numero_brasileiro($valor, $decimais = 2)
{
	return number_format($valor, $decimais, ',', '.');
}

function moeda($valor, $simbolo = true, $decimais = 2)
{
	$result = "";
	
	if($simbolo) $result .= 'R$ ';
	
	return $result . number_format($valor, $decimais, ',', '.');
}

function periodo_mes_ano($valor)
{
    return substr($valor, 4, 2) . "/" . substr($valor, 0, 4);
}

function truncar_numero($number, $precision = 2, $separator = '.'){
    $numberParts = explode($separator, $number);
    $response = $numberParts[0];
    if(count($numberParts)>1){
        $response .= $separator;
        $response .= substr($numberParts[1], 0, $precision);
    }
    return $response;
}

function arredondar_numero($number, $casas_decimais = 2){
	return number_format($number, $casas_decimais);
}

function timestamp_em_dias($timestamp)
{
	return $timestamp / (3600 * 24);
}

function porcentagem($valor, $decimais = 2)
{
	return number_format($valor, $decimais, ',', '.') . ' %';
}

function valor_percentual($parte, $todo)
{
    return number_format($parte / $todo * 100, 2, ',', '.') . ' %';
}

function porcentagem_desconto($preco, $sem_desconto)
{

	$desconto = abs(($preco - $sem_desconto) / $sem_desconto * 100);
	return number_format($desconto, 0, ',', '.') . '%';
}


function decimal($valor, $unidade = '')
{
	return number_format($valor, 2, ',', '.') . ' ' . $unidade;
}

function numero_inteiro($valor)
{
	return number_format($valor, 0, ',', '.');
}

function sanitizar_string($str, $remover_html = FALSE)
{
	if($remover_html == TRUE)
	{
		$str = preg_replace('/<[^>]+>/', '', $str);
	}

	$str = str_replace("&#39;", '\'', $str);
	$str = str_replace('&#8217;', '\'', $str);
	$str = str_replace('&#8211;', '-', $str);
	$str = str_replace('&ldquo;', '', $str);
	$str = str_replace('&quot;', '', $str);
	$str = str_replace('&#233', 'e', $str);
	$str = str_replace('&amp;', '&', $str);

	return $str;
}

function converter_para_yyyymmdd($str)
{
	if($str) {
		$str = str_replace('/', '-', $str);
		return date('Y-m-d', strtotime($str));
	}

	return null;
}

function hoje_yyyymmdd()
{
	return date('Y-m-d');
}

function converter_para_yyyymmdd_HHiiss($str)
{
	$str = str_replace('/', '-', $str);
	return date('Y-m-d H:i:s', strtotime($str));
}

function converter_para_ddmmyyyy($str)
{
	if($str)
		return date('d/m/Y', strtotime($str));
	return '';
}

function converter_para_ddmmyyyy_HHii($str)
{
	if($str)
		return date('d/m/Y H:i', strtotime($str));
	return '';
}

function converter_para_ddmmyyyy_HHiiss($str)
{
	if($str)
		return date('d/m/Y H:i:s', strtotime($str));
	return '';
}

function converter_para_yyyy($str)
{
	if($str)
		return date('Y', strtotime($str));
	return '';
}

function print_pre($var, $interromper = true) {
	if($_GET['pp']) {
		echo "<pre>";
		print_r($var);
		echo "</pre>";
		if($interromper) exit;
	}
}

function limitar_texto($texto, $limite){
	$contador = strlen($texto);
	if ( $contador >= $limite ) {
		$texto = substr($texto, 0, strrpos(substr($texto, 0, $limite), ' ')) . '...';
		return $texto;
	}
	else{
		return $texto;
	}
}

function get_percentual_desconto($produto, $decimais = 0)
{
	$diferenca = $produto->regular_price - $produto->price;
	
	$percentual = $diferenca / $produto->regular_price * 100;
	return number_format($percentual, $decimais);
}

function get_percentual_desconto_str($produto, $decimais = 0)
{
	if($produto->regular_price > 0) {
		return "Economia de " . get_percentual_desconto($produto) . "%";
	}
	
	return "";
}

function get_estados_array()
{
	return array(
		"" 	 => "",
		"AC" => "Acre",
		"AL" => "Alagoas",
		"AP" => "Amapá",
		"AM" => "Amazonas",
		"BA" => "Bahia",
		"CE" => "Ceará",
		"DF" => "Distrito Federal",
		"ES" => "Espirito Santo",
		"GO" => "Goiás",
		"MA" => "Maranhão",
		"MT" => "Mato Grosso",
		"MS" => "Mato Grosso do Sul",
		"MG" => "Minas Gerais",
		"PA" => "Pará",
		"PB" => "Paraiba",
		"PR" => "Paraná",
		"PE" => "Pernambuco",
		"PI" => "Piauí",
		"RJ" => "Rio de Janeiro",
		"RN" => "Rio Grande do Norte",
		"RS" => "Rio Grande do Sul",
		"RO" => "Rondônia",
		"RR" => "Roraima",
		"SC" => "Santa Catarina",
		"SP" => "São Paulo",
		"SE" => "Sergipe",
		"TO" => "Tocantins");
}

function get_cidades_array($uf)
{
	if($uf) {
		$cidades = get_cidades_por_estado($uf);
		
		$cidades_array = array();
		foreach ($cidades as $cidade) {
			$cidades_array[$cidade['cid_codigo_municipio']] = $cidade['cid_nome'];
		}
		
		return $cidades_array;
	}
	
	return array();
}

function array_de_inteiros($array_in)
{
	$array_out = array();
	
	foreach ($array_in as $item) {
		array_push($array_out, (int)$item);
	}
	
	return $array_out;
}

function remover_ultimos_valores_vazio($array_in)
{
	$i = count($array_in) - 1;
	for(; $i >= 0;)
	{
		if(empty(trim($array_in[$i]))){
			$i--;
			continue;
		}else{
			break;
		}
	}
	if($i >= 0)
	{
		return array_slice($array_in, 0, $i + 1);
	}
	else
	{
		return array();
	}
}

function get_data_hora_agora($formato = 'Y-m-d H:i:s')
{
	return date($formato);
}

function redirecionar_erro_500()
{
	header('Location: /500.php');
	exit;
}

function redirecionar_erro_404()
{
	global $wp_query;
	$wp_query->set_404();
	status_header( 404 );
	get_template_part( 404 );
}

function get_thumb_url_for_facebook($post)
{
	if(UrlHelper::is_url_cursos_por_concurso_especifico()) {
		$nome_concurso = get_query_var('nome');
		$dados_post = get_concurso_por_slug($nome_concurso);
		$meta = get_post_meta($dados_post->ID, '_thumbnail_id', TRUE);
		$post_agora = get_post($meta);
		return $post_agora->guid;
	}
	
	if($post && $post->post_type == 'post') {
		$thumb = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full' );
		$url = $thumb['0'];
		return $url;
	}
}

function converter_para_arquivo($url)
{
	return str_replace(get_site_url(), $_SERVER['DOCUMENT_ROOT'], trim($url));
}

function converter_para_url($arquivo)
{
	return str_replace($_SERVER['DOCUMENT_ROOT'], get_site_url(), trim($arquivo));
}

function purificar_html($texto) {
	$texto = stripcslashes($texto);
	
	if(substr_count($texto, '<!--') > substr_count($texto, '-->'))
		return $texto . "-->";
	return $texto;
}

function is_homologacao()
{
	return $_SERVER['HTTP_HOST'] == 'homol.exponencialconcursos.com.br';
}

function is_desenvolvimento()
{
	return $_SERVER['HTTP_HOST'] == 'exponencial';
}

function is_producao()
{
	return $_SERVER['HTTP_HOST'] == 'www.exponencialconcursos.com.br';
}

function is_selenium()
{
	return isset($_GET['skip']) && $_GET['skip'] == SELENIUM_KEY ? true : false;
}



function array_merge_recursive_ex(array & $array1, array & $array2)
{
    $merged = $array1;

    foreach ($array2 as $key => & $value)
    {
        if (is_array($value) && isset($merged[$key]) && is_array($merged[$key]))
        {
            $merged[$key] = array_merge_recursive_ex($merged[$key], $value);
        } else if (is_numeric($key))
        {
             if (!in_array($value, $merged))
                $merged[] = $value;
        } else
            $merged[$key] = $value;
    }

    return $merged;
}

function get_utm($utm_source, $utm_medium, $utm_name)
{
	return "?utm_source={$utm_source}&utm_medium={$utm_medium}&utm_name={$utm_name}";
}

function array_remove_blank($arr) 
{
	return array_filter($arr, function($value) { return $value !== ''; });
}

function get_url_atual()
{
	return (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
}

function get_path_atual()
{
	return $_SERVER[REQUEST_URI];
}

function is_pagina_cursos_online()
{
	return strpos($_SERVER['REQUEST_URI'], '/cursos-online/') !== false;
}

function is_pagina_pesquisa()
{
	return strpos($_SERVER['REQUEST_URI'], '/pesquisa/') !== false;
}

function is_pagina_cursos_gratis()
{
	return strpos($_SERVER['REQUEST_URI'], '/cursos-gratis/') !== false;
}

function is_pagina_simulados_gratis()
{
	return strpos($_SERVER['REQUEST_URI'], '/simulados-gratis/') !== false;
}
function is_pagina_mapas_mentais_gratis()
{
	return strpos($_SERVER['REQUEST_URI'], '/mapas-mentais-gratis/') !== false;
}

function is_pagina_questoes_comentadas_gratis()
{
	return strpos($_SERVER['REQUEST_URI'], '/questoes-comentadas-gratis/') !== false;
}

function is_pagina_audiobooks_gratis()
{
	return strpos($_SERVER['REQUEST_URI'], '/audiobooks-gratis/') !== false;
}

/**
 * Verifica se a página corrente é a de cursos por professor
 * 
 * @since K3
 * 
 * @return bool
 */
 
function is_pagina_cursos_por_professor_especifico()
{
	return preg_match("/\/cursos-por-professor\/.+/", $_SERVER['REQUEST_URI']);
}

function is_pagina_carrinho()
{
	return strpos($_SERVER['REQUEST_URI'], '/carrinho/') !== false;
}

function is_pagina_checkout()
{
	return strpos($_SERVER['REQUEST_URI'], '/checkout/') !== false;
}

function is_pagina_agradecimento()
{
	return strpos($_SERVER['REQUEST_URI'], '/order-received/') !== false;
}

function is_pagina_sq_meus_simulados()
{
	return strpos($_SERVER['REQUEST_URI'], '/meus_simulados') !== false;
}

function get_uf($estado)
{
	if(strlen($estado) <= 2) {
		return strtoupper($estado);
	}
	else {

		$estado = strtolower(trim($estado));

		switch ($estado) {
			case 'acre': return 'AC'; break;
			case 'amapá': return 'AP'; break;
			case 'amazonas': return 'AM'; break;
			case 'bahia': return 'BA'; break;
			case 'ceará': return 'CE'; break;
			case 'distrito federal': return 'DF'; break;
			case 'espírito santo': return 'ES'; break;
			case 'goiás': return 'GO'; break;
			case 'maranhão': return 'MA'; break;
			case 'mato grosso': return 'MG'; break;
			case 'mato grosso do sul': return 'MS'; break;
			case 'minas gerais': return 'MG'; break;
			case 'pará': return 'PA'; break;
			case 'paraíba': return 'PB'; break;
			case 'paraná': return 'PR'; break;
			case 'pernambuco': return 'PE'; break;
			case 'piaui': return 'PI'; break;
			case 'rio de janeiro': return 'RJ'; break;
			case 'rio grande do norte': return 'RN'; break;
			case 'rio grande do sul': return 'RS'; break;
			case 'rondônia': return 'RO'; break;
			case 'roraima': return 'RR'; break;
			case 'santa catarina': return 'SC'; break;
			case 'são paulo': return 'SP'; break;
			case 'sergipe': return 'SE'; break;
			case 'tocantins': return 'TO'; break;

			default: return ''; break;
		}
	}
}

function atualizar_billing_postcode()
{
	$aluno = get_usuario_array();

	$bp = get_user_meta($aluno['id'], 'billing_postcode', true);

	$tamanho = strlen($bp);

	if($tamanho == 8) return;

	$cep = $aluno['cep'];
	$tamanho = strlen($cep);

	if($tamanho == 5) { 
		update_user_meta($aluno['id'], 'billing_postcode', $cep . "000");
	}
	else if($tamanho == 8) { 
		update_user_meta($aluno['id'], 'billing_postcode', $cep);
	}
	else {
		update_user_meta($aluno['id'], 'billing_postcode', 10000000);
	}

}

function atualizar_billing_shipping_number()
{
	$aluno = get_usuario_array();

	update_user_meta($aluno['id'], 'billing_address_2', "1");
	update_user_meta($aluno['id'], 'billing_number', "1");
}

/**
 * Groups an array by a given key.
 *
 * Groups an array into arrays by a given key, or set of keys, shared between all array members.
 *
 * Based on {@author Jake Zatecky}'s {@link https://github.com/jakezatecky/array_group_by array_group_by()} function.
 * This variant allows $key to be closures.
 *
 * @param array $array   The array to have grouping performed on.
 * @param mixed $key,... The key to group or split by. Can be a _string_,
 *                       an _integer_, a _float_, or a _callable_.
 *
 *                       If the key is a callback, it must return
 *                       a valid key from the array.
 *
 *                       If the key is _NULL_, the iterated element is skipped.
 *
 *                       ```
 *                       string|int callback ( mixed $item )
 *                       ```
 *
 * @return array|null Returns a multidimensional array or `null` if `$key` is invalid.
 */
function array_group_by(array $array, $key)
{
	if (!is_string($key) && !is_int($key) && !is_float($key) && !is_callable($key) ) {
		trigger_error('array_group_by(): The key should be a string, an integer, or a callback', E_USER_ERROR);
		return null;
	}
	$func = (!is_string($key) && is_callable($key) ? $key : null);
	$_key = $key;
	// Load the new array, splitting by the target key
	$grouped = [];
	foreach ($array as $value) {
		$key = null;
		if (is_callable($func)) {
			$key = call_user_func($func, $value);
		} elseif (is_object($value) && isset($value->{$_key})) {
			$key = $value->{$_key};
		} elseif (isset($value[$_key])) {
			$key = $value[$_key];
		}
		if ($key === null) {
			continue;
		}
		$grouped[$key][] = $value;
	}
	// Recursively build a nested grouping if more parameters are supplied
	// Each grouped array value is grouped according to the next sequential key
	if (func_num_args() > 2) {
		$args = func_get_args();
		foreach ($grouped as $key => $value) {
			$params = array_merge([ $value ], array_slice($args, 2, func_num_args()));
			$grouped[$key] = call_user_func_array('array_group_by', $params);
		}
	}
	return $grouped;
}

function remover_ultimo_char_se($string, $char) {
	if($string[strlen($string) - 1] == $char) {		
		return substr($string, 0, strlen($string) - 1);
	}

	return $string;
}

function startsWith($haystack, $needle)
{
     $length = strlen($needle);
     return (substr($haystack, 0, $length) === $needle);
}

function endsWith($haystack, $needle)
{
    $length = strlen($needle);
    if ($length == 0) {
        return true;
    }

    return (substr($haystack, -$length) === $needle);
}