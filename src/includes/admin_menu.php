<?php
/**
 * Funções responsáveis pelo menu do WP-Admin
 *
 * @deprecated Use kcore/hooks/WPAdminMenuHooks.php
 * 
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2412
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2228
 */

 /*
add_action( 'admin_menu', 'my_remove_menu_pages' );
function my_remove_menu_pages() {
	// global $submenu, $menu, $pagenow;
	// echo "<pre>";
	// print_r($menu); 

	if(!tem_acesso([ADMINISTRADOR, REVISOR])) {

		remove_menu_page('index.php');
		remove_menu_page('edit.php?post_type=question');
		remove_menu_page('profile.php');
		remove_menu_page('edit.php?post_type=acf');
		remove_menu_page('edit.php?post_type=slide');
		remove_menu_page('edit.php?post_type=testimonial');
		remove_menu_page('edit.php?post_type=lesson');
		remove_menu_page('monsterinsights_dashboard');
		remove_menu_page('monsterinsights_settings');
		remove_menu_page('widgets-on-pages');
		remove_menu_page('sg-cachepress');
		remove_menu_page('wp-to-buffer-settings');
		remove_menu_page('admin.php?page=wp-to-buffer');
		remove_menu_page('edit-comments.php');

		// Remove os links do Woocommerce, exceto para Atendente

		if(!tem_acesso([ATENDENTE])) {
			remove_menu_page('woocommerce');
		}

		remove_menu_page('WP-Optimize');

		if(!tem_acesso([COORDENADOR])) {

			// Remove os link Concursos e RevSlider, exceto dos perfis: Apoio e Atendente
			
			if(!tem_acesso([APOIO, ATENDENTE])) {
				remove_menu_page('edit.php?post_type=concurso');
				remove_menu_page('admin.php?page=revslider');
			}
			
		}
		
	}

	if(!tem_acesso([ADMINISTRADOR, APOIO, ATENDENTE])) {
		remove_menu_page('upload.php');
		remove_menu_page('edit.php?post_type=acf-field-group');
	}

	remove_submenu_page('edit.php?post_type=topic', 'edit-tags.php?taxonomy=topic-tag&amp;post_type=topic');
	
}

add_action('admin_init', 'my_remove_menu_pages2');
function my_remove_menu_pages2() {
	// global $submenu, $menu, $pagenow;
	// echo "<pre>";
	// print_r($submenu);
	// exit;

	if(!tem_acesso([ADMINISTRADOR, REVISOR])) {
		remove_menu_page('faq');
		remove_menu_page('edit.php?post_type=acf');
		remove_menu_page('tp_main');
		remove_menu_page('wpseo_dashboard');
		remove_menu_page('wp-to-buffer');
		remove_menu_page('jetpack');
		remove_menu_page('w3tc_dashboard');
		remove_menu_page('pap-top-level-options-handle');
		remove_menu_page('integrations-config-page-handle');
		remove_menu_page('edit.php?post_type=feedback');

		// if(!is_apoio()) {
		// 	remove_menu_page('exponencial');
		// }
		remove_menu_page('w3tc_dashboard');
		remove_menu_page('yst_ga_dashboard');
		remove_menu_page('themepunch-google-fonts');
		remove_menu_page('admin.php?page=yst_ga_dashboard');
		remove_menu_page('woocommerce-checkout-manager');
		remove_menu_page('wp_crm');

		// Remove páginas do Woocommerce
		remove_submenu_page('woocommerce', 'wc-reports');
		remove_submenu_page('woocommerce', 'wc-settings');
		remove_submenu_page('woocommerce', 'woocommerce-extra-checkout-fields-for-brazil');
		remove_submenu_page('woocommerce', 'wc-status');
		remove_submenu_page('woocommerce', 'wc-addons');
		remove_submenu_page('woocommerce', 'custom_related_products');

		if(!tem_acesso([COORDENADOR, APOIO, ATENDENTE])) {
			remove_menu_page('revslider');
		}	
		
		if(!tem_acesso([ATENDENTE])) {
			remove_submenu_page('edit.php?post_type=product', 'profile.php?page=courses');
		}
		
		remove_submenu_page('edit.php?post_type=topic', 'post-new.php?post_type=topic');
		remove_submenu_page('edit.php?post_type=reply', 'post-new.php?post_type=reply');
		
		// Remove o link Posts > Categorias, exceto dos perfis: Apoio e Atendente
		
		if(!tem_acesso([APOIO, ATENDENTE])) {
			remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=category');
		}

		remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=post_tag');
		
		remove_meta_box( 'wpseo-dashboard-overview', 'dashboard', 'normal' );
	}
}

add_action( 'admin_menu', 'my_URL_menu' );
function my_URL_menu() {
	global $menu;
	global $submenu;

	if(tem_acesso([ADMINISTRADOR])) {
		$menu[] = array('Relatórios Gerenciais', 'read', 'relatorio-de-vendas', '', 'menu-top menu-icon-generic target-blank','toplevel_page_courses', 'dashicons-admin-generic');
		$submenu['relatorio-de-vendas'][] = array( 'Relatório de Vendas', 'read', '/relatorio-de-vendas' );
		$submenu['relatorio-de-vendas'][] = array( 'Relatório de Vendas por Professor', 'read', '/relatorio-de-vendas-por-professor' );
		$submenu['relatorio-de-vendas'][] = array( 'Relatório de Vendas por Concurso', 'read', '/relatorio-de-vendas-por-concurso' );
		$submenu['relatorio-de-vendas'][] = array( 'Relatório de Vendas por Área', 'read', '/relatorio-de-vendas-por-area' );
		$submenu['relatorio-de-vendas'][] = array( 'Coaching', 'read', '/painel-coaching/relatorios/inscricoes' );
		$submenu['relatorio-de-vendas'][] = array( 'Notas Fiscais', 'read', '/notafiscal/main/listar_notas_fiscais' );
		$submenu['relatorio-de-vendas'][] = array( 'Pagamentos Pendentes do Coaching', 'read', '/painel-coaching/relatorios/pagamentos_pendentes' );
		$submenu['relatorio-de-vendas'][] = array( 'Pagamentos do Mês do Coaching', 'read', '/painel-coaching/relatorios/pagamentos_do_mes' );
		$submenu['relatorio-de-vendas'][] = array( 'Usuários Buscadores', 'read', '/questoes/buscador/usuario_buscador' );
		$submenu['relatorio-de-vendas'][] = array( 'Relatório de Erros', 'read', '/relatorio-de-erros' );
		$submenu['relatorio-de-vendas'][] = array( 'Relatório Cursos', 'read', '/relatorio-cursos' );
		$submenu['relatorio-de-vendas'][] = array( 'Monitor Keydea', 'read', 'http://monitor.keydea.com.br');
		$submenu['relatorio-de-vendas'][] = array( 'Relatório de Questões Comentadas', 'read', '/questoes/relatorios/questoes_comentadas');
		$submenu['relatorio-de-vendas'][] = array( 'Relatório de Avaliações de Comentários', 'read', '/questoes/relatorios/avaliacoes_comentarios');
		$submenu['relatorio-de-vendas'][] = array( 'Relatório de Assinaturas do SQ', 'read', '/questoes/relatorios/assinaturas');
		$submenu['relatorio-de-vendas'][] = array( 'Relatório de Avaliações de Comentários', 'read', '/questoes/relatorios/avaliacoes_comentarios');
		$submenu['relatorio-de-vendas'][] = array( 'Relatório de Produtos Premium', 'read', '/relatorio-produtos-premium');

	}
	else {
		if(!tem_acesso([REVISOR, APOIO, ATENDENTE])) {

			if(tem_acesso([COORDENADOR_COACHING])) {
				$menu[] = array('Coaching', 'read', '/painel-coaching/relatorios/inscricoes', '', 'menu-top menu-icon-generic target-blank','toplevel_page_courses', 'dashicons-admin-generic');
				$menu[] = array( 'Pagamentos Pendentes do Coaching', 'read', '/painel-coaching/relatorios/pagamentos_pendentes', '', 'menu-top menu-icon-generic target-blank','toplevel_page_courses', 'dashicons-admin-generic' );
				$menu[] = array( 'Pagamentos do Mês do Coaching', 'read', '/painel-coaching/relatorios/pagamentos_do_mes', '', 'menu-top menu-icon-generic target-blank','toplevel_page_courses', 'dashicons-admin-generic' );
			}

			$menu[] = array('Relatório de Vendas', 'read', '/relatorio-de-vendas', '', 'menu-top menu-icon-generic target-blank','toplevel_page_courses', 'dashicons-admin-generic');
			$menu[] = array('Relatório de Vendas Resumido', 'read', 'relatorio-de-vendas-por-professor', '', 'menu-top menu-icon-generic target-blank','toplevel_page_courses', 'dashicons-admin-generic');

			if(tem_acesso([COORDENADOR])) {
				$menu[] = array( 'Relatório de Vendas por Concurso', 'read', '/relatorio-de-vendas-por-concurso', '', 'menu-top menu-icon-generic target-blank','toplevel_page_courses', 'dashicons-admin-generic');
			}
			
			$menu[] = array('Relatório Cursos', 'read', '/relatorio-cursos', '', 'menu-top menu-icon-generic target-blank','toplevel_page_courses', 'dashicons-admin-generic');
			$menu[] = array('Relatório de Questões Comentadas', 'read', '/questoes/relatorios/questoes_comentadas', '', 'menu-top menu-icon-generic target-blank','toplevel_page_courses', 'dashicons-admin-generic');
			$menu[] = array( 'Relatório de Avaliações de Comentários', 'read', '/questoes/relatorios/avaliacoes_comentarios', '', 'menu-top menu-icon-generic target-blank','toplevel_page_courses', 'dashicons-admin-generic');
		}

		if(tem_acesso([ATENDENTE])) {
			$menu[] = array('Relatório Cursos', 'read', '/relatorio-cursos', '', 'menu-top menu-icon-generic target-blank','toplevel_page_courses', 'dashicons-admin-generic');
			// $menu[] = array( 'rEPROCESSAR', 'read', '/wp-admin/admin.php?page=exponencial-reprocessar', '', 'menu-top menu-icon-generic target-blank','toplevel_page_courses', 'dashicons-admin-generic');
		}
	}
	
	// Remove o link Acessar Fóruns, exceto dos perfis: Apoio e Atendente

 	if(!tem_acesso([APOIO, ATENDENTE])) {
		$menu[59] = array('Acessar Fóruns', 'read', '/forum', '', 'menu-top menu-icon-generic target-blank','toplevel_page_courses', 'dashicons-admin-generic');
	}

	// Adiciona o link Alterar Senha para todos os usuários

	$menu[] = array('Alterar Senha', 'read', '/alteracao-de-senha', '', 'menu-top menu-icon-generic target-blank','toplevel_page_courses', 'dashicons-admin-generic');

	if(!tem_acesso([ADMINISTRADOR])) {
		$menu[] = array('Artigos', 'read', 'edit.php', '', 'menu-top menu-icon-generic','toplevel_page_courses', 'dashicons-admin-generic');
		
		if(tem_acesso([REVISOR, APOIO, ATENDENTE])) {
			$menu[] = array('Páginas', 'read', 'edit.php?post_type=page', '', 'menu-top menu-icon-generic','toplevel_page_courses', 'dashicons-admin-generic');
		}
	}

}

add_action('admin_enqueue_scripts', 'expo_admin_menu_adicionar_target_blank');
function expo_admin_menu_adicionar_target_blank() {
	wp_enqueue_style('admin-css', get_template_directory_uri() . '/css/admin.css');
    wp_enqueue_script('admin-menu', get_template_directory_uri() . '/js/admin-menu.js', array('jquery'));
}

function expo_add_edit_form_multipart_encoding() {

	echo ' enctype="multipart/form-data"';

}
add_action('post_edit_form_tag', 'expo_add_edit_form_multipart_encoding');
*/