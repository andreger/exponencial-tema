<?php
function get_facebook_pixel_code($post_id)
{
	return get_post_meta($post_id, 'fb_pxl_conversion_code', TRUE);
}