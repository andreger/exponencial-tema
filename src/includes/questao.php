<?php

function get_disciplina_nome_from_assuntos($assuntos)
{
// 	print_r($assuntos);
	if(!empty($assuntos)) {
		return $assuntos[0]['dis_nome'];
	} 
	return null;
} 

function get_assuntos_str($assuntos)
{
	$assuntos_array = array();
	
	foreach ($assuntos as $assunto) {
		array_push($assuntos_array, $assunto['ass_nome']);
	}
	
	return implode(', ', $assuntos_array);
}

function get_resposta_questao($questao, $resposta, $correta = '** Resposta Correta **', $errada = '** Resposta Errada **', $anulada = NULL)
{

	if($anulada && $questao['que_status'] == ANULADA) {
		return "<div>$anulada</div>";
	}
	else {
		if($resposta == $questao['que_resposta']) {
			return "<div>$correta</div>";
		}
		else {
			return "<div>$errada</div>";		
		}
	}
	
}
?>