<?php
function salvar_log_erro($data) 
{
	global $wpdb;
	$wpdb->insert("log_erros_acesso", $data);
}

function listar_log_erros($data_inicio, $data_fim)
{
	global $wpdb;
	
	$sql = "SELECT * FROM log_erros_acesso WHERE lea_data_hora >= '{$data_inicio}'  
			AND lea_data_hora <= '{$data_fim}' ORDER BY lea_data_hora DESC";

	return $wpdb->get_results($sql);
}