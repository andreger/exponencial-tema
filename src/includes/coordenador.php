<?php
/*********************************************************************************
 * COORDENADOR SQ
 *********************************************************************************/

function salvar_percentual_coordenador_sq($usuario_id, $percentual)
{
	global $wpdb;

	if($percentual_atual = get_percentual_atual_coordenador_sq($usuario_id)) {

		// se o percentual tiver sido setado no mesmo dia do antigo, atualizar o antigo 
		if($percentual_atual->data_inicio == date('Y-m-d')) {
			$wpdb->update('coordenadores_sq', array(
				'percentual' => $percentual,
			), array(
				'user_id' => $usuario_id,
				'data_inicio' => date('Y-m-d'),
			));
		}
		// senão criar um novo valor e invalidar o antigo
		else {

			// invalida antigo
			$wpdb->update('coordenadores_sq', array(
				'data_fim' => date('Y-m-d')
			), array(
				'user_id' => $usuario_id,
				'data_inicio' => $percentual_atual->data_inicio
			));

			// cria novo
			$wpdb->insert('coordenadores_sq', array(
				'user_id' => $usuario_id,
				'percentual' => $percentual,
				'data_inicio' => date('Y-m-d')
			));
		}
	}

	// se não existir percentual atual, criar o primeiro
	else {

		$wpdb->insert('coordenadores_sq', array(
			'user_id' => $usuario_id,
			'percentual' => $percentual,
			'data_inicio' => date('Y-m-d')
		));
	}

	wp_update_user(array(
		'ID' => $usuario_id,
		'role' => 'coordenador'
	));
}

function remover_coordenador_sq($usuario_id)
{
	global $wpdb;

	$atual = get_percentual_atual_coordenador_sq($usuario_id);

	if($atual) {

		if($atual->data_inicio == date('Y-m-d')) {
			$wpdb->delete('coordenadores_sq', array(
				'user_id' => $atual->user_id,
				'data_inicio' => $atual->data_inicio
			));
		}
		else {
			$wpdb->update('coordenadores_sq', array(
				'data_fim' => date('Y-m-d')
			), array(
				'user_id' => $usuario_id,
				'data_fim' => NULL
			));
		}
	}

	atualizar_role_para_author($usuario_id);
}

function get_percentual_atual_coordenador_sq($usuario_id)
{
	global $wpdb;

	$sql = "SELECT * FROM coordenadores_sq WHERE user_id = {$usuario_id} AND data_fim IS NULL";

	return $wpdb->get_row($sql);
}

function get_valor_percentual_por_data_coordenador_sq($data, $usuario_id = null)
{
	global $wpdb;

	$usuario_frag =  $usuario_id ? "user_id = {$usuario_id} AND " : "";

	$sql = "SELECT SUM(percentual) AS soma FROM coordenadores_sq WHERE {$usuario_frag} data_inicio <= '{$data}' AND (data_fim IS NULL OR data_fim > '{$data}')";

	$resultado = $wpdb->get_row($sql);

	return $resultado ? $resultado->soma : 0;
}

function get_percentual_por_data_coordernador_sq($usuario_id, $data)
{
	global $wpdb;

	$sql = "SELECT * FROM coordenadores_sq WHERE user_id = {$usuario_id} AND 
		data_inicio <= '{$data}' AND (data_fim IS NULL OR data_fim > '{$data}')";

	return $wpdb->get_row($sql);
}

function listar_coordenadores_sq_ativos()
{
	global $wpdb;

	$sql = "SELECT * FROM coordenadores_sq JOIN wp_users ON user_id = ID WHERE data_fim IS NULL ORDER BY display_name";

	return $wpdb->get_results($sql);
}

function is_coordenador_sq_ativo($usuario_id = NULL)
{
	global $wpdb;

	if(!$usuario_id) {
		$usuario_id = get_current_user_id();
	}

	$sql = "SELECT * FROM coordenadores_sq WHERE data_fim IS NULL AND user_id = {$usuario_id}";

	return $wpdb->get_results($sql) ? TRUE : FALSE;
}

function listar_coordenadores_sq_por_data($data)
{
	global $wpdb;

	$sql = "SELECT * FROM coordenadores_sq JOIN wp_users ON user_id = ID WHERE 
		data_inicio <= '{$data}' AND (data_fim IS NULL OR data_fim > '{$data}') ORDER BY display_name";

	return $wpdb->get_results($sql);
}

function get_coordenador_sq_historico($usuario_id)
{
	global $wpdb;

	$sql = "SELECT * FROM coordenadores_sq WHERE user_id = $usuario_id ORDER BY data_inicio DESC";

	return $wpdb->get_results($sql);
}

/*********************************************************************************
 * COORDENADOR AREAS
 *********************************************************************************/
function remover_coordenador_area($usuario_id, $area_id)
{
	global $wpdb;

	$atual = get_percentual_atual_coordenador_area($usuario_id, $area_id);

	if($atual) {


		if($atual->data_inicio == date('Y-m-d')) {
			$wpdb->delete('coordenadores_areas', array(
				'user_id' => $atual->user_id,
				'area_id' => $atual->area_id,
				'data_inicio' => $atual->data_inicio
			));
		}
		else {

			$wpdb->update('coordenadores_areas', array(
				'data_fim' => date('Y-m-d')
			), array(
				'user_id' => $usuario_id,
				'area_id' => $area_id,
				'data_fim' => NULL
			));
		}
	}

	atualizar_role_para_author($usuario_id);

}

function get_percentual_atual_coordenador_area($usuario_id, $area_id)
{
	global $wpdb;

	$sql = "SELECT * FROM coordenadores_areas WHERE user_id = {$usuario_id} AND area_id = {$area_id} AND data_fim IS NULL";

	return $wpdb->get_row($sql);
}

function get_valor_percentual_por_data_coordenador_area($data, $area_id, $usuario_id = null)
{
	global $wpdb;

	$usuario_frag =  $usuario_id ? "user_id = {$usuario_id} AND " : "";

	$sql = "SELECT SUM(percentual) AS soma FROM coordenadores_areas WHERE {$usuario_frag} area_id = {$area_id} AND data_inicio <= '{$data}' AND (data_fim IS NULL OR data_fim > '{$data}')";

	$resultado = $wpdb->get_row($sql);

	return $resultado->soma;
}

function listar_areas_coordenacao()
{
	$args = array ('taxonomy' => 'product_cat','child_of' => CATEGORIA_AREA, 'hide_empty' => false); // ID da categoria pai AREA
	return get_categories( $args );
}


function listar_areas_coordenacao_combo_options()
{
	$areas = listar_areas_coordenacao();

	$lista = array();
	$lista[0] = "Selecione uma área";
	foreach ($areas as $area) {
		$lista[$area->term_id] = $area->name;
	}

	return $lista;
}

function listar_coordenadores_areas_ativos()
{
	global $wpdb;

	$sql = "SELECT * FROM coordenadores_areas JOIN wp_users ON user_id = ID WHERE data_fim IS NULL ORDER BY display_name";

	return $wpdb->get_results($sql);
}

function is_coordenador_area_ativo($usuario_id = NULL)
{
	global $wpdb;

	if(!$usuario_id) {
		$usuario_id = get_current_user_id();
	}

	$sql = "SELECT * FROM coordenadores_areas WHERE data_fim IS NULL AND user_id = {$usuario_id}";

	return $wpdb->get_results($sql) ? TRUE : FALSE;
}

function listar_coordenadores_areas_por_data($data, $area_id)
{
	global $wpdb;

	$sql = "SELECT * FROM coordenadores_areas WHERE area_id = {$area_id} AND data_inicio <= '{$data}' AND (data_fim IS NULL OR data_fim > '{$data}')";

	return $wpdb->get_results($sql);
}

function salvar_percentual_coordenador_area($usuario_id, $area_id, $percentual)
{
	global $wpdb;

	if($percentual_atual = get_percentual_atual_coordenador_area($usuario_id, $area_id)) {

		// se o percentual tiver sido setado no mesmo dia do antigo, atualizar o antigo 
		if($percentual_atual->data_inicio == date('Y-m-d')) {
			$wpdb->update('coordenadores_areas', array(
				'percentual' => $percentual
			), array(
				'user_id' => $usuario_id,
				'area_id' => $area_id,
				'data_inicio' => date('Y-m-d')
			));
		}
		// senão criar um novo valor e invalidar o antigo
		else {

			// invalida antigo
			$wpdb->update('coordenadores_areas', array(
				'data_fim' => date('Y-m-d')
			), array(
				'user_id' => $usuario_id,
				'area_id' => $area_id,
				'data_inicio' => $percentual_atual->data_inicio
			));

			// cria novo
			$wpdb->insert('coordenadores_areas', array(
				'user_id' => $usuario_id,
				'area_id' => $area_id,
				'percentual' => $percentual,
				'data_inicio' => date('Y-m-d')
			));
		}
	}

	// se não existir percentual atual, criar o primeiro
	else {
		$wpdb->insert('coordenadores_areas', array(
			'user_id' => $usuario_id,
			'area_id' => $area_id,
			'percentual' => $percentual,
			'data_inicio' => date('Y-m-d')
		));

	}
	
	atualizar_role_para_coordenador($usuario_id);
}

function get_coordenador_area_historico($usuario_id, $area_id)
{
	global $wpdb;

	$sql = "SELECT * FROM coordenadores_areas WHERE user_id = $usuario_id AND area_id = $area_id ORDER BY data_inicio DESC";

	return $wpdb->get_results($sql);
}

function get_percentual_coordenadores($produto_id, $data)
{
	$usuario_id = NULL;
	if(tem_acesso(array(ADMINISTRADOR))) {
		$usuario_id = NULL;
	}
	elseif(tem_acesso(array(COORDENADOR))) {
		$usuario_id = get_current_user_id();
	}
	else {
		return 0;
	}


	if(is_produto_assinatura($produto_id) || is_produto_simulado($produto_id) || is_produto_caderno($produto_id)) {
		return get_valor_percentual_por_data_coordenador_sq($data, $usuario_id);
	}

	else {

		$area_id = get_produto_area_coordenacao($produto_id);
		return $area_id ? get_valor_percentual_por_data_coordenador_area($data, $area_id, $usuario_id) : 0;
	}

	return 0;
}

function get_produto_area_coordenacao($produto_id)
{
	$categorias = listar_categorias($produto_id);

	if($categorias) {

		foreach ($categorias as $categoria) {
			if($categoria->parent == CATEGORIA_AREA) {
				return $categoria->term_id;
			}
		}

	}

	return NULL;
}

/*********************************************************************************
 * COORDENADOR COACHING
 *********************************************************************************/

function get_valor_percentual_por_data_coordenador_coaching($data, $usuario_id = null)
{
	global $wpdb;

	$usuario_frag =  $usuario_id ? "user_id = {$usuario_id} AND " : "";

	$sql = "SELECT SUM(percentual) AS soma FROM coordenadores_coaching WHERE {$usuario_frag} data_inicio <= '{$data}' AND (data_fim IS NULL OR data_fim > '{$data}')";

	$resultado = $wpdb->get_row($sql);

	return $resultado ? $resultado->soma : 0;
}

function listar_coordenadores_coaching_por_data($data)
{
	global $wpdb;

	$sql = "SELECT * FROM coordenadores_coaching JOIN wp_users ON user_id = ID WHERE 
		data_inicio <= '{$data}' AND (data_fim IS NULL OR data_fim > '{$data}') ORDER BY display_name";

	return $wpdb->get_results($sql);
}

function salvar_coordenador_coaching($usuario_id, $percentual)
{
	global $wpdb;

	if($percentual_atual = get_percentual_atual_coordenador_coaching($usuario_id)) {

		// se o percentual tiver sido setado no mesmo dia do antigo, atualizar o antigo 
		if($percentual_atual->data_inicio == date('Y-m-d')) {
			$wpdb->update('coordenadores_coaching', array(
				'percentual' => $percentual,
			), array(
				'user_id' => $usuario_id,
				'data_inicio' => date('Y-m-d'),
			));
		}
		// senão criar um novo valor e invalidar o antigo
		else {

			// invalida antigo
			$wpdb->update('coordenadores_coaching', array(
				'data_fim' => date('Y-m-d')
			), array(
				'user_id' => $usuario_id,
				'data_inicio' => $percentual_atual->data_inicio
			));

			// cria novo
			$wpdb->insert('coordenadores_coaching', array(
				'user_id' => $usuario_id,
				'percentual' => $percentual,
				'data_inicio' => date('Y-m-d')
			));
		}
	}

	// se não existir percentual atual, criar o primeiro
	else {

		$wpdb->insert('coordenadores_coaching', array(
			'user_id' => $usuario_id,
			'percentual' => $percentual,
			'data_inicio' => date('Y-m-d')
		));
	}

	atualizar_role_para_coordenador($usuario_id);
}

function get_percentual_atual_coordenador_coaching($usuario_id)
{
	global $wpdb;

	$sql = "SELECT * FROM coordenadores_coaching WHERE user_id = {$usuario_id} AND data_fim IS NULL";

	return $wpdb->get_row($sql);
}

function get_coordernador_coaching($usuario_id)
{
	global $wpdb;

	$sql = "SELECT * FROM coordenadores_coaching WHERE user_id = $usuario_id";

	return $wpdb->get_results($sql);
}


function remover_coordenador_coaching($usuario_id)
{

	global $wpdb;

	$atual = get_percentual_atual_coordenador_coaching($usuario_id);

	if($atual) {

		if($atual->data_inicio == date('Y-m-d')) {
			$wpdb->delete('coordenadores_coaching', array(
				'user_id' => $atual->user_id,
				'data_inicio' => $atual->data_inicio
			));
		}
		else {
			$wpdb->update('coordenadores_coaching', array(
				'data_fim' => date('Y-m-d')
			), array(
				'user_id' => $usuario_id,
				'data_fim' => NULL
			));
		}
	}

	atualizar_role_para_author($usuario_id);
	
}

function get_coordenador_coaching_historico($usuario_id)
{
	global $wpdb;

	$sql = "SELECT * FROM coordenadores_coaching WHERE user_id = $usuario_id ORDER BY data_inicio DESC";

	return $wpdb->get_results($sql);
}

function atualizar_role_para_author($usuario_id)
{
	if(!is_coordenador_sq_ativo($usuario_id) && !is_coordenador_area_ativo($usuario_id) && !is_coordenador_coaching($usuario_id)) {
	
		$u = new WP_User($usuario_id);
		$u->remove_role('coordenador');
		$u->add_role('author');
	}
}

function atualizar_role_para_coordenador($usuario_id)
{
	$u = new WP_User($usuario_id);
	$u->remove_role('author');
	$u->add_role('coordenador');
}

function listar_coordenadores_coaching()
{
	global $wpdb;

	$sql = "SELECT * FROM coordenadores_coaching JOIN wp_users ON user_id = ID ORDER BY display_name";

	return $wpdb->get_results($sql);
}

function listar_coordenadores_coaching_ativos()
{
	global $wpdb;

	$sql = "SELECT * FROM coordenadores_coaching JOIN wp_users ON user_id = ID WHERE data_fim IS NULL ORDER BY display_name";

	return $wpdb->get_results($sql);
}

function listar_coordenadores()
{
	global $wpdb;

	$sql = "(SELECT ID, display_name FROM coordenadores_sq JOIN wp_users ON user_id = ID GROUP BY ID) UNION DISTINCT (SELECT ID, display_name FROM coordenadores_areas JOIN wp_users ON user_id = ID GROUP BY ID)  UNION DISTINCT (SELECT ID, display_name FROM coordenadores_coaching JOIN wp_users ON user_id = ID GROUP BY ID) 
		ORDER BY display_name";

	return $wpdb->get_results($sql);
}