<?php

use \setasign\Fpdi\Fpdi;

//faz um tratamento para considerar pdf bloqueado
function assinar_pdf_tratamento($file_path, $output = 'D')
{
	set_time_limit(600);
	$url_array = parse_url($file_path);
	
	$nome_original = get_nome_arquivo($url_array['path'], false);
	$extensao = get_extensao($url_array['path']);

	$dir =  'wp-content/temp/';
	$nome = $dir . sanitize_title($nome_original) . '-' . md5(time()) . '.' . $extensao;
	$name = $_SERVER['DOCUMENT_ROOT'] . '/' . $nome;
	$temp_folder = $_SERVER['DOCUMENT_ROOT'] . '/' . $dir;
	
	if (!file_exists($temp_folder)) {
		mkdir($temp_folder, 0777, true);
	}

	$caminho_arq = $_SERVER['DOCUMENT_ROOT'] . $url_array['path'];
	if( file_exists($caminho_arq) ){

		copy($caminho_arq, $name);

		if(!is_pdf($caminho_arq)) {
			return $name;
		}

		$url_array = explode('/', $url_array['path']);
		$filename = remove_accents(array_pop($url_array));
		
		require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-content/pdftools/fpdf/fpdf.php';
		require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-content/pdftools/fpdi/src/autoload.php';
		require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-content/pdftools/FPDI_PDF-Parser/src/autoload.php';
		require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-content/pdftools/fpdi-protection/src/autoload.php';
		require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-content/pdftools/rfpdi/src/rfpdi.php';
		
		try {
			$file = new FPDI();
			$file->setSourceFile($name);
				
			$pdf = new RFPDI();
			// $pdf->SetProtection(array('print','annot-forms'));
			$pdf->SetFont('Helvetica');
			$pdf->SetFontSize('9');
			// 		$pdf->SetTextColor(47, 63, 96); //dark blue
			$pdf->SetTextColor(160, 160, 160); //gray
				
			$current_user = wp_get_current_user();
			$text1 = 'Cópia registrada para '. $current_user->display_name . ' (CPF: ' . get_user_meta($current_user->ID, 'oneTarek_CPF', true) .')';
			$text1 =  stripslashes($text1);
			$text1 = iconv('UTF-8', 'windows-1252', $text1);
			$stringWidth1 = $pdf->GetStringWidth($text1);
				
			$text2 = 'Direitos autorais reservados (Lei 9610/98). Proibida a reprodução, venda ou compartilhamento deste arquivo. Uso individual.';
			$text2 =  stripslashes($text2);
			$text2 = iconv('UTF-8', 'windows-1252', $text2);
			$stringWidth2 = $pdf->GetStringWidth($text2);
		
			$pagesCount = $pdf->setSourceFile($name);
		
			for ($i = 1; $i <= $pagesCount; $i++) {
				$tplIdxFile = $file->importPage($i);

				$size = $file->getTemplateSize($tplIdxFile);
		
				$pdf->AddPage($size['orientation'],  array($size['width'], $size['height']));
				
				$tplIdx = $pdf->importPage($i);
				$pdf->useTemplate($tplIdx);
		
				$position1 = $size['height'] - (($size['height'] - $stringWidth1) / 2);
				$position2 = $size['height'] - (($size['height'] - $stringWidth2) / 2);
		
				$pdf->TextWithRotation(10, $position1, $text1, 90);
				$pdf->TextWithRotation(5, $position2, $text2, 90);
			}

			$file->Close();
			$pdf->Output($name,'F'); 
			$pdf->Close();

			return $name;
		} catch (Exception $e) {
			return $name;
		}
			
	}
}



function download_all_pdf_produto_zip($arquivos, $titulo)
{
	$dir =  '/wp-content/temp/';
	$filename = $dir . $titulo . '.zip';
	$path = $_SERVER['DOCUMENT_ROOT'] . $filename;
	
	$files = array();
    
	log_zip("INFO", "Iniciando assinaturas: " . $path);
	foreach ($arquivos as $arq){
		$files[] = assinar_pdf_tratamento($arq);	
	}

	log_zip("INFO", "Iniciando zip: " . $path);
	if( create_zip($files, $path, true) ){
	    return $filename;
	}
	else {
	    /**
	     * @todo Tratar erro. Detectado: pasta temp não existia
	     */
	}
}


function create_zip($files = array(), $destination = '',$overwrite = false) 
{
    if(file_exists($destination) && !$overwrite) { return false; }

	$valid_files = array();

	if(is_array($files)) {

		foreach($files as $file) {

			if(file_exists($file)) {
				$valid_files[] = $file;
			}
		}
	}

	if(count($valid_files)) {

		$zip = new ZipArchive();

		$res = $zip->open($destination, ZIPARCHIVE::CREATE | ZIPARCHIVE::OVERWRITE);
		if($res !== TRUE) {
		    log_debug("ERROR", "Erro abrindo zip: " . $destination);
			return false;
		}

		foreach($valid_files as $file) {
			$zip->addFile($file,basename($file));
		}
		
		log_debug("INFO", "Zip criado: " . $destination);
		$zip->close();

		//check to make sure the file exists
		return file_exists($destination);
	}
	else
	{
	    log_debug("INFO", "Nao existe arquivo valido: " . $destination);
		return false;
	}
}