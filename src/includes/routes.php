<?php

use Exponencial\Core\Controllers\Footer\CadastroGratisController;
use Exponencial\Core\Controllers\Header\CarrinhoController;

add_action('init', 'add_rewrite_rules');
add_filter('query_vars', 'add_rewrite_query_vars');

function add_rewrite_rules()
{
	add_rewrite_rule(
			'eventos/([^/]+)/?',
			'index.php?pagename=landingpage&nome=$matches[1]',
			'top' );

	add_rewrite_rule(
			'concurso/([^/]+)/?',
			'index.php?pagename=concurso&nome=$matches[1]',
			'top' );

	add_rewrite_rule(
			'professor/([^/]+)/?',
			'index.php?pagename=site-do-professor&nome=$matches[1]',
			'top' );

	add_rewrite_rule(
			'cursos-por-professor/([^/]+)/?',
			'index.php?pagename=cursos-por-professor-especifico&nome=$matches[1]',
			'top' );

	add_rewrite_rule(
			'cursos-por-materia/([^/]+)/?',
			'index.php?pagename=cursos-por-materia-especifica&cat_name=$matches[1]',
			'top' );

	add_rewrite_rule(
			'cursos-por-concurso/([^/]+)/?',
			'index.php?pagename=cursos-por-concurso-especifico&con_name=$matches[1]',
			'top' );

	add_rewrite_rule(
			'artigos-por-professor/([^/]+)/?',
			'index.php?pagename=artigos-por-professor&nome=$matches[1]',
			'top' );

	add_rewrite_rule(
		'depoimento/([^/]+)/?',
		'index.php?pagename=depoimento&nome=$matches[1]',
		'top' );

	add_rewrite_rule(
		'entrevista/([^/]+)/?',
		'index.php?pagename=entrevista&nome=$matches[1]',
		'top' );
	
	add_rewrite_rule(
		'kcore/xhr/([^/]+)/?',
		'index.php?pagename=kcore&metodo=$matches[1]',
		'top' );

    adicionarRotaController('header', CarrinhoController::class);
	adicionarRotaController('cadastro-gratis', CadastroGratisController::class);

//	flush_rewrite_rules(true);
}

function adicionarRotaController(string $slug, string $controller)
{
    add_rewrite_rule(
        "{$slug}/([A-Za-z0-9-_/]+)/?",
        "index.php?pagename=controller&controller={$controller}&controller_params=\$matches[1]",
        'top' );
}

function add_rewrite_query_vars($public_query_vars) 
{
	$public_query_vars[] = "nome";
	$public_query_vars[] = "cat_name";
	$public_query_vars[] = "con_name";
    $public_query_vars[] = "controller";
	$public_query_vars[] = "controller_params";
	$public_query_vars[] = "metodo";
	return $public_query_vars;
}