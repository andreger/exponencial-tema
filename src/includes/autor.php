<?php 
function get_autor_link($author, $class = 'author', $incluir_titulo = false) {
	$nome_professor = get_autor_str($author, $incluir_titulo);
	$url = get_autor_url($author->ID);
	
	return "<a class='{$class}' href='{$url}'>" . $nome_professor . "</a>";
}

/**
 * @deprecated K4
 * Utilizar UrlHelper::get_professor_url($professor)
 */ 

function get_autor_url($author_id)
{
	$slug = get_user_meta($author_id, "slug", TRUE);
	return "/professor/{$slug}";
}

function get_cursos_por_autor_url($author_id)
{
	$slug = get_user_meta($author_id, "slug", TRUE);
	return "/cursos-por-professor/{$slug}";
}

function get_autor_str($author, $incluir_titulo = false) {
	$titulo = $incluir_titulo ? 'Prof. ' : '';
	
	return "{$titulo}{$author->first_name} {$author->last_name}";
}

function get_professores($incluir_ocultos = TRUE) {
	global $wpdb;

	$transientKey = TRANSIENT_PROFESSORES . "_" . (int)$incluir_ocultos;
	$professores = get_transient($transientKey);

	if(!$professores) {

        $sqlOcultos = $incluir_ocultos == false ? " AND col_oculto = 0 " : "";

        $professores = $wpdb->get_results(
            "SELECT user_id AS ID, display_name, col_slug AS user_nicename, col_oculto FROM colaboradores c JOIN wp_users u ON u.ID = c.user_id WHERE col_tipo IN (1,3) {$sqlOcultos} ORDER BY display_name");

        set_transient($transientKey, $professores, 60 * MINUTE_IN_SECONDS);
    }




//	if(!$incluir_ocultos) {
//		$temp = $professores;
//
//		$professores = array();
//
//		foreach ($temp as $item) {
//
//			if(is_professor_oculto($item->ID)) {
//				continue;
//			}
//
//			array_push($professores, $item);
//		}
//	}

	return $professores;
}

function get_professores_array() {
	$professores = get_professores(FALSE);

	$professores_array = array();
	foreach ($professores as $professor) {
		array_push($professores_array, get_usuario_array($professor->ID));
	}

	return $professores_array;
}

function get_professores_combo_options($label = "Todos os professores", $value = 0, $incluir_ocultos = TRUE) {
	$professores = get_professores($incluir_ocultos);

	$professores_array[$value] = $label;
	foreach ($professores as $professor) {
		$professores_array[$professor->ID] = $professor->display_name;
	}

	return $professores_array;
}

function get_coordenadores_combo_options($label = "Todos os coordenadores", $value = 0) {
	$coordenadores = listar_coordenadores();

	$coordenadores_array[$value] = $label;
	foreach ($coordenadores as $coordenador) {
		$coordenadores_array[$coordenador->ID] = $coordenador->display_name;
	}

	return $coordenadores_array;
}

function get_consultores($incluir_ocultos = TRUE) {
	global $wpdb;

    $sqlOcultos = $incluir_ocultos == false ? " AND col_oculto = 0 " : "";

	$consultores = $wpdb->get_results(
			"SELECT user_id AS ID, display_name, col_slug AS user_nicename FROM colaboradores c JOIN wp_users u ON u.ID = c.user_id WHERE col_tipo IN (2,3) {$sqlOcultos} ORDER BY display_name");

//	if(!$incluir_ocultos) {
//		$temp = $consultores;
//
//		$consultores = array();
//
//		foreach ($temp as $item) {
//
//			if(is_professor_oculto($item->ID)) {
//				continue;
//			}
//
//			array_push($consultores, $item);
//		}
//
//	}

    return $consultores;
}


function get_consultores_array($incluir_ocultos = TRUE) {
	$consultores = get_consultores($incluir_ocultos);
	
	$consultores_array = array();
	foreach ($consultores as $consultor) {
		array_push($consultores_array, get_usuario_array($consultor->ID));
	}
	
	return $consultores_array;
}

function is_professor_oculto($usuario_id)
{
    global $wpdb;

    $sql = "SELECT * FROM colaboradores c WHERE user_id = $usuario_id AND col_oculto = 1";
    return $wpdb->get_row($sql) ? true : false;

    //	$oculto = get_user_meta($usuario_id, 'ocultar', TRUE);
    //  return isset($oculto) && $oculto ? TRUE : FALSE;
}

function listar_professores_exceto($ids)
{
	global $wpdb;

	$ids = array_diff(array_unique($ids), array(''));

	$ids_query = "(" . implode(',', $ids) . ")";

//	$sql = "SELECT ID,display_name,user_nicename FROM wp_users u LEFT JOIN wp_usermeta um ON um.user_id = u.ID WHERE um.meta_key LIKE 'oneTarek_author_type' AND (um.meta_value = '1' OR um.meta_value = '3') AND ID NOT IN {$ids_query} ORDER BY display_name";
	$sql = "SELECT user_id AS ID, display_name, col_slug AS user_nicename FROM colaboradores c JOIN wp_users u ON u.ID = c.user_id WHERE col_tipo IN (1,3) AND user_id NOT IN {$ids_query} ORDER BY display_name";

	return $wpdb->get_results($sql);			
}

/**
 * Retorna o número de questões comentadas por um determinado usuário
 * 
 * @param int $usuario_id Id do usuário
 * @param bool $somente_destaque Flag para contar somente destaques ou todos
 * 
 * @return int Contagem de comentários do usuário
 */

function get_num_questoes_comentadas($usuario_id)
{
	$wpdb_corp = new wpdb(DB_USER_CORP, DB_PASSWORD_CORP, DB_NAME_CORP, DB_HOST_CORP);

	$sql = "SELECT sum(qcp_qtde_comentada) as qtde from questoes_comentadas_professor where user_id = {$usuario_id} group by user_id";

	
	$resultado = $wpdb_corp->get_row($sql);

	return $resultado ? $resultado->qtde : 0;		
}

/**
 * Lista as mídias de um autor
 * 
 * @param int $usuario_id Id do usuário
 * @param int $midia Tipo da mídia [Artigos, Notícias, Vídeos]
 * @param int $limit Número de mídias que serão retornadas
 * @param string $order_by Campo da ordenação
 * @param string $order ASC ou DESC
 * 
 * @return array Lista de WP_Posts
 */

function listar_midias_por_autor($usuario_id, $midia, $limit = -1, $order_by = 'post_date', $order = 'DESC') 
{
	$args = array(
	  'category_name' => $midia,
	  'author'        =>  $usuario_id,
	  'orderby'       =>  $order_by,
	  'order'         =>  $order,
	  'posts_per_page' => $limit
	);

 	return get_posts($args);
}

/**
 * Lista todos os professores e consultores
 * 
 * @since K4
 * 
 * @deprecated Remover em L1
 * 
 * @return array Lista de WP_Posts
 */

function listar_professores_e_consultores() {
	global $wpdb;

	return $wpdb->get_results(
			"SELECT ID,display_name,user_nicename FROM wp_users u LEFT JOIN wp_usermeta um ON um.user_id = u.ID ".
			"WHERE um.meta_key LIKE 'oneTarek_author_type' AND um.meta_value IN ('1', '2', '3') ORDER BY display_name", ARRAY_A);
}
