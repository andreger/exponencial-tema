<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/corporativo/usuario_lucro.php';

function get_valor_lucro_atual($usuario_id) {
	$usuarioLucro = UsuarioLucro::get_valor_atual($usuario_id);
	return $usuarioLucro['ul_valor'];
}

function salvar_valor_lucro($usuario_id, $valor) {
	$usuarioLucro = array('usu_id' => $usuario_id, 'ul_valor' => $valor);
	UsuarioLucro::salvar($usuarioLucro);
}

function get_lucro_por_data($usuario_id, $data) {
	$usuarioLucro = UsuarioLucro::get_lucro_por_data($usuario_id, $data);
	if(empty($usuarioLucro))
		return 0;
	
	return $usuarioLucro['ul_valor'];
}

function redirecionar_se_nao_estiver_logado($url = '/cadastro-login/') {
	
	if(!is_user_logged_in()) {
		header('Location: /acesso-restrito/?uri=' . $_SERVER['REQUEST_URI']);
		exit;
	}
	return false;
}

function redirecionar_se_nao_for_administrador() {
	if(!redirecionar_se_nao_estiver_logado()) {
		if(!current_user_can('administrator')) {
			header('Location: /acesso-negado');
			exit;
		}
	}
	return false;
}


function redirecionar_para_acesso_negado_simulado($negar_para_aluno_podio = false)
{
	if(!redirecionar_se_nao_estiver_logado()) {
		// considera somente alunos
		if(is_aluno()) {
			// se for negar para alunos do podio, já redireciona
			if($negar_para_aluno_podio) {
				header('Location: /acesso-negado-ao-sistema-de-questoes');
				exit;
			}
			else {
				// redireciona só se não for aluno ativo do podio
				if(!is_aluno_ativo_podio()) {
					header('Location: /acesso-negado-ao-sistema-de-questoes');
					exit;
				}
			}
		}
	}
}

function redirecionar_para_acesso_negado()
{
	if(!redirecionar_se_nao_estiver_logado()) {
		header('Location: /acesso-negado');
		exit;
	}
}

function redirecionar_para_acesso_negado_sq()
{
	if(!redirecionar_se_nao_estiver_logado()) {
		header('Location: /acesso-negado-ao-sistema-de-questoes');
		exit;
	}
}

function get_id_usuario_atual() {
	return get_current_user_id();
}

function get_usuario_nome_exibicao($usuario_id)
{
	$usuario = get_usuario_array($usuario_id);
	return $usuario['mostrar_apelido'] ? $usuario['apelido'] : $usuario['nome_completo'];
}

function get_usuario_nome_completo($usuario_id)
{
	$usuario = get_usuario_array($usuario_id);
	return $usuario['nome_completo'];
}

function get_usuario_email($usuario_id){
	$usuario = get_usuario_array($usuario_id);
	return $usuario['email'];
}

function get_usuario($usuario_id) {
	return get_userdata($usuario_id);
}

function get_usuario_by_email($email) 
{
	return get_user_by('email', $email);	
}

function ordenar_usuarios_arrays($usuarios_arrays)
{
	usort($usuarios_arrays, 'cmp_nome_usuarios');
	return $usuarios_arrays;
}

function cmp_nome_usuarios($a, $b)
{
	return strcmp($a['nome_completo'], $b['nome_completo']);
}

function get_usuario_array($usuario_id = null)
{
	if(is_null($usuario_id)) {
		$usuario_id = get_current_user_id();
	}
	
	$u = get_usuario($usuario_id);

	$nome_completo = $u->first_name . ' ' . $u->last_name;
	
	$usuario = array(
		'id' => $usuario_id,
		'login' => $u->user_login,
		'email' => $u->user_email,
		'nome' => $u->first_name,
		'sobrenome' => $u->last_name,
		'nome_completo' => trim($nome_completo) ? $nome_completo : $u->display_name,
		'cep' => get_user_meta($usuario_id, 'oneTarek_CEP', true),
		'endereco' => get_user_meta($usuario_id, 'endereco_1', true),
		'complemento' => get_user_meta($usuario_id, 'endereco_2', true),
		'bairro' => get_user_meta($usuario_id, 'oneTarek_nhb', true),
		'cidade_nome' => get_user_meta($usuario_id, 'cidade_nome', true),
		'cidade' => get_user_meta($usuario_id, 'cidade', true),
		'estado' => get_user_meta($usuario_id, 'estado', true),			
		'uf' => get_uf(get_user_meta($usuario_id, 'estado', true)),		
		'telefone' => get_user_meta($usuario_id, 'telefone', true),
		'nome_mae' => get_user_meta($usuario_id, 'oneTarek_mother_name', true),
		'cpf' => get_user_meta($usuario_id, 'oneTarek_CPF', true),
		'data_nascimento' => get_user_meta($usuario_id, 'oneTarek_date', true),
		'apelido' => get_user_meta($usuario_id, 'apelido', true),
		'mostrar_apelido' => get_user_meta($usuario_id, 'mostrar_apelido', true),
		'codigo_desconto' => get_user_meta($usuario_id, 'codigo_desconto', true),
		'slug' => get_user_meta($usuario_id, 'slug', true),
	);	

	// adiciona informações caso usuário seja professor
	if(is_professor($usuario_id)) {
		$usuario['is_professor'] = true;
		$usuario['cargo'] = get_user_meta($usuario_id,'cargo', true);
		$usuario['avatar'] = get_avatar($usuario_id, 365);
		$usuario['descricao'] = nl2br(get_user_meta($usuario_id,'description', true)); 
	}
	else {
		$usuario['is_professor'] = false;
	}

	return $usuario;
}

function usuario_tem_email($usuario_id)
{
	$usuario = get_usuario_array($usuario_id);
	return $usuario['email'] ? true : false;
}

function criar_usuario($email, $senha, $nome)
{
	$nome_array = explode(' ', $nome, 2 );
	
	$userdata = array (
			'user_login' => $email,
			'user_email' => $email,
			'user_pass' => $senha,
			'role' => 'Customer',
			'display_name' => $nome,
			'first_name' => $nome_array[0],
			'last_name' => $nome_array[1]
	);
	
	return wp_insert_user($userdata);
}

function dados_usuario_completos($usuario_id, $perfil_basico = false)
{
	$usuario = get_usuario_array($usuario_id);

	if(!$usuario['email']) return false;
	if(!$usuario['nome']) return false;
	if(!$usuario['sobrenome']) return false;
	if(!$usuario['data_nascimento']) return false;

	if(!$perfil_basico) {
		if(!$usuario['cidade']) return false;
		if(!$usuario['estado']) return false;
		if(!$usuario['cep']) return false;
		if(!$usuario['endereco']) return false;
		if(!$usuario['cpf']) return false;
		if(!$usuario['telefone']) return false;
		if(!validaCPF($usuario['cpf'])) return false;

		if($usuario['telefone'] && !preg_match(REGEX_TELEFONE, $usuario['telefone'])) {
			return false;
		}
	}

	return true;
}

function formatar_telefone($telefone)
{
	$telefone = preg_replace("/[^0-9]/", "", $telefone);

	if(strlen($telefone) == 10) {
		$ddd = substr($telefone, 0, 2);
		$pre = substr($telefone, 2, 4);
		$pos = substr($telefone, 6, 4);
	}
	else if(strlen($telefone) == 11) {
		$ddd = substr($telefone, 0, 2);
		$pre = substr($telefone, 2, 5);
		$pos = substr($telefone, 7, 4);
	}
	else {
		return "";
	}

	return "($ddd) $pre-$pos";
}

function atualizar_usuario($usuario)
{
	$tem_email = usuario_tem_email($usuario['id']);
	
	$userdata = array(
		'ID' => $usuario['id'],
		'first_name' => $usuario['nome'],
		'last_name' => $usuario['sobrenome'],
		'display_name' => $usuario['nome_completo'],
		'user_email' => $usuario['email']
	);
	wp_update_user($userdata);

	$cidade_row = get_cidade_por_codigo_ibge($usuario['cidade']);

	update_user_meta($usuario['id'], 'oneTarek_CEP', $usuario['cep']);
	update_user_meta($usuario['id'], 'billing_first_name', $usuario['nome']);
	update_user_meta($usuario['id'], 'billing_last_name', $usuario['sobrenome']);

	update_user_meta($usuario['id'], 'endereco_1', $usuario['endereco']);
	update_user_meta($usuario['id'], 'billing_address_1', $usuario['endereco']);

	update_user_meta($usuario['id'], 'oneTarek_nhb', $usuario['bairro']);

	update_user_meta($usuario['id'], 'billing_city', $cidade_row['cid_nome']);
	update_user_meta($usuario['id'], 'cidade_nome', $cidade_row['cid_nome']);
	update_user_meta($usuario['id'], 'cidade', $usuario['cidade']);
	
	update_user_meta($usuario['id'], 'estado', $usuario['estado']);
	update_user_meta($usuario['id'], 'billing_state', $usuario['estado']);

	update_user_meta($usuario['id'], 'telefone', formatar_telefone($usuario['telefone']));
	update_user_meta($usuario['id'], 'billing_phone', formatar_telefone($usuario['telefone']));
	
	update_user_meta($usuario['id'], 'oneTarek_mother_name', $usuario['nome_mae']);
	update_user_meta($usuario['id'], 'oneTarek_CPF', $usuario['cpf']);
	update_user_meta($usuario['id'], 'oneTarek_date', $usuario['data_nascimento']);
	update_user_meta($usuario['id'], 'apelido', $usuario['apelido']);
	update_user_meta($usuario['id'], 'codigo_desconto', $usuario['codigo_desconto']);

	update_user_meta($usuario['id'], 'billing_email', $usuario['email']);
	
	if($usuario['apelido']) {
		update_user_meta($usuario['id'], 'mostrar_apelido', $usuario['mostrar_apelido']);
	}
	
	if(!$tem_email) {
		global $wpdb;
		$wpdb->update($wpdb->users, array('user_login' => $usuario['email']), array('ID' => $usuario['id']));
		
		$user = wp_signon(array(
				'user_login' => $usuario['email'],
				'user_password' => get_senha_oauth($usuario['email']),
				'remember' => false
		), false);
	}
}

function get_senha_oauth($email)
{
	return 'oauth-' . $email; 
}

function get_foto_usuario_filepath($usuario_id)
{
	return $_SERVER['DOCUMENT_ROOT'] . get_foto_usuario_url($usuario_id);
}

function get_foto_usuario_url($usuario_id)
{
	return '/questoes/uploads/fotos/' . md5($usuario_id) . '.jpg';
} 

function get_foto_usuario($usuario_id, $size = 150)
{
	if(is_professor($usuario_id)) {
		return get_avatar($usuario_id, $size);
	}
	
	$foto = '/questoes/assets-admin/img/foto-default.png';
	
	if(file_exists(get_foto_usuario_filepath($usuario_id))) {
		$foto = get_foto_usuario_url($usuario_id);
	}
	
	$now = time();
	return "<img src='$foto?v={$now}' width='{$size}'>";
}

function recuperar_senha($email)
{
	$user = get_user_by('email', $email);
	if($user) {
		$password = wp_generate_password();
		wp_set_password( $password, $user->ID );
	
		$titulo = "Recuperação de Senha";
		$mensagem = get_template_email ( 'recuperacao-senha.php', array (
				'senha' => $password
		) );
		
		enviar_email($email, $titulo, $mensagem);
	}
}

function excluir_posts_usuario($usuario_id)
{
	global $wpdb;

	$sql = "select * from wp_posts where post_author = {$usuario_id}";

	$wpdb->query($sql);
}

/**
 * Salva os campos adicionais de um usuário WP
 *
 * @param int $user_id Id do usuário no WP
 */

function expo_salvar_campos_profile_wp($user_id) {
	if (! current_user_can ( 'edit_user', $user_id )) {
		return false;
	}
	update_user_meta ( $user_id, 'oneTarek_mother_name', $_POST ['oneTarek_mother_name'] );
	update_user_meta ( $user_id, 'oneTarek_CPF', $_POST ['oneTarek_CPF'] );
	update_user_meta ( $user_id, 'oneTarek_date', $_POST ['oneTarek_date'] );
	update_user_meta ( $user_id, 'oneTarek_CEP', $_POST ['oneTarek_CEP'] );
	update_user_meta ( $user_id, 'oneTarek_nhb', $_POST ['oneTarek_nhb'] );
	update_user_meta ( $user_id, 'oneTarek_designation', $_POST ['oneTarek_designation'] );
	update_user_meta ( $user_id, 'oneTarek_author_type', $_POST ['oneTarek_author_type'] );
	update_user_meta ( $user_id, 'mini_cv', $_POST ['mini_cv'] );
	update_user_meta ( $user_id, 'cargo', $_POST ['cargo'] );
	update_user_meta ( $user_id, 'cargo_coaching', $_POST ['cargo_coaching'] );
	update_user_meta ( $user_id, 'cargo_concurso', $_POST ['cargo_concurso'] );
	update_user_meta ( $user_id, 'ocultar', $_POST ['ocultar'] );
	update_user_meta ( $user_id, LINKEDIN, $_POST [LINKEDIN] );
	update_user_meta ( $user_id, TWITTER, $_POST [TWITTER] );
	update_user_meta ( $user_id, FACEBOOK, $_POST [FACEBOOK] );
	update_user_meta ( $user_id, GOOGLEPLUS, $_POST [GOOGLEPLUS] );
	update_user_meta ( $user_id, INSTAGRAM, $_POST [INSTAGRAM] );
	update_user_meta ( $user_id, YOUTUBE, $_POST [YOUTUBE] );

	if(tem_acesso([ADMINISTRADOR, ATENDENTE])) {
 		salvar_valor_lucro($user_id, $_POST['lucro']);
	}

	if($_POST['oneTarek_author_type']) {

		if(!$_POST['slug']) {
			$_POST['slug'] = get_slug_disponivel($user_id);
		}

		if($_POST['slug'] && !tem_usuario_slug_duplicado($_POST['user_id'], $_POST['slug'])) {
			update_user_meta($user_id, 'slug', $_POST ['slug']);
		}
	}
}
add_action ( 'personal_options_update', 'expo_salvar_campos_profile_wp' );
add_action ( 'edit_user_profile_update', 'expo_salvar_campos_profile_wp' );

/**
 * Valida o campo slug e exibe erros na tela.
 * 
 * @since K4
 */

function validar_usuario_slug( &$errors ) 
{
	// Valida apenas usuários que são professores e/ou consultores
	if($_POST['oneTarek_author_type']) {

		if(tem_usuario_slug_duplicado($_POST['user_id'], $_POST['slug'])) {
			$errors->add( 'empty_missing_', '<strong>ERRO</strong>: Este slug já está sendo utilizado. Por favor, escolha outro.' );
		}
		
	}        
}
add_action( 'user_profile_update_errors', 'validar_usuario_slug' );

/**
 * Verifica se o slug exibido é duplicado.
 * 
 * @since K4
 * 
 * @param int $usuario_id
 * @param string $slug
 * 
 * @return bool
 */

function tem_usuario_slug_duplicado($usuario_id, $slug)
{
	global $wpdb;

	$sql = "SELECT * FROM wp_usermeta WHERE user_id != {$usuario_id} AND meta_key = 'slug' AND meta_value = '{$slug}' ";

	return $wpdb->get_results($sql) ? TRUE : FALSE;
}

/**
 * Adicionar campos de redes sociais
 */

function expo_contactmethods( $contactmethods ) 
{
    $contactmethods[INSTAGRAM] = 'Instagram';
    $contactmethods[LINKEDIN] = 'LinkedIn';

    return $contactmethods;
}
add_filter('user_contactmethods','expo_contactmethods',10,1);

function atualizar_login($usuario_antes, $novo_email, $from_checkout)
{	
	$usuario_id = $usuario_antes['id'];

	if($novo_email && ($usuario_antes['login'] != $novo_email)) {

		if(username_exists($novo_email)) {
            return false;            
        } else {
            global $wpdb;

            // Atualiza o login
            $query = $wpdb->query( $wpdb->prepare( "UPDATE wp_users SET user_login = %s WHERE ID = $usuario_id", $novo_email));

            if ($query) {
            	// // revalida a sessão
            	// wp_set_auth_cookie($usuario_id);

            	// atualiza o e-mail
            	wp_update_user(array('ID' => $usuario_id, 'user_email' => $novo_email ));

            	 $query = $wpdb->query( $wpdb->prepare( "UPDATE $wpdb->wp_woocommerce_downloadable_product_permissions SET user_email = %s WHERE user_id = $usuario_id", $novo_email));

            	 // define para onde o usuário será direcionado após login
            	 $ref = $from_checkout ? "/checkout" : get_perfil_url();
            	 // flag para exibição de mensagem correta na tela de login
            	 $ref .= "&rs=1"; 
            	 redirect(login_url($ref));
	        }
	        else {
	        	return false;
	        }
		}
	}
}

function verifica_parceiro_meta( $user_login, $user ) 
{
	delete_user_meta($user->ID, 'codigo_parceiro');

	if($parceiro_meta = recupera_cupons_parceiros()) {
		update_user_meta($user->ID, 'codigo_parceiro', $parceiro_meta);
	}
}
add_action('wp_login', 'verifica_parceiro_meta', 10, 2);

/**
 * Conta usermetas que tenha determinado conteúdo na coluna meta_value
 * 
 * @since K2
 * 
 * @param $conteudo string
 * @param $exclude string a ignorar durante a busca
 * 
 * @return int
 */

function contar_usermetas_com_conteudo($conteudo, $exclude = null)
{
	global $wpdb;
	
	if($exclude)
	{
	    //Acredito que o DB seja inteligente só faça o replace após o primeiro filtro, senão haverá problema de performance
	    $where .= " AND replace(meta_value, '{$exclude}', '') LIKE '%{$conteudo}%'";
	}

	return $wpdb->get_var("SELECT COUNT(*) FROM wp_usermeta WHERE meta_value LIKE '%{$conteudo}%'" . $where );
}

/**
 * Lista usermetas que tenha determinado conteúdo na coluna meta_value
 *
 * @since L1
 *
 * @param $conteudo string
 * @param $exclude string para ser ignorada
 *
 * @return array
 */

function listar_usermeta_com_conteudo($conteudo, $exclude = null)
{
    global $wpdb;
    
    if($exclude)
    {
        //Acredito que o DB seja inteligente só faça o replace após o primeiro filtro, senão haverá problema de performance
        $where .= " AND replace(meta_value, '{$exclude}', '') LIKE '%{$conteudo}%'";
    }
    
    return $wpdb->get_results("SELECT * FROM wp_usermeta WHERE meta_value LIKE '%{$conteudo}%'" .$where );
}

/**
 * Substitui conteúdo antigo pelo novo em todos os usermeta, coluna meta_value
 * 
 * @since K2
 * 
 * @param $antigo string
 * @param $novo string
 */

function replace_conteudo_de_usermetas($antigo, $novo)
{
	global $wpdb;

	$wpdb->query("UPDATE wp_usermeta SET meta_value = REPLACE(meta_value, '{$antigo}', '{$novo}') WHERE meta_value LIKE '%{$antigo}%'");
}

/**
 * Recupera um usuário array a partir de um slug
 * 
 * @since K4
 * 
 * @param $slug string
 *
 * @return array
 */

function get_usuario_by_slug($slug)
{
	global $wpdb;

	$id = $wpdb->get_var("SELECT user_id FROM wp_usermeta WHERE meta_key = 'slug' AND meta_value = '{$slug}'");

	if($id) {
		return get_usuario_array($id);
	}

	return NULL;
}

function get_slug_disponivel($usuario_id)
{
	$usuario = get_usuario_array($usuario_id);

	$slug_base = sanitize_title($usuario['nome_completo']);

	if(get_usuario_by_slug($slug_base)) {
		$i = 1;
		while(TRUE) {
			$candidato = $slug_base . "-{$i}";

			if(!get_usuario_by_slug($candidato)) {
				return $candidato;
			}

			$i++;
		}

	}
	else {
		return $slug_base;
	}

}