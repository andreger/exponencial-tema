<?php
function mailchimp_listar_interesses_areas()
{
	$curl = curl_init();

	curl_setopt_array($curl, array(
	    CURLOPT_RETURNTRANSFER => 1,
	    CURLOPT_URL => 'https://us1.api.mailchimp.com/3.0/lists/7746555c27/interest-categories/cd3440d75a/interests',
	    CURLOPT_USERPWD => "andreger:9a8bb3e1c8eba8bb248fffcc29a842d1-us1",
	    CURLOPT_HTTPAUTH => CURLAUTH_BASIC
	));

	$resp = curl_exec($curl);
	curl_close($curl);

	$interesses = [];

	if($json = json_decode($resp)) {

		if($interests = $json->interests) {
			foreach ($interests as $interest) {
				array_push($interesses, [
					'id' => $interest->id,
					'nome' => $interest->name
				]);
			}
		}
	}

	return $interesses;

}

function mailchimp_salvar($email, $nome, $interesses)
{
	$interests = [];

	if($interesses) {
		foreach ($interesses as $interesse) {
			$interest[$interesse] = true;
		}
	}

	$nome_a = explode(" ", $nome);
	$merge = [
		'FNAME' => $nome[0],
		'LNAME' => $nome[1]
	];

	$fields = array(
		"email_address" => $email,
		"status" => "subscribed",
		"merge_fields" => (object) $merge,
		"interests" => (object) $interests
	);

	$ch = curl_init('https://us1.api.mailchimp.com/3.0/lists/7746555c27/members');
 
	curl_setopt( $ch, CURLOPT_HTTPHEADER, array
	(
		'Content-Type: application/json', 
		'X-HTTP-Method-Override: POST',
	) );
 
	curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'POST' );
	curl_setopt( $ch, CURLOPT_USERPWD, "exponencial:9a8bb3e1c8eba8bb248fffcc29a842d1-us1");
	curl_setopt( $ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	curl_setopt( $ch, CURLOPT_TIMEOUT, 5 );
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

	curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
 
	$response = curl_exec( $ch );
	curl_close( $ch );
 
	$json = json_decode($response);

	if($erro = $json->title) {
		switch($erro) {
			case "Forgotten Email Not Subscribed": 
				return "O e-mail foi previamente excluído da lista. Entre em contato pelo Fale Conosco para resolver a situação."; break;
			case "Member Exists": 
				return "O e-mail já está cadastrado na lista."; break;
			default: 
				return "Erro ao cadastrar e-mail na lista. Entre em contato pelo Fale Conosco para resolver a situação."; break;
		}
	}
	else {
		return 0;
	}
}