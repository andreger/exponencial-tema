<?php 
function get_share_buttons($url = null) {
	
	$data_url = !is_null($url) ? "data-url='" . $url . "'" : '';
	
	return "<div class='mt-4 mb-4'>
			<div class='fb-share-button' data-layout='button_count' $data_url style='vertical-align: top; margin-top: -2.09px;'></div>
			<a href='https://twitter.com/share' class='twitter-share-button' $data_url>Tweet</a>
			<div class='g-plus' data-width='200' data-action='share' $data_url style='vertical-align: sub;'></div>
			</div>";
}				
?>