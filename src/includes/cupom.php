<?php
function get_cupom_by_post_name($post_name)
{
    global $wpdb;
    $post = $wpdb->get_var( $wpdb->prepare( "SELECT id FROM $wpdb->posts WHERE post_title = '%s' AND post_type = 'shop_coupon' AND post_status = 'publish'", $post_name ) );

	if($post)
		return get_post($post, OBJECT);

    return null;
}

function incluir_cupom_bonus($cupom, $item, $tipo)
{
	global $wpdb;
	
	$wpdb->insert(
			'cupons_bonus',
			array(
					'cupom_slug' => $cupom,
					'item_slug' => $item,
					'item_tipo' => $tipo,
			)
	);
	
	// adiciona_limite_de_uso_por_usuario($cupom, 1);

	$cupom_wp = get_cupom_by_post_name($cupom);
	update_post_meta($cupom_wp->ID, 'customer_email', array('cupom@exponencialconcursos.com.br'));
}

function atualizar_cupom_bonus($cupom_id, $cupom_slug, $item, $tipo)
{
	global $wpdb;

	$wpdb->update(
			'cupons_bonus',
			array(
					'cupom_slug' => $cupom_slug,
					'item_slug' => $item,
					'item_tipo' => $tipo,
			),
			array(
					'cb_id' => $cupom_id
			)
	);

	// adiciona_limite_de_uso_por_usuario($cupom_slug, 1);
}

function get_cupom_bonus($cb_id)
{
	global $wpdb;
	
	return $wpdb->get_row("SELECT * FROM cupons_bonus WHERE cb_id = {$cb_id}");
	
}

function excluir_cupom_bonus($cb_id)
{
	global $wpdb;

	$cupom = get_cupom_bonus($cb_id);
	$cupom_wp = get_cupom_by_post_name($cupom->cupom_slug);
	update_post_meta($cupom_wp->ID, 'customer_email', array('cupom@exponencialconcursos.com.br'));
	
	$wpdb->delete(
			'cupons_bonus',
			array(
					'cb_id' => $cb_id
			)
	);	
}

/***************************************************************************
 * Recupera cupons bônus pelo slug do cupom
 ***************************************************************************/
function get_cupom_bonus_by_slug($cupom_slug)
{
	if(is_null($cupom_slug)) {
		return null;
	}
	
	global $wpdb;
	$post = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM wp_posts WHERE post_title = '%s' AND post_type = 'shop_coupon' AND post_status = 'publish'", $cupom_slug ) );
	
	if($post)
		return get_post($post, OBJECT);
	
	return null;
}

/***************************************************************************
 * Recupera cupons bônus pelo slug do produto ou da categoria
 ***************************************************************************/
function get_cupom_bonus_by_item_slug($item_slug, $item_tipo)
{
	global $wpdb;
	$cupons_bonus = $wpdb->get_results("SELECT * FROM cupons_bonus WHERE item_slug = '{$item_slug}' AND item_tipo = {$item_tipo}", ARRAY_A);
	
	$cupons = array();

	if($cupons_bonus) {
		foreach ($cupons_bonus as $cupom_bonus) {
			array_push($cupons, get_cupom_bonus_by_slug($cupom_bonus['cupom_slug']));
		}
	}

	return $cupons;
}

/***************************************************************************
 * Lista todos os cupons bônus
 ***************************************************************************/
function listar_cupons_bonus()
{
	global $wpdb;
	return $wpdb->get_results("SELECT * FROM cupons_bonus ORDER BY cupom_slug ASC, item_slug ASC", ARRAY_A);

}

/***************************************************************************
 * Lista todos os cupons designados para os alunos do coaching
 ***************************************************************************/
function listar_cupons_coaching()
{
	global $wpdb;
	return $wpdb->get_results("SELECT * FROM items_coaching WHERE ite_tipo = 1 ORDER BY ite_slug", ARRAY_A);

}

function adiciona_email_a_restricoes_de_cupom($cupom_slug, $usuario_id)
{
	$usuario = get_usuario_array($usuario_id);
	$cupom = get_cupom_by_post_name($cupom_slug);
	
	$emails = get_post_meta($cupom->ID, 'customer_email', true);
	if(!$emails) {
		$emails = array();
	}

	if($usuario['email']) {
		array_push($emails, $usuario['email']); 
		update_post_meta($cupom->ID, 'customer_email', $emails);
		
		log_plugins('debug', "Adicionando restrição de e-mails ao cupom id: {$cupom->ID}, slug: {$cupom_slug}. E-mails: " . implode(', ', $emails));
	}
	else {
		log_plugins('debug', "Usuario {$usuario_id} nao possui e-mail para ser adicionado como restrição de e-mails ao cupom id: {$cupom->ID}, slug: {$cupom_slug}.");
	}
}

function adiciona_limite_de_uso_por_usuario($cupom_slug, $qtde_por_usuario)
{
	$cupom = get_cupom_by_post_name($cupom_slug);
	update_post_meta($cupom->ID, 'usage_limit_per_user', $qtde_por_usuario);
	
	log_plugins('debug', "Adicionando limite de uso ao cupom id: {$cupom->ID}, slug: {$cupom_slug}. Quantidade: {$qtde_por_usuario}");
}

function verifica_cupom_produto($post, $order, $enviar_email = TRUE)
{	
	log_wp('debug', 'Buscando cupom pelo slug: ' . $post->post_title);

	$usuario_array = get_usuario_array($order->user_id);
	$cupons = get_cupom_bonus_by_item_slug($post->post_title, ITEM_BONUS_PRODUTO);	
	

	if(!$cupons) {
		log_wp('debug','Nao encontramos cupom com slug: ' . $post->post_title);
		log_wp('debug','Listando categorias do post: ' . $post->ID);
		$categorias = listar_categorias($post->ID);
		
		log_wp('debug','Achamos '. count($categorias) . ' categorias do post: ' . $post->ID);
		foreach ($categorias as $categoria) {

			log_wp('debug','verificando categorias com slug: ' . $categoria->slug);
			if($itens = get_cupom_bonus_by_item_slug($categoria->slug, ITEM_BONUS_CATEGORIA)) {

				foreach ($itens as $item) {
					array_push($cupons, $item);
					log_wp('debug','Achou cupom de categoria' . $categoria->slug);
				}
			}
		}
	}
	
	if($cupons) {

		$bcc = array('leonardo.coelho@exponencialconcursos.com.br','contato@exponencialconcursos.com.br');

		foreach ($cupons as $cupom) {
		
			log_wp('debug','Temos cupom para item: ' . $post->post_title);
			log_wp('debug','Adicionando restricoes para item: ' . $post->post_title . ' usuario: ' . $order->user_id. ' cupom: ' . $cupom->post_title);
			adiciona_email_a_restricoes_de_cupom($cupom->post_title, $order->user_id);

				
//			if($enviar_email) {
//				$mensagem_cupom = get_template_email('cupom-bonus.php', array('nome' => $usuario_array['nome_completo'], 'cupom' => $cupom->post_title, 'desc' => $cupom->post_excerpt));
//
//				enviar_email($usuario_array['email'], 'Exponencial Concursos - Cupom Bônus', $mensagem_cupom, NULL, ['contato@exponencialconcursos.com.br', 'leonardo.coelho@exponencialconcursos.com.br']);
//			}
		}
	}
	else {
		log_wp('debug','nao ha cupons para esse item: ' . $post->post_title);
	}

	log_wp('debug','Finaliza verificacao de cupom bonus'); 
}