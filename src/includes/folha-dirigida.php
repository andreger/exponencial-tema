<?php
/**********************************************************************************
 * Affiliate Folha Dirigida
 * https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/upgrades/items/350
 **********************************************************************************/

/**********************************************************************************
 * TODO: Verificar se é necessário adicionar os ids dos produtos
 **********************************************************************************/

function folha_dirigida_vendas_snippet($valor, $order_id)
{
	$html = 
		"<script id='pap_x2s6df8d' src='https://folhadirigidamarketplace.postaffiliatepro.com/scripts/jxly4zxzvx'></script>
		<script>
			PostAffTracker.setAccountId('8d2f4490');
			var sale = PostAffTracker.createSale();
			sale.setTotalCost('{$valor}');
			sale.setOrderID('{$order_id}');
			sale.setProductID('{$order_id}');
			PostAffTracker.register();
		</script>";

	return $html;
}

function folha_dirigida_snippet()
{
	$html = 
		"<script type='text/javascript' id='pap_x2s6df8d' src='https://folhadirigidamarketplace.postaffiliatepro.com/scripts/jxly4zxzvx'></script>
		<script type='text/javascript'>
			PostAffTracker.setAccountId('8d2f4490');
			try {
			PostAffTracker.track();
			} catch (err) { }
		</script>";

	return $html;
}

function folha_dirigida_atual()
{
	global $wpdb;

	$sql = "SELECT * from fd_percentuais where data_fim is null";

	return $wpdb->get_row($sql);
}

function folha_dirigida_percentual_por_data($data)
{
	global $wpdb;

	$sql = "SELECT * from fd_percentuais where data_inicio <= '{$data} 00:00:00' and (data_fim is null or data_fim > '{$data} 23:59:59')";

	$resultado = $wpdb->get_row($sql);

	return $resultado ? $resultado->percentual / 100 : 0;
}

function folha_dirigida_listar_historico()
{
	global $wpdb;

	$sql = "SELECT * from fd_percentuais order by data_inicio desc";

	return $wpdb->get_results($sql);
}

function folha_dirigida_salvar_percentual($percentual)
{
	global $wpdb;

	$atual = folha_dirigida_atual();
	$hoje = date('Y-m-d 00:00:00');
	$ontem = date('Y-m-d 23:59:59', strtotime('-1 day'));

	if($atual) {

		if($atual->data_inicio == $hoje) {
			$wpdb->query("UPDATE fd_percentuais set percentual = '{$percentual}' where data_fim is null");

			return;
		}
		else {
			$wpdb->query("UPDATE fd_percentuais set data_fim = '{$ontem}' where data_fim is null");
		}
	}

	$wpdb->query("INSERT into fd_percentuais values ('{$hoje}', null, $percentual)");
}