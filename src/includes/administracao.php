<?php 
remove_action('admin_notices', 'woothemes_updater_notice');
if(!is_administrador()) {
	function remove_core_updates(){
		global $wp_version;return(object) array('last_checked'=> time(),'version_checked'=> $wp_version,);
	}

	add_filter('pre_site_transient_update_core','remove_core_updates');
	add_filter('pre_site_transient_update_plugins','remove_core_updates');
	add_filter('pre_site_transient_update_themes','remove_core_updates');
	
	/**
	 * Remove mensagem: Connect your store to WooCommerce.com to receive extensions updates and support.
	 * @see https://wpfixit.com/connect-your-store-to-woocommerce/
	 */ 
	add_filter( 'woocommerce_helper_suppress_admin_notices', '__return_true' );

	remove_action( 'admin_notices', array( Yoast_Notification_Center::get(), 'display_notifications' ) );
	remove_action( 'all_admin_notices', array( Yoast_Notification_Center::get(), 'display_notifications' ) );
	remove_all_actions( 'admin_notices' );
}

add_action('restrict_manage_posts','admin_filtro_forum');
function admin_filtro_forum($data)
{
    KLoader::model("ProdutoModel");
    
	if (in_array($_GET['post_type'], ['topic', 'reply'])) {

		// Administradores, coordenadores e revisores acessam todos os produtos
		$professor_id = tem_acesso([ADMINISTRADOR, COORDENADOR, REVISOR]) ? null : get_current_user_id();
	    $produtos = ProdutoModel::listar_por_professor($professor_id);
		
		$options = array();
		foreach ($produtos as $produto) {
			$forum_id = ProdutoModel::get_forum_id($produto->post_id);
			array_push($options, array('value' => $forum_id, 'text' => $produto->post->post_title));
		}

		$html = "";
		
		$html .= "<option>Em todos os fóruns</option>";
		foreach ($options as $option) {
			$html .= "<option value='{$option['value']}'>{$option['text']}</option>";
		}
		
		echo 	"<script>
					jQuery(document).ready(function() {
						jQuery('#bbp_forum_id').find('option').remove().end().append(\"$html\");		
					});
				</script>";
	}
}

add_action('admin_enqueue_scripts', 'my_admin_theme_style');
function my_admin_theme_style() {
	wp_enqueue_style('my-admin-style', get_template_directory_uri() . '/custom-admin.css');
}


add_action( 'do_meta_boxes', 'remove_revolution_slider_meta_boxes' );
function remove_revolution_slider_meta_boxes() {
	
	if(!tem_acesso([ADMINISTRADOR, REVISOR, APOIO, ATENDENTE])) {
		remove_meta_box( 'categorydiv', 'post', 'side' );
	}
}

add_action( 'admin_init', 'adaptar_campos_para_professores' );
function adaptar_campos_para_professores() {
	
	if(!tem_acesso([ADMINISTRADOR, COORDENADOR, REVISOR, ATENDENTE, APOIO])) {
		if(get_post_type($_GET['post']) == 'product') {
			echo "<style>#wpbody-content {display:none}</style>";
			echo "<style>#wpseo_meta {display:none}</style>";
			echo "<style>#wpseo-score {display:none}</style>";
			echo "<style>.page-title-action {display:none}</style>";
			echo "<style>#bundled_product_data {display:none}</style>";
			
			remove_post_type_support('product', 'editor');
			remove_meta_box( 'fb_pxl_metabox', 'product', 'normal' );		
			remove_meta_box( 'postcustom', 'product', 'normal' );
			remove_meta_box( 'postexcerpt', 'product', 'normal' );
			remove_meta_box( 'commentsdiv', 'product', 'normal' );
			remove_meta_box( 'product_catdiv', 'product', 'side' );
			remove_meta_box( 'tagsdiv-product_tag', 'product', 'side' );
			remove_meta_box( 'expirationdatediv', 'product', 'side' );
			remove_meta_box( 'postimagediv', 'product', 'side' );
			remove_meta_box( 'woocommerce-product-images', 'product', 'side' );
			remove_meta_box( 'slugdiv', 'product', 'normal');
			remove_meta_box( 'wpseo_meta', 'product', 'normal');
			wp_enqueue_script('campos-professor', get_template_directory_uri() . '/js/campos-professor.js', array('jquery'));
		}

		if($_GET['post_type'] == 'product') {
			echo "<style>.page-title-action {display:none}</style>";
		}

	}
}


/****************************************************************************
 * Hook para exibir apenas arquivos do proprio autor
 ****************************************************************************/
add_filter( 'posts_where', 'devplus_wpquery_where' );
function devplus_wpquery_where( $where ){
    global $current_user;
    
    if(!tem_acesso([ADMINISTRADOR, COORDENADOR, REVISOR, APOIO, ATENDENTE])) {

    	if( is_user_logged_in() ){
       		 // logged in user, but are we viewing the library?
       		if( isset( $_POST['action'] ) && ( $_POST['action'] == 'query-attachments' ) ){
           		// here you can add some extra logic if you'd want to.
           		$where .= ' AND post_author='.$current_user->data->ID;
       		}
    	}
    }

    return $where;
}


/****************************************************************************
 * Hook para exibir apenas produtos do proprio autor
 ****************************************************************************/

add_filter ( 'parse_query', 'expo_produto_professores' );
function expo_produto_professores($query)
{   
    if (is_admin () && ($_GET['post_type'] == 'product')) {
        
	   if (!tem_acesso([ADMINISTRADOR, COORDENADOR, REVISOR, APOIO, ATENDENTE])) {
	       
	       KLoader::model("ProdutoModel");
	       
			echo "<style type='text/css'>#wpbody-content {display:none}</style>";
				
			$_GET['post_type'] = null;

			$produtos = ProdutoModel::listar_por_professor(get_current_user_id());
			
			$produtos_ids = array ();
			foreach ( $produtos as $produto ) {
				array_push ( $produtos_ids, $produto->post_id );
			}
				
			$qv = &$query->query_vars;
			$qv['post__in'] = $produtos_ids;
			
			wp_enqueue_script('filtro-professor', get_template_directory_uri() . '/js/filtro-professor.js', array('jquery'));
			$_GET['post_type'] = 'product';
		}
	}
	
}

/**
 * Remove WooCommerce Status Widget do Dashboard de não administradores
 *	
 * [JOULE] Professores e coordenadores vendo painel impróprio!!
 * 
 * Bug aconteceu após atualização do WooCommerce e diferente do que foi descrito só 
 * acontece com coordenadores
 * 
 * @since J1
 * 
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2071
 */

add_action( 'do_meta_boxes', 'remove_woocommerce_status_boxes' );
function remove_woocommerce_status_boxes() {
	
	if(!tem_acesso(array(ADMINISTRADOR))) {
		remove_meta_box( 'woocommerce_dashboard_status', 'dashboard', 'normal' );
	}
}

/**
 * Ajustar painel inicial de coordenadores
 * 
 * @since J1
 * 
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/1809
 */

add_action( 'hidden_meta_boxes', 'expo_add_dashboard_widgets' );
function expo_add_dashboard_widgets() 
{
	global $post_type;

	if(is_admin() && !$post_type && !tem_acesso([ADMINISTRADOR])) {

		// lado esquerdo
		remove_meta_box('dashboard_activity', 'dashboard', 'normal');	
		remove_meta_box('dashboard_right_now', 'dashboard', 'normal');
		remove_meta_box('jetpack_summary_widget', 'dashboard', 'normal');		

		// lado direito
		remove_meta_box('dashboard_quick_press', 'dashboard', 'side');
		remove_meta_box('dashboard_primary', 'dashboard', 'side');	

        /**
         * Professores não devem ver o painel "Agora nos Fóruns". Coordenadores podem vê-lo
         * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2451
         */
		if(!tem_acesso([COORDENADOR])) {
		  remove_meta_box('bbp-dashboard-right-now', 'dashboard', 'normal');
		}
		
    	//wp_add_dashboard_widget( 'bbp-dashboard-right-now', __('Right Now in Forums', 'bbpress' ), 'bbp_dashboard_widget_right_now', 'dashboard', 'normal', 'high');
	}

	if(is_admin() && !$post_type && tem_acesso([APOIO, ATENDENTE, APOIO])) {
		exit;
	}

}

/**
 * Remove a notificação de atualização do WP exceto para usuário admin
 * 
 * @since K1
 * 
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2362
 */

add_action( 'admin_head', 'remover_notificacao_atualizacao_wp', 1 );
function remover_notificacao_atualizacao_wp()
{
	global $current_user;

	if($current_user->ID != 1) {
		remove_action('admin_notices', 'update_nag', 3);		
	}
}

function listar_painel_controle(){
	
	global $wpdb;

	$sql = "SELECT * FROM painel_controle WHERE pai_exibe = 1 ORDER BY pai_id ASC";

	return $wpdb->get_results($sql, ARRAY_A);

}

function contar_email_painel_controle_aviso(){
	global $wpdb;

	$sql = "SELECT COUNT(DISTINCT pca_email) FROM painel_controle_aviso";

	$count = $wpdb->get_var($sql);

	return $count;
}

function enviar_email_painel_controle_aviso(){

	global $wpdb;

//	$sql = "SELECT DISTINCT pca_email FROM painel_controle_aviso";
//
//	$result = $wpdb->get_results($sql, ARRAY_A);
//
//	foreach($result as $email){
//		$mensagem = get_template_email('aviso-manutencao.php');
//		enviar_email($email['pca_email'], "Exponencial Concursos - Manutenção encerrada", $mensagem, null, null);
//	}

	$wpdb->query('TRUNCATE TABLE painel_controle_aviso');

}

function limpar_painel_controle_aviso(){
	global $wpdb;
	$wpdb->query('TRUNCATE TABLE painel_controle_aviso');
}

function salva_painel_controle_aviso($email){
	global $wpdb;
	$wpdb->insert('painel_controle_aviso', array('pca_email' => $email, 'pca_data_hora' => time()));
}

function ativa_ou_desativa_area_painel_controle($pai_id, $ativo){
	global $wpdb;

	$wpdb->update('painel_controle', array('pai_ativo' => $ativo), array('pai_id' => $pai_id));
}

function is_area_desativada($areas_ids){
	global $wpdb;
	if(empty($areas_ids)){
		return FALSE;
	}
	$sql = "SELECT COUNT(*) FROM painel_controle WHERE pai_ativo = 0 AND pai_id IN (" . implode(",", $areas_ids) . ")";
	//echo $sql;
	$count = $wpdb->get_var($sql);
	//echo "<br/>Count: ".$count;
	return $count > 0;
}

/**
 * Lista todos os cronjobs cadastrados
 * 
 * @since K3
 * 
 * @return array
 */

function listar_cronjobs()
{
	global $wpdb;

	$sql = "SELECT * FROM cronjobs";

	return $wpdb->get_results($sql, ARRAY_A);
}

/**
 * Salva os dados de um cronjob novo ou existente
 * 
 * @since K
 * 
 * @param array $dados
 */

function salvar_cronjob($dados)
{
	global $wpdb;

	if(isset($dados['cro_id'])) {
		$wpdb->update(
				'cronjobs',
				$dados,
				array(
					'cro_id' => $dados['cro_id']
			)
		);
		
		return $dados['cro_id'];
	}
	else {
		$wpdb->insert('cronjobs', $dados);
		
		return $wpdb->insert_id;
	}
}

/**
 * Salva os dados de um cronjob table
 *
 * @since K
 *
 * @param array $dados
 */

function salvar_cronjob_table($dados)
{
    global $wpdb;
    
    $wpdb->delete('cronjobs_table', array('cro_id' => $id));
                
    $ct = [
        'cro_id' => $dados['cro_id'],
        'crt_minuto' => gerar_cronjob_data_string($dados['cro_minuto'], 0, 59),
        'crt_hora' => gerar_cronjob_data_string($dados['cro_hora'], 0, 23),
        'crt_dia' => gerar_cronjob_data_string($dados['cro_dia'], 1, 31),
        'crt_mes' => gerar_cronjob_data_string($dados['cro_mes'], 1, 12),
        'crt_dia_semana' => gerar_cronjob_data_string($dados['cro_dia_semana'], 0, 6),
    ];
    
    $wpdb->insert("cronjobs_table", $ct);
}

function gerar_cronjob_data_string($input, $base_inicial, $base_final)
{
    $input_a = explode(",", $input);
    $output = [];
    
    foreach ($input_a as $item) {
        
        $item = trim($item);

        for($i = $base_inicial; $i <= $base_final; $i++) {
        
            // todo o range
            if($item == "*") {
                $output[] = $i;
            }
            else {
                // processa a divisão
                if(strpos($item, "/") !== false) {
                    
                    $divisao = explode("/", $item);
                    
                    if($i / $divisao[1] == 0) {
                        $output[] = $i;
                    }  
                }
                
                // processa o número inteiro
                else {
                    
                    if($i == $item) {
                        $output[] = $i;
                    } 
                    
                }
                
            }
            
        }
        
    }
    
    return "*" . implode("*", array_unique($output)) . "*";
}

/**
 * Exclui um cronjob existente
 * 
 * @since K3
 * 
 * @param int $id
 */

function excluir_cronjob($id)
{
	global $wpdb;

	$wpdb->delete('cronjobs', array('cro_id' => $id));
}

/**
 * Recupera um cronjob através do id
 * 
 * @since K3
 * 
 * @param int $id
 *
 * @return object
 */

function get_cronjob($id)
{
	global $wpdb;
	
	return $wpdb->get_row("SELECT * FROM cronjobs WHERE cro_id = {$id}", ARRAY_A);
}

/**
 * Lista cronjobs de um determinado horário
 *
 * @since K5
 *
 * @param int $timestamp Timestamp
 *
 * @return array
 */

function listar_cronjobs_table($timestamp = null)
{
    global $wpdb;
    
    if(is_null($timestamp)) {
        $timestamp = time();
    }
    
    $min = intval(date("i", $timestamp));
    $hor = date("G", $timestamp);
    $dia = date("j", $timestamp);
    $mes = date("n", $timestamp);
    $ds = date("w", $timestamp);
    
	$sql = "SELECT ct.cro_id, c.cro_comando FROM cronjobs_table as ct INNER JOIN cronjobs as c on ct.cro_id = c.cro_id
		      WHERE ct.crt_minuto LIKE '%*{$min}*%' 
                AND ct.crt_hora LIKE '%*{$hor}*%' 
                AND ct.crt_dia LIKE '%*{$dia}*%' 
                AND ct.crt_mes LIKE '%*{$mes}*%' 
                AND ct.crt_dia_semana LIKE '%*{$ds}*%' 
                AND c.cro_status = 1;";

	return $wpdb->get_results($sql, ARRAY_A);
}

/**
 * Lista cronjobs de um determinado horário
 *
 * @since K5
 *
 * @param int $timestamp Timestamp
 *
 * @return array
 */

function executar_cronjobs()
{
    KLoader::helper("UrlRequestHelper");
    
    $cronjobs = listar_cronjobs_table();
    
    if($cronjobs) {
        foreach ($cronjobs as $item) {
            UrlRequestHelper::get_conteudo($item['cro_comando']);
        }
    }
}

function mytheme_tinymce_settings($init) 
{
    $init['block_formats'] = 'Paragraph=p;Heading 2=h2;Heading 3=h3;Heading 4=h4;Heading 5=h5;Heading 6=h6;Pre=pre';
    return $init;
}

add_filter( 'tiny_mce_before_init', 'mytheme_tinymce_settings' );