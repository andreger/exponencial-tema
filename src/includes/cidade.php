<?php
function get_cidades_por_estado($uf)
{
	global $wpdb;
	
	$query = "SELECT * FROM cidades WHERE cid_uf = '{$uf}' ";
	return $wpdb->get_results($query, ARRAY_A);
}

function get_cidade_por_codigo_ibge($codigo)
{
	global $wpdb;

	$query = "SELECT * FROM cidades WHERE cid_codigo_municipio = '{$codigo}' ";

	return $wpdb->get_row($query, ARRAY_A);
}

function get_cidade_usuario($usuario_array)
{
	if($usuario_array['cidade']) {
		$cidade = get_cidade_por_codigo_ibge($usuario_array['cidade']);
		return $cidade['cid_nome'];
	}
	else {
		if($usuario_array['cidade_nome']) {
			return $usuario_array['cidade_nome'];
		}
	}
	return null;
}