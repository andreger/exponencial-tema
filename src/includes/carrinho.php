<?php 
function get_ultimo_item_carrinho()
{
	global $woocommerce;

	$items = listar_items_carrinho();

	if($items) {
		return end($items);
	}

	return null;
}

function listar_items_carrinho()
{
	global $woocommerce;

	return WC()->cart->cart_contents;
}

function continuar_comprando_url()
{
	$ultimo_item = get_ultimo_item_carrinho();

	$categorias = get_todas_categoria_concurso($ultimo_item['product_id']);

	if(count($categorias) == 1) {
		return get_concurso_url($categorias[0]);
	}

	return '/cursos-por-concurso';
}

/*
function botoes_finalizar_compra($id_continuar = "btn-continuar", $id_finalizar = "btn-finalizar")
{
	$continuar_url = continuar_comprando_url();

	return 
		"<div class='checkout-botoes'>
			<a class='btn u-btn-secondary' href='{$continuar_url}' id='$id_continuar'>Adicionar mais itens ao carrinho</a>
			<a class='btn u-btn-primary finalizar' href='#' id='$id_finalizar'>Finalizar</a>
		</div>";
}
*/

add_action( 'init', 'woocommerce_clear_cart_url' );
function woocommerce_clear_cart_url() 
{
    if ( isset( $_GET['empty-cart'] ) ) {
        limpar_carrinho();
    }
}

function limpar_carrinho() 
{
    global $woocommerce;
    $woocommerce->cart->empty_cart();
}