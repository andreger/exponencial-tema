<?php
/**
 * Salva o estorno
 * 
 * @since J5 
 * 
 * @param int $pedido_id
 * @param float $valor
 * 
 * @return string Erros do PagSeguro. String vazia se não houver erro.
 */ 

function estornar_pedido_pagseguro($pedido_id, $valor)
{
	$pagseguro_id = get_pagseguro_transaction_id($pedido_id);

	//Se não é transação do PagSeguro então volta
	if(!$pagseguro_id)
	{
		return "";
	}

	if(is_producao()) {
		$dominio = "ws.pagseguro.uol.com.br";
		$token = "67E47689AEB74EF685A9B47B2FCB1E4A";
	}
	else {
		$dominio = "ws.sandbox.pagseguro.uol.com.br";
		$token = "5564F338213046488B37E7121D06AA71";
	}

	$url = "https://{$dominio}/v2/transactions/refunds/";
	$campos = "email=leonardo.coelho@exponencialconcursos.com.br&token={$token}&transactionCode={$pagseguro_id}&refundValue=" . number_format($valor, 2, ".", "");

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_TIMEOUT, 30);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_URL, $url );
	curl_setopt($ch, CURLOPT_POST, count(explode("&", $campos)));
	curl_setopt($ch, CURLOPT_POSTFIELDS, $campos);
	$return = curl_exec($ch);
	curl_close($ch);

	$xml = str_get_html($return);

	if($xml->find("result")) {
		return "";
	}
	else {
		$erros_a = [];
		
		foreach ($xml->find("error") as $erro) {
			array_push($erros_a, $erro->message);
		}

		return implode(", ", $erros_a);
	}

}