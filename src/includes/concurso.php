<?php 
function get_concurso_por_slug($slug) {

	$args = array(
			'name'				=> $slug,
			'post_type'    		=> 'concurso',
			'post_status' 		=> 'publish',
			'posts_per_page' 	=> 1,
			'orderby' 			=> 'title',
			'order' 			=> 'ASC',
	);

	$posts = get_posts($args);
	return count($posts) > 0 ? $posts[0] : null;
}

function get_concurso_titulo($concurso)
{
	return $concurso->post_title;
}

function get_concurso_descricao($concurso)
{
	return $concurso->post_content;
}

function get_concurso_video_url($concurso)
{
	return get_post_meta($concurso->ID, 'id_do_video', true);
}

function get_concurso_video_componente($concurso)
{
	$video_id = get_concurso_video_url($concurso);
	
	if(!empty($video_id))
		return "<iframe width='560' height='315' src='https://www.youtube.com/embed/{$video_id}' allowfullscreen></iframe>";
	return '';
}

function get_concurso_imagem_componente($concurso)
{
	$imagem_id = get_post_meta($concurso->ID, 'imagem', true);

	if(!empty($imagem_id))
		return wp_get_attachment_image($imagem_id);
	return '';
}

function concurso_slide_componente($concurso)
{
	$slide_id = get_post_meta($concurso->ID, 'id_do_slide', true);

	if(!empty($slide_id))
		return putRevSlider($slide_id);
	return '';
}

function listar_concursos_por_vendas($status = null) 
{
	global $wpdb;

	$join_status = "";
	$where_status = "";

	if($status) {
		$join_status = "join wp_postmeta pm2 on p.ID = pm2.post_id ";
		$where_status = "and pm2.meta_key = 'status_do_concurso' and pm2.meta_value = $status" ;
	}
	
	$sql = 
		"select p.ID from wp_posts p 
		join wp_postmeta pm on p.ID = pm.post_id 
		{$join_status}
		where post_type = 'concurso' 
			and post_status = 'publish' 
			and	pm.meta_key = 'vendas' 
			{$where_status}
		order by abs(pm.meta_value) desc";

	$resultado = $wpdb->get_results($sql);

	$posts = [];

	if($resultado = $wpdb->get_results($sql)) {
		foreach ($resultado as $item) {
			$nome_concurso = get_concurso_slug_por_id($item->ID);

			if(contar_todos_produtos_por_categoria($nome_concurso) > 0) {
				array_push($posts, get_post($item->ID));
			}
		}
	}
	return $posts;
}

function get_concurso_status_str($id)
{
	$status = get_post_meta($id, 'status_do_concurso', true);

	switch ($status) {
		case EDITAL_DIVULGADO:
			return "Edital Divulgado";
			break;

		case EDITAL_PROXIMO:
			return "Edital Próximo";
			break;
		
		default:
			return null;
			break;
	}
}

function get_concurso_status_estilo($id)
{
	$status = get_post_meta($id, 'status_do_concurso', true);

	switch ($status) {
		case EDITAL_DIVULGADO:
			return "";
			break;

		case EDITAL_PROXIMO:
			return "midia-artigo-yellow-2";
			break;
		
		default:
			return "";
			break;
	}
}

function get_concurso_url_por_id($id)
{
	$slug = get_concurso_slug_por_id($id);

	return "/concurso/" . $slug;
}

function get_concurso_slug_por_id($id)
{
	$post = get_post($id);
	return $post->post_name;
}