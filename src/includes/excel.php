<?php 
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-content/Spout/Autoloader/autoload.php';

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;

function exportar_xls($tabela, $nome)
{
	$newFileName = $nome . '.xlsx';
	$newFilePath = $_SERVER['DOCUMENT_ROOT'] . '/wp-content/temp/' . $newFileName;
	
	$writer = WriterFactory::create(Type::XLSX);
	$writer->openToFile($newFilePath);
	
	foreach ($tabela as $linha) {
		
		$row = array();

		foreach ($linha as $coluna) {
			array_push($row, $coluna);
		}

		$writer->addRow($row);
	}
	
	$writer->close();
	
	header('Content-Description: File Transfer');
	header('Content-Disposition: attachment; filename="' . $newFileName . '"');
	header('Content-Type: application/octet-stream');
	header('Content-Transfer-Encoding: binary');
	header('Content-Length: ' . filesize($newFilePath));
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	header('Pragma: public');
	header('Expires: 0');
	
	ob_end_clean();
	flush();

	readfile($newFilePath);
	exit;
}

function html_to_xls($html)
{
	$html = str_replace("<table", "<table style='border-collapse:collapse; border: 1px solid black;'", $html);
	$html = str_replace("<tr", "<tr style='border: 1px solid black;'", $html);
	$html = str_replace("<td", "<td style='border: 1px solid black;'", $html);
	
	$list = get_html_translation_table(HTML_ENTITIES);
	unset($list['"']);
	unset($list['<']);
	unset($list['>']);
	unset($list['&']);
	
	$search = array_keys($list);
	$values = array_values($list);
	$search = array_map('utf8_encode', $search);
	
	$out =  str_replace($search, $values, $html);
	
	return "<html xmlns:x='urn:schemas-microsoft-com:office:excel'>
		<head>
			<meta http-equiv='Content-Type' content='text/html;charset=UTF-8' />
		    <!--[if gte mso 9]>
		    <xml>
		        <x:ExcelWorkbook>
		            <x:ExcelWorksheets>
		                <x:ExcelWorksheet>
		                    <x:Name>Sheet 1</x:Name>
		                    <x:WorksheetOptions>
		                        <x:Print>
		                            <x:ValidPrinterInfo/>
		                        </x:Print>
		                    </x:WorksheetOptions>
		                </x:ExcelWorksheet>
		            </x:ExcelWorksheets>
		        </x:ExcelWorkbook>
		    </xml>
		    <![endif]-->
		</head>
				
		<body>
		   " . $out . "
		</body></html>";
}