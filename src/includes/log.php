<?php
function logger_msg($mensagem, $tipo = '[INFO]', $arquivo = null)
{
	$arquivo = is_null($arquivo) ?  $_SERVER['DOCUMENT_ROOT'] . '/logs/app.log' : $arquivo;
	$mensagem = $tipo . ' ' . $mensagem . "\r\n";
	@error_log($mensagem, 3, $arquivo);
} 

function logger_info($mensagem, $arquivo = null)
{
	logger_msg($mensagem, '[INFO]', $arquivo);
}
	
function logger_error($mensagem, $arquivo = null)
{
	logger_msg($mensagem, '[ERROR]', $arquivo);
}

function logger_warning($mensagem, $arquivo = null)
{
	logger_msg($mensagem, '[WARNING]', $arquivo);
}

function gravar_log($arquivo, $level, $mensagem)
{

    if( $level == "DEBUG" )
    {
        $caller = debug_backtrace( DEBUG_BACKTRACE_IGNORE_ARGS )[2];

        if( isset($caller['function']) && isset($caller['class']) )
        {
            $mensagem = "[{$caller['class']}->{$caller['function']}]: {$mensagem}";
        }
        elseif(  isset($caller['function']) )
        {
            $mensagem = "[{$caller['function']}]: {$mensagem}";
        }
        elseif( isset($caller['class']) )
        {
            $mensagem = "[{$caller['class']}]: {$mensagem}";
        }
    }

    $log = strtoupper($level) . ' - ' . date('Y-m-d H:i:s') . ' - ' . $mensagem . "\r\n";
    file_put_contents($arquivo, $log, FILE_APPEND);
}

function log_debug($level, $mensagem)
{
    $arquivo = $_SERVER['DOCUMENT_ROOT'] . '/../../../files/exponencial/logs/debug.txt';
    gravar_log($arquivo, $level, $mensagem);
}

function log_zip($level, $mensagem)
{
    $arquivo = $_SERVER['DOCUMENT_ROOT'] . '/../../../files/exponencial/logs/zip.txt';
    gravar_log($arquivo, $level, $mensagem);
}

function log_cron($level, $mensagem)
{
    $arquivo = $_SERVER['DOCUMENT_ROOT'] . '/../../../files/exponencial/logs/cron.txt';
    gravar_log($arquivo, $level, $mensagem);
}

function log_produto($level, $mensagem)
{
    $arquivo = $_SERVER['DOCUMENT_ROOT'] . '/../../../files/exponencial/logs/produto.txt';
    gravar_log($arquivo, $level, $mensagem);
}

function log_buscador($level, $mensagem)
{
    $arquivo = $_SERVER['DOCUMENT_ROOT'] . '/../../../files/exponencial/logs/buscador.txt';
    gravar_log($arquivo, $level, $mensagem);
}

function log_podio($level, $mensagem)
{
    $arquivo = $_SERVER['DOCUMENT_ROOT'] . '/../../../files/exponencial/logs/podio.txt';
    gravar_log($arquivo, $level, $mensagem);
}

function log_plugins($level, $mensagem)
{
    $arquivo = $_SERVER['DOCUMENT_ROOT'] . '/../../../files/exponencial/logs/plugins.txt';
    gravar_log($arquivo, $level, $mensagem);
}

function log_mailchimp($level, $mensagem)
{
    $arquivo = $_SERVER['DOCUMENT_ROOT'] . '/../../../files/exponencial/logs/mailchimp.txt';
    gravar_log($arquivo, $level, $mensagem);
}

function log_email($level, $mensagem)
{
    $arquivo = $_SERVER['DOCUMENT_ROOT'] . '/../../../files/exponencial/logs/email.txt';
    gravar_log($arquivo, $level, $mensagem);
}

function log_forum($level, $mensagem)
{
    $arquivo = $_SERVER['DOCUMENT_ROOT'] . '/../../../files/exponencial/logs/forum.txt';
    gravar_log($arquivo, $level, $mensagem);
}

function log_wp($level, $mensagem)
{
    $arquivo = $_SERVER['DOCUMENT_ROOT'] . '/../../../files/exponencial/logs/wp.txt';
    gravar_log($arquivo, $level, $mensagem);
}

function log_kcore($level, $mensagem)
{
    $arquivo = $_SERVER['DOCUMENT_ROOT'] . '/../../../files/exponencial/logs/kcore.txt';
    gravar_log($arquivo, $level, $mensagem);
}

function log_listar_cursos_online($mensagem, $level = "INFO")
{
    $arquivo = $_SERVER['DOCUMENT_ROOT'] . '/../../../files/exponencial/logs/listar_cursos_online.txt';
    gravar_log($arquivo, $level, $mensagem);
}

function log_sq($level, $mensagem)
{
    $arquivo = $_SERVER['DOCUMENT_ROOT'] . '/../../../files/exponencial/logs/sq.txt';
    gravar_log($arquivo, $level, $mensagem);
}

function log_relatorio($level, $mensagem)
{
    $arquivo = $_SERVER['DOCUMENT_ROOT'] . '/../../../files/exponencial/logs/relatorios.txt';
    gravar_log($arquivo, $level, $mensagem);
}

function log_estorno($mensagem, $level = "INFO")
{
    $arquivo = $_SERVER['DOCUMENT_ROOT'] . '/../../../files/exponencial/logs/estornos.txt';
    gravar_log($arquivo, $level, $mensagem);
}

function log_premium($mensagem)
{
    $arquivo = $_SERVER['DOCUMENT_ROOT'] . '/../../../files/exponencial/logs/premium.csv';
    file_put_contents($arquivo, $mensagem . "\n", FILE_APPEND);
}

function log_premium_error($mensagem, $level = "INFO")
{
    $arquivo = $_SERVER['DOCUMENT_ROOT'] . '/../../../files/exponencial/logs/premium_error.txt';
    gravar_log($arquivo, $level, $mensagem);
}

function log_pedido($level, $mensagem)
{
    $arquivo = $_SERVER['DOCUMENT_ROOT'] . '/../../../files/exponencial/logs/pedidos.txt';
    gravar_log($arquivo, $level, $mensagem);
}

function log_memcached($mensagem, $level = "INFO")
{
    $arquivo = $_SERVER['DOCUMENT_ROOT'] . '/../../../files/exponencial/logs/memcached.txt';
    gravar_log($arquivo, $level, $mensagem);
}

function log_temp($mensagem, $level = "INFO")
{
    $arquivo = $_SERVER['DOCUMENT_ROOT'] . '/../../../files/exponencial/logs/temp.txt';
    gravar_log($arquivo, $level, $mensagem);
}

function log_erro($mensagem, $e = null, $variaveis = null)
{
    $arquivo = $_SERVER['DOCUMENT_ROOT'] . '/../../../files/exponencial/logs/erros.txt';
    gravar_log($arquivo, "error", $mensagem);

    if($e) {
        gravar_log($arquivo, "error", $e->getMessage());
    }

    if($variaveis) {
        gravar_log($arquivo, "error", json_encode($variaveis));
    }

    if(is_usuario_logado()) {
        $usuario = get_usuario_array();
        gravar_log($arquivo, "error", json_encode(["id" => $usuario["id"], "email" => $usuario["email"], "nome" => $usuario["nome_completo"]]));
    }

    gravar_log($arquivo, "error", "- - - - -");
}