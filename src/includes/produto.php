<?php

/**
 * Recupera área de um ou mais produtos
 * 
 * @since K3
 * 
 * @param $produto_id int|array
 * 
 * @return string
 */
function get_area($produtos_ids)
{
    global $wpdb;

    // se nada for passado retorna string vazia
    if (! $produtos_ids) {
        return "";
    } 
    else {
        // query para lista de ids
        if (! is_array($produtos_ids)) {
            $produtos_ids = [
                $produtos_ids
            ];
        }

        $sql = "select t.name AS area from wp_term_relationships tr join wp_term_taxonomy tt on tr.term_taxonomy_id = tt.term_taxonomy_id join wp_terms t on tt.term_id = t.term_id where tt.parent = 381 and tr.object_id IN (" . implode(",", $produtos_ids) . ") group by t.name";

        $areas_a = [];

        $resultado = $wpdb->get_results($sql);
        foreach ($resultado as $item) {
            array_push($areas_a, $item->area);
        }

        return $areas_a ? implode(", ", $areas_a) : "";
    }
}

function get_produto_caderno($produto_id)
{
    $wpdb_corp = new wpdb(DB_USER_CORP, DB_PASSWORD_CORP, DB_NAME_CORP, DB_HOST_CORP);

    $sql = "SELECT * FROM cadernos_produtos WHERE produto_id = {$produto_id}";

    return $wpdb_corp->get_row($sql);
}

function get_produto_caderno_usuario($produto_id, $usuario_id)
{
    $produto_caderno = get_produto_caderno($produto_id);

    $wpdb_corp = new wpdb(DB_USER_CORP, DB_PASSWORD_CORP, DB_NAME_CORP, DB_HOST_CORP);

    $sql = "SELECT * FROM cadernos WHERE cad_ref_id = {$produto_caderno->cad_id} AND usu_id = {$usuario_id}";

    return $wpdb_corp->get_row($sql);
}

function is_produto_caderno($produto_id)
{
    return get_produto_caderno($produto_id) ? true : false;
}

function is_produto_caderno_questoes_online($produto_id)
{
    return tem_categoria($produto_id, TERM_CADERNO_QUESTOES_ONLINE);
}

/**
 * Retorna a máxima categoria de um produto para o agrupamento de categorias no
 * Relatório de Vendas por Concurso
 *
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/kb/items/54
 *
 * @param array $categorias_ids Lista dos IDs das categorias do produto
 *
 * @return int|string|null Slug da categoria máxima
 */
function get_curso_max_hierarquia(array $categorias_ids)
{
    $categorias_agrupadas = get_lista_categorias_agrupadas();

    foreach ($categorias_agrupadas as $id => $slug) {

        if(in_array($id, $categorias_ids)) {
            return $slug;
        }
    }

    return null;
}

/**
 * Obtém a lista de categorias agrupadas utilizadas no Relatório de Vendas por Concurso
 *
 * @return array Lista de categorias agrupadas
 */

function get_lista_categorias_agrupadas()
{
    return get_option(CATEGORIAS_AGRUPADAS) ?? 0;

//    return [
//        TERM_ASSINATURA_CURSO => CATEGORIA_ASSINATURA_CURSO,
//        TERM_COACHING => CATEGORIA_COACHING,
//        TERM_SIMULADO => CATEGORIA_SIMULADO,
//        TERM_CADERNO_QUESTOES_ONLINE => CATEGORIA_CADERNO_QUESTOES_ONLINE,
//        TERM_RESUMO_QUESTOES_COMENTADAS => CATEGORIA_RESUMO_QUESTOES_COMENTADAS,
//        TERM_MAPAS_MENTAIS => CATEGORIA_MAPA_MENTAL,
//        TERM_TECNICAS_ESTUDO => CATEGORIA_TECNICAS_ESTUDO,
//        TERM_TI_TECNOLOGIA_INFORMACAO => CATEGORIA_TI_TECNOLOGIA_INFORMACAO,
//        TERM_DISCURSIVA_REDACAO => CATEGORIA_DISCURSIVA,
//        TERM_RECURSOS_POS_PROVA => CATEGORIA_RECURSO,
//        TERM_CURSOS_REGULARES_AREA_FISCAL => CATEGORIA_CURSO_REGULAR_FISCAL,
//        TERM_CURSOS_REGULARES_AREA_CONTROLE => CATEGORIA_CURSO_REGULAR_CONTROLE,
//        TERM_CURSOS_REGULARES_AREA_POLICIAL => CATEGORIA_CURSO_REGULAR_POLICIAL,
//        TERM_CURSOS_REGULARES_POLICIA_MILITAR => CATEGORIA_CURSO_REGULAR_POLICIA_MILITAR,
//        TERM_CURSOS_REGULARES_POLICIA_CIVIL => CATEGORIA_CURSO_REGULAR_POLICIA_CIVIL,
//        TERM_BASICO_PARA_CONCURSOS => CATEGORIA_BASICO_PARA_CONCURSOS,
//        TERM_CURSOS_REGULARES_GERAL => CATEGORIA_CURSO_REGULAR_GERAL,
//        TERM_CURSOS_REGULARES_BOMBEIROS => CATEGORIA_CURSO_REGULAR_BOMBEIROS,
//        TERM_CURSOS_REGULARES_AREA_AGEPEN => CATEGORIA_CURSO_REGULAR_AGEPEN,
//        TERM_CURSOS_REGULARES_AREA_DEPEN => CATEGORIA_CURSO_REGULAR_DEPEN,
//        TERM_CURSOS_REGULARES_TJ => CATEGORIA_TJ_REGULAR,
//        TERM_CURSOS_REGULARES_TRE => CATEGORIA_TRE_REGULAR,
//        TERM_CURSOS_REGULARES_TRT => CATEGORIA_TRT_REGULAR,
//        TERM_CURSOS_REGULARES_TRF => CATEGORIA_TRF_REGULAR,
//        TERM_SENADO_FEDERAL => CATEGORIA_SENADO_FEDERAL
//    ];
}

function get_produtos_associados_a_caderno($caderno_id)
{
    $wpdb_corp = new wpdb(DB_USER_CORP, DB_PASSWORD_CORP, DB_NAME_CORP, DB_HOST_CORP);

    $sql = "SELECT * FROM cadernos_produtos WHERE cad_id = {$caderno_id}";

    $resultado = $wpdb_corp->get_results($sql);

    $produtos = array();
    foreach ($resultado as $cp) {
        array_push($produtos, get_produto_by_id($cp->produto_id));
    }

    return $produtos;
}

function get_produtos_associados_a_caderno_msg($caderno_id)
{
    if (! tem_acesso(array(
        ADMINISTRADOR
    )))
        return "";

    $produtos_a = array();
    $produtos = get_produtos_associados_a_caderno($caderno_id);

    if ($produtos) {
        foreach ($produtos as $produto) {
            array_push($produtos_a, $produto->post->post_title);
        }

        return ui_alerta('<b>Atenção! Produtos associados a esse caderno:</b> ' . implode(", ", $produtos_a), ALERTA_INFO);
    } else {
        return "";
    }
}

function criar_caderno($produto_id, $order_id, $incluir_caderno_produto = FALSE, $incluir_aula_produto = FALSE)
{
    //Verifica quais cadernos devem ser copiados para o usuário
    $cadernos = array();

    //Caderno associado ao produto
    if($incluir_caderno_produto){
        
        $pc = get_produto_caderno($produto_id);

        if ($pc) {

            $produto_caderno = get_produto_caderno_usuario($produto_id, get_current_user_id());
            //Só cria se de fato não encontrou o caderno para o usuário
            if(!$produto_caderno){
                array_push($cadernos, $pc->cad_id);
            }
        }
    }

    //Cadernos associados às aulas do produto
    if($incluir_aula_produto){

        $aulas_caderno = get_post_meta ( $produto_id, 'aulas_caderno', true );

        for($i = 0; $i <= count ( $aulas_caderno ); $i ++) { 	
            if($aulas_caderno[$i]) {
                foreach($aulas_caderno[$i] as $item) {
                    if($item){
                        //Só cria se de fato não encontrou o cderno para o usuário
                        KLoader::model("CadernoModel");
                        if(!CadernoModel::get_caderno($item, TRUE, $produto_id, $order_id)){
                            array_push($cadernos, $item);
                        }
                    }
                }
            }
        }
    }

    log_wp("debug", "Cadernos do produto {$produto_id}: " . print_r($cadernos, true));

    foreach($cadernos as $caderno_id){

        $wpdb_corp = new wpdb(DB_USER_CORP, DB_PASSWORD_CORP, DB_NAME_CORP, DB_HOST_CORP);
        // $wpdb_corp->show_errors();
        $sql = "SELECT * FROM cadernos WHERE cad_id = {$caderno_id}";
        $caderno_ref = $wpdb_corp->get_row($sql);

        if(!$caderno_ref)
        {
            log_wp("ERRO", "O produto {$produto_id} possui associação com um caderno inexistente {$caderno_id}");
            continue;
        }

        $novo_caderno = array(
            'cad_nome' => $caderno_ref->cad_nome,
            'cad_ref_id' => $caderno_ref->cad_id,
            'cad_data_criacao' => date('Y-m-d H:i:s'),
            'usu_id' => get_current_user_id(),
            'cad_comprado' => 1,
        );

        $wpdb_corp->insert('cadernos', $novo_caderno);
        $novo_caderno_id = $wpdb_corp->insert_id;


        //Se não for o caderno associado ao produto marca como caderno associado a aula
        if(!$pc || $pc->cad_id != $caderno_id){
            $caderno_pedido = array(
                'cad_id' => $novo_caderno_id,
                'cpe_prd_id' => $produto_id,
                'cpe_ped_id' => $order_id,
            );
            $wpdb_corp->insert('cadernos_pedidos', $caderno_pedido);
        }
        

        $sql = "SELECT * FROM cadernos_questoes WHERE cad_id = {$caderno_id}";
        $questoes = $wpdb_corp->get_results($sql);

        foreach ($questoes as $questao) {
            $novo_caderno_questao = array(
                'cad_id' => $novo_caderno_id,
                'que_id' => $questao->que_id
            );

            $wpdb_corp->insert('cadernos_questoes', $novo_caderno_questao);
        }
    }
}

function tem_categoria($produto_id, $categoria_id)
{
    $mcache_id = MCACHE_TEM_CATEGORIA . $produto_id . "_" . $categoria_id;
    $mcache = wp_cache_get($mcache_id);
    
    if($mcache === false) {
        global $wpdb;
        
        $sql = "SELECT * 
                FROM wp_term_relationships tr 
                    JOIN wp_term_taxonomy tt on tr.term_taxonomy_id = tt.term_taxonomy_id 
                WHERE tt.term_id = {$categoria_id} AND object_id = {$produto_id}";
        
        $mcache = $wpdb->get_row($sql) ? 1 : 0;
        
        wp_cache_set($mcache_id, $mcache);
    }
    
    return $mcache;
}

function tem_uma_das_categoria($produto_id, $categorias_ids)
{
    if(!is_array($categorias_ids)) {
        $categorias_ids = [$categorias_ids];
    }
    $ids = implode(",", $categorias_ids);
    
    $mcache_id = MCACHE_TEM_UMA_DAS_CATEGORIA . $produto_id . "_" . $ids;
    $mcache = wp_cache_get($mcache_id);
    
    if($mcache === false) {
        global $wpdb;
        
        $sql = "SELECT * from wp_term_relationships tr join wp_term_taxonomy tt on tr.term_taxonomy_id = tt.term_taxonomy_id where tt.term_id IN ($ids) AND object_id = {$produto_id}";
        
        $mcache = $wpdb->get_row($sql) ? 1 : 0;
        
        wp_cache_set($mcache_id, $mcache);
    }
    
    return $mcache;
}


function is_produto_simulado($produto_id)
{
    return tem_categoria($produto_id, TERM_SIMULADO);
}

function is_produto_audiobook($produto_id)
{
    return tem_categoria($produto_id, TERM_AUDIOBOOK);
}

function is_produto_pdf($produto_id)
{
    return tem_categoria($produto_id, TERM_TIPO_PDF);
}

function is_produto_video($produto_id)
{
    return tem_categoria($produto_id, TERM_TIPO_VIDEO);
}

function is_produto_turma_coaching($produto_id)
{
    return tem_categoria($produto_id, TERM_TURMA_COACHING);
}

function is_produto_coaching($produto_id)
{
    return tem_categoria($produto_id, TERM_COACHING);
}

function is_produto_pacote_curso($produto_id)
{
    return tem_categoria($produto_id, TERM_PACOTE_CURSO);
}

function is_produto_ti_tecnologia_informacao($produto_id)
{
    return tem_categoria($produto_id, TERM_TI_TECNOLOGIA_INFORMACAO);
}

function is_produto_pacote_sq($produto_id)
{
    return tem_categoria($produto_id, TERM_PACOTE_SQ);
}

function is_produto_curso_regular_fiscal_controle($produto_id)
{
    return tem_categoria($produto_id, TERM_CURSOS_REGULARES_AREA_FISCAL);
}

function is_produto_curso_regular_policia_militar($produto_id)
{
    return tem_categoria($produto_id, TERM_CURSOS_REGULARES_POLICIA_MILITAR);
}

function is_produto_curso_regular_policia_civil($produto_id)
{
    return tem_categoria($produto_id, TERM_CURSOS_REGULARES_POLICIA_CIVIL);
}

function is_produto_curso_regular_bombeiros($produto_id)
{
    return tem_categoria($produto_id, TERM_CURSOS_REGULARES_BOMBEIROS);
}

function is_produto_curso_regular_agepen($produto_id)
{
    return tem_categoria($produto_id, TERM_CURSOS_REGULARES_AREA_AGEPEN);
}

function is_produto_curso_regular_depen($produto_id)
{
    return tem_categoria($produto_id, TERM_CURSOS_REGULARES_AREA_DEPEN);
}

function is_produto_trf_regular($produto_id)
{
    return tem_categoria($produto_id, TERM_CURSOS_REGULARES_TRF);
}

function is_produto_tj_regular($produto_id)
{
    return tem_categoria($produto_id, TERM_CURSOS_REGULARES_TJ);
}

function is_produto_tre_regular($produto_id)
{
    return tem_categoria($produto_id, TERM_CURSOS_REGULARES_TRE);
}

function is_produto_trt_regular($produto_id)
{
    return tem_categoria($produto_id, TERM_CURSOS_REGULARES_TRT);
}

function get_produto_categorias_curso($post_id, $tipo)
{
    $categs = array();

    $categorias = get_the_terms($post_id, 'product_cat');

    foreach ($categorias as $categoria) {
        $pai = get_term($categoria->parent, 'product_cat');

        if ($pai->slug == $tipo) {
            array_push($categs, $categoria);
        }
    }

    return $categs;
}

function get_materia_url($categoria)
{
    return "/categoria/?cat_name={$categoria->slug}";
}

function get_produto_categorias_curso_str($post_id, $tipo, $link = FALSE)
{
    $categs = get_produto_categorias_curso($post_id, $tipo);

    $nomes = array();

    foreach ($categs as $item) {
        if ($link && $tipo == "discipline") {
            $url = get_materia_url($item);
            array_push($nomes, "<a href='{$url}'>{$item->name}</a>");
        } 
        elseif ($link && $tipo == "exam") {
            $url = get_concurso_url($item);
            array_push($nomes, "<a href='{$url}'>{$item->name}</a>");
        } 
        else {
            array_push($nomes, $item->name);
        }
    }

    return implode(', ', $nomes);
}

function get_primeira_categoria_concurso($curso_id, $excluir_categorias_slugs = null)
{
    $categorias = listar_categorias($curso_id);

    // print_r($categorias);

    if ($categorias) {
        foreach ($categorias as $categoria) {
            if (in_array($categoria->slug, array(
                'simulados',
                'discursiva'
            ))) {
                continue;
            }

            if ($excluir_categorias_slugs && in_array($categoria->slug, $excluir_categorias_slugs)) {
                continue;
            }

            if (in_array($categoria->slug, $excluir_categorias_slugs)) {
                continue;
            }

            if (is_categoria_concurso($categoria)) {
                return $categoria;
            }
        }
    }

    return null;
}

function get_todas_categoria_concurso($curso_id)
{
    $categorias = listar_categorias($curso_id);

    $resultado = [];
    if ($categorias) {
        foreach ($categorias as $categoria) {
            if (in_array($categoria->slug, array(
                'simulados',
                'discursiva'
            ))) {
                continue;
            }

            if (is_categoria_concurso($categoria)) {
                array_push($resultado, $categoria);
            }
        }
    }

    return $resultado;
}

function get_concurso_url($categoria)
{
    return "/concurso/" . $categoria->slug;
}

function listar_categorias_relatorio($curso_id)
{
    $categorias = listar_categorias($curso_id);

    if (! $categorias)
        return [];

    $retorno = array();

    foreach ($categorias as $categoria) {

        if (is_categoria_cursos_regulares($categoria) || is_categoria_mapas_mentais($categoria) || is_categoria_caderno_de_questoes_online($categoria) 
            || is_categoria_resumo_questoes_comentadas($categoria) || is_categoria_assinatura_sq_quinzenal($categoria) || is_categoria_assinatura_sq_mensal($categoria) 
            || is_categoria_assinatura_sq_trimestral($categoria) || is_categoria_assinatura_sq_semestral($categoria) || is_categoria_assinatura_sq_anual($categoria) 
            || is_categoria_simulado($categoria) || is_categoria_coaching($categoria) || is_categoria_concurso($categoria)) {

            array_push($retorno, $categoria);
        }
    }

    return $retorno;
}

function listar_categorias_relatorio_nao_somar($categorias)
{
    if (! $categorias)
        return [];

    $somar = array();

    // print_r($categorias);

    foreach ($categorias as $categoria) {

        if (is_categoria_cursos_regulares($categoria) || is_categoria_mapas_mentais($categoria) || is_categoria_caderno_de_questoes_online($categoria) 
            || is_categoria_resumo_questoes_comentadas($categoria) || is_categoria_assinatura_sq_quinzenal($categoria) || is_categoria_assinatura_sq_mensal($categoria) 
            || is_categoria_assinatura_sq_trimestral($categoria) || is_categoria_assinatura_sq_semestral($categoria) || is_categoria_assinatura_sq_anual($categoria) 
            || is_categoria_simulado($categoria) || is_categoria_coaching($categoria) || is_categoria_concurso($categoria)) {

            array_push($retorno, $categoria);
        }
    }

    return $retorno;
}

function get_categorias_str($curso_id, $incluir_coaching = false, $campo = 'name')
{
    $categorias = listar_categorias($curso_id);

    if (! $categorias)
        return "";

    $is_simulado = false;
    $disciplinas = array();
    $concursos = array();

    foreach ($categorias as $categoria) {

        if (is_categoria_cursos_regulares($categoria) || is_categoria_mapas_mentais($categoria) || is_categoria_caderno_de_questoes($categoria) || is_categoria_resumo_questoes_comentadas($categoria) || is_categoria_assinatura_sq_quinzenal($categoria) || is_categoria_assinatura_sq_mensal($categoria) || is_categoria_assinatura_sq_trimestral($categoria) || is_categoria_assinatura_sq_semestral($categoria) || is_categoria_assinatura_sq_anual($categoria)) {

            return $categoria->$campo;
        }

        if (is_categoria_simulado($categoria)) {
            $is_simulado = true;
            continue;
        }

        if (is_categoria_concurso($categoria)) {
            array_push($concursos, $categoria->$campo);
        }

        // Verifica se é coaching
        if ($incluir_coaching) {
            if (is_categoria_coaching($categoria)) {
                return $categoria->$campo;
            }
        }
    }

    if ($is_simulado) {
        return implode(', ', $concursos);
    }

    $todos = array_merge($concursos, $disciplinas);
    return implode(', ', $todos);
}

function tem_cronograma($produto_id)
{
    return get_post_meta($produto_id, 'aulas_nome', true) ? true : false;
}

function get_autores($post)
{
    if (filter_var($post, FILTER_VALIDATE_INT)) {
        $post_id = (int)$post;
    } else {
        $post_id = $post->ID;
    }

    $authors = array();
    $authors_ids = get_post_meta($post_id, '_authors', TRUE);

    if (! is_array($authors_ids))
        return $authors; // se consulta não retornar nada, retorna array vazio

    foreach ($authors_ids as $author_id) {
        if ($author_id == - 1)
            continue;

        array_push($authors, get_userdata($author_id));
    }

    return $authors;
}

function get_url_aula_demonstrativa($url)
{
    // $index = get_post_meta($product_id, 'aula_demo', true);
    return "/aula-demonstrativa-download?url=" . urldecode($url);
    // return "/aula-demonstrativa-download?pr={$product_id}&pk={$product_key}";
}

function get_arquivos_aula_demonstrativa($produto)
{
    //$cache = get_transient(CACHE_PRODUTO_AULAS_DEMO_DATA . $produto->get_id());

    $cache = null;
    
    if(!$cache) {
        $aulas = array();
    
        if (is_pacote($produto)) {
    
            foreach ($produto->get_bundled_items() as $item) {
                $p = $item->get_product();
    
                $produto_nome = $p->get_title();
    
                $index = get_post_meta($p->get_id(), 'aula_demo', true);
                $aula = get_cronograma_aula_por_index($p->get_id(), $index);
    
                if (is_array($aula['arquivo'])) {
                    foreach ($aula['arquivo'] as $item) {
                        adicionar_aula_demonstrativa_em_array($aulas, $item, $produto_nome);
                    }
                } else {
                    adicionar_aula_demonstrativa_em_array($aulas, $aula['arquivo'], $produto_nome);
                }
            }
    
            // return $aulas;
        } 
        else {
    
            $produto_nome = $produto->post->post_title;
    
            $product_id = $produto->id;
            $index = get_post_meta($product_id, 'aula_demo', true);
            $aula = get_cronograma_aula_por_index($product_id, $index);
    
            if (is_array($aula['arquivo'])) {
    
                foreach ($aula['arquivo'] as $item) {
                    adicionar_aula_demonstrativa_em_array($aulas, $item, $produto_nome);
                }
            } else {
                adicionar_aula_demonstrativa_em_array($aulas, $aula['arquivo'], $produto_nome);
            }
    
            // return $aulas;
            // return listar_arquivos_aula($product_id, $aula['nome']);
        }
    
        get_vimeo_aula_demonstrativa($aulas, $produto);

        get_caderno_aula_demonstrativa($aulas, $produto);
    
        $cache = $aulas;
        
        set_transient(CACHE_PRODUTO_AULAS_DEMO_DATA . $produto->get_id(), $cache, DAY_IN_SECONDS);
    }
    
    return $cache;
}

function get_aula_demonstrativa($meta_key, $tipo, &$aulas, $produto)
{
    if (is_pacote($produto)) {

        foreach ($produto->get_bundled_items() as $item) {
            $p = $item->get_product();
            $produto_nome = $p->get_title();

            $index = get_post_meta($p->id, 'aula_demo', true);
            $meta_value = get_post_meta($p->id, $meta_key, true);

            if ($meta_value) {

                if ($meta_value[$index]) {
                    foreach ($meta_value[$index] as $item) {
                        adicionar_aula_demonstrativa_em_array($aulas, $item, $produto_nome, $tipo);
                    }
                }
            }
        }
        return $aulas;
    } 
    else {

        $produto_nome = $produto->post->post_title;

        $index = get_post_meta($produto->id, 'aula_demo', true);
        $meta_value = get_post_meta($produto->id, $meta_key, true);

        if ($meta_value) {

            if ($meta_value[$index]) {
                foreach ($meta_value[$index] as $item) {
                    adicionar_aula_demonstrativa_em_array($aulas, $item, $produto_nome, $tipo);
                }
            }
        }

        return $aulas;
    }
}

function get_vimeo_aula_demonstrativa(&$aulas, $produto)
{
    return get_aula_demonstrativa("aulas_vimeo", "vimeo", $aulas, $produto);
}

function get_caderno_aula_demonstrativa(&$aulas, $produto)
{
    return get_aula_demonstrativa("aulas_caderno", "caderno", $aulas, $produto);
}

function adicionar_aula_demonstrativa_em_array(&$aulas, $arquivo, $curso, $tipo = 'arquivo')
{
    if ($arquivo) {
        array_push($aulas, array(
            'arquivo' => $arquivo,
            'curso' => $curso,
            'tipo' => $tipo
        ));
    }
}

function redirecionar_se_produto_indisponivel($product_id)
{
    if (is_produto_expirado($product_id)) {
        wp_redirect(get_site_url() . '/cursos-por-concurso/?expirado=1', 301);
    }
    
    if(!is_produto_publicado($product) && !is_produto_privado($product)){
        wp_redirect(get_site_url() . '/cursos-por-concurso/?expirado=1', 301);
        exit;
    }
    
    
    exit();
}

function get_data_vendas_ate($product_id)
{
    $data_produto = get_post_meta($product_id, PRODUTO_VENDAS_ATE_POST_META, true);
    if (empty($data_produto))
        return false;

    return $data_produto;
}

/**
 * Usado como base para o método
 * 'function get_data_disponivel_por_vendas_ate_e_disponivel_ate($meta_vendas_ate, $meta_disponivel_ate)'
 * Alterações devem refletir em ambos
 */
function get_data_disponivel_ate($product_id, $usuario_id = null, $pedido_id = null)
{
    // tratamento para produtos com tempo de acesso definido
    if($pedido_id) {
        
        if($tempo_acesso = get_post_meta($product_id, PRODUTO_TEMPO_DE_ACESSO, true)) {

            $pedido = wc_get_order($pedido_id);

            $data_base = $pedido->get_date_completed();
            if(!$data_base) {
                $data_base = $pedido->get_date_created();
            }
            
            if($data_base) {
                $expiracao = date("Y-m-d", strtotime($data_base->date("Y-m-d") . " +{$tempo_acesso} days"));
                
                return date("d/m/Y", strtotime($expiracao));
            }
            else {
                return "";
            }
        }
    }
    
    // tratamento geral
    if (is_produto_assinatura($product_id) && $usuario_id) {
        return converter_para_ddmmyyyy(get_validade_assinatura_sq($usuario_id));
    } 
    else {

        $data_disponivel = get_post_meta($product_id, PRODUTO_DISPONIVEL_ATE_POST_META, true);

        if (empty($data_disponivel)) {
            if ($venda_ate = get_data_vendas_ate($product_id)) {
                $venda_yyyymmdd = converter_para_yyyymmdd($venda_ate);

                return date('d/m/Y', strtotime("{$venda_yyyymmdd} +6 months"));
            } else {
                return false;
            }
        } else {
            $data_disponivel = converter_para_yyyymmdd($data_disponivel);
            $data_disponivel = strtotime($data_disponivel . ' 00:00:00');

            // checa se ainda está sendo vendido. Se sim, está disponível
            $data_venda = get_data_vendas_ate($product_id);
            $data_venda = converter_para_yyyymmdd($data_venda);
            $data_venda = strtotime($data_venda . ' 00:00:00');

            if ($data_venda > $data_disponivel) {
                return get_data_vendas_ate($product_id);
            }

            return date('d/m/Y', $data_disponivel);
        }
    }
}

/**
 * Feito com base no método
 * 'function get_data_disponivel_ate($product_id, $usuario_id = NULL)'
 * Alterações devem refletir em ambos
 */
function get_data_disponivel_por_vendas_ate_e_disponivel_ate($meta_vendas_ate, $meta_disponivel_ate)
{
    if ($meta_vendas_ate) {
        $venda_yyyymmdd = converter_para_yyyymmdd($meta_vendas_ate);
    } else {
        $venda_yyyymmdd = hoje_yyyymmdd();
    }

    if (empty($meta_disponivel_ate)) {
        return strtotime("{$venda_yyyymmdd} +6 months");
    } else {
        $data_disponivel = converter_para_yyyymmdd($meta_disponivel_ate);
        $data_disponivel = strtotime($data_disponivel . ' 00:00:00');

        // checa se ainda está sendo vendido. Se sim, está disponível
        $data_venda = strtotime($venda_yyyymmdd . ' 00:00:00');

        if ($data_venda > $data_disponivel) {
            return $data_venda;
        }

        return $data_disponivel;
    }
}

function is_pacote_antigo($item)
{
    $product = new WC_Product($item['product_id']);

    if (is_pacote($product))
        return false;

    $product_name = $item['name'];
    return strpos($product_name, 'Pacote') !== false;
}

function get_autores_str($post, $class = 'author', $incluir_titulo = false, $remover_link = false, $titulo_geral = false)
{
    $authors_str = array();
    $authors = get_autores($post);

    for ($i = 0; $i < count($authors); $i ++) {
        $author = $authors[$i];

        if ($i > 0)
            $incluir_titulo = false; // remove o título dos professores além do primeiro

        if ($remover_link) {
            $item = get_autor_str($author, $incluir_titulo);
        } else {
            $item = get_autor_link($author, $class, $incluir_titulo);
        }

        array_push($authors_str, $item);
    }

    $str = implode($authors_str, ', ');

    if ($titulo_geral) {
        $t = count($authors) > 1 ? "Professores: " : "Professor: ";
        $str = "<span class='font-weight-bold'>$t</span>" . $str;
    }

    return str_replace_last(', ', ' e ', $str);
}

function is_produto_publicado($produto)
{
    return $produto->post->post_status == "publish" ? TRUE : FALSE;
}

/**
 * Verifica se o produto é privado (escondido)
 *
 * @param Object $produto
 *            Produto a ser verificado
 * @return boolean
 */
function is_produto_privado($produto)
{
    return $produto->post->post_status == "private" ? TRUE : FALSE;
}

function is_produto_expirado($product_id)
{
    $data_string = get_data_vendas_ate($product_id);

    if (empty($data_string)) {
        return FALSE;
    }

    $data_string = converter_para_yyyymmdd($data_string);

    if ($data_string >= date('Y-m-d')) {
        return FALSE;
    } else {
        return TRUE;
    }
}

/**
 * Verifica se um produto está visivel ou não
 * Para ser escondido ele precisa ter a propriedade _visibility definida como hidden
 *
 * @since J4
 *       
 * @param $produto_id Int
 *            ID do produto
 *            
 * @return bool
 */
function is_produto_visivel($produto_id)
{
    $visibility = get_post_meta($produto_id, '_visibility', true);

    if (empty($visibility)) {
        return true;
    } else {
        return $visibility == "hidden" ? false : true;
    }
}

function definir_produto_listagem_status($produto_id)
{
    if (is_produto_expirado($produto_id) || is_produto_assinatura($produto_id) || ! is_produto_visivel($produto_id)) {
        update_post_meta($produto_id, EXIBIR_EM_CURSOS_ONLINE, NAO);
    } else {
        update_post_meta($produto_id, EXIBIR_EM_CURSOS_ONLINE, SIM);
    }
}

function listar_produtos_expirados($somente_ativos = TRUE)
{
    global $wpdb;

    $hoje = hoje_yyyymmdd();
    $meta = PRODUTO_VENDAS_ATE_POST_META;
    $ativos_frag = $somente_ativos ? " AND post_status = 'publish'" : "";

    $sql = "SELECT ID FROM wp_posts p JOIN wp_postmeta pm ON p.ID = pm.post_id 
				WHERE pm.meta_key = '{$meta}' 
				AND pm.meta_value is not null and trim(pm.meta_value) <> ''
				AND STR_TO_DATE(pm.meta_value, '%d/%m/%Y') <= STR_TO_DATE('{$hoje}', '%Y-%m-%d') {$ativos_frag}";

    $produtos = array();
    if ($resultado = $wpdb->get_results($sql)) {
        foreach ($resultado as $item) {
            array_push($produtos, new WC_Product($item->ID));
        }
    }

    return $produtos;
}

function is_produto_indisponivel($product_id, $pedido_id = null)
{
    KLoader::model("ProdutoModel");
    
    // tratamento para produtos com tempo de acesso definido
    if($pedido_id) {
        
        if($tempo_acesso = get_post_meta($product_id, PRODUTO_TEMPO_DE_ACESSO, true)) {
            
            $pedido = wc_get_order($pedido_id);
            
            $data_base = $pedido ? $pedido->get_date_completed() : null;
            if(!$data_base) {
                $data_base = $pedido ? $pedido->get_date_created() : null;
            }
            
            if($data_base) {
                $expiracao = date("Y-m-d", strtotime($data_base->date("Y-m-d") . " +{$tempo_acesso} days"));
                
                return date("Y-m-d") < $expiracao ? false : true;
            }
         
        }
    }
    
    // tratamento geral
    if (is_produto_assinatura($product_id)) {
        return is_assinatura_sq_expirada(get_current_user_id());
    } 
    else {

        $data_disponivel = get_data_disponivel_ate($product_id);

        if (empty($data_disponivel))
            return false;

        $data_disponivel = converter_para_yyyymmdd($data_disponivel);
        $data_disponivel = strtotime($data_disponivel . ' 00:00:00');

        // checa se ainda está sendo vendido. Se sim, está disponível
        $data_venda = get_data_vendas_ate($product_id);
        $data_venda = converter_para_yyyymmdd($data_venda);
        $data_venda = strtotime($data_venda . ' 00:00:00');

        if ($data_venda > $data_disponivel) {
            return true;
        }

        $agora = strtotime("now");

        if (($data_disponivel - $agora) > 0)
            return false;

        return true;
    }
    
}

function is_autor($product_id, $autor_id)
{
    $produto = new WC_Product($product_id);
    $autores = get_autores($produto->post);

    foreach ($autores as $autor) {
        if ($autor->ID == $autor_id)
            return true;
    }

    return false;
}

function get_participacao_autor($product_id, $autor_id)
{
    if (! has_multiplos_autores($product_id))
        return 100;

    $produto = new WC_Product($product_id);
    $post = $produto->post;
    $authors = get_post_meta($post->ID, '_authors', true);
    $percentuais = get_post_meta($post->ID, '_percentuais', true);

    $key = array_search($autor_id, $authors);
    return $percentuais[$key];
}

function has_multiplos_autores($product_id)
{
    $produto = new WC_Product($product_id);
    $post = get_post($produto->get_id());

    return count(get_autores($post)) > 1 ? true : false;
}

function get_produto_compra_url($produto, $ref)
{
    return is_user_logged_in() ? $produto->get_permalink() . "?add-to-cart=" . $produto->id : "/cadastro-login?ref=" . $ref . "?add-to-cart=" . $produto->id;
}

function get_todos_produtos_posts($somente_discipline = true, $status = 'publish')
{
    $args = array(
        'post_type' => 'product',
        'posts_per_page' => - 1,
        'post_status' => $status,
        'orderby' => 'title',
        'order' => 'ASC'
    );

    if ($somente_discipline) {
        $args['product_cat'] = 'discipline';
    }

    return get_posts($args);
}

function get_todos_produtos_e_coaching_posts($status = 'publish')
{
    $args = array(
        'post_type' => 'product',
        'post_status' => $status,
        'tax_query' => array(
            'relation' => 'OR',
            array(
                'taxonomy' => 'product_cat',
                'field' => 'slug',
                'terms' => 'discipline'
            ),
            array(
                'taxonomy' => 'product_cat',
                'field' => 'slug',
                'terms' => 'coaching'
            ),
            array(
                'taxonomy' => 'product_cat',
                'field' => 'slug',
                'terms' => 'simulados'
            )
        ),
        'posts_per_page' => - 1,
        'orderby' => 'title',
        'order' => 'ASC'
    );

    return get_posts($args);
}

function get_todos_produtos($status = 'publish')
{
    $posts = get_todos_produtos_posts(TRUE, $status);

    $produtos = array();
    foreach ($posts as $post) {
        array_push($produtos, new WC_Product($post->ID));
    }

    return $produtos;
}

function listar_arquivos_de_produto($produto_id)
{
    $produto = new WC_Product($produto_id);

    if ($produto->is_downloadable()) {
        return $produto->get_files();
    }
    return false;
}

function get_informacao_produto($product_id)
{
    return get_post($product_id)->post_content;
}

function get_nome_produto($product_id)
{
    return get_post($product_id)->post_title;
}

function get_produto_by_id($product_id)
{
    return new WC_Product($product_id);
}

function get_produtos_relacionados($product_id, $qtd)
{
    $product = new WC_Product($product_id);
    // recupera produtos relacionados selecionados manualmente
    $related_ids = array_filter(array_map('absint', (array) get_post_meta($product->post->ID, '_related_ids', true)));

    // se nenhum foi selecionado, recupera produtos relacionados gerados automaticamente
    $num_related_ids = count($related_ids);
    if ($num_related_ids > $qtd) {
        shuffle($related_ids);
        $related_ids = array_slice($related_ids, 0, $qtd);
    } else {
        // se não houver cursos relacionados suficiente busca cursos das mesmas categorias
        if ($num_related_ids < $qtd) {

            $categorias = listar_categorias($product_id);
            $auto_related_ids = array();

            foreach ($categorias as $categoria) {

                if (is_categoria_nao_relacionavel($categoria)) {
                    continue;
                }

                $cursos = get_cursos_por_categoria($categoria->slug, true);

                foreach ($cursos as $curso) {

                    if (is_produto_expirado($curso->ID))
                        continue;

                    if (is_produto_assinatura($curso->ID))
                        continue;

                    if (is_produto_coaching($curso->ID))
                        continue;

                    if ($product_id == $curso->ID)
                        continue;

                    if (! in_array($curso->ID, $related_ids)) {
                        array_push($auto_related_ids, $curso->ID);
                    }
                }
            }

            if (count($auto_related_ids) > 0) {
                shuffle($auto_related_ids);
                $auto_related_ids = array_slice($auto_related_ids, 0, $qtd - $num_related_ids);
                $related_ids = array_merge($related_ids, $auto_related_ids);
            }
        }

        /**
         * ***********************************************************************
         * E-mail de confirmação de compra não está no padrão previsto
         * https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/1307
         *
         * Foi verificado que os cursos relacionados até essa parte do código
         * poderiam estar com status diferente de "publicado".
         *
         * O código abaixo elimina essa possibilidade
         * ***********************************************************************
         */

        $temp_a = $related_ids;
        $related_ids = array();

        foreach ($temp_a as $id) {
            if (is_produto_publicado(new WC_Product($id))) {
                array_push($related_ids, $id);
            }
        }

        // se ainda assim não houver cursos relacionados suficientes busca qualquer curso.
        $num_related_ids = count($related_ids);
        if ($num_related_ids < $qtd) {

            $posts = get_todos_produtos_posts();
            $auto_related_ids = array();

            foreach ($posts as $post) {

                if (is_produto_expirado($post->ID))
                    continue;

                if (is_produto_assinatura($post->ID))
                    continue;

                if (is_produto_coaching($post->ID))
                    continue;

                if ($product_id == $post->ID)
                    continue;

                if (! in_array($post->ID, $related_ids)) {
                    array_push($auto_related_ids, $post->ID);
                }
            }

            if (count($auto_related_ids) > 0) {
                shuffle($auto_related_ids);
                $auto_related_ids = array_slice($auto_related_ids, 0, $qtd - $num_related_ids);
                $related_ids = array_merge($related_ids, $auto_related_ids);
            }
        }
    }

    $produtos = array();

    foreach ($related_ids as $id) {
        array_push($produtos, new WC_Product($id));
    }

    return $produtos;
}

function get_produtos_por_coordenador($user_id, $maximo = -1, $order_by = 'title', $order = 'ASC', $status = 'publish')
{
    global $wpdb;

    $ids = array();

    $sql = "SELECT DISTINCT(post_id) FROM wp_postmeta WHERE meta_key = '_authors' AND meta_value LIKE '%{$user_id}%'";

    if ($resultado = $wpdb->get_results($sql)) {
        foreach ($resultado as $item) {
            if (! in_array($item->post_id, $ids)) {
                array_push($ids, $item->post_id);
            }
        }
    }
    ;

    $sql = "SELECT object_id FROM wp_term_relationships tr JOIN wp_term_taxonomy tt ON tt.term_taxonomy_id = tr.term_taxonomy_id WHERE term_id IN (
			SELECT area_id FROM coordenadores_areas WHERE user_id = {$user_id}
		)";

    if ($resultado = $wpdb->get_results($sql)) {
        foreach ($resultado as $item) {
            if (is_produto_caderno($item->object_id) || is_produto_simulado($item->object_id) || is_produto_assinatura($item->object_id))
                continue;

            if (! in_array($item->object_id, $ids)) {

                array_push($ids, $item->object_id);
            }
        }
    }
    ;

    $args = array(
        'post_type' => 'product',
        'post__in' => $ids,
        'product_cat' => 'discipline',
        'post_status' => $status,
        'posts_per_page' => $maximo,
        'orderby' => $order_by,
        'order' => $order
    );

    $produtos = array();

    foreach (get_posts($args) as $item) {
        $product = get_product($item->ID);
        array_push($produtos, $product);
    }

    return $produtos;
}

function get_todos_produtos_por_coordenador($user_id, $maximo = -1)
{
    return get_produtos_por_coordenador($user_id, $maximo, 'title', 'ASC', 'any');
}

/*
 * Feito com base no método abaixo
 */
function is_autor_produto($product_id, $author_id)
{
    $args = array(
        'post_type' => 'product',
        'product_cat' => 'discipline',
        'post_status' => 'publish',
        'product_id' => $product_id,
        'meta_query' => array(
            array(
                'key' => '_authors',
                'value' => '"' . $author_id . '"',
                'compare' => 'LIKE'
            )
        )
    );

    return count(get_posts($args)) > 0;
}

/**
 * *********************************************************************
 * Recupera os produtos PUBLICADOS de um autor
 * TODO: Fazer merge com a função abaixo
 * *********************************************************************
 */
function get_produtos_por_autor($author_id, $maximo = -1, $order_by = 'title', $order = 'ASC')
{
    $args = array(
        'post_type' => 'product',
        'product_cat' => 'discipline',
        'post_status' => 'publish',
        'posts_per_page' => $maximo,
        'orderby' => $order_by,
        'order' => $order,
        'meta_query' => array(
            array(
                'key' => '_authors',
                'value' => '"' . $author_id . '"',
                'compare' => 'LIKE'
            )
        )
    );

    // $pacotes = array();
    // $cursos = array();
    $produtos = array();

    foreach (get_posts($args) as $item) {

        if (isset($_GET['kdebug']) && $_GET['kdebug'] == 1) {
            print_r($args);
            exit();
        }

        $product = get_product($item->ID);
        array_push($produtos, $product);
        // if(is_pacote($product)) {
        // array_push($pacotes, $product);
        // } else {
        // array_push($cursos, $product);
        // }
    }

    // return array_merge($pacotes, $cursos);
    return $produtos;
}

/**
 * *********************************************************************
 * Recupera os produtos de um autor (INDEPENDENTE de status)
 * TODO: Fazer merge com função acima
 * *********************************************************************
 */
function get_todos_produtos_por_autor($author_id, $maximo = -1)
{
    $args = array(
        'post_type' => 'product',
        'product_cat' => 'discipline',
        'post_status' => 'any',
        'posts_per_page' => $maximo,
        'orderby' => 'title',
        'order' => 'ASC',
        'meta_query' => array(
            array(
                'key' => '_authors',
                'value' => '"' . $author_id . '"',
                'compare' => 'LIKE'
            )
        )
    );

    $pacotes = array();
    $cursos = array();

    foreach (get_posts($args) as $item) {
        $product = get_product($item->ID);

        if (is_pacote($product)) {
            array_push($pacotes, $product);
        } else {
            array_push($cursos, $product);
        }
    }

    return array_merge($pacotes, $cursos);
}

function get_cursos_por_autor($author_id, $maximo = -1, $orderby = 'title', $order = 'ASC', $meta_key = null)
{
    $args = array(
        'post_type' => 'product',
        'product_cat' => 'discipline',
        'post_status' => 'publish',
        'posts_per_page' => $maximo,
        'orderby' => $orderby,
        'order' => $order,
        'meta_query' => array(
            array(
                'key' => '_authors',
                'value' => '"' . $author_id . '"',
                'compare' => 'LIKE'
            )
        )
    );

    if ($meta_key) {
        $args['meta_key'] = $meta_key;
    }

    $cursos = array();

    foreach (get_posts($args) as $item) {
        if (is_produto_expirado($item->ID))
            continue;

        $product = get_product($item->ID);

        if (! is_pacote($product)) {
            array_push($cursos, $product);
        }
    }

    return $cursos;
}

function get_todos_cursos_por_autor($author_id, $maximo = -1, $orderby = 'title', $order = 'ASC', $meta_key = null)
{
    $args = array(
        'post_type' => 'product',
        'product_cat' => 'discipline',
        'post_status' => 'any',
        'posts_per_page' => $maximo,
        'orderby' => $orderby,
        'order' => $order,
        'meta_query' => array(
            array(
                'key' => '_authors',
                'value' => '"' . $author_id . '"',
                'compare' => 'LIKE'
            )
        )
    );

    if ($meta_key) {
        $args['meta_key'] = $meta_key;
    }

    $cursos = array();

    foreach (get_posts($args) as $item) {
        $product = get_product($item->ID);

        if (! is_pacote($product)) {
            array_push($cursos, $product);
        }
    }

    return $cursos;
}

function get_pacotes_por_autor($author_id, $maximo = -1, $orderby = 'title', $order = 'ASC', $meta_key = null)
{
    $args = array(
        'post_type' => 'product',
        'product_cat' => 'discipline',
        'post_status' => 'publish',
        'posts_per_page' => $maximo,
        'orderby' => $orderby,
        'order' => $order,
        'meta_query' => array(
            array(
                'key' => '_authors',
                'value' => '"' . $author_id . '"',
                'compare' => 'LIKE'
            )
        )
    );

    if ($meta_key) {
        $args['meta_key'] = $meta_key;
    }

    $pacotes = array();

    foreach (get_posts($args) as $item) {
        $product = get_product($item->ID);

        if (is_pacote($product)) {
            array_push($pacotes, $product);
        }
    }

    return $pacotes;
}

function get_destaques_por_categoria($categoria_nome)
{
    $args = array(
        'product_cat' => $categoria_nome . "+area-destaque",
        'post_type' => 'product',
        'posts_per_page' => - 1,
        'orderby' => 'title',
        'order' => 'ASC'
    );

    $destaques = array();

    foreach (get_posts($args) as $item) {
        if (is_produto_expirado($item->ID))
            continue;

        $product = get_product($item->ID);
        array_push($destaques, $product);
    }

    return $destaques;
}

function get_pacotes_por_categoria($categoria_nome, $excluir_destaques = false)
{
    $args = array(
        'product_cat' => $categoria_nome . "+pacote",
        'post_type' => 'product',
        'posts_per_page' => - 1,
        'orderby' => 'title',
        'order' => 'ASC'
    );

    $pacotes = array();

    foreach (get_posts($args) as $item) {

        if (is_produto_expirado($item->ID))
            continue;

        if ($excluir_destaques && is_area_destaque($item->ID)) {
            continue;
        }

        $product = get_product($item->ID);
        array_push($pacotes, $product);
    }

    return $pacotes;
}

function get_categorias_de_curso($cat_number)
{
    global $wpdb;
    return $wpdb->get_results("SELECT * FROM wp_term_taxonomy tt JOIN wp_terms t ON tt.term_id = t.term_id WHERE parent=$cat_number ORDER BY t.name");
}

function listar_todas_disciplinas()
{
    return get_categorias_de_curso(34);
}

function listar_todos_concursos()
{
    return get_categorias_de_curso(33);
}

function get_cursos_por_categoria($categoria_nome, $post = false, $incluir_simulado = false, $excluir_destaques = false, $somente_pagos = false, $excluir_pacotes = true, $somente_visiveis = FALSE)
{
    $args = array(
        'post_status' => 'publish',
        'product_cat' => $categoria_nome,
        'post_type' => 'product',
        'posts_per_page' => - 1,
        'orderby' => 'title',
        'order' => 'ASC'
    );

    $cursos = array();

    foreach (get_posts($args) as $curso) {

        if (! $incluir_simulado && is_produto_simulado($curso->ID))
            continue;

        if (is_produto_expirado($curso->ID))
            continue;

        if ($excluir_pacotes && tem_categoria_pacote($curso->ID))
            continue;

        if ($excluir_destaques && is_area_destaque($curso->ID)) {
            continue;
        }

        if ($somente_visiveis) {
            $p = new WC_Product($curso->ID);
            if ($p->get_catalog_visibility() == 'hidden') {
                continue;
            }
        }

        if ($post) {
            array_push($cursos, $curso);
        } else {

            $product = wc_get_product($curso->ID);

            if ($somente_pagos && $product->get_price() == 0) {
                continue;
            }

            array_push($cursos, $product);
        }
    }

    return $cursos;
}

function contar_todos_produtos_por_categoria($categoria_nome)
{
    $args = array(
        'post_status' => 'publish',
        'product_cat' => $categoria_nome,
        'post_type' => 'product',
        'posts_per_page' => - 1,
        'orderby' => 'title',
        'order' => 'ASC'
    );

    $num = 0;

    foreach (get_posts($args) as $curso) {
        if (is_produto_expirado($curso->ID))
            continue;

        $num ++;
    }

    return $num;
}

function get_produtos_por_categoria($categoria_nome, $post = false, $incluir_simulado = false)
{
    $args = array(
        'post_status' => 'publish',
        'product_cat' => $categoria_nome,
        'post_type' => 'product',
        'posts_per_page' => - 1,
        'orderby' => 'title',
        'order' => 'ASC'
    );

    $cursos = array();
    foreach (get_posts($args) as $curso) {

        // if(!$incluir_simulado && is_produto_simulado($curso->ID)) continue;

        // if(is_produto_expirado($curso->ID)) continue;

        $product = wc_get_product($curso->ID);

        if ($post)
            array_push($cursos, $curso);
        else
            array_push($cursos, $product);
    }

    return $cursos;
}

/**
 * Lista todos os produtos com categoria COACHING
 *
 * [JOULE][J1] UPGRADE - Cadastro automático de pagamentos do coaching no Podio
 *
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/upgrades/items/193
 *
 * @since 10.0.0
 *       
 * @return array Lista de WC_Products
 */
function listar_produtos_coaching()
{
    return get_produtos_por_categoria(COACHING);
}

/**
 * Retorna todos os produtos com categoria COACHING em dados formatos para combobox
 *
 * [JOULE][J1] UPGRADE - Cadastro automático de pagamentos do coaching no Podio
 *
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/upgrades/items/193
 *
 * @since 10.0.0
 *       
 * @return array Lista de [id => titulo]
 */
function get_produtos_coaching_combo()
{
    $combo = [];

    foreach (listar_produtos_coaching() as $item) {
        $combo[$item->id] = $item->post->post_title;
    }

    return $combo;
}

/**
 *
 * @param int $tipo
 *            1 => somente não pacotes, 2 => somente pacotes
 */
function get_simulados_por_categoria($categoria_nome, $post = false, $tipo = TIPO_SIMULADO_SOMENTE_CURSOS)
{
    $args = array(
        'post_status' => 'publish',
        'product_cat' => $categoria_nome,
        'post_type' => 'product',
        'posts_per_page' => - 1,
        'orderby' => 'title',
        'order' => 'ASC'
    );

    $cursos = array();
    foreach (get_posts($args) as $curso) {

        if (! is_produto_simulado($curso->ID))
            continue;

        if (is_produto_expirado($curso->ID))
            continue;

        $product = wc_get_product($curso->ID);

        if ($tipo == TIPO_SIMULADO_SOMENTE_CURSOS && tem_categoria_pacote($curso->ID)) {
            continue;
        }

        if ($tipo == TIPO_SIMULADO_SOMENTE_PACOTES && ! tem_categoria_pacote($curso->ID)) {
            continue;
        }

        if ($post)
            array_push($cursos, $curso);
        else
            array_push($cursos, $product);
    }

    return $cursos;
}

function listar_simulados($offset = 0, $limit = null, $somente_gratuitos = false, $titulo = null)
{
    KLoader::helper("StringHelper");
    
    $args = array(
        'post_status' => 'publish',
        'product_cat' => 'simulados',
        'post_type' => 'product',
        'posts_per_page' => - 1,
        'orderby' => 'title',
        'order' => 'ASC'
    );

    $simulados = array();
    foreach (get_posts($args) as $curso) {

        if (is_produto_expirado($curso->ID)) {
            continue;
        } else {
            $product = wc_get_product($curso->ID);
            $simulados[$curso->ID] = $product;
        }
    }

    krsort($simulados);

    $s = [];
    foreach ($simulados as $item) {
        if ($somente_gratuitos) {
            if ($item->get_price() != 0) {
                continue;
            }
        }

        if ($titulo) {
            if (strpos(StringHelper::strtolower_sem_acento($item->post->post_title), StringHelper::strtolower_sem_acento($titulo)) === false) {
                continue;
            }
        }

        array_push($s, $item);
    }

    return array_slice($s, $offset, $limit);
}

function is_pacote($produto)
{
    return WC_Product_Factory::get_product_type($produto->get_id()) == 'bundle' ? true : false;
}

function salvar_autores_em_produto($post_id, $autores, $percentuais = null)
{
    update_post_meta($post_id, '_authors', $autores);

    if (! is_null($percentuais)) {
        update_post_meta($post_id, '_percentuais', $percentuais);
    }
}

function salvar_categorias_em_produto($post_id, $categorias)
{
    $categorias = array_unique($categorias);
    wp_set_object_terms($post_id, $categorias, 'product_cat');
}

function get_product_url($post_id)
{
    $post = get_post($post_id);
    return $post->guid;
}

function get_product_link($post_id, $class = '')
{
    $post = get_post($post_id);

    return "<a class='{$class}' href='/produto/{$post->post_name}'>{$post->post_title}</a>";
}

function get_soma_produtos_pacote($pacote)
{
    $total = 0;

    if ($itens = $pacote->get_bundled_items()) {

        foreach ($itens as $item) {
            $bundle_product = $item->get_product();
            $total += $bundle_product->get_price();
        }
    }

    return $total;
}

function get_qtde_questoes($post_id)
{
    $questoes = get_post_meta($post_id, 'qtde_questoes', true);

    return empty($questoes) ? 0 : $questoes;
}

function get_qtde_esquemas($post_id)
{
    $esquemas = get_post_meta($post_id, 'qtde_esquemas', true);

    return empty($esquemas) ? 0 : $esquemas;
}

function get_qtde_aulas($post_id)
{
    $aulas = get_post_meta($post_id, 'qtde_aulas', true);

    return empty($aulas) ? 0 : $aulas;
}

function ordenar_produtos_do_pacote($produtos)
{
    count($produtos);

    usort($produtos, "cmp_produtos_pacote");
    return $produtos;
}

function cmp_produtos_pacote($a, $b)
{
    return strcmp($a->post->post_title, $b->post->post_title);
}

function has_cronograma($post_id)
{
    $aulas = get_post_meta($post_id, 'aulas_nome', true);

    return ! empty($aulas);
}

function get_cronograma_aula_por_index($post_id, $index)
{
    $cronograma = get_cronograma($post_id);

    if ($cronograma) {
        return $cronograma[$index];
    }
    return null;
}

/*
function get_cronograma_aula_por_nome($post_id, $aula_nome, $data_pedido = null)
{
    $cronograma = get_cronograma($post_id, $data_pedido);

    if ($cronograma) {
        foreach ($cronograma as $aula) {
            if (trim($aula_nome) == trim($aula['nome'])) {
                return $aula;
            }
        }
    }
    return null;
}
*/

function get_cronograma($post_id, $data_pedido = null)
{
    $demo_index = get_post_meta($post_id, 'aula_demo', true);
    $aulas_nome = get_post_meta($post_id, 'aulas_nome', true);

    if (! $aulas_nome)
        return false;

    $aulas_data = get_post_meta($post_id, 'aulas_data', true);
    $aulas_liberacao = get_post_meta($post_id, 'aulas_liberacao', true);
    $aulas_descricao = get_post_meta($post_id, 'aulas_descricao', true);
    $aulas_arquivo = get_post_meta($post_id, 'aulas_arquivo', true);

    $liberar_gradualmente = get_post_meta($post_id, 'liberar_gradualmente', true) == 'yes';

    $cronograma = array();
    for ($i = 0; $i < count($aulas_nome); $i ++) {

        $hoje = date('Y-m-d 12:00');
        $timestamp_hoje = strtotime($hoje);
        
        // calcula disponibilidade para liberação gradual de aulas
        if ($liberar_gradualmente) {  
            $data_base = $data_pedido ?: date("Y-m-d");
            $data_aula = date('Y-m-d 12:00', strtotime($data_base . " +" . ($aulas_liberacao[$i] ?: 0) . " days "));
        } else {
            $data_aula = date('Y-m-d 12:00', strtotime(str_replace('/', '-', $aulas_data[$i])));
        }
        
        $timestamp_data_aula = strtotime($data_aula);

        $disponivel = null;
        // print_r("<br/>Tem arquivo: ");
        // print_r(tem_arquivo_aula($post_id, $i));
        if (tem_arquivo_aula($post_id, $i)) {

            if ($timestamp_hoje >= $timestamp_data_aula) {
                $disponivel = 'Disponível';
                $disponivel_minha_conta = 'Aula Disponível';
                $is_disponivel = true;
            } else {
                $disponivel = 'Em ' . converter_para_ddmmyyyy($data_aula);
                $disponivel_minha_conta = 'Disponível em ' . converter_para_ddmmyyyy($data_aula);
                $is_disponivel = false;
            }
        } else {

            if ($timestamp_hoje > $timestamp_data_aula) {
                $disponivel = 'Em breve';
                $disponivel_minha_conta = 'Disponível em breve';
                $is_disponivel = false;
            } else {
                $disponivel = 'Em ' . converter_para_ddmmyyyy($data_aula);
                $disponivel_minha_conta = 'Disponível em ' . converter_para_ddmmyyyy($data_aula);
                $is_disponivel = false;
            }
        }

        $demo = is_null($demo_index) || ($demo_index == "") ? FALSE : $i == $demo_index;

        $aula = array(
            'nome' => $aulas_nome[$i],
            'data' => converter_para_ddmmyyyy($data_aula),
            'descricao' => $aulas_descricao[$i],
            'arquivo' => $aulas_arquivo[$i],
            'disponivel' => $disponivel,
            'disponivel_minha_conta' => $disponivel_minha_conta,
            'is_disponivel' => $is_disponivel,
            'demo' => $demo,
            'index_demo' => $demo_index
        );
        array_push($cronograma, $aula);
    }

    return $cronograma;
}

function get_nome_aula_by_file($post_id, $file, $ignorar_path = FALSE, &$tipo_aula = NULL)
{
    $aulas_nome = get_post_meta($post_id, 'aulas_nome', true);
    $aulas_arquivo = get_post_meta($post_id, 'aulas_arquivo', TRUE);

    $tipo_aula = TIPO_AULA_PDF;

    $result = busca_nome_aula_by_file_e_tipo( $aulas_arquivo, $aulas_nome, $file, $ignorar_path );
    
    if(!$result)
    {
        $tipo_aula = TIPO_AULA_RESUMO;
        $aulas_resumo = get_post_meta($post_id, 'aulas_resumo', TRUE);
        $result = busca_nome_aula_by_file_e_tipo( $aulas_resumo, $aulas_nome, $file, $ignorar_path );
    }

    
    if(!$result)
    {
        $tipo_aula = TIPO_AULA_MAPA;
        $aulas_mapa = get_post_meta($post_id, 'aulas_mapa', TRUE);
        $result = busca_nome_aula_by_file_e_tipo( $aulas_mapa, $aulas_nome, $file, $ignorar_path );
    }

    return $result;
}

function busca_nome_aula_by_file_e_tipo($aulas_tipo, $aulas_nome, $file, $ignorar_path )
{
    if($aulas_tipo)
    {
        foreach ($aulas_tipo as $i => $aula) {
            if ($aula) {
                if (is_array($aula)) {
                    foreach ($aula as $arquivo) {
                        if($ignorar_path == TRUE)
                        {
                            $arquivo = basename($arquivo);
                        }
                        if ($arquivo == $file) {
                            return trim($aulas_nome[$i]);
                        }
                    }
                } else {
                    if($ignorar_path == TRUE)
                    {
                        $aula =  basename($aula);
                    }
                    if ($aula == $file) {
                        return trim($aulas_nome[$i]);
                    }
                }
            }
        }
    }

    return NULL;
}

/**
 * *************************************************************
 * Verifica se a aula estah disponivel (incluindo verificacao
 * de existencia de arquivo) use "is_aula_disponivel_para_aluno()"
 * *************************************************************
 */
function is_aula_disponivel_para_aluno($produto_id, $index, $pedido_id = null)
{
    $aulas_nome = get_post_meta($produto_id, 'aulas_nome', true);
    $aulas_data = get_post_meta($produto_id, 'aulas_data', true);
    $aulas_liberacao = get_post_meta($produto_id, 'aulas_liberacao', true);
    $aulas_descricao = get_post_meta($produto_id, 'aulas_descricao', true);

    $liberar_gradualmente = get_post_meta($produto->id, 'liberar_gradualmente', true) == 'yes';

    if ($liberar_gradualmente && $pedido_id) {
        $order = new WC_Order($pedido_id);
        $data_aula = converter_para_yyyymmdd($order->get_date_completed() . format('Y-m-d') . " + " . ($aulas_liberacao[$index] ?: 0) . " days");
    } else {
        $data_aula = date('Y-m-d', strtotime(str_replace('/', '-', $aulas_data[$index])));
    }
    $timestamp_data_aula = strtotime($data_aula);
    $hoje = date('Y-m-d');
    $timestamp_hoje = strtotime($hoje);

    $disponivel = null;
    if ($timestamp_hoje >= $timestamp_data_aula) {
        return tem_arquivo_aula($produto_id, $index);
    } else {
        return false;
    }
}

function tem_arquivo_aula($post_id, $index)
{
    $produto = get_produto_by_id($post_id);
    $arquivos = $produto->get_files();
    $aulas_arquivo = get_post_meta($post_id, 'aulas_arquivo', true);
    $aula_arquivo = $aulas_arquivo[$index];

//     print_r($arquivos);
    
    foreach ($arquivos as $arquivo) {

        $file = ($arquivo instanceof WC_Product_Download) ? $arquivo->get_file() : $arquivo['file'];
        
//         echo $file;

        if (is_array($aula_arquivo)) {
            foreach ($aula_arquivo as $item) {

                if ($item == $file) {
                    return true;
                }
            }
        } else {
            if ($aula_arquivo == $file) {
                return true;
            }
        }
    }

    if (contar_videos_aula_vimeo($post_id, $index))
        return true;
    
    if (contar_cadernos_aula($post_id, $index))
        return true;

    if (contar_resumos_aula($post_id, $index))
        return true;

    if (contar_mapas_aula($post_id, $index))
        return true;

    return false;
}

function is_arquivo_formato_antigo($aulas_arquivo)
{
    if ($aulas_arquivo) {

        foreach ($aulas_arquivo as $value) {
            if (is_array($value)) {
                return FALSE;
            }
        }

        return TRUE;
    }

    return NULL;
}

function contar_cadernos_aula($post_id, $index)
{
    return contar_aulas_por_tipo( 'aulas_caderno', $post_id, $index );
}

function contar_videos_aula_vimeo($post_id, $index)
{
    return contar_aulas_por_tipo( 'aulas_vimeo', $post_id, $index );
}

function contar_resumos_aula($post_id, $index)
{
    return contar_aulas_por_tipo( 'aulas_resumo', $post_id, $index );
}

function contar_mapas_aula($post_id, $index)
{
    return contar_aulas_por_tipo( 'aulas_mapa', $post_id, $index );
}

function contar_aulas_por_tipo($aulas_tipo, $post_id, $index)
{
    $num = 0;

    $aulas_vimeo = get_post_meta($post_id, $aulas_tipo, TRUE);

    if ($aulas_vimeo) {
        foreach ($aulas_vimeo[$index] as $item) {
            if ($item) {
                $num ++;
            }
        }
    }

    return $num;
}

function listar_videos_aula_vimeo($post_id, $index)
{
    $retorno = [];

    $aulas_vimeo = get_post_meta($post_id, 'aulas_vimeo', TRUE);

    if ($aulas_vimeo) {
        foreach ($aulas_vimeo[$index] as $item) {
            if ($item) {
                array_push($retorno, $item);
            }
        }
    }

    return $retorno;
}

function contar_arquivos_aula_por_extensao($post_id, $index, $extensoes)
{
    $num = 0;

    $aulas_arquivo = get_post_meta($post_id, 'aulas_arquivo', TRUE);

    if (is_arquivo_formato_antigo($aulas_arquivo)) {
        $arquivos = array(
            $aulas_arquivo[$index]
        );
    } else {
        if (is_array($aulas_arquivo)) {
            $arquivos = $aulas_arquivo[$index];
        }
    }

    if ($arquivos && is_array($arquivos)) {
        foreach ($arquivos as $file) {
            // $file = $arquivo['file'];

            $file_extensao = get_extensao($file);

            // se não for array joga dentro de um array
            if (! is_array($extensoes)) {
                $extensoes = array(
                    $extensoes
                );
            }

            foreach ($extensoes as $extensao) {
                if ($extensao == $file_extensao) {
                    $num ++;
                }
            }
        }

        return $num;
    }

    return 0;
}

function listar_arquivos_aula_por_extensao($post_id, $index, $extensoes)
{
    $aulas_arquivo = get_post_meta($post_id, 'aulas_arquivo', TRUE);

    if (is_arquivo_formato_antigo($aulas_arquivo)) {
        $arquivos = array(
            $aulas_arquivo[$index]
        );
    } else {
        if (is_array($aulas_arquivo)) {
            $arquivos = $aulas_arquivo[$index];
        }
    }

    $retorno = [];

    if ($arquivos && is_array($arquivos)) {
        foreach ($arquivos as $file) {

            $file_extensao = get_extensao($file);

            // se não for array joga dentro de um array
            if (! is_array($extensoes)) {
                $extensoes = array(
                    $extensoes
                );
            }

            foreach ($extensoes as $extensao) {
                if ($extensao == $file_extensao) {
                    array_push($retorno, $file);
                }
            }
        }
    }

    return $retorno;
}

function listar_arquivos_aula($post_id, $nome_aula)
{
    $resultado = array();

    $produto = get_produto_by_id($post_id);
    $arquivos = $produto->get_files();

    foreach ($arquivos as $chave => $arquivo) {

        if (starts_with($arquivo['name'], $nome_aula)) {
            $resultado[$chave] = $arquivo;
        }
    }

    return $resultado;
}

function get_produto_by_post_name($post_name)
{
    global $wpdb;
    $post = $wpdb->get_var($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE post_name = %s AND post_type='product'", $post_name));
    if ($post)
        return get_post($post, OBJECT);

    return null;
}

function get_produto_by_forum_id($forum_id)
{
    $produto_id = get_post_meta($forum_id, PRODUTO_ID_CHAVE, true);

    if ($produto_id)
        return get_produto_by_id($produto_id);

    return null;
}

function comprou_produto($usuario_id, $produto_id)
{
    global $wpdb;

    $sql = "SELECT DISTINCT(pm.meta_value) as user_id FROM  wp_woocommerce_order_itemmeta om 
		JOIN wp_woocommerce_order_items oi ON om.order_item_id = oi.order_item_id 
		JOIN wp_postmeta pm ON pm.post_id = oi.order_id 
		JOIN wp_posts p ON pm.post_id = p.ID
		WHERE p.post_status LIKE 'wc-completed' AND om.meta_key LIKE  '_product_id' AND  om.meta_value LIKE '$produto_id' AND pm.meta_key LIKE '_customer_user' AND pm.meta_value = '$usuario_id'";

    return $wpdb->get_results($sql) ? true : false;
}

/**
 * Retorna o número do pedido em que um usuário comprou um produto
 *
 * @since Joule 3
 *       
 * @param int $usuario_id
 * @param int $produto_id
 *
 * @return int
 */
function get_pedido_usuario_comprou_produto($usuario_id, $produto_id)
{
    global $wpdb;

    $sql = "SELECT DISTINCT(oi.order_id) as order_id FROM  wp_woocommerce_order_itemmeta om 
		JOIN wp_woocommerce_order_items oi ON om.order_item_id = oi.order_item_id 
		JOIN wp_postmeta pm ON pm.post_id = oi.order_id 
		JOIN wp_posts p ON pm.post_id = p.ID
		WHERE p.post_status LIKE 'wc-completed' AND om.meta_key LIKE  '_product_id' AND  om.meta_value LIKE '$produto_id' AND pm.meta_key LIKE '_customer_user' AND pm.meta_value = '$usuario_id'
        ORDER BY order_id DESC";

    return ($resultado = $wpdb->get_row($sql)) ? $resultado->order_id : null;
}

/**
 * Retorna a data do último pedido que um usuário comprou determinado produto
 *
 * @since K5
 *
 * @param int $usuario_id
 * @param int $produto_id
 *
 * @return string Data no formato Y-m-d
 */

function get_data_pedido_usuario_comprou_produto($usuario_id, $produto_id)
{  
    if(!$usuario_id) {
        return null;
    }

    if ($pedido_id = get_pedido_usuario_comprou_produto($usuario_id, $produto_id)) {
        $pedido = new WC_Order($pedido_id);

        if($pedido) {
            $data_pedido = $pedido->get_date_completed()?:$pedido->get_date_created();
            return $data_pedido->format('Y-m-d');
        }
        
        return null;
    }
    
    return null;
}

function get_usuarios_que_compraram($produto_id)
{
    global $wpdb;

    $sql = "SELECT DISTINCT(pm.meta_value) as user_id FROM  wp_woocommerce_order_itemmeta om 
		JOIN wp_woocommerce_order_items oi ON om.order_item_id = oi.order_item_id 
		JOIN wp_postmeta pm ON pm.post_id = oi.order_id 
		JOIN wp_posts p ON pm.post_id = p.ID
		WHERE p.post_status LIKE 'wc-completed' AND om.meta_key LIKE  '_product_id' AND  om.meta_value LIKE '$produto_id' AND pm.meta_key LIKE '_customer_user'";

    $result = $wpdb->get_results($sql);

    $usuarios = array();
    foreach ($result as $row) {
        array_push($usuarios, get_userdata($row->user_id));
    }

    return $usuarios;
}

function get_usuarios_que_compraram_por_dias_apos_compra($produto_id, $dias_apos_compra)
{
    global $wpdb;

    $sql = "SELECT DISTINCT pm.meta_value as user_id, oi.order_id as order_id FROM  wp_woocommerce_order_itemmeta om 
		JOIN wp_woocommerce_order_items oi ON om.order_item_id = oi.order_item_id 
		JOIN wp_postmeta pm ON pm.post_id = oi.order_id 
		JOIN wp_posts p ON pm.post_id = p.ID
		WHERE p.post_status LIKE 'wc-completed' AND om.meta_key LIKE  '_product_id' AND  om.meta_value LIKE '$produto_id' AND pm.meta_key LIKE '_customer_user'";

    $result = $wpdb->get_results($sql);

    $usuarios = array();
    foreach ($result as $row) {
        $order = new WC_Order($row->order_id);
        $data_pedido = $order->get_date_completed()?:$order->get_date_created();

        if (hoje_yyyymmdd() == converter_para_yyyymmdd($data_pedido->format('Y-m-d') . " + " . ($dias_apos_compra ?: 0) . " days")) {
            array_push($usuarios, get_userdata($row->user_id));
        }
    }

    return $usuarios;
}

function contar_usuarios_que_compraram($produto_id)
{
    global $wpdb;

    $sql = "SELECT COUNT(DISTINCT pm.meta_value) as soma FROM  wp_woocommerce_order_itemmeta om
	JOIN wp_woocommerce_order_items oi ON om.order_item_id = oi.order_item_id
	JOIN wp_postmeta pm ON pm.post_id = oi.order_id
	JOIN wp_posts p ON pm.post_id = p.ID
	WHERE p.post_status LIKE 'wc-completed' AND om.meta_key LIKE  '_product_id' AND  om.meta_value LIKE '$produto_id' AND pm.meta_key LIKE '_customer_user'";

    $result = $wpdb->get_row($sql);
    return $result->soma;
}

function contar_produtos_por_autor($autor_id)
{
    $produtos = get_cursos_por_autor($autor_id);
    return count($produtos);
}

function contar_produtos_por_categoria($categoria_nome, $incluir_simulado = false)
{
    $produtos = get_cursos_por_categoria($categoria_nome, false, $incluir_simulado, FALSE, FALSE, FALSE, TRUE);

    return count($produtos);
}

function get_valor_percentual_item_pacote($pacote_id, $item_id)
{
    $pacote = new WC_Product_Bundle($pacote_id);
    $item = new WC_Product($item_id);

    $total = 0;
    $bundled_items = $pacote->get_bundled_items();

    foreach ($bundled_items as $bundled_item) {
        $product = new WC_Product($bundled_item->product_id);
        $total += $product->price;
    }

    return $item->price / $total;
}

function get_preco_parcela_produto($item, $parcelas = 1)
{
    $preco = is_pacote($item) ? $item->get_bundle_price() : $preco = $item->get_price();
    return $preco / $parcelas;
}

function get_nome_categoria_by_slug($slug)
{
    global $wpdb;
    $result = $wpdb->get_row("SELECT * FROM wp_terms where slug='$slug'");
    return $result->name;
}

function get_editar_produto_url($produto_id)
{
    return "/wp-admin/post.php?post={$produto_id}&action=edit";
}

/**
 * *************************************************************
 * Verifica se uma aula jah estah disponivel para download
 * Checa somente a se a data da aula estah disponivel
 * Essa funcao eh usada exclusivamente na Minha Conta.
 * Nessa pagina, previamente sao selecionadas aulas que possuem
 * arquivos no Woo, não sendo necessario checar se o arquivo existe
 *
 * Para verificar se a aula estah disponivel (incluindo verificacao
 * de existencia de arquivo) use "is_aula_disponivel_para_aluno()"
 * *************************************************************
 */
function is_aula_disponivel_para_download($produto_id, $nome_aula, $pedido_id = null)
{
    if (! tem_cronograma($produto_id))
        return true;

    $aulas = get_post_meta($produto_id, 'aulas_nome', true);

    // recupera o indice da aula
    $index = null;
    for ($i = 0; $i < count($aulas); $i ++) {
        if (trim($nome_aula) == trim($aulas[$i])) {
            $index = $i;
            break;
        }
    }

    // echo $index;

    // verifica se data da aula eh menor ou igual a data atual
    if (! is_null($index)) {
        $datas = get_post_meta($produto_id, 'aulas_data', true);
        $dias_liberacao = get_post_meta($produto_id, 'aulas_liberacao', true);
        $liberar_gradualmente = get_post_meta($produto_id, 'liberar_gradualmente', true) == 'yes';

        if ($pedido_id && $liberar_gradualmente) {
            $order = new WC_Order($pedido_id);
            $data_pedido = $order->get_date_completed()?:$order->get_date_created();
            $data_pedido = $data_pedido->format('Y-m-d');
            $data_aula = converter_para_yyyymmdd($data_pedido . " + " . ($dias_liberacao[$index] ?: 0) . " days ");
        } else {
            $data_aula = converter_para_yyyymmdd($datas[$index]);
        }

        if (hoje_yyyymmdd() >= $data_aula) {
            return true;
        }

        return false;
    }

    return false;
}

function is_produto_gratis($product)
{
    return $product->get_price() == 0 ? true : false;
}

function is_woocommerce_produto_gratis($product)
{
    return (strpos($product->get_price_html(), '<ins>') === false) && (strpos($product->get_price_html(), 'amount') === false);
}

function listar_categorias($post_id)
{
    return get_the_terms($post_id, 'product_cat');
}

function is_categoria_disciplina($categoria)
{
    return $categoria->parent == 34 ? true : false;
}

function is_categoria_area($categoria)
{
    return $categoria->parent == CATEGORIA_AREA ? true : false;
}

function is_categoria_simulado($categoria)
{
    return $categoria->term_id == 124 ? true : false;
}

function is_categoria_cursos_regulares($categoria)
{
    return is_categoria_cursos_regulares_fiscal($categoria)
        || is_categoria_cursos_regulares_controle($categoria)
        || is_categoria_cursos_regulares_policial($categoria)
        || is_categoria_cursos_regulares_policia_militar($categoria)
        || is_categoria_cursos_regulares_policia_civil($categoria)
        || is_categoria_cursos_regulares_bombeiros($categoria)
        || is_categoria_cursos_regulares_agepen($categoria)
        || is_categoria_cursos_regulares_depen($categoria);
}

/** @deprecated  */
function is_categoria_cursos_regulares_fiscal_controle($categoria)
{
    return $categoria->slug == CATEGORIA_CURSO_REGULAR_FISCAL_CONTROLE ? true : false;
}

function is_categoria_cursos_regulares_fiscal($categoria)
{
    return $categoria->slug == CATEGORIA_CURSO_REGULAR_FISCAL ? true : false;
}

function is_categoria_cursos_regulares_controle($categoria)
{
    return $categoria->slug == CATEGORIA_CURSO_REGULAR_CONTROLE ? true : false;
}

function is_categoria_cursos_regulares_policial($categoria)
{
    return $categoria->slug == CATEGORIA_CURSO_REGULAR_POLICIAL ? true : false;
}

function is_categoria_cursos_regulares_policia_militar($categoria)
{
    return $categoria->slug == CATEGORIA_CURSO_REGULAR_POLICIA_MILITAR ? true : false;
}

function is_categoria_cursos_regulares_policia_civil($categoria)
{
    return $categoria->slug == CATEGORIA_CURSO_REGULAR_POLICIA_CIVIL ? true : false;
}

function is_categoria_cursos_regulares_bombeiros($categoria)
{
    return $categoria->slug == CATEGORIA_CURSO_REGULAR_BOMBEIROS ? true : false;
}

function is_categoria_cursos_regulares_agepen($categoria)
{
    return $categoria->slug == CATEGORIA_CURSO_REGULAR_AGEPEN ? true : false;
}

function is_categoria_cursos_regulares_depen($categoria)
{
    return $categoria->slug == CATEGORIA_CURSO_REGULAR_DEPEN ? true : false;
}

function is_categoria_trf_regular($categoria)
{
    return $categoria->slug == CATEGORIA_TRF_REGULAR ? true : false;
}

function is_categoria_tj_regular($categoria)
{
    return $categoria->slug == CATEGORIA_TJ_REGULAR ? true : false;
}

function is_categoria_tre_regular($categoria)
{
    return $categoria->slug == CATEGORIA_TRE_REGULAR ? true : false;
}

function is_categoria_trt_regular($categoria)
{
    return $categoria->slug == CATEGORIA_TRT_REGULAR ? true : false;
}

function is_categoria_mapas_mentais($categoria)
{
    return $categoria->term_id == 103 ? true : false;
}

function is_categoria_tecnicas_estudo($categoria)
{
    return $categoria->term_id == 533 ? true : false;
}

function is_categoria_discursiva($categoria)
{
    return $categoria->slug == CATEGORIA_DISCURSIVA ? true : false;
}

function is_categoria_recurso($categoria)
{
    return $categoria->slug == CATEGORIA_RECURSO ? true : false;
}

function is_categoria_resumo_questoes_comentadas($categoria)
{
    return $categoria->term_id == 400 ? true : false;
}

function is_categoria_caderno_de_questoes($categoria)
{
    return $categoria->slug == 'caderno-questoes' ? true : false;
}

function is_categoria_caderno_de_questoes_online($categoria)
{
    return $categoria->slug == 'caderno-questoes-online' ? true : false;
}

function is_categoria_area_destaque($categoria)
{
    return $categoria->slug == 'area-destaque' ? true : false;
}

function is_categoria_sistema_questoes($categoria)
{
    return in_array($categoria->term_id, [
        211,
        212,
        213,
        214,
        418,
        303,
        124
    ]) ? true : false;
}

/**
 * Verifica se a categoria é coaching
 *
 * [JOULE][J1] UPGRADE - Cadastro automático de pagamentos do coaching no Podio
 *
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/upgrades/items/193
 *
 * @since 10.0.0
 *       
 * @param mixed $categoria
 *            Categoria do produto WP
 *            
 * @return bool
 */
function is_categoria_coaching($categoria)
{
    return $categoria->slug == 'coaching' ? true : false;
}

/**
 * Verifica se a categoria é coaching mensal
 *
 * @since 10.0.0
 *       
 * @param mixed $categoria
 *            Categoria do produto WP
 *            
 * @return bool
 */
function is_categoria_coaching_mensal($categoria)
{
    return $categoria->slug == 'coaching-mensal' ? true : false;
}

/**
 * Verifica se a categoria é coaching trimestral
 *
 * [JOULE][J1] UPGRADE - Cadastro automático de pagamentos do coaching no Podio
 *
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/upgrades/items/193
 *
 * @since 10.0.0
 *       
 * @param mixed $categoria
 *            Categoria do produto WP
 *            
 * @return bool
 */
function is_categoria_coaching_trimestral($categoria)
{
    return $categoria->slug == 'coaching-trimestral' ? true : false;
}

function is_categoria_pdf($categoria)
{
    return $categoria->slug == 'pdf' ? true : false;
}

function is_categoria_video_aula($categoria)
{
    return $categoria->slug == 'videoaula' ? true : false;
}

/**
 * Verifica se a categoria é coaching semestral
 *
 * [JOULE][J1] UPGRADE - Cadastro automático de pagamentos do coaching no Podio
 *
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/upgrades/items/193
 *
 * @since 10.0.0
 *       
 * @param mixed $categoria
 *            Categoria do produto WP
 *            
 * @return bool
 */
function is_categoria_coaching_semestral($categoria)
{
    return $categoria->slug == 'coaching-semestral' ? true : false;
}

function is_categoria_concurso($categoria)
{
    return $categoria->parent == 33 ? true : false;
}

function is_categoria_assinatura_sq_semanal($categoria)
{
    return $categoria->slug == 'assinatura-sq-semanal' ? true : false;
}

function is_categoria_assinatura_sq_quinzenal($categoria)
{
    return $categoria->slug == 'assinatura-sq-quinzenal' ? true : false;
}

function is_categoria_assinatura_sq_mensal($categoria)
{
    return $categoria->slug == 'assinatura-sq-mensal' ? true : false;
}

function is_categoria_assinatura_sq_trimestral($categoria)
{
    return $categoria->slug == 'assinatura-sq-trimestral' ? true : false;
}

function is_categoria_assinatura_sq_semestral($categoria)
{
    return $categoria->slug == 'assinatura-sq-semestral' ? true : false;
}

function is_categoria_assinatura_sq_anual($categoria)
{
    return $categoria->slug == 'assinatura-sq-anual' ? true : false;
}

/**
 * Retorna o tipo do produto assinatura
 *
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/upgrades/items/310
 *
 * @since Joule 2
 *       
 * @param int $produto_id
 *            Id do produto no WP
 *            
 * @return float|false Tipo do produto assinatura ou false se o produto não for do tipo ASSINATURA
 */
function get_produto_assinatura_tipo($produto_id)
{
    $categorias = listar_categorias($produto_id);

    if ($categorias) {

        foreach ($categorias as $categoria) {
            if (is_categoria_assinatura_sq_semanal($categoria)) {
                return ASSINATURA_SEMANAL;
            } elseif (is_categoria_assinatura_sq_quinzenal($categoria)) {
                return ASSINATURA_QUINZENAL;
            } elseif (is_categoria_assinatura_sq_mensal($categoria)) {
                return ASSINATURA_MENSAL;
            } elseif (is_categoria_assinatura_sq_trimestral($categoria)) {
                return ASSINATURA_TRIMESTRAL;
            } elseif (is_categoria_assinatura_sq_semestral($categoria)) {
                return ASSINATURA_SEMESTRAL;
            } elseif (is_categoria_assinatura_sq_anual($categoria)) {
                return ASSINATURA_ANUAL;
            }
        }
    }

    return false;
}

function is_produto_assinatura($produto_id)
{
    $categorias_ids = [ TERM_ASSINATURA_SEMANAL, TERM_ASSINATURA_QUINZENAL, TERM_ASSINATURA_MENSAL, TERM_ASSINATURA_TRIMESTRAL, TERM_ASSINATURA_SEMESTRAL, TERM_ASSINATURA_ANUAL ];
	
    return tem_uma_das_categoria($produto_id, $categorias_ids) ? true : false;
}

function is_produto_acesso_limitado($produto_id)
{
    $categorias = listar_categorias($produto_id);

    if ($categorias) {

        foreach ($categorias as $categoria) {

            if ($categoria->slug == 'acesso-limitado') {
                return true;
            }
        }
    }

    return false;
}

function salvar_atualizacao_aula($product_id, $aula_nome, $usuario_id)
{
    global $wpdb;

    $aula_nome = trim($aula_nome);

    $wpdb->insert('aulas_atualizacoes_usuarios', array(
        'post_id' => $product_id,
        'aula_nome' => $aula_nome,
        'data_atualizacao' => date('Y-m-d H:i:s'),
        'user_id' => $usuario_id
    ));
}

function remover_atualizacoes_aula($product_id, $aula_nome, $usuario_id)
{
    global $wpdb;

    $aula_nome = trim($aula_nome);

    $wpdb->delete('aulas_atualizacoes_usuarios', array(
        'post_id' => $product_id,
        'aula_nome' => $aula_nome,
        'user_id' => $usuario_id
    ));
}

function remover_todas_atualizacoes($product_id, $usuario_id)
{
    global $wpdb;

    $aula_nome = trim($aula_nome);

    $wpdb->delete('aulas_atualizacoes_usuarios', array(
        'post_id' => $product_id,
        'user_id' => $usuario_id
    ));
}

function tem_atualizacao_aula($product_id, $aula_nome, $usuario_id)
{
    global $wpdb;

    $aula_nome = trim($aula_nome);

    $sql = "SELECT * FROM aulas_atualizacoes_usuarios WHERE 
			post_id = {$product_id} AND  
			aula_nome = '{$aula_nome}' AND
			user_id = {$usuario_id}";

    return $wpdb->get_results($sql, ARRAY_A);
}

function get_categoria_by_slug($cat_name)
{
    global $wpdb;

    return $wpdb->get_row("SELECT * FROM wp_terms where slug='$cat_name'");
}

function get_categoria_by_nome($nome)
{
    return get_term_by('name', $nome, 'product_cat');
}

function get_validade_assinatura_sq($usuario_id)
{
    return get_user_meta($usuario_id, 'assinante-sq-validade', TRUE);
}

function is_assinatura_sq_expirada($usuario_id)
{
    $validade = get_validade_assinatura_sq($usuario_id);

    return $validade >= date('Y-m-d') ? FALSE : TRUE;
}

function is_mapa_mental($produto_id)
{
    return tem_categoria($produto_id, TERM_MAPAS_MENTAIS);
}

function is_com_correcao($produto_id)
{
    return tem_categoria($produto_id, TERM_COM_CORRECAO);
}

function is_tecnicas_estudo($produto_id)
{
    return tem_categoria($produto_id, TERM_TECNICAS_ESTUDO);
}

function is_discursiva($produto_id)
{
    return tem_categoria($produto_id, TERM_DISCURSIVA_REDACAO);
}

function is_recurso($produto_id)
{
    return tem_categoria($produto_id, TERM_RECURSOS_POS_PROVA);
}

function is_resumo_questoes_comentadas($produto_id)
{
    return tem_categoria($produto_id, TERM_RESUMO_QUESTOES_COMENTADAS);
}

function is_curso_regular($produto_id)
{
    if ($categorias = listar_categorias($produto_id)) {

        foreach ($categorias as $categoria) {

            if (is_categoria_cursos_regulares($categoria)) {
                return true;
            }
        }
    }

    return false;
}

function is_caderno_questoes($produto_id)
{
    if ($categorias = listar_categorias($produto_id)) {

        foreach ($categorias as $categoria) {

            if (is_categoria_caderno_de_questoes($categoria)) {
                return true;
            }
        }
    }

    return false;
}

function is_area_destaque($produto_id)
{
    if ($categorias = listar_categorias($produto_id)) {

        foreach ($categorias as $categoria) {

            if (is_categoria_area_destaque($categoria)) {
                return true;
            }
        }
    }

    return false;
}

/**
 *
 * @todo Essa função deverá substituir is_pacote.
 */
function tem_categoria_pacote($produto_id)
{
    return tem_categoria($produto_id, CATEGORIA_PACOTE);
}

/**
 * Retorna o tipo do produto coaching
 *
 * [JOULE][J1] UPGRADE - Cadastro automático de pagamentos do coaching no Podio
 *
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/upgrades/items/193
 *
 * @since 10.0.0
 *       
 * @param int $produto_id
 *            Id do produto no WP
 *            
 * @return string|false Tipo do produto coaching [COACHING_MENSAL, COACHING_TRIMESTRAL, COACHING_SEMESTRAL] ou false se o produto não for do tipo COACHING
 */
function get_produto_coaching_tipo($produto_id)
{
    $categorias = listar_categorias($produto_id);

    if ($categorias) {
        foreach ($categorias as $categoria) {

            if (is_categoria_coaching_mensal($categoria)) {
                log_wp('debug', "É coaching mensal");
                return COACHING_MENSAL;
            }

            if (is_categoria_coaching_trimestral($categoria)) {
                log_wp('debug', "É coaching trimestral");
                return COACHING_TRIMESTRAL;
            }

            if (is_categoria_coaching_semestral($categoria)) {
                log_wp('debug', "É coaching semestral");
                return COACHING_SEMESTRAL;
            }
        }
    }

    log_wp('debug', "Não é coaching");
    return false;
}

function get_download_url($pedido_id, $produto_id, $download_id) {
    
    if(get_current_user_id() == 138669) {
        return "";    
    }
    
    $order = wc_get_order($pedido_id);
    
    foreach ($order->get_items() as $item) {
        
        if ($item['product_id'] == $produto_id) {
            
            $dis = $order->get_downloadable_items();
            
            foreach ($dis as $di) {
                
                if($di['download_id'] == $download_id) {
                    return $di['download_url'];
                }
            }
        }
    }
}




add_action('woocommerce_product_query', 'expo_woocommerce_product_query', 1, 50);

function expo_woocommerce_product_query($query)
{
    if ($query->is_main_query()) {
        $query->set('posts_per_page', CURSOS_ONLINE_POR_PAGINA);

        $meta_query[] = array(
            'key' => EXIBIR_EM_CURSOS_ONLINE,
            'value' => SIM,
            'compare' => '='
        );

        $query->set('meta_query', $meta_query);
    }
}

/**
 * Verifica se a categoria pode ser considerada para identificar produtos relacionáveis
 *
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/1916
 *
 * @since J4
 *       
 * @param mixed $categoria
 *            WP_Term
 *            
 * @return bool
 */
function is_categoria_nao_relacionavel($categoria)
{
    if ($categoria->parent == CATEGORIA_AREA || $categoria->term_id == CATEGORIA_AREA || $categoria->parent == CATEGORIA_PACOTE || $categoria->term_id == CATEGORIA_PACOTE || $categoria->parent == CATEGORIA_TIPO_CURSO || $categoria->term_id == CATEGORIA_TIPO_CURSO) {

        return true;
    }

    return false;
}

/**
 * Retorna primeira área de um determinado curso.
 * Em princípio cada produto só poderá estar em uma área
 *
 * @since J4
 *       
 * @param int $curso_id
 *            Id do produto no WP
 * @param bool $agrupar_categorias_sq
 *            Categorias SQ retornarão SQ
 *            
 * @return WP_Term
 */
function get_primeira_area($curso_id, $agrupar_categorias_sq = true)
{
    $categorias = listar_categorias($curso_id);

    foreach ($categorias as $categoria) {

        if ($agrupar_categorias_sq && is_categoria_sistema_questoes($categoria)) {
            return "SQ";
        }

        if (is_categoria_area($categoria)) {
            return $categoria;
        }
    }

    return null;
}

/**
 * Retorna primeira disciplina de um determinado curso.
 *
 * @since J5
 *       
 * @param int $curso_id
 *            Id do produto no WP
 *            
 * @return WP_Term
 */
function get_primeira_disciplina($curso_id)
{
    $categorias = listar_categorias($curso_id);

    foreach ($categorias as $categoria) {

        if (is_categoria_disciplina($categoria)) {
            return $categoria;
        }
    }

    return null;
}

function listar_todas_disciplinas_de_curso($curso_id)
{
    $categorias = listar_categorias($curso_id);

    $disciplinas = [];

    foreach ($categorias as $categoria) {

        if (is_categoria_disciplina($categoria)) {
            $disciplinas[] = $categoria;
        }
    }

    return $disciplinas;
}

/**
 * Lista as categorias das áreas
 *
 * @since J4
 *       
 * @return array Lista de WP_Terms
 */
function get_areas()
{
    return get_terms('product_cat', [
        'parent' => CATEGORIA_AREA
    ]);
}

/**
 * Gera combo options das áreas
 *
 * @since J4
 *       
 * @param $label String
 *            Label default
 * @param $value String
 *            Value default
 *            
 * @return array
 */
function get_areas_combo_options($label = "", $value = "")
{
    $areas = get_areas();

    // $areas_array[$value] = $label;
    foreach ($areas as $area) {
        $areas_array[$area->term_id] = $area->name;
    }

    return $areas_array;
}

function get_alunos_ativos_turma_coaching($forceUpdate = FALSE)
{
    $alunos = get_transient(TRANSIENT_USUARIOS_ATIVOS_COACHING);

    if (! $alunos || (is_administrador() && $forceUpdate)) {
        $alunos = get_aluno_produto_ativo_por_categoria([773, 774]);
        set_transient(TRANSIENT_USUARIOS_ATIVOS_COACHING, $alunos, 5 * MINUTE_IN_SECONDS);
    }

    return $alunos;
}

/**
 * @deprecated Reta Final foi removido e consolidada na categoria Coaching
 * 
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2405
 */

function get_alunos_ativos_reta_final($forceUpdate = FALSE)
{
    $alunos = get_transient(TRANSIENT_USUARIOS_ATIVOS_RF);

    if (! $alunos || (is_administrador() && $forceUpdate)) {
        $alunos = get_aluno_produto_ativo_por_categoria(array(
            530,
            534
        ));
        set_transient(TRANSIENT_USUARIOS_ATIVOS_RF, $alunos, 5 * MINUTE_IN_SECONDS);
    }

    return $alunos;
}

/**
 * Busca os alunos de produtos ativos de uma dada categoria
 *
 * @since J5
 *       
 * @param $categorias array
 *            ou String ou int
 *            
 * @return array
 */
function get_aluno_produto_ativo_por_categoria($categorias)
{
    KLoader::model("ProdutoModel");
    
    global $wpdb;

    if (is_null($categorias)) {
        return null;
    }

    if (is_array($categorias)) {
        $cat_ids = implode(", ", $categorias);
    } else {
        $cat_ids = $categorias;
    }

    // $meta_key_disponivel = PRODUTO_DISPONIVEL_ATE_POST_META;
    // $meta_key_vendas = PRODUTO_VENDAS_ATE_POST_META;
    $publish = STATUS_POST_PUBLICADO;

    $sql = "SELECT u.id, u.display_name as nome_completo, u.user_email as email, p.ID as produto_id
			FROM wp_vendas v 
				inner JOIN wp_users u ON v.ven_aluno_id = u.ID
				inner JOIN wp_posts p ON v.ven_curso_id = p.ID
			where 1=1 
				and p.post_status = '{$publish}' 
				and v.ven_curso_id IN ( 
					SELECT object_id FROM wp_term_relationships tr 
						JOIN wp_term_taxonomy tt ON tt.term_taxonomy_id = tr.term_taxonomy_id 
						WHERE tt.term_id in ( {$cat_ids} )
				)
			ORDER BY upper(u.display_name)";

    $result = $wpdb->get_results($sql, ARRAY_A);

    $alunos = array();

    foreach ($result as $item) {
        
        if(!ProdutoModel::tem_permissao($item['id'], $item['produto_id'])) {
            continue;
        }

        $disponivel_ate = get_data_disponivel_ate($item['p.id']);
        if (! $disponivel_ate) {
            $disponivel_ate = strtotime(hoje_yyyymmdd() . ' 00:00:00');
        }

        $hoje = strtotime(hoje_yyyymmdd() . ' 00:00:00');

        if ($disponivel_ate >= $hoje) {
            array_push($alunos, $item);
        }
    }

    return $alunos;
}

/**
 * Recupera link de adição de um produto ao carrinho
 *
 * @since J5
 * 
 * @deprecated utilizar UrlHelper::get_adicionar_produto_ao_carrinho_url()
 *       
 * @param $produto_id int
 *
 * @return string
 */
function adicionar_produto_ao_carrinho_url($produto_id)
{
    $produto = wc_get_product($produto_id);
    
    $url = $produto->get_permalink() . "?add-to-cart=" . $produto_id;
    
    // se usuário não estiver logado joga para tela de login
    if(!is_usuario_logado()) {
        return login_url($url);
    }
    
    return $url;
}

/**
 * Recupera todos os produtos de um determinado pedido
 *
 * @since J5
 *       
 * @param $pedido int|WC_Order
 * @param $retornar_somente_ids bool
 *
 * @return array
 */
function listar_produtos_pedido($pedido, $retornar_somente_ids = false)
{
    if (is_int($pedido)) {
        $pedido = new WC_Order($pedido);
    }

    $produtos = [];

    foreach ($pedido->get_items() as $item) {

        if ($retornar_somente_ids) {
            array_push($produtos, $item->get_id());
        } else {
            array_push($produtos, $item);
        }
    }

    return $produtos;
}

/**
 * Gera produtos relacionados de um pedido
 *
 * @since J5
 *       
 * @param $pedido int|WC_Order
 * @param $qtde int
 *
 * @return array
 */
function get_cursos_relacionados_pedido($pedido, $qtde = 4)
{
    if (is_int($pedido)) {
        $pedido = new WC_Order($pedido);
    }

    $produtos_pedido = listar_produtos_pedido($pedido);

    $relacionados_ids = [];

    while (true) {
        foreach ($pedido->get_items() as $item) {
            if (count($relacionados_ids) >= $qtde) {
                break 2;
            }

            $encontrou = false;

            if (! $encontrou) {

                // busca produtos do mesmo concurso
                $concurso = get_primeira_categoria_concurso($item['product_id'], [
                    'caderno-questoes-online',
                    'mapas-mentais',
                    'turma-de-coaching',
                    'turma-de-coaching-1',
                    'turma-coaching-reta-final-1',
                    'turma-coaching-reta-final',
                    'audiobooks'
                ]);

                if ($produtos = get_cursos_por_categoria($concurso->slug, false, false, false, true)) {
                    shuffle($produtos);

                    foreach ($produtos as $produto) {

                        if (! in_array($produto->get_id(), $produtos_pedido) && ! in_array($relacionados_ids) && $produto->get_id() != $item['product_id']) {

                            array_push($relacionados_ids, $produto->get_id());
                            $encontrou = true;
                            break;
                        }
                    }
                }
            }

            if (! $encontrou) {
                // busca produtos da mesma área
                $area = get_primeira_area($item['product_id'], false);

                if ($produtos = get_cursos_por_categoria($area->slug, false, false, false, true)) {
                    shuffle($produtos);

                    foreach ($produtos as $produto) {

                        if (! in_array($produto->get_id(), $produtos_pedido) && ! in_array($relacionados_ids) && $produto->get_id() != $item['product_id']) {

                            array_push($relacionados_ids, $produto->get_id());
                            $encontrou = true;
                            break;
                        }
                    }
                }
            }

            if (! $encontrou) {

                // busca produtos da mesma disciplina
                $disciplina = get_primeira_disciplina($item['product_id'], false);

                if ($produtos = get_cursos_por_categoria($disciplina->slug, false, false, false, true)) {
                    shuffle($produtos);

                    foreach ($produtos as $produto) {

                        if (! in_array($produto->get_id(), $produtos_pedido) && ! in_array($relacionados_ids) && $produto->get_id() != $item['product_id']) {

                            array_push($relacionados_ids, $produto->get_id());
                            $encontrou = true;
                            break;
                        }
                    }
                }
            }

            if (! $encontrou) {

                // busca produtos aleatoriamente
                if ($produtos = get_cursos_por_categoria("", false, false, false, true)) {
                    shuffle($produtos);

                    foreach ($produtos as $produto) {

                        if (! in_array($produto->get_id(), $produtos_pedido) && ! in_array($relacionados_ids) && $produto->get_id() != $item['product_id']) {

                            array_push($relacionados_ids, $produto->get_id());
                            $encontrou = true;
                            break;
                        }
                    }
                }
            }
        }
    }

    $relacionados = [];
    foreach ($relacionados_ids as $id) {
        array_push($relacionados, get_product($id));
    }

    return $relacionados;
}

/**
 * Atualiza o campo meta _produto_busca com os dados: título, concurso e professores
 *
 * @since K1
 *       
 * @param $produto_id int
 */
function atualizar_campo_produto_busca($produto_id)
{
    $produto = get_product($produto_id);
    $campo = $produto->get_title();

    $concursos = get_todas_categoria_concurso($produto_id);
    foreach ($concursos as $item) {
        $campo .= " " . $item->name;
    }

    $professores = get_autores($produto_id);
    foreach ($professores as $item) {
        $campo .= " " . $item->first_name . " " . $item->last_name;
    }

    update_post_meta($produto_id, META_PRODUTO_BUSCA, $campo);
}

/**
 * Busca por produtos a partir de filtros
 *
 * @since K1
 *       
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2293
 *
 * @param $texto string
 * @param $offset int
 * @param $limit int
 * @param $tipo int
 * @param $filtro string
 * @param $ordem int
 * @param $categorias array
 * @param $somente_gratuitos bool
 *
 * @return array
 */
function buscar_produtos($texto, $offset = 0, $limit = PESQUISA_PRODUTOS_POR_PAGINA, $tipo = null, $filtro = null, $ordem = null, $categorias = null, $somente_gratuitos = false, $contar = FALSE)
{
    global $wpdb;

    // Inclusão de joins
    $join_term_relationships = "";
    $join_term_relationships2 = "";

    if ($tipo) {
        $join_term_relationships = " JOIN wp_term_relationships tr ON tr.object_id = pr.post_id ";
    }

    if ($categorias) {
        $join_term_relationships2 = " JOIN wp_term_relationships tr2 ON tr2.object_id = pr.post_id ";
    }

    // Filtro por tipo
    $where_tipo = "";
    if ($tipo) {
        $where_tipo = " AND tr.term_taxonomy_id = $tipo ";
    }

    // Filtro por categorias
    $where_categorias = "";
    if ($categorias) {
        $where_categorias = " AND tr2.term_taxonomy_id IN (" . implode(",", $categorias) . ") ";
    }

    // Filtro por somente gratuitos
    $where_somente_gratuitos = "";
    if ($somente_gratuitos) {
        $where_somente_gratuitos = " AND pr.pro_preco = 0 ";
    }

    // Filtro por texto
    $where_filtro = $filtro ? " AND pr.pro_busca LIKE '%{$filtro}%' " : "";

    // Ordenação
    $where_ordem = "";
    $order_by = "";

    if ($ordem) {

        if ($ordem == PESQUISA_ORDEM_MENOR_PRECO) {
            $order_by = " pr.pro_preco ASC, ";
        } elseif ($ordem == PESQUISA_ORDEM_MAIOR_PRECO) {
            $order_by = " pr.pro_preco DESC, ";
        } elseif ($ordem == PESQUISA_ORDEM_A_Z) {
            $order_by = " p.post_title ASC, ";
        } elseif ($ordem == PESQUISA_ORDEM_Z_A) {
            $order_by = " p.post_title DESC, ";
        } elseif ($ordem == PESQUISA_MAIS_NOVOS) {
            $order_by = " pr.post_id DESC, ";
        }
    }
    $order_by .= " pr.pro_prioridade DESC ";

    $select = $contar ? "COUNT(pr.post_id) as count" : "pr.post_id";
    $order_limit_offset = $contar ? "" : "ORDER BY {$order_by} LIMIT {$limit} OFFSET {$offset}";

    // Query
    $sql = "SELECT {$select} FROM produtos pr 
				JOIN wp_posts p ON p.ID = pr.post_id {$join_term_relationships} {$join_term_relationships2} 
			WHERE pr.pro_busca LIKE '%{$texto}%' {$where_tipo} {$where_categorias} {$where_somente_gratuitos} {$where_filtro} {$where_ordem} 
				AND p.post_status = 'publish' 
				AND (pr.pro_vendas_ate IS NULL OR pr.pro_vendas_ate >= CURDATE()) 
				{$order_limit_offset}";

    if ($contar) {
        return $wpdb->get_var($sql);
    } else {
        $resultado = $wpdb->get_results($sql);

        $produtos = [];
        foreach ($resultado as $item) {
            $produto = get_product($item->post_id);
            array_push($produtos, $produto);
        }

        return $produtos;
    }
}

/**
 * Conta a partir de filtros
 *
 * @since K1
 *       
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2293
 *
 * @param $texto string
 * @param $tipo int
 * @param $filtro string
 * @param $categorias array
 * @param $somente_gratuitos bool
 *
 * @return int
 */
function contar_produtos($texto, $tipo = NULL, $filtro = NULL, $categorias = NULL, $somente_gratuitos = FALSE)
{
    return buscar_produtos($texto, NULL, NULL, $tipo, $filtro, NULL, $categorias, $somente_gratuitos, TRUE);
}

function get_produto_imagem($produto_id)
{
    if (has_post_thumbnail($produto_id)) {
        return get_the_post_thumbnail($produto_id);
    } else {
        return "";
        // return "<img src='/wp-content/themes/academy/images/default_book.jpg' />";
    }
}
