<?php
function get_template_email($filename, $data = array()) {
	$filename = $_SERVER['DOCUMENT_ROOT'] . '/wp-content/themes/academy/emails/' . $filename;
	extract($data);
	if (is_file($filename)) {
		ob_start();
		include $filename;
		return ob_get_clean();
	}
	return false;
}

function get_email($email_id){
	global $wpdb;
	$result = $wpdb->get_results("SELECT * FROM wp_fale_conosco WHERE id = " . $email_id );
	return $result;
}

function salvar_email($nome, $endereco, $titulo, $mensagem){
	global $wpdb;
	$wpdb->insert('wp_fale_conosco', array(
		'nome' => $nome,
		'email' => $endereco,
		'assunto' => $titulo,
		'conteudo' => $mensagem
	));
	$email_id = $wpdb->insert_id;
	return $email_id;

}

function remover_email($email_id){
	global $wpdb;
	$wpdb->delete('wp_fale_conosco', array('id' => $email_id));
}

function enviar_email($endereco, $titulo, $mensagem, $from = null, $bcc = 'leonardo.coelho@exponencialconcursos.com.br', $embed_images = null, $debug = false, $senha = null) {
	
	// Inicia a classe PHPMailer
	global $phpmailer;
	// (Re)create it, if it's gone missing
	if ( !is_object( $phpmailer ) || !is_a( $phpmailer, 'PHPMailer' ) ) {
		require_once ABSPATH . WPINC . '/class-phpmailer.php';
		require_once ABSPATH . WPINC . '/class-smtp.php';
		$phpmailer = new PHPMailer( true ); 
	}
	
	$phpmailer = new PHPMailer(true); 

	try {
	    /**
	     * Remoção de endereços de envio de e-mails conforme solicitado em reunião de 26/05/22
	     */
//        $username = "atendimento" . rand(1,3) . "@exponencialconcursos.com.br";
        $username = "atendimento1@exponencialconcursos.com.br";

		$phpmailer->CharSet = 'UTF-8';
		$phpmailer->isSMTP();
		$phpmailer->SMTPAuth   = true;                  
		$phpmailer->Host       = "smtp.gmail.com";
		$phpmailer->Port       = 465;
		$phpmailer->Username   = $username;
		$phpmailer->Password   = "Expo@2020;";
		$phpmailer->SMTPSecure = "ssl";
		
		if($from && $senha) {
		    $phpmailer->Username   = $from;
		    $phpmailer->Password   = $senha; 
		}
		
		if($debug) {
			$phpmailer->SMTPDebug  = 2;
			$phpmailer->Debugoutput = 'html';
		}

		$bcc_original = null;
		$endereco_original = null;
		
		switch($_SERVER['HTTP_HOST']) {
			case 'jenkins.exponencialconcursos.com.br' : {
				$titulo = '[JENKINS] ' . $titulo;
				$bcc_original = $bcc;
				$bcc = null;
				$endereco_original = $endereco;
				$endereco = EMAIL_BCC_KEYDEA;

				break;
			}

			case 'homol.exponencialconcursos.com.br' : {
				$titulo = '[HOMOLOGACAO] ' . $titulo;
				$bcc_original = $bcc;
				$bcc = null;
				$endereco_original = $endereco;
				$endereco = EMAIL_BCC_KEYDEA;
				break;
			}

			case 'exponencial' : {
				$titulo = '[DESENVOLVIMENTO] ' . $titulo;
				$bcc_original = $bcc;
				$bcc = null;
				$endereco_original = $endereco;
				$endereco = EMAIL_BCC_KEYDEA;
				break;
			}

		}

		if(is_null($from)) {
			$from = 'exponencial@exponencialconcursos.com.br';
		}
		
		$phpmailer->AddReplyTo($from, 'Exponencial Concursos');
		$phpmailer->setFrom($from, 'Exponencial Concursos');
		$phpmailer->addAddress($endereco);
		$phpmailer->addBCC(EMAIL_BCC_KEYDEA);

		if($embed_images) {
			foreach ($embed_images as $embed_image) {
				$phpmailer->AddEmbeddedImage($embed_image['path'], $embed_image['cid'], $embed_image['name']);
			}		
		}
		
		if(!is_null($bcc)) { 
		   
		    
			if(!is_array($bcc)) {
				$bcc = array($bcc);
			}

			foreach ($bcc as $item) { 
				$phpmailer->addBCC($item);
			}		

		}

		if(!is_null($endereco_original)) { 
			if(!is_array($endereco_original)) {
				$endereco_original = array($endereco_original);
			}

			$mensagem .= "<br>-------------------------";
			foreach ($endereco_original as $item) {	
				$mensagem .= "<br>Endereço original: $item";
			}		
		}

		if(!is_null($bcc_original)) { 
			if(!is_array($bcc_original)) {
				$bcc_original = array($bcc_original);
			}

			$mensagem .= "<br>-------------------------";
			foreach ($bcc_original as $item) {	
				$mensagem .= "<br>BCC Original: $item";
			}		
		}

		$titulo = sanitizar_string($titulo);
		$phpmailer->Subject = '=?UTF-8?B?'.base64_encode($titulo).'?=';
		$phpmailer->msgHTML($mensagem);
		$phpmailer->AltBody = ' ';

		if(!$phpmailer->Send()) {
			log_email('error', 'Erro ao enviar e-mail');
			log_email('error', $phpmailer->ErrorInfo);
		}
		else {
			
			log_email('debug', "E-mail enviado com sucesso para {$endereco} (assunto: {$titulo})");
		}
		
	} catch (phpmailerException $e) {
	  	log_email('error', 'Erro ao enviar e-mail');
		log_email('error', $phpmailer->ErrorInfo);
	} catch (Exception $e) {
		log_email('error', 'Erro ao enviar e-mail');
	  	log_email('error', $e->getMessage());
	}
}

/*************************************************************************
 * 
 * H O O K S
 * 
 *************************************************************************/
// Change new order email recipient for registered customers
function wc_change_admin_new_order_email_recipient( $recipient, $order ) {
	global $woocommerce;
	
	foreach ( $order->get_items () as $item ) {
		$produto_id = $item ['product_id'];
		
		if(get_post_meta($produto_id, 'notificar_vendas', true) == 'yes') {
			return $recipient;
		}
	}
	
	return "trash@exponencialconcursos.com.br";
}


add_filter( 'woocommerce_email_recipient_new_order', 'expo_alterar_email_homologacao', 10, 2);
add_filter( 'woocommerce_email_recipient_customer_processing_order', 'expo_alterar_email_homologacao', 10, 2);
add_filter( 'woocommerce_email_recipient_customer_completed_order', 'expo_alterar_email_homologacao', 10, 2);
add_filter( 'woocommerce_email_recipient_customer_invoice', 'expo_alterar_email_homologacao', 10, 2);
add_filter( 'woocommerce_email_recipient_customer_note', 'expo_alterar_email_homologacao', 10, 2);
add_filter( 'woocommerce_email_recipient_low_stock', 'expo_alterar_email_homologacao', 10, 2);
add_filter( 'woocommerce_email_recipient_no_stock', 'expo_alterar_email_homologacao', 10, 2);
add_filter( 'woocommerce_email_recipient_backorder', 'expo_alterar_email_homologacao', 10, 2);
add_filter( 'woocommerce_email_recipient_customer_new_account', 'expo_alterar_email_homologacao', 10, 2);
add_filter( 'woocommerce_email_recipient_customer_invoice_paid', 'expo_alterar_email_homologacao', 10, 2);

function expo_alterar_email_homologacao( $recipient, $order ) 
{	
	switch($_SERVER['HTTP_HOST']) {
		case 'exponencial': $recipient = EMAIL_BCC_KEYDEA . EMAIL_BCC_DESENVOLVEDOR ? ', '.EMAIL_BCC_DESENVOLVEDOR:''; break;
		case 'homol.exponencialconcursos.com.br': $recipient = EMAIL_BCC_KEYDEA . ', ' . EMAIL_LEONARDO; break;
	}

	log_wp("debug", "Alterando e-mail: $recipient");

	return $recipient;
}

add_filter( 'woocommerce_email_subject_new_order', 'expo_alterar_assunto_homologacao', 10, 2);
add_filter( 'woocommerce_email_subject_customer_processing_order', 'expo_alterar_assunto_homologacao', 10, 2);
add_filter( 'woocommerce_email_subject_customer_completed_order', 'expo_alterar_assunto_homologacao', 10, 2);
add_filter( 'woocommerce_email_subject_customer_invoice', 'expo_alterar_assunto_homologacao', 10, 2);
add_filter( 'woocommerce_email_subject_customer_note', 'expo_alterar_assunto_homologacao', 10, 2);
add_filter( 'woocommerce_email_subject_low_stock', 'expo_alterar_assunto_homologacao', 10, 2);
add_filter( 'woocommerce_email_subject_no_stock', 'expo_alterar_assunto_homologacao', 10, 2);
add_filter( 'woocommerce_email_subject_backorder', 'expo_alterar_assunto_homologacao', 10, 2);
add_filter( 'woocommerce_email_subject_customer_new_account', 'expo_alterar_assunto_homologacao', 10, 2);
add_filter( 'woocommerce_email_subject_customer_invoice_paid', 'expo_alterar_assunto_homologacao', 10, 2);

function expo_alterar_assunto_homologacao( $subject, $order ) 
{	
	switch($_SERVER['HTTP_HOST']) {
		case 'exponencial': $subject = "[DESENVOLVIMENTO] " . $subject; break;
		case 'homol.exponencialconcursos.com.br': $subject = "[HOMOLOGACAO] " . $subject; break;
	}

	log_wp("debug", "Alterando assunto: $subject");

	return $subject;
}