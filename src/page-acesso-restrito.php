<?php get_header(); ?>
<div id="acesso-restrito" class="container-fluid">
	<div class="p-5"></div>
	<div class="p-5 col-12 mt-5">
		<h1 class="mt-4 text-white">Você precisa fazer login para acessar esta página.</h1>
		<a class="btn btn-lg u-btn-primary" href="<?php echo SITE_URL; ?>cadastro-login/?ref=<?php echo $_GET['uri']; ?>">Clique aqui</a>
	</div>
</div>
<?php get_footer(); ?>