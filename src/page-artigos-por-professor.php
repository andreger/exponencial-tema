<?php 
/**
 * Controller para organizar fluxos dos artigos por professor especifico
 *
 * @package academy/Controllersacademy
 * @author 	Ivan Duarte <ivan.duarte@keydea.com.br>
 * @since	K5
 */

get_header();

global $professor;

KLoader::model("BlogModel");
KLoader::model("ProfessorModel");
KLoader::helper("ProfessorHelper");

$slug = get_query_var("nome");
$pg = isset($_GET['pg']) ? $_GET['pg'] : "1";

if(!$slug) {
	wp_redirect('/blog-artigos', 301);
}

$professor = ProfessorModel::get_by_slug(get_query_var("nome"));
$data['professor_id'] = $professor->user_id;
$data['slug'] = $slug;
$data['pg'] = $pg;
$data['professor'] = ProfessorHelper::get_avatar_box($professor->user_id);

KLoader::view('main/artigos', $data);