<?php
    use Exponencial\Core\Helpers\Template;

	KLoader::controller('HeaderController');
	KLoader::helper('UrlHelper');
	KLoader::helper('PesquisaHelper');
	KLoader::helper('AcessoGrupoHelper');
	KLoader::helper("HeadHelper");
	KLoader::helper("YoastHelper");
	
	KLoader::init_globals();
	
	/**
	 * Código provisório para redirecionamento de home entre os dias 01/02/20 e 03/02/20
	 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2821
	 */
	$agora = date("Y-m-d H:i:s");
	$inicial1 = "2021-10-25 10:00:00";
	$final1 = "2022-05-15 23:59:59";
	
	if(is_front_page() && is_producao() && $agora >= $inicial1 && $agora <= $final1) {
	    header('Location: https://www.exponencialconcursos.com.br/assinaturas');
	    exit;
	}

    $inicial2 = "2021-05-16 00:00:00";
    $final2 = "2022-05-23 23:59:59";

    if(is_front_page() && is_producao() && $agora >= $inicial2 && $agora <= $final2) {
        header('Location: https://www.exponencialconcursos.com.br/assinatura-lote-secreto');
        exit;
    }

	$template = new Template();


	global $woocommerce, $post;
	?>
<!DOCTYPE html>

<html lang="pt-BR" prefix="og:http://ogp.me/ns#" class="wf-opensans-n4-active wf-opensans-i4-active wf-opensans-n6-active wf-active">

<head>
    <?php KLoader::view("plugins/ga4/ga4") ?>
	<link rel="shortcut icon" href="<?= get_stylesheet_directory_uri(); ?>/assets/favicon/favicon.png" />
	<?php if(!is_producao()) : ?>
		<meta name="robots" content="noindex, nofollow" />
	<?php endif ?>

    <meta name="facebook-domain-verification" content="xz90wb6efrp1u2cr0vqr1a0z9taov9" />
	<meta property="og:site_name" content="Exponencial Concursos">
	<meta property="og:image" content="<?= get_thumb_url_for_facebook($post) ?>" />

	<?= HeadHelper::get_produto_meta() ?>
    <?= HeadHelper::get_canonical() ?>

	<?php wp_head(); ?>

    <?php echo $template->render('comum/header/estilos.html.twig'); ?>
    <?php echo $template->render('comum/header/scripts.html.twig'); ?>

    <?= YoastHelper::render_keyword() ?>

	<meta http-equiv="content-type" content="text/html;charset=utf-8" />

	<meta name="viewport" content="user-scalable=yes, initial-scale=1">
	<link href="https://fonts.googleapis.com/css?family=Raleway:400,400i,500,500i,700,700i%7CRoboto:400,400i,700,700i%7CPoppins:400,600"
          rel="stylesheet">

	<script>
    if(window.location.hash && window.location.hash == '#_=_') {
        window.location.href = '/?action=login';
    }
	</script>

	<!--[if lt IE 9]>
	<script src="/wp-content/themes/academy/js/html5.js"></script>
	<![endif]-->

	<?php KLoader::view("plugins/google_analytics/ga") ?>

	<?= gtm_pagina() ?>
	<?php KLoader::view("plugins/google_tag_manager/gtm_head") ?>
	<?php KLoader::view("plugins/google_ads/google_ads") ?>
	<?php KLoader::view("plugins/facebook_pixel/facebook_pixel") ?>


	<?php if(isset($include_summernote)) : ?>
		<link href="/wp-content/themes/academy/css/summernote/summernote.css" rel="stylesheet">
		<link href="/wp-content/themes/academy/css/summernote/summernote-bs3.css" rel="stylesheet">
		<script src="/wp-content/themes/academy/js/summernote/summernote.js"></script>
		<script src="/wp-content/themes/academy/js/summernote/lang/summernote-pt-BR.js"></script>
		<script src="/wp-content/themes/academy/js/ext/summernote-ext.js"></script>
		<script src="/wp-content/themes/academy/js/ext/summernote-ext-specialchars.js"></script>
		<script src="/wp-content/themes/academy/js/ext/summernote-ext-myenter.js"></script>
	<?php endif ?>
</head>

<body <?php body_class(); ?>>

	<?php KLoader::view("plugins/facebook_sdk/facebook_sdk") ?>
	<?php KLoader::view("plugins/twitter/twitter") ?>
	<?php KLoader::view("plugins/google_plus/google_plus") ?>
	<?php KLoader::view("plugins/google_tag_manager/gtm_body") ?>

	<?php include_componente('assinatura-bar') ?>

	<?php
		$header_topbar_user = 'header-topbar';
		$logo_index_user = 'logo-index';
		$navbar_index_user = 'navbar-index';
		$secao_barra_clara_user = 'secao-barra-clara';
		$b_top_user = 'b-top';
		$featured_content = "";
		if($usuario_id = get_current_user_id()) {
			if(exibe_assinatura_bar($usuario_id)) {
				$header_topbar_user = 'header-topbar-ass-bar';
				$navbar_index_user = 'navbar-index-ass-bar';
				$logo_index_user = 'logo-index-ass-bar';
				$secao_barra_clara_user = 'secao-barra-clara-ass-bar';
				$b_top_user = 'b-top-ass-bar';
				$featured_content = "featured-content-ass-bar";
			}
		}
	?>

	<div class="row-fluid">
		<div class="hrtb pr-0 pl-0 col-12 <?= $header_topbar_user ?> bg-gray text-left text-md-right">
			<div class="row d-none">
			</div>
			<div class="col-12">
					<?php if(is_user_logged_in()) : ?>

						<span id="header-bemvindo" class="d-none d-lg-inline-block">
							Bem-vindo, <?php echo get_usuario_nome_exibicao(get_current_user_id()) ."."; ?>

							<?php if(AcessoGrupoHelper::relatorio_de_vendas()) : ?>
							<a href="/downloads-agendados" style="display: none" class="downloads-agendados-div">
    							<span class="btn-download-agendado rounded" title="Um ou mais downloads agendados estão prontos">
    								<i class="fa fa-download"></i>
    							</span>
							</a>
							<?php endif ?>

						</span>

					<?php endif ?>

					<span class="header-botao d-sm-none">
						<a href="/" rel="home" class="d-inline-block pt-2 pb-2">
							<img class="img-logo" style="height: auto; width: 140px;" src="/core/resources/img/layout/logo-header.png" alt="Exponencial Concursos" title="Exponencial Concursos">
						</a>
					</span>

					<span class="header-botao header-botao-lupa d-sm-none">
						<a href="#" id="header-buscar-mobile" class="d-inline-block margin-item-topbar pr-2 pl-2 pt-1 pb-1">
							<i class="fa fa-search text-white"></i>		
						</a>
					</span>

					<span class="header-botao-sq d-none d-lg-inline-block">
						<a class="d-inline-block margin-item-topbar pt-2 pb-2 pl-2 pl-xl-3 pr-2 pr-xl-0  <?= is_user_logged_in() ? 'mr-1' :  'mr-0'?>" href="/questoes/main/resolver_questoes">
							 <span class="comum-sq d-inline-block align-bottom"></span> <span class="pr-lg-3 pr-2 pl-1 d-none d-xl-inline-block">Questões</span>
						</a>
					</span>

					<?php if(is_user_logged_in()) : ?>

					    <span class="header-botao d-none d-lg-inline-block">
							<a class="t-d-none font-weight-bold text-green" href="/minha-conta" rel="nofollow">
								<i class="margin-item-topbar position-relative font-20 fa fa-user" style="top: 3px;"></i>
								<span class="pr-md-3 padding-area d-none d-xl-inline-block">Área do Aluno</span>
							</a>

						</span>

						<span class="d-none d-md-inline header-divider d-none d-lg-inline-block"></span>

						<span class="header-botao">
							<a href="/questoes/login/logout/?empty-cart=1"><span class="margin-item-topbar">Sair</span></a>
						</span>

					<?php else : ?>

						<span class="header-botao d-none d-lg-inline-block">
							<a class="d-inline d-md-none t-d-none font-weight-bold text-blue" href="/minha-conta" rel="nofollow">
								<i class="margin-item-topbar-cadastro position-relative font-20 fa fa-user" style="top: 3px;"></i>
							</a>
							<a class="margin-item-topbar d-none d-md-inline" href="/cadastro-login"><span>Cadastre-se</span></a>
						</span>

						<span class="d-none d-md-inline header-divider d-none d-lg-inline-block"></span>

						<span class="header-botao">
							<a class="margin-item-topbar d-none d-md-inline" href="/cadastro-login" id="login-button"><span>Entrar</span></a>
						</span>

						<span class="header-botao">
							<a class="margin-item-topbar d-inline d-md-none" href="/cadastro-login"><span>Entrar</span></a>
						</span>

					<?php endif; ?>

					<span class="d-none d-md-inline header-divider"></span>

					<span class="header-botao d-none d-lg-inline-block">
						<a class="margin-item-topbar" href="/fale-conosco">
                            <span class="comum-fale-conosco d-inline-block align-bottom"></span>
							<span class="d-none d-lg-inline-block">Fale Conosco</span>
						</a>
					</span>

					<span class="d-none d-md-inline header-divider d-none d-lg-inline-block"></span>

					<?php KLoader::view("header/barra_superior/carrinho/carrinho") ?>

					<?php KLoader::view("header/barra_superior/login/login") ?>
					
					<span class="d-lg-none float-right navbar-dark">
						<button class="mt-1 mt-sm-0 d-w bg-blue navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
							<span class="navbar-toggler-icon"></span>
						</button>
					</span>

				</div>
			</div>
		</div>



<div id="barra-container" class="container-fluid mt-0 mt-md-0 pt-sm-0 pt-md-4 pb-2 pb-md-4 pb-lg-3 bg-white p-absolute <?= $logo_index_user ?>">
    <div class="container">
    	<div class="m-p-r row mt-5 mt-md-2">

            <?php KLoader::view("header/barra_principal/barra_principal") ?>

            <div class="pl-md-0 pr-md-0 pl-lg-3 pr-lg-3 mt-3 col-12 col-md-3 d-none header-social-carrinho text-center text-lg-right">
            	<div class="row mt-md-3 mt-xl-3">

            		<?php if(AcessoGrupoHelper::relatorio_de_vendas()) : ?>
					<div class="col-3 p-0 text-center downloads-agendados-div" style="margin-top: 6px; display:none">
						<a href="/downloads-agendados">
                			<span class="btn-download-agendado btn-download-agendado-bar rounded" title="Um ou mais downloads agendados estão prontos">
    							<i class="fa fa-download"></i>
    						</span>
    					</a>
            		</div>
            		<?php endif ?>

            		<div class="col-3 p-0 text-center">
            			<a class="t-d-none" href="/questoes/main/resolver_questoes">
                            <span class="d-inline-block" style="padding: 4px; background-color: #fbb255">
                                <span class="comum-sq d-inline-block align-bottom"></span>
                            </span>
            			</a>
            		</div>

            		<div class="p-0 text-center mt-1 col-3 text-center">

            		<?php if(is_user_logged_in()) : ?>
                		<div>
                        	<a class="t-d-none font-weight-bold text-green" href="/minha-conta" rel="nofollow"><i class="font-20 fa fa-user"></i></a>
                        </div>
            		<?php else : ?>
            			<div>
                    		<a class="t-d-none font-weight-bold text-blue" href="/cadastro-login" id="login-button-scroll"><i class="font-20 fa fa-user"></i></a>
                    	</div>
            		<?php endif; ?>
                	</div>
            		<div class="text-center p-0 col-3 header-botao toolbar-carrinho">
            			<a class="t-d-none text-dark cart-button" href="/checkout">
                            <span class="comum-carrinho-mobile d-inline-block align-bottom"></span>
            				<span class="text-white item-carrinho font-8"><?= $woocommerce->cart->cart_contents_count == 0 ? "0" : sprintf(_n('%d', '%d', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count); ?></span>
                        </a>
                    </div>
            	</div>
            </div>
    	</div>
    </div>
</div>

<div>
	<?php include_componente('header-navbar') ?>
</div>

<div class="featured-content <?= $featured_content ?>">
	<?php
        if (function_exists('putRevSlider') && is_front_page() && is_page()) {
		    putRevSlider("slider1","homepage");
	    }
    ?>
</div>
<div>

<?= HeaderController::breadcrumb() ?>

<script>
jQuery(function(){
	var header_topbar_user = "<?php echo ".".$header_topbar_user; ?>";
	var logo_index_user = "<?php echo $logo_index_user; ?>";
	var navbar_index_user = "<?php echo $navbar_index_user; ?>";
	var secao_barra_clara_user = "<?php echo ".".$secao_barra_clara_user; ?>";
	var b_top_user = "<?php echo ".".$b_top_user; ?>";

	jQuery('.assinatura-bar a').click(function(){
  		header_topbar_user = ".header-topbar";
		logo_index_user = " logo-index";
		navbar_index_user = " navbar-index";
		secao_barra_clara_user = ".secao-barra-clara";
		b_top_user = ".b-top";
	});

	jQuery("#header-buscar-mobile").click(function(e){
		if(jQuery(".header-pesquisa").hasClass("d-none")){
			jQuery(".header-pesquisa").removeClass("d-none");
			jQuery(".featured-content").addClass("featured-content-pesquisa");
		}else{
			jQuery(".header-pesquisa").addClass("d-none");
			jQuery(".featured-content").removeClass("featured-content-pesquisa");
		}
	});

   jQuery(window).scroll(function() {
   	if(jQuery(this).width() > 576){
       if(jQuery(this).scrollTop() > 53){
           jQuery(".hrtb").addClass("header-topbar-scroll");
           jQuery(".header-social").addClass("d-none");
           jQuery(".login-box").addClass("cart-box-scroll bg-white");
           jQuery(".ipt-scroll").addClass("pd-scroll");
           jQuery(".n-b-i").addClass("navbar-index-scroll")
           		.removeClass(navbar_index_user + " navbar-index");
           jQuery(".m-p-r").removeClass("mt-5 mt-md-2");
           jQuery(".header-social-carrinho").addClass("d-block")
				.removeClass("mt-3 d-none");
           jQuery(".p-absolute").addClass("pt-0 pb-2 logo-index-scroll")
          		.removeClass(logo_index_user + " logo-index pt-4 pb-3 pb-md-4 pb-lg-3");
           jQuery(".img-logo").addClass("img-logo-scroll");
           jQuery(".header-pesquisa").addClass("col-md-6 mt-md-2")
           		.removeClass("mt-md-4 col-md-5");
           jQuery(".header-pesquisa-lupa").addClass("header-pesquisa-lupa-scroll");
           jQuery(".h-scroll").removeClass("col-md-4")
          		 .addClass("col-md-3");
           jQuery(secao_barra_clara_user).addClass("secao-barra-clara-scroll");
           jQuery("#rev_slider_1_1_wrapper").addClass("rev-scroll");
           jQuery(b_top_user).addClass("blog-scroll");
           jQuery(".ass-b").addClass("assinatura-bar-scroll");
       } else {
           jQuery(".hrtb").removeClass("header-topbar-scroll");
           jQuery(".h-scroll").addClass("col-md-4")
           		.removeClass("col-md-3");
           jQuery(".header-social").removeClass("d-none");
           jQuery(".n-b-i").removeClass("navbar-index-scroll")
           		.addClass(navbar_index_user);
           jQuery(".m-p-r").addClass("mt-5 mt-md-2");
           jQuery(".header-social-carrinho")
           		.removeClass("d-block").addClass("mt-3 d-none");
           jQuery(".p-absolute").removeClass("pt-0 pb-2 logo-index-scroll")
           		.addClass(logo_index_user + " pt-4 pb-3 pb-md-4 pb-lg-3");
           jQuery(".img-logo").removeClass("img-logo-scroll");
           jQuery(".header-pesquisa").removeClass("col-md-6 mt-md-2")
           		.addClass("mt-md-4 col-md-5");
           jQuery(".ipt-scroll").removeClass("pd-scroll");
           jQuery(".header-pesquisa-lupa").removeClass("header-pesquisa-lupa-scroll");
           jQuery(".login-box").removeClass("cart-box-scroll bg-white");
           jQuery(secao_barra_clara_user).removeClass("secao-barra-clara-scroll");
           jQuery("#rev_slider_1_1_wrapper").removeClass("rev-scroll");
           jQuery(b_top_user).removeClass("blog-scroll");
           jQuery(".ass-b").removeClass("assinatura-bar-scroll");
       }
	}else{
		if(jQuery(this).scrollTop() > 30){
			//jQuery(header_topbar_user).addClass("header-topbar-mobile");
			jQuery(".n-b-i").addClass("navbar-mobile-scroll")
				/*.removeClass("navbar-index navbar-index-ass-bar")*/;
			//jQuery(".scroll-header-icon").removeClass("d-none");
			jQuery(".ass-b").addClass("assinatura-bar-scroll");
			jQuery(".featured-content").addClass("featured-content-scroll");
			jQuery("#barra-container").removeClass(logo_index_user);
			//jQuery("#navbarNavDropdown").addClass("d-none");
		}else {
			//jQuery(header_topbar_user).removeClass("header-topbar-mobile");
			jQuery(".n-b-i").removeClass("navbar-mobile-scroll")
				.addClass(navbar_index_user);
			jQuery(".scroll-header-icon").addClass("d-none");
			jQuery(".ass-b").removeClass("assinatura-bar-scroll");
			jQuery(".featured-content").removeClass("featured-content-scroll");
			jQuery("#barra-container").addClass(logo_index_user);
			//jQuery("#navbarNavDropdown").removeClass("d-none");
		}
	}
   });
});
</script>

