<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/wp-config.php' );

switch ($_GET ['act']) {
	case 'aprovar_reply':
		aprovar_reply($_GET['id']);
		break;
	case 'aprovar_topic':
		aprovar_topic($_GET['id']);
		break;
	case 'editar_reply':
		$reply['ID'] = $_GET['id'];
		$reply['post_content'] = $_POST['bbp_reply_content'];
		editar_reply($reply);
		break;
	case 'editar_topic':
		$topic['ID'] = $_GET['id'];
		$topic['post_title'] = $_POST['bbp_topic_title'];
		$topic['post_content'] = $_POST['bbp_topic_content'];
		editar_topic($topic);
		break;
}

function aprovar_reply($reply_id) {
	aprovar_post($reply_id);
	$forum_id = bbp_get_reply_forum_id($reply_id);
	bbp_update_forum_last_active_id($forum_id);
	bbp_update_forum_last_active_time($forum_id);
	bbp_update_forum_last_reply_id($forum_id);
	bbp_update_forum_last_topic_id($forum_id);
	$topic_id = bbp_get_reply_topic_id($reply_id);
	$url = bbp_get_topic_permalink($topic_id);
	header ("Location: {$url}");
}

function aprovar_topic($topic_id) {
	aprovar_post($topic_id);
	$url = bbp_get_topic_permalink($topic_id);
	header ("Location: {$url}");
}

function editar_reply($reply) {
	atualizar_reply($reply);
	$topic_id = bbp_get_reply_topic_id($reply['ID']);
	$url = bbp_get_topic_permalink($topic_id);
	header ("Location: {$url}");
}

function editar_topic($topic) {
	atualizar_topic($topic);
	$url = bbp_get_topic_permalink($topic['ID']);
	header ("Location: {$url}");
}