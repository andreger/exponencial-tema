<?php

/**
 * No Topics Feedback Part
 *
 * @package bbPress
 * @subpackage Theme
 */

?>

<div class="bbp-template-notice">
	<p><?php _e( 'Ainda n&atilde;o h&aacute; t&oacute;picos!', 'bbpress' ); ?></p>
</div>
