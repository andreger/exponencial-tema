<?php

/**
 * Single Forum Content Part
 *
 * @package bbPress
 * @subpackage Theme
 */
if(is_area_desativada(array(PAINEL_FORUM))){
	redirecionar_erro_500();
}
redirecionar_se_forum_expirado(bbp_get_forum_id());
?>
<script type='text/javascript'>

jQuery(function() {

	<?php if(!isset($_POST['bbp_topic_submit'])) : ?>
		jQuery(".bbp-topic-form" ).hide();
	<?php endif; ?>
	jQuery( "#hider" ).click(function() {
		 jQuery(".bbp-topic-form" ).toggle(400);
	});

	
});
</script>



            <div class="col-12 pt-4">
                <h1>Fórum</h1>
            </div>
    
<div class="col-12 mt-2 mb-5" id="bbpress-forums">
	<a href="/forum" class="bbp-breadcrumb-forum">Ir para os Fóruns</a>
	
	<?php do_action( 'bbp_template_before_single_forum' ); ?>

	<?php if ( post_password_required() ) : ?>

		<?php bbp_get_template_part( 'form', 'protected' ); ?>

	<?php else : ?>
		<?php if ( bbp_has_forums() ) : ?>

			<?php bbp_get_template_part( 'loop', 'forums' ); ?>

		<?php endif; ?>

		<?php if ( !bbp_is_forum_category() && bbp_has_topics() ) : ?>
			<?php bbp_get_template_part( 'loop',       'topics'    ); ?>
			
			<?php bbp_get_template_part( 'pagination', 'topics'    ); ?>
			
			<div class="col-12 text-center">
				<input class="btn u-btn-blue" id="hider" type="button" value="Novo T&oacute;pico">
			</div>

			<?php bbp_get_template_part( 'form',       'topic'     ); ?>

		<?php elseif ( !bbp_is_forum_category() ) : ?>

			<?php bbp_get_template_part( 'feedback',   'no-topics' ); ?>
			
			<div class="col-12 text-center">
			<input class="btn u-btn-blue" id="hider" type="button" value="Novo T&oacute;pico">
			</div>

			<?php bbp_get_template_part( 'form',       'topic'     ); ?>

		<?php endif; ?>

	<?php endif; ?>

	<?php do_action( 'bbp_template_after_single_forum' ); ?>

</div>