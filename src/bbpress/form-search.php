<?php

/**
 * Search 
 *
 * @package bbPress
 * @subpackage Theme
 */

?>
<script type="text/javascript">
jQuery(document).ready(function() {
	jQuery('.bbp-breadcrumb-current').hide();
});
</script>
<form role="search" method="get" id="bbp-search-form" action="<?php bbp_search_url(); ?>">
	<div class="row">
	<div>
		<label class="screen-reader-text hidden" for="bbp_search"><?php _e( 'Busca por:', 'bbpress' ); ?></label>
		<input class="form-control" type="hidden" name="action" value="bbp-search-request" />
		<input class="form-control" tabindex="<?php bbp_tab_index(); ?>" type="text" value="<?php echo esc_attr( bbp_get_search_terms() ); ?>" name="bbp_search" id="bbp_search" />		
	</div>
	<div class="ml-2">
		<input tabindex="<?php bbp_tab_index(); ?>" class="btn u-btn-darkblue" type="submit" id="bbp_search_submit" value="<?php esc_attr_e( 'Buscar', 'bbpress' ); ?>" />
	</div>
	</div>
</form>
