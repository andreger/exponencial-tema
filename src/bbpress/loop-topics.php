<?php

/**
 * Topics Loop
 *
 * @package bbPress
 * @subpackage Theme
 */

?>

<script>
jQuery(document).ready(function(){
	jQuery(".productab").slideDown("slow");
	
  jQuery("tr:nth-child(odd)").css("background-color","#efefef");
  jQuery("tr:nth-child(even)").css("background-color","#fff");  
});
</script>

<?php do_action( 'bbp_template_before_topics_loop' ); ?>
<table  border="0" cellspacing="0" cellpadding="0" id="table-basic" class="table table-bordered table-striped">
<thead>	
  <tr class="text-center">
	<th class="text-white bg-blue" ><?php _e( 'Tópicos', 'bbpress' ); ?></th>
	<th class="text-white bg-blue" ><?php _e( 'Participantes', 'bbpress' ); ?></th>
	<th class="text-white bg-blue" ><?php bbp_show_lead_topic() ? _e( 'Respostas', 'bbpress' ) : _e( 'Posts', 'bbpress' ); ?></th>
 </tr>
</thead>	
		<?php while ( bbp_topics() ) : bbp_the_topic(); ?>

			<?php bbp_get_template_part( 'loop', 'single-topic' ); ?>

		<?php endwhile; ?>
</table>
<?php do_action( 'bbp_template_after_topics_loop' ); ?>


<script>
	$(window).resize(function () {
    var viewportWidth = $(window).width();
    if (viewportWidth < 576) {
           $('table').addClass("table-responsive");
    }
    if (viewportWidth > 576) {
           $('table').removeClass("table-responsive");
    }
	});	

	
jQuery(document).ready(
	function () {
		html = "<a href='/forum' class='bbp-breadcrumb-forum'>Ir para os Fóruns</a><span class='bbp-breadcrumb-sep'> › </span>"
		jQuery('.bbp-pagination-count').prepend(html);
	}
);
</script>
