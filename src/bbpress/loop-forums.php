<?php
/**
 * Forums Loop
 *
 * @package bbPress
 * @subpackage Theme
 */
if(is_area_desativada(array(PAINEL_FORUM))){
	redirecionar_erro_500();
}
?>

<br />
<br />		
<?php do_action( 'bbp_template_before_forums_loop' ); ?>

<div class="mt-2">
	<table id="tabela-forums" class="table-margin-forums table table-striped table-bordered">
		<tr class="bg-blue text-white">
			<td class="font-weight-bold">F&oacute;rum</td>
			<td class="font-weight-bold">Alunos</td>
			<td class="text-center font-weight-bold">T&oacute;picos</td>
			<td class="text-center font-weight-bold">Post mais recente</td>
		</tr>
		<?php while ( bbp_forums() ) : bbp_the_forum(); ?>
			<?php bbp_get_template_part( 'loop', 'single-forum' ); ?>
		<?php endwhile; ?>
	</table>

	<?php do_action( 'bbp_template_after_forums_loop' ); ?>

	<div class="bbp-pagination bbp-mainpage">
		<?php 
		global $wp_query;

		$args = array(
			'base'               => '/forums/%_%',
			'format'             => '?page=%#%',
			'total'              => ceil($wp_query->found_posts / FORUMS_PER_PAGE),
			'current'            => get_query_var("page", 1),
			'show_all'           => false,
			'end_size'           => 1,
			'mid_size'           => 2,
			'prev_next'          => true,
			'prev_text'          => __('« Anterior'),
			'next_text'          => __('Próximo »'),
			'type'               => 'plain',
			'add_args'           => false,
			'add_fragment'       => '',
			'before_page_number' => '',
			'after_page_number'  => ''
		); 

		$start_num = intval( ( $args['current'] - 1 ) * FORUMS_PER_PAGE ) + 1;
		$from_num  = numero_inteiro( $start_num );
		$to_num    = numero_inteiro( ( $start_num + ( FORUMS_PER_PAGE - 1 ) > $wp_query->found_posts ) ? $wp_query->found_posts : $start_num + ( FORUMS_PER_PAGE - 1 ) );
		$total_int = (int) $wp_query->found_posts;
		$total     = numero_inteiro( $total_int );
		?>

		<div class="bbp-pagination-count mt-2">
			Visualizando fóruns de <?= $from_num ?> até <?= $to_num ?> (de <?= $total ?> do total)
		</div>

		<div class="bbp-pagination-links">
			<?= paginate_links( $args ); ?>
		</div>
		
	</div>
	
	<div class="col-12 text-center mt-3">
		<a class="btn u-btn-darkblue" href="/forum">Voltar &agrave; listagem normal</a>
	</div>
</div>
<script>
	jQuery(window).resize(function () {
    var viewportWidth = jQuery(window).width();
    if (viewportWidth < 576) {
           jQuery("#tabela-forums").addClass("table-responsive");
    }
    if (viewportWidth > 576) {
           jQuery("#tabela-forums").removeClass("table-responsive");
    }
	});	

</script>


