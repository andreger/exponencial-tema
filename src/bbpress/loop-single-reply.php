<?php

/**
 * Replies Loop - Single Reply
 *
 * @package bbPress
 * @subpackage Theme
 */

$is_professor = is_professor( bbp_get_reply_author_id() );
$is_moderador = is_user_forum_moderador(get_current_user_id(), bbp_get_reply_forum_id());
$is_autor_and_reply_pendente = is_autor_and_reply_pendente(bbp_get_reply_id());
if(is_reply_acessivel(bbp_get_reply_id())) {
?>
<div id="post-<?php bbp_reply_id(); ?>" class="bbp-reply-header">

	<div class="bbp-meta">

		<span class="bbp-reply-post-date"><?php bbp_reply_post_date(); ?></span>

		<?php if ( bbp_is_single_user_replies() ) : ?>

			<span class="bbp-header">
				<?php _e( 'em resposta a: ', 'bbpress' ); ?>
				<a class="border bbp-topic-permalink" href="<?php bbp_topic_permalink( bbp_get_reply_topic_id() ); ?>"><?php bbp_topic_title( bbp_get_reply_topic_id() ); ?></a>
			</span>

		<?php endif; ?>
		
		
		<?php do_action( 'bbp_theme_before_reply_admin_links' ); ?>

		<?php bbp_reply_admin_links(); ?>

		<?php do_action( 'bbp_theme_after_reply_admin_links' ); ?>

	</div><br><!-- .bbp-meta -->

</div>

<div <?php bbp_reply_class(); ?>>

	<div class="bbp-reply-author">

		<?php do_action( 'bbp_theme_before_reply_author_details' );
		
			if($is_professor) { ?>
				<a href="<?= get_autor_url(bbp_get_reply_author_id()) ?>">
				<?php bbp_reply_author_link( array( 'sep' => '<br />', 'show_role' => true ) );?></a>
			<?php 
			}
			else {
				echo get_foto_usuario(bbp_get_reply_author_id()) . "<br>";
				echo get_usuario_nome_exibicao(bbp_get_reply_author_id()) . "<br>";
				echo bbp_get_reply_author_role();
// 				bbp_reply_author_link( array( 'sep' => '<br />', 'show_role' => true ) ); 
			}
			
			if ( bbp_is_user_keymaster() ) : ?>

			<?php do_action( 'bbp_theme_before_reply_author_admin_details' ); ?>

			<div class="bbp-reply-ip"><?php bbp_author_ip( bbp_get_reply_id() ); ?></div>

			<?php do_action( 'bbp_theme_after_reply_author_admin_details' ); ?>

		<?php endif; ?>

		<?php do_action( 'bbp_theme_after_reply_author_details' ); ?>

	</div><!-- .bbp-reply-author -->

	<div class="bbp-reply-content">

		<?php do_action( 'bbp_theme_before_reply_content' ); ?>
		<?php if($is_autor_and_reply_pendente) :?>
			<p> (Sua mensagem foi enviada. Aguardando moderação para ser publicada no fórum)<br />
				<?php echo get_post_field('post_content', bbp_get_reply_id());?></p>
		<?php else : ?>
			<?php html_entity_decode(bbp_reply_content(), ENT_COMPAT, 'UTF-8'); ?>
		<?php endif; ?>

		<?php do_action( 'bbp_theme_after_reply_content' ); ?>

	</div><!-- .bbp-reply-content -->

</div><!-- .reply -->
<?php }?>