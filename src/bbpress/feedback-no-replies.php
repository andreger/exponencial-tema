<?php

/**
 * No Replies Feedback Part
 *
 * @package bbPress
 * @subpackage Theme
 */

?>

<div class="bbp-template-notice">
	<p><?php _e( 'Ainda n&atilde;o h&aacute; respostas!', 'bbpress' ); ?></p>
</div>
