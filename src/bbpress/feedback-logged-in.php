<?php

/**
 * Logged In Feedback Part
 *
 * @package bbPress
 * @subpackage Theme
 */

?>

<div class="bbp-template-notice info">
	<p><?php _e( 'Voc&ecirc; j&aacute; est&aacute; logado.', 'bbpress' ); ?></p>
</div>
