<?php

/**
 * No Access Feedback Part
 *
 * @package bbPress
 * @subpackage Theme
 */

?>

<div id="forum-private" class="bbp-forum-content">
	<h1 class="entry-title"><?php _e( 'Privado', 'bbpress' ); ?></h1>
	<div class="entry-content">
		<div class="bbp-template-notice info">
			<p><?php _e( 'Voc&ecirc; n&atilde;o tem permiss&atilde;o para acessar este f&oacute;rum.', 'bbpress' ); ?></p>
		</div>
	</div>
</div><!-- #forum-private -->
