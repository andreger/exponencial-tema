<?php

/**
 * Forums Loop - Single Forum
 *
 * @package bbPress
 * @subpackage Theme
 */
 
if($_GET['update'] == true) {
	set_time_limit(180);
	
	$produto = get_produto_by_forum_id(bbp_get_forum_id());
	$usuarios = get_usuarios_que_compraram($produto->id);
	
	bbp_remove_forum_from_all_subscriptions(bbp_get_forum_id());
	
	foreach ($usuarios as $usuario) {
		bbp_add_user_forum_subscription($usuario->ID, bbp_get_forum_id());
	}
}
?>

<tr>
	<td class="">

		<?php do_action( 'bbp_theme_before_forum_title' ); ?>

		<a class="bbp-forum-title" href="<?php bbp_forum_permalink(); ?>"><?php bbp_forum_title(); ?></a>

		<?php do_action( 'bbp_theme_after_forum_title' ); ?>

		<?php do_action( 'bbp_theme_before_forum_sub_forums' ); ?>

		<?php bbp_list_forums(); ?>

		<?php do_action( 'bbp_theme_after_forum_sub_forums' ); ?>

		<?php bbp_forum_row_actions(); ?>

	</td>
	
	<td class="text-center"><?php echo count(bbp_get_forum_subscribers()) ?></td>
	
	<td class="text-center"><?php bbp_forum_topic_count(); ?></td>

	<td class="text-center">

		<?php do_action( 'bbp_theme_before_forum_freshness_link' ); ?>

		<?php bbp_forum_freshness_link(); ?>

		<?php do_action( 'bbp_theme_after_forum_freshness_link' ); ?>

		<p class="bbp-topic-meta">

			<?php do_action( 'bbp_theme_before_topic_author' ); ?>

			<span class="bbp-topic-freshness-author">
			<?php 
				if(bbp_get_forum_topic_count() > 0)
					echo get_foto_usuario(bbp_get_forum_last_reply_author_id(), 14) . ' ' . get_usuario_nome_exibicao(bbp_get_forum_last_reply_author_id()) ?>
			</span>

			<?php do_action( 'bbp_theme_after_topic_author' ); ?>

		</p>
	</td>
						
</tr>
