<?php

/**
 * Topics Loop - Single
 *
 * @package bbPress
 * @subpackage Theme
 */

$is_professor = is_professor();
$has_resposta_pendente = has_resposta_pendente(bbp_get_topic_id());
$is_moderador = is_user_forum_moderador(get_current_user_id(), bbp_get_topic_forum_id());
if(is_topico_acessivel(bbp_get_topic_id())) {
?><tbody>
	<tr class="bg-white text-center forum-status-<?php bbp_topic_status() ?>">
		<td>
<?php 		do_action( 'bbp_theme_before_topic_title' ); ?>
			<a class="t-d-none text-blue bbp-topic-permalink" href="<?php bbp_topic_permalink(); ?>"><?php bbp_topic_title(); ?></a>
<?php 
			do_action( 'bbp_theme_after_topic_title' );
			bbp_topic_pagination();
			do_action( 'bbp_theme_before_topic_meta' ); ?>
			
			<p class="bbp-topic-meta">
<?php 			do_action( 'bbp_theme_before_topic_started_by' );
			
				if($is_professor) { ?>
					<span class="bbp-topic-started-by">Iniciado por: 
						<a href="<?= get_autor_url(bbp_get_topic_author_id()) ?>">
<?php 						echo bbp_get_topic_author_link( array( 'size' => '14' ) )?></a>
					</span>
<?php 
				}
				else {
?>
					<span class="bbp-topic-started-by">Iniciado por: <?php echo get_foto_usuario(bbp_get_topic_author_id(), 14) . ' ' . get_usuario_nome_exibicao(bbp_get_topic_author_id())?></span>
<?php 			} ?>
			
<?php 			do_action( 'bbp_theme_after_topic_started_by' ); ?>

<?php 			if ( !bbp_is_single_forum() || ( bbp_get_topic_forum_id() !== bbp_get_forum_id() ) ) {

					do_action( 'bbp_theme_before_topic_started_in' ); ?>
	
					<span class="bbp-topic-started-in"><?php printf( __( 'em: <a href=\"%1$s\">%2$s</a>', 'bbpress' ), 
							bbp_get_forum_permalink( bbp_get_topic_forum_id() ), bbp_get_forum_title( bbp_get_topic_forum_id() ) ); ?></span>

<?php				do_action( 'bbp_theme_after_topic_started_in' );
				} ?>
			</p>
<?php 
			do_action( 'bbp_theme_after_topic_meta' );
			bbp_topic_row_actions(); 
?>
		</td>
		<td><?php bbp_topic_voice_count(); ?></td>

		<td class="numero-post">
<?php 		bbp_show_lead_topic() ? bbp_topic_reply_count() : bbp_topic_post_count();
			if($has_resposta_pendente && $is_moderador) { ?>
				 <img class="numero-post-warning" alt="Existem respostas pendentes!" src="/wp-content/themes/academy/images/warning.png">
<?php 		} ?>
		</td>
	</tr></tbody>
<?php }?>
