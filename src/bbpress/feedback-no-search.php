<?php

/**
 * No Search Results Feedback Part
 *
 * @package bbPress
 * @subpackage Theme
 */

?>

<div class="bbp-template-notice">
	<p><?php _e( 'A Busca n&atilde;o obteve resultados!', 'bbpress' ); ?></p>
</div>
