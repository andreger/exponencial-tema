<?php

/**
 * Template Name: Cadastro Coaching
*/

get_header();
?>
<div class="row">
<?php
redirecionar_se_nao_estiver_logado();

global $wpdb;
global $current_user;

$turma = $_GET['t'];

include_once $_SERVER['DOCUMENT_ROOT'] . '/corporativo/coaching.php';

if(count(Coaching::get_aluno_by_email($current_user->user_email)) == 0) {
	$aluno = Coaching::init($current_user->display_name, $current_user->user_email, $turma);
	Coaching::start($aluno);
	Coaching::goto_step(1, $aluno);
	Coaching::send_email(1, $aluno);
	echo "<div style='margin-top:50px;font-size: 30px'><h2>Obrigado. Verifique seu e-mail para saber os próximos passos.</h2></div>";
} else {
	echo "<div style='margin-top:50px;font-size: 30px'><h2>Sua inscrição já tinha sido inicializada. Verifique seu e-mail.</h2></div>";
}
?>
</div>
</div><!-- #content -->
<div class="sectionlargegap"></div>
<?php get_footer(); ?>  
