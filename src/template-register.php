<?php
/*
 * Template Name: Registration
 */

/**
 * Se usuário estiver logado redirecionar para Home
 *
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2319
 */
KLoader::helper("RecaptchaHelper");

if (is_usuario_logado()) {
    echo "<script>window.location.href='/';</script>";
}
$debug = "nao esta logado<br>";
KLoader::helper("UrlHelper");
if(UrlHelper::is_url_cadastro_login()){
	$register_url = home_url() . "/cadastro-login/";
	if(isset($_GET['ref']) && $_GET['ref']){
		$register_url .= "?ref=".urlencode( $_GET['ref'] );
	}
}

if (
    (isset($_POST['submit-register']) && $_POST['submit-register']) 
        || (isset($_GET['is_ajax']) && $_GET['is_ajax'])
) {
    $debug .= "submeteu<br>";
    if (is_selenium() || is_desenvolvimento()) {
        $debug .= "eh selenium/desenvolvimento<br>";
        $validacao_ok = true;
    } else {
        $debug .= "nao eh selenium/desenvolvimento<br>";
        if ($_POST["g-recaptcha-response"]) {
            $debug .= "tem repatcha<br>";
            $response = RecaptchaHelper::get_response($_POST['g-recaptcha-response']);
            $debug .= print_r($response, true) . "<br>";
            if ($response != null && $response->success) {
                $debug .= "validação ok<br>";
                $validacao_ok = true;
            } else {
                $debug .= "validação nao ok<br>";
                $validacao_ok = false;
                $erro_validacao = "Código de verificação incorreto";
            }
        } else {
            $debug .= "nao tem recaptcha -> validação nao ok<br>";
            $validacao_ok = false;
            $erro_validacao = "Código de verificação incorreto";
        }
    }

    if ($validacao_ok) {
        $debug .= "vai cadastrar<br>";
        global $wpdb;

        $nome = $_POST['nome'];
        $sobrenome = $_POST['sobrenome'];
        $mail = $_POST['email'];
        $data_nascimento = $_POST['data_nascimento'];
        $senha = $_POST['senha'];
        $confirm_senha = $_POST['confirm_senha'];
        $check = isset($_POST['check']) ? $_POST['check'] : false;

        $display_name = $nome . " " . $sobrenome;

        $userdata = array(
            'user_login' => $mail,
            'user_email' => $mail,
            'user_pass' => $senha,
            'role' => 'Aluno',
            'display_name' => $display_name,
            'first_name' => $nome,
            'last_name' => $sobrenome
        );

        $uid = wp_insert_user($userdata);

        if ( $uid && is_wp_error( $uid ) )
        {
            $debug .= "erro uid: " . $uid->get_error_message() . "<br>";
            $erro_validacao = $uid->get_error_message();
        }
        else
        {
            $debug .= "inseriu usuario<br>";
            add_user_meta($uid, 'oneTarek_check', $check);
            add_user_meta($uid, 'oneTarek_date', $data_nascimento);
            
            unlink(get_foto_usuario_filepath($uid));

            $mensagem = get_template_email('aluno-cadastrado.php');

            enviar_email($mail, "Exponencial Concursos - Acesse sua Conta!", $mensagem, 'exponencial@exponencialconcursos.com.br', null);
            $debug .= "vai pegar o user e logar<br>";
            //Loga o usuário no sistema e encaminha para a página inicial do portal
            $user = get_user_by('ID', $uid );
            if ( $user && !is_wp_error( $user ) )
            {
                $debug .= "pegou user<br>";
                wp_clear_auth_cookie();
                wp_set_current_user ( $user->ID );
                wp_set_auth_cookie  ( $user->ID );
                $debug .= "autenticacao usuario ok<br>";
                if(isset($_GET['ref']) && $_GET['ref']){
                    $debug .= "tem ref<br>";
                    $redirect_to = $_GET['ref'];
                }else{
                    $debug .= "nao tem ref, vai pra home<br>";
                    $redirect_to = home_url();
                }			
                
                if(isset($_GET['is_ajax'])){
                    $debug .= "is_ajax<br>";
                }else{
                    $debug .= "fazendo redirect<br>";
                    wp_safe_redirect( $redirect_to );
                    exit();
                }
            }
            else
            {
                $debug .= "erro user: " . $user->get_error_message() . "<br>";
                $erro_validacao = $user->get_error_message();
            }
        }
    }
}
?>

<?php if($_GET['is_ajax']): //Se chegou aqui então deu erro ?>

    <?php 
        echo json_encode(['erro' => $erro_validacao, 'redirect' => $redirect_to/*, 'debug' => $debug*/]);
    ?>

<?php else: ?>

    <?php get_header(); ?>

    <?php KLoader::view('cadastro_login/cadastro_login', ['register_url' => $register_url, 'erro_validacao' => $erro_validacao/*, 'debug' => $debug*/]) ?>
    <?php 
        //Foi separado da view pq estava interferindo no funcionamento do modal de login
        KLoader::view('cadastro_login/cadastro_login_scripts') 
    ?>

    <?php get_footer(); ?>

<?php endif; ?>