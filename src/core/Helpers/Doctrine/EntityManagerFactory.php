<?php

namespace Exponencial\Core\Helpers\Doctrine;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;

class EntityManagerFactory
{

    public function getEntityManager(): EntityManager
    {
        $rootDir = __DIR__ . '/../../exponencial-tema';

        $config = Setup::createAnnotationMetadataConfiguration(
            [$rootDir . '/core/Models'],
            true
        );

        $connection = [
            'driver' => 'pdo_mysql',
            'user' => 'root',
            'password' => 'rootpass',
            'dbname' => 'exponenc_db'
        ];

        return EntityManager::create($connection, $config);
    }
}
