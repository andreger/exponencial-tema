<?php

namespace Exponencial\Core\Helpers;

use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Loader\FilesystemLoader;
use Twig\Environment;

class Template
{
    private $loader = null;

    private $twig = null;
    
    private const TWIG_LOADER_PATH = '../../exponencial-wp/www/wp-content/themes/academy/core/resources/views';
    
    /**
     * Método construtor
     */
    public function __construct()
    {
        $this->loader = new FilesystemLoader(self::TWIG_LOADER_PATH);
        $this->twig = new Environment($this->loader, ['debug' => true]);
    }

    /**
     * @param string $template
     * @param array $variaveis
     * @param string|null $cacheNome
     * @param int $cacheExpiracao
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    
    public function renderPage(string $template, array $variaveis = [], string $cacheNome = null, int $cacheExpiracao = 0): void
    {
        get_header();
        
        echo self::render($template, $variaveis, $cacheNome, $cacheExpiracao);
        
        get_footer();
    }

    /**
     * Retorna um template
     *
     * @param string $template
     * @param array $variaveis
     * @param string $cacheNome
     * @param int $cacheExpiracao
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     *
     * @suppress PhanUndeclaredFunction
     */

    public function render(string $template, array $variaveis = [], string $cacheNome = null, int $cacheExpiracao = 0): string
    {
        $cacheNome = null;

        if($cacheNome) {
            $cacheFactory = new CacheFactory();
            $memcached = $cacheFactory->getMemcached();

            $cacheValor = $cacheNome ? $memcached->get($cacheNome) : null;

            if(!$cacheValor) {
                $cacheValor = $this->twig->render($template, $variaveis);
                $memcached->set($cacheNome, $cacheValor, $cacheExpiracao);
            }

            return $cacheValor;
        }
        else {
            return $this->twig->render($template, $variaveis);
        }
    }
}
