<?php

namespace Exponencial\Core\Controllers\Footer;

/**
 * @todo Remover modelos antigo
 */

use Exponencial\Core\Helpers\Template;
use Kcore\models\MailChimpModel;

/**
 * @todo Remover inclusão de wp-load
 */
include_once $_SERVER['DOCUMENT_ROOT'] .'/wp-load.php';

class CadastroGratisController
{
    public function exibirForm(int $post_id) : void
    {
        if($this->deveExibir($post_id)) {
            $options = MailChimpModel::listar_interesses(MC_LISTA_EXPONENCIAL_CONCURSOS, MC_INTERESSE_AREA);

            /** @todo Transformar isso em view */
            $recaptcha = carregar_recaptcha();

            $template = new Template();

            echo $template->render('comum/footer/cadastro-gratis/form.html.twig', compact('options', 'recaptcha'));
        }
    }

    /**
     * Checa se o formulário de cadastro grátis deve ser exibido
     *
     * @param int $post_id
     * @return bool
     */
    private function deveExibir(int $post_id) : bool
    {
        $post = get_post($post_id);
        $postType = $post->post_type;

        if($postType == "post") {
            return true;
        }
        elseif($postType == "page") {
            return $this->deveExibirPagina($post->post_name);
        }

        return false;
    }

    /**
     * Checa se o formulário de cadastro grátis deve ser exibido em uma página
     * @param string $slug
     * @return bool
     */
    private function deveExibirPagina(string $slug) : bool
    {
        $paginasPermitidas = [
            'home',
            'sistema-de-questoes',
            'simulados',
            'professores',
            'blog-noticias',
            'blog-posts-noticias',
            'blog-artigos',
            'blog-posts-artigos',
            'blog-videos',
            'blog-posts-videos',
            'descontos-promocoes',
            'metodologia-exponencial',
            'quem-somos',
            'perguntas-frequentes-portal',
            'como-funciona',
            'parceiros',
        ];

        if(in_array($slug, $paginasPermitidas)) {
            return true;
        }

        return false;
    }

    public function carregarModal()
    {
        $template = new Template();
        echo $template->render('comum/footer/cadastro-gratis/modal.html.twig');
    }
}