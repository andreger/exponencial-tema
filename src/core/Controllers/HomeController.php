<?php
namespace Exponencial\Core\Controllers;

use Exponencial\Core\Helpers\Template;

class HomeController
{
    
    /**
     * Renderiza a página principal do sistema
     */
    
    public function index(): void
    {

        $template = new Template();

        $blogs = BlogModel::listar();
        
        echo $template->renderPage('home/index.html.twig');
    }

}
