<?php

namespace Exponencial\Core\Controllers\Header;

use Exponencial\Core\Helpers\Template;

class CarrinhoController
{
    /**
     * Renderiza o popup do carrinho
     */
    public function exibirCarrinho() : void
    {
        /** @todo Acesso do carrinho deve ser feito fora do objeto global. Deve-se usar a sessão */
        global $woocommerce;

        $total = $woocommerce->cart->subtotal;
        $produtos = [];

        foreach($woocommerce->cart->cart_contents as $cart_item) {
            $produto = new \stdClass();

            /** @todo Dados devem ser acessados pelo ORM */

            // Recupera o nome
            $wc_product = get_product($cart_item['product_id']);
            $produto->nome = $wc_product->get_title();

            // Recupera a imagem
            $url = wp_get_attachment_url($wc_product->get_image_id());
            $produto->imagem  = @getimagesize($url) ? get_resized_image_tag($url, 36, 36) : "";

            // Recupera o preço
            $produto->preco = $cart_item['line_total'];

            $produtos[] = $produto;
        }

        $template = new Template();

        echo $template->render('comum/header/carrinhoPopup.html.twig', compact('total', 'produtos'));
    }
}