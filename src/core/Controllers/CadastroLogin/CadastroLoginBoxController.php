<?php

namespace Exponencial\Core\Controllers\CadastroLogin;

use Exponencial\Core\Helpers\Template;

/**
 * @todo Remover inclusão de wp-load
 */
include_once $_SERVER['DOCUMENT_ROOT'] .'/wp-load.php';

class CadastroLoginBoxController
{
    public function carregarModal()
    {
        $template = new Template();

        echo $template->render("cadastroLogin/cadastroLoginBox/modal");
    }
}