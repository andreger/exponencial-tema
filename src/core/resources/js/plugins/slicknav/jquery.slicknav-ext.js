jQuery(function() {
    jQuery('#menu-button').click(function() {
        jQuery('.login-box-mobile').hide();
        jQuery('.slicknav_parent ul').css('display', 'block');
        jQuery('#pai-menu').slicknav('toggle');
    });

    jQuery('#pai-menu').slicknav({
        'prependTo': '#menu-mobile-rendered',
        'allowParentLinks': true
    });
});