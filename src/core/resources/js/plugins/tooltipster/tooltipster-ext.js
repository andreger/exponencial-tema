function gerar_tooltipster(){
    
    jQuery('.tooltipster, .tooltipster-top').tooltipster({
      maxWidth: 300,
      theme: 'tooltipster-shadow',
      interactive: true,
    });
    
    jQuery('.tooltipster-right').tooltipster({
      maxWidth: 300,
      theme: 'tooltipster-shadow',
      side: 'right',
      interactive: true,
    });
    
    jQuery('.tooltipster-left').tooltipster({
      maxWidth: 300,
      theme: 'tooltipster-shadow',
      side: 'left',
      interactive: true,
    });

    jQuery('.tooltipster-bottom').tooltipster({
      maxWidth: 300,
      theme: 'tooltipster-shadow',
      side: 'bottom',
      interactive: true,
    });
    
}

jQuery(function() {
    gerar_tooltipster();
});