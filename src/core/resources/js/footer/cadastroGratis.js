var CadastroGratis = function() {

    function init() {
        jQuery(function () {
            const post_id = jQuery('#cadastro-gratis').data('post-id');
            const url = '/wp-content/themes/academy/ajax/cadastroGratisExibirForm.php?post_id=' + post_id;

            jQuery.get(url, function (data) {
                jQuery("#cadastro-gratis").html(data);

                initListeners();
            });
        });
    }

    function initListeners() {
        var modalCarregada = false;
        jQuery("#cadastro-gratis-nome").on('focus', function() {
            if(!modalCarregada) {
                modalCarregada = true;

                //const url = '/cadastro-gratis/carregarModal';
                const url = '/wp-content/themes/academy/ajax/cadastroGratisModal.php';

                jQuery.get(url, function (data) {
                    jQuery("#mc-cadastrar").prop("disabled", false);
                    jQuery("#cadastro-gratis-modal").html(data);
                });
            }
        });
    }

    return {
        'init': function() {
            init();
        }
    };
}();

CadastroGratis.init();