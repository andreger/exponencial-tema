function resizeHeader() {
    jQuery(".header-push").height(jQuery(".header-wrap").height());
}

function pesquisaBuscar() {
	jQuery("#pesquisa-form").prop("action", jQuery("#pesquisa-pagina").val());
	jQuery("#pesquisa-tipo").val(jQuery("#tipo").val());
	jQuery("#pesquisa-ordem").val(jQuery("#ordem").val());
	jQuery("#pesquisa-texto").val(jQuery('#busca2').val());
    jQuery("#pesquisa-busca").trigger("click");
    jQuery("#carregado").hide();
    jQuery("#carregando").show();
}

function pesquisaReset() {
	jQuery("#pesquisa-tipo").val("");
	jQuery("#pesquisa-ordem").val("");
	jQuery("#pesquisa-texto").val("");
}

var Header = function () {
	// Checagem de downloads agendados
	function checarDownloadsAgendados(timeout) {

	    setTimeout(function () {
            if(jQuery(".downloads-agendados-div").length) {
                jQuery.get("/wp-content/themes/academy/ajax/checar_downloads_agendados.php", function(data) {
                    if(parseInt(data, 10) == 1) {
                        jQuery(".downloads-agendados-div").show();
                    }
                    else {
                        jQuery(".downloads-agendados-div").hide();
                    }
                });
            }
            checarDownloadsAgendados(timeout * 2);

        }, timeout);

	}
	
	return {
		init: function() {
			// realiza a primeira verificação da checagem de downloads agendados
            checarDownloadsAgendados(20000);
		}
	};	
}();

var FbPixel = function () {

	function initListeners() {
		jQuery(".btn-adicionar-carrinho").click(function() {

			var nome = jQuery(this).data("nome"),
			    id = jQuery(this).data("id"),
			    preco = jQuery(this).data("preco");
			
			fbq('track', 'AddToCart', {
		        content_name: nome, 
		        content_category: '',
		        content_ids: [id],
		        content_type: 'product',
		        value: preco,
		        currency: 'BRL' 
			});
		});
	}
	
	return {
		init: function() {
			initListeners();	
		}
	};	
}();
var LoginModal = function () {

    function initListeners() {
        var modalCarregada = false;

        jQuery(".login-modal-link").on("click", function(e) {
            e.preventDefault();

            var redirect = jQuery(this).data("redirect");
            if(!redirect){
                redirect = window.location.href;
            }
            var el = jQuery(this);
            el.prop('disabled', true);

            if(!modalCarregada) {
                jQuery.get('/wp-content/themes/academy/ajax/carregarLoginModal.php', function(data) {
                    jQuery('#login-modal-div').html(data);
                    modalCarregada = true;

                    jQuery("#login-modal").data("redirect", redirect);
                    var inst = jQuery('#login-modal').modal();
                    el.prop('disabled', false);
                    jQuery("#login-modal").data("redirect", null);
                });
            }
            else {
                jQuery("#login-modal").data("redirect", redirect);
                var inst = jQuery('#login-modal').modal();
                el.prop('disabled', false);
                jQuery("#login-modal").data("redirect", null);
            }


        });
    }

    return {
        init: function () {
            initListeners();
        }
    }
}();

jQuery(function() {
    jQuery(window).resize(function() {
        resizeHeader()
    });    
    
//    jQuery("#busca").easyAutocomplete({
//
//        url: "/wp-content/themes/academy/ajax/busca.php",
//
//        getValue: "name",
//
//        list: {   
//            match: {
//                enabled: true
//            }    
//        },
//
//    });
	
    
    jQuery(".menu-item-has-children .sub-menu").before("<span class='seta-branca'><img src='/wp-content/themes/academy/images/seta-branca.png'></span>");
    jQuery(".menu-item-has-children").mouseenter(function() {
        jQuery(this).find(".seta-branca").show();
    });
    jQuery(".menu-item-has-children").mouseleave(function() {
        jQuery(this).find(".seta-branca").hide();
    });
  
	jQuery(".coaching2-area-link").click(function(e) {
		e.preventDefault();
		jQuery(this).closest(".coaching2-row").find(".coaching2-area-cinza").toggle();
	});

    jQuery("#owl-demo").owlCarousel({
        margin: 20,
        nav: true,
        navText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
        loop: true,
        dots: false,
        responsive: {
            0:{
                items:1
            },
            640:{
                items:2
            },
            1000: {
                items:5
            }
        }
    });

    jQuery("#pesquisa-grid").click(function() {
        jQuery(".pesquisa-box")
        .removeClass('twelvecol')
        .addClass('fourcol')
        .addClass('pesquisa-box-grid');
         jQuery(".pesquisa-rodape")        
        .addClass('h-rodape-grid')
        .removeClass('h-pesquisa-rodape');
        jQuery(".pesquisa-parcelado")
        .removeClass('d-md-inline');
        

    });

    jQuery("#pesquisa-linha").click(function() {
        jQuery(".pesquisa-box")
        .removeClass('fourcol')
        .addClass('twelvecol')
        .removeClass('pesquisa-box-grid');
         jQuery(".pesquisa-rodape")
        .removeClass('h-rodape-grid')
        .addClass('h-pesquisa-rodape');
        jQuery(".pesquisa-parcelado")
        .addClass('d-md-inline');
    });

    jQuery("#tipo").change(function() {
        pesquisaBuscar();
    });

    jQuery("#ordem").change(function() {
        pesquisaBuscar();
    });
    
    jQuery('#busca2').keydown (function(e) {
		if(e.keyCode == 13) {
			jQuery("#busca").val("");
			pesquisaBuscar();
            return false;
		}
	});
    
    jQuery('#busca').keydown (function(e) {
		if(e.keyCode == 13) {
			pesquisaReset();
			jQuery("#pesquisa-busca").trigger("click");
            return false;
		}
	});

    jQuery("#texto-btn").click(function() {
    	jQuery("#busca").val("");
    	pesquisaBuscar();
    });

    jQuery("#header-buscar").click(function(e) {
        e.preventDefault();
        pesquisaReset();
        jQuery('#pesquisa-busca').trigger("click");
    });

    jQuery('#filtro-noticias').click(function(e) { 
        e.preventDefault(); jQuery('#midias').html('</p><div style="margin-bottom: 30px">Carregando...</div><p>'); 
        jQuery.get('/wp-content/themes/academy/xhr-filtrar-midias.php?midia=news', function(data) { 
            jQuery('#midias').html(data); 
        }); 
    });

    jQuery('#filtro-artigos').click(function(e) {
        e.preventDefault();
        jQuery('#midias').html('</p><div style="margin-bottom: 30px">Carregando...</div><p>'); 
        jQuery.get('/wp-content/themes/academy/xhr-filtrar-midias.php?midia=articles', function(data) {
            jQuery('#midias').html(data); 
        });
    });

    jQuery('#filtro-videos').click(function(e) {
        e.preventDefault();
        jQuery('#midias').html('</p><div style="margin-bottom: 30px">Carregando...</div><p>'); 
        jQuery.get('/wp-content/themes/academy/xhr-filtrar-midias.php?midia=videos', function(data) {
            jQuery('#midias').html(data); 
        });
    });

    jQuery('#filtro-edital-proximo').click(function(e) { 
        e.preventDefault(); 
        jQuery('#concursos').data("status", 2);
        jQuery('#concursos').html('</p><div style="margin-bottom: 30px">Carregando...</div><p>'); 
        jQuery.get('/wp-content/themes/academy/ajax/filtrar_concursos.php?status=2', function(data) { 
            jQuery('#concursos').html(data); 
        });
    });

    jQuery('#filtro-edital-divulgado').click(function(e) { 
        e.preventDefault(); 
        jQuery('#concursos').data("status", 1);
        jQuery('#concursos').html('</p><div style="margin-bottom: 30px">Carregando...</div><p>'); 
        jQuery.get('/wp-content/themes/academy/ajax/filtrar_concursos.php?status=1', function(data) { 
            jQuery('#concursos').html(data); 
        });
    });

    jQuery('#filtro-todos-concursos').click(function(e) { 
        e.preventDefault(); 
        jQuery('#concursos').data("status", 0);
        jQuery('#concursos').html('</p><div style="margin-bottom: 30px">Carregando...</div><p>'); 
        jQuery.get('/wp-content/themes/academy/ajax/filtrar_concursos.php', function(data) { 
            jQuery('#concursos').html(data); 
        });
    });

    jQuery('#carregar-mais').click(function(e) {
        e.preventDefault();

        jQuery("#carregar-mais").html("Carregando...");
        var offset = jQuery('.box-home-concursos').length;
        var status = jQuery('#concursos').data("status");

        if(status == undefined) {
            status = 0;
        }

        jQuery.get('/wp-content/themes/academy/ajax/filtrar_concursos.php?status='+status+'&offset='+offset, function(data) { 

            if(data.length > 0) {
                jQuery("#carregar-mais").html("CARREGAR MAIS");
                jQuery('#concursos').append(data); 
            }
            else {
                jQuery("#carregar-mais").html("");
            }
        });
        
    });

    jQuery('.woocommerce').on("click", ".finalizar", function(e) {
        e.preventDefault();
        jQuery("#place_order").click();
    });

    jQuery(".pesquisa-barra#texto").keydown(function(e) {
        if(e.keyCode == 13) {
            return false;
        }
    });
    
    jQuery("#login-rapido-senha").keydown(function(e) {
        if(e.keyCode == 13) {
        	jQuery('#frm-login-box').submit();
        }
    });

    resizeHeader();
    
    Header.init();
    
    FbPixel.init();

    LoginModal.init();
});