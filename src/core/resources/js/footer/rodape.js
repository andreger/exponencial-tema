var Rodape = function() {

    function init() {
        jQuery(function () {
            jQuery.get('/wp-content/themes/academy/ajax/rodape.php', function (data) {
                jQuery("#rodape").html(data);

                initListeners();
            });
        });
    }

    function initListeners() {

    }

    return {
        'init': function() {
            init();
        }
    };
}();

Rodape.init();