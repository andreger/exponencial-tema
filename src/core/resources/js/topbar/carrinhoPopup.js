var CarrinhoPopup = function() {

    function init() {
        const num_items = jQuery("#carrinho-qtde").data("qtde");

        if (num_items > 0) {
            initCarrinhoPopup();
        }
    }

    function initCarrinhoPopup() {
        let carregado = false;

        jQuery(".cart-button").mouseover(function () {
            jQuery('#login-content').hide();
            jQuery('#cart-content').show();

            if(!carregado) {
                carregado = true;
                jQuery.get('/header/exibirCarrinho', function(data) {
                   jQuery("#cart-content").html(data);
                });
            }

        });

        jQuery('#cart-content').mouseleave(function () {
            jQuery('#cart-content').hide();
        });
    }

    return {
        'init': function() {
            init();
        }
    };
}();

CarrinhoPopup.init();