var LoginPopup = function() {

    function init() {
        jQuery('.login-close-btn a').click(function () {
            jQuery('#login-content').hide();
        });

        jQuery("#login-button").mouseover(function () {
            jQuery('#cart-content').hide();
            jQuery('#login-content').show();
        });
        jQuery("#login-button-scroll").mouseover(function () {
            jQuery('#cart-content').hide();
            jQuery('#login-content').show();
        });

        jQuery('#login-button-mobile').click(function () {
            jQuery('#pai-menu').slicknav('close');
            jQuery('.login-box-mobile').toggle();
        });

        jQuery(document).click(function (event) {
            if (!jQuery(event.target).closest('#login-content').length) {
                if (jQuery('#login-content').is(":visible")) {
                    jQuery('#login-content').hide();
                }
            }
        });
    }

    return {
        'init': function() {
            init();
        }
    };
}();

LoginPopup.init();