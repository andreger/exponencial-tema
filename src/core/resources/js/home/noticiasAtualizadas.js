var NoticiasAtualizadas = function() {

    function init() {
        jQuery(function () {
            jQuery.get('/wp-content/themes/academy/ajax/home_noticias_atualizadas.php', function (data) {
                jQuery("#noticias-atualizadas").html(data);

                initListeners();
            });
        });
    }

    function initListeners() {
        jQuery('#filtro-noticias').click(function(e) {
            e.preventDefault(); jQuery('#midias').html('</p><div style="margin-bottom: 30px">Carregando...</div><p>');
            jQuery.get('/wp-content/themes/academy/xhr-filtrar-midias.php?midia=news', function(data) {
                jQuery('#midias').html(data);
            });
        });

        jQuery('#filtro-artigos').click(function(e) {
            e.preventDefault();
            jQuery('#midias').html('</p><div style="margin-bottom: 30px">Carregando...</div><p>');
            jQuery.get('/wp-content/themes/academy/xhr-filtrar-midias.php?midia=articles', function(data) {
                jQuery('#midias').html(data);
            });
        });

        jQuery('#filtro-videos').click(function(e) {
            e.preventDefault();
            jQuery('#midias').html('</p><div style="margin-bottom: 30px">Carregando...</div><p>');
            jQuery.get('/wp-content/themes/academy/xhr-filtrar-midias.php?midia=videos', function(data) {
                jQuery('#midias').html(data);
            });
        });
    }

    return {
        'init': function() {
            init();
        }
    };
}();

NoticiasAtualizadas.init();