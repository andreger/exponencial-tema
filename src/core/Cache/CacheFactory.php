<?php

namespace Exponencial\Core\Cache;

use Memcached;

class CacheFactory
{
    const HOME_NOTICIAS_ATUALIZADAS = 'home:noticias-atualizadas';

    const COMUM_CADASTRO_GRATIS = 'comum:cadastro-gratis';

    const COMUM_FOOTER = 'comum:footer';

    const PROFESSOR_ARTIGOS = "blogs:artigos-professor-";

    const PROFESSOR_TOTAL_ARTIGOS = "blogs:total-artigos-professor-";


    public function getMemcached(): Memcached
    {
        $memcached = new Memcached();
        $memcached->addServer("127.0.0.1", 11211);

        return $memcached;
    }

}