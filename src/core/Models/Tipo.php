<?php

namespace Exponencial\Core\Models;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\Table;

/**
 * @Entity
 * @Table(name="tipos")
 */
class Tipo
{
    public const NOTICIA = 1;

    public const ARTIGO = 2;

    public const VIDEO = 14;

    /**
     * @Id
     * @Column(type="integer")
     */
    private $id;

    /**
     * @Column(type="string")
     */
    private $nome;

    /**
     * @ManyToMany(targetEntity="Blog", inversedBy="tipos")
     */
    private $blogs;

    /**
     * Construtor de Tipo
     */

    public function __construct()
    {
        $this->blogs = new ArrayCollection();
    }

    /**
     * Retorna o ID
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Define o ID
     *
     * @param int $id
     * @return Tipo
     */
    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Retorna o nome
     *
     * @return string
     */
    public function getNome(): string
    {
        return $this->nome;
    }

    /**
     * Define o nome
     *
     * @param string $nome
     * @return Tipo
     */
    public function setNome(string $nome): self
    {
        $this->nome = $nome;
        return $this;
    }

    /**
     * Adiciona um blog ao tipo
     *
     * @param Blog $blog
     * @return $this
     */
    public function addBlog(Blog $blog): self
    {
        if ($this->blogs->contains($blog)) {
            return $this;
        }

        $this->blogs->add($blog);
        $blog->addTipo($this);

        return $this;
    }

    /**
     * Retorna os blogs associados.
     *
     * @return Collection
     */
    public function getBlogs(): Collection
    {
        return $this->blogs;
    }
}
