<?php

namespace Exponencial\Core\Models;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;

/**
 * Class Blog
 * @Entity
 * @Table(name="blogs")
 */
class Blog
{
    /**
     * @Id
     * @Column(type="integer", name="blo_id")
     */
    private $id;

    /**
     * @Column(type="string", name="blo_slug")
     */
    private $slug;

    /**
     * @Column(type="string", name="blo_thumb")
     */
    private $thumb;

    /**
     * @Column(type="string", name="blo_titulo")
     */
    private $titulo;

    /**
     * @Column(type="string", name="blo_conteudo")
     */
    private $conteudo;

    /**
     * @Column(type="string", name="blo_resumo")
     */
    private $resumo;

    /**
     * @Column(type="string", name="blo_created_on")
     */
    private $created;

    /**
     * @Column(type="string", name="blo_updated_on")
     */
    private $updated;

    /**
     * @ManyToMany(targetEntity="Tipo", mappedBy="blogs")
     * @JoinTable(name="blogs_tipos",
     *      joinColumns={
     *          @JoinColumn(name="blog_id", referencedColumnName="blo_id")
     *      },
     *      inverseJoinColumns={
     *          @JoinColumn(name="blo_tipo", referencedColumnName="id")
     *      }
     * )
     */
    private $tipos;

    /**
     * Construtor de Blog.
     */
    public function __construct()
    {
        $this->tipos = new ArrayCollection();
    }

    /**
     * Retorna o ID.
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Define o ID.
     *
     * @param int $id
     * @return Blog
     */
    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Recupera o slug.
     *
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * Define o slug.
     *
     * @param string $slug
     * @return $this
     */
    public function setSlug(string $slug): self
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * Recupera o thumb.
     *
     * @return string
     */
    public function getThumb(): string
    {
        return $this->thumb;
    }

    /**
     * Define o thumb.
     *
     * @param string $thumb
     * @return $this
     */
    public function setThumb(string $thumb): self
    {
        $this->thumb = $thumb;
        return $this;
    }

    /**
     * Recupera o título
     *
     * @return string
     */
    public function getTitulo(): string
    {
        return $this->titulo;
    }

    /**
     * Define o título.
     *
     * @param string $titulo
     * @return $this
     */
    public function setTitulo(string $titulo): self
    {
        $this->titulo = $titulo;
        return $this;
    }

    /**
     * Recupera o conteúdo.
     *
     * @return string
     */
    public function getConteudo(): string
    {
        return $this->conteudo;
    }

    /**
     * Define o conteúdo.
     *
     * @param string $conteudo
     * @return $this
     */
    public function setConteudo(string $conteudo): self
    {
        $this->conteudo = $conteudo;
        return $this;
    }

    /**
     * Recupera o resumo.
     *
     * @return string
     */
    public function getResumo(): string
    {
        return $this->resumo;
    }

    /**
     * Define o resumo.
     *
     * @param string $resumo
     * @return $this
     */
    public function setResumo(string $resumo): self
    {
        $this->resumo = $resumo;
        return $this;
    }

    /**
     * Recupera a data de criação.
     *
     * @return DateTime
     */
    public function getCreated(): DateTime
    {
        return $this->created;
    }

    /**
     * Define a data de criação.
     *
     * @param DateTime $created
     * @return $this
     */
    public function setCreated(DateTime $created): self
    {
        $this->created = $created;
        return $this;
    }

    /**
     * Recupera a data de atualização.
     *
     * @return DateTime
     */
    public function getUpdated(): DateTime
    {
        return $this->updated;
    }

    /**
     * Define a data de atualização.
     *
     * @param DateTime $updated
     * @return $this
     */
    public function setUpdated(DateTime $updated): self
    {
        $this->updated = $updated;
    }

    /**
     * Adiciona um tipo ao blog.
     *
     * @param Tipo $tipo
     * @return $this
     */
    public function addTipo(Tipo $tipo): self
    {
        if ($this->tipos->contains($tipo)) {
            return $this;
        }

        $this->tipos->add($tipo);
        $tipo->addBlog($this);

        return $this;
    }

    /**
     * Retorna os tipos associados.
     *
     * @return Collection
     */
    public function getTipos(): Collection
    {
        return $this->tipos;
    }
}
